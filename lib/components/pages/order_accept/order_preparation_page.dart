import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/atoms/medium_sized_card.dart';
import 'package:li/components/molecules/divider_text_tab.dart';
import 'package:li/components/pages/add_new_card_payment.dart';
import 'package:li/const.dart';
import 'package:li/main.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/components/molecules/CustomTextField.dart';
import 'package:time_app_components/utils/testStyles.dart';

class OrderPrepPage extends StatefulWidget {
  const OrderPrepPage({Key? key}) : super(key: key);

  @override
  _OrderPrepPageState createState() => _OrderPrepPageState();
}

class _OrderPrepPageState extends State<OrderPrepPage> {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Stack(
        children: [
          CustomScrollView(
            physics: ClampingScrollPhysics(),
            slivers: [
              SliverToBoxAdapter(
                child: SizedBox(
                  height: MediaQuery.of(context).padding.top + 56 - 16,
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  height: 376,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.vertical(
                      bottom: Radius.circular(16),
                    ),
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: const SizedBox(
                  height: 20,
                ),
              ),
              SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Адрес доставки',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w600,
                          color: ColorData.allMainBlack,
                        ),
                      ),
                      SizedBox(height: 10),
                      InkWell(
                        onTap: () => showModalBottomSheet(
                            isScrollControlled: true,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            backgroundColor: Colors.white,
                            context: context,
                            builder: (context) {
                              return FractionallySizedBox(
                                heightFactor: 0.9,
                                child: Column(
                                  children: [
                                    Container(
                                      padding: EdgeInsets.all(20),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(15),
                                          topRight: Radius.circular(15),
                                        ),
                                        boxShadow: bigBlur,
                                        color: Colors.white,
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            height: 20,
                                            width: 20,
                                          ),
                                          Text(
                                            'Адрес доставки',
                                            style: TextStyle(
                                              fontSize: 18,
                                              color: ColorData.allMainBlack,
                                              fontWeight: FontWeight.w500,
                                            ),
                                          ),
                                          SvgPicture.asset(
                                            'assets/svg/icon/close.svg',
                                            color: ColorData.allMainBlack,
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 24,
                                    ),
                                    DividerTapTile(
                                      assetSvg: 'assets/svg/icon/location.svg',
                                      title: 'Новый адрес',
                                      onTap: () => showModalBottomSheet(
                                          isScrollControlled: true,
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                          ),
                                          backgroundColor: Colors.white,
                                          context: context,
                                          builder: (context) {
                                            return FractionallySizedBox(
                                              heightFactor: 0.9,
                                              child: Column(
                                                children: [
                                                  Container(
                                                    padding: EdgeInsets.all(20),
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.only(
                                                        topLeft:
                                                            Radius.circular(15),
                                                        topRight:
                                                            Radius.circular(15),
                                                      ),
                                                      boxShadow: bigBlur,
                                                      color: Colors.white,
                                                    ),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Container(
                                                          height: 20,
                                                          width: 20,
                                                        ),
                                                        Text(
                                                          'Адрес доставки',
                                                          style: TextStyle(
                                                            fontSize: 18,
                                                            color: ColorData
                                                                .allMainBlack,
                                                            fontWeight:
                                                                FontWeight.w500,
                                                          ),
                                                        ),
                                                        SvgPicture.asset(
                                                          'assets/svg/icon/close.svg',
                                                          color: ColorData
                                                              .allMainBlack,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        horizontal: 20.0),
                                                    child: Column(
                                                      children: [
                                                        SizedBox(
                                                          height: 40,
                                                        ),
                                                        Row(
                                                          children: [
                                                            Flexible(
                                                              flex: 3,
                                                              child: CustomTextField(
                                                                  labelColor:
                                                                      ColorData
                                                                          .allMainActivegray,
                                                                  label:
                                                                      'Адрес доставки'),
                                                            ),
                                                            SizedBox(
                                                              width: 10,
                                                            ),
                                                            Expanded(
                                                              flex: 1,
                                                              child: Container(
                                                                decoration:
                                                                    BoxDecoration(
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                              15),
                                                                  color: Color(
                                                                      0xffFAFAFA),
                                                                ),
                                                                child: Padding(
                                                                  padding: const EdgeInsets
                                                                          .symmetric(
                                                                      vertical:
                                                                          20.0),
                                                                  child:
                                                                      SvgPicture
                                                                          .asset(
                                                                    'assets/svg/icon/location.svg',
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        Row(
                                                          children: [
                                                            Expanded(
                                                                child:
                                                                    CustomTextField(
                                                              labelColor: ColorData
                                                                  .allMainActivegray,
                                                              label: 'Подъезд',
                                                            )),
                                                            SizedBox(
                                                              width: 10,
                                                            ),
                                                            Expanded(
                                                                child:
                                                                    CustomTextField(
                                                              labelColor: ColorData
                                                                  .allMainActivegray,
                                                              label: 'Домофон',
                                                            )),
                                                          ],
                                                        ),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        Row(
                                                          children: [
                                                            Expanded(
                                                                child:
                                                                    CustomTextField(
                                                              labelColor: ColorData
                                                                  .allMainActivegray,
                                                              label: 'Кв./Офис',
                                                            )),
                                                            SizedBox(
                                                              width: 10,
                                                            ),
                                                            Expanded(
                                                              child:
                                                                  CustomTextField(
                                                                labelColor:
                                                                    ColorData
                                                                        .allMainActivegray,
                                                                label: 'Этаж',
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        CustomTextField(
                                                          labelColor: ColorData
                                                              .allMainActivegray,
                                                          label:
                                                              'Комментарий курьеру',
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            );
                                          }),
                                    ),
                                    const Divider(
                                      height: 0,
                                      indent: 20,
                                      endIndent: 20,
                                      color: ColorData.allMainGray,
                                      thickness: 0.5,
                                    ),
                                    DividerTapTile(
                                      onTap: () {},
                                      assetSvg: 'assets/svg/icon/location.svg',
                                      title: 'ул. Афцинао 4 блок 2',
                                      subTitle:
                                          'подъезд 2, квартира 39, 4 этаж',
                                    ),
                                    const Divider(
                                      height: 0,
                                      indent: 20,
                                      endIndent: 20,
                                      color: ColorData.allMainGray,
                                      thickness: 0.5,
                                    ),
                                    DividerTapTile(
                                      onTap: () {},
                                      assetSvg: 'assets/svg/icon/location.svg',
                                      title: 'ул. Шевченко 85',
                                      subTitle:
                                          'подъезд 1, квартира 12, 5 этаж',
                                    ),
                                    const Divider(
                                      height: 0,
                                      indent: 20,
                                      endIndent: 20,
                                      color: ColorData.allMainGray,
                                      thickness: 0.5,
                                    ),
                                  ],
                                ),
                              );
                            }),
                        child: MediumSizedCard(
                          subtitle: 'изменить',
                          title: 'Укажите адрес доставки',
                          icon: CircleAvatar(
                            maxRadius: 20,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: const SizedBox(
                  height: 40,
                ),
              ),
              SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Выбранные блюда',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w600,
                          color: ColorData.allMainBlack,
                        ),
                      ),
                      SizedBox(height: 10),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: ColorData.allMessagesbackground,
                        ),
                        padding: EdgeInsets.all(15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Паста «Фетучини»',
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                SizedBox(height: 5),
                                Text(
                                  '1550 ₸ — 300 г. ',
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                Text(
                                  'Итальянская кухня — второе',
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: ColorData.allMainActivegray),
                                ),
                              ],
                            ),
                            CircleAvatar(minRadius: 20),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: const SizedBox(
                  height: 40,
                ),
              ),
              SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Оплата',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w600,
                          color: ColorData.allMainBlack,
                        ),
                      ),
                      SizedBox(height: 10),
                      InkWell(
                        onTap: () => showModalBottomSheet(
                            isScrollControlled: true,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            backgroundColor: Colors.white,
                            context: context,
                            builder: (context) {
                              return FractionallySizedBox(
                                heightFactor: 0.9,
                                child: Column(
                                  children: [
                                    Container(
                                      padding: EdgeInsets.all(20),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(15),
                                          topRight: Radius.circular(15),
                                        ),
                                        boxShadow: bigBlur,
                                        color: Colors.white,
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            height: 20,
                                            width: 20,
                                          ),
                                          Text(
                                            'Способ Оплаты',
                                            style: TextStyle(
                                              fontSize: 18,
                                              color: ColorData.allMainBlack,
                                              fontWeight: FontWeight.w500,
                                            ),
                                          ),
                                          SvgPicture.asset(
                                            'assets/svg/icon/close.svg',
                                            color: ColorData.allMainBlack,
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 24,
                                    ),
                                    DividerTapTile(
                                      onTap: () {},
                                      assetSvg: 'assets/svg/icon/applePay.svg',
                                      title: 'ApplePay',
                                    ),
                                    const Divider(
                                      height: 0,
                                      indent: 20,
                                      endIndent: 20,
                                      color: ColorData.allMainGray,
                                      thickness: 0.5,
                                    ),
                                    DividerTapTile(
                                        onTap: () {},
                                        assetSvg: 'assets/svg/icon/Card.svg',
                                        subTitle: '**** 2312',
                                        title: 'MasterCard'),
                                    const Divider(
                                      height: 0,
                                      indent: 20,
                                      endIndent: 20,
                                      color: ColorData.allMainGray,
                                      thickness: 0.5,
                                    ),
                                    DividerTapTile(
                                      onTap: () => routerDelegate.push(
                                        const AddNewCardPayment(),
                                      ),
                                      assetSvg: 'assets/svg/icon/Card.svg',
                                      title: 'Добавить карту',
                                    ),
                                    const Divider(
                                      height: 0,
                                      indent: 20,
                                      endIndent: 20,
                                      color: ColorData.allMainGray,
                                      thickness: 0.5,
                                    ),
                                  ],
                                ),
                              );
                            }),
                        child: MediumSizedCard(
                          subtitle: 'изменить способ оплаты',
                          title: 'Apple Pay',
                          icon: SvgPicture.asset(
                            'assets/svg/icon/applePay.svg',
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: const SizedBox(
                  height: 40,
                ),
              ),
              SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Счет',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w600,
                          color: ColorData.allMainBlack,
                        ),
                      ),
                      SizedBox(height: 10),
                      Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'data',
                                style: TextStyle(
                                  fontSize: P3TextStyle.fontSize,
                                  fontWeight: P3TextStyle.fontWeight,
                                  height: P3TextStyle.height,
                                  color: ColorData.allMainActivegray,
                                ),
                              ),
                              Text(
                                'data',
                                style: TextStyle(
                                  fontSize: P3TextStyle.fontSize,
                                  fontWeight: P3TextStyle.fontWeight,
                                  height: P3TextStyle.height,
                                  color: ColorData.allMainBlack,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'data',
                                style: TextStyle(
                                  fontSize: P3TextStyle.fontSize,
                                  fontWeight: P3TextStyle.fontWeight,
                                  height: P3TextStyle.height,
                                  color: ColorData.allMainActivegray,
                                ),
                              ),
                              Text(
                                'data',
                                style: TextStyle(
                                  fontSize: P3TextStyle.fontSize,
                                  fontWeight: P3TextStyle.fontWeight,
                                  height: P3TextStyle.height,
                                  color: ColorData.allMainBlack,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'data',
                                style: TextStyle(
                                  fontSize: P3TextStyle.fontSize,
                                  fontWeight: P3TextStyle.fontWeight,
                                  height: P3TextStyle.height,
                                  color: ColorData.allMainActivegray,
                                ),
                              ),
                              Text(
                                'data',
                                style: TextStyle(
                                  fontSize: P3TextStyle.fontSize,
                                  fontWeight: P3TextStyle.fontWeight,
                                  height: P3TextStyle.height,
                                  color: ColorData.allMainBlack,
                                ),
                              ),
                            ],
                          ),
                          Divider(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'data',
                                style: TextStyle(
                                  fontSize: P3TextStyle.fontSize,
                                  fontWeight: P3TextStyle.fontWeight,
                                  height: P3TextStyle.height,
                                  color: ColorData.allMainActivegray,
                                ),
                              ),
                              Text(
                                'data',
                                style: TextStyle(
                                  fontSize: P3TextStyle.fontSize,
                                  fontWeight: P3TextStyle.fontWeight,
                                  height: P3TextStyle.height,
                                  color: ColorData.allMainBlack,
                                ),
                              ),
                            ],
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: const SizedBox(
                  height: 200,
                ),
              ),
            ],
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: CustomAppBar(
              title: Text(
                'Заказ готовится',
                style: TextStyle(
                  color: ColorData.allMainBlack,
                ),
              ),
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              height: MediaQuery.of(context).padding.bottom + 100,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16),
                  topRight: Radius.circular(16),
                ),
                boxShadow: bigBlur,
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                  top: 20.0,
                  bottom: 20,
                  left: 20,
                  right: 20,
                ),
                child: ElevatedButton(
                  onPressed: () {},
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(Color(0xffB4B4B4)),
                      fixedSize: MaterialStateProperty.all(Size(60, 60)),
                      elevation: MaterialStateProperty.all(0.0),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(16),
                        ),
                      )),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Выберите адрес доставки',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: P2TextStyle.fontSize,
                          height: P2TextStyle.height,
                          fontWeight: P2TextStyle.fontWeight,
                        ),
                      ),
                      Text(
                        'Нажмите, чтобы продолжить',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: P4TextStyle.fontSize,
                          height: P4TextStyle.height,
                          fontWeight: P4TextStyle.fontWeight,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

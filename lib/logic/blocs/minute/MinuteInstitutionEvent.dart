import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:li/logic/blocs/minute/MinuteInstitutionState.dart';

class MinuteInstitutionEvent extends Equatable {
  const MinuteInstitutionEvent();

  @override
  List<Object?> get props => [];
}

class GetCurrentServiceEvent extends MinuteInstitutionEvent {
  final String institutionId;
  final Completer<MinuteInstitutionState> state;

  const GetCurrentServiceEvent({
    required this.institutionId,
    required this.state,
  });
}

class GetServiceEvent extends MinuteInstitutionEvent {
  final String serviceId;
  final Completer<MinuteInstitutionState> state;

  const GetServiceEvent({
    required this.serviceId,
    required this.state,
  });
}

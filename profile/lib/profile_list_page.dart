import 'dart:io';

import 'package:flutter/material.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/logic/firebase_token/firebase_token_device.dart';
import 'package:li/logic/providers/CreditCard.dart';
import 'package:li/main.dart';
import 'package:li/utils/restart_widget.dart';
import 'package:profile/CardPaymentCard.dart';
import 'package:profile/about/about_page.dart';
import 'package:profile/bonusPages/bonusCard.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'bloc/profile_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'model/profile_list_model.dart';
import 'personal_tile.dart';
import 'profile_tile.dart';

import 'package:li/network/graphql_provider.dart';
import 'package:provider/provider.dart';

class ProfileListPage extends StatelessWidget {
  const ProfileListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    late final graphqlProvider = Provider.of<GraphqlProvider>(
      context,
      listen: false,
    );
    return Scaffold(
        appBar: CustomAppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(Icons.arrow_back),
            color: const Color(0xff14142B),
          ),
          title: const Text(
            'Мой профиль',
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w500,
              color: Color(0xff424242),
            ),
          ),
          actions: [
            IconButton(
              onPressed: () async {
                await Future.wait([
                  deleteID(
                    deviceId: await getId(),
                    provider: graphqlProvider,
                  ),
                  sharedPreferences.clear()
                ]);
                RestartWidget.restartApp(context);
              },
              icon: const Icon(
                Icons.login_outlined,
                color: Colors.black,
                size: 24,
              ),
            ),
            const SizedBox(width: 10),
          ],
        ),
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: BlocBuilder<ProfileBloc, ProfileState>(
                builder: (context, state) {
                  if (state is ProfileInformation) {
                    // client = state.client;
                    // bonus = state.client.bonus!;
                    return SizeTapAnimation(
                      child: PersonalTile(client: state.client),
                    );
                  }
                  return const SizedBox(height: 0);
                },
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate([
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Container(
                    height: 118,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Expanded(
                          child: BonusCard(),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Consumer<CreditCardProvider>(
                            builder: (context, value, child) {
                              return const PaymentCard();
                            },
                          ),
                        )

                        // TODO
                        // Expanded(
                        //   child: PaymentCard(creditCard: creditCard),
                        // ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                ProfileTile(
                  title: 'Поддержка',
                  subTitle: 'Справочный центр и связь со специалистом',
                  onTap: () {
                    launchLink('https://timeapp.kz/client/help');
                  },
                  svgAsset: 'assets/svg/profile/support.svg',
                ),
                ProfileTile(
                  title: 'Приложение для исполнителей',
                  onTap: () {
                    launchLink('http://timeapp.kz/business');
                  },
                  svgAsset: 'assets/svg/profile/timeAppLogoBlue.svg',
                  isSmall: true,
                ),
                ProfileTile(
                  title: 'Новости',
                  subTitle: 'Будьте в курсе обновлений и изменений в TimeApp',
                  svgAsset: 'assets/svg/profile/news.svg',
                  onTap: () {
                    launchLink('https://timeapp.kz/news');
                  },
                ),
                Platform.isIOS
                    ? ProfileTile(
                        title: 'Оценить приложение',
                        subTitle: 'Переход в App Store',
                        svgAsset: 'assets/svg/profile/appStore.svg',
                        onTap: () {
                          launchLink(
                              'https://apps.apple.com/ru/app/timeapp/id1514221318');
                        },
                      )
                    : ProfileTile(
                        title: 'Оценить приложение',
                        subTitle: 'Переход в Google Play',
                        svgAsset: 'assets/svg/profile/appStore.svg',
                        onTap: () {
                          launchLink(
                            'https://play.google.com/store/apps/details?id=kz.time.app',
                          );
                        },
                      ),
                ProfileTile(
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const AboutPage(),
                    ),
                  ),
                  title: 'О приложении',
                  subTitle: 'Версия ${packageInfo.version}',
                  svgAsset: 'assets/svg/profile/aboutApp.svg',
                ),
              ]),
            ),
            const SliverToBoxAdapter(
              child: SizedBox(
                height: 40,
              ),
            ),
          ],
        ));
  }
}

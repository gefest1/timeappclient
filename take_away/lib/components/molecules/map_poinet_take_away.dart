import 'package:flutter/material.dart';
import 'package:li/components/icons/motorcycle.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

bool hey = false;

class MapPointerTakeAway extends StatefulWidget {
  final String? title;
  final String? label;
  final String? tile;

  final VoidCallback? onTap;

  const MapPointerTakeAway({
    this.title,
    this.label,
    this.tile,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  State<MapPointerTakeAway> createState() => _MapPointerTakeAwayState();
}

class _MapPointerTakeAwayState extends State<MapPointerTakeAway> {
  late PointerDownEvent initEvent;

  @override
  Widget build(BuildContext context) {
    return Listener(
      behavior: HitTestBehavior.translucent,
      onPointerDown: (PointerDownEvent _) {
        initEvent = _;
        hey = true;
      },
      onPointerUp: (_) async {
        if (!hey) return;

        hey = false;
        final _diff = initEvent.position - _.position;
        if (_diff.dx > -10 &&
            _diff.dx < 10 &&
            _diff.dy > -10 &&
            _diff.dy < 10) {
          widget.onTap?.call();
        }
      },
      child: RepaintBoundary(
        child: IgnorePointer(
          ignoring: true,
          child: CustomPaint(
            painter: MapPointerTakeAwaylPainter(),
            child: Padding(
              padding: const EdgeInsets.only(bottom: 20),
              child: SizedBox(
                width: 194,
                child: Padding(
                  padding: const EdgeInsets.all(14),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        children: [
                          const CircleAvatar(
                            radius: 22.5,
                            backgroundColor: Colors.grey,
                          ),
                          const SizedBox(width: 10),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                RichText(
                                  textAlign: TextAlign.left,
                                  text: TextSpan(
                                    children: [
                                      const TextSpan(
                                        text: 'Наталья О.',
                                        style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w600,
                                          color: Color(0xFF434343),
                                        ),
                                      ),
                                      WidgetSpan(
                                        alignment: PlaceholderAlignment.middle,
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(left: 2),
                                          child: Container(
                                            width: 11,
                                            height: 11,
                                            decoration: const BoxDecoration(
                                              color: Color(0xFFBEFFC5),
                                              shape: BoxShape.circle,
                                            ),
                                            alignment: Alignment.center,
                                            child: Container(
                                              height: 5,
                                              width: 5,
                                              decoration: const BoxDecoration(
                                                color: Color(0xFF0FF426),
                                                shape: BoxShape.circle,
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                const Text(
                                  'ул.Сейфуллина, 120',
                                  style: TextStyle(
                                    color: ColorData.allMainActivegray,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    height: 14 / 12,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 6),
                      Container(
                        height: 30,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(6),
                        ),
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: const [
                            CustomPaint(
                              painter: MotorCycleCustomPainter(),
                              size: Size(12, 12),
                            ),
                            SizedBox(width: 2),
                            Text(
                              '15 мин',
                              style: TextStyle(
                                fontSize: P3TextStyle.fontSize,
                                fontWeight: P3TextStyle.fontWeight,
                                height: P3TextStyle.height,
                                color: ColorData.allMainBlack,
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class MapPointerTakeAwaylPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path _path = Path();

    _path.cubicTo(7.02767, 0, 13.1132, 4.87915, 14.6412, 11.7387);
    _path.lineTo(16.7662, 21.279);
    _path.cubicTo(16.9942, 22.3024, 18.4431, 22.3298, 18.7095, 21.3157);
    _path.lineTo(21.3702, 11.1885);
    _path.cubicTo(23.1022, 4.59591, 28.9686, 0, 35.7849, 0);

    _path.close();
    _path = _path.shift(
      Offset(size.width / 2 - 18, size.height - 21),
    );

    _path = Path.combine(
      PathOperation.union,
      _path,
      Path()
        ..addRRect(
          RRect.fromLTRBR(
            0,
            0,
            size.width,
            size.height - 20,
            const Radius.circular(16),
          ),
        ),
    );
    canvas.drawPath(
      _path,
      const BoxShadow(
        offset: Offset(1, 1),
        blurRadius: 5,
        color: Colors.black12,
      ).toPaint(),
    );
    canvas.drawPath(
      _path,
      Paint()..color = ColorData.allMessagesbackground,
    );
  }

  @override
  bool shouldRepaint(MapPointerTakeAwaylPainter oldDelegate) => true;

  @override
  bool shouldRebuildSemantics(MapPointerTakeAwaylPainter oldDelegate) => true;
}

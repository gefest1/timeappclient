import 'package:flutter/material.dart';
import 'package:li/const.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class MediumSizedCard extends StatefulWidget {
  final String title;
  final String? subtitle;
  final Widget icon;

  MediumSizedCard(
      {required this.subtitle,
      required this.title,
      required this.icon,
      Key? key})
      : super(key: key);

  @override
  _MediumSizedCardState createState() => _MediumSizedCardState();
}

class _MediumSizedCardState extends State<MediumSizedCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 20,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        boxShadow: bigBlur,
        color: Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.title,
                      style: TextStyle(
                        fontSize: H3TextStyle.fontSize,
                        color: ColorData.allMainBlack,
                        height: H3TextStyle.height,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    if (widget.subtitle != null)
                      Text(
                        widget.subtitle!,
                        style: TextStyle(
                          fontSize: P4TextStyle.fontSize,
                          color: ColorData.specsButtonColorDefault,
                          height: P4TextStyle.height,
                          fontWeight: P4TextStyle.fontWeight,
                        ),
                      ),
                  ],
                ),
              ),
              const SizedBox(width: 16),
              widget.icon,
            ],
          ),
        ],
      ),
    );
  }
}

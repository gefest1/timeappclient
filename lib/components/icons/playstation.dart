import 'package:flutter/material.dart';

class PlaystationCustomPainter extends CustomPainter {
  const PlaystationCustomPainter({Listenable? repaint})
      : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.9723250, size.height * 0.2460675);
    path_0.lineTo(size.width * 0.9575643, size.height * 0.2460675);
    path_0.lineTo(size.width * 0.04243536, size.height * 0.2460675);
    path_0.lineTo(size.width * 0.04243536, size.height * 0.2460696);
    path_0.lineTo(size.width * 0.02767571, size.height * 0.2460696);
    path_0.cubicTo(size.width * 0.01239061, size.height * 0.2460714, 0,
        size.height * 0.2584621, 0, size.height * 0.2737471);
    path_0.lineTo(0, size.height * 0.4859250);
    path_0.cubicTo(
        0,
        size.height * 0.5012107,
        size.width * 0.01239061,
        size.height * 0.5136000,
        size.width * 0.02767571,
        size.height * 0.5136000);
    path_0.lineTo(size.width * 0.1117636, size.height * 0.5136000);
    path_0.cubicTo(
        size.width * 0.1252829,
        size.height * 0.4824714,
        size.width * 0.1477125,
        size.height * 0.4560500,
        size.width * 0.1769568,
        size.height * 0.4373821);
    path_0.cubicTo(
        size.width * 0.2157282,
        size.height * 0.4126321,
        size.width * 0.2618236,
        size.height * 0.4044643,
        size.width * 0.3067379,
        size.height * 0.4143786);
    path_0.lineTo(size.width * 0.3695429, size.height * 0.4282464);
    path_0.lineTo(size.width * 0.6304571, size.height * 0.4282464);
    path_0.lineTo(size.width * 0.6932607, size.height * 0.4143786);
    path_0.cubicTo(
        size.width * 0.7381857,
        size.height * 0.4044679,
        size.width * 0.7842750,
        size.height * 0.4126357,
        size.width * 0.8230429,
        size.height * 0.4373821);
    path_0.cubicTo(
        size.width * 0.8522857,
        size.height * 0.4560500,
        size.width * 0.8747179,
        size.height * 0.4824679,
        size.width * 0.8882357,
        size.height * 0.5136000);
    path_0.lineTo(size.width * 0.9723250, size.height * 0.5136000);
    path_0.cubicTo(size.width * 0.9876107, size.height * 0.5136000, size.width,
        size.height * 0.5012107, size.width, size.height * 0.4859250);
    path_0.lineTo(size.width, size.height * 0.2737454);
    path_0.cubicTo(
        size.width,
        size.height * 0.2584582,
        size.width * 0.9876107,
        size.height * 0.2460675,
        size.width * 0.9723250,
        size.height * 0.2460675);
    path_0.close();
    path_0.moveTo(size.width * 0.1715857, size.height * 0.3881393);
    path_0.lineTo(size.width * 0.1162361, size.height * 0.3881393);
    path_0.cubicTo(
        size.width * 0.1060468,
        size.height * 0.3881393,
        size.width * 0.09778679,
        size.height * 0.3798786,
        size.width * 0.09778679,
        size.height * 0.3696893);
    path_0.cubicTo(
        size.width * 0.09778679,
        size.height * 0.3595000,
        size.width * 0.1060486,
        size.height * 0.3512393,
        size.width * 0.1162361,
        size.height * 0.3512393);
    path_0.lineTo(size.width * 0.1715857, size.height * 0.3512393);
    path_0.cubicTo(
        size.width * 0.1817750,
        size.height * 0.3512393,
        size.width * 0.1900346,
        size.height * 0.3595000,
        size.width * 0.1900346,
        size.height * 0.3696893);
    path_0.cubicTo(
        size.width * 0.1900346,
        size.height * 0.3798786,
        size.width * 0.1817750,
        size.height * 0.3881393,
        size.width * 0.1715857,
        size.height * 0.3881393);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Colors.white;
    canvas.drawPath(path_0, paint_0_fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.9723214, size.height * 0.07083571);
    path_1.lineTo(size.width * 0.02767571, size.height * 0.07083571);
    path_1.cubicTo(size.width * 0.01239061, size.height * 0.07083571, 0,
        size.height * 0.08322643, 0, size.height * 0.09851143);
    path_1.lineTo(0, size.height * 0.1907611);
    path_1.cubicTo(
        0,
        size.height * 0.2060464,
        size.width * 0.01239061,
        size.height * 0.2184368,
        size.width * 0.02767571,
        size.height * 0.2184368);
    path_1.lineTo(size.width * 0.04398250, size.height * 0.2184368);
    path_1.lineTo(size.width * 0.9560179, size.height * 0.2184368);
    path_1.lineTo(size.width * 0.9723250, size.height * 0.2184368);
    path_1.cubicTo(
        size.width * 0.9876071,
        size.height * 0.2184368,
        size.width * 0.9999964,
        size.height * 0.2060464,
        size.width * 0.9999964,
        size.height * 0.1907611);
    path_1.lineTo(size.width * 0.9999964, size.height * 0.09851143);
    path_1.cubicTo(
        size.width * 0.9999964,
        size.height * 0.08322643,
        size.width * 0.9876071,
        size.height * 0.07083571,
        size.width * 0.9723214,
        size.height * 0.07083571);
    path_1.close();

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = Colors.white;
    canvas.drawPath(path_1, paint_1_fill);

    Path path_2 = Path();
    path_2.moveTo(size.width * 0.9263714, size.height * 0.8427036);
    path_2.lineTo(size.width * 0.8624929, size.height * 0.5533607);
    path_2.cubicTo(
        size.width * 0.8547000,
        size.height * 0.5180679,
        size.width * 0.8336321,
        size.height * 0.4879214,
        size.width * 0.8031679,
        size.height * 0.4684750);
    path_2.cubicTo(
        size.width * 0.7999143,
        size.height * 0.4663964,
        size.width * 0.7965821,
        size.height * 0.4645000,
        size.width * 0.7932000,
        size.height * 0.4627214);
    path_2.cubicTo(
        size.width * 0.7932821,
        size.height * 0.4618536,
        size.width * 0.7933357,
        size.height * 0.4609750,
        size.width * 0.7933357,
        size.height * 0.4600821);
    path_2.cubicTo(
        size.width * 0.7933357,
        size.height * 0.4448000,
        size.width * 0.7809429,
        size.height * 0.4324071,
        size.width * 0.7656571,
        size.height * 0.4324071);
    path_2.lineTo(size.width * 0.7103107, size.height * 0.4324071);
    path_2.cubicTo(
        size.width * 0.6969857,
        size.height * 0.4324071,
        size.width * 0.6858679,
        size.height * 0.4418250,
        size.width * 0.6832321,
        size.height * 0.4543679);
    path_2.lineTo(size.width * 0.6344607, size.height * 0.4651357);
    path_2.lineTo(size.width * 0.3654964, size.height * 0.4651357);
    path_2.lineTo(size.width * 0.3167239, size.height * 0.4543679);
    path_2.cubicTo(
        size.width * 0.3140893,
        size.height * 0.4418250,
        size.width * 0.3029700,
        size.height * 0.4324071,
        size.width * 0.2896479,
        size.height * 0.4324071);
    path_2.lineTo(size.width * 0.2342943, size.height * 0.4324071);
    path_2.cubicTo(
        size.width * 0.2190093,
        size.height * 0.4324071,
        size.width * 0.2066186,
        size.height * 0.4448000,
        size.width * 0.2066186,
        size.height * 0.4600821);
    path_2.cubicTo(
        size.width * 0.2066186,
        size.height * 0.4609750,
        size.width * 0.2066693,
        size.height * 0.4618536,
        size.width * 0.2067514,
        size.height * 0.4627214);
    path_2.cubicTo(
        size.width * 0.2033746,
        size.height * 0.4645000,
        size.width * 0.2000425,
        size.height * 0.4663964,
        size.width * 0.1967886,
        size.height * 0.4684750);
    path_2.cubicTo(
        size.width * 0.1663218,
        size.height * 0.4879214,
        size.width * 0.1452536,
        size.height * 0.5180643,
        size.width * 0.1374625,
        size.height * 0.5533571);
    path_2.lineTo(size.width * 0.07358357, size.height * 0.8427036);
    path_2.cubicTo(
        size.width * 0.06957214,
        size.height * 0.8608750,
        size.width * 0.07287679,
        size.height * 0.8795179,
        size.width * 0.08289036,
        size.height * 0.8952036);
    path_2.cubicTo(
        size.width * 0.09290000,
        size.height * 0.9108857,
        size.width * 0.1084214,
        size.height * 0.9217357,
        size.width * 0.1265914,
        size.height * 0.9257429);
    path_2.cubicTo(
        size.width * 0.1642029,
        size.height * 0.9340464,
        size.width * 0.2028061,
        size.height * 0.9272071,
        size.width * 0.2352807,
        size.height * 0.9064821);
    path_2.cubicTo(
        size.width * 0.2677514,
        size.height * 0.8857536,
        size.width * 0.2902064,
        size.height * 0.8536214,
        size.width * 0.2985129,
        size.height * 0.8160071);
    path_2.lineTo(size.width * 0.3051243, size.height * 0.7860607);
    path_2.cubicTo(
        size.width * 0.3147629,
        size.height * 0.7946179,
        size.width * 0.3261379,
        size.height * 0.8012464,
        size.width * 0.3386536,
        size.height * 0.8053536);
    path_2.cubicTo(
        size.width * 0.3479836,
        size.height * 0.8084143,
        size.width * 0.3579321,
        size.height * 0.8100929,
        size.width * 0.3682714,
        size.height * 0.8100929);
    path_2.cubicTo(
        size.width * 0.4027893,
        size.height * 0.8100929,
        size.width * 0.4330679,
        size.height * 0.7916357,
        size.width * 0.4497714,
        size.height * 0.7640750);
    path_2.cubicTo(
        size.width * 0.4513964,
        size.height * 0.7614000,
        size.width * 0.4528857,
        size.height * 0.7586321,
        size.width * 0.4542429,
        size.height * 0.7557929);
    path_2.lineTo(size.width * 0.5466429, size.height * 0.7557929);
    path_2.cubicTo(
        size.width * 0.5480643,
        size.height * 0.7586071,
        size.width * 0.5496214,
        size.height * 0.7613393,
        size.width * 0.5513036,
        size.height * 0.7639821);
    path_2.cubicTo(
        size.width * 0.5682143,
        size.height * 0.7905643,
        size.width * 0.5979143,
        size.height * 0.8082500,
        size.width * 0.6316857,
        size.height * 0.8082500);
    path_2.cubicTo(
        size.width * 0.6418857,
        size.height * 0.8082500,
        size.width * 0.6517071,
        size.height * 0.8066214,
        size.width * 0.6609250,
        size.height * 0.8036357);
    path_2.cubicTo(
        size.width * 0.6734429,
        size.height * 0.7995893,
        size.width * 0.6848250,
        size.height * 0.7930143,
        size.width * 0.6944893,
        size.height * 0.7845143);
    path_2.lineTo(size.width * 0.7014429, size.height * 0.8160071);
    path_2.cubicTo(
        size.width * 0.7097500,
        size.height * 0.8536250,
        size.width * 0.7322036,
        size.height * 0.8857536,
        size.width * 0.7646750,
        size.height * 0.9064821);
    path_2.cubicTo(
        size.width * 0.7879500,
        size.height * 0.9213393,
        size.width * 0.8146750,
        size.height * 0.9291929,
        size.width * 0.8419500,
        size.height * 0.9291929);
    path_2.cubicTo(
        size.width * 0.8524286,
        size.height * 0.9291929,
        size.width * 0.8629964,
        size.height * 0.9280321,
        size.width * 0.8733643,
        size.height * 0.9257429);
    path_2.cubicTo(
        size.width * 0.8915321,
        size.height * 0.9217357,
        size.width * 0.9070536,
        size.height * 0.9108857,
        size.width * 0.9170643,
        size.height * 0.8952036);
    path_2.cubicTo(
        size.width * 0.9270750,
        size.height * 0.8795179,
        size.width * 0.9303821,
        size.height * 0.8608750,
        size.width * 0.9263714,
        size.height * 0.8427036);
    path_2.close();
    path_2.moveTo(size.width * 0.7573571, size.height * 0.5431143);
    path_2.cubicTo(
        size.width * 0.7721321,
        size.height * 0.5431143,
        size.width * 0.7841071,
        size.height * 0.5550929,
        size.width * 0.7841071,
        size.height * 0.5698643);
    path_2.cubicTo(
        size.width * 0.7841071,
        size.height * 0.5846393,
        size.width * 0.7721321,
        size.height * 0.5966179,
        size.width * 0.7573571,
        size.height * 0.5966179);
    path_2.cubicTo(
        size.width * 0.7425821,
        size.height * 0.5966179,
        size.width * 0.7306036,
        size.height * 0.5846393,
        size.width * 0.7306036,
        size.height * 0.5698643);
    path_2.cubicTo(
        size.width * 0.7306036,
        size.height * 0.5550893,
        size.width * 0.7425821,
        size.height * 0.5431143,
        size.width * 0.7573571,
        size.height * 0.5431143);
    path_2.close();
    path_2.moveTo(size.width * 0.2859543, size.height * 0.6266643);
    path_2.lineTo(size.width * 0.2822668, size.height * 0.6266643);
    path_2.lineTo(size.width * 0.2822668, size.height * 0.6303536);
    path_2.cubicTo(
        size.width * 0.2822650,
        size.height * 0.6456393,
        size.width * 0.2698743,
        size.height * 0.6580286,
        size.width * 0.2545911,
        size.height * 0.6580286);
    path_2.cubicTo(
        size.width * 0.2393061,
        size.height * 0.6580286,
        size.width * 0.2269154,
        size.height * 0.6456393,
        size.width * 0.2269154,
        size.height * 0.6303536);
    path_2.lineTo(size.width * 0.2269154, size.height * 0.6266643);
    path_2.lineTo(size.width * 0.2232279, size.height * 0.6266643);
    path_2.cubicTo(
        size.width * 0.2079429,
        size.height * 0.6266643,
        size.width * 0.1955521,
        size.height * 0.6142750,
        size.width * 0.1955521,
        size.height * 0.5989893);
    path_2.cubicTo(
        size.width * 0.1955521,
        size.height * 0.5837036,
        size.width * 0.2079429,
        size.height * 0.5713143,
        size.width * 0.2232279,
        size.height * 0.5713143);
    path_2.lineTo(size.width * 0.2269154, size.height * 0.5713143);
    path_2.lineTo(size.width * 0.2269154, size.height * 0.5676250);
    path_2.cubicTo(
        size.width * 0.2269154,
        size.height * 0.5523429,
        size.width * 0.2393061,
        size.height * 0.5399500,
        size.width * 0.2545911,
        size.height * 0.5399500);
    path_2.cubicTo(
        size.width * 0.2698764,
        size.height * 0.5399500,
        size.width * 0.2822668,
        size.height * 0.5523429,
        size.width * 0.2822668,
        size.height * 0.5676250);
    path_2.lineTo(size.width * 0.2822668, size.height * 0.5713143);
    path_2.lineTo(size.width * 0.2859543, size.height * 0.5713143);
    path_2.cubicTo(
        size.width * 0.3012396,
        size.height * 0.5713143,
        size.width * 0.3136300,
        size.height * 0.5837036,
        size.width * 0.3136300,
        size.height * 0.5989893);
    path_2.cubicTo(
        size.width * 0.3136300,
        size.height * 0.6142750,
        size.width * 0.3012396,
        size.height * 0.6266643,
        size.width * 0.2859543,
        size.height * 0.6266643);
    path_2.close();
    path_2.moveTo(size.width * 0.3682714, size.height * 0.7694607);
    path_2.cubicTo(
        size.width * 0.3381186,
        size.height * 0.7694607,
        size.width * 0.3136750,
        size.height * 0.7450179,
        size.width * 0.3136750,
        size.height * 0.7148643);
    path_2.cubicTo(
        size.width * 0.3136750,
        size.height * 0.6847107,
        size.width * 0.3381186,
        size.height * 0.6602679,
        size.width * 0.3682714,
        size.height * 0.6602679);
    path_2.cubicTo(
        size.width * 0.3984250,
        size.height * 0.6602679,
        size.width * 0.4228679,
        size.height * 0.6847107,
        size.width * 0.4228679,
        size.height * 0.7148643);
    path_2.cubicTo(
        size.width * 0.4228679,
        size.height * 0.7450179,
        size.width * 0.3984250,
        size.height * 0.7694607,
        size.width * 0.3682714,
        size.height * 0.7694607);
    path_2.close();
    path_2.moveTo(size.width * 0.4999786, size.height * 0.6870214);
    path_2.cubicTo(
        size.width * 0.4852036,
        size.height * 0.6870214,
        size.width * 0.4732250,
        size.height * 0.6750429,
        size.width * 0.4732250,
        size.height * 0.6602714);
    path_2.cubicTo(
        size.width * 0.4732250,
        size.height * 0.6454964,
        size.width * 0.4852036,
        size.height * 0.6335179,
        size.width * 0.4999786,
        size.height * 0.6335179);
    path_2.cubicTo(
        size.width * 0.5147536,
        size.height * 0.6335179,
        size.width * 0.5267286,
        size.height * 0.6454964,
        size.width * 0.5267286,
        size.height * 0.6602714);
    path_2.cubicTo(
        size.width * 0.5267286,
        size.height * 0.6750429,
        size.width * 0.5147536,
        size.height * 0.6870214,
        size.width * 0.4999786,
        size.height * 0.6870214);
    path_2.close();
    path_2.moveTo(size.width * 0.4127964, size.height * 0.5984607);
    path_2.cubicTo(
        size.width * 0.4038821,
        size.height * 0.5984607,
        size.width * 0.3966571,
        size.height * 0.5912357,
        size.width * 0.3966571,
        size.height * 0.5823214);
    path_2.lineTo(size.width * 0.3966571, size.height * 0.5039000);
    path_2.cubicTo(
        size.width * 0.3966571,
        size.height * 0.4949857,
        size.width * 0.4038821,
        size.height * 0.4877607,
        size.width * 0.4127964,
        size.height * 0.4877607);
    path_2.lineTo(size.width * 0.5871607, size.height * 0.4877607);
    path_2.cubicTo(
        size.width * 0.5960714,
        size.height * 0.4877607,
        size.width * 0.6033000,
        size.height * 0.4949857,
        size.width * 0.6033000,
        size.height * 0.5039000);
    path_2.lineTo(size.width * 0.6033000, size.height * 0.5823214);
    path_2.cubicTo(
        size.width * 0.6033000,
        size.height * 0.5912357,
        size.width * 0.5960714,
        size.height * 0.5984607,
        size.width * 0.5871607,
        size.height * 0.5984607);
    path_2.lineTo(size.width * 0.4127964, size.height * 0.5984607);
    path_2.close();
    path_2.moveTo(size.width * 0.6316857, size.height * 0.7676143);
    path_2.cubicTo(
        size.width * 0.6015286,
        size.height * 0.7676143,
        size.width * 0.5770857,
        size.height * 0.7431714,
        size.width * 0.5770857,
        size.height * 0.7130179);
    path_2.cubicTo(
        size.width * 0.5770857,
        size.height * 0.6828643,
        size.width * 0.6015286,
        size.height * 0.6584214,
        size.width * 0.6316857,
        size.height * 0.6584214);
    path_2.cubicTo(
        size.width * 0.6618357,
        size.height * 0.6584214,
        size.width * 0.6862786,
        size.height * 0.6828643,
        size.width * 0.6862786,
        size.height * 0.7130179);
    path_2.cubicTo(
        size.width * 0.6862786,
        size.height * 0.7431714,
        size.width * 0.6618357,
        size.height * 0.7676143,
        size.width * 0.6316857,
        size.height * 0.7676143);
    path_2.close();
    path_2.moveTo(size.width * 0.6881714, size.height * 0.6123000);
    path_2.cubicTo(
        size.width * 0.6881714,
        size.height * 0.5975250,
        size.width * 0.7001464,
        size.height * 0.5855464,
        size.width * 0.7149214,
        size.height * 0.5855464);
    path_2.cubicTo(
        size.width * 0.7296964,
        size.height * 0.5855464,
        size.width * 0.7416750,
        size.height * 0.5975250,
        size.width * 0.7416750,
        size.height * 0.6123000);
    path_2.cubicTo(
        size.width * 0.7416750,
        size.height * 0.6270750,
        size.width * 0.7296964,
        size.height * 0.6390536,
        size.width * 0.7149214,
        size.height * 0.6390536);
    path_2.cubicTo(
        size.width * 0.7001464,
        size.height * 0.6390536,
        size.width * 0.6881714,
        size.height * 0.6270714,
        size.width * 0.6881714,
        size.height * 0.6123000);
    path_2.close();
    path_2.moveTo(size.width * 0.7573571, size.height * 0.6814857);
    path_2.cubicTo(
        size.width * 0.7425821,
        size.height * 0.6814857,
        size.width * 0.7306071,
        size.height * 0.6695071,
        size.width * 0.7306071,
        size.height * 0.6547321);
    path_2.cubicTo(
        size.width * 0.7306071,
        size.height * 0.6399571,
        size.width * 0.7425821,
        size.height * 0.6279821,
        size.width * 0.7573571,
        size.height * 0.6279821);
    path_2.cubicTo(
        size.width * 0.7721321,
        size.height * 0.6279821,
        size.width * 0.7841107,
        size.height * 0.6399607,
        size.width * 0.7841107,
        size.height * 0.6547321);
    path_2.cubicTo(
        size.width * 0.7841107,
        size.height * 0.6695071,
        size.width * 0.7721321,
        size.height * 0.6814857,
        size.width * 0.7573571,
        size.height * 0.6814857);
    path_2.close();
    path_2.moveTo(size.width * 0.7997929, size.height * 0.6390500);
    path_2.cubicTo(
        size.width * 0.7850179,
        size.height * 0.6390500,
        size.width * 0.7730429,
        size.height * 0.6270714,
        size.width * 0.7730429,
        size.height * 0.6122964);
    path_2.cubicTo(
        size.width * 0.7730429,
        size.height * 0.5975214,
        size.width * 0.7850179,
        size.height * 0.5855464,
        size.width * 0.7997929,
        size.height * 0.5855464);
    path_2.cubicTo(
        size.width * 0.8145679,
        size.height * 0.5855464,
        size.width * 0.8265464,
        size.height * 0.5975250,
        size.width * 0.8265464,
        size.height * 0.6122964);
    path_2.cubicTo(
        size.width * 0.8265464,
        size.height * 0.6270714,
        size.width * 0.8145679,
        size.height * 0.6390500,
        size.width * 0.7997929,
        size.height * 0.6390500);
    path_2.close();

    Paint paint_2_fill = Paint()..style = PaintingStyle.fill;
    paint_2_fill.color = Colors.white;
    canvas.drawPath(path_2, paint_2_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}

import 'package:flutter/material.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/text_styles.dart';
import 'package:time_app_components/utils/testStyles.dart';

class TabSearch extends StatefulWidget {
  const TabSearch({Key? key}) : super(key: key);

  @override
  _TabSearchState createState() => _TabSearchState();
}

class _TabSearchState extends State<TabSearch>
    with SingleTickerProviderStateMixin {
  late final TabController tabController =
      TabController(length: 2, vsync: this);

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, top: 20, bottom: 20, right: 10),
      child: SizedBox(
        height: 50,
        child: Row(
          children: [
            Expanded(
              child: Material(
                color: Color(0xffF8F8F8),
                borderRadius: BorderRadius.circular(5),
                child: SizedBox(
                  height: 50,
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: TabBar(
                      controller: tabController,
                      labelColor: ColorData.clientsButtonColorPressed,
                      labelStyle: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        height: 19 / 16,
                      ),
                      unselectedLabelColor: ColorData.allMainBlack,
                      unselectedLabelStyle: const TextStyle(
                        fontSize: 16,
                        height: 19 / 16,
                        fontWeight: FontWeight.w400,
                      ),
                      indicator: BoxDecoration(
                        color: Colors.white,
                        borderRadius: const BorderRadius.all(
                          Radius.circular(4),
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: Color(0x1a000000),
                            spreadRadius: 0,
                            blurRadius: 5,
                            offset: const Offset(1, 1),
                          ),
                        ],
                      ),
                      tabs: [
                        SizedBox(
                          height: 40,
                          child: Center(child: Text('Списком')),
                        ),
                        SizedBox(
                          height: 40,
                          child: Center(child: Text('На карте')),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            InkWell(
              borderRadius: BorderRadius.circular(6),
              onTap: () {},
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Text(
                  'Фильтры',
                  style: TextStyle(
                    color: ColorData.clientsButtonColorPressed,
                    fontSize: P3TextStyle.fontSize,
                    height: P3TextStyle.height,
                    fontWeight: P3TextStyle.fontWeight,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

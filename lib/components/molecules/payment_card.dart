import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:li/components/atoms/fade_animation.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/const.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class PaymentCard extends StatelessWidget {
  final String? title;
  final String? label;
  final VoidCallback? onTap;
  final VoidCallback? deleteTap;
  final bool choosen;

  const PaymentCard({
    this.title,
    this.label,
    this.choosen = false,
    this.onTap,
    this.deleteTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(16),
            boxShadow: bigBlur,
            border: choosen
                ? Border.all(
                    color: buttonsDefault,
                    width: 1,
                  )
                : null,
          ),
          padding: EdgeInsets.all(16),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (title != null)
                      Text(
                        title!,
                        style: TextStyle(
                          color: ColorData.allMainBlack,
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          height: 21 / 18,
                        ),
                      ),
                    const SizedBox(height: 4),
                    if (label != null)
                      Text(
                        label!,
                        style: TextStyle(
                          fontWeight: P4TextStyle.fontWeight,
                          fontSize: P4TextStyle.fontSize,
                          height: P4TextStyle.height,
                          color: ColorData.allMainActivegray,
                        ),
                      ),
                    const SizedBox(height: 4),
                    if (deleteTap != null)
                      GestureDetector(
                        onTap: deleteTap,
                        child: Text(
                          'Удалить',
                          style: TextStyle(
                            fontWeight: P4TextStyle.fontWeight,
                            fontSize: P4TextStyle.fontSize,
                            height: P4TextStyle.height,
                            color: ColorData.allButtonsError,
                          ),
                        ),
                      )
                    else
                      const SizedBox(height: 16),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 16),
                height: 30,
                width: 30,
                padding: EdgeInsets.all(3.5),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xffF0F0F0),
                ),
                child: FadeAnimation(
                  open: choosen,
                  child: Container(
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: buttonsDefault,
                    ),
                    alignment: Alignment.center,
                    child: SvgPicture.asset(
                      'assets/svg/icon/check.svg',
                      height: 20,
                      width: 20,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

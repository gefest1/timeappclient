part of 'getdebt_bloc.dart';

abstract class GetdebtEvent extends Equatable {
  const GetdebtEvent();

  @override
  List<Object> get props => [];
}

class GetDebtOfClientEvent extends GetdebtEvent {}

class GetCurrentDebtOfUser extends GetdebtEvent {
  const GetCurrentDebtOfUser();
}

class CloseDebtEvent extends GetdebtEvent {
  final Completer<void> completer;
  final String creditCardId;

  const CloseDebtEvent({
    required this.completer,
    required this.creditCardId,
  });
}

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:li/logic/blocs/institution/InstitutionBloc.dart';
import 'package:li/logic/blocs/institution/InstitutionEvent.dart';

class TestPage extends StatelessWidget {
  const TestPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Image.network(
        "https://timeapp.fra1.digitaloceanspaces.com/1634594645760qgymc_XL",
        fit: BoxFit.cover,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final _xFile = await ImagePicker().pickImage(
            source: ImageSource.gallery,
          );
          // log('MAGIC');
          if (_xFile == null) return;

          final _file = File(_xFile.path);

          final institutionBloc = BlocProvider.of<InstitutionBloc>(
            context,
            listen: false,
          );

          institutionBloc.add(EditDataCertificatgeInstitutionEvent(_file));
        },
      ),
    );
  }
}

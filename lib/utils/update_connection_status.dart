import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:li/logic/navigation.dart';
import 'package:li/main.dart';
import 'package:li/utils/color.dart';

// ConnectivityResult? prevValue;
// Future<void> updateConnectionStatus(ConnectivityResult result) async {
//   switch (result) {
//     case ConnectivityResult.none:
//       if (prevValue == result) break;
//       internetConnection = false;
//       buildAlertDialog();

//       break;
//     default:
//       if (prevValue == result) break;
//       internetConnection = true;

//       break;
//   }
//   prevValue = result;
// }

void buildAlertDialog() {
  showDialog(
    barrierDismissible: false,
    context: navigatorKey.currentContext!,
    builder: (BuildContext context) {
      return AlertDialog(
        contentPadding: EdgeInsets.all(15),
        insetPadding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(15),
          ),
        ),
        title: Column(
          children: [
            SvgPicture.asset('assets/svg/icon/NoNetworkIcon.svg'),
            SizedBox(height: 10.5),
            Text(
              "Пожалуйста, проверьте соединение с интернетом",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 18.0,
                height: 21 / 18,
                fontWeight: FontWeight.w600,
              ),
            ),
          ],
        ),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              "Не можем открыть страницу без интернета. Пожалуйста, попробуйте еще раз — если не получится, обратитесь в нашу службу поддержки ",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16.0,
                color: Color(0xff979797),
                height: 19 / 16,
              ),
            )
          ],
        ),
        actions: <Widget>[
          ElevatedButton(
            style: ButtonStyle(
              fixedSize: MaterialStateProperty.all(Size(double.infinity, 60)),
              shape: MaterialStateProperty.all(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
              ),
              backgroundColor: MaterialStateProperty.all(
                ColorData.clientsButtonColorDefault,
              ),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
              ),
              width: double.infinity,
              child: Text(
                'Понятно',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 18,
                  height: 21 / 18,
                  color: ColorData.grayScaleOffWhite,
                ),
              ),
            ),
          ),
        ],
      );
    },
  );
}

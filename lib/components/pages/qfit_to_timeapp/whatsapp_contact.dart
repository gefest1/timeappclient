import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/const.dart';
import 'package:li/utils/color.dart';
import 'package:url_launcher/url_launcher.dart';

class WhatsAppContact extends StatelessWidget {
  final String label;
  const WhatsAppContact({required this.label, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: GestureDetector(
        onTap: () async {
          final _tt =
              "https://wa.me/$label?text=%D0%97%D0%B4%D1%80%D0%B0%D0%B2%D1%81%D1%82%D0%B2%D1%83%D0%B9%D1%82%D0%B5%21+%D0%9F%D0%B8%D1%88%D1%83+%D0%B8%D0%B7+%D0%BF%D1%80%D0%B8%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D1%8F+TimeApp.";
          log('message');
          // if (Platform.isIOS) launchUniversalLinkIos(_tt);
          launch(_tt);
        },
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(16)),
            boxShadow: bigBlur,
          ),
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              RepaintBoundary(
                child: SvgPicture.asset('assets/svg/icon/whatsApp.svg'),
              ),
              Text(
                'Whatsapp',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  color: ColorData.allMainBlack,
                ),
              ),
              const SizedBox(height: 2),
              Text(
                '$label',
                style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 16,
                  color: ColorData.allMainBlack,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

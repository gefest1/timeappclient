import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:li/data/models/client_model.dart';
import 'package:li/data/models/photo_url.dart';

class CommentModel extends Equatable {
  final Client? client;
  final String? dateAdded;
  final int? rating;
  final String text;
  final String? id;
  // final PhotoUrl? photoURL;

  CommentModel(
      {required this.client,
      required this.dateAdded,
      required this.rating,
      required this.text,
      this.id
      // this.photoURL,
      });

  CommentModel copyWith({
    Client? client,
    String? dateAdded,
    int? rating,
    String? text,
    PhotoUrl? photoURL,
    String? id,
  }) {
    return CommentModel(
      client: client ?? this.client,
      dateAdded: dateAdded ?? this.dateAdded,
      rating: rating ?? this.rating,
      text: text ?? this.text,
      id: id ?? this.id,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'client': client!.toMap(),
      'dateAdded': dateAdded,
      'rating': rating,
      'text': text,
      '_id': id,
    };
  }

  factory CommentModel.fromMap(Map<String, dynamic> map) {
    return CommentModel(
      id: map["_id"],
      client: Client.fromMap(map['client']),
      dateAdded: map['dateAdded'] != null ? map['dateAdded'] : null,
      rating: map['rating'] != null ? map['rating'] : null,
      text: map['text'],
      // photoURL: PhotoUrl.fromMap(map['photoURL']),
    );
  }

  String toJson() => json.encode(toMap());

  factory CommentModel.fromJson(String source) =>
      CommentModel.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object?> get props => [id, client!, dateAdded!, rating!, text];
}

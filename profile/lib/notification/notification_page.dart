import 'package:flutter/material.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';

class NotificationPage extends StatefulWidget {
  const NotificationPage({Key? key}) : super(key: key);

  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  bool value = true;
  bool valuePhone = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        leading: InkWell(
          onTap: () => Navigator.pop(context),
          borderRadius: const BorderRadius.all(
            Radius.circular(40),
          ),
          child: const Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
        ),
        title: const Text(
          'Уведомления',
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500,
            color: Color(0xff424242),
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text('Из приложения'),
                Switch.adaptive(
                    activeColor: const Color(0xff00AB28),
                    value: value,
                    onChanged: (change) {
                      setState(() {
                        value = change;
                      });
                    })
              ],
            ),
            const Divider(
              height: 0,
              thickness: 0.5,
              color: ColorData.allMainGray,
            ),
            ListTile(
              contentPadding: const EdgeInsets.all(0),
              title: const Text(
                'По телефону',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              ),
              subtitle: const Text(
                '+7 708 382 24 61',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w400,
                ),
              ),
              leading: const Icon(Icons.call_outlined),
              trailing: Switch.adaptive(
                  activeColor: const Color(0xff00AB28),
                  value: valuePhone,
                  onChanged: (change) {
                    setState(() {
                      valuePhone = change;
                    });
                  }),
            ),
            const Divider(
              height: 0,
              thickness: 0.5,
              color: ColorData.allMainGray,
            ),
          ],
        ),
      ),
    );
  }
}

import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:li/components/atoms/leading_icon.dart';
import 'package:li/components/pages/qfit_to_timeapp/qfit_finish_bottom.dart';
import 'package:li/components/pages/qfit_to_timeapp/qr_scan_page.dart';
import 'package:li/data/models/session_minute.dart';
import 'package:li/logic/blocs/qfit_bloc/session/session_minute_bloc.dart';
import 'package:li/logic/navigation.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/text_styles.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'dart:math' as math;

import 'package:time_app_components/utils/testStyles.dart';

class FinishMinutePage extends StatefulWidget {
  final SessionMinute sessionMinute;

  const FinishMinutePage({
    required this.sessionMinute,
    Key? key,
  }) : super(key: key);

  @override
  _FinishMinutePageState createState() => _FinishMinutePageState();
}

class _FinishMinutePageState extends State<FinishMinutePage> {
  StreamSubscription<Barcode>? subscription;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  bool finish = false;
  late QRViewController _controller;

  @override
  void dispose() {
    subscription?.cancel();
    _controller.dispose();
    super.dispose();
  }

  Future<void> endService() async {
    finish = true;
    _controller.pauseCamera();
    if (!mounted) return;
    final _mediaQuery = MediaQuery.of(context);
    await showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      useRootNavigator: true,
      isScrollControlled: true,
      builder: (_ctx) => WillPopScope(
        onWillPop: () async {
          return !blockBackRoutes;
        },
        child: SizedBox(
          height: _mediaQuery.size.height - _mediaQuery.padding.top - 20,
          child: QFitFinishBottom(
            session: widget.sessionMinute,
          ),
        ),
      ),
    );
    if (!mounted) return;
    _controller.resumeCamera();
    finish = false;
  }

  // Future<void> endByService() async {}

  void _listen(Barcode event) async {
    if (finish) return;
    if (event.code == null) return;
    if (event.code!.contains('https://timeapp.kz/minutePay/')) {
      final _uri = Uri.dataFromString(event.code ?? '');
      if (_uri.queryParameters['serviceId'] ==
          widget.sessionMinute.service?.id) {
        await endService();
        return;
      }
      if (_uri.queryParameters['institutionId'] ==
          widget.sessionMinute.institutionId) {
        await endService();
        return;
      }
    }
  }

  void _onCreated(QRViewController controller) {
    _controller = controller;
    subscription = controller.scannedDataStream.listen(_listen);
  }

  @override
  Widget build(BuildContext context) {
    final _mediaQuery = MediaQuery.of(context);
    final _cubit =
        math.min(_mediaQuery.size.height, _mediaQuery.size.width) * 0.64;

    return Material(
      color: Colors.white,
      child: BlocBuilder<SessionMinuteBloc, SessionState>(
        builder: (context, state) {
          return Stack(
            children: [
              QRView(
                key: qrKey,
                onQRViewCreated: _onCreated,
                cameraFacing:
                    kReleaseMode ? CameraFacing.back : CameraFacing.back,
              ),
              Positioned(
                top: 0,
                left: 0,
                right: 0,
                child: CustomAppBar(
                  title: Text(
                    'Завершение сессии',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 18,
                      height: 21 / 18,
                      color: ColorData.allMainBlack,
                    ),
                  ),
                  leading: const LeadingIcon(),
                ),
              ),
              Positioned(
                top: MediaQuery.of(context).padding.top + 56,
                bottom: 0,
                right: 0,
                left: 0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                      decoration: BoxDecoration(
                        color: Color(0xb3000000),
                        borderRadius: BorderRadius.circular(6),
                      ),
                      child: Text(
                        'Наведите камеру на QR-код',
                        style: TextStyle(
                          fontSize: P3TextStyle.fontSize,
                          fontWeight: P3TextStyle.fontWeight,
                          height: P3TextStyle.height,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    const SizedBox(height: 20),
                    CustomPaint(
                      child: SizedBox(
                        height: _cubit,
                        width: _cubit,
                      ),
                      painter: ScanAreaPainter(),
                    ),
                    const SizedBox(height: 100),
                  ],
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}

import 'dart:convert';

import 'package:equatable/equatable.dart';

class ReviewModel extends Equatable {
  final String? institutionId;
  final String? text;
  final num? rating;

  const ReviewModel({
    this.institutionId,
    this.rating,
    this.text,
  });

  ReviewModel copyWith({
    String? institutionId,
    String? text,
    num? rating,
  }) {
    return ReviewModel(
      institutionId: institutionId ?? this.institutionId,
      text: text ?? this.text,
      rating: rating ?? this.rating,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'institutionId': institutionId,
      'text': text,
      'rating': rating,
    };
  }

  static ReviewModel? fromMap(Map<String, dynamic>? map) {
    if (map == null) return null;
    return ReviewModel(
      institutionId: map['institutionId'],
      text: map['text'],
      rating: map['rating'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ReviewModel.fromJson(String source) =>
      ReviewModel.fromMap(json.decode(source))!;

  @override
  bool get stringify => true;

  @override
  List<Object?> get props => [institutionId, text, rating];
}

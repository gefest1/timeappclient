import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/molecules/chat_text_field.dart';
import 'package:li/const.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class InChatPage extends StatefulWidget {
  const InChatPage({Key? key}) : super(key: key);

  @override
  _InChatPageState createState() => _InChatPageState();
}

class _InChatPageState extends State<InChatPage> {
  List<String> texts = [
    '1Добрый день! Я подойти не смогу, но могу отправить деньги на каспи за фотку эскиза, по рукам? Хочу на голове что-то типа черной дыры набить. Другу проспорил :с',
    'Привет! Думаю минут через 15 начну, к двум часам закончу. Может и быстрее успею',
    'Окей, буду ждать. Если будут вопросы, обращайся)',
    'Добрый день! Я подойти не смогу, но могу отправить деньги на каспи за фотку эскиза, по рукам? Хочу на голове что-то типа черной дыры набить. Другу проспорил :с',
    'Привет! Думаю минут через 15 начну, к двум часам закончу. Может и быстрее успею',
    'Окей, буду ждать. Если будут вопросы, обращайся)',
    'Добрый день! Я подойти не смогу, но могу отправить деньги на каспи за фотку эскиза, по рукам? Хочу на голове что-то типа черной дыры набить. Другу проспорил :с',
    'Привет! Думаю минут через 15 начну, к двум часам закончу. Может и быстрее успею',
    'Окей, буду ждать. Если будут вопросы, обращайся)',
    'Добрый день! Я подойти не смогу, но могу отправить деньги на каспи за фотку эскиза, по рукам? Хочу на голове что-то типа черной дыры набить. Другу проспорил :с',
    'Привет! Думаю минут через 15 начну, к двум часам закончу. Может и быстрее успею',
    'Окей, буду ждать. Если будут вопросы, обращайся)',
    'Добрый день! Я подойти не смогу, но могу отправить деньги на каспи за фотку эскиза, по рукам? Хочу на голове что-то типа черной дыры набить. Другу проспорил :с',
    'Привет! Думаю минут через 15 начну, к двум часам закончу. Может и быстрее успею',
    'Окей, буду ждать. Если будут вопросы, обращайся)',
  ];
  List<bool> isMe = [
    true,
    false,
    true,
    true,
    false,
    true,
    true,
    false,
    true,
    true,
    false,
    true,
    true,
    false,
    true,
  ];
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final _f = FocusScope.of(context);

        if (_f.hasFocus || _f.hasPrimaryFocus) {
          _f.unfocus();
        }
      },
      child: Material(
        color: Colors.white,
        child: Stack(
          children: [
            ListView.separated(
              padding: EdgeInsets.only(
                top: 20 + 56 + MediaQuery.of(context).padding.top,
                left: 20,
                right: 20,
                bottom: MediaQuery.of(context).padding.bottom + 72 + 20,
              ),
              itemCount: texts.length,
              reverse: true,
              separatorBuilder: (context, index) => const SizedBox(height: 6),
              itemBuilder: (context, index) => Align(
                alignment:
                    isMe[index] ? Alignment.centerRight : Alignment.centerLeft,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: isMe[index] ? Color(0xFF3ADE00) : Color(0xFFF9F9F9),
                  ),
                  width: MediaQuery.of(context).size.width * 0.6,
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    children: [
                      Text(
                        texts[index],
                        style: TextStyle(
                          fontSize: P3TextStyle.fontSize,
                          fontWeight: P3TextStyle.fontWeight,
                          height: P3TextStyle.height,
                          color: isMe[index] ? Colors.white : Colors.black,
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomRight,
                        child: Text(
                          '21:09',
                          style: TextStyle(
                            fontSize: 12,
                            color:
                                isMe[index] ? Colors.white : Color(0xFF979797),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              bottom: MediaQuery.of(context).viewInsets.bottom,
              right: 0,
              left: 0,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: bigBlur,
                  borderRadius: BorderRadius.all(Radius.circular(16)),
                ),
                child: Padding(
                  padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).padding.bottom,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: 16,
                    ),
                    child: Row(
                      children: [
                        SvgPicture.asset('assets/svg/icon/Attach.svg'),
                        SizedBox(
                          width: 14,
                        ),
                        Flexible(
                          child: ChatTextField(
                            hintText: 'Сообщение...',
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              left: 0,
              right: 0,
              top: 0,
              child: CustomAppBar(
                leading: IconButton(
                  icon: SvgPicture.asset('assets/svg/icon/backArrow.svg'),
                  onPressed: () => Navigator.pop(context),
                ),
                title: Column(
                  children: [
                    Text(
                      'Иван',
                      style: TextStyle(
                        fontSize: 18,
                        color: ColorData.allMainBlack,
                        fontWeight: FontWeight.w500,
                        height: 21 / 18,
                      ),
                    ),
                    Text(
                      'Тату-студия Zabey',
                      style: TextStyle(
                        fontSize: P4TextStyle.fontSize,
                        fontWeight: P4TextStyle.fontWeight,
                        height: P4TextStyle.height,
                        color: Color(0xFF979797),
                      ),
                    ),
                  ],
                ),
                actions: [
                  Padding(
                    padding: const EdgeInsets.only(right: 17.0),
                    child: CircleAvatar(
                      minRadius: 16.5,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

class BottomAuthNav extends StatelessWidget {
  final VoidCallback onTap;

  const BottomAuthNav({
    required this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: MediaQuery.of(context).viewInsets.bottom,
      left: 0,
      right: 0,
      height: MediaQuery.of(context).padding.bottom + 64 + 28,
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Color(0xff323232).withOpacity(0.1),
              blurRadius: 30,
              offset: Offset(0, -10),
            )
          ],
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(16),
          ),
        ),
        padding: EdgeInsets.only(top: 8, left: 20, right: 20),
        child: SizedBox(
          height: 60,
          width: double.infinity,
          child: ElevatedButton(
            onPressed: onTap,
            child: Text(
              'Продолжить',
              style: const TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.w400,
              ),
            ),
            style: ButtonStyle(
              shape: MaterialStateProperty.all(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(16),
                ),
              ),
              elevation: MaterialStateProperty.all(0.0),
            ),
          ),
        ),
      ),
    );
  }
}

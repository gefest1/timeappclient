import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/data/models/review/comment_model.dart';
import 'package:li/data/models/meta_rating_model.dart';
import 'package:li/network/graphql_provider.dart';

class _GetCommentsResult {
  final List<CommentModel> getCommentsOfAInstitution;
  final MetaRatingModel getRatingMetaOfInstitution;

  const _GetCommentsResult({
    required this.getCommentsOfAInstitution,
    required this.getRatingMetaOfInstitution,
  });
}

mixin GetCommentsAPI {
  static Future<_GetCommentsResult> getCommentsAPI({
    required GraphqlMinuteProvider graphqlMinuteProvider,
    required String institutionId,
    required int page,
  }) async {
    final _queryResult = await graphqlMinuteProvider.client.query(
      QueryOptions(
        document: gql('''
          query(
            \$institutionId: String!, 
            \$page: Int!
          ){
          getCommentsOfAInstitution(
            institutionId: \$institutionId,
            page:\$page,
          ){
            _id
            client{
              _id,
              fullName,
              email,
              photoURL{
                M,
                XL,
                thumbnail,
              },
              sex,
              phoneNumber,
            }
            dateAdded,
            rating,
            text,
          }
          getRatingMetaOfInstitution(institutionId:\$institutionId){
              groupedRatings{
                fiveStars,
                fourStars,
                threeStars,
                oneStars,
                twoStars
              },
              totalSum
            }
      }'''),
        variables: {
          "institutionId": institutionId,
          "page": page,
        },
      ),
    );
    if (_queryResult.hasException) throw _queryResult.exception!;
    return _GetCommentsResult(
      getCommentsOfAInstitution: List<CommentModel>.from(
        _queryResult.data!['getCommentsOfAInstitution']?.map(
          (x) => CommentModel.fromMap(x),
        ),
      ),
      getRatingMetaOfInstitution: MetaRatingModel.fromMap(
        _queryResult.data!['getRatingMetaOfInstitution'],
      ),
    );
  }
}

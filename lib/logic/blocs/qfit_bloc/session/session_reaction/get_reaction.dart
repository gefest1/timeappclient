part of '../session_minute_bloc.dart';

extension GetReaction on SessionMinuteBloc {
  void getSessions(
    GetActiveSessionEvent event,
    Emitter<SessionState> emit,
  ) async {
    final activeSessions = await GetSessionApi.getActiveSessions(
      client: graphqlMinuteProvider.client,
    );

    emit(StartedSessionState(sessions: activeSessions ?? []));
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/const.dart';
import 'package:li/data/models/client_model.dart';
import 'package:li/main.dart';
import 'package:profile/personal_page/personal_page.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class PersonalTile extends StatelessWidget {
  final Client client;
  const PersonalTile({required this.client, Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final _style = ButtonStyle(
      elevation: MaterialStateProperty.all(0.0),
      padding: MaterialStateProperty.all(const EdgeInsets.all(0)),
      shape: MaterialStateProperty.all(
        const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15)),
        ),
      ),
      backgroundColor: MaterialStateProperty.all(Colors.white),
      overlayColor: MaterialStateProperty.all(Colors.transparent),
    );

    return Container(
      decoration: const BoxDecoration(boxShadow: bigBlur),
      margin: const EdgeInsets.all(20),
      child: ElevatedButton(
        style: _style,
        onPressed: () {
          routerDelegate.push(PersonalPage(client: client));
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
          child: Row(
            children: [
              Container(
                height: 82,
                width: 70,
                alignment: Alignment.center,
                child: client.photoURL?.xl == null
                    ? FittedBox(
                        child: Text(
                          '${client.fullName}'.substring(0, 1),
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            fontSize: 46,
                          ),
                        ),
                      )
                    : null,
                decoration: BoxDecoration(
                  image: client.photoURL?.xl == null
                      ? null
                      : DecorationImage(
                          image: NetworkImage(
                            client.photoURL!.xl!,
                          ),
                          fit: BoxFit.cover,
                        ),
                  borderRadius: BorderRadius.circular(10),
                  color: const Color(0xffFF8282),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: ConstrainedBox(
                  constraints: const BoxConstraints(
                    minHeight: 50.0,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Flexible(
                            child: Text(
                              client.fullName!,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(
                                fontSize: H2TextStyle.fontSize,
                                fontWeight: H2TextStyle.fontWeight,
                                height: H2TextStyle.height,
                                color: ColorData.allMainBlack,
                              ),
                              maxLines: 1,
                            ),
                          ),
                          const SizedBox(width: 5),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SvgPicture.asset(
                                  'assets/svg/icon/FilledStarSmall.svg'),
                              const Text(
                                "5.0",
                                style: TextStyle(
                                  fontSize: P4TextStyle.fontSize,
                                  fontWeight: P4TextStyle.fontWeight,
                                  color: ColorData.allMainGray,
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                      // const SizedBox(height: 4),
                      // Text(
                      //   (client.sex.toString() == 'Male')
                      //       ? 'Мужчина'
                      //       : 'Женщина',
                      //   style: const TextStyle(
                      //     fontSize: 16,
                      //     fontWeight: FontWeight.w400,
                      //     color: Color(0xFF434343),
                      //   ),
                      // ),
                      const SizedBox(height: 2),
                      Text(
                        "+" + client.phoneNumber.toString(),
                        style: const TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF434343),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              const Icon(
                Icons.arrow_forward_ios_rounded,
                color: Colors.grey,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

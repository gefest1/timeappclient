import 'package:flutter/material.dart';
import 'package:li/utils/color.dart';

class ChoosenDish extends StatelessWidget {
  const ChoosenDish({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Выбранные блюда',
          style: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.w600,
            color: ColorData.allMainBlack,
          ),
        ),
        SizedBox(height: 10),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: ColorData.allMessagesbackground,
          ),
          padding: EdgeInsets.all(15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Паста «Фетучини»',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: 5),
                  Text(
                    '1550 ₸ — 300 г. ',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Text(
                    'Итальянская кухня — второе',
                    style: TextStyle(
                        fontSize: 14, color: ColorData.allMainActivegray),
                  ),
                ],
              ),
              CircleAvatar(
                minRadius: 20,
                backgroundColor: ColorData.clientsIcons,
              ),
            ],
          ),
        ),
      ],
    );
  }
}

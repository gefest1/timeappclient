import 'package:flutter/material.dart';

class RotateAnimation extends StatefulWidget {
  final Widget child;

  const RotateAnimation({required this.child, Key? key}) : super(key: key);

  @override
  State<RotateAnimation> createState() => _RotateAnimationState();
}

class _RotateAnimationState extends State<RotateAnimation>
    with SingleTickerProviderStateMixin {
  late final AnimationController turns = AnimationController(
    vsync: this,
    lowerBound: 0,
    upperBound: 4,
    duration: const Duration(minutes: 2),
  );

  @override
  void initState() {
    super.initState();
    turns.repeat();
  }

  @override
  void dispose() {
    turns.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RotationTransition(
      turns: turns,
      child: widget.child,
    );
  }
}

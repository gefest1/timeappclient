import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/network/graphql_provider.dart';
import 'package:take_away/data/models/dish.dart';

part 'take_away_event.dart';
part 'take_away_state.dart';

class TakeAwayBloc extends Bloc<TakeAwayEvent, TakeAwayState> {
  final GraphqlProvider graphqlProvider;
  final GraphqlTakeAwayProvider graphqlTakeAwayProvider;

  TakeAwayBloc(
    this.graphqlProvider,
    this.graphqlTakeAwayProvider,
  ) : super(TakeAwayInitial()) {
    on<TakeAwayEvent>((event, emit) {});
  }

  Future<List<Dish>> takeData() async {
    final _queryRes = await graphqlTakeAwayProvider.client.query(
      QueryOptions(
        document: gql('''
          {
            getDishes {
              _id
          		canteenPrice
          		description
          		dishContainerId
              institutionId
          		institution {
                _id
                name
                specialist{ 
                	fullName
                  photoURL {
                    M
                    XL
                    thumbnail
                  }
                }
                location{
                  address
                  latitude
                  longitude
                }
              }
          		name
          		price
          		priceCurrency
          		type
            }
          }'''),
      ),
    );
    if (_queryRes.hasException) throw _queryRes.exception!;
    final _dishList = (_queryRes.data!['getDishes'] as List<dynamic>)
        .map((_) => Dish.fromMap(_))
        .toList();
    return _dishList;
  }
}

part of '../main_page_bloc.dart';

extension CheckFetchData on MainPageBloc {
  void checkFetchData(
    GetDataMainPage event,
    Emitter<MainPageState> emit,
  ) async {
    final queryResult = await graphqlProvider.client.query(
      QueryOptions(
        document: gql('''
              {
              	getSpecialOffer {
                  title {
              			ru
              		}
              		svgPicture 
              		slug
                }
                getCategoryData {
                  shortSvgPicture
                  slug
                  title {
                    ru
                  }
                }
              	getPreview {
                  slug
                  title{
                    ru
                  }
                  photoSvg
                  subTitle {
                    ru
                  }
                }  
              }'''),
        fetchPolicy: FetchPolicy.cacheAndNetwork,
      ),
    );
    if (queryResult.hasException) throw queryResult.exception!;
    final _data = queryResult.data!;
    final List<CategoryModel> getSpecOffer =
        (_data['getSpecialOffer'] as List<dynamic>?)
                ?.map((e) => CategoryModel.fromMap(e)!)
                .toList() ??
            [];
    final List<CategoryModel> getCategoryData =
        (_data['getCategoryData'] as List<dynamic>?)
                ?.map((e) => CategoryModel.fromMap(e)!)
                .toList() ??
            [];
    final List<MainSlugModel> getPreview =
        (_data['getPreview'] as List<dynamic>?)
                ?.map((e) => MainSlugModel.fromMap(e)!)
                .toList() ??
            [];
    emit(
      FetchedData(
        categoriesMain: getCategoryData,
        mainAppCategoriesModel: getPreview,
        specialCategoriesMain: getSpecOffer,
      ),
    );
  }
}

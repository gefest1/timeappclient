import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:li/data/models/sub_category_model.dart';
import 'package:li/utils/translate.dart';

class CategoryModel extends Equatable {
  final List<Translate>? services;
  final String? shortSvgPicture;
  final String? slug;
  final SubCategoryModel? subCategory;
  final String? svgPicture;
  final Translate? title;

  const CategoryModel({
    this.services,
    this.shortSvgPicture,
    this.slug,
    this.subCategory,
    this.svgPicture,
    this.title,
  });

  CategoryModel copyWith({
    List<Translate>? services,
    String? shortSvgPicture,
    String? slug,
    SubCategoryModel? subCategory,
    String? svgPicture,
    Translate? title,
  }) {
    return CategoryModel(
      services: services ?? this.services,
      shortSvgPicture: shortSvgPicture ?? this.shortSvgPicture,
      slug: slug ?? this.slug,
      subCategory: subCategory ?? this.subCategory,
      svgPicture: svgPicture ?? this.svgPicture,
      title: title ?? this.title,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'services': services?.map((x) => x.toMap()).toList(),
      'shortSvgPicture': shortSvgPicture,
      'slug': slug,
      'subCategory': subCategory?.toMap(),
      'svgPicture': svgPicture,
      'title': title?.toMap(),
    };
  }

  static CategoryModel? fromMap(Map<String, dynamic>? map) {
    if (map == null) return null;

    return CategoryModel(
      services: map['services'] == null
          ? null
          : List<Translate>.from(
              map['services']?.map((x) => Translate.fromMap(x)),
            ),
      shortSvgPicture: map['shortSvgPicture'],
      slug: map['slug'],
      subCategory: SubCategoryModel.fromMap(map['subCategory']),
      svgPicture: map['svgPicture'],
      title: Translate.fromMap(map['title']),
    );
  }

  String toJson() => json.encode(toMap());

  factory CategoryModel.fromJson(String source) =>
      CategoryModel.fromMap(json.decode(source))!;

  @override
  bool get stringify => true;

  @override
  List<Object?> get props {
    return [
      services,
      shortSvgPicture,
      slug,
      subCategory,
      svgPicture,
      title,
    ];
  }
}

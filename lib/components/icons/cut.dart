// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';

class CutCustomPainter extends CustomPainter {
  const CutCustomPainter({Listenable? repaint}) : super(repaint: repaint);
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint_0_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.1023296;
    paint_0_stroke.color = Colors.white;
    canvas.drawOval(
        Rect.fromCenter(
            center: Offset(size.width * 0.2500000, size.height * 0.2500175),
            width: size.width * 0.2500000,
            height: size.height * 0.2500000),
        paint_0_stroke);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.3396432, size.height * 0.3459125);
    path_1.lineTo(size.width * 0.8750036, size.height * 0.8749071);

    Paint paint_1_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.1023296;
    paint_1_stroke.color = Colors.white;
    paint_1_stroke.strokeCap = StrokeCap.round;
    paint_1_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_1, paint_1_stroke);

    Path path_2 = Path();
    path_2.moveTo(size.width * 0.3396432, size.height * 0.6521679);
    path_2.lineTo(size.width * 0.4805286, size.height * 0.5129607);

    Paint paint_2_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.1023296;
    paint_2_stroke.color = Colors.white;
    paint_2_stroke.strokeCap = StrokeCap.round;
    paint_2_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_2, paint_2_stroke);

    Path path_3 = Path();
    path_3.moveTo(size.width * 0.6185571, size.height * 0.3765786);
    path_3.lineTo(size.width * 0.8721500, size.height * 0.1260011);

    Paint paint_3_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.1023296;
    paint_3_stroke.color = Colors.white;
    paint_3_stroke.strokeCap = StrokeCap.round;
    paint_3_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_3, paint_3_stroke);

    Paint paint_4_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.1023296;
    paint_4_stroke.color = Colors.white;
    canvas.drawCircle(Offset(size.width * 0.2500000, size.height * 0.7500179),
        size.width * 0.1250000, paint_4_stroke);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}

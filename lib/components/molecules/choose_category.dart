import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ChooseCategory extends StatelessWidget {
  final String title;
  final String assetPath;
  final VoidCallback onTap;
  final bool choosen;

  const ChooseCategory({
    required this.title,
    required this.assetPath,
    required this.onTap,
    required this.choosen,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Ink(
      decoration: BoxDecoration(
        color: Color(0xffFAFAFA),
        border: Border.all(
          color: choosen ? const Color(0xff0F84F4) : const Color(0xffC9C9C9),
          width: choosen ? 2 : 0.5,
        ),
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            blurRadius: 40,
            offset: Offset(0, -1),
            color: Colors.black.withOpacity(0.05),
          ),
          BoxShadow(
            blurRadius: 40,
            offset: Offset(0, 20),
            color: Colors.black.withOpacity(0.05),
          ),
        ],
      ),
      child: InkWell(
        onTap: onTap,
        child: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                top: 64,
                bottom: 4,
                left: 4,
                right: 4,
              ),
              child: RepaintBoundary(
                child: SvgPicture.asset(
                  assetPath,
                  width: double.infinity,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 16,
                top: 16,
                right: 16,
              ),
              child: Text(
                title,
                style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 18,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

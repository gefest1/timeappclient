import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/main.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/text_styles.dart';
import 'package:new_version/new_version.dart';
import 'package:time_app_components/utils/testStyles.dart';
import 'package:url_launcher/url_launcher.dart';

void opnedUpdate() async {
  final newVersion = NewVersion(
    iOSId: 'kz.time.app.li',
    androidId: 'kz.time.app',
  );
  final status = await newVersion.getVersionStatus();
  if (status == null || !status.canUpdate) return;
  navigatorKey.currentState!.overlay!.insert(
    OverlayEntry(
      builder: (_) => const UpdateAvailable(),
    ),
  );
}

class UpdateAvailable extends StatelessWidget {
  const UpdateAvailable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(color: Colors.black45),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: Center(
          child: Material(
            color: Colors.white,
            borderRadius: BorderRadius.circular(16),
            child: Padding(
              padding: const EdgeInsets.all(12),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SvgPicture.asset('assets/update.svg'),
                  const SizedBox(height: 40),
                  Text(
                    'Обновите приложение',
                    style: TextStyle(
                      fontSize: H2TextStyle.fontSize,
                      fontWeight: H2TextStyle.fontWeight,
                      height: H2TextStyle.height,
                    ),
                  ),
                  const SizedBox(height: 20),
                  Text(
                    'Мы обновили дизайн и добавили новые функции, вам понравится!',
                    style: TextStyle(
                      fontSize: P2TextStyle.fontSize,
                      fontWeight: P2TextStyle.fontWeight,
                      height: P2TextStyle.height,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 20),
                  SizedBox(
                    height: 60,
                    child: ElevatedButton(
                      onPressed: () {
                        final iosLink =
                            "https://apps.apple.com/us/app/timeapp-%D0%BC%D0%BE%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B5-%D0%B7%D0%B0%D0%BF%D0%B8%D1%81%D0%B8/id1514221318";
                        final androidLink =
                            "https://play.google.com/store/apps/details?id=kz.time.app";
                        if (Platform.isIOS) {
                          launch(iosLink);
                        }
                        if (Platform.isAndroid) {
                          launch(androidLink);
                        }
                      },
                      child: Text(
                        'Скачайте обновление',
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          color: ColorData.grayScaleOffWhite,
                        ),
                      ),
                      style: ButtonStyle(
                        padding: MaterialStateProperty.all(
                          EdgeInsets.symmetric(horizontal: 32),
                        ),
                        backgroundColor: MaterialStateProperty.all(
                          ColorData.clientsButtonColorDefault,
                        ),
                        shape: MaterialStateProperty.all(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(16),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

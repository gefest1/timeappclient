import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/molecules/divider_text_tab.dart';
import 'package:li/const.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/components/molecules/CustomTextField.dart';

class AddressChoose extends StatelessWidget {
  const AddressChoose({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FractionallySizedBox(
      heightFactor: 0.9,
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15),
                topRight: Radius.circular(15),
              ),
              boxShadow: bigBlur,
              color: Colors.white,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  height: 20,
                  width: 20,
                ),
                Text(
                  'Адрес доставки',
                  style: TextStyle(
                    fontSize: 18,
                    color: ColorData.allMainBlack,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                SvgPicture.asset(
                  'assets/svg/icon/close.svg',
                  color: ColorData.allMainBlack,
                ),
              ],
            ),
          ),
          SizedBox(
            height: 24,
          ),
          DividerTapTile(
            assetSvg: 'assets/svg/icon/location.svg',
            title: 'Новый адрес',
            onTap: () => showModalBottomSheet(
                isScrollControlled: true,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                backgroundColor: Colors.white,
                context: context,
                builder: (context) {
                  return FractionallySizedBox(
                    heightFactor: 0.9,
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.all(20),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(15),
                              topRight: Radius.circular(15),
                            ),
                            boxShadow: bigBlur,
                            color: Colors.white,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                height: 20,
                                width: 20,
                              ),
                              Text(
                                'Адрес доставки',
                                style: TextStyle(
                                  fontSize: 18,
                                  color: ColorData.allMainBlack,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              SvgPicture.asset(
                                'assets/svg/icon/close.svg',
                                color: ColorData.allMainBlack,
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: Column(
                            children: [
                              SizedBox(
                                height: 40,
                              ),
                              Row(
                                children: [
                                  Flexible(
                                    flex: 3,
                                    child: CustomTextField(
                                        labelColor: ColorData.allMainActivegray,
                                        label: 'Адрес доставки'),
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        color: Color(0xffFAFAFA),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 20.0),
                                        child: SvgPicture.asset(
                                          'assets/svg/icon/location.svg',
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                      child: CustomTextField(
                                    labelColor: ColorData.allMainActivegray,
                                    label: 'Подъезд',
                                  )),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                      child: CustomTextField(
                                    labelColor: ColorData.allMainActivegray,
                                    label: 'Домофон',
                                  )),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                      child: CustomTextField(
                                    labelColor: ColorData.allMainActivegray,
                                    label: 'Кв./Офис',
                                  )),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Expanded(
                                    child: CustomTextField(
                                      labelColor: ColorData.allMainActivegray,
                                      label: 'Этаж',
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              CustomTextField(
                                labelColor: ColorData.allMainActivegray,
                                label: 'Комментарий курьеру',
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                }),
          ),
          const Divider(
            height: 0,
            indent: 20,
            endIndent: 20,
            color: ColorData.allMainGray,
            thickness: 0.5,
          ),
          DividerTapTile(
            onTap: () {},
            assetSvg: 'assets/svg/icon/location.svg',
            title: 'ул. Афцинао 4 блок 2',
            subTitle: 'подъезд 2, квартира 39, 4 этаж',
          ),
          const Divider(
            height: 0,
            indent: 20,
            endIndent: 20,
            color: ColorData.allMainGray,
            thickness: 0.5,
          ),
          DividerTapTile(
            onTap: () {},
            assetSvg: 'assets/svg/icon/location.svg',
            title: 'ул. Шевченко 85',
            subTitle: 'подъезд 1, квартира 12, 5 этаж',
          ),
          const Divider(
            height: 0,
            indent: 20,
            endIndent: 20,
            color: ColorData.allMainGray,
            thickness: 0.5,
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:li/utils/color.dart';

class DialogUpdate extends StatelessWidget {
  const DialogUpdate({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding:
                const EdgeInsets.only(bottom: 36, left: 40, right: 40, top: 40),
            child: SvgPicture.asset(
              'assets/svg/update.svg',
            ),
          ),
          const Text(
            'Скоро будет обновление',
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.w700,
              color: ColorData.mainBlack,
            ),
          ),
          const SizedBox(height: 10),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 36),
            child: Text(
              'Это фича будет скоро будет доступно',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w400,
                color: ColorData.mainBlack,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }
}

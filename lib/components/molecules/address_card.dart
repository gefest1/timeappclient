import 'package:flutter/material.dart';
import 'package:li/const.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class AdressCard extends StatelessWidget {
  const AdressCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        boxShadow: bigBlur,
        color: Colors.white,
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 16.0,
          vertical: 20.0,
        ),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      'Адрес работы',
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w600,
                        color: ColorData.allMainBlack,
                      ),
                    ),
                    SizedBox(
                      height: 7.5,
                    ),
                    Text(
                      'Сейфуллина, 120',
                      style: TextStyle(
                        fontSize: P3TextStyle.fontSize,
                        fontWeight: P3TextStyle.fontWeight,
                        color: ColorData.allMainActivegray,
                      ),
                    ),
                  ],
                ),
                CircleAvatar(
                  minRadius: 20,
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              height: 160,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: Colors.grey,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

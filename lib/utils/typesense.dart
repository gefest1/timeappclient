import 'package:typesense/typesense.dart' as typesense;

final _config = typesense.Configuration(
  'hQOHqqQ2QboW1jCeLKJqbaEg2QZTlxLl',
  nodes: {
    typesense.Node(
      typesense.Protocol.http,
      '167.172.162.8',
      port: 8108,
    ),
  },
  connectionTimeout: Duration(seconds: 2),
);
final typesenseClient = typesense.Client(_config);

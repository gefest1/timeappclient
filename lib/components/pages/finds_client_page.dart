import 'package:flutter_svg/svg.dart';
import 'package:li/components/atoms/switch_work_button.dart';
import 'package:li/const.dart';
import 'package:flutter/material.dart';

class FindsClientsPage extends StatelessWidget {
  final VoidCallback nextPage;
  const FindsClientsPage({
    required this.nextPage,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 40,
        left: 20,
        right: 20,
        bottom: 20,
      ),
      child: Column(
        children: [
          Expanded(
            child: Column(
              children: [
                const SwitchWorkButton(),
                const SizedBox(
                  height: 24,
                ),
                const Text(
                  'Как находить клиентов',
                  style: TextStyle(
                    color: mainBlack,
                    fontSize: 38,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(height: 4),
                const Text(
                  'TimeApp Pro помогает находить клиентов, которые находятся недалеко от вас и готовы прийти в ближайшее время',
                  style: TextStyle(
                    fontSize: 18,
                    color: mainBlack,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
          ),
          Center(
            child: SizedBox(
              child: ElevatedButton(
                onPressed: this.nextPage,
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Color(0xff007AFF)),
                  minimumSize: MaterialStateProperty.all(
                    Size(180, 58),
                  ),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16),
                      side: BorderSide.none,
                    ),
                  ),
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 2),
                      child: Text(
                        'Далее',
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 18,
                        ),
                      ),
                    ),
                    const SizedBox(width: 10),
                    SvgPicture.asset('assets/svg/icon/forward.svg'),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';

class BackArrowCustomPainter extends CustomPainter {
  const BackArrowCustomPainter({Listenable? repaint}) : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.4181520, size.height * 0.1859912);
    path_0.cubicTo(
        size.width * 0.4045760,
        size.height * 0.1729632,
        size.width * 0.3830084,
        size.height * 0.1734080,
        size.width * 0.3699804,
        size.height * 0.1869848);
    path_0.lineTo(size.width * 0.1012976, size.height * 0.4669840);
    path_0.cubicTo(
        size.width * 0.08864800,
        size.height * 0.4801680,
        size.width * 0.08864800,
        size.height * 0.5009800,
        size.width * 0.1012976,
        size.height * 0.5141640);
    path_0.lineTo(size.width * 0.3699804, size.height * 0.7941640);
    path_0.cubicTo(
        size.width * 0.3830084,
        size.height * 0.8077400,
        size.width * 0.4045760,
        size.height * 0.8081840,
        size.width * 0.4181520,
        size.height * 0.7951560);
    path_0.cubicTo(
        size.width * 0.4317280,
        size.height * 0.7821280,
        size.width * 0.4321720,
        size.height * 0.7605600,
        size.width * 0.4191440,
        size.height * 0.7469840);
    path_0.lineTo(size.width * 0.1730984, size.height * 0.4905720);
    path_0.lineTo(size.width * 0.4191440, size.height * 0.2341628);
    path_0.cubicTo(
        size.width * 0.4321720,
        size.height * 0.2205860,
        size.width * 0.4317280,
        size.height * 0.1990188,
        size.width * 0.4181520,
        size.height * 0.1859912);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Color(0xff323232).withOpacity(1.0);
    canvas.drawPath(path_0, paint_0_fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.1365576, size.height * 0.4859120);
    path_1.cubicTo(
        size.width * 0.1365576,
        size.height * 0.5047280,
        size.width * 0.1518112,
        size.height * 0.5199800,
        size.width * 0.1706272,
        size.height * 0.5199800);
    path_1.lineTo(size.width * 0.8118080, size.height * 0.5199800);
    path_1.cubicTo(
        size.width * 0.8306240,
        size.height * 0.5199800,
        size.width * 0.8458800,
        size.height * 0.5047280,
        size.width * 0.8458800,
        size.height * 0.4859120);
    path_1.cubicTo(
        size.width * 0.8458800,
        size.height * 0.4670960,
        size.width * 0.8306240,
        size.height * 0.4518400,
        size.width * 0.8118080,
        size.height * 0.4518400);
    path_1.lineTo(size.width * 0.1706272, size.height * 0.4518400);
    path_1.cubicTo(
        size.width * 0.1518112,
        size.height * 0.4518400,
        size.width * 0.1365576,
        size.height * 0.4670960,
        size.width * 0.1365576,
        size.height * 0.4859120);
    path_1.close();

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = Color(0xff323232).withOpacity(1.0);
    canvas.drawPath(path_1, paint_1_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}

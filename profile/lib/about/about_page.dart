import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:profile/about_app_tile.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/utils/color.dart';

import 'package:li/components/atoms/leading_icon.dart';
import 'package:li/main.dart';

class AboutPage extends StatelessWidget {
  const AboutPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(
          title: const Text(
            'О приложении',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 18,
              height: 21 / 18,
              color: ColorData.allMainBlack,
            ),
          ),
          leading: const LeadingIcon(),
        ),
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 40,
              ),
              RepaintBoundary(
                child: SvgPicture.asset('assets/svg/profile/about_time.svg'),
              ),
              const SizedBox(
                height: 20,
              ),
              Flexible(
                child: Text(
                  'Версия ${packageInfo.version}',
                  style:
                      const TextStyle(color: Color(0xffB7B7B7), fontSize: 15),
                ),
              ),
              const SizedBox(height: 30),
              AboutAppTile(index: 0),
              AboutAppTile(index: 1)
            ],
          ),
        ));
  }
}

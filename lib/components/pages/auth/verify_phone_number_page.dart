import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/molecules/register_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:li/components/pages/auth/timer_wait.dart';
import 'package:li/const.dart';
import 'package:li/logic/blocs/register/RegisterBloc.dart';
import 'package:li/logic/blocs/register/RegisterEvent.dart';
import 'package:li/logic/blocs/register/RegisterState.dart';
import 'package:time_app_components/components/molecules/CustomTextField.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:time_app_components/utils/color.dart';

class VerifyPhoneNumberPage extends StatefulWidget {
  final String phoneNumber;

  const VerifyPhoneNumberPage({
    required this.phoneNumber,
    Key? key,
  }) : super(key: key);

  @override
  _VerifyPhoneNumberPageState createState() => _VerifyPhoneNumberPageState();
}

class _VerifyPhoneNumberPageState extends State<VerifyPhoneNumberPage> {
  late final TextEditingController codeText = TextEditingController()
    ..addListener(() {
      setState(() {});
    });
  CollectionReference session =
      FirebaseFirestore.instance.collection('Registered Clients');
  late Timer timer;
  late DateTime dateTime;

  @override
  void initState() {
    super.initState();
    dateTime = DateTime.now();
    timer = Timer(
      Duration(seconds: 60),
      () {
        if (!mounted) return;
        setState(() {});
      },
    );
  }

  @override
  void dispose() {
    timer.cancel();
    codeText.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final _focus = FocusScope.of(context);
        if (_focus.hasFocus || _focus.hasPrimaryFocus) {
          _focus.unfocus();
        }
      },
      child: Material(
        color: Colors.white,
        child: AnnotatedRegion(
          value: SystemUiOverlayStyle(statusBarBrightness: Brightness.light),
          child: BlocBuilder<RegisterBloc, RegisterState>(
            builder: (ctx, state) {
              if (state is CheckSMSLoadingState) {
                return Center(child: CircularProgressIndicator());
              }
              return Stack(
                children: [
                  CustomScrollView(
                    physics: ClampingScrollPhysics(),
                    slivers: [
                      SliverList(
                        delegate: SliverChildListDelegate([
                          const RegisterAppBar(
                            show: true,
                          ),
                          const SizedBox(height: 20),
                        ]),
                      ),
                      SliverPadding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        sliver: SliverList(
                          delegate: SliverChildListDelegate([
                            const Text(
                              'Введите код подтверждения',
                              style: TextStyle(
                                fontSize: 38,
                                color: Color(0xff323232),
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            const SizedBox(height: 40),
                            SizedBox(
                              height: 64,
                              child: CustomTextField(
                                label: 'Код из СМС',
                                controller: codeText,
                                keyboardType: TextInputType.number,
                              ),
                            ),
                            const SizedBox(height: 10),
                            Text(
                              'На номер ${widget.phoneNumber} отправлен код подтверждения',
                              style: const TextStyle(
                                color: Color(0xff323232),
                                fontSize: 18,
                              ),
                            ),
                          ]),
                        ),
                      ),
                    ],
                  ),
                  Positioned(
                    bottom: 16 + MediaQuery.of(context).viewInsets.bottom,
                    left: 20,
                    right: 20,
                    height: 64,
                    child: Center(
                      child: Row(
                        children: [
                          SizedBox(
                            height: 64,
                            child: Material(
                              borderRadius: BorderRadius.circular(16),
                              color: Colors.white,
                              child: Opacity(
                                opacity: !timer.isActive ? 1 : 0.2,
                                child: ElevatedButton(
                                  style: ButtonStyle(
                                    backgroundColor: MaterialStateProperty.all(
                                      Colors.transparent,
                                    ),
                                    shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(16),
                                        side: BorderSide(
                                          width: 1,
                                          color: ColorData
                                              .clientsButtonColorDefault,
                                        ),
                                      ),
                                    ),
                                    elevation: MaterialStateProperty.all(0.0),
                                  ),
                                  onPressed: () async {
                                    if (timer.isActive) return;
                                    setState(() {
                                      timer.cancel();
                                      dateTime = DateTime.now();
                                      timer = Timer(
                                        Duration(seconds: 60),
                                        () {
                                          setState(() {});
                                        },
                                      );
                                    });

                                    RegisterBloc.of(context).add(
                                      SendCodeRegisterEvent(
                                        phoneNumber: widget.phoneNumber,
                                      ),
                                    );
                                  },
                                  child: timer.isActive
                                      ? TimerWait(
                                          timer: timer,
                                          dateTime: dateTime,
                                        )
                                      : SvgPicture.asset(
                                          'assets/svg/icon/refresh.svg',
                                        ),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(width: 10),
                          Expanded(
                            flex: 2,
                            child: SizedBox(
                              height: 64,
                              child: ElevatedButton(
                                onPressed: () async {
                                  RegisterBloc.of(context).add(
                                    CheckSMSRegisterEvent(
                                      phoneNumber: widget.phoneNumber,
                                      code: codeText.text,
                                    ),
                                  );
                                },
                                child: Text(
                                  'Подтвердить номер',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                    codeText.text.length == 4
                                        ? buttonsDefault
                                        : Color(0xffF0F0F0),
                                  ),
                                  foregroundColor: MaterialStateProperty.all(
                                    codeText.text.length == 4
                                        ? Colors.white
                                        : Color(0xffCCCCCC),
                                  ),
                                  animationDuration:
                                      Duration(milliseconds: 300),
                                  shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(16),
                                    ),
                                  ),
                                  elevation: MaterialStateProperty.all(0.0),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}

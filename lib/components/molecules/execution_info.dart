import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/const.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class ExecutorInfo extends StatelessWidget {
  const ExecutorInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: Color(0xffF8F8F8),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 16),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CircleAvatar(
                  radius: 34.5,
                  backgroundImage: NetworkImage(
                    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSCflNxb--nFOgiocMMGT0i2R_V780eOG0eTA&usqp=CAU',
                  ),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: 'Иван Иванов',
                              style: TextStyle(
                                fontSize: H2TextStyle.fontSize,
                                fontWeight: H2TextStyle.fontWeight,
                                height: H2TextStyle.height,
                                color: Colors.black,
                              ),
                            ),
                            WidgetSpan(
                              child: Padding(
                                padding: EdgeInsets.only(left: 2),
                                child: SvgPicture.asset(
                                  'assets/svg/icon/ExecutorInfoIcons/ExecutorOnline.svg',
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      const SizedBox(height: 2),
                      const Text(
                        'Тату-студия Zabey',
                        style: TextStyle(
                          fontSize: P3TextStyle.fontSize,
                          fontWeight: P3TextStyle.fontWeight,
                          height: P3TextStyle.height,
                          color: ColorData.allMainActivegray,
                        ),
                      ),
                      const Text(
                        'ул. Сейфуллина, 120',
                        style: TextStyle(
                          fontSize: P3TextStyle.fontSize,
                          fontWeight: P3TextStyle.fontWeight,
                          height: P3TextStyle.height,
                          color: ColorData.allMainActivegray,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
            const SizedBox(height: 20),
            DecoratedBox(
              decoration: BoxDecoration(
                boxShadow: bigBlur,
                color: Colors.white,
                borderRadius: BorderRadius.circular(16),
              ),
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Татуировки',
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              Text(
                                'От 15 000 ₸',
                                style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  height: 17 / 14,
                                  color: ColorData.allMainBlack,
                                ),
                              )
                            ],
                          ),
                        ),
                        SvgPicture.asset(
                            'assets/svg/icon/ExecutorInfoIcons/tattooMachine.svg')
                      ],
                    ),
                    const SizedBox(height: 16),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Описание',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            height: 17 / 14,
                            color: ColorData.allMainBlack,
                            fontSize: 14,
                          ),
                        ),
                        Text(
                          'Цена указана за час работы. Делаю как по своим, так и по чужим эскизам.',
                          style: TextStyle(
                            fontWeight: FontWeight.w400,
                            height: 17 / 14,
                            color: ColorData.allMainActivegray,
                            fontSize: 14,
                          ),
                        )
                      ],
                    ),
                    const SizedBox(height: 16),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Фотографий работ',
                          style: TextStyle(fontWeight: FontWeight.w600),
                        ),
                        const SizedBox(height: 4),
                        Container(
                          height: 80,
                          child: ListView.separated(
                            separatorBuilder: (_, __) =>
                                const SizedBox(width: 5),
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, index) => Container(
                              width: 105,
                              height: 80,
                              decoration: BoxDecoration(
                                color: Colors.orangeAccent,
                                borderRadius: BorderRadius.circular(6),
                              ),
                            ),
                            itemCount: 20,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      RepaintBoundary(
                        child: SvgPicture.asset(
                            'assets/svg/icon/ExecutorInfoIcons/starrating.svg'),
                      ),
                      SizedBox(width: 2),
                      Text(
                        '5,0',
                        style: TextStyle(
                          fontSize: 16,
                          color: Color(0xff00DF31),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      RepaintBoundary(
                        child: SvgPicture.asset(
                            'assets/svg/icon/ExecutorInfoIcons/smilerating.svg'),
                      ),
                      SizedBox(width: 2),
                      Text(
                        '100',
                        style: TextStyle(
                          fontSize: 16,
                          color: Color(0xff00DF31),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      RepaintBoundary(
                        child: SvgPicture.asset(
                            'assets/svg/icon/ExecutorInfoIcons/repost.svg'),
                      ),
                      SizedBox(width: 2),
                      Text(
                        '0',
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      RepaintBoundary(
                        child: SvgPicture.asset(
                            'assets/svg/icon/ExecutorInfoIcons/location.svg'),
                      ),
                      SizedBox(width: 2),
                      Text(
                        '500м',
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

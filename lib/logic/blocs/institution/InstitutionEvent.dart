import 'dart:io';

import 'package:equatable/equatable.dart';

abstract class InstitutionEvent extends Equatable {
  const InstitutionEvent();

  @override
  List<Object?> get props => [];
}

class EditDataCertificatgeInstitutionEvent extends InstitutionEvent {
  final File file;

  const EditDataCertificatgeInstitutionEvent(this.file);
}

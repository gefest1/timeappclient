import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart';

Future<bool> getPermsissionsLocation() async {
  final Location _location = Location();
  bool _serviceEnabled;
  PermissionStatus _permissionGranted;

  _serviceEnabled = await _location.serviceEnabled();
  if (!_serviceEnabled) {
    if (!_serviceEnabled) return false;
  }

  _permissionGranted = await _location.hasPermission();
  if (_permissionGranted == PermissionStatus.denied) {
    if (_permissionGranted != PermissionStatus.granted) return false;
  }
  return true;
}

Future<void> getPermsissionsLocationAndRequest() async {
  final Location _location = Location();
  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  _serviceEnabled = await _location.serviceEnabled();
  if (!_serviceEnabled) {
    _serviceEnabled = await _location.requestService();
    if (!_serviceEnabled) throw 'ERROR';
  }

  _permissionGranted = await _location.hasPermission();
  if (_permissionGranted == PermissionStatus.denied) {
    _permissionGranted = await _location.requestPermission();
    if (_permissionGranted != PermissionStatus.granted) throw 'ERROR';
  }
}

Future<Position> determinePosition() async {
  try {
    await getPermsissionsLocationAndRequest();
    return await Geolocator.getCurrentPosition();
  } catch (e) {
    throw e;
    // return await determinePosition();
  }
}

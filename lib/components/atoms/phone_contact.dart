import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/const.dart';
import 'package:time_app_components/utils/color.dart';

class PhoneContact extends StatelessWidget {
  const PhoneContact({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(16)),
        boxShadow: bigBlur,
      ),
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          RepaintBoundary(child: SvgPicture.asset('assets/svg/icon/call.svg')),
          Text(
            'Телефон',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 16,
              color: ColorData.allMainBlack,
            ),
          ),
          const SizedBox(height: 2),
          Text(
            '+7 708 382 24 61',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 16,
              color: ColorData.allMainBlack,
            ),
          ),
        ],
      ),
    );
  }
}

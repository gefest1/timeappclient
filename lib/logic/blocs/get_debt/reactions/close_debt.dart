part of '../getdebt_bloc.dart';

extension CloseDebt on GetdebtBloc {
  void closeDebtEvent(CloseDebtEvent event, Emitter<GetdebtState> emit) async {
    final isLoading = state is LoadingDebtState;
    if (isLoading) {
      add(
        CloseDebtEvent(
          completer: event.completer,
          creditCardId: event.creditCardId,
        ),
      );
      return;
    }
    if (state is NoDebtState) {
      event.completer.complete();
      return;
    }
    if (state is DebtState) {
      try {
        final newUser = await PayDebtSession.payDebtOfUser(
          creditCardId: event.creditCardId,
          client: graphqlMinuteProvider.client,
        );
        if (newUser.debt == null || newUser.debt == 0) {
          emit(NoDebtState(newUser));
          event.completer.complete();
          return;
        }
        throw newUser.debt.toString();
      } catch (e) {
        event.completer.completeError(e.toString());
        return;
      }
    }
  }
}

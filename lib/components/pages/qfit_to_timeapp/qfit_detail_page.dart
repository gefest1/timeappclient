import 'dart:io';
import 'dart:math';

import 'package:expandable_text/expandable_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:li/components/pages/auth/your_phone_number_page.dart';
import 'package:provider/provider.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/utils/testStyles.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:li/components/molecules/minute/bottom_detail_data.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/components/pages/qfit_to_timeapp/minute_map_page.dart';
import 'package:li/const.dart';
import 'package:li/data/models/institution_minute.dart';
import 'package:li/data/models/meta_rating_model.dart';
import 'package:li/data/models/review/color_review_model.dart';
import 'package:li/data/models/review/comment_model.dart';
import 'package:li/logic/blocs/comments_of_club/commentsofclub_bloc.dart';
import 'package:li/logic/providers/location/location_provider.dart';
import 'package:li/main.dart';
import 'package:li/utils/launch_universal_link.dart';
import 'package:li/utils/locationsBetween.dart';

import '../../../utils/color.dart';
import '../../../utils/text_styles.dart';
import '../../molecules/sliver_slider_top_card.dart';
import 'additional_container.dart';
import 'instagram_contact.dart';
import 'phone_contact.dart';
import 'qr_scan_page.dart';
import 'raiting_feedback.dart';
import 'review_card.dart';
import 'whatsapp_contact.dart';

class QfitDetailPage extends StatefulWidget {
  final InstitutionsMinute institution;

  QfitDetailPage({required this.institution, Key? key}) : super(key: key);

  @override
  State<QfitDetailPage> createState() => _QfitDetailPageState();
}

class _QfitDetailPageState extends State<QfitDetailPage> {
  final ScrollController scrollController = ScrollController();
  final List<Color> colors = [];
  late final locationProvider = Provider.of<LocationProvider>(
    context,
    listen: false,
  );

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final _workTime = widget.institution.workTimes?.isNotEmpty == true
        ? widget.institution.workTimes![0]
        : null;
    return Material(
      color: Colors.white,
      child: Stack(
        children: [
          Padding(
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).padding.top + 56 - 16,
            ),
            child: CustomScrollView(
              physics: BouncingScrollPhysics(),
              controller: scrollController,
              slivers: [
                if (widget.institution.galleryURLs != null &&
                    widget.institution.galleryURLs!.isNotEmpty)
                  SliverSliderTopCard(
                    imageHeight: 300,
                    scrollController: scrollController,
                    institution: widget.institution,
                  )
                else
                  SliverToBoxAdapter(
                    child: const SizedBox(height: 52),
                  ),
                SliverPadding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  sliver: SliverList(
                    delegate: SliverChildListDelegate(
                      [
                        SizedBox(height: 30),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            widget.institution.name!,
                            style: TextStyle(
                              color: ColorData.allMainBlack,
                              fontSize: H2TextStyle.fontSize,
                              fontWeight: H2TextStyle.fontWeight,
                              height: H2TextStyle.height,
                            ),
                          ),
                        ),
                        SizedBox(height: 5),
                        if (widget.institution.description != null)
                          ExpandableText(
                            widget.institution.description!,
                            expandText: 'показать полностью',
                            collapseText: 'скрыть',
                            maxLines: 5,
                            expandOnTextTap: true,
                            collapseOnTextTap: true,
                            linkColor: ColorData.allMainActivegray,
                            animation: true,
                            style: TextStyle(
                              color: ColorData.allMainGray,
                              fontSize: P3TextStyle.fontSize,
                              fontWeight: P3TextStyle.fontWeight,
                              height: P3TextStyle.height,
                            ),
                            linkStyle: TextStyle(
                              color: ColorData.allMainGray,
                              fontSize: P3TextStyle.fontSize,
                              fontWeight: FontWeight
                                  .values[P3TextStyle.fontWeight.index + 1],
                              height: P3TextStyle.height,
                            ),
                          ),
                        const SizedBox(height: 10),
                        BottomDetailData(
                          price: widget.institution.averagePrice!,
                          locationLabel: locationProvider.location == null
                              ? null
                              : locationBetweenText(
                                  widget.institution.location!.latitude!
                                      .toDouble(),
                                  widget.institution.location!.longitude!
                                      .toDouble(),
                                  locationProvider.location!.latitude,
                                  locationProvider.location!.longitude,
                                ),
                          rating: widget.institution.rating == null
                              ? null
                              : widget.institution.rating?.toStringAsFixed(1),
                          ratingCount: widget.institution.ratingCount == null
                              ? null
                              : widget.institution.ratingCount.toString(),
                        ),
                        const SizedBox(height: 10),
                        if (_workTime?.startTime != null &&
                            _workTime?.endTime != null)
                          Container(
                            height: 50,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              color: ColorData.allMainLightgray,
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Row(
                                      children: [
                                        const SizedBox(width: 5),
                                        Flexible(
                                          child: Text(
                                            'Сегодня ' +
                                                DateFormat('Hm').format(widget
                                                    .institution
                                                    .workTimes![0]
                                                    .startTime!) +
                                                ' - ' +
                                                DateFormat('Hm').format(widget
                                                    .institution
                                                    .workTimes![0]
                                                    .endTime!),
                                            style: TextStyle(
                                              fontSize: 16,
                                            ),
                                          ),
                                        ),
                                        const SizedBox(width: 4),
                                        CircleAvatar(
                                          backgroundColor: Color(0xFFBEFFC5),
                                          maxRadius: 7,
                                          child: CircleAvatar(
                                            backgroundColor:
                                                ColorData.clientsStrokeSucces,
                                            maxRadius: 3.18,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      const SizedBox(width: 4),
                                      if (widget.institution.contacts
                                              ?.instagram !=
                                          null)
                                        GestureDetector(
                                          onTap: () {
                                            if (Platform.isIOS)
                                              launchUniversalLinkIos(
                                                'https://www.instagram.com/${widget.institution.contacts!.instagram}/',
                                              );
                                            if (Platform.isAndroid)
                                              launch(
                                                  'https://www.instagram.com/${widget.institution.contacts!.instagram}/');
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(6),
                                            ),
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(10.0),
                                              child: Center(
                                                child: RepaintBoundary(
                                                  child: SvgPicture.asset(
                                                    'assets/svg/icon/worldNet.svg',
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      const SizedBox(width: 6),
                                      if (widget.institution.contacts
                                              ?.phoneNumber! !=
                                          null)
                                        GestureDetector(
                                          behavior: HitTestBehavior.opaque,
                                          onTap: () => launch(
                                              "tel://${widget.institution.contacts!.phoneNumber!}"),
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(6),
                                            ),
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(10.0),
                                              child: Center(
                                                child: RepaintBoundary(
                                                  child: SvgPicture.asset(
                                                    'assets/svg/icon/greenCall.svg',
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        const SizedBox(height: 20),
                        SizeTapAnimation(
                          child: GestureDetector(
                            onTap: () {
                              routerDelegate.push(
                                MinutePagePage(
                                  institutionsMinute: widget.institution,
                                ),
                              );
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: Colors.white,
                                boxShadow: bigBlur,
                              ),
                              padding: EdgeInsets.symmetric(
                                vertical: 20,
                                horizontal: 15,
                              ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          'Адрес',
                                          style: TextStyle(
                                            color: ColorData.allMainBlack,
                                            fontSize: H2TextStyle.fontSize,
                                            fontWeight: H2TextStyle.fontWeight,
                                            height: H2TextStyle.height,
                                          ),
                                        ),
                                        Text(
                                          '${widget.institution.location!.address}',
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w400,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  RepaintBoundary(
                                    child: SvgPicture.asset(
                                      'assets/svg/icon/ic_point_address.svg',
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 10),
                        GestureDetector(
                          onTap: () async {
                            print(
                                'https://yandex.kz/maps/162/almaty/?ll=${locationProvider.location!.longitude}%2C43.236092&mode=routes&rtext=${locationProvider.location!.latitude}%2C76.915264~${widget.institution.location!.latitude}%2C${widget.institution.location!.longitude}&rtt=auto&ruri=~&z=14');
                            if (locationProvider.location != null) {
                              if (Platform.isIOS) {
                                launchUniversalLinkIos(
                                    'https://yandex.kz/maps/162/almaty/?ll=${locationProvider.location!.longitude}%2C43.236092&mode=routes&rtext=${locationProvider.location!.latitude}%2C76.915264~${widget.institution.location!.latitude}%2C${widget.institution.location!.longitude}&rtt=auto&ruri=~&z=14');
                              } else {
                                launch(
                                    'https://yandex.kz/maps/162/almaty/?ll=${locationProvider.location!.longitude}%2C43.236092&mode=routes&rtext=${locationProvider.location!.latitude}%2C76.915264~${widget.institution.location!.latitude}%2C${widget.institution.location!.longitude}&rtt=auto&ruri=~&z=14');
                              }
                            }
                          },
                          child: Container(
                            // color: Colors.white,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Colors.white,
                              boxShadow: bigBlur,
                            ),
                            padding: EdgeInsets.symmetric(
                              vertical: 20,
                              horizontal: 15,
                            ),
                            child: Text(
                              'Проложить маршрут',
                              style: TextStyle(
                                color: buttonsDefault,
                                fontSize: 18,
                                fontWeight: FontWeight.w400,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        SizedBox(height: 40),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if (widget.institution.contacts?.toJson() != null)
                              Text(
                                'Контакты',
                                style: TextStyle(
                                  fontSize: H2TextStyle.fontSize,
                                  fontWeight: H2TextStyle.fontWeight,
                                  height: H2TextStyle.height,
                                  color: ColorData.allMainBlack,
                                ),
                              ),
                            const SizedBox(height: 10),
                            Row(
                              children: [
                                if (widget.institution.contacts?.phoneNumber !=
                                    null)
                                  Expanded(
                                    child: PhoneContact(
                                      label: widget
                                          .institution.contacts!.phoneNumber!,
                                    ),
                                  )
                                else
                                  Expanded(child: SizedBox()),
                                const SizedBox(width: 10),
                                if (widget.institution.contacts?.instagram !=
                                    null)
                                  Expanded(
                                    child: InstagramContact(
                                        label: widget
                                            .institution.contacts!.instagram!),
                                  )
                                else
                                  Expanded(child: SizedBox()),
                              ],
                            ),
                            const SizedBox(height: 10),
                            Row(
                              children: [
                                if (widget.institution.contacts?.whatsApp !=
                                    null)
                                  Expanded(
                                    child: WhatsAppContact(
                                      label: widget
                                          .institution.contacts!.whatsApp!,
                                    ),
                                  ),
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(height: 40),
                        if (widget.institution.tags != null) ...[
                          AdditionalContainer(
                            tags: widget.institution.tags!,
                          ),
                          const SizedBox(height: 40),
                        ],
                        BlocBuilder<CommentsofclubBloc, CommentsofclubState>(
                          builder: (context, state) {
                            MetaRatingModel? metaRating;
                            if (state is CommentsFetched) {
                              metaRating =
                                  state.metaRatingModel[widget.institution.id];
                            }
                            if (metaRating == null) return const SizedBox();
                            return RaingFeedback(
                              fiveStars:
                                  metaRating.groupedRatings?.fiveStars ?? 0,
                              fourStars:
                                  metaRating.groupedRatings?.fourStars ?? 0,
                              threeStars:
                                  metaRating.groupedRatings?.threeStars ?? 0,
                              twoStars:
                                  metaRating.groupedRatings?.twoStars ?? 0,
                              oneStars:
                                  metaRating.groupedRatings?.oneStars ?? 0,
                              rating: (widget.institution.rating ?? 0)
                                  .toStringAsFixed(1),
                              sum: (metaRating.totalSum ?? 0),
                              ratingCount: (widget.institution.ratingCount ?? 0)
                                  .toString(),
                            );
                          },
                        ),
                        const SizedBox(height: 20),
                      ],
                    ),
                  ),
                ),
                BlocBuilder<CommentsofclubBloc, CommentsofclubState>(
                  builder: (context, state) {
                    List<CommentModel>? commentsOfclub;
                    if (state is CommentsFetched) {
                      commentsOfclub = state.comments[widget.institution.id];
                      if (commentsOfclub != null) {
                        while (colors.length < commentsOfclub.length) {
                          colors.add(ColorRandom.random);
                        }
                      }
                    }
                    if (commentsOfclub == null || commentsOfclub.isEmpty)
                      return SliverToBoxAdapter(
                        child: Padding(
                          padding: const EdgeInsets.only(
                            top: 20,
                            bottom: 40,
                            left: 20,
                          ),
                          child: Text(
                            'Комментариев пока нет. Будьте первым!',
                            style: TextStyle(
                              fontSize: P2TextStyle.fontSize,
                              fontWeight: P2TextStyle.fontWeight,
                              height: P2TextStyle.height,
                              color: ColorData.allMainActivegray,
                            ),
                          ),
                        ),
                      );
                    return SliverPadding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      sliver: SliverList(
                        delegate: SliverChildBuilderDelegate(
                          (ctx, ind) {
                            if (ind % 2 == 1) return const SizedBox(height: 20);
                            return ReviewCard(
                              key: ValueKey(commentsOfclub![ind ~/ 2].id!),
                              comments: commentsOfclub[ind ~/ 2],
                              backgroundColor: colors[ind ~/ 2],
                            );
                          },
                          childCount: commentsOfclub.length * 2,
                        ),
                      ),
                    );
                  },
                ),
                SliverToBoxAdapter(
                  child: Container(
                    height: 100,
                  ),
                )
              ],
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: CustomAppBar(
              leading: IconButton(
                onPressed: () => Navigator.pop(context),
                icon: SvgPicture.asset('assets/svg/icon/backArrow.svg'),
              ),
              actions: [
                // IconButton(
                //   onPressed: () {},
                //   icon: SvgPicture.asset(
                //     'assets/svg/icon/hearth.svg',
                //     color: Colors.black,
                //   ),
                // ),
                // IconButton(
                //   onPressed: () async {
                //     await Share.share('Скачайте приложение TimeApp!');
                //   },
                //   icon: SvgPicture.asset('assets/svg/icon/ic_share_header.svg'),
                // ),
              ],
              centerTitle: true,
              title: Text(
                '${widget.institution.name}',
                style: TextStyle(
                  fontSize: 18,
                  height: 21 / 18,
                  fontWeight: FontWeight.w400,
                  color: ColorData.allMainBlack,
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: DecoratedBox(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(16),
                ),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, 10),
                    blurRadius: 30,
                    color: Color(0xFF323232).withOpacity(0.1),
                  )
                ],
              ),
              child: Padding(
                padding: EdgeInsets.only(
                  right: 20,
                  left: 20,
                  bottom: max(MediaQuery.of(context).padding.bottom, 20),
                  top: 20,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: Container(
                        height: 40,
                        child: ElevatedButton(
                          style: ButtonStyle(
                            elevation: MaterialStateProperty.all(0),
                            shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                          ),
                          onPressed: () {
                            routerDelegate.push(
                              isRegistered()
                                  ? const QrScanPage()
                                  : const YourPhoneNumber(),
                            );
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(
                                height: 20,
                                width: 20,
                                child: SvgPicture.asset(
                                  'assets/svg/icon/icon_qr.svg',
                                  color: Colors.white,
                                ),
                              ),
                              const SizedBox(width: 4),
                              Text(
                                'Начать',
                                style: TextStyle(
                                  fontSize: P3TextStyle.fontSize,
                                  fontWeight: P3TextStyle.fontWeight,
                                  height: P3TextStyle.height,
                                  color: Color(0xffFCFCFC),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

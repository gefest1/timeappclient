import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:cloudpayments/cloudpayments.dart';
import 'package:cloudpayments/three_ds_response.dart';
import 'package:dart_ipify/dart_ipify.dart';
import 'package:li/components/atoms/error_overlay.dart';
import 'package:li/logic/payment_api.dart';
import 'package:li/logic/blocs/Payment/paymentEvent.dart';
import 'package:li/logic/blocs/Payment/paymentState.dart';
import 'package:li/logic/repository/PaymentsRepository.dart';
import 'package:li/main.dart';

class PaymentBloc extends Bloc<PaymentEvent, PaymentState> {
  final PaymentRepository paymentRepository;

  PaymentBloc({required this.paymentRepository}) : super(PaymentInitial()) {
    on<AuthPaymentRequested>(authPaymentStream);
  }

  void authPaymentStream(
    AuthPaymentRequested event,
    Emitter<PaymentState> emit,
  ) async {
    emit(PaymentLoadingInProgress());
    try {
      final _array = await Future.wait([
        Cloudpayments.isValidNumber(event.cardNumber),
        Cloudpayments.isValidExpiryDate(event.expireDate),
      ]);
      final bool isValidNumber = _array[0];
      final bool isValidExpiryDate = _array[1];
      if (!isValidNumber) {
        emit(PaymentCredentialsFailure(errorMessage: 'Невалидный номер карты'));
        return;
      }
      if (!isValidExpiryDate) {
        emit(PaymentCredentialsFailure(errorMessage: 'Невалидный срок карты'));
        return;
      }

      final cryptogram = await Cloudpayments.cardCryptogram(
        cardNumber: event.cardNumber,
        cardDate: event.expireDate,
        cardCVC: event.cvcCode,
        publicId: cloud_payments_public_id,
      );
      final ipv4 = await Ipify.ipv4();
      final res = await paymentRepository.authPayment(
        accountId: event.accountId,
        amount: event.amount,
        cardHolderName: event.cardHolderName,
        cryptogram: cryptogram.cryptogram!,
        currency: 'KZT',
        ipAddress: ipv4,
        email: event.email,
      );

      bool isSuccess = res.data['Success'];
      if (isSuccess) {
        emit(PaymentLoadingSuccessPrepare(
          cardHolderName: res.data['Model']['Name'],
          amount: event.amount,
          cardHolderMessage: res.data['Model']['CardHolderMessage'],
          transactionId: res.data["Model"]['TransactionId'],
          token: res.data["Model"]["Token"],
          cardType: res.data["Model"]["CardType"],
          cardNumberLast: event.cardNumber
              .substring(event.cardNumber.length - 4, event.cardNumber.length),
        ));
        emit(PaymentInitial());

        paymentRepository.cancelPayment(
          transactionId: res.data["Model"]['TransactionId'].toString(),
        );
        return;
      }
      final acsUrl = res.data['Model']['AcsUrl'];
      bool isThreeDsRequired = acsUrl != null;

      if (!isThreeDsRequired) {
        emit(PaymentLoadingFailure(
          errorMessage: res.data['Model']['CardHolderMessage'],
        ));
        return;
      }

      final String transactionId =
          res.data['Model']['TransactionId'].toString();
      final String paReq = res.data['Model']['PaReq'];

      final ThreeDsResponse threeDsCallbackData = (await Cloudpayments.show3ds(
        acsUrl: acsUrl,
        transactionId: transactionId,
        paReq: paReq,
      ))!;

      final paymentResult = await paymentRepository.post3ds(
          paRes: threeDsCallbackData.paRes,
          transactionId: threeDsCallbackData.md!);

      isSuccess = paymentResult.data['Success'];

      if (!isSuccess) {
        emit(
          PaymentLoadingFailure(
            errorMessage: paymentResult.data['Model']['CardHolderMessage'],
          ),
        );
      }

      emit(
        PaymentLoadingSuccessPrepare(
          cardHolderName: paymentResult.data['Model']['Name'],
          amount: event.amount,
          cardHolderMessage: paymentResult.data['Model']['CardHolderMessage'],
          transactionId: res.data["Model"]['TransactionId'],
          token: paymentResult.data["Model"]["Token"],
          cardType: paymentResult.data["Model"]["CardType"],
          cardNumberLast: event.cardNumber
              .substring(event.cardNumber.length - 4, event.cardNumber.length),
        ),
      );
      emit(PaymentInitial());

      paymentRepository.cancelPayment(
        transactionId: res.data["Model"]['TransactionId'].toString(),
      );
    } catch (e) {
      navigatorKey.currentState?.overlay?.insert(
        ErrorOverlayLoader(text: 'Оплата не прошла'),
      );
      // return;
      // log(e.toString());
    }
    emit(PaymentInitial());
  }
}

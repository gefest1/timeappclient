part of '../MainPageBloc.dart';

extension GetData on MainPageMinuteBloc {
  void getData(
    GetDataMinuteEvent event,
    Emitter<MainPageMinuteState> emit,
  ) async {
    final queryResult = await this.graphqlMinuteProvider.client.query(
          QueryOptions(
            document: gql('''
            query {
              getMainPage {
                slug
                institutions {
                  type
                  avatarURL {
                    M
                    XL
                    thumbnail
                  }
                  averagePrice
                  city
                  rating
                  ratingCount
                  contacts {
                    email
                    instagram
                    phoneNumber
                    whatsApp
                  }
                  tags {
                    iconSVGPath
                    name
                  }
                  dateAdded
                  description
                  galleryURLs {
                    M
                    XL
                    thumbnail
                  }
                  location{
                    latitude
                    longitude
                    address
                  }
                  name
                  _id    
                  workTimes {
                    endTime
                    startTime
                  }
              	}
                title {
                  ru
                }
                numberOfInstitutions
              }
            }'''),
          ),
        );
    if (queryResult.hasException) throw queryResult.exception!;
    emit(
      MainPageMinuteLodedState(
        List<MainPageModel>.from(
          queryResult.data!['getMainPage']?.map(
            (x) => MainPageModel.fromMap(x),
          ),
        ),
      ),
    );
  }
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';
import 'package:li/logic/providers/location/location_provider.dart';
import 'package:li/utils/flow_menu_delegate.dart';
import 'package:li/utils/offset_flow_mixin.dart';

extension ListenFlow on _LocationMarkerState {
  void listenChanges([Null _]) async {
    if (notFinished) return;

    notFinished = true;
    try {
      final List<Offset?> _newOffsets = [
        await getOffset(
          latitude: locationProvider.location!.latitude.toDouble(),
          longitude: locationProvider.location!.longitude.toDouble(),
        )
      ];

      offsetListen.value = _newOffsets.map((e) => e!).toList();
    } catch (e) {}
    notFinished = false;
  }
}

class LocationMarker extends StatefulWidget {
  final YandexMapController yandexMapController;
  final StreamController<Null> streamController;

  const LocationMarker({
    required this.yandexMapController,
    required this.streamController,
    Key? key,
  }) : super(key: key);

  @override
  _LocationMarkerState createState() => _LocationMarkerState();
}

class _LocationMarkerState extends State<LocationMarker> with OffsetExtension {
  late LocationProvider locationProvider =
      Provider.of<LocationProvider>(context, listen: false);
  final ValueNotifier<List<Offset>> offsetListen =
      ValueNotifier<List<Offset>>([]);
  late StreamSubscription<Null> streamSubscription;
  bool notFinished = false;

  @override
  void initState() {
    listenChanges();
    yandexMapController = widget.yandexMapController;
    streamSubscription = widget.streamController.stream.listen(listenChanges);

    super.initState();
  }

  @override
  void setState(VoidCallback fn) {
    super.setState(fn);
  }

  @override
  void dispose() {
    streamSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LocationProvider>(
      builder: (_, LocationProvider locationProvider, Widget? child) {
        return IgnorePointer(
          child: Flow(
            delegate: FlowMenuDelegate(
              offsetListen: offsetListen,
            ),
            children: [
              IgnorePointer(
                child: Container(
                  height: 60,
                  width: 60,
                  decoration: BoxDecoration(
                    color: Color(0x4d527e5c),
                    shape: BoxShape.circle,
                  ),
                  alignment: Alignment.center,
                  child: Container(
                    height: 20,
                    width: 20,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.circle,
                    ),
                    child: Container(
                      height: 13.5,
                      width: 13.5,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Color(0xff55D472),
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}

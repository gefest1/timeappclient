import 'package:flutter/material.dart';

class DeleteTrash extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(3.25, 4.02734);
    path_0.lineTo(16.75, 4.02734);
    path_0.lineTo(16.3125, 12.777);
    path_0.cubicTo(16.1795, 15.4381, 13.9831, 17.5273, 11.3188, 17.5273);
    path_0.lineTo(8.68125, 17.5273);
    path_0.cubicTo(6.01688, 17.5273, 3.82054, 15.4381, 3.68748, 12.777);
    path_0.lineTo(3.25, 4.02734);
    path_0.close();

    Paint paint_0_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.07500000;
    paint_0_stroke.color = Color(0xffF40F0F).withOpacity(1.0);
    canvas.drawPath(path_0, paint_0_stroke);

    Path path_1 = Path();
    path_1.moveTo(6.25, 4.02734);
    path_1.lineTo(6.25, 4.02734);
    path_1.cubicTo(6.25, 2.37049, 7.59315, 1.02734, 9.25, 1.02734);
    path_1.lineTo(10.75, 1.02734);
    path_1.cubicTo(12.4069, 1.02734, 13.75, 2.37049, 13.75, 4.02734);
    path_1.lineTo(13.75, 4.02734);

    Paint paint_1_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.07500000;
    paint_1_stroke.color = Color(0xffF40F0F).withOpacity(1.0);
    canvas.drawPath(path_1, paint_1_stroke);

    Path path_2 = Path();
    path_2.moveTo(1.75, 4.02734);
    path_2.lineTo(18.25, 4.02734);

    Paint paint_2_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.1000000;
    paint_2_stroke.color = Color(0xffF40F0F).withOpacity(1.0);
    paint_2_stroke.strokeCap = StrokeCap.round;
    canvas.drawPath(path_2, paint_2_stroke);

    Paint paint_2_fill = Paint()..style = PaintingStyle.fill;
    paint_2_fill.color = Color(0xff000000).withOpacity(1.0);
    canvas.drawPath(path_2, paint_2_fill);

    Path path_3 = Path();
    path_3.moveTo(12.25, 8.52734);
    path_3.lineTo(12.25, 12.2773);

    Paint paint_3_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.07500000;
    paint_3_stroke.color = Color(0xffF40F0F).withOpacity(1.0);
    paint_3_stroke.strokeCap = StrokeCap.round;
    canvas.drawPath(path_3, paint_3_stroke);

    Paint paint_3_fill = Paint()..style = PaintingStyle.fill;
    paint_3_fill.color = Color(0xff000000).withOpacity(1.0);
    canvas.drawPath(path_3, paint_3_fill);

    Path path_4 = Path();
    path_4.moveTo(7.75, 8.52734);
    path_4.lineTo(7.75, 12.2773);

    Paint paint_4_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.07500000;
    paint_4_stroke.color = Color(0xffF40F0F).withOpacity(1.0);
    paint_4_stroke.strokeCap = StrokeCap.round;
    canvas.drawPath(path_4, paint_4_stroke);

    Paint paint_4_fill = Paint()..style = PaintingStyle.fill;
    paint_4_fill.color = Color(0xff000000).withOpacity(1.0);
    canvas.drawPath(path_4, paint_4_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

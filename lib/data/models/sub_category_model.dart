import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:li/utils/translate.dart';

class SubCategoryModel extends Equatable {
  final Translate? services;
  final String? slug;
  final String? svgPicture;
  final Translate? title;

  const SubCategoryModel({
    this.services,
    this.slug,
    this.svgPicture,
    this.title,
  });

  SubCategoryModel copyWith({
    Translate? services,
    String? slug,
    String? svgPicture,
    Translate? title,
  }) {
    return SubCategoryModel(
      services: services ?? this.services,
      slug: slug ?? this.slug,
      svgPicture: svgPicture ?? this.svgPicture,
      title: title ?? this.title,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'services': services?.toMap(),
      'slug': slug,
      'svgPicture': svgPicture,
      'title': title?.toMap(),
    };
  }

  static SubCategoryModel? fromMap(Map<String, dynamic>? map) {
    if (map == null) return null;
    return SubCategoryModel(
      services: Translate.fromMap(map['services']),
      slug: map['slug'],
      svgPicture: map['svgPicture'],
      title: Translate.fromMap(map['title']),
    );
  }

  String toJson() => json.encode(toMap());

  factory SubCategoryModel.fromJson(String source) =>
      SubCategoryModel.fromMap(json.decode(source))!;

  @override
  bool get stringify => true;

  @override
  List<Object?> get props => [services, slug, svgPicture, title];
}

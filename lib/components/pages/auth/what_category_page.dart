import 'package:li/components/molecules/choose_category.dart';
import 'package:li/components/molecules/choose_category_model.dart';
import 'package:li/components/molecules/register_app_bar.dart';

import 'package:li/const.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:math' as math;

class WhatCategoryPage extends StatefulWidget {
  const WhatCategoryPage({Key? key}) : super(key: key);

  @override
  _WhatCategoryPageState createState() => _WhatCategoryPageState();
}

class _WhatCategoryPageState extends State<WhatCategoryPage> {
  String? choosenCategory;

  void onTapCategory(String assetPath, List<String> list, String title) async {
    await showDialog(
      context: context,
      builder: (_) => Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20,
            vertical: 40,
          ),
          child: SizedBox(
            width: math.min(400, MediaQuery.of(_).size.width),
            height: double.infinity,
            child: ChooseCategoryModal(
              list: list,
              assetPath: assetPath,
              title: title,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final _focus = FocusScope.of(context);
        if (_focus.hasFocus || _focus.hasPrimaryFocus) {
          _focus.unfocus();
        }
      },
      child: Material(
        color: Colors.white,
        child: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle(statusBarBrightness: Brightness.light),
          child: Stack(
            children: [
              CustomScrollView(
                slivers: [
                  SliverList(
                    delegate: SliverChildListDelegate([
                      const RegisterAppBar(),
                      const SizedBox(height: 20),
                    ]),
                  ),
                  SliverPadding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    sliver: SliverList(
                      delegate: SliverChildListDelegate([
                        const Text(
                          'Вы вошли!',
                          style: TextStyle(
                            fontSize: 38,
                            color: Color(0xff323232),
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        const SizedBox(height: 4),
                        Text(
                          'Для того, чтобы мы показывали актуальные для вас предложения в первую очередь — укажите услуги, которыми вы пользуетесь чаще всего.',
                          style: const TextStyle(
                            color: Color(0xff323232),
                            fontSize: 18,
                          ),
                        ),
                        const SizedBox(height: 40),
                      ]),
                    ),
                  ),
                  SliverPadding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, bottom: 40),
                    sliver: SliverGrid(
                      delegate: SliverChildListDelegate([
                        ChooseCategory(
                          title: 'Красота',
                          choosen: 'Красота' == choosenCategory,
                          assetPath: 'assets/svg/category/outline.svg',
                          onTap: () => onTapCategory(
                            'assets/svg/category/outline.svg',
                            beatyCategory,
                            'Красота',
                          ),
                        ),
                        ChooseCategory(
                          title: 'Бытовые услуги',
                          choosen: 'Бытовые услуги' == choosenCategory,
                          assetPath: 'assets/svg/category/sewing.svg',
                          onTap: () => onTapCategory(
                            'assets/svg/category/sewing.svg',
                            beatyCategory,
                            'Бытовые услуги',
                          ),
                        ),
                        ChooseCategory(
                          title: 'Авто-мото сервис',
                          choosen: 'Авто-мото сервис' == choosenCategory,
                          assetPath: 'assets/svg/category/autoService.svg',
                          onTap: () => onTapCategory(
                            'assets/svg/category/autoService.svg',
                            beatyCategory,
                            'Авто-мото сервис',
                          ),
                        ),
                        ChooseCategory(
                          title: 'Ремонт электронных устройств',
                          choosen:
                              'Ремонт электронных устройств' == choosenCategory,
                          assetPath: 'assets/svg/category/repairElectronic.svg',
                          onTap: () => onTapCategory(
                            'assets/svg/category/repairElectronic.svg',
                            beatyCategory,
                            'Ремонт электронных устройств',
                          ),
                        ),
                        ChooseCategory(
                          title: 'Развлечения',
                          choosen: 'Развлечения' == choosenCategory,
                          assetPath: 'assets/svg/category/entertainment.svg',
                          onTap: () => onTapCategory(
                            'assets/svg/category/entertainment.svg',
                            beatyCategory,
                            'Развлечения',
                          ),
                        ),
                        ChooseCategory(
                          title: 'Туризм и недвижимость',
                          choosen: 'Туризм и недвижимость' == choosenCategory,
                          assetPath: 'assets/svg/category/tourism.svg',
                          onTap: () => onTapCategory(
                            'assets/svg/category/tourism.svg',
                            beatyCategory,
                            'Туризм и недвижимость',
                          ),
                        ),
                        ChooseCategory(
                          title: 'Еда на вынос',
                          choosen: 'Еда на вынос' == choosenCategory,
                          assetPath: 'assets/svg/category/takeAway.svg',
                          onTap: () => onTapCategory(
                            'assets/svg/category/takeAway.svg',
                            beatyCategory,
                            'Еда на вынос',
                          ),
                        ),
                        ChooseCategory(
                          title: 'Юридическая и бухгалтерская помощь',
                          choosen: 'Юридическая и бухгалтерская помощь' ==
                              choosenCategory,
                          assetPath: 'assets/svg/category/juridical.svg',
                          onTap: () => onTapCategory(
                            'assets/svg/category/juridical.svg',
                            beatyCategory,
                            'Юридическая и бухгалтерская помощь',
                          ),
                        ),
                        ChooseCategory(
                          title: 'Медицина',
                          choosen: 'Медицина' == choosenCategory,
                          assetPath: 'assets/svg/category/medicine.svg',
                          onTap: () => onTapCategory(
                            'assets/svg/category/medicine.svg',
                            beatyCategory,
                            'Медицина',
                          ),
                        ),
                      ]),
                      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                        maxCrossAxisExtent: 200,
                        childAspectRatio: 0.6,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'dart:math' show Random;
import 'dart:ui' show Color;

enum EnumColor { first, second, third, fourth, fifth }

class ColorRandom {
  static const colors = {
    EnumColor.first: Color(0xffFF8360),
    EnumColor.second: Color(0xffEBB84B),
    EnumColor.third: Color(0xff466EC3),
    EnumColor.fourth: Color(0xff8AE85F),
    EnumColor.fifth: Color(0xffEDB12F),
  };

  static Color get random {
    return ColorRandom
        .colors[EnumColor.values[Random().nextInt(EnumColor.values.length)]]!;
  }
}

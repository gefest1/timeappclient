import 'package:flutter/material.dart';

class Text2TypeCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.4792454, size.height * 0.9049816);
    path_0.cubicTo(
        size.width * 0.4797761,
        size.height * 0.9011288,
        size.width * 0.4804380,
        size.height * 0.8980245,
        size.width * 0.4812307,
        size.height * 0.8956687);
    path_0.cubicTo(
        size.width * 0.4820209,
        size.height * 0.8933497,
        size.width * 0.4831877,
        size.height * 0.8914172,
        size.width * 0.4847319,
        size.height * 0.8898773);
    path_0.lineTo(size.width * 0.4847589, size.height * 0.8895337);
    path_0.lineTo(size.width * 0.4561393, size.height * 0.8873190);
    path_0.lineTo(size.width * 0.4526000, size.height * 0.9331104);
    path_0.lineTo(size.width * 0.4750184, size.height * 0.9348405);
    path_0.lineTo(size.width * 0.4792454, size.height * 0.9049816);
    path_0.close();
    path_0.moveTo(size.width * 0.4922601, size.height * 0.8816503);
    path_0.lineTo(size.width * 0.4932153, size.height * 0.8692883);
    path_0.lineTo(size.width * 0.5021479, size.height * 0.8699816);
    path_0.lineTo(size.width * 0.5005423, size.height * 0.8907485);
    path_0.lineTo(size.width * 0.4959896, size.height * 0.8903988);
    path_0.cubicTo(
        size.width * 0.4937540,
        size.height * 0.8914785,
        size.width * 0.4920589,
        size.height * 0.8932822,
        size.width * 0.4909037,
        size.height * 0.8958098);
    path_0.cubicTo(
        size.width * 0.4897196,
        size.height * 0.8983313,
        size.width * 0.4888288,
        size.height * 0.9017669,
        size.width * 0.4882313,
        size.height * 0.9061166);
    path_0.lineTo(size.width * 0.4830006, size.height * 0.9438773);
    path_0.lineTo(size.width * 0.4422399, size.height * 0.9407301);
    path_0.lineTo(size.width * 0.4464264, size.height * 0.8865706);
    path_0.lineTo(size.width * 0.4386209, size.height * 0.8859632);
    path_0.lineTo(size.width * 0.4402264, size.height * 0.8651963);
    path_0.lineTo(size.width * 0.4491160, size.height * 0.8658834);
    path_0.lineTo(size.width * 0.4481607, size.height * 0.8782393);
    path_0.lineTo(size.width * 0.4922601, size.height * 0.8816503);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_0, paint_0_fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.4001448, size.height * 0.8739080);
    path_1.cubicTo(
        size.width * 0.4049141,
        size.height * 0.8706319,
        size.width * 0.4105104,
        size.height * 0.8697117,
        size.width * 0.4169350,
        size.height * 0.8711411);
    path_1.cubicTo(
        size.width * 0.4233595,
        size.height * 0.8725706,
        size.width * 0.4280368,
        size.height * 0.8757791,
        size.width * 0.4309656,
        size.height * 0.8807669);
    path_1.cubicTo(
        size.width * 0.4338890,
        size.height * 0.8857853,
        size.width * 0.4345534,
        size.height * 0.8918773,
        size.width * 0.4329595,
        size.height * 0.8990368);
    path_1.cubicTo(
        size.width * 0.4313724,
        size.height * 0.9061656,
        size.width * 0.4281945,
        size.height * 0.9113681,
        size.width * 0.4234258,
        size.height * 0.9146442);
    path_1.cubicTo(
        size.width * 0.4186503,
        size.height * 0.9179509,
        size.width * 0.4130503,
        size.height * 0.9188896,
        size.width * 0.4066258,
        size.height * 0.9174540);
    path_1.cubicTo(
        size.width * 0.4002012,
        size.height * 0.9160245,
        size.width * 0.3955276,
        size.height * 0.9128037,
        size.width * 0.3926049,
        size.height * 0.9077853);
    path_1.cubicTo(
        size.width * 0.3896755,
        size.height * 0.9027975,
        size.width * 0.3890043,
        size.height * 0.8967362,
        size.width * 0.3905920,
        size.height * 0.8896074);
    path_1.cubicTo(
        size.width * 0.3921853,
        size.height * 0.8824417,
        size.width * 0.3953699,
        size.height * 0.8772147,
        size.width * 0.4001448,
        size.height * 0.8739080);
    path_1.close();
    path_1.moveTo(size.width * 0.4152534, size.height * 0.8786994);
    path_1.cubicTo(
        size.width * 0.4116018,
        size.height * 0.8778834,
        size.width * 0.4084098,
        size.height * 0.8786135,
        size.width * 0.4056761,
        size.height * 0.8808896);
    path_1.cubicTo(
        size.width * 0.4029423,
        size.height * 0.8831595,
        size.width * 0.4010276,
        size.height * 0.8867607,
        size.width * 0.3999313,
        size.height * 0.8916810);
    path_1.cubicTo(
        size.width * 0.3988417,
        size.height * 0.8965828,
        size.width * 0.3990669,
        size.height * 0.9006380,
        size.width * 0.4006067,
        size.height * 0.9038650);
    path_1.cubicTo(
        size.width * 0.4021184,
        size.height * 0.9070798,
        size.width * 0.4046853,
        size.height * 0.9090920,
        size.width * 0.4083080,
        size.height * 0.9099018);
    path_1.cubicTo(
        size.width * 0.4119307,
        size.height * 0.9107055,
        size.width * 0.4151227,
        size.height * 0.9099755,
        size.width * 0.4178847,
        size.height * 0.9077117);
    path_1.cubicTo(
        size.width * 0.4206184,
        size.height * 0.9054356,
        size.width * 0.4225307,
        size.height * 0.9018528,
        size.width * 0.4236202,
        size.height * 0.8969571);
    path_1.cubicTo(
        size.width * 0.4247166,
        size.height * 0.8920307,
        size.width * 0.4245086,
        size.height * 0.8879632,
        size.width * 0.4229969,
        size.height * 0.8847423);
    path_1.cubicTo(
        size.width * 0.4214571,
        size.height * 0.8815215,
        size.width * 0.4188755,
        size.height * 0.8795031,
        size.width * 0.4152534,
        size.height * 0.8786994);
    path_1.close();

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_1, paint_1_fill);

    Path path_2 = Path();
    path_2.moveTo(size.width * 0.3441706, size.height * 0.8823252);
    path_2.lineTo(size.width * 0.3527092, size.height * 0.8853129);
    path_2.cubicTo(
        size.width * 0.3525650,
        size.height * 0.8876564,
        size.width * 0.3531067,
        size.height * 0.8897975,
        size.width * 0.3543344,
        size.height * 0.8917301);
    path_2.cubicTo(
        size.width * 0.3555626,
        size.height * 0.8936687,
        size.width * 0.3574902,
        size.height * 0.8950920,
        size.width * 0.3601178,
        size.height * 0.8960123);
    path_2.cubicTo(
        size.width * 0.3635387,
        size.height * 0.8972086,
        size.width * 0.3667681,
        size.height * 0.8968037,
        size.width * 0.3698049,
        size.height * 0.8947914);
    path_2.cubicTo(
        size.width * 0.3728055,
        size.height * 0.8927975,
        size.width * 0.3751000,
        size.height * 0.8895337,
        size.width * 0.3766890,
        size.height * 0.8849877);
    path_2.cubicTo(
        size.width * 0.3783160,
        size.height * 0.8803374,
        size.width * 0.3785724,
        size.height * 0.8763129,
        size.width * 0.3774577,
        size.height * 0.8729080);
    path_2.cubicTo(
        size.width * 0.3763055,
        size.height * 0.8695276,
        size.width * 0.3739920,
        size.height * 0.8672270,
        size.width * 0.3705160,
        size.height * 0.8660123);
    path_2.cubicTo(
        size.width * 0.3679160,
        size.height * 0.8651043,
        size.width * 0.3655521,
        size.height * 0.8649693,
        size.width * 0.3634245,
        size.height * 0.8656074);
    path_2.cubicTo(
        size.width * 0.3612877,
        size.height * 0.8662699,
        size.width * 0.3595067,
        size.height * 0.8676319,
        size.width * 0.3580828,
        size.height * 0.8696810);
    path_2.lineTo(size.width * 0.3495436, size.height * 0.8666933);
    path_2.cubicTo(
        size.width * 0.3517049,
        size.height * 0.8625399,
        size.width * 0.3549374,
        size.height * 0.8597055,
        size.width * 0.3592417,
        size.height * 0.8582025);
    path_2.cubicTo(
        size.width * 0.3635092,
        size.height * 0.8567117,
        size.width * 0.3681607,
        size.height * 0.8568528,
        size.width * 0.3731969,
        size.height * 0.8586135);
    path_2.cubicTo(
        size.width * 0.3794098,
        size.height * 0.8607853,
        size.width * 0.3836466,
        size.height * 0.8645215,
        size.width * 0.3859067,
        size.height * 0.8698282);
    path_2.cubicTo(
        size.width * 0.3881675,
        size.height * 0.8751350,
        size.width * 0.3880914,
        size.height * 0.8812393,
        size.width * 0.3856798,
        size.height * 0.8881350);
    path_2.cubicTo(
        size.width * 0.3833055,
        size.height * 0.8949202,
        size.width * 0.3795767,
        size.height * 0.8996994,
        size.width * 0.3744926,
        size.height * 0.9024663);
    path_2.cubicTo(
        size.width * 0.3693718,
        size.height * 0.9052515,
        size.width * 0.3637460,
        size.height * 0.9055767,
        size.width * 0.3576153,
        size.height * 0.9034294);
    path_2.cubicTo(
        size.width * 0.3524975,
        size.height * 0.9016380,
        size.width * 0.3487847,
        size.height * 0.8987730,
        size.width * 0.3464761,
        size.height * 0.8948344);
    path_2.cubicTo(
        size.width * 0.3441313,
        size.height * 0.8909141,
        size.width * 0.3433626,
        size.height * 0.8867423,
        size.width * 0.3441706,
        size.height * 0.8823252);
    path_2.close();

    Paint paint_2_fill = Paint()..style = PaintingStyle.fill;
    paint_2_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_2, paint_2_fill);

    Path path_3 = Path();
    path_3.moveTo(size.width * 0.2990890, size.height * 0.8772393);
    path_3.lineTo(size.width * 0.3022890, size.height * 0.8705276);
    path_3.lineTo(size.width * 0.3152448, size.height * 0.8767055);
    path_3.lineTo(size.width * 0.3317331, size.height * 0.8421166);
    path_3.lineTo(size.width * 0.3401736, size.height * 0.8461411);
    path_3.lineTo(size.width * 0.3236853, size.height * 0.8807239);
    path_3.lineTo(size.width * 0.3366411, size.height * 0.8869018);
    path_3.lineTo(size.width * 0.3334405, size.height * 0.8936135);
    path_3.lineTo(size.width * 0.2990890, size.height * 0.8772393);
    path_3.close();

    Paint paint_3_fill = Paint()..style = PaintingStyle.fill;
    paint_3_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_3, paint_3_fill);

    Path path_4 = Path();
    path_4.moveTo(size.width * 0.2979620, size.height * 0.8310982);
    path_4.cubicTo(
        size.width * 0.2951472,
        size.height * 0.8293620,
        size.width * 0.2922650,
        size.height * 0.8287607,
        size.width * 0.2893147,
        size.height * 0.8292945);
    path_4.cubicTo(
        size.width * 0.2863252,
        size.height * 0.8298405,
        size.width * 0.2840546,
        size.height * 0.8313681,
        size.width * 0.2825037,
        size.height * 0.8338896);
    path_4.lineTo(size.width * 0.2806110, size.height * 0.8369632);
    path_4.lineTo(size.width * 0.2905675, size.height * 0.8422761);
    path_4.cubicTo(
        size.width * 0.2930687,
        size.height * 0.8435767,
        size.width * 0.2952853,
        size.height * 0.8441227,
        size.width * 0.2972172,
        size.height * 0.8439202);
    path_4.cubicTo(
        size.width * 0.2991092,
        size.height * 0.8437239,
        size.width * 0.3005718,
        size.height * 0.8427853,
        size.width * 0.3016055,
        size.height * 0.8411043);
    path_4.cubicTo(
        size.width * 0.3026546,
        size.height * 0.8393988,
        size.width * 0.3028564,
        size.height * 0.8376380,
        size.width * 0.3022117,
        size.height * 0.8358098);
    path_4.cubicTo(
        size.width * 0.3015264,
        size.height * 0.8339877,
        size.width * 0.3001098,
        size.height * 0.8324172,
        size.width * 0.2979620,
        size.height * 0.8310982);
    path_4.close();
    path_4.moveTo(size.width * 0.3040834, size.height * 0.8261350);
    path_4.cubicTo(
        size.width * 0.3079104,
        size.height * 0.8284908,
        size.width * 0.3103644,
        size.height * 0.8314969,
        size.width * 0.3114460,
        size.height * 0.8351595);
    path_4.cubicTo(
        size.width * 0.3125031,
        size.height * 0.8388098,
        size.width * 0.3119595,
        size.height * 0.8423742,
        size.width * 0.3098160,
        size.height * 0.8458528);
    path_4.cubicTo(
        size.width * 0.3077178,
        size.height * 0.8492638,
        size.width * 0.3047939,
        size.height * 0.8511902,
        size.width * 0.3010442,
        size.height * 0.8516380);
    path_4.cubicTo(
        size.width * 0.2972791,
        size.height * 0.8521104,
        size.width * 0.2930123,
        size.height * 0.8510491,
        size.width * 0.2882436,
        size.height * 0.8484540);
    path_4.lineTo(size.width * 0.2771902, size.height * 0.8425153);
    path_4.lineTo(size.width * 0.2753429, size.height * 0.8455153);
    path_4.cubicTo(
        size.width * 0.2740049,
        size.height * 0.8476871,
        size.width * 0.2736546,
        size.height * 0.8498037,
        size.width * 0.2742926,
        size.height * 0.8518650);
    path_4.cubicTo(
        size.width * 0.2749147,
        size.height * 0.8539509,
        size.width * 0.2764479,
        size.height * 0.8557485,
        size.width * 0.2788920,
        size.height * 0.8572515);
    path_4.cubicTo(
        size.width * 0.2811638,
        size.height * 0.8586503,
        size.width * 0.2833270,
        size.height * 0.8592883,
        size.width * 0.2853822,
        size.height * 0.8591534);
    path_4.cubicTo(
        size.width * 0.2874129,
        size.height * 0.8590123,
        size.width * 0.2891607,
        size.height * 0.8581595,
        size.width * 0.2906258,
        size.height * 0.8566135);
    path_4.lineTo(size.width * 0.2981804, size.height * 0.8612638);
    path_4.cubicTo(
        size.width * 0.2957380,
        size.height * 0.8647301,
        size.width * 0.2923736,
        size.height * 0.8667117,
        size.width * 0.2880865,
        size.height * 0.8672025);
    path_4.cubicTo(
        size.width * 0.2837988,
        size.height * 0.8676994,
        size.width * 0.2792975,
        size.height * 0.8664908,
        size.width * 0.2745822,
        size.height * 0.8635890);
    path_4.cubicTo(
        size.width * 0.2698669,
        size.height * 0.8606871,
        size.width * 0.2668282,
        size.height * 0.8572147,
        size.width * 0.2654681,
        size.height * 0.8531718);
    path_4.cubicTo(
        size.width * 0.2640822,
        size.height * 0.8491227,
        size.width * 0.2645908,
        size.height * 0.8451411,
        size.width * 0.2669933,
        size.height * 0.8412393);
    path_4.lineTo(size.width * 0.2835270, size.height * 0.8143926);
    path_4.lineTo(size.width * 0.2911933, size.height * 0.8191166);
    path_4.lineTo(size.width * 0.2872479, size.height * 0.8255215);
    path_4.lineTo(size.width * 0.2874331, size.height * 0.8256319);
    path_4.cubicTo(
        size.width * 0.2898663,
        size.height * 0.8241718,
        size.width * 0.2926141,
        size.height * 0.8234969,
        size.width * 0.2956761,
        size.height * 0.8236135);
    path_4.cubicTo(
        size.width * 0.2987135,
        size.height * 0.8237117,
        size.width * 0.3015160,
        size.height * 0.8245521,
        size.width * 0.3040834,
        size.height * 0.8261350);
    path_4.close();

    Paint paint_4_fill = Paint()..style = PaintingStyle.fill;
    paint_4_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_4, paint_4_fill);

    Path path_5 = Path();
    path_5.moveTo(size.width * 0.2391816, size.height * 0.8293742);
    path_5.lineTo(size.width * 0.2467215, size.height * 0.8353374);
    path_5.lineTo(size.width * 0.2541663, size.height * 0.8259202);
    path_5.lineTo(size.width * 0.2473429, size.height * 0.8205276);
    path_5.cubicTo(
        size.width * 0.2431810,
        size.height * 0.8172331,
        size.width * 0.2398411,
        size.height * 0.8171840,
        size.width * 0.2373233,
        size.height * 0.8203681);
    path_5.cubicTo(
        size.width * 0.2361546,
        size.height * 0.8218466,
        size.width * 0.2357331,
        size.height * 0.8233558,
        size.width * 0.2360589,
        size.height * 0.8249080);
    path_5.cubicTo(
        size.width * 0.2363669,
        size.height * 0.8264847,
        size.width * 0.2374074,
        size.height * 0.8279693,
        size.width * 0.2391816,
        size.height * 0.8293742);
    path_5.close();
    path_5.moveTo(size.width * 0.2509387, size.height * 0.8149939);
    path_5.lineTo(size.width * 0.2582393, size.height * 0.8207669);
    path_5.lineTo(size.width * 0.2666282, size.height * 0.8101595);
    path_5.lineTo(size.width * 0.2585086, size.height * 0.8037362);
    path_5.cubicTo(
        size.width * 0.2564166,
        size.height * 0.8020859,
        size.width * 0.2544736,
        size.height * 0.8012699,
        size.width * 0.2526804,
        size.height * 0.8012945);
    path_5.cubicTo(
        size.width * 0.2508693,
        size.height * 0.8013374,
        size.width * 0.2492804,
        size.height * 0.8022270,
        size.width * 0.2479141,
        size.height * 0.8039571);
    path_5.cubicTo(
        size.width * 0.2451086,
        size.height * 0.8075031,
        size.width * 0.2461166,
        size.height * 0.8111840,
        size.width * 0.2509387,
        size.height * 0.8149939);
    path_5.close();
    path_5.moveTo(size.width * 0.2781301, size.height * 0.8105460);
    path_5.lineTo(size.width * 0.2497534, size.height * 0.8464356);
    path_5.lineTo(size.width * 0.2334454, size.height * 0.8335460);
    path_5.cubicTo(
        size.width * 0.2299429,
        size.height * 0.8307730,
        size.width * 0.2278497,
        size.height * 0.8277914,
        size.width * 0.2271656,
        size.height * 0.8245890);
    path_5.cubicTo(
        size.width * 0.2264411,
        size.height * 0.8213926,
        size.width * 0.2272025,
        size.height * 0.8183681,
        size.width * 0.2294503,
        size.height * 0.8155276);
    path_5.cubicTo(
        size.width * 0.2309252,
        size.height * 0.8136626,
        size.width * 0.2328472,
        size.height * 0.8124479,
        size.width * 0.2352166,
        size.height * 0.8118773);
    path_5.cubicTo(
        size.width * 0.2375454,
        size.height * 0.8113190,
        size.width * 0.2397000,
        size.height * 0.8115644,
        size.width * 0.2416804,
        size.height * 0.8126135);
    path_5.lineTo(size.width * 0.2418963, size.height * 0.8123374);
    path_5.cubicTo(
        size.width * 0.2398282,
        size.height * 0.8101840,
        size.width * 0.2386914,
        size.height * 0.8077914,
        size.width * 0.2384859,
        size.height * 0.8051534);
    path_5.cubicTo(
        size.width * 0.2382626,
        size.height * 0.8025337,
        size.width * 0.2390411,
        size.height * 0.8000982,
        size.width * 0.2408215,
        size.height * 0.7978466);
    path_5.cubicTo(
        size.width * 0.2433215,
        size.height * 0.7946871,
        size.width * 0.2464454,
        size.height * 0.7930736,
        size.width * 0.2501945,
        size.height * 0.7930061);
    path_5.cubicTo(
        size.width * 0.2539209,
        size.height * 0.7929202,
        size.width * 0.2577288,
        size.height * 0.7944172,
        size.width * 0.2616178,
        size.height * 0.7974908);
    path_5.lineTo(size.width * 0.2781301, size.height * 0.8105460);
    path_5.close();

    Paint paint_5_fill = Paint()..style = PaintingStyle.fill;
    paint_5_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_5, paint_5_fill);

    Path path_6 = Path();
    path_6.moveTo(size.width * 0.2139110, size.height * 0.7873681);
    path_6.lineTo(size.width * 0.2160706, size.height * 0.7553190);
    path_6.lineTo(size.width * 0.2244350, size.height * 0.7636196);
    path_6.lineTo(size.width * 0.2224442, size.height * 0.7913681);
    path_6.lineTo(size.width * 0.2226914, size.height * 0.7916135);
    path_6.lineTo(size.width * 0.2375521, size.height * 0.7766442);
    path_6.lineTo(size.width * 0.2441883, size.height * 0.7832331);
    path_6.lineTo(size.width * 0.2119534, size.height * 0.8156994);
    path_6.lineTo(size.width * 0.2053178, size.height * 0.8091166);
    path_6.lineTo(size.width * 0.2191061, size.height * 0.7952270);
    path_6.lineTo(size.width * 0.2188595, size.height * 0.7949816);
    path_6.lineTo(size.width * 0.1923546, size.height * 0.7962454);
    path_6.lineTo(size.width * 0.1843914, size.height * 0.7883374);
    path_6.lineTo(size.width * 0.2139110, size.height * 0.7873681);
    path_6.close();

    Paint paint_6_fill = Paint()..style = PaintingStyle.fill;
    paint_6_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_6, paint_6_fill);

    Path path_7 = Path();
    path_7.moveTo(size.width * 0.1983521, size.height * 0.7439571);
    path_7.cubicTo(
        size.width * 0.1962834,
        size.height * 0.7413804,
        size.width * 0.1937699,
        size.height * 0.7398466,
        size.width * 0.1908123,
        size.height * 0.7393620);
    path_7.cubicTo(
        size.width * 0.1878141,
        size.height * 0.7388650,
        size.width * 0.1851613,
        size.height * 0.7395460,
        size.width * 0.1828546,
        size.height * 0.7413988);
    path_7.lineTo(size.width * 0.1800399, size.height * 0.7436564);
    path_7.lineTo(size.width * 0.1876331, size.height * 0.7520061);
    path_7.cubicTo(
        size.width * 0.1895515,
        size.height * 0.7540736,
        size.width * 0.1914552,
        size.height * 0.7553313,
        size.width * 0.1933442,
        size.height * 0.7557853);
    path_7.cubicTo(
        size.width * 0.1951926,
        size.height * 0.7562331,
        size.width * 0.1968853,
        size.height * 0.7558466,
        size.width * 0.1984233,
        size.height * 0.7546074);
    path_7.cubicTo(
        size.width * 0.1999834,
        size.height * 0.7533558,
        size.width * 0.2007663,
        size.height * 0.7517607,
        size.width * 0.2007724,
        size.height * 0.7498221);
    path_7.cubicTo(
        size.width * 0.2007380,
        size.height * 0.7478834,
        size.width * 0.1999307,
        size.height * 0.7459264,
        size.width * 0.1983521,
        size.height * 0.7439571);
    path_7.close();
    path_7.moveTo(size.width * 0.2057853, size.height * 0.7413374);
    path_7.cubicTo(
        size.width * 0.2085988,
        size.height * 0.7448466,
        size.width * 0.2098994,
        size.height * 0.7485031,
        size.width * 0.2096877,
        size.height * 0.7523190);
    path_7.cubicTo(
        size.width * 0.2094583,
        size.height * 0.7561043,
        size.width * 0.2077497,
        size.height * 0.7592822,
        size.width * 0.2045613,
        size.height * 0.7618405);
    path_7.cubicTo(
        size.width * 0.2014405,
        size.height * 0.7643436,
        size.width * 0.1980387,
        size.height * 0.7651779,
        size.width * 0.1943564,
        size.height * 0.7643436);
    path_7.cubicTo(
        size.width * 0.1906509,
        size.height * 0.7635215,
        size.width * 0.1869883,
        size.height * 0.7610920,
        size.width * 0.1833687,
        size.height * 0.7570429);
    path_7.lineTo(size.width * 0.1749515, size.height * 0.7477362);
    path_7.lineTo(size.width * 0.1722043, size.height * 0.7499448);
    path_7.cubicTo(
        size.width * 0.1702147,
        size.height * 0.7515399,
        size.width * 0.1691736,
        size.height * 0.7534172,
        size.width * 0.1690822,
        size.height * 0.7555706);
    path_7.cubicTo(
        size.width * 0.1689675,
        size.height * 0.7577485,
        size.width * 0.1698086,
        size.height * 0.7599509,
        size.width * 0.1716055,
        size.height * 0.7621902);
    path_7.cubicTo(
        size.width * 0.1732748,
        size.height * 0.7642699,
        size.width * 0.1751000,
        size.height * 0.7655951,
        size.width * 0.1770798,
        size.height * 0.7661656);
    path_7.cubicTo(
        size.width * 0.1790411,
        size.height * 0.7667055,
        size.width * 0.1809718,
        size.height * 0.7664969,
        size.width * 0.1828724,
        size.height * 0.7655276);
    path_7.lineTo(size.width * 0.1884258, size.height * 0.7724479);
    path_7.cubicTo(
        size.width * 0.1849607,
        size.height * 0.7748957,
        size.width * 0.1811264,
        size.height * 0.7756319,
        size.width * 0.1769227,
        size.height * 0.7746503);
    path_7.cubicTo(
        size.width * 0.1727190,
        size.height * 0.7736748,
        size.width * 0.1688840,
        size.height * 0.7710307,
        size.width * 0.1654178,
        size.height * 0.7667117);
    path_7.cubicTo(
        size.width * 0.1619515,
        size.height * 0.7623926,
        size.width * 0.1602558,
        size.height * 0.7581043,
        size.width * 0.1603307,
        size.height * 0.7538405);
    path_7.cubicTo(
        size.width * 0.1603877,
        size.height * 0.7495583,
        size.width * 0.1622025,
        size.height * 0.7459816,
        size.width * 0.1657748,
        size.height * 0.7431166);
    path_7.lineTo(size.width * 0.1903663, size.height * 0.7233804);
    path_7.lineTo(size.width * 0.1960012, size.height * 0.7303988);
    path_7.lineTo(size.width * 0.1901331, size.height * 0.7351104);
    path_7.lineTo(size.width * 0.1902693, size.height * 0.7352761);
    path_7.cubicTo(
        size.width * 0.1930534,
        size.height * 0.7347178,
        size.width * 0.1958681,
        size.height * 0.7350061,
        size.width * 0.1987135,
        size.height * 0.7361411);
    path_7.cubicTo(
        size.width * 0.2015405,
        size.height * 0.7372577,
        size.width * 0.2038982,
        size.height * 0.7389877,
        size.width * 0.2057853,
        size.height * 0.7413374);
    path_7.close();

    Paint paint_7_fill = Paint()..style = PaintingStyle.fill;
    paint_7_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_7, paint_7_fill);

    Path path_8 = Path();
    path_8.moveTo(size.width * 0.1099890, size.height * 0.6126485);
    path_8.cubicTo(
        size.width * 0.1138080,
        size.height * 0.6118994,
        size.width * 0.1169589,
        size.height * 0.6115184,
        size.width * 0.1194417,
        size.height * 0.6115049);
    path_8.cubicTo(
        size.width * 0.1218957,
        size.height * 0.6114988,
        size.width * 0.1241006,
        size.height * 0.6119761,
        size.width * 0.1260571,
        size.height * 0.6129374);
    path_8.lineTo(size.width * 0.1263939, size.height * 0.6128503);
    path_8.lineTo(size.width * 0.1192018, size.height * 0.5850613);
    path_8.lineTo(size.width * 0.07473926, size.height * 0.5965687);
    path_8.lineTo(size.width * 0.08037301, size.height * 0.6183374);
    path_8.lineTo(size.width * 0.1099890, size.height * 0.6126485);
    path_8.close();
    path_8.moveTo(size.width * 0.1362834, size.height * 0.6173865);
    path_8.lineTo(size.width * 0.1482834, size.height * 0.6142822);
    path_8.lineTo(size.width * 0.1505282, size.height * 0.6229571);
    path_8.lineTo(size.width * 0.1303601, size.height * 0.6281779);
    path_8.lineTo(size.width * 0.1292160, size.height * 0.6237546);
    path_8.cubicTo(
        size.width * 0.1274712,
        size.height * 0.6219877,
        size.width * 0.1252160,
        size.height * 0.6209693,
        size.width * 0.1224503,
        size.height * 0.6206994);
    path_8.cubicTo(
        size.width * 0.1196767,
        size.height * 0.6203988,
        size.width * 0.1161380,
        size.height * 0.6206687,
        size.width * 0.1118344,
        size.height * 0.6215153);
    path_8.lineTo(size.width * 0.07441534, size.height * 0.6288160);
    path_8.lineTo(size.width * 0.06417239, size.height * 0.5892399);
    path_8.lineTo(size.width * 0.1167607, size.height * 0.5756301);
    path_8.lineTo(size.width * 0.1147994, size.height * 0.5680509);
    path_8.lineTo(size.width * 0.1349675, size.height * 0.5628313);
    path_8.lineTo(size.width * 0.1372012, size.height * 0.5714626);
    path_8.lineTo(size.width * 0.1252012, size.height * 0.5745687);
    path_8.lineTo(size.width * 0.1362834, size.height * 0.6173865);
    path_8.close();

    Paint paint_8_fill = Paint()..style = PaintingStyle.fill;
    paint_8_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_8, paint_8_fill);

    Path path_9 = Path();
    path_9.moveTo(size.width * 0.1136387, size.height * 0.5275859);
    path_9.cubicTo(
        size.width * 0.1182859,
        size.height * 0.5310319,
        size.width * 0.1209761,
        size.height * 0.5360258,
        size.width * 0.1217104,
        size.height * 0.5425663);
    path_9.cubicTo(
        size.width * 0.1224442,
        size.height * 0.5491067,
        size.width * 0.1209282,
        size.height * 0.5545730,
        size.width * 0.1171613,
        size.height * 0.5589638);
    path_9.cubicTo(
        size.width * 0.1133656,
        size.height * 0.5633577,
        size.width * 0.1078227,
        size.height * 0.5659638,
        size.width * 0.1005331,
        size.height * 0.5667822);
    path_9.cubicTo(
        size.width * 0.09327178,
        size.height * 0.5675975,
        size.width * 0.08731779,
        size.height * 0.5662816,
        size.width * 0.08267117,
        size.height * 0.5628350);
    path_9.cubicTo(
        size.width * 0.07799571,
        size.height * 0.5593920,
        size.width * 0.07529080,
        size.height * 0.5544000,
        size.width * 0.07455706,
        size.height * 0.5478595);
    path_9.cubicTo(
        size.width * 0.07382270,
        size.height * 0.5413190,
        size.width * 0.07535337,
        size.height * 0.5358515,
        size.width * 0.07914908,
        size.height * 0.5314571);
    path_9.cubicTo(
        size.width * 0.08291595,
        size.height * 0.5270663,
        size.width * 0.08843006,
        size.height * 0.5244632,
        size.width * 0.09569080,
        size.height * 0.5236485);
    path_9.cubicTo(
        size.width * 0.1029810,
        size.height * 0.5228301,
        size.width * 0.1089632,
        size.height * 0.5241423,
        size.width * 0.1136387,
        size.height * 0.5275859);
    path_9.close();
    path_9.moveTo(size.width * 0.1140172, size.height * 0.5434301);
    path_9.cubicTo(
        size.width * 0.1136000,
        size.height * 0.5397129,
        size.width * 0.1118724,
        size.height * 0.5369307,
        size.width * 0.1088350,
        size.height * 0.5350834);
    path_9.cubicTo(
        size.width * 0.1057975,
        size.height * 0.5332362,
        size.width * 0.1017718,
        size.height * 0.5325939,
        size.width * 0.09675828,
        size.height * 0.5331571);
    path_9.cubicTo(
        size.width * 0.09177362,
        size.height * 0.5337166,
        size.width * 0.08800675,
        size.height * 0.5352479,
        size.width * 0.08545767,
        size.height * 0.5377515);
    path_9.cubicTo(
        size.width * 0.08290552,
        size.height * 0.5402264,
        size.width * 0.08183620,
        size.height * 0.5433080,
        size.width * 0.08225031,
        size.height * 0.5469957);
    path_9.cubicTo(
        size.width * 0.08266442,
        size.height * 0.5506840,
        size.width * 0.08439141,
        size.height * 0.5534663,
        size.width * 0.08743252,
        size.height * 0.5553423);
    path_9.cubicTo(
        size.width * 0.09046994,
        size.height * 0.5571896,
        size.width * 0.09448098,
        size.height * 0.5578331,
        size.width * 0.09946564,
        size.height * 0.5572736);
    path_9.cubicTo(
        size.width * 0.1044791,
        size.height * 0.5567110,
        size.width * 0.1082620,
        size.height * 0.5551920,
        size.width * 0.1108141,
        size.height * 0.5527172);
    path_9.cubicTo(
        size.width * 0.1133638,
        size.height * 0.5502141,
        size.width * 0.1144313,
        size.height * 0.5471178,
        size.width * 0.1140172,
        size.height * 0.5434301);
    path_9.close();

    Paint paint_9_fill = Paint()..style = PaintingStyle.fill;
    paint_9_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_9, paint_9_fill);

    Path path_10 = Path();
    path_10.moveTo(size.width * 0.08752822, size.height * 0.4759000);
    path_10.lineTo(size.width * 0.08745828, size.height * 0.4849460);
    path_10.cubicTo(
        size.width * 0.08519202,
        size.height * 0.4855663,
        size.width * 0.08334172,
        size.height * 0.4867699,
        size.width * 0.08190675,
        size.height * 0.4885564);
    path_10.cubicTo(
        size.width * 0.08047239,
        size.height * 0.4903429,
        size.width * 0.07974417,
        size.height * 0.4926282,
        size.width * 0.07972270,
        size.height * 0.4954117);
    path_10.cubicTo(
        size.width * 0.07969509,
        size.height * 0.4990356,
        size.width * 0.08112209,
        size.height * 0.5019607,
        size.width * 0.08400429,
        size.height * 0.5041865);
    path_10.cubicTo(
        size.width * 0.08685828,
        size.height * 0.5063834,
        size.width * 0.09069141,
        size.height * 0.5075000,
        size.width * 0.09550429,
        size.height * 0.5075374);
    path_10.cubicTo(
        size.width * 0.1004331,
        size.height * 0.5075755,
        size.width * 0.1043270,
        size.height * 0.5065184,
        size.width * 0.1071847,
        size.height * 0.5043656);
    path_10.cubicTo(
        size.width * 0.1100141,
        size.height * 0.5021840,
        size.width * 0.1114429,
        size.height * 0.4992515,
        size.width * 0.1114718,
        size.height * 0.4955693);
    path_10.cubicTo(
        size.width * 0.1114926,
        size.height * 0.4928153,
        size.width * 0.1108583,
        size.height * 0.4905344,
        size.width * 0.1095675,
        size.height * 0.4887264);
    path_10.cubicTo(
        size.width * 0.1082472,
        size.height * 0.4869184,
        size.width * 0.1063871,
        size.height * 0.4856718,
        size.width * 0.1039853,
        size.height * 0.4849865);
    path_10.lineTo(size.width * 0.1040552, size.height * 0.4759405);
    path_10.cubicTo(
        size.width * 0.1086890,
        size.height * 0.4766429,
        size.width * 0.1124129,
        size.height * 0.4787883,
        size.width * 0.1152270,
        size.height * 0.4823767);
    path_10.cubicTo(
        size.width * 0.1180123,
        size.height * 0.4859356,
        size.width * 0.1193840,
        size.height * 0.4903828,
        size.width * 0.1193429,
        size.height * 0.4957172);
    path_10.cubicTo(
        size.width * 0.1192920,
        size.height * 0.5022988,
        size.width * 0.1171202,
        size.height * 0.5075160,
        size.width * 0.1128282,
        size.height * 0.5113681);
    path_10.cubicTo(
        size.width * 0.1085362,
        size.height * 0.5152202,
        size.width * 0.1027374,
        size.height * 0.5171184,
        size.width * 0.09543067,
        size.height * 0.5170620);
    path_10.cubicTo(
        size.width * 0.08824049,
        size.height * 0.5170061,
        size.width * 0.08251472,
        size.height * 0.5150196,
        size.width * 0.07825337,
        size.height * 0.5111012);
    path_10.cubicTo(
        size.width * 0.07396380,
        size.height * 0.5071534,
        size.width * 0.07184356,
        size.height * 0.5019325,
        size.width * 0.07189387,
        size.height * 0.4954380);
    path_10.cubicTo(
        size.width * 0.07193558,
        size.height * 0.4900160,
        size.width * 0.07344908,
        size.height * 0.4855773,
        size.width * 0.07643313,
        size.height * 0.4821209);
    path_10.cubicTo(
        size.width * 0.07938834,
        size.height * 0.4786350,
        size.width * 0.08308712,
        size.height * 0.4765613,
        size.width * 0.08752822,
        size.height * 0.4759000);
    path_10.close();

    Paint paint_10_fill = Paint()..style = PaintingStyle.fill;
    paint_10_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_10, paint_10_fill);

    Path path_11 = Path();
    path_11.moveTo(size.width * 0.07768773, size.height * 0.4314387);
    path_11.lineTo(size.width * 0.08507546, size.height * 0.4322969);
    path_11.lineTo(size.width * 0.08341963, size.height * 0.4465534);
    path_11.lineTo(size.width * 0.1214798, size.height * 0.4509736);
    path_11.lineTo(size.width * 0.1204012, size.height * 0.4602620);
    path_11.lineTo(size.width * 0.08234049, size.height * 0.4558417);
    path_11.lineTo(size.width * 0.08068466, size.height * 0.4700982);
    path_11.lineTo(size.width * 0.07329755, size.height * 0.4692399);
    path_11.lineTo(size.width * 0.07768773, size.height * 0.4314387);
    path_11.close();

    Paint paint_11_fill = Paint()..style = PaintingStyle.fill;
    paint_11_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_11, paint_11_fill);

    Path path_12 = Path();
    path_12.moveTo(size.width * 0.1207589, size.height * 0.4162270);
    path_12.cubicTo(
        size.width * 0.1214896,
        size.height * 0.4130037,
        size.width * 0.1211258,
        size.height * 0.4100822,
        size.width * 0.1196681,
        size.height * 0.4074626);
    path_12.cubicTo(
        size.width * 0.1181883,
        size.height * 0.4048086,
        size.width * 0.1160061,
        size.height * 0.4031546,
        size.width * 0.1131221,
        size.height * 0.4025006);
    path_12.lineTo(size.width * 0.1096012, size.height * 0.4017031);
    path_12.lineTo(size.width * 0.1077908, size.height * 0.4128429);
    path_12.cubicTo(
        size.width * 0.1073675,
        size.height * 0.4156307,
        size.width * 0.1075656,
        size.height * 0.4179055,
        size.width * 0.1083853,
        size.height * 0.4196669);
    path_12.cubicTo(
        size.width * 0.1091834,
        size.height * 0.4213933,
        size.width * 0.1105436,
        size.height * 0.4224748,
        size.width * 0.1124663,
        size.height * 0.4229104);
    path_12.cubicTo(
        size.width * 0.1144178,
        size.height * 0.4233521,
        size.width * 0.1161534,
        size.height * 0.4229730,
        size.width * 0.1176742,
        size.height * 0.4217712);
    path_12.cubicTo(
        size.width * 0.1191736,
        size.height * 0.4205356,
        size.width * 0.1202018,
        size.height * 0.4186871,
        size.width * 0.1207589,
        size.height * 0.4162270);
    path_12.close();
    path_12.moveTo(size.width * 0.1274356, size.height * 0.4204153);
    path_12.cubicTo(
        size.width * 0.1264423,
        size.height * 0.4247988,
        size.width * 0.1243877,
        size.height * 0.4280939,
        size.width * 0.1212718,
        size.height * 0.4303012);
    path_12.cubicTo(
        size.width * 0.1181613,
        size.height * 0.4324804,
        size.width * 0.1146129,
        size.height * 0.4331184,
        size.width * 0.1106258,
        size.height * 0.4322147);
    path_12.cubicTo(
        size.width * 0.1067233,
        size.height * 0.4313307,
        size.width * 0.1039540,
        size.height * 0.4291871,
        size.width * 0.1023172,
        size.height * 0.4257834);
    path_12.cubicTo(
        size.width * 0.1006515,
        size.height * 0.4223736,
        size.width * 0.1002767,
        size.height * 0.4179933,
        size.width * 0.1011920,
        size.height * 0.4126411);
    path_12.lineTo(size.width * 0.1032387, size.height * 0.4002613);
    path_12.lineTo(size.width * 0.09980307, size.height * 0.3994828);
    path_12.cubicTo(
        size.width * 0.09731472,
        size.height * 0.3989190,
        size.width * 0.09519816,
        size.height * 0.3992724,
        size.width * 0.09345399,
        size.height * 0.4005417);
    path_12.cubicTo(
        size.width * 0.09168098,
        size.height * 0.4018049,
        size.width * 0.09047791,
        size.height * 0.4038362,
        size.width * 0.08984356,
        size.height * 0.4066356);
    path_12.cubicTo(
        size.width * 0.08925399,
        size.height * 0.4092374,
        size.width * 0.08935337,
        size.height * 0.4114896,
        size.width * 0.09014110,
        size.height * 0.4133920);
    path_12.cubicTo(
        size.width * 0.09093497,
        size.height * 0.4152669,
        size.width * 0.09230184,
        size.height * 0.4166466,
        size.width * 0.09424172,
        size.height * 0.4175325);
    path_12.lineTo(size.width * 0.09228160, size.height * 0.4261853);
    path_12.cubicTo(
        size.width * 0.08821043,
        size.height * 0.4249951,
        size.width * 0.08524908,
        size.height * 0.4224515,
        size.width * 0.08339693,
        size.height * 0.4185534);
    path_12.cubicTo(
        size.width * 0.08154540,
        size.height * 0.4146552,
        size.width * 0.08123129,
        size.height * 0.4100061,
        size.width * 0.08245521,
        size.height * 0.4046049);
    path_12.cubicTo(
        size.width * 0.08367853,
        size.height * 0.3992037,
        size.width * 0.08598160,
        size.height * 0.3952067,
        size.width * 0.08936380,
        size.height * 0.3926135);
    path_12.cubicTo(
        size.width * 0.09275215,
        size.height * 0.3899926,
        size.width * 0.09668037,
        size.height * 0.3891877,
        size.width * 0.1011485,
        size.height * 0.3902000);
    path_12.lineTo(size.width * 0.1319006, size.height * 0.3971681);
    path_12.lineTo(size.width * 0.1299110, size.height * 0.4059485);
    path_12.lineTo(size.width * 0.1225730, size.height * 0.4042853);
    path_12.lineTo(size.width * 0.1225252, size.height * 0.4044975);
    path_12.cubicTo(
        size.width * 0.1246969,
        size.height * 0.4063276,
        size.width * 0.1262233,
        size.height * 0.4087098,
        size.width * 0.1271043,
        size.height * 0.4116448);
    path_12.cubicTo(
        size.width * 0.1279914,
        size.height * 0.4145509,
        size.width * 0.1281018,
        size.height * 0.4174748,
        size.width * 0.1274356,
        size.height * 0.4204153);
    path_12.close();

    Paint paint_12_fill = Paint()..style = PaintingStyle.fill;
    paint_12_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_12, paint_12_fill);

    Path path_13 = Path();
    path_13.moveTo(size.width * 0.1035282, size.height * 0.3591877);
    path_13.lineTo(size.width * 0.1003215, size.height * 0.3682485);
    path_13.lineTo(size.width * 0.1116374, size.height * 0.3722534);
    path_13.lineTo(size.width * 0.1145393, size.height * 0.3640540);
    path_13.cubicTo(
        size.width * 0.1163098,
        size.height * 0.3590515,
        size.width * 0.1152816,
        size.height * 0.3558736,
        size.width * 0.1114552,
        size.height * 0.3545190);
    path_13.cubicTo(
        size.width * 0.1096785,
        size.height * 0.3538902,
        size.width * 0.1081086,
        size.height * 0.3539810,
        size.width * 0.1067460,
        size.height * 0.3547902);
    path_13.cubicTo(
        size.width * 0.1053552,
        size.height * 0.3555896,
        size.width * 0.1042828,
        size.height * 0.3570558,
        size.width * 0.1035282,
        size.height * 0.3591877);
    path_13.close();
    path_13.moveTo(size.width * 0.1209337, size.height * 0.3656712);
    path_13.lineTo(size.width * 0.1178282, size.height * 0.3744448);
    path_13.lineTo(size.width * 0.1305791, size.height * 0.3789577);
    path_13.lineTo(size.width * 0.1340325, size.height * 0.3692000);
    path_13.cubicTo(
        size.width * 0.1349227,
        size.height * 0.3666853,
        size.width * 0.1350669,
        size.height * 0.3645834,
        size.width * 0.1344650,
        size.height * 0.3628945);
    path_13.cubicTo(
        size.width * 0.1338362,
        size.height * 0.3611951,
        size.width * 0.1324834,
        size.height * 0.3599779,
        size.width * 0.1304061,
        size.height * 0.3592429);
    path_13.cubicTo(
        size.width * 0.1261417,
        size.height * 0.3577337,
        size.width * 0.1229847,
        size.height * 0.3598767,
        size.width * 0.1209337,
        size.height * 0.3656712);
    path_13.close();
    path_13.moveTo(size.width * 0.1339252, size.height * 0.3899693);
    path_13.lineTo(size.width * 0.09079325, size.height * 0.3747031);
    path_13.lineTo(size.width * 0.09773006, size.height * 0.3551055);
    path_13.cubicTo(
        size.width * 0.09921963,
        size.height * 0.3508963,
        size.width * 0.1013693,
        size.height * 0.3479509,
        size.width * 0.1041791,
        size.height * 0.3462693);
    path_13.cubicTo(
        size.width * 0.1069712,
        size.height * 0.3445509,
        size.width * 0.1100755,
        size.height * 0.3442963,
        size.width * 0.1134920,
        size.height * 0.3455055);
    path_13.cubicTo(
        size.width * 0.1157331,
        size.height * 0.3462988,
        size.width * 0.1175043,
        size.height * 0.3477252,
        size.width * 0.1188055,
        size.height * 0.3497853);
    path_13.cubicTo(
        size.width * 0.1200890,
        size.height * 0.3518080,
        size.width * 0.1205540,
        size.height * 0.3539258,
        size.width * 0.1202012,
        size.height * 0.3561387);
    path_13.lineTo(size.width * 0.1205294, size.height * 0.3562546);
    path_13.cubicTo(
        size.width * 0.1218988,
        size.height * 0.3536025,
        size.width * 0.1237994,
        size.height * 0.3517528,
        size.width * 0.1262301,
        size.height * 0.3507061);
    path_13.cubicTo(
        size.width * 0.1286337,
        size.height * 0.3496503,
        size.width * 0.1311890,
        size.height * 0.3496006,
        size.width * 0.1338951,
        size.height * 0.3505589);
    path_13.cubicTo(
        size.width * 0.1376939,
        size.height * 0.3519031,
        size.width * 0.1402307,
        size.height * 0.3543393,
        size.width * 0.1415043,
        size.height * 0.3578656);
    path_13.cubicTo(
        size.width * 0.1427883,
        size.height * 0.3613650,
        size.width * 0.1426025,
        size.height * 0.3654515,
        size.width * 0.1409485,
        size.height * 0.3701252);
    path_13.lineTo(size.width * 0.1339252, size.height * 0.3899693);
    path_13.close();

    Paint paint_13_fill = Paint()..style = PaintingStyle.fill;
    paint_13_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_13, paint_13_fill);

    Path path_14 = Path();
    path_14.moveTo(size.width * 0.1353589, size.height * 0.3212564);
    path_14.lineTo(size.width * 0.1664172, size.height * 0.3130436);
    path_14.lineTo(size.width * 0.1612276, size.height * 0.3236252);
    path_14.lineTo(size.width * 0.1343031, size.height * 0.3306202);
    path_14.lineTo(size.width * 0.1341497, size.height * 0.3309325);
    path_14.lineTo(size.width * 0.1530877, size.height * 0.3402209);
    path_14.lineTo(size.width * 0.1489706, size.height * 0.3486160);
    path_14.lineTo(size.width * 0.1078920, size.height * 0.3284687);
    path_14.lineTo(size.width * 0.1120092, size.height * 0.3200736);
    path_14.lineTo(size.width * 0.1295810, size.height * 0.3286920);
    path_14.lineTo(size.width * 0.1297344, size.height * 0.3283791);
    path_14.lineTo(size.width * 0.1200528, size.height * 0.3036736);
    path_14.lineTo(size.width * 0.1249939, size.height * 0.2935988);
    path_14.lineTo(size.width * 0.1353589, size.height * 0.3212564);
    path_14.close();

    Paint paint_14_fill = Paint()..style = PaintingStyle.fill;
    paint_14_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_14, paint_14_fill);

    Path path_15 = Path();
    path_15.moveTo(size.width * 0.1715202, size.height * 0.2925791);
    path_15.cubicTo(
        size.width * 0.1733049,
        size.height * 0.2897969,
        size.width * 0.1739583,
        size.height * 0.2869264,
        size.width * 0.1734798,
        size.height * 0.2839669);
    path_15.cubicTo(
        size.width * 0.1729920,
        size.height * 0.2809675,
        size.width * 0.1715037,
        size.height * 0.2786693,
        size.width * 0.1690147,
        size.height * 0.2770724);
    path_15.lineTo(size.width * 0.1659761, size.height * 0.2751233);
    path_15.lineTo(size.width * 0.1604798, size.height * 0.2849810);
    path_15.cubicTo(
        size.width * 0.1591325,
        size.height * 0.2874577,
        size.width * 0.1585442,
        size.height * 0.2896638,
        size.width * 0.1587147,
        size.height * 0.2915994);
    path_15.cubicTo(
        size.width * 0.1588773,
        size.height * 0.2934945,
        size.width * 0.1597877,
        size.height * 0.2949742,
        size.width * 0.1614479,
        size.height * 0.2960387);
    path_15.cubicTo(
        size.width * 0.1631313,
        size.height * 0.2971190,
        size.width * 0.1648933,
        size.height * 0.2973534,
        size.width * 0.1667319,
        size.height * 0.2967417);
    path_15.cubicTo(
        size.width * 0.1685626,
        size.height * 0.2960902,
        size.width * 0.1701583,
        size.height * 0.2947025,
        size.width * 0.1715202,
        size.height * 0.2925791);
    path_15.close();
    path_15.moveTo(size.width * 0.1763718, size.height * 0.2987908);
    path_15.cubicTo(
        size.width * 0.1739454,
        size.height * 0.3025736,
        size.width * 0.1708908,
        size.height * 0.3049724,
        size.width * 0.1672092,
        size.height * 0.3059865);
    path_15.cubicTo(
        size.width * 0.1635429,
        size.height * 0.3069761,
        size.width * 0.1599896,
        size.height * 0.3063675,
        size.width * 0.1565485,
        size.height * 0.3041601);
    path_15.cubicTo(
        size.width * 0.1531804,
        size.height * 0.3020000,
        size.width * 0.1513061,
        size.height * 0.2990411,
        size.width * 0.1509264,
        size.height * 0.2952834);
    path_15.cubicTo(
        size.width * 0.1505215,
        size.height * 0.2915104,
        size.width * 0.1516613,
        size.height * 0.2872638,
        size.width * 0.1543442,
        size.height * 0.2825436);
    path_15.lineTo(size.width * 0.1604847, size.height * 0.2716012);
    path_15.lineTo(size.width * 0.1575196, size.height * 0.2696994);
    path_15.cubicTo(
        size.width * 0.1553718,
        size.height * 0.2683215,
        size.width * 0.1532620,
        size.height * 0.2679325,
        size.width * 0.1511896,
        size.height * 0.2685325);
    path_15.cubicTo(
        size.width * 0.1490926,
        size.height * 0.2691166,
        size.width * 0.1472693,
        size.height * 0.2706166,
        size.width * 0.1457190,
        size.height * 0.2730325);
    path_15.cubicTo(
        size.width * 0.1442791,
        size.height * 0.2752779,
        size.width * 0.1436055,
        size.height * 0.2774294,
        size.width * 0.1436982,
        size.height * 0.2794865);
    path_15.cubicTo(
        size.width * 0.1438061,
        size.height * 0.2815196,
        size.width * 0.1446215,
        size.height * 0.2832828,
        size.width * 0.1461442,
        size.height * 0.2847755);
    path_15.lineTo(size.width * 0.1413540, size.height * 0.2922436);
    path_15.cubicTo(
        size.width * 0.1379313,
        size.height * 0.2897387,
        size.width * 0.1360135,
        size.height * 0.2863380,
        size.width * 0.1356000,
        size.height * 0.2820423);
    path_15.cubicTo(
        size.width * 0.1351859,
        size.height * 0.2777466,
        size.width * 0.1364742,
        size.height * 0.2732681,
        size.width * 0.1394644,
        size.height * 0.2686067);
    path_15.cubicTo(
        size.width * 0.1424540,
        size.height * 0.2639454,
        size.width * 0.1459804,
        size.height * 0.2609718,
        size.width * 0.1500436,
        size.height * 0.2596853);
    path_15.cubicTo(
        size.width * 0.1541227,
        size.height * 0.2583748,
        size.width * 0.1580896,
        size.height * 0.2589558,
        size.width * 0.1619460,
        size.height * 0.2614294);
    path_15.lineTo(size.width * 0.1884865, size.height * 0.2784534);
    path_15.lineTo(size.width * 0.1836258, size.height * 0.2860313);
    path_15.lineTo(size.width * 0.1772926, size.height * 0.2819693);
    path_15.lineTo(size.width * 0.1771755, size.height * 0.2821521);
    path_15.cubicTo(
        size.width * 0.1785939,
        size.height * 0.2846123,
        size.width * 0.1792178,
        size.height * 0.2873724,
        size.width * 0.1790466,
        size.height * 0.2904313);
    path_15.cubicTo(
        size.width * 0.1788914,
        size.height * 0.2934663,
        size.width * 0.1779994,
        size.height * 0.2962528,
        size.width * 0.1763718,
        size.height * 0.2987908);
    path_15.close();

    Paint paint_15_fill = Paint()..style = PaintingStyle.fill;
    paint_15_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_15, paint_15_fill);

    Path path_16 = Path();
    path_16.moveTo(size.width * 0.2668957, size.height * 0.1671417);
    path_16.cubicTo(
        size.width * 0.2688356,
        size.height * 0.1705160,
        size.width * 0.2702117,
        size.height * 0.1733761,
        size.width * 0.2710245,
        size.height * 0.1757221);
    path_16.cubicTo(
        size.width * 0.2718209,
        size.height * 0.1780429,
        size.width * 0.2720798,
        size.height * 0.1802847,
        size.width * 0.2717994,
        size.height * 0.1824460);
    path_16.lineTo(size.width * 0.2719908, size.height * 0.1827368);
    path_16.lineTo(size.width * 0.2959804, size.height * 0.1669742);
    path_16.lineTo(size.width * 0.2707595, size.height * 0.1285908);
    path_16.lineTo(size.width * 0.2519681, size.height * 0.1409387);
    path_16.lineTo(size.width * 0.2668957, size.height * 0.1671417);
    path_16.close();
    path_16.moveTo(size.width * 0.2708810, size.height * 0.1935613);
    path_16.lineTo(size.width * 0.2776877, size.height * 0.2039209);
    path_16.lineTo(size.width * 0.2702000, size.height * 0.2088405);
    path_16.lineTo(size.width * 0.2587601, size.height * 0.1914301);
    path_16.lineTo(size.width * 0.2625767, size.height * 0.1889221);
    path_16.cubicTo(
        size.width * 0.2636853,
        size.height * 0.1867025,
        size.width * 0.2639227,
        size.height * 0.1842393,
        size.width * 0.2632896,
        size.height * 0.1815325);
    path_16.cubicTo(
        size.width * 0.2626804,
        size.height * 0.1788104,
        size.width * 0.2612828,
        size.height * 0.1755485,
        size.width * 0.2590969,
        size.height * 0.1717460);
    path_16.lineTo(size.width * 0.2401258, size.height * 0.1386761);
    path_16.lineTo(size.width * 0.2742920, size.height * 0.1162264);
    path_16.lineTo(size.width * 0.3041221, size.height * 0.1616239);
    path_16.lineTo(size.width * 0.3106644, size.height * 0.1573252);
    path_16.lineTo(size.width * 0.3221043, size.height * 0.1747356);
    path_16.lineTo(size.width * 0.3146534, size.height * 0.1796319);
    path_16.lineTo(size.width * 0.3078466, size.height * 0.1692724);
    path_16.lineTo(size.width * 0.2708810, size.height * 0.1935613);
    path_16.close();

    Paint paint_16_fill = Paint()..style = PaintingStyle.fill;
    paint_16_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_16, paint_16_fill);

    Path path_17 = Path();
    path_17.moveTo(size.width * 0.3367025, size.height * 0.1556656);
    path_17.cubicTo(
        size.width * 0.3485399,
        size.height * 0.1500767,
        size.width * 0.3524650,
        size.height * 0.1382196,
        size.width * 0.3461699,
        size.height * 0.1248871);
    path_17.lineTo(size.width * 0.3461325, size.height * 0.1248086);
    path_17.cubicTo(
        size.width * 0.3398748,
        size.height * 0.1115552,
        size.width * 0.3281656,
        size.height * 0.1071276,
        size.width * 0.3164061,
        size.height * 0.1126798);
    path_17.cubicTo(
        size.width * 0.3046472,
        size.height * 0.1182325,
        size.width * 0.3006436,
        size.height * 0.1301264,
        size.width * 0.3068828,
        size.height * 0.1433405);
    path_17.lineTo(size.width * 0.3069202, size.height * 0.1434196);
    path_17.cubicTo(
        size.width * 0.3131963,
        size.height * 0.1567123,
        size.width * 0.3248252,
        size.height * 0.1612736,
        size.width * 0.3367025,
        size.height * 0.1556656);
    path_17.close();
    path_17.moveTo(size.width * 0.3333804, size.height * 0.1485288);
    path_17.cubicTo(
        size.width * 0.3267344,
        size.height * 0.1516669,
        size.width * 0.3199380,
        size.height * 0.1484791,
        size.width * 0.3156117,
        size.height * 0.1393153);
    path_17.lineTo(size.width * 0.3155748, size.height * 0.1392368);
    path_17.cubicTo(
        size.width * 0.3113037,
        size.height * 0.1301914,
        size.width * 0.3131994,
        size.height * 0.1228994,
        size.width * 0.3197675,
        size.height * 0.1197982);
    path_17.cubicTo(
        size.width * 0.3263742,
        size.height * 0.1166791,
        size.width * 0.3332098,
        size.height * 0.1198485,
        size.width * 0.3374804,
        size.height * 0.1288939);
    path_17.lineTo(size.width * 0.3375178, size.height * 0.1289724);
    path_17.cubicTo(
        size.width * 0.3418258,
        size.height * 0.1380969,
        size.width * 0.3400270,
        size.height * 0.1453908,
        size.width * 0.3333804,
        size.height * 0.1485288);
    path_17.close();

    Paint paint_17_fill = Paint()..style = PaintingStyle.fill;
    paint_17_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_17, paint_17_fill);

    Path path_18 = Path();
    path_18.moveTo(size.width * 0.3803988, size.height * 0.1381288);
    path_18.cubicTo(
        size.width * 0.3908387,
        size.height * 0.1346534,
        size.width * 0.3958761,
        size.height * 0.1255509,
        size.width * 0.3939264,
        size.height * 0.1165276);
    path_18.lineTo(size.width * 0.3938718, size.height * 0.1162252);
    path_18.lineTo(size.width * 0.3853294, size.height * 0.1190687);
    path_18.lineTo(size.width * 0.3853571, size.height * 0.1194264);
    path_18.cubicTo(
        size.width * 0.3858503,
        size.height * 0.1244877,
        size.width * 0.3834417,
        size.height * 0.1288190,
        size.width * 0.3779534,
        size.height * 0.1306460);
    path_18.cubicTo(
        size.width * 0.3710209,
        size.height * 0.1329540,
        size.width * 0.3645859,
        size.height * 0.1289080,
        size.width * 0.3614264,
        size.height * 0.1194172);
    path_18.lineTo(size.width * 0.3614123, size.height * 0.1193761);
    path_18.cubicTo(
        size.width * 0.3583215,
        size.height * 0.1100908,
        size.width * 0.3608963,
        size.height * 0.1028166,
        size.width * 0.3678288,
        size.height * 0.1005086);
    path_18.cubicTo(
        size.width * 0.3736472,
        size.height * 0.09857178,
        size.width * 0.3781436,
        size.height * 0.1010632,
        size.width * 0.3805080,
        size.height * 0.1049975);
    path_18.lineTo(size.width * 0.3807000, size.height * 0.1053000);
    path_18.lineTo(size.width * 0.3892006, size.height * 0.1024699);
    path_18.lineTo(size.width * 0.3890632, size.height * 0.1021951);
    path_18.cubicTo(
        size.width * 0.3856006,
        size.height * 0.09427178,
        size.width * 0.3764429,
        size.height * 0.08934417,
        size.width * 0.3653012,
        size.height * 0.09305337);
    path_18.cubicTo(
        size.width * 0.3528804,
        size.height * 0.09718834,
        size.width * 0.3476908,
        size.height * 0.1085877,
        size.width * 0.3522656,
        size.height * 0.1223288);
    path_18.lineTo(size.width * 0.3522791, size.height * 0.1223706);
    path_18.cubicTo(
        size.width * 0.3569086,
        size.height * 0.1362767,
        size.width * 0.3677301,
        size.height * 0.1423460,
        size.width * 0.3803988,
        size.height * 0.1381288);
    path_18.close();

    Paint paint_18_fill = Paint()..style = PaintingStyle.fill;
    paint_18_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_18, paint_18_fill);

    Path path_19 = Path();
    path_19.moveTo(size.width * 0.4280012, size.height * 0.07794785);
    path_19.lineTo(size.width * 0.4295620, size.height * 0.08521963);
    path_19.lineTo(size.width * 0.4155288, size.height * 0.08823067);
    path_19.lineTo(size.width * 0.4235681, size.height * 0.1256945);
    path_19.lineTo(size.width * 0.4144258, size.height * 0.1276558);
    path_19.lineTo(size.width * 0.4063865, size.height * 0.09019264);
    path_19.lineTo(size.width * 0.3923534, size.height * 0.09320429);
    path_19.lineTo(size.width * 0.3907933, size.height * 0.08593252);
    path_19.lineTo(size.width * 0.4280012, size.height * 0.07794785);
    path_19.close();

    Paint paint_19_fill = Paint()..style = PaintingStyle.fill;
    paint_19_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_19, paint_19_fill);

    Path path_20 = Path();
    path_20.moveTo(size.width * 0.4559319, size.height * 0.1138798);
    path_20.cubicTo(
        size.width * 0.4592196,
        size.height * 0.1135362,
        size.width * 0.4618693,
        size.height * 0.1122540,
        size.width * 0.4638822,
        size.height * 0.1100319);
    path_20.cubicTo(
        size.width * 0.4659209,
        size.height * 0.1077785,
        size.width * 0.4667859,
        size.height * 0.1051810,
        size.width * 0.4664791,
        size.height * 0.1022393);
    path_20.lineTo(size.width * 0.4661037, size.height * 0.09864908);
    path_20.lineTo(size.width * 0.4549724, size.height * 0.1005117);
    path_20.cubicTo(
        size.width * 0.4521963,
        size.height * 0.1010055,
        size.width * 0.4501061,
        size.height * 0.1019239,
        size.width * 0.4487012,
        size.height * 0.1032656);
    path_20.cubicTo(
        size.width * 0.4473221,
        size.height * 0.1045761,
        size.width * 0.4467350,
        size.height * 0.1062117,
        size.width * 0.4469399,
        size.height * 0.1081724);
    path_20.cubicTo(
        size.width * 0.4471479,
        size.height * 0.1101626,
        size.width * 0.4480644,
        size.height * 0.1116847,
        size.width * 0.4496908,
        size.height * 0.1127393);
    path_20.cubicTo(
        size.width * 0.4513429,
        size.height * 0.1137620,
        size.width * 0.4534233,
        size.height * 0.1141417,
        size.width * 0.4559319,
        size.height * 0.1138798);
    path_20.close();
    path_20.moveTo(size.width * 0.4541092, size.height * 0.1215479);
    path_20.cubicTo(
        size.width * 0.4496393,
        size.height * 0.1220147,
        size.width * 0.4458589,
        size.height * 0.1211270,
        size.width * 0.4427681,
        size.height * 0.1188847);
    path_20.cubicTo(
        size.width * 0.4397055,
        size.height * 0.1166393,
        size.width * 0.4379620,
        size.height * 0.1134834,
        size.width * 0.4375368,
        size.height * 0.1094172);
    path_20.cubicTo(
        size.width * 0.4371215,
        size.height * 0.1054374,
        size.width * 0.4382620,
        size.height * 0.1021264,
        size.width * 0.4409595,
        size.height * 0.09948344);
    path_20.cubicTo(
        size.width * 0.4436540,
        size.height * 0.09681104,
        size.width * 0.4476822,
        size.height * 0.09504908,
        size.width * 0.4530448,
        size.height * 0.09419755);
    path_20.lineTo(size.width * 0.4654258, size.height * 0.09216074);
    path_20.lineTo(size.width * 0.4650601, size.height * 0.08865706);
    path_20.cubicTo(
        size.width * 0.4647945,
        size.height * 0.08611902,
        size.width * 0.4637810,
        size.height * 0.08422822,
        size.width * 0.4620184,
        size.height * 0.08298405);
    path_20.cubicTo(
        size.width * 0.4602528,
        size.height * 0.08171043,
        size.width * 0.4579423,
        size.height * 0.08122331,
        size.width * 0.4550877,
        size.height * 0.08152147);
    path_20.cubicTo(
        size.width * 0.4524344,
        size.height * 0.08179877,
        size.width * 0.4503337,
        size.height * 0.08261595,
        size.width * 0.4487847,
        size.height * 0.08397301);
    path_20.cubicTo(
        size.width * 0.4472644,
        size.height * 0.08532699,
        size.width * 0.4463963,
        size.height * 0.08706503,
        size.width * 0.4461810,
        size.height * 0.08918650);
    path_20.lineTo(size.width * 0.4373564, size.height * 0.09010798);
    path_20.cubicTo(
        size.width * 0.4371761,
        size.height * 0.08587055,
        size.width * 0.4386344,
        size.height * 0.08224908,
        size.width * 0.4417313,
        size.height * 0.07924356);
    path_20.cubicTo(
        size.width * 0.4448282,
        size.height * 0.07623804,
        size.width * 0.4491307,
        size.height * 0.07444785,
        size.width * 0.4546387,
        size.height * 0.07387239);
    path_20.cubicTo(
        size.width * 0.4601466,
        size.height * 0.07329693,
        size.width * 0.4646712,
        size.height * 0.07419448,
        size.width * 0.4682129,
        size.height * 0.07656442);
    path_20.cubicTo(
        size.width * 0.4717840,
        size.height * 0.07893190,
        size.width * 0.4738074,
        size.height * 0.08239387,
        size.width * 0.4742834,
        size.height * 0.08694969);
    path_20.lineTo(size.width * 0.4775595, size.height * 0.1183110);
    path_20.lineTo(size.width * 0.4686055, size.height * 0.1192460);
    path_20.lineTo(size.width * 0.4678233, size.height * 0.1117632);
    path_20.lineTo(size.width * 0.4676074, size.height * 0.1117853);
    path_20.cubicTo(
        size.width * 0.4665718,
        size.height * 0.1144301,
        size.width * 0.4648055,
        size.height * 0.1166405,
        size.width * 0.4623092,
        size.height * 0.1184172);
    path_20.cubicTo(
        size.width * 0.4598417,
        size.height * 0.1201908,
        size.width * 0.4571086,
        size.height * 0.1212344,
        size.width * 0.4541092,
        size.height * 0.1215479);
    path_20.close();

    Paint paint_20_fill = Paint()..style = PaintingStyle.fill;
    paint_20_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_20, paint_20_fill);

    Path path_21 = Path();
    path_21.moveTo(size.width * 0.5040914, size.height * 0.07917117);
    path_21.lineTo(size.width * 0.4944804, size.height * 0.07906012);
    path_21.lineTo(size.width * 0.4943417, size.height * 0.09106319);
    path_21.lineTo(size.width * 0.5030393, size.height * 0.09116380);
    path_21.cubicTo(
        size.width * 0.5083448,
        size.height * 0.09122515,
        size.width * 0.5110215,
        size.height * 0.08922638,
        size.width * 0.5110681,
        size.height * 0.08516748);
    path_21.cubicTo(
        size.width * 0.5110902,
        size.height * 0.08328282,
        size.width * 0.5104982,
        size.height * 0.08182638,
        size.width * 0.5092920,
        size.height * 0.08079755);
    path_21.cubicTo(
        size.width * 0.5080865,
        size.height * 0.07973926,
        size.width * 0.5063534,
        size.height * 0.07919755,
        size.width * 0.5040914,
        size.height * 0.07917117);
    path_21.close();
    path_21.moveTo(size.width * 0.5035724, size.height * 0.09773742);
    path_21.lineTo(size.width * 0.4942656, size.height * 0.09762945);
    path_21.lineTo(size.width * 0.4941092, size.height * 0.1111546);
    path_21.lineTo(size.width * 0.5044595, size.height * 0.1112748);
    path_21.cubicTo(
        size.width * 0.5071264,
        size.height * 0.1113055,
        size.width * 0.5091626,
        size.height * 0.1107638,
        size.width * 0.5105675,
        size.height * 0.1096491);
    path_21.cubicTo(
        size.width * 0.5119724,
        size.height * 0.1085055,
        size.width * 0.5126877,
        size.height * 0.1068319,
        size.width * 0.5127135,
        size.height * 0.1046288);
    path_21.cubicTo(
        size.width * 0.5127656,
        size.height * 0.1001055,
        size.width * 0.5097184,
        size.height * 0.09780859,
        size.width * 0.5035724,
        size.height * 0.09773742);
    path_21.close();
    path_21.moveTo(size.width * 0.4847669, size.height * 0.1178755);
    path_21.lineTo(size.width * 0.4852963, size.height * 0.07212515);
    path_21.lineTo(size.width * 0.5060840, size.height * 0.07236564);
    path_21.cubicTo(
        size.width * 0.5105491,
        size.height * 0.07241718,
        size.width * 0.5140307,
        size.height * 0.07350184,
        size.width * 0.5165288,
        size.height * 0.07561840);
    path_21.cubicTo(
        size.width * 0.5190564,
        size.height * 0.07770613,
        size.width * 0.5202988,
        size.height * 0.08056258,
        size.width * 0.5202571,
        size.height * 0.08418650);
    path_21.cubicTo(
        size.width * 0.5202294,
        size.height * 0.08656380,
        size.width * 0.5194509,
        size.height * 0.08870061,
        size.width * 0.5179209,
        size.height * 0.09059632);
    path_21.cubicTo(
        size.width * 0.5164209,
        size.height * 0.09246380,
        size.width * 0.5145663,
        size.height * 0.09358773,
        size.width * 0.5123583,
        size.height * 0.09396810);
    path_21.lineTo(size.width * 0.5123540, size.height * 0.09431595);
    path_21.cubicTo(
        size.width * 0.5153067,
        size.height * 0.09475644,
        size.width * 0.5176706,
        size.height * 0.09595828,
        size.width * 0.5194454,
        size.height * 0.09792147);
    path_21.cubicTo(
        size.width * 0.5212209,
        size.height * 0.09985583,
        size.width * 0.5220920,
        size.height * 0.1022577,
        size.width * 0.5220589,
        size.height * 0.1051282);
    path_21.cubicTo(
        size.width * 0.5220123,
        size.height * 0.1091583,
        size.width * 0.5205252,
        size.height * 0.1123448,
        size.width * 0.5175988,
        size.height * 0.1146890);
    path_21.cubicTo(
        size.width * 0.5147006,
        size.height * 0.1170331,
        size.width * 0.5107730,
        size.height * 0.1181767,
        size.width * 0.5058153,
        size.height * 0.1181190);
    path_21.lineTo(size.width * 0.4847669, size.height * 0.1178755);
    path_21.close();

    Paint paint_21_fill = Paint()..style = PaintingStyle.fill;
    paint_21_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_21, paint_21_fill);

    Path path_22 = Path();
    path_22.moveTo(size.width * 0.5517534, size.height * 0.09708528);
    path_22.lineTo(size.width * 0.5695558, size.height * 0.1238270);
    path_22.lineTo(size.width * 0.5578644, size.height * 0.1223325);
    path_22.lineTo(size.width * 0.5425503, size.height * 0.09910920);
    path_22.lineTo(size.width * 0.5422049, size.height * 0.09906503);
    path_22.lineTo(size.width * 0.5395301, size.height * 0.1199883);
    path_22.lineTo(size.width * 0.5302546, size.height * 0.1188025);
    path_22.lineTo(size.width * 0.5360571, size.height * 0.07341840);
    path_22.lineTo(size.width * 0.5453319, size.height * 0.07460429);
    path_22.lineTo(size.width * 0.5428503, size.height * 0.09401779);
    path_22.lineTo(size.width * 0.5431951, size.height * 0.09406196);
    path_22.lineTo(size.width * 0.5634515, size.height * 0.07692086);
    path_22.lineTo(size.width * 0.5745816, size.height * 0.07834356);
    path_22.lineTo(size.width * 0.5517534, size.height * 0.09708528);
    path_22.close();

    Paint paint_22_fill = Paint()..style = PaintingStyle.fill;
    paint_22_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_22, paint_22_fill);

    Path path_23 = Path();
    path_23.moveTo(size.width * 0.5906086, size.height * 0.1220534);
    path_23.cubicTo(
        size.width * 0.5938178,
        size.height * 0.1228448,
        size.width * 0.5967460,
        size.height * 0.1225368,
        size.width * 0.5993926,
        size.height * 0.1211288);
    path_23.cubicTo(
        size.width * 0.6020748,
        size.height * 0.1197000,
        size.width * 0.6037699,
        size.height * 0.1175503,
        size.width * 0.6044779,
        size.height * 0.1146785);
    path_23.lineTo(size.width * 0.6053423, size.height * 0.1111742);
    path_23.lineTo(size.width * 0.5942393, size.height * 0.1091515);
    path_23.cubicTo(
        size.width * 0.5914601,
        size.height * 0.1086755,
        size.width * 0.5891822,
        size.height * 0.1088301,
        size.width * 0.5874055,
        size.height * 0.1096160);
    path_23.cubicTo(
        size.width * 0.5856638,
        size.height * 0.1103810,
        size.width * 0.5845571,
        size.height * 0.1117209,
        size.width * 0.5840847,
        size.height * 0.1136350);
    path_23.cubicTo(
        size.width * 0.5836055,
        size.height * 0.1155773,
        size.width * 0.5839521,
        size.height * 0.1173202,
        size.width * 0.5851245,
        size.height * 0.1188638);
    path_23.cubicTo(
        size.width * 0.5863313,
        size.height * 0.1203859,
        size.width * 0.5881595,
        size.height * 0.1214491,
        size.width * 0.5906086,
        size.height * 0.1220534);
    path_23.close();
    path_23.moveTo(size.width * 0.5862939, size.height * 0.1286491);
    path_23.cubicTo(
        size.width * 0.5819307,
        size.height * 0.1275724,
        size.width * 0.5786755,
        size.height * 0.1254552,
        size.width * 0.5765276,
        size.height * 0.1222975);
    path_23.cubicTo(
        size.width * 0.5744080,
        size.height * 0.1191466,
        size.width * 0.5738380,
        size.height * 0.1155865,
        size.width * 0.5748172,
        size.height * 0.1116178);
    path_23.cubicTo(
        size.width * 0.5757755,
        size.height * 0.1077325,
        size.width * 0.5779712,
        size.height * 0.1050043,
        size.width * 0.5814049,
        size.height * 0.1034325);
    path_23.cubicTo(
        size.width * 0.5848460,
        size.height * 0.1018325,
        size.width * 0.5892331,
        size.height * 0.1015411,
        size.width * 0.5945669,
        size.height * 0.1025583);
    path_23.lineTo(size.width * 0.6069049, size.height * 0.1048399);
    path_23.lineTo(size.width * 0.6077485, size.height * 0.1014196);
    path_23.cubicTo(
        size.width * 0.6083595,
        size.height * 0.09894233,
        size.width * 0.6080472,
        size.height * 0.09681963,
        size.width * 0.6068110,
        size.height * 0.09505153);
    path_23.cubicTo(
        size.width * 0.6055816,
        size.height * 0.09325521,
        size.width * 0.6035736,
        size.height * 0.09201288,
        size.width * 0.6007871,
        size.height * 0.09132577);
    path_23.cubicTo(
        size.width * 0.5981969,
        size.height * 0.09068650,
        size.width * 0.5959436,
        size.height * 0.09074294,
        size.width * 0.5940258,
        size.height * 0.09149448);
    path_23.cubicTo(
        size.width * 0.5921368,
        size.height * 0.09225276,
        size.width * 0.5907307,
        size.height * 0.09359325,
        size.width * 0.5898086,
        size.height * 0.09551595);
    path_23.lineTo(size.width * 0.5811945, size.height * 0.09339141);
    path_23.cubicTo(
        size.width * 0.5824620,
        size.height * 0.08934356,
        size.width * 0.5850613,
        size.height * 0.08643129,
        size.width * 0.5889939,
        size.height * 0.08465399);
    path_23.cubicTo(
        size.width * 0.5929264,
        size.height * 0.08287669,
        size.width * 0.5975810,
        size.height * 0.08265092,
        size.width * 0.6029577,
        size.height * 0.08397730);
    path_23.cubicTo(
        size.width * 0.6083350,
        size.height * 0.08530368,
        size.width * 0.6122871,
        size.height * 0.08768221,
        size.width * 0.6148160,
        size.height * 0.09111288);
    path_23.cubicTo(
        size.width * 0.6173742,
        size.height * 0.09455092,
        size.width * 0.6181043,
        size.height * 0.09849387,
        size.width * 0.6170061,
        size.height * 0.1029411);
    path_23.lineTo(size.width * 0.6094528, size.height * 0.1335552);
    path_23.lineTo(size.width * 0.6007117, size.height * 0.1313994);
    path_23.lineTo(size.width * 0.6025141, size.height * 0.1240939);
    path_23.lineTo(size.width * 0.6023025, size.height * 0.1240417);
    path_23.cubicTo(
        size.width * 0.6004319,
        size.height * 0.1261785,
        size.width * 0.5980209,
        size.height * 0.1276595,
        size.width * 0.5950699,
        size.height * 0.1284840);
    path_23.cubicTo(
        size.width * 0.5921472,
        size.height * 0.1293160,
        size.width * 0.5892221,
        size.height * 0.1293712,
        size.width * 0.5862939,
        size.height * 0.1286491);
    path_23.close();

    Paint paint_23_fill = Paint()..style = PaintingStyle.fill;
    paint_23_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_23, paint_23_fill);

    Path path_24 = Path();
    path_24.moveTo(size.width * 0.7407055, size.height * 0.1726945);
    path_24.cubicTo(
        size.width * 0.7381227,
        size.height * 0.1756074,
        size.width * 0.7358528,
        size.height * 0.1778227,
        size.width * 0.7338834,
        size.height * 0.1793399);
    path_24.cubicTo(
        size.width * 0.7319387,
        size.height * 0.1808344,
        size.width * 0.7298957,
        size.height * 0.1817933,
        size.width * 0.7277607,
        size.height * 0.1822166);
    path_24.lineTo(size.width * 0.7275460, size.height * 0.1824902);
    path_24.lineTo(size.width * 0.7501288, size.height * 0.2002110);
    path_24.lineTo(size.width * 0.7784785, size.height * 0.1640810);
    path_24.lineTo(size.width * 0.7607914, size.height * 0.1501994);
    path_24.lineTo(size.width * 0.7407055, size.height * 0.1726945);
    path_24.close();
    path_24.moveTo(size.width * 0.7169325, size.height * 0.1848853);
    path_24.lineTo(size.width * 0.7092761, size.height * 0.1946368);
    path_24.lineTo(size.width * 0.7022270, size.height * 0.1891055);
    path_24.lineTo(size.width * 0.7150920, size.height * 0.1727166);
    path_24.lineTo(size.width * 0.7186810, size.height * 0.1755362);
    path_24.cubicTo(
        size.width * 0.7211411,
        size.height * 0.1758798,
        size.width * 0.7235521,
        size.height * 0.1753209,
        size.width * 0.7259141,
        size.height * 0.1738583);
    path_24.cubicTo(
        size.width * 0.7283006,
        size.height * 0.1724141,
        size.width * 0.7309509,
        size.height * 0.1700503,
        size.width * 0.7338589,
        size.height * 0.1667675);
    path_24.lineTo(size.width * 0.7591656, size.height * 0.1382528);
    path_24.lineTo(size.width * 0.7913252, size.height * 0.1634920);
    path_24.lineTo(size.width * 0.7577914, size.height * 0.2062252);
    path_24.lineTo(size.width * 0.7639509, size.height * 0.2110583);
    path_24.lineTo(size.width * 0.7510859, size.height * 0.2274472);
    path_24.lineTo(size.width * 0.7440736, size.height * 0.2219429);
    path_24.lineTo(size.width * 0.7517239, size.height * 0.2121920);
    path_24.lineTo(size.width * 0.7169325, size.height * 0.1848853);
    path_24.close();

    Paint paint_24_fill = Paint()..style = PaintingStyle.fill;
    paint_24_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_24, paint_24_fill);

    Path path_25 = Path();
    path_25.moveTo(size.width * 0.7890245, size.height * 0.2418613);
    path_25.cubicTo(
        size.width * 0.7832393,
        size.height * 0.2419607,
        size.width * 0.7780613,
        size.height * 0.2396423,
        size.width * 0.7734969,
        size.height * 0.2349043);
    path_25.cubicTo(
        size.width * 0.7689264,
        size.height * 0.2301663,
        size.width * 0.7667975,
        size.height * 0.2249092,
        size.width * 0.7671043,
        size.height * 0.2191325);
    path_25.cubicTo(
        size.width * 0.7674356,
        size.height * 0.2133350,
        size.width * 0.7702454,
        size.height * 0.2078908,
        size.width * 0.7755215,
        size.height * 0.2027988);
    path_25.cubicTo(
        size.width * 0.7807853,
        size.height * 0.1977270,
        size.width * 0.7863067,
        size.height * 0.1951411,
        size.width * 0.7920920,
        size.height * 0.1950411);
    path_25.cubicTo(
        size.width * 0.7978957,
        size.height * 0.1949209,
        size.width * 0.8030798,
        size.height * 0.1972294,
        size.width * 0.8076503,
        size.height * 0.2019675);
    path_25.cubicTo(
        size.width * 0.8122209,
        size.height * 0.2067055,
        size.width * 0.8143374,
        size.height * 0.2119724,
        size.width * 0.8140061,
        size.height * 0.2177699);
    path_25.cubicTo(
        size.width * 0.8136994,
        size.height * 0.2235466,
        size.width * 0.8109141,
        size.height * 0.2289712,
        size.width * 0.8056503,
        size.height * 0.2340429);
    path_25.cubicTo(
        size.width * 0.8003742,
        size.height * 0.2391350,
        size.width * 0.7948282,
        size.height * 0.2417411,
        size.width * 0.7890245,
        size.height * 0.2418613);
    path_25.close();
    path_25.moveTo(size.width * 0.7790675, size.height * 0.2295307);
    path_25.cubicTo(
        size.width * 0.7816626,
        size.height * 0.2322227,
        size.width * 0.7847301,
        size.height * 0.2333755,
        size.width * 0.7882638,
        size.height * 0.2329890);
    path_25.cubicTo(
        size.width * 0.7917975,
        size.height * 0.2326018,
        size.width * 0.7953804,
        size.height * 0.2306577,
        size.width * 0.7990123,
        size.height * 0.2271552);
    path_25.cubicTo(
        size.width * 0.8026196,
        size.height * 0.2236736,
        size.width * 0.8046748,
        size.height * 0.2201632,
        size.width * 0.8051718,
        size.height * 0.2166252);
    path_25.cubicTo(
        size.width * 0.8056871,
        size.height * 0.2131074,
        size.width * 0.8046564,
        size.height * 0.2100129,
        size.width * 0.8020798,
        size.height * 0.2073411);
    path_25.cubicTo(
        size.width * 0.7995031,
        size.height * 0.2046699,
        size.width * 0.7964356,
        size.height * 0.2035172,
        size.width * 0.7928834,
        size.height * 0.2038828);
    path_25.cubicTo(
        size.width * 0.7893497,
        size.height * 0.2042699,
        size.width * 0.7857730,
        size.height * 0.2062043,
        size.width * 0.7821656,
        size.height * 0.2096859);
    path_25.cubicTo(
        size.width * 0.7785337,
        size.height * 0.2131883,
        size.width * 0.7764601,
        size.height * 0.2166982,
        size.width * 0.7759448,
        size.height * 0.2202153);
    path_25.cubicTo(
        size.width * 0.7754479,
        size.height * 0.2237540,
        size.width * 0.7764908,
        size.height * 0.2268589,
        size.width * 0.7790675,
        size.height * 0.2295307);
    path_25.close();

    Paint paint_25_fill = Paint()..style = PaintingStyle.fill;
    paint_25_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_25, paint_25_fill);

    Path path_26 = Path();
    path_26.moveTo(size.width * 0.8404724, size.height * 0.2656847);
    path_26.lineTo(size.width * 0.8349939, size.height * 0.2584871);
    path_26.cubicTo(
        size.width * 0.8364049,
        size.height * 0.2566098,
        size.width * 0.8371350,
        size.height * 0.2545252,
        size.width * 0.8371779,
        size.height * 0.2522344);
    path_26.cubicTo(
        size.width * 0.8372147,
        size.height * 0.2499436,
        size.width * 0.8363926,
        size.height * 0.2476908,
        size.width * 0.8347055,
        size.height * 0.2454761);
    path_26.cubicTo(
        size.width * 0.8325092,
        size.height * 0.2425926,
        size.width * 0.8295951,
        size.height * 0.2411528,
        size.width * 0.8259509,
        size.height * 0.2411558);
    path_26.cubicTo(
        size.width * 0.8223497,
        size.height * 0.2411650,
        size.width * 0.8186319,
        size.height * 0.2426270,
        size.width * 0.8148037,
        size.height * 0.2455429);
    path_26.cubicTo(
        size.width * 0.8108834,
        size.height * 0.2485294,
        size.width * 0.8084540,
        size.height * 0.2517479,
        size.width * 0.8075092,
        size.height * 0.2551994);
    path_26.cubicTo(
        size.width * 0.8066074,
        size.height * 0.2586564,
        size.width * 0.8072699,
        size.height * 0.2618497,
        size.width * 0.8095031,
        size.height * 0.2647798);
    path_26.cubicTo(
        size.width * 0.8111718,
        size.height * 0.2669712,
        size.width * 0.8130675,
        size.height * 0.2683865,
        size.width * 0.8151963,
        size.height * 0.2690264);
    path_26.cubicTo(
        size.width * 0.8173436,
        size.height * 0.2696485,
        size.width * 0.8195767,
        size.height * 0.2694963,
        size.width * 0.8218957,
        size.height * 0.2685687);
    path_26.lineTo(size.width * 0.8273804, size.height * 0.2757663);
    path_26.cubicTo(
        size.width * 0.8232822,
        size.height * 0.2780460,
        size.width * 0.8190245,
        size.height * 0.2786276,
        size.width * 0.8146012,
        size.height * 0.2775117);
    path_26.cubicTo(
        size.width * 0.8102209,
        size.height * 0.2764012,
        size.width * 0.8064172,
        size.height * 0.2737239,
        size.width * 0.8031840,
        size.height * 0.2694791);
    path_26.cubicTo(
        size.width * 0.7991963,
        size.height * 0.2642429,
        size.width * 0.7977239,
        size.height * 0.2587877,
        size.width * 0.7987607,
        size.height * 0.2531147);
    path_26.cubicTo(
        size.width * 0.7997975,
        size.height * 0.2474417,
        size.width * 0.8032209,
        size.height * 0.2423914,
        size.width * 0.8090368,
        size.height * 0.2379650);
    path_26.cubicTo(
        size.width * 0.8147546,
        size.height * 0.2336092,
        size.width * 0.8205031,
        size.height * 0.2316767,
        size.width * 0.8262699,
        size.height * 0.2321681);
    path_26.cubicTo(
        size.width * 0.8320798,
        size.height * 0.2326650,
        size.width * 0.8369509,
        size.height * 0.2354969,
        size.width * 0.8408834,
        size.height * 0.2406644);
    path_26.cubicTo(
        size.width * 0.8441656,
        size.height * 0.2449779,
        size.width * 0.8456871,
        size.height * 0.2494147,
        size.width * 0.8454417,
        size.height * 0.2539748);
    path_26.cubicTo(
        size.width * 0.8452393,
        size.height * 0.2585405,
        size.width * 0.8435828,
        size.height * 0.2624436,
        size.width * 0.8404724,
        size.height * 0.2656847);
    path_26.close();

    Paint paint_26_fill = Paint()..style = PaintingStyle.fill;
    paint_26_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_26, paint_26_fill);

    Path path_27 = Path();
    path_27.moveTo(size.width * 0.8759448, size.height * 0.2956429);
    path_27.lineTo(size.width * 0.8695706, size.height * 0.2994724);
    path_27.lineTo(size.width * 0.8621779, size.height * 0.2871693);
    path_27.lineTo(size.width * 0.8293313, size.height * 0.3069000);
    path_27.lineTo(size.width * 0.8245153, size.height * 0.2988847);
    path_27.lineTo(size.width * 0.8573620, size.height * 0.2791540);
    path_27.lineTo(size.width * 0.8499693, size.height * 0.2668509);
    path_27.lineTo(size.width * 0.8563497, size.height * 0.2630209);
    path_27.lineTo(size.width * 0.8759448, size.height * 0.2956429);
    path_27.close();

    Paint paint_27_fill = Paint()..style = PaintingStyle.fill;
    paint_27_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_27, paint_27_fill);

    Path path_28 = Path();
    path_28.moveTo(size.width * 0.8511350, size.height * 0.3339779);
    path_28.cubicTo(
        size.width * 0.8525276,
        size.height * 0.3369773,
        size.width * 0.8545951,
        size.height * 0.3390693,
        size.width * 0.8573497,
        size.height * 0.3402546);
    path_28.cubicTo(
        size.width * 0.8601411,
        size.height * 0.3414540,
        size.width * 0.8628773,
        size.height * 0.3414319,
        size.width * 0.8655644,
        size.height * 0.3401896);
    path_28.lineTo(size.width * 0.8688405, size.height * 0.3386724);
    path_28.lineTo(size.width * 0.8634724, size.height * 0.3287436);
    path_28.cubicTo(
        size.width * 0.8621104,
        size.height * 0.3262767,
        size.width * 0.8605644,
        size.height * 0.3245957,
        size.width * 0.8588405,
        size.height * 0.3237006);
    path_28.cubicTo(
        size.width * 0.8571534,
        size.height * 0.3228202,
        size.width * 0.8554172,
        size.height * 0.3227939,
        size.width * 0.8536258,
        size.height * 0.3236221);
    path_28.cubicTo(
        size.width * 0.8518098,
        size.height * 0.3244632,
        size.width * 0.8506687,
        size.height * 0.3258233,
        size.width * 0.8501963,
        size.height * 0.3277031);
    path_28.cubicTo(
        size.width * 0.8497607,
        size.height * 0.3295969,
        size.width * 0.8500736,
        size.height * 0.3316890,
        size.width * 0.8511350,
        size.height * 0.3339779);
    path_28.close();
    path_28.moveTo(size.width * 0.8432883, size.height * 0.3347350);
    path_28.cubicTo(
        size.width * 0.8414049,
        size.height * 0.3306571,
        size.width * 0.8410184,
        size.height * 0.3267926,
        size.width * 0.8421411,
        size.height * 0.3231417);
    path_28.cubicTo(
        size.width * 0.8432761,
        size.height * 0.3195178,
        size.width * 0.8456933,
        size.height * 0.3168466,
        size.width * 0.8494049,
        size.height * 0.3151288);
    path_28.cubicTo(
        size.width * 0.8530368,
        size.height * 0.3134472,
        size.width * 0.8565399,
        size.height * 0.3134546,
        size.width * 0.8599141,
        size.height * 0.3151515);
    path_28.cubicTo(
        size.width * 0.8633129,
        size.height * 0.3168362,
        size.width * 0.8662822,
        size.height * 0.3200773,
        size.width * 0.8688221,
        size.height * 0.3248755);
    path_28.lineTo(size.width * 0.8747607, size.height * 0.3359307);
    path_28.lineTo(size.width * 0.8779571, size.height * 0.3344503);
    path_28.cubicTo(
        size.width * 0.8802699,
        size.height * 0.3333785,
        size.width * 0.8817301,
        size.height * 0.3318074,
        size.width * 0.8823374,
        size.height * 0.3297368);
    path_28.cubicTo(
        size.width * 0.8829693,
        size.height * 0.3276540,
        size.width * 0.8826871,
        size.height * 0.3253104,
        size.width * 0.8814785,
        size.height * 0.3227055);
    path_28.cubicTo(
        size.width * 0.8803558,
        size.height * 0.3202853,
        size.width * 0.8789080,
        size.height * 0.3185620,
        size.width * 0.8771227,
        size.height * 0.3175350);
    path_28.cubicTo(
        size.width * 0.8753436,
        size.height * 0.3165350,
        size.width * 0.8734233,
        size.height * 0.3162761,
        size.width * 0.8713436,
        size.height * 0.3167589);
    path_28.lineTo(size.width * 0.8676135, size.height * 0.3087080);
    path_28.cubicTo(
        size.width * 0.8715644,
        size.height * 0.3071663,
        size.width * 0.8754663,
        size.height * 0.3073736,
        size.width * 0.8793129,
        size.height * 0.3093307);
    path_28.cubicTo(
        size.width * 0.8831595,
        size.height * 0.3112883,
        size.width * 0.8862454,
        size.height * 0.3147798,
        size.width * 0.8885706,
        size.height * 0.3198049);
    path_28.cubicTo(
        size.width * 0.8908957,
        size.height * 0.3248301,
        size.width * 0.8915153,
        size.height * 0.3294018,
        size.width * 0.8904172,
        size.height * 0.3335202);
    path_28.cubicTo(
        size.width * 0.8893313,
        size.height * 0.3376650,
        size.width * 0.8867117,
        size.height * 0.3407000,
        size.width * 0.8825583,
        size.height * 0.3426252);
    path_28.lineTo(size.width * 0.8539448, size.height * 0.3558755);
    path_28.lineTo(size.width * 0.8501595, size.height * 0.3477061);
    path_28.lineTo(size.width * 0.8569877, size.height * 0.3445442);
    path_28.lineTo(size.width * 0.8568957, size.height * 0.3443466);
    path_28.cubicTo(
        size.width * 0.8540613,
        size.height * 0.3442227,
        size.width * 0.8513988,
        size.height * 0.3432675,
        size.width * 0.8489080,
        size.height * 0.3414804);
    path_28.cubicTo(
        size.width * 0.8464294,
        size.height * 0.3397196,
        size.width * 0.8445583,
        size.height * 0.3374712,
        size.width * 0.8432883,
        size.height * 0.3347350);
    path_28.close();

    Paint paint_28_fill = Paint()..style = PaintingStyle.fill;
    paint_28_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_28, paint_28_fill);

    Path path_29 = Path();
    path_29.moveTo(size.width * 0.8994847, size.height * 0.3682859);
    path_29.lineTo(size.width * 0.8964847, size.height * 0.3591552);
    path_29.lineTo(size.width * 0.8850798, size.height * 0.3629037);
    path_29.lineTo(size.width * 0.8877975, size.height * 0.3711669);
    path_29.cubicTo(
        size.width * 0.8894540,
        size.height * 0.3762074,
        size.width * 0.8922086,
        size.height * 0.3780939,
        size.width * 0.8960675,
        size.height * 0.3768264);
    path_29.cubicTo(
        size.width * 0.8978589,
        size.height * 0.3762380,
        size.width * 0.8990429,
        size.height * 0.3752067,
        size.width * 0.8996258,
        size.height * 0.3737331);
    path_29.cubicTo(
        size.width * 0.9002393,
        size.height * 0.3722503,
        size.width * 0.9001902,
        size.height * 0.3704344,
        size.width * 0.8994847,
        size.height * 0.3682859);
    path_29.close();
    path_29.moveTo(size.width * 0.8817485, size.height * 0.3737963);
    path_29.lineTo(size.width * 0.8788405, size.height * 0.3649540);
    path_29.lineTo(size.width * 0.8659939, size.height * 0.3691779);
    path_29.lineTo(size.width * 0.8692270, size.height * 0.3790117);
    path_29.cubicTo(
        size.width * 0.8700552,
        size.height * 0.3815454,
        size.width * 0.8712270,
        size.height * 0.3832969,
        size.width * 0.8727362,
        size.height * 0.3842663);
    path_29.cubicTo(
        size.width * 0.8742761,
        size.height * 0.3852264,
        size.width * 0.8760920,
        size.height * 0.3853620,
        size.width * 0.8781840,
        size.height * 0.3846742);
    path_29.cubicTo(
        size.width * 0.8824785,
        size.height * 0.3832613,
        size.width * 0.8836687,
        size.height * 0.3796356,
        size.width * 0.8817485,
        size.height * 0.3737963);
    path_29.close();
    path_29.moveTo(size.width * 0.8566135, size.height * 0.3625098);
    path_29.lineTo(size.width * 0.9000798, size.height * 0.3482221);
    path_29.lineTo(size.width * 0.9065706, size.height * 0.3679718);
    path_29.cubicTo(
        size.width * 0.9079632,
        size.height * 0.3722135,
        size.width * 0.9080613,
        size.height * 0.3758589,
        size.width * 0.9068712,
        size.height * 0.3789067);
    path_29.cubicTo(
        size.width * 0.9057117,
        size.height * 0.3819736,
        size.width * 0.9034110,
        size.height * 0.3840730,
        size.width * 0.8999632,
        size.height * 0.3852049);
    path_29.cubicTo(
        size.width * 0.8977055,
        size.height * 0.3859472,
        size.width * 0.8954356,
        size.height * 0.3859012,
        size.width * 0.8931472,
        size.height * 0.3850663);
    path_29.cubicTo(
        size.width * 0.8908896,
        size.height * 0.3842503,
        size.width * 0.8892270,
        size.height * 0.3828589,
        size.width * 0.8881534,
        size.height * 0.3808920);
    path_29.lineTo(size.width * 0.8878282, size.height * 0.3810006);
    path_29.cubicTo(
        size.width * 0.8883620,
        size.height * 0.3839368,
        size.width * 0.8879877,
        size.height * 0.3865626,
        size.width * 0.8867055,
        size.height * 0.3888767);
    path_29.cubicTo(
        size.width * 0.8854479,
        size.height * 0.3911822,
        size.width * 0.8834601,
        size.height * 0.3927828,
        size.width * 0.8807301,
        size.height * 0.3936791);
    path_29.cubicTo(
        size.width * 0.8769018,
        size.height * 0.3949380,
        size.width * 0.8734049,
        size.height * 0.3945613,
        size.width * 0.8702393,
        size.height * 0.3925491);
    path_29.cubicTo(
        size.width * 0.8670859,
        size.height * 0.3905644,
        size.width * 0.8647362,
        size.height * 0.3872172,
        size.width * 0.8631840,
        size.height * 0.3825074);
    path_29.lineTo(size.width * 0.8566135, size.height * 0.3625098);
    path_29.close();

    Paint paint_29_fill = Paint()..style = PaintingStyle.fill;
    paint_29_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_29, paint_29_fill);

    Path path_30 = Path();
    path_30.moveTo(size.width * 0.8972699, size.height * 0.4167939);
    path_30.lineTo(size.width * 0.8777669, size.height * 0.4423190);
    path_30.lineTo(size.width * 0.8753804, size.height * 0.4307767);
    path_30.lineTo(size.width * 0.8923681, size.height * 0.4087479);
    path_30.lineTo(size.width * 0.8922945, size.height * 0.4084067);
    path_30.lineTo(size.width * 0.8716380, size.height * 0.4126755);
    path_30.lineTo(size.width * 0.8697485, size.height * 0.4035178);
    path_30.lineTo(size.width * 0.9145521, size.height * 0.3942601);
    path_30.lineTo(size.width * 0.9164479, size.height * 0.4034172);
    path_30.lineTo(size.width * 0.8972822, size.height * 0.4073773);
    path_30.lineTo(size.width * 0.8973497, size.height * 0.4077184);
    path_30.lineTo(size.width * 0.9201411, size.height * 0.4213061);
    path_30.lineTo(size.width * 0.9224110, size.height * 0.4322945);
    path_30.lineTo(size.width * 0.8972699, size.height * 0.4167939);
    path_30.close();

    Paint paint_30_fill = Paint()..style = PaintingStyle.fill;
    paint_30_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_30, paint_30_fill);

    Path path_31 = Path();
    path_31.moveTo(size.width * 0.8863252, size.height * 0.4620963);
    path_31.cubicTo(
        size.width * 0.8866196,
        size.height * 0.4653890,
        size.width * 0.8878589,
        size.height * 0.4680583,
        size.width * 0.8900491,
        size.height * 0.4701049);
    path_31.cubicTo(
        size.width * 0.8922699,
        size.height * 0.4721779,
        size.width * 0.8948589,
        size.height * 0.4730834,
        size.width * 0.8978037,
        size.height * 0.4728215);
    path_31.lineTo(size.width * 0.9013988, size.height * 0.4725018);
    path_31.lineTo(size.width * 0.8997055, size.height * 0.4613429);
    path_31.cubicTo(
        size.width * 0.8992577,
        size.height * 0.4585595,
        size.width * 0.8983681,
        size.height * 0.4564552,
        size.width * 0.8970491,
        size.height * 0.4550301);
    path_31.cubicTo(
        size.width * 0.8957607,
        size.height * 0.4536307,
        size.width * 0.8941350,
        size.height * 0.4530190,
        size.width * 0.8921718,
        size.height * 0.4531933);
    path_31.cubicTo(
        size.width * 0.8901779,
        size.height * 0.4533706,
        size.width * 0.8886442,
        size.height * 0.4542638,
        size.width * 0.8875644,
        size.height * 0.4558736);
    path_31.cubicTo(
        size.width * 0.8865153,
        size.height * 0.4575098,
        size.width * 0.8861043,
        size.height * 0.4595840,
        size.width * 0.8863252,
        size.height * 0.4620963);
    path_31.close();
    path_31.moveTo(size.width * 0.8786871, size.height * 0.4601564);
    path_31.cubicTo(
        size.width * 0.8782883,
        size.height * 0.4556798,
        size.width * 0.8792331,
        size.height * 0.4519135,
        size.width * 0.8815215,
        size.height * 0.4488571);
    path_31.cubicTo(
        size.width * 0.8838160,
        size.height * 0.4458294,
        size.width * 0.8870000,
        size.height * 0.4441350,
        size.width * 0.8910675,
        size.height * 0.4437724);
    path_31.cubicTo(
        size.width * 0.8950552,
        size.height * 0.4434184,
        size.width * 0.8983497,
        size.height * 0.4446098,
        size.width * 0.9009509,
        size.height * 0.4473479);
    path_31.cubicTo(
        size.width * 0.9035828,
        size.height * 0.4500828,
        size.width * 0.9052822,
        size.height * 0.4541380,
        size.width * 0.9060491,
        size.height * 0.4595129);
    path_31.lineTo(size.width * 0.9078957, size.height * 0.4719239);
    path_31.lineTo(size.width * 0.9114049, size.height * 0.4716117);
    path_31.cubicTo(
        size.width * 0.9139448,
        size.height * 0.4713859,
        size.width * 0.9158528,
        size.height * 0.4704012,
        size.width * 0.9171227,
        size.height * 0.4686577);
    path_31.cubicTo(
        size.width * 0.9184233,
        size.height * 0.4669123,
        size.width * 0.9189448,
        size.height * 0.4646098,
        size.width * 0.9186933,
        size.height * 0.4617503);
    path_31.cubicTo(
        size.width * 0.9184540,
        size.height * 0.4590933,
        size.width * 0.9176687,
        size.height * 0.4569804,
        size.width * 0.9163374,
        size.height * 0.4554104);
    path_31.cubicTo(
        size.width * 0.9150061,
        size.height * 0.4538693,
        size.width * 0.9132822,
        size.height * 0.4529748,
        size.width * 0.9111656,
        size.height * 0.4527264);
    path_31.lineTo(size.width * 0.9103804, size.height * 0.4438890);
    path_31.cubicTo(
        size.width * 0.9146196,
        size.height * 0.4437742,
        size.width * 0.9182209,
        size.height * 0.4452877,
        size.width * 0.9211779,
        size.height * 0.4484307);
    path_31.cubicTo(
        size.width * 0.9241350,
        size.height * 0.4515736,
        size.width * 0.9258589,
        size.height * 0.4559031,
        size.width * 0.9263497,
        size.height * 0.4614190);
    path_31.cubicTo(
        size.width * 0.9268405,
        size.height * 0.4669350,
        size.width * 0.9258712,
        size.height * 0.4714460,
        size.width * 0.9234479,
        size.height * 0.4749509);
    path_31.cubicTo(
        size.width * 0.9210245,
        size.height * 0.4784847,
        size.width * 0.9175337,
        size.height * 0.4804546,
        size.width * 0.9129693,
        size.height * 0.4808601);
    path_31.lineTo(size.width * 0.8815644, size.height * 0.4836534);
    path_31.lineTo(size.width * 0.8807669, size.height * 0.4746859);
    path_31.lineTo(size.width * 0.8882577, size.height * 0.4740196);
    path_31.lineTo(size.width * 0.8882393, size.height * 0.4738031);
    path_31.cubicTo(
        size.width * 0.8856135,
        size.height * 0.4727264,
        size.width * 0.8834294,
        size.height * 0.4709270,
        size.width * 0.8816871,
        size.height * 0.4684031);
    path_31.cubicTo(
        size.width * 0.8799571,
        size.height * 0.4659086,
        size.width * 0.8789509,
        size.height * 0.4631595,
        size.width * 0.8786871,
        size.height * 0.4601564);
    path_31.close();

    Paint paint_31_fill = Paint()..style = PaintingStyle.fill;
    paint_31_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_31, paint_31_fill);

    Path path_32 = Path();
    path_32.moveTo(size.width * 0.8868466, size.height * 0.6227546);
    path_32.cubicTo(
        size.width * 0.8832577,
        size.height * 0.6212515,
        size.width * 0.8804294,
        size.height * 0.6198160,
        size.width * 0.8783558,
        size.height * 0.6184479);
    path_32.cubicTo(
        size.width * 0.8763129,
        size.height * 0.6170859,
        size.width * 0.8747485,
        size.height * 0.6154601,
        size.width * 0.8736564,
        size.height * 0.6135767);
    path_32.lineTo(size.width * 0.8733252, size.height * 0.6134613);
    path_32.lineTo(size.width * 0.8638528, size.height * 0.6405583);
    path_32.lineTo(size.width * 0.9072025, size.height * 0.6557178);
    path_32.lineTo(size.width * 0.9146258, size.height * 0.6344908);
    path_32.lineTo(size.width * 0.8868466, size.height * 0.6227546);
    path_32.close();
    path_32.moveTo(size.width * 0.8676319, size.height * 0.6041896);
    path_32.lineTo(size.width * 0.8559325, size.height * 0.6000975);
    path_32.lineTo(size.width * 0.8588896, size.height * 0.5916405);
    path_32.lineTo(size.width * 0.8785521, size.height * 0.5985178);
    path_32.lineTo(size.width * 0.8770429, size.height * 0.6028282);
    path_32.cubicTo(
        size.width * 0.8775153,
        size.height * 0.6052650,
        size.width * 0.8788221,
        size.height * 0.6073663,
        size.width * 0.8809693,
        size.height * 0.6091307);
    path_32.cubicTo(
        size.width * 0.8831043,
        size.height * 0.6109227,
        size.width * 0.8861963,
        size.height * 0.6126644,
        size.width * 0.8902454,
        size.height * 0.6143558);
    path_32.lineTo(size.width * 0.9254049, size.height * 0.6290920);
    path_32.lineTo(size.width * 0.9119080, size.height * 0.6676871);
    path_32.lineTo(size.width * 0.8606319, size.height * 0.6497546);
    path_32.lineTo(size.width * 0.8580491, size.height * 0.6571411);
    path_32.lineTo(size.width * 0.8383865, size.height * 0.6502638);
    path_32.lineTo(size.width * 0.8413313, size.height * 0.6418466);
    path_32.lineTo(size.width * 0.8530307, size.height * 0.6459387);
    path_32.lineTo(size.width * 0.8676319, size.height * 0.6041896);
    path_32.close();

    Paint paint_32_fill = Paint()..style = PaintingStyle.fill;
    paint_32_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_32, paint_32_fill);

    Path path_33 = Path();
    path_33.moveTo(size.width * 0.8364110, size.height * 0.6915460);
    path_33.cubicTo(
        size.width * 0.8344663,
        size.height * 0.6860982,
        size.width * 0.8350061,
        size.height * 0.6804479,
        size.width * 0.8380368,
        size.height * 0.6746074);
    path_33.cubicTo(
        size.width * 0.8410675,
        size.height * 0.6687607,
        size.width * 0.8453681,
        size.height * 0.6650675,
        size.width * 0.8509387,
        size.height * 0.6635153);
    path_33.cubicTo(
        size.width * 0.8565399,
        size.height * 0.6619755,
        size.width * 0.8625951,
        size.height * 0.6628896,
        size.width * 0.8691104,
        size.height * 0.6662699);
    path_33.cubicTo(
        size.width * 0.8755951,
        size.height * 0.6696319,
        size.width * 0.8798098,
        size.height * 0.6740368,
        size.width * 0.8817546,
        size.height * 0.6794847);
    path_33.cubicTo(
        size.width * 0.8837239,
        size.height * 0.6849448,
        size.width * 0.8831902,
        size.height * 0.6906012,
        size.width * 0.8801656,
        size.height * 0.6964417);
    path_33.cubicTo(
        size.width * 0.8771350,
        size.height * 0.7022883,
        size.width * 0.8728221,
        size.height * 0.7059755,
        size.width * 0.8672209,
        size.height * 0.7075153);
    path_33.cubicTo(
        size.width * 0.8616503,
        size.height * 0.7090675,
        size.width * 0.8556196,
        size.height * 0.7081656,
        size.width * 0.8491350,
        size.height * 0.7048037);
    path_33.cubicTo(
        size.width * 0.8426196,
        size.height * 0.7014294,
        size.width * 0.8383804,
        size.height * 0.6970061,
        size.width * 0.8364110,
        size.height * 0.6915460);
    path_33.close();
    path_33.moveTo(size.width * 0.8449080, size.height * 0.6781718);
    path_33.cubicTo(
        size.width * 0.8431902,
        size.height * 0.6814908,
        size.width * 0.8430736,
        size.height * 0.6847607,
        size.width * 0.8445706,
        size.height * 0.6879877);
    path_33.cubicTo(
        size.width * 0.8460675,
        size.height * 0.6912147,
        size.width * 0.8490552,
        size.height * 0.6939877,
        size.width * 0.8535337,
        size.height * 0.6963067);
    path_33.cubicTo(
        size.width * 0.8579877,
        size.height * 0.6986135,
        size.width * 0.8619693,
        size.height * 0.6994417,
        size.width * 0.8654847,
        size.height * 0.6987791);
    path_33.cubicTo(
        size.width * 0.8689816,
        size.height * 0.6981411,
        size.width * 0.8715828,
        size.height * 0.6961779,
        size.width * 0.8732883,
        size.height * 0.6928773);
    path_33.cubicTo(
        size.width * 0.8750000,
        size.height * 0.6895828,
        size.width * 0.8751104,
        size.height * 0.6863129,
        size.width * 0.8736258,
        size.height * 0.6830613);
    path_33.cubicTo(
        size.width * 0.8721350,
        size.height * 0.6798344,
        size.width * 0.8691595,
        size.height * 0.6770675,
        size.width * 0.8647055,
        size.height * 0.6747607);
    path_33.cubicTo(
        size.width * 0.8602270,
        size.height * 0.6724417,
        size.width * 0.8562393,
        size.height * 0.6715951,
        size.width * 0.8527362,
        size.height * 0.6722331);
    path_33.cubicTo(
        size.width * 0.8492270,
        size.height * 0.6728957,
        size.width * 0.8466196,
        size.height * 0.6748773,
        size.width * 0.8449080,
        size.height * 0.6781718);
    path_33.close();

    Paint paint_33_fill = Paint()..style = PaintingStyle.fill;
    paint_33_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_33, paint_33_fill);

    Path path_34 = Path();
    path_34.moveTo(size.width * 0.8300859, size.height * 0.7479509);
    path_34.lineTo(size.width * 0.8351656, size.height * 0.7404663);
    path_34.cubicTo(
        size.width * 0.8373926,
        size.height * 0.7412086,
        size.width * 0.8396012,
        size.height * 0.7412331,
        size.width * 0.8417853,
        size.height * 0.7405399);
    path_34.cubicTo(
        size.width * 0.8439693,
        size.height * 0.7398528,
        size.width * 0.8458405,
        size.height * 0.7383558,
        size.width * 0.8474049,
        size.height * 0.7360491);
    path_34.cubicTo(
        size.width * 0.8494417,
        size.height * 0.7330552,
        size.width * 0.8498773,
        size.height * 0.7298282,
        size.width * 0.8487117,
        size.height * 0.7263742);
    path_34.cubicTo(
        size.width * 0.8475583,
        size.height * 0.7229632,
        size.width * 0.8449877,
        size.height * 0.7199080,
        size.width * 0.8410061,
        size.height * 0.7172086);
    path_34.cubicTo(
        size.width * 0.8369264,
        size.height * 0.7144417,
        size.width * 0.8330982,
        size.height * 0.7131595,
        size.width * 0.8295276,
        size.height * 0.7133620);
    path_34.cubicTo(
        size.width * 0.8259632,
        size.height * 0.7136074,
        size.width * 0.8231472,
        size.height * 0.7152577,
        size.width * 0.8210798,
        size.height * 0.7183006);
    path_34.cubicTo(
        size.width * 0.8195337,
        size.height * 0.7205828,
        size.width * 0.8187975,
        size.height * 0.7228344,
        size.width * 0.8188712,
        size.height * 0.7250552);
    path_34.cubicTo(
        size.width * 0.8189632,
        size.height * 0.7272883,
        size.width * 0.8198221,
        size.height * 0.7293558,
        size.width * 0.8214356,
        size.height * 0.7312638);
    path_34.lineTo(size.width * 0.8163558, size.height * 0.7387485);
    path_34.cubicTo(
        size.width * 0.8128957,
        size.height * 0.7355890,
        size.width * 0.8109877,
        size.height * 0.7317423,
        size.width * 0.8106380,
        size.height * 0.7271963);
    path_34.cubicTo(
        size.width * 0.8102945,
        size.height * 0.7226871,
        size.width * 0.8116196,
        size.height * 0.7182270,
        size.width * 0.8146135,
        size.height * 0.7138098);
    path_34.cubicTo(
        size.width * 0.8183129,
        size.height * 0.7083620,
        size.width * 0.8230123,
        size.height * 0.7052270,
        size.width * 0.8287178,
        size.height * 0.7044049);
    path_34.cubicTo(
        size.width * 0.8344294,
        size.height * 0.7035828,
        size.width * 0.8403067,
        size.height * 0.7052209,
        size.width * 0.8463497,
        size.height * 0.7093252);
    path_34.cubicTo(
        size.width * 0.8523006,
        size.height * 0.7133620,
        size.width * 0.8559632,
        size.height * 0.7181902,
        size.width * 0.8573374,
        size.height * 0.7238160);
    path_34.cubicTo(
        size.width * 0.8587117,
        size.height * 0.7294785,
        size.width * 0.8575767,
        size.height * 0.7350000,
        size.width * 0.8539325,
        size.height * 0.7403742);
    path_34.cubicTo(
        size.width * 0.8508896,
        size.height * 0.7448589,
        size.width * 0.8471656,
        size.height * 0.7477178,
        size.width * 0.8427669,
        size.height * 0.7489325);
    path_34.cubicTo(
        size.width * 0.8383742,
        size.height * 0.7501963,
        size.width * 0.8341472,
        size.height * 0.7498650,
        size.width * 0.8300859,
        size.height * 0.7479509);
    path_34.close();

    Paint paint_34_fill = Paint()..style = PaintingStyle.fill;
    paint_34_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_34, paint_34_fill);

    Path path_35 = Path();
    path_35.moveTo(size.width * 0.8138650, size.height * 0.7900491);
    path_35.lineTo(size.width * 0.8081840, size.height * 0.7852454);
    path_35.lineTo(size.width * 0.8174479, size.height * 0.7742883);
    path_35.lineTo(size.width * 0.7881902, size.height * 0.7495521);
    path_35.lineTo(size.width * 0.7942270, size.height * 0.7424110);
    path_35.lineTo(size.width * 0.8234847, size.height * 0.7671472);
    path_35.lineTo(size.width * 0.8327546, size.height * 0.7561840);
    path_35.lineTo(size.width * 0.8384294, size.height * 0.7609877);
    path_35.lineTo(size.width * 0.8138650, size.height * 0.7900491);
    path_35.close();

    Paint paint_35_fill = Paint()..style = PaintingStyle.fill;
    paint_35_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_35, paint_35_fill);

    Path path_36 = Path();
    path_36.moveTo(size.width * 0.7687362, size.height * 0.7796258);
    path_36.cubicTo(
        size.width * 0.7663374,
        size.height * 0.7819018,
        size.width * 0.7650245,
        size.height * 0.7845337,
        size.width * 0.7647853,
        size.height * 0.7875215);
    path_36.cubicTo(
        size.width * 0.7645399,
        size.height * 0.7905521,
        size.width * 0.7654417,
        size.height * 0.7931350,
        size.width * 0.7674785,
        size.height * 0.7952822);
    path_36.lineTo(size.width * 0.7699632, size.height * 0.7978957);
    path_36.lineTo(size.width * 0.7776503, size.height * 0.7896319);
    path_36.cubicTo(
        size.width * 0.7795460,
        size.height * 0.7875460,
        size.width * 0.7806442,
        size.height * 0.7855460,
        size.width * 0.7809387,
        size.height * 0.7836258);
    path_36.cubicTo(
        size.width * 0.7812331,
        size.height * 0.7817485,
        size.width * 0.7806994,
        size.height * 0.7800920,
        size.width * 0.7793436,
        size.height * 0.7786626);
    path_36.cubicTo(
        size.width * 0.7779632,
        size.height * 0.7772147,
        size.width * 0.7763129,
        size.height * 0.7765644,
        size.width * 0.7743804,
        size.height * 0.7767239);
    path_36.cubicTo(
        size.width * 0.7724479,
        size.height * 0.7769202,
        size.width * 0.7705644,
        size.height * 0.7778834,
        size.width * 0.7687362,
        size.height * 0.7796258);
    path_36.close();
    path_36.moveTo(size.width * 0.7655031, size.height * 0.7724356);
    path_36.cubicTo(
        size.width * 0.7687607,
        size.height * 0.7693374,
        size.width * 0.7723006,
        size.height * 0.7677362,
        size.width * 0.7761166,
        size.height * 0.7676258);
    path_36.cubicTo(
        size.width * 0.7799141,
        size.height * 0.7675399,
        size.width * 0.7832209,
        size.height * 0.7689755,
        size.width * 0.7860368,
        size.height * 0.7719387);
    path_36.cubicTo(
        size.width * 0.7887914,
        size.height * 0.7748405,
        size.width * 0.7899080,
        size.height * 0.7781595,
        size.width * 0.7893804,
        size.height * 0.7819018);
    path_36.cubicTo(
        size.width * 0.7888773,
        size.height * 0.7856626,
        size.width * 0.7867607,
        size.height * 0.7895153,
        size.width * 0.7830307,
        size.height * 0.7934601);
    path_36.lineTo(size.width * 0.7744601, size.height * 0.8026258);
    path_36.lineTo(size.width * 0.7768834, size.height * 0.8051779);
    path_36.cubicTo(
        size.width * 0.7786442,
        size.height * 0.8070307,
        size.width * 0.7806012,
        size.height * 0.8079080,
        size.width * 0.7827546,
        size.height * 0.8078221);
    path_36.cubicTo(
        size.width * 0.7849325,
        size.height * 0.8077546,
        size.width * 0.7870613,
        size.height * 0.8067301,
        size.width * 0.7891411,
        size.height * 0.8047546);
    path_36.cubicTo(
        size.width * 0.7910736,
        size.height * 0.8029141,
        size.width * 0.7922393,
        size.height * 0.8009877,
        size.width * 0.7926380,
        size.height * 0.7989632);
    path_36.cubicTo(
        size.width * 0.7930184,
        size.height * 0.7969632,
        size.width * 0.7926442,
        size.height * 0.7950613,
        size.width * 0.7915215,
        size.height * 0.7932454);
    path_36.lineTo(size.width * 0.7979571, size.height * 0.7871350);
    path_36.cubicTo(
        size.width * 0.8006810,
        size.height * 0.7903804,
        size.width * 0.8017362,
        size.height * 0.7941411,
        size.width * 0.8011166,
        size.height * 0.7984110);
    path_36.cubicTo(
        size.width * 0.8004908,
        size.height * 0.8026810,
        size.width * 0.7981718,
        size.height * 0.8067239,
        size.width * 0.7941595,
        size.height * 0.8105399);
    path_36.cubicTo(
        size.width * 0.7901472,
        size.height * 0.8143558,
        size.width * 0.7860123,
        size.height * 0.8164049,
        size.width * 0.7817607,
        size.height * 0.8166871);
    path_36.cubicTo(
        size.width * 0.7774847,
        size.height * 0.8169877,
        size.width * 0.7737730,
        size.height * 0.8154785,
        size.width * 0.7706135,
        size.height * 0.8121595);
    path_36.lineTo(size.width * 0.7488957, size.height * 0.7893006);
    path_36.lineTo(size.width * 0.7554172, size.height * 0.7830982);
    path_36.lineTo(size.width * 0.7606012, size.height * 0.7885521);
    path_36.lineTo(size.width * 0.7607607, size.height * 0.7884049);
    path_36.cubicTo(
        size.width * 0.7599693,
        size.height * 0.7856748,
        size.width * 0.7600184,
        size.height * 0.7828466,
        size.width * 0.7609141,
        size.height * 0.7799141);
    path_36.cubicTo(
        size.width * 0.7617914,
        size.height * 0.7770061,
        size.width * 0.7633190,
        size.height * 0.7745092,
        size.width * 0.7655031,
        size.height * 0.7724356);
    path_36.close();

    Paint paint_36_fill = Paint()..style = PaintingStyle.fill;
    paint_36_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_36, paint_36_fill);

    Path path_37 = Path();
    path_37.moveTo(size.width * 0.7522331, size.height * 0.8359264);
    path_37.lineTo(size.width * 0.7599018, size.height * 0.8301288);
    path_37.lineTo(size.width * 0.7526626, size.height * 0.8205521);
    path_37.lineTo(size.width * 0.7457239, size.height * 0.8257975);
    path_37.cubicTo(
        size.width * 0.7414908,
        size.height * 0.8289939,
        size.width * 0.7406012,
        size.height * 0.8322147,
        size.width * 0.7430491,
        size.height * 0.8354540);
    path_37.cubicTo(
        size.width * 0.7441840,
        size.height * 0.8369571,
        size.width * 0.7455399,
        size.height * 0.8377485,
        size.width * 0.7471288,
        size.height * 0.8378221);
    path_37.cubicTo(
        size.width * 0.7487239,
        size.height * 0.8379202,
        size.width * 0.7504294,
        size.height * 0.8372883,
        size.width * 0.7522331,
        size.height * 0.8359264);
    path_37.close();
    path_37.moveTo(size.width * 0.7412822, size.height * 0.8209264);
    path_37.lineTo(size.width * 0.7487055, size.height * 0.8153129);
    path_37.lineTo(size.width * 0.7405521, size.height * 0.8045215);
    path_37.lineTo(size.width * 0.7322945, size.height * 0.8107607);
    path_37.cubicTo(
        size.width * 0.7301656,
        size.height * 0.8123681,
        size.width * 0.7288834,
        size.height * 0.8140429,
        size.width * 0.7284601,
        size.height * 0.8157853);
    path_37.cubicTo(
        size.width * 0.7280491,
        size.height * 0.8175521,
        size.width * 0.7285031,
        size.height * 0.8193129,
        size.width * 0.7298344,
        size.height * 0.8210675);
    path_37.cubicTo(
        size.width * 0.7325583,
        size.height * 0.8246810,
        size.width * 0.7363742,
        size.height * 0.8246319,
        size.width * 0.7412822,
        size.height * 0.8209264);
    path_37.close();
    path_37.moveTo(size.width * 0.7438221, size.height * 0.7934908);
    path_37.lineTo(size.width * 0.7714110, size.height * 0.8299939);
    path_37.lineTo(size.width * 0.7548221, size.height * 0.8425276);
    path_37.cubicTo(
        size.width * 0.7512638,
        size.height * 0.8452147,
        size.width * 0.7478466,
        size.height * 0.8464908,
        size.width * 0.7445767,
        size.height * 0.8463436);
    path_37.cubicTo(
        size.width * 0.7412945,
        size.height * 0.8462393,
        size.width * 0.7385644,
        size.height * 0.8447423,
        size.width * 0.7363804,
        size.height * 0.8418528);
    path_37.cubicTo(
        size.width * 0.7349448,
        size.height * 0.8399571,
        size.width * 0.7342577,
        size.height * 0.8377914,
        size.width * 0.7343067,
        size.height * 0.8353558);
    path_37.cubicTo(
        size.width * 0.7343497,
        size.height * 0.8329571,
        size.width * 0.7351288,
        size.height * 0.8309325,
        size.width * 0.7366380,
        size.height * 0.8292822);
    path_37.lineTo(size.width * 0.7364294, size.height * 0.8290061);
    path_37.cubicTo(
        size.width * 0.7338282,
        size.height * 0.8304663,
        size.width * 0.7312209,
        size.height * 0.8309632,
        size.width * 0.7286135,
        size.height * 0.8304969);
    path_37.cubicTo(
        size.width * 0.7260245,
        size.height * 0.8300552,
        size.width * 0.7238650,
        size.height * 0.8286871,
        size.width * 0.7221350,
        size.height * 0.8263926);
    path_37.cubicTo(
        size.width * 0.7197055,
        size.height * 0.8231779,
        size.width * 0.7189325,
        size.height * 0.8197485,
        size.width * 0.7198098,
        size.height * 0.8161043);
    path_37.cubicTo(
        size.width * 0.7206687,
        size.height * 0.8124785,
        size.width * 0.7230736,
        size.height * 0.8091718,
        size.width * 0.7270307,
        size.height * 0.8061779);
    path_37.lineTo(size.width * 0.7438221, size.height * 0.7934908);
    path_37.close();

    Paint paint_37_fill = Paint()..style = PaintingStyle.fill;
    paint_37_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_37, paint_37_fill);

    Path path_38 = Path();
    path_38.moveTo(size.width * 0.7050368, size.height * 0.8497730);
    path_38.lineTo(size.width * 0.6745706, size.height * 0.8395828);
    path_38.lineTo(size.width * 0.6847178, size.height * 0.8335890);
    path_38.lineTo(size.width * 0.7110613, size.height * 0.8425276);
    path_38.lineTo(size.width * 0.7113620, size.height * 0.8423497);
    path_38.lineTo(size.width * 0.7006319, size.height * 0.8241902);
    path_38.lineTo(size.width * 0.7086810, size.height * 0.8194356);
    path_38.lineTo(size.width * 0.7319509, size.height * 0.8588282);
    path_38.lineTo(size.width * 0.7239018, size.height * 0.8635828);
    path_38.lineTo(size.width * 0.7139509, size.height * 0.8467301);
    path_38.lineTo(size.width * 0.7136503, size.height * 0.8469080);
    path_38.lineTo(size.width * 0.7081779, size.height * 0.8728712);
    path_38.lineTo(size.width * 0.6985153, size.height * 0.8785828);
    path_38.lineTo(size.width * 0.7050368, size.height * 0.8497730);
    path_38.close();

    Paint paint_38_fill = Paint()..style = PaintingStyle.fill;
    paint_38_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_38, paint_38_fill);

    Path path_39 = Path();
    path_39.moveTo(size.width * 0.6588712, size.height * 0.8538160);
    path_39.cubicTo(
        size.width * 0.6558528,
        size.height * 0.8551595,
        size.width * 0.6537301,
        size.height * 0.8572025,
        size.width * 0.6525031,
        size.height * 0.8599387);
    path_39.cubicTo(
        size.width * 0.6512577,
        size.height * 0.8627117,
        size.width * 0.6512393,
        size.height * 0.8654479,
        size.width * 0.6524417,
        size.height * 0.8681534);
    path_39.lineTo(size.width * 0.6539141, size.height * 0.8714479);
    path_39.lineTo(size.width * 0.6639202, size.height * 0.8662331);
    path_39.cubicTo(
        size.width * 0.6664049,
        size.height * 0.8649018,
        size.width * 0.6681104,
        size.height * 0.8633804,
        size.width * 0.6690307,
        size.height * 0.8616748);
    path_39.cubicTo(
        size.width * 0.6699387,
        size.height * 0.8600000,
        size.width * 0.6699877,
        size.height * 0.8582638,
        size.width * 0.6691840,
        size.height * 0.8564601);
    path_39.cubicTo(
        size.width * 0.6683742,
        size.height * 0.8546319,
        size.width * 0.6670307,
        size.height * 0.8534724,
        size.width * 0.6651595,
        size.height * 0.8529693);
    path_39.cubicTo(
        size.width * 0.6632699,
        size.height * 0.8525092,
        size.width * 0.6611718,
        size.height * 0.8527914,
        size.width * 0.6588712,
        size.height * 0.8538160);
    path_39.close();
    path_39.moveTo(size.width * 0.6582270, size.height * 0.8459632);
    path_39.cubicTo(
        size.width * 0.6623374,
        size.height * 0.8441350,
        size.width * 0.6662025,
        size.height * 0.8438098,
        size.width * 0.6698405,
        size.height * 0.8449816);
    path_39.cubicTo(
        size.width * 0.6734479,
        size.height * 0.8461718,
        size.width * 0.6760798,
        size.height * 0.8486319,
        size.width * 0.6777423,
        size.height * 0.8523681);
    path_39.cubicTo(
        size.width * 0.6793681,
        size.height * 0.8560245,
        size.width * 0.6793129,
        size.height * 0.8595215,
        size.width * 0.6775644,
        size.height * 0.8628712);
    path_39.cubicTo(
        size.width * 0.6758282,
        size.height * 0.8662454,
        size.width * 0.6725399,
        size.height * 0.8691718,
        size.width * 0.6677055,
        size.height * 0.8716380);
    path_39.lineTo(size.width * 0.6565644, size.height * 0.8774110);
    path_39.lineTo(size.width * 0.6580000, size.height * 0.8806258);
    path_39.cubicTo(
        size.width * 0.6590368,
        size.height * 0.8829571,
        size.width * 0.6605828,
        size.height * 0.8844417,
        size.width * 0.6626442,
        size.height * 0.8850798);
    path_39.cubicTo(
        size.width * 0.6647178,
        size.height * 0.8857423,
        size.width * 0.6670675,
        size.height * 0.8854908,
        size.width * 0.6696871,
        size.height * 0.8843252);
    path_39.cubicTo(
        size.width * 0.6721227,
        size.height * 0.8832393,
        size.width * 0.6738712,
        size.height * 0.8818160,
        size.width * 0.6749264,
        size.height * 0.8800429);
    path_39.cubicTo(
        size.width * 0.6759509,
        size.height * 0.8782822,
        size.width * 0.6762393,
        size.height * 0.8763620,
        size.width * 0.6757853,
        size.height * 0.8742822);
    path_39.lineTo(size.width * 0.6838896, size.height * 0.8706748);
    path_39.cubicTo(
        size.width * 0.6853742,
        size.height * 0.8746442,
        size.width * 0.6851104,
        size.height * 0.8785399,
        size.width * 0.6830920,
        size.height * 0.8823558);
    path_39.cubicTo(
        size.width * 0.6810798,
        size.height * 0.8861718,
        size.width * 0.6775399,
        size.height * 0.8892086,
        size.width * 0.6724847,
        size.height * 0.8914601);
    path_39.cubicTo(
        size.width * 0.6674233,
        size.height * 0.8937117,
        size.width * 0.6628466,
        size.height * 0.8942577,
        size.width * 0.6587423,
        size.height * 0.8931043);
    path_39.cubicTo(
        size.width * 0.6546135,
        size.height * 0.8919571,
        size.width * 0.6516196,
        size.height * 0.8892883,
        size.width * 0.6497546,
        size.height * 0.8851043);
    path_39.lineTo(size.width * 0.6369325, size.height * 0.8563006);
    path_39.lineTo(size.width * 0.6451595, size.height * 0.8526380);
    path_39.lineTo(size.width * 0.6482147, size.height * 0.8595092);
    path_39.lineTo(size.width * 0.6484172, size.height * 0.8594233);
    path_39.cubicTo(
        size.width * 0.6485828,
        size.height * 0.8565890,
        size.width * 0.6495767,
        size.height * 0.8539387,
        size.width * 0.6513988,
        size.height * 0.8514785);
    path_39.cubicTo(
        size.width * 0.6531963,
        size.height * 0.8490245,
        size.width * 0.6554724,
        size.height * 0.8471902,
        size.width * 0.6582270,
        size.height * 0.8459632);
    path_39.close();

    Paint paint_39_fill = Paint()..style = PaintingStyle.fill;
    paint_39_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_39, paint_39_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}

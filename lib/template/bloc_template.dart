import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:li/logic/blocs/bonus/bonus_bloc.dart';
import 'package:li/logic/blocs/comments_of_club/commentsofclub_bloc.dart';

import 'package:li/logic/blocs/get_debt/getdebt_bloc.dart';
import 'package:li/logic/blocs/Payment/paymentBloc.dart';
import 'package:li/logic/blocs/Payment/paymentState.dart';
import 'package:li/logic/blocs/institution/InstitutionBloc.dart';
import 'package:li/logic/blocs/minute/MinuteInstiutionBloc.dart';
import 'package:li/logic/blocs/qfit_bloc/mainPage/MainPageBloc.dart';
import 'package:li/logic/blocs/register/RegisterBloc.dart';
import 'package:li/logic/providers/CreditCard.dart';
import 'package:li/logic/repository/PaymentsRepository.dart';
import 'package:li/main.dart';
import 'package:li/network/graphql_provider.dart';
import 'package:li/template/session_provider.dart';
import 'package:main_page/bloc/main_page_bloc.dart';
import 'package:profile/bloc/profile_bloc.dart';
import 'package:provider/provider.dart';
import 'package:take_away/logic/blocs/take_away/take_away_bloc.dart';

class BlocTemplate extends StatefulWidget {
  final Widget child;
  const BlocTemplate({
    required this.child,
    Key? key,
  }) : super(key: key);

  @override
  _BlocTemplateState createState() => _BlocTemplateState();
}

GetdebtBloc? getdebtBloc;

class _BlocTemplateState extends State<BlocTemplate> {
  late final List<BlocProvider> providers;
  StreamSubscription<PaymentState>? subscription;
  late final graphqlMinuteProvider = Provider.of<GraphqlMinuteProvider>(
    context,
    listen: false,
  );
  late final graphqlProvider = Provider.of<GraphqlProvider>(
    context,
    listen: false,
  );
  late final graphqlTakeAwayProvider = Provider.of<GraphqlTakeAwayProvider>(
    context,
    listen: false,
  );

  @override
  void dispose() {
    subscription?.cancel();
    super.dispose();
  }

  void savePayment(PaymentLoadingSuccessPrepare event) =>
      Provider.of<CreditCardProvider>(
        context,
        listen: false,
      ).saveCard(
        cardHolderName: event.cardHolderName,
        cardToken: event.token,
        cardType: event.cardType,
        last4Digits: event.cardNumberLast,
      );

  @override
  void initState() {
    super.initState();
    providers = [
      BlocProvider<RegisterBloc>(
        create: (_) => RegisterBloc(graphqlProvider),
      ),
      BlocProvider<InstitutionBloc>(
        create: (_) => InstitutionBloc(graphqlProvider),
      ),
      BlocProvider<MainPageBloc>(
        create: (_) => MainPageBloc(graphqlProvider),
      ),
      BlocProvider<MainPageMinuteBloc>(
        create: (_) => MainPageMinuteBloc(graphqlMinuteProvider),
      ),
      BlocProvider<MinuteInstitutionBloc>(
        create: (_) => MinuteInstitutionBloc(graphqlMinuteProvider),
      ),
      BlocProvider<CommentsofclubBloc>(
        create: (_) => CommentsofclubBloc(graphqlMinuteProvider),
      ),
      BlocProvider<PaymentBloc>(
        create: (_) {
          final _p = PaymentBloc(
            paymentRepository: PaymentRepository(),
          );
          subscription = _p.stream.listen((event) {
            if (event is PaymentLoadingSuccessPrepare) savePayment(event);
          });
          return _p;
        },
      ),
      BlocProvider<GetdebtBloc>(
        create: (_) {
          return getdebtBloc ??= GetdebtBloc(
            graphqlProvider,
            graphqlMinuteProvider,
          );
        },
      ),
      BlocProvider<ProfileBloc>(
        create: (_) => ProfileBloc(graphqlProvider: graphqlProvider),
      ),
      BlocProvider<TakeAwayBloc>(
        create: (_) => TakeAwayBloc(
          graphqlProvider,
          graphqlTakeAwayProvider,
        ),
      ),
      BlocProvider<BonusBloc>(
        create: (_) => BonusBloc(graphqlProvider),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: providers,
      child: SessionBlocProvider(child: widget.child),
    );
  }
}

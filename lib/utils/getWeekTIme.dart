import 'package:li/data/models/work_times.dart';
import 'dart:math' as math;

const weekTime = 3600000 * 24 * 7;
const startTime = 345600000;

WorkTimes? getWorkTime(List<WorkTimes>? _workTimes) {
  if (_workTimes == null || _workTimes.isEmpty) return null;
  final _currentTime = DateTime.fromMillisecondsSinceEpoch(
      ((DateTime.now().millisecondsSinceEpoch - startTime) % weekTime) +
          startTime);
  final firstPoint = _currentTime.subtract(Duration(days: 7));
  final secondPoint = _currentTime;
  final thirdPoint = _currentTime.add(Duration(days: 7));

  Duration _gDuration = Duration(days: 30);
  WorkTimes? gWorkTime;

  for (final workTime in _workTimes) {
    Duration durationDiff = workTime.startTime!.difference(firstPoint).abs();
    Duration durationDiff1 = workTime.startTime!.difference(secondPoint).abs();
    Duration durationDiff2 = workTime.startTime!.difference(thirdPoint).abs();

    if (durationDiff > durationDiff1) {
      durationDiff = durationDiff1;
    }
    if (durationDiff > durationDiff2) {
      durationDiff = durationDiff1;
    }

    if (_gDuration > durationDiff) {
      _gDuration = durationDiff;
      gWorkTime = workTime;
    }
  }
  return gWorkTime;
}

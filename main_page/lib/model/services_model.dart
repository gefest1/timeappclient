import 'package:flutter/material.dart';

class ServicesModel {
  final String title, countServices, svgPath;
  final Color colorCategory;
  const ServicesModel(
      {required this.title,
      required this.countServices,
      required this.svgPath,
      required this.colorCategory});
}

class ServicesRepository {
  static List<ServicesModel> getServicesList() => const [
        ServicesModel(
            title: 'Ремонт смартфонов',
            countServices: 'Спортивные залы, катки, развлечения',
            svgPath: 'assets/svg/category/category/category_repair_phone.svg',
            colorCategory: Color(0xFFD4D8E1)),
        ServicesModel(
            title: 'Кваритиры посуточно',
            countServices: 'Спортивные залы, катки, развлечения',
            svgPath: 'assets/svg/category/category/category_rent_apart.svg',
            colorCategory: Color(0xFF9EDBC4)),
        ServicesModel(
            title: 'Еда на вынос',
            countServices: 'Спортивные залы, катки, развлечения',
            svgPath: 'assets/svg/category/main/categories_homemade_food.svg',
            colorCategory: Color(0xFF00DDEB)),
        ServicesModel(
            title: 'Ремонт часов',
            countServices: 'Спортивные залы, катки, развлечения',
            svgPath: 'assets/svg/category/category/category_repair_clock.svg',
            colorCategory: Color(0xFF48D6A3)),
        ServicesModel(
            title: 'Маникюр и педикюр',
            countServices: 'Спортивные залы, катки, развлечения',
            svgPath: 'assets/svg/category/main/categories_services.svg',
            colorCategory: Color(0xFFFFDA90)),
        ServicesModel(
            title: 'Услуги Нотариуса',
            countServices: 'Спортивные залы, катки, развлечения',
            svgPath: 'assets/svg/category/juridical.svg',
            colorCategory: Color(0xFFFF8376)),
        ServicesModel(
            title: 'Шиномонтаж',
            countServices: 'Спортивные залы, катки, развлечения',
            svgPath: 'assets/svg/category/autoService.svg',
            colorCategory: Color(0xFFA3A3A3)),
        ServicesModel(
            title: 'Playstation клубы',
            countServices: 'Спортивные залы, катки, развлечения',
            svgPath: 'assets/svg/category/category/category_gaming.svg',
            colorCategory: Color(0xFF3F4D71)),
        ServicesModel(
            title: 'Консультация терапевта',
            countServices: 'Спортивные залы, катки, развлечения',
            svgPath: 'assets/svg/category/medicine.svg',
            colorCategory: Color(0xFFEFE6D2)),
      ];
}

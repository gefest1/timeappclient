import 'package:flutter/foundation.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/data/models/institution_minute.dart';
import 'package:li/logic/providers/geo_institution/geo_api/getRadius.dart';
import 'package:li/network/graphql_provider.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

// REACTIONS
part 'reactions/GetInstitution.dart';
part 'reactions/ImportInstitution.dart';

class GeoInstitutionProvider extends ChangeNotifier {
  final GraphqlMinuteProvider graphqlMinuteProvider;

  final Map<String, InstitutionsMinute> institutionsMinuteMap =
      Map<String, InstitutionsMinute>();

  List<InstitutionsMinute> institutionsMinuteList = [];

  GraphQLClient get client => graphqlMinuteProvider.client;

  GeoInstitutionProvider({
    required this.graphqlMinuteProvider,
  });
}

import 'package:equatable/equatable.dart';

String getCardType(String number) {
  // visa
  var re = new RegExp("^4");

  if (re.hasMatch(number)) return "Visa";

  // Mastercard
  // Updated for Mastercard 2017 BINs expansion
  re = RegExp(
      "(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))");
  if (re.hasMatch(number)) return "Mastercard";

  // AMEX
  re = new RegExp("^3[47]");
  if (re.hasMatch(number)) return "AMEX";

  // Discover
  re = new RegExp(
      "^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
  if (re.hasMatch(number)) return "Discover";

  // Diners
  re = new RegExp("^36");
  if (re.hasMatch(number)) return "Diners";

  // Diners - Carte Blanche
  re = new RegExp("^30[0-5]");
  if (re.hasMatch(number)) return "Diners - Carte Blanche";

  // JCB
  re = new RegExp("^35(2[89]|[3-8][0-9])");
  if (re.hasMatch(number)) return "JCB";

  // Visa Electron
  re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
  if (re.hasMatch(number)) return "Visa Electron";

  return "";
}

abstract class PaymentState extends Equatable {
  const PaymentState();

  @override
  List<Object> get props => [];
}

class PaymentInitial extends PaymentState {}

class PaymentCredentialsFailure extends PaymentState {
  final String errorMessage;

  PaymentCredentialsFailure({required this.errorMessage});
}

class PaymentLoadingInProgress extends PaymentState {}

class PaymentLoadingSuccess extends PaymentState {
  const PaymentLoadingSuccess({
    required this.amount,
    this.cardHolderMessage = '',
    required this.transactionId,
    required this.token,
  });

  final int amount;
  final String cardHolderMessage;
  final int transactionId;
  final String token;
}

class PaymentLoadingSuccessPrepare extends PaymentLoadingSuccess {
  const PaymentLoadingSuccessPrepare({
    required final int amount,
    required final String cardHolderMessage,
    required final int transactionId,
    required final String token,
    required this.cardHolderName,
    required this.cardNumberLast,
    required this.cardType,
  }) : super(
          amount: amount,
          cardHolderMessage: cardHolderMessage,
          transactionId: transactionId,
          token: token,
        );
  final String cardHolderName;
  final String cardNumberLast;
  final String cardType;
}

class PaymentException extends PaymentState {
  const PaymentException({required this.errorMessage});

  final String errorMessage;
}

class PaymentLoadingFailure extends PaymentException {
  const PaymentLoadingFailure({required String errorMessage})
      : super(
          errorMessage: errorMessage,
        );
}

class PaymentThreeDsRequested extends PaymentState {}

class GetCryptoGramSaved extends PaymentState {
  final String cryptogram;
  final String savedCode;

  const GetCryptoGramSaved(this.cryptogram, this.savedCode);
}

class SavedCryptoGramState extends PaymentState {
  final String cardNumber;
  final String expireDate;
  final String crypto;
  final String type;
  final String cardHolderName;

  const SavedCryptoGramState(
    this.cardNumber,
    this.expireDate,
    this.crypto,
    this.type,
    this.cardHolderName,
  );
}

class CryptFailure extends PaymentException {
  CryptFailure({required String errorMessage})
      : super(errorMessage: errorMessage);
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class BigCustomTextField extends StatefulWidget {
  final FocusNode? focusNode;
  final String? label;
  final TextEditingController? controller;
  final TextInputType? keyboardType;
  final List<TextInputFormatter>? inputFormatters;

  const BigCustomTextField({
    this.focusNode,
    this.controller,
    this.label,
    this.keyboardType,
    this.inputFormatters,
    Key? key,
  }) : super(key: key);

  @override
  _BigCustomTextFieldState createState() => _BigCustomTextFieldState();
}

class _BigCustomTextFieldState extends State<BigCustomTextField> {
  bool isChoosen = false;

  late final FocusNode focusNode = (widget.focusNode ?? FocusNode())
    ..addListener(() {
      if (focusNode.hasFocus && !isChoosen) {
        setState(() {
          isChoosen = true;
        });
      } else if (!focusNode.hasFocus && isChoosen) {
        setState(() {
          isChoosen = false;
        });
      }
    });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: isChoosen
          ? BoxDecoration(
              color: Color(0xffF9FBFF),
              borderRadius: BorderRadius.circular(16),
              border: Border.all(
                width: 0.5,
                color: Color(0xff0F84F4),
              ),
            )
          : BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              color: Color(0xffFAFAFA),
              border: Border.all(
                width: 0,
                color: Colors.transparent,
              ),
            ),
      padding: EdgeInsets.only(left: 20, right: 20, top: 4),
      child: TextFormField(
        focusNode: focusNode,

        validator: (_) => _!.length != 15 ? 'Заполните номер' : null,
        style: const TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: 20,
        ),
        minLines: 6,
        maxLines: 8,

        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: 'Все, о чем хотите поделится с клиентами',
          prefixStyle: TextStyle(
            color: const Color(0xff434343),
          ),
        ),
      ),
    );
  }
}

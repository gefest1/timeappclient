import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'dart:math' as math;

class SliverContainer extends ConstrainedLayoutBuilder<SliverConstraints> {
  /// Creates a sliver widget that defers its building until layout.
  ///
  /// The [builder] argument must not be null.
  const SliverContainer({
    Key? key,
    required SliverLayoutWidgetBuilder builder,
  }) : super(key: key, builder: builder);

  /// Called at layout time to construct the widget tree.
  ///
  /// The builder must return a non-null sliver widget.
  @override
  SliverLayoutWidgetBuilder get builder => super.builder;

  @override
  RenderObject createRenderObject(BuildContext context) =>
      _RenderSliverLayoutBuilder();
}

class _RenderSliverLayoutBuilder extends RenderSliver
    with
        RenderObjectWithChildMixin<RenderSliver>,
        RenderConstrainedLayoutBuilder<SliverConstraints, RenderSliver> {
  @override
  double childMainAxisPosition(RenderObject child) {
    assert(child == this.child);
    return 0;
  }

  @override
  void performLayout() {
    rebuildIfNecessary();
    child?.layout(constraints, parentUsesSize: true);
    geometry = child?.geometry ?? SliverGeometry.zero;
  }

  @override
  void applyPaintTransform(RenderObject child, Matrix4 transform) {
    assert(child == this.child);
    // child's offset is always (0, 0), transform.translate(0, 0) does not mutate the transform.
  }

  @override
  void paint(PaintingContext context, Offset offset) {
    if (child?.geometry?.visible == true) {
      final _rect = RRect.fromLTRBAndCorners(
        0,
        0 + offset.dy,
        constraints.crossAxisExtent,
        math.max((geometry?.cacheExtent ?? 0),
                constraints.remainingPaintExtent) +
            offset.dy,
        topLeft: const Radius.circular(15),
        topRight: const Radius.circular(15),
      );
      context.canvas.drawRRect(
        _rect,
        const BoxShadow(
          offset: Offset(0, -5),
          blurRadius: 30,
          color: Color(0x0d323232),
        ).toPaint(),
      );
      context.canvas.drawRRect(
        _rect,
        Paint()..color = Colors.white,
      );
      context.paintChild(child!, offset);
    }
  }

  @override
  bool hitTestChildren(SliverHitTestResult result,
      {required double mainAxisPosition, required double crossAxisPosition}) {
    return child != null &&
        child!.geometry!.hitTestExtent > 0 &&
        child!.hitTest(result,
            mainAxisPosition: mainAxisPosition,
            crossAxisPosition: crossAxisPosition);
  }
}

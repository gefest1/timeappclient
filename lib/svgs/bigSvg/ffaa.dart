import 'package:flutter/material.dart';

import 'package:li/svgs/bigSvg/paths/ffa_path0.dart';
import 'package:li/svgs/bigSvg/paths/ffa_path1.dart';
import 'package:li/svgs/bigSvg/paths/ffa_path2.dart';
import 'package:li/svgs/bigSvg/paths/ffa_path3.dart';
import 'package:li/svgs/bigSvg/paths/ffa_path4.dart';

class RPSCustomPainter extends CustomPainter {
  const RPSCustomPainter({Listenable? repaint}) : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawPath(
      getPath0(size),
      Paint()
        ..style = PaintingStyle.fill
        ..color = Color(0xff060806),
    );

    canvas.drawPath(
        getPath1(size),
        Paint()
          ..style = PaintingStyle.fill
          ..color = Color(0xff12c819));

    canvas.drawPath(
      getPath2(size),
      Paint()
        ..style = PaintingStyle.fill
        ..color = Color(0xfffbfbfb),
    );

    canvas.drawPath(
      getPath3(size),
      Paint()
        ..style = PaintingStyle.fill
        ..color = Color(0xff7c7c7c),
    );

    canvas.drawPath(
      getPath4(size),
      Paint()
        ..style = PaintingStyle.fill
        ..color = Color(0xff9a9a9a),
    );
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/data/models/credit_card_model.dart';
import 'package:li/main.dart';

import 'profile_list_page.dart';

class ProfileIntro extends StatelessWidget {
  // final ValueNotifier<CreditCardModel?> creditCard;
  const ProfileIntro({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: ListView(
          children: [
            const SizedBox(
              height: 80,
            ),
            RepaintBoundary(
                child: SvgPicture.asset('assets/svg/icon/IntroArt.svg')),
            const SizedBox(height: 30),
            const Text(
              'Добро пожаловать в TimeApp!',
              style: TextStyle(
                fontSize: 38,
                fontWeight: FontWeight.w600,
              ),
            ),
            const SizedBox(height: 10),
            const Text(
              'Для записи на услуги необходимо создать профиль.',
              style: TextStyle(fontSize: 18),
            ),
            const SizedBox(height: 20),
            MaterialButton(
              height: 60,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15),
              ),
              color: const Color(0xff00D72C),
              onPressed: () {
                routerDelegate.push(const ProfileListPage());
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RepaintBoundary(
                      child: SvgPicture.asset('assets/svg/icon/PhoneIcon.svg')),
                  const SizedBox(width: 5),
                  const Text(
                    'Войти с номером телефона',
                    style: TextStyle(
                      color: Color(0xffFCFCFC),
                      fontSize: 18,
                    ),
                  )
                ],
              ),
            ),
            const SizedBox(height: 20),
            GestureDetector(
              onTap: () {},
              child: Center(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    RepaintBoundary(
                        child: SvgPicture.asset(
                            'assets/svg/icon/SupportIcon.svg')),
                    const SizedBox(width: 5),
                    const Text(
                      'Поддержка',
                      style: TextStyle(
                        fontSize: 18,
                        color: Color(0xff979797),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 40,
            ),
          ],
        ),
      ),
    );
  }
}

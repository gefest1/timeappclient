import 'dart:async';
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/text_styles.dart';
import 'package:time_app_components/utils/testStyles.dart';

class ActiveTimerSession extends StatefulWidget {
  final DateTime startTime;
  final num price;
  final int bufferTime;

  const ActiveTimerSession({
    required this.startTime,
    required this.price,
    required this.bufferTime,
    Key? key,
  }) : super(key: key);

  @override
  _ActiveTimerSessionState createState() => _ActiveTimerSessionState();
}

class _ActiveTimerSessionState extends State<ActiveTimerSession>
    with WidgetsBindingObserver {
  late String? textAmount, textSeconds;

  Timer? _timer;

  @override
  void initState() {
    startTimer();
    WidgetsBinding.instance?.addObserver(this);
    super.initState();
  }

  void startTimer() {
    _timer?.cancel();

    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (mounted)
          setState(() {});
        else {
          if (_timer?.isActive ?? false) {
            _timer?.cancel();
            _timer = null;
          }
        }
      },
    );
  }

  @override
  void dispose() {
    _timer?.cancel();
    _timer = null;
    WidgetsBinding.instance?.addObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (AppLifecycleState.paused == state) {
      _timer?.cancel();
      _timer = null;
    }
    if (AppLifecycleState.resumed == state) {
      startTimer();
    }
  }

  @override
  Widget build(BuildContext context) {
    final _diff = DateTime.now().difference(widget.startTime);
    Duration _priceDuration = _diff;

    _priceDuration = Duration(
      milliseconds: math.max(
          0, (_diff - Duration(minutes: widget.bufferTime)).inMilliseconds),
    );

    return Row(
      children: [
        TextTimer(label: (_priceDuration.inMinutes * widget.price).toString()),
        SizedBox(
          width: 10,
        ),
        Expanded(
          child: Container(
            decoration: BoxDecoration(
              color: ColorData.allMessagesbackground,
              borderRadius: BorderRadius.circular(16),
            ),
            padding: EdgeInsets.all(20),
            child: FittedBox(
              child: Text(
                _diff.inHours > 0
                    ? (_diff.toString()).substring(0, 7)
                    : (_diff.toString()).substring(2, 7),
                style: TextStyle(
                  fontSize: H1TextStyle.fontSize,
                  fontWeight: H1TextStyle.fontWeight,
                  height: H1TextStyle.height,
                  color: ColorData.allMainBlack,
                ),
              ),
            ),
          ),
        )
        // (hours == 0)
      ],
    );
  }
}

class TextTimer extends StatelessWidget {
  final String label;
  const TextTimer({
    required this.label,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        decoration: BoxDecoration(
          color: ColorData.allMessagesbackground,
          borderRadius: BorderRadius.circular(16),
        ),
        padding: EdgeInsets.all(20),
        child: FittedBox(
          child: Text(
            '$label тг',
            style: TextStyle(
              fontWeight: H1TextStyle.fontWeight,
              height: H1TextStyle.height,
              color: ColorData.allMainBlack,
            ),
          ),
        ),
      ),
    );
  }
}

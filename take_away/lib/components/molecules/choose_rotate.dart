import 'package:flutter/material.dart';
import 'package:li/components/icons/motorcycle.dart';
import 'package:li/components/icons/profile.dart';
import 'package:take_away/components/molecules/rotate_circular_box.dart';
import 'package:time_app_components/utils/color.dart';
import 'dart:math' as math;

// TODO NOT FINISHED CODE

class ChooseRotate extends StatelessWidget {
  const ChooseRotate({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, BoxConstraints constraints) {
        final _aspec = math.min((constraints.maxWidth - 10) / 2, 163.0);
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: const BoxDecoration(
                color: ColorData.clientsButtonColorDefault,
                shape: BoxShape.circle,
              ),
              height: _aspec,
              width: _aspec,
              child: Stack(
                children: [
                  RotateCircularBox(
                    size: _aspec,
                  ),
                  Center(
                    child: SizedBox(
                      height: (58 / 163) * _aspec,
                      width: (58 / 163) * _aspec,
                      child: const CustomPaint(
                        size: Size(58, 58),
                        painter: ProfileCustomPainter(
                          color: Colors.white,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            const SizedBox(width: 10),
            Container(
              decoration: const BoxDecoration(
                color: Color(0xff434343),
                shape: BoxShape.circle,
              ),
              height: _aspec,
              width: _aspec,
              child: Stack(
                children: [
                  RotateCircularBox1(
                    size: _aspec,
                  ),
                  Container(
                    child: Center(
                      child: SizedBox(
                        height: (58 / 163) * _aspec,
                        width: (58 / 163) * _aspec,
                        child: CustomPaint(
                          size: Size(
                            (58 / 163) * _aspec,
                            (58 / 163) * _aspec,
                          ),
                          painter: const MotorCycleCustomPainter(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        );
      },
    );
  }
}

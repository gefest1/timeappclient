import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/const.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class ShippingTypeCard extends StatefulWidget {
  final String title;
  final String subTitle;
  final String svg;
  final VoidCallback ontap;
  const ShippingTypeCard(
      {required this.ontap,
      required this.subTitle,
      required this.svg,
      required this.title,
      Key? key})
      : super(key: key);

  @override
  State<ShippingTypeCard> createState() => _ShippingTypeCardState();
}

class _ShippingTypeCardState extends State<ShippingTypeCard> {
  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: GestureDetector(
        onTap: widget.ontap,
        child: Container(
          decoration: BoxDecoration(
            boxShadow: bigBlur,
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
          ),
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                RepaintBoundary(
                  child: SvgPicture.asset(
                    widget.svg,
                  ),
                ),
                Text(
                  widget.title,
                  style: TextStyle(
                    fontSize: H3TextStyle.fontSize,
                    fontWeight: H3TextStyle.fontWeight,
                    height: H3TextStyle.height,
                  ),
                ),
                Text(
                  widget.subTitle,
                  style: TextStyle(
                    fontSize: P3TextStyle.fontSize,
                    fontWeight: P3TextStyle.fontWeight,
                    height: P3TextStyle.height,
                    color: ColorData.allMainActivegray,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

part of 'commentsofclub_bloc.dart';

abstract class CommentsofclubEvent extends Equatable {
  const CommentsofclubEvent();

  @override
  List<Object> get props => [];
}

class GetCommentsOfClubEvent extends CommentsofclubEvent {
  final String institutionId;
  const GetCommentsOfClubEvent({
    required this.institutionId,
  });
}

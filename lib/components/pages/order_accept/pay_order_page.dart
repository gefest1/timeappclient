import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'package:li/components/atoms/medium_sized_card.dart';
import 'package:li/components/icons/location.dart';
import 'package:li/components/molecules/map/address_choose.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/components/pages/order_accept/order_preparation_page.dart';
import 'package:li/components/pages/order_accept/pay_order/choosen_dish.dart';
import 'package:li/components/pages/order_accept/pay_order/receipt_column.dart';
import 'package:li/components/pages/qfit_to_timeapp/start_train/card_data.dart';
import 'package:li/const.dart';
import 'package:li/data/models/credit_card_model.dart';
import 'package:li/main.dart';
import 'package:li/utils/color.dart';
import 'package:provider/provider.dart';
import 'package:take_away/logic/provider/institution/institution_provider.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/utils/testStyles.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';
import 'package:dio/dio.dart';

class AddressData {
  final num price;
  final String address;
  final double longitude;
  final double latitude;

  const AddressData({
    required this.address,
    required this.latitude,
    required this.longitude,
    required this.price,
  });
}

class PayOrderPage extends StatefulWidget {
  final String institutionId;

  const PayOrderPage({required this.institutionId, Key? key}) : super(key: key);

  @override
  _PayOrderPageState createState() => _PayOrderPageState();
}

class _PayOrderPageState extends State<PayOrderPage> {
  final ValueNotifier<AddressData?> addressData =
      ValueNotifier<AddressData?>(null);
  YandexMapController? yandexMapController;
  final ValueNotifier<CreditCardModel?> creditCard =
      ValueNotifier<CreditCardModel?>(null);

  void onMapTap(Point point) async {
    final _provider =
        Provider.of<InstitutionTakeAwayProvider>(context, listen: false);
    final _searchREsulr = YandexSearch.searchByPoint(
        point: point, searchOptions: SearchOptions());
    SearchSessionResult _searchSessionResult = await _searchREsulr.result;
    final dio = Dio();
    try {
      final _responce = await dio.post(
        'https://b2b.taxi.yandex.net/b2b/cargo/integration/v1/check-price',
        data: {
          "items": [
            {
              "quantity": 1,
              "size": {"height": 0.1, "length": 0.1, "width": 0.1},
              "weight": 0.1
            }
          ],
          "route_points": [
            {
              "coordinates": [point.longitude, point.latitude]
            },
            {
              "coordinates": [
                _provider.institutionTakeAwayMap[widget.institutionId]!
                    .location!.longitude,
                _provider.institutionTakeAwayMap[widget.institutionId]!
                    .location!.latitude
              ]
            }
          ]
        },
        options: Options(
          headers: {
            'Authorization': 'Bearer AgAAAABR-4CdAAVM1dG2nH-A2EWJiXO5-A-G3B0',
            "Accept-Language": 'ru'
          },
        ),
      );
      setState(() {
        if (_searchSessionResult.items != null &&
            _searchSessionResult.items!.isNotEmpty) {
          addressData.value = AddressData(
            address: _searchSessionResult.items!.first.name,
            latitude: point.latitude,
            longitude: point.longitude,
            price: num.tryParse(_responce.data['price'])!,
          );
        }

        // courierPrice = num.tryParse(_responce.data['price']);
      });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
          color: Color(0xff14142B),
        ),
        title: Text(
          'Оплата заказа',
          style: TextStyle(
            color: ColorData.allMainBlack,
          ),
        ),
      ),
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: CustomScrollView(
              clipBehavior: Clip.none,
              slivers: [
                SliverToBoxAdapter(
                  child: const SizedBox(
                    height: 25,
                  ),
                ),
                SliverToBoxAdapter(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Container(
                      height: 340,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Stack(
                        children: [
                          YandexMap(
                            gestureRecognizers: {
                              Factory<OneSequenceGestureRecognizer>(
                                () => EagerGestureRecognizer(),
                              )
                            },
                            fastTapEnabled: true,
                            tiltGesturesEnabled: false,
                            indoorEnabled: true,
                            onCameraPositionChanged:
                                (cameraPosition, reason, finished) {},
                            onMapCreated: (controller) async {
                              yandexMapController = controller;

                              await yandexMapController!.moveCamera(
                                CameraUpdate.newCameraPosition(
                                  CameraPosition(
                                    target: Point(
                                      latitude: 43.2353544,
                                      longitude: 76.896372,
                                    ),
                                    zoom: 12.2,
                                  ),
                                ),
                                animation: MapAnimation(
                                  duration: 0,
                                ),
                              );
                            },
                            onMapTap: onMapTap,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SliverToBoxAdapter(
                  child: const SizedBox(
                    height: 20,
                  ),
                ),
                SliverToBoxAdapter(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Адрес доставки',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w600,
                          color: ColorData.allMainBlack,
                        ),
                      ),
                      SizedBox(height: 10),
                      SizeTapAnimation(
                        child: GestureDetector(
                          onTap: () {
                            showModalBottomSheet(
                              isScrollControlled: true,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              backgroundColor: Colors.white,
                              context: context,
                              builder: (context) {
                                return AddressChoose();
                              },
                            );
                          },
                          child: MediumSizedCard(
                            subtitle: 'изменить',
                            title: addressData.value?.address ??
                                'Укажите адрес доставки',
                            icon: CircleAvatar(
                              radius: 20,
                              backgroundColor: ColorData.clientsIcons,
                              child: Center(
                                child: CustomPaint(
                                  size: Size(20, 20),
                                  painter: LocationCustomPainter(),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SliverToBoxAdapter(
                  child: const SizedBox(
                    height: 40,
                  ),
                ),
                SliverToBoxAdapter(
                  child: ChoosenDish(),
                ),
                SliverToBoxAdapter(
                  child: const SizedBox(
                    height: 40,
                  ),
                ),
                SliverToBoxAdapter(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Оплата',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w600,
                          color: ColorData.allMainBlack,
                        ),
                      ),
                      SizedBox(height: 10),
                      CardData(creditCard: creditCard),
                    ],
                  ),
                ),
                SliverToBoxAdapter(
                  child: const SizedBox(
                    height: 40,
                  ),
                ),
                SliverToBoxAdapter(
                  child: ReceiptColumn(),
                ),
                SliverToBoxAdapter(
                  child: const SizedBox(
                    height: 200,
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              height: MediaQuery.of(context).padding.bottom + 100,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(16),
                  topRight: Radius.circular(16),
                ),
                boxShadow: bigBlur,
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                  top: 20.0,
                  bottom: 20,
                  left: 20,
                  right: 20,
                ),
                child: ElevatedButton(
                  onPressed: () => routerDelegate.push(OrderPrepPage()),
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(Color(0xffB4B4B4)),
                      fixedSize: MaterialStateProperty.all(Size(60, 60)),
                      elevation: MaterialStateProperty.all(0.0),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(16),
                        ),
                      )),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Выберите адрес доставки',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: P2TextStyle.fontSize,
                          height: P2TextStyle.height,
                          fontWeight: P2TextStyle.fontWeight,
                        ),
                      ),
                      Text(
                        'Нажмите, чтобы продолжить',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: P4TextStyle.fontSize,
                          height: P4TextStyle.height,
                          fontWeight: P4TextStyle.fontWeight,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

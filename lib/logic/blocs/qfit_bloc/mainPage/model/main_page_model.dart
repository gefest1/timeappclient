import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:li/data/models/allowed_institution_types.dart';
import 'package:li/data/models/institution_minute.dart';

import 'package:li/utils/translate.dart';

class MainPageModel extends Equatable {
  final String? id;
  final List<InstitutionsMinute>? institutions;
  final num? numberOfInstitutions;
  final AllowedInstitutionTypes? slug;
  final Translate? title;

  MainPageModel({
    this.id,
    this.institutions,
    this.numberOfInstitutions,
    this.slug,
    this.title,
  });

  MainPageModel copyWith({
    String? id,
    List<InstitutionsMinute>? institutions,
    num? numberOfInstitutions,
    AllowedInstitutionTypes? slug,
    Translate? title,
  }) {
    return MainPageModel(
      id: id ?? this.id,
      institutions: institutions ?? this.institutions,
      numberOfInstitutions: numberOfInstitutions ?? this.numberOfInstitutions,
      slug: slug ?? this.slug,
      title: title ?? this.title,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'institutions': institutions?.map((x) => x.toMap()).toList(),
      'numberOfInstitutions': numberOfInstitutions,
      'slug': slug?.toString(),
      'title': title?.toMap(),
    };
  }

  factory MainPageModel.fromMap(Map<String, dynamic> map) {
    return MainPageModel(
      id: map['_id'] != null ? map['_id'] : null,
      institutions: map['institutions'] != null
          ? List<InstitutionsMinute>.from(
              map['institutions']?.map((x) => InstitutionsMinute.fromMap(x)))
          : null,
      numberOfInstitutions: map['numberOfInstitutions'] != null
          ? map['numberOfInstitutions']
          : null,
      slug: map['slug'] != null
          ? AllowedInstitutionTypes.fromMap(map['slug'])
          : null,
      title: map['title'] != null ? Translate.fromMap(map['title']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory MainPageModel.fromJson(String source) =>
      MainPageModel.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object?> get props {
    return [
      id,
      institutions,
      numberOfInstitutions,
      slug,
      title,
    ];
  }
}

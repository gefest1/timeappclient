import 'dart:async';

import 'package:flutter/material.dart';

import 'drawn_line.dart';
import 'sketcher.dart';

late Offset firstOffset;

class DrawingPage extends StatefulWidget {
  @override
  _DrawingPageState createState() => _DrawingPageState();
}

class _DrawingPageState extends State<DrawingPage> {
  final GlobalKey _globalKey = GlobalKey();
  DrawnLine? line;
  final Color? selectedColor = Color(0xff00AB28);
  final double selectedWidth = 2.0;

  StreamController<DrawnLine> linesStreamController =
      StreamController<DrawnLine>.broadcast();

  Future<void> clear() async {
    setState(() {
      line = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return buildCurrentPath(context);
  }

  Widget buildCurrentPath(BuildContext context) {
    return GestureDetector(
      onPanStart: onPanStart,
      onPanUpdate: onPanUpdate,
      onPanEnd: onPanEnd,
      child: RepaintBoundary(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          padding: EdgeInsets.all(4.0),
          color: Colors.transparent,
          alignment: Alignment.topLeft,
          child: StreamBuilder<DrawnLine>(
            stream: linesStreamController.stream,
            builder: (context, snapshot) {
              return CustomPaint(
                painter: Sketcher(
                  line: line,
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Widget buildAllPaths(BuildContext context) {
    return RepaintBoundary(
      key: _globalKey,
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.transparent,
        padding: EdgeInsets.all(4.0),
        alignment: Alignment.topLeft,
        child: StreamBuilder<DrawnLine>(
          stream: linesStreamController.stream,
          builder: (context, snapshot) {
            return CustomPaint(
              painter: Sketcher(
                line: line,
              ),
            );
          },
        ),
      ),
    );
  }

  void onPanStart(DragStartDetails details) {
    line = null;
    line = DrawnLine(
      path: [details.localPosition],
      color: selectedColor!,
      width: selectedWidth,
      fillColor: Color(0x3300D63C),
    );
    firstOffset = details.globalPosition;
  }

  void onPanUpdate(DragUpdateDetails details) {
    line!.path.add(details.localPosition);
    linesStreamController.add(line!);
  }

  void onPanEnd(DragEndDetails details) {
    line!.path.add(firstOffset);
    linesStreamController.add(line!);
  }
}

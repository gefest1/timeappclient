import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/data/models/service_model.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class CardInstitutionShort extends StatelessWidget {
  final ServiceMinute service;
  final String priceTitle;

  const CardInstitutionShort({
    required this.service,
    required this.priceTitle,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: Color(0xffF8F8F8),
        borderRadius: BorderRadius.circular(16),
      ),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (service.institution?.name != null)
                      Text(
                        service.institution!.name!,
                        style: TextStyle(
                          fontSize: H2TextStyle.fontSize,
                          fontWeight: H2TextStyle.fontWeight,
                          height: H2TextStyle.height,
                          color: ColorData.allMainBlack,
                        ),
                      ),
                    const SizedBox(height: 2),
                    if (service.institution?.location!.address != null)
                      Text(
                        service.institution!.location!.address!,
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          height: 17 / 14,
                          color: ColorData.allMainBlack,
                        ),
                      ),
                    // if (service.institution?.description != null)
                    //   Text(
                    //     service.institution!.description!,
                    //     style: TextStyle(
                    //       fontSize: 14,
                    //       fontWeight: FontWeight.w400,
                    //       height: 17 / 14,
                    //       color: ColorData.allMainActivegray,
                    //     ),
                    //   ),
                  ],
                ),
              ),
              const SizedBox(width: 10),
              if (service.institution?.iconPath != null)
                CircleAvatar(
                  radius: 20,
                  backgroundColor: ColorData.clientsIcons,
                  child: Center(
                    child: RepaintBoundary(
                      child: SvgPicture.network(service.institution!.iconPath!),
                    ),
                  ),
                )
            ],
          ),
          const SizedBox(height: 10),
          Row(
            children: [
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    color: ColorData.allMainBlack,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  padding: EdgeInsets.symmetric(
                    vertical: 6,
                    horizontal: 16,
                  ),
                  alignment: Alignment.center,
                  child: FittedBox(
                    child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: priceTitle,
                            style: TextStyle(
                              fontSize: P3TextStyle.fontSize,
                              fontWeight: P3TextStyle.fontWeight,
                              height: P3TextStyle.height,
                              color: Colors.white,
                              fontFamily:
                                  Platform.isIOS ? 'SfProDisplay' : 'Roboto',
                            ),
                          ),
                          TextSpan(
                            text: ' ₸/мин',
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                              height: 14 / 12,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: Container(
                  height: 30,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  alignment: Alignment.center,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: service.institution?.rating == null
                        ? []
                        : [
                            RepaintBoundary(
                              child: SvgPicture.asset(
                                'assets/svg/icon/star.svg',
                                color: Color(0xff00DF31),
                                height: 16,
                                width: 16,
                              ),
                            ),
                            const SizedBox(width: 2),
                            Text(
                              service.institution!.rating!.toStringAsFixed(1),
                              style: TextStyle(
                                color: const Color(0xff00DF31),
                                fontSize: P3TextStyle.fontSize,
                                fontWeight: P3TextStyle.fontWeight,
                                height: P3TextStyle.height,
                              ),
                            ),
                          ],
                  ),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: Container(
                  height: 30,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      RepaintBoundary(
                        child: SvgPicture.asset(
                          'assets/svg/icon/bullHorn.svg',
                          color: ColorData.allMainBlack,
                          height: 16,
                          width: 16,
                        ),
                      ),
                      const SizedBox(width: 2),
                      Text(
                        (service.institution?.ratingCount ?? '0').toString(),
                        style: TextStyle(
                          color: ColorData.allMainBlack,
                          fontSize: P3TextStyle.fontSize,
                          fontWeight: P3TextStyle.fontWeight,
                          height: P3TextStyle.height,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

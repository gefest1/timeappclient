import 'package:flutter/material.dart';
import 'package:take_away/components/atoms/atc_text.dart';
import 'dart:math' as math;

import 'package:take_away/components/molecules/rotate_animated.dart';
import 'package:take_away/components/molecules/rt_text.dart';
import 'package:take_away/components/molecules/tr1_text.dart';

class RotateCircularBox extends StatelessWidget {
  final double size;

  const RotateCircularBox({
    this.size = 163,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RotateAnimation(
      child: CustomPaint(
        size: Size(size, size),
        painter: Text1TypeCustomPainter(),
      ),
    );
  }
}

class RotateCircularBox1 extends StatelessWidget {
  final double size;

  const RotateCircularBox1({
    this.size = 163,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RotateAnimation(
      child: CustomPaint(
        size: Size(size * 1.1, size * 1.1),
        painter: Text2TypeCustomPainter(),
      ),
    );
  }
}

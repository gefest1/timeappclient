part of 'profile_bloc.dart';

abstract class ProfileEvent extends Equatable {
  const ProfileEvent();

  @override
  List<Object> get props => [];
}

class GetClient extends ProfileEvent {}

class EditClient extends ProfileEvent {
  final File? avatar;
  final String? name;
  final SexEnum? sex;

  const EditClient(this.avatar, this.name, this.sex);
}

class DeleteClientEvent extends ProfileEvent {}

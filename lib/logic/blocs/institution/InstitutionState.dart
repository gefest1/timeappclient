import 'package:equatable/equatable.dart';

abstract class InstitutionState extends Equatable {
  @override
  List<Object?> get props => [];
}

class InitInstitutionState extends InstitutionState {}

import 'package:flutter/material.dart';
import 'package:li/components/atoms/little_button.dart';
import 'package:li/components/molecules/execution_info.dart';
import 'package:li/components/molecules/tab_search.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';

class ReturnSearchPage extends StatefulWidget {
  const ReturnSearchPage({Key? key}) : super(key: key);

  @override
  _ReturnSearchPageState createState() => _ReturnSearchPageState();
}

class _ReturnSearchPageState extends State<ReturnSearchPage> {
  @override
  Widget build(BuildContext context) {
    final _media = MediaQuery.of(context);
    return Material(
      color: Colors.white,
      child: Stack(
        children: [
          ListView(
            clipBehavior: Clip.none,
            padding: EdgeInsets.only(
              bottom: _media.padding.bottom + 90,
              top: _media.padding.top + 56 + 20,
              left: 20,
              right: 20,
            ),
            children: [
              Row(
                children: [
                  Expanded(
                    child: Text(
                      'Найдено: 13 исполнителей',
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        height: 19 / 16,
                        color: ColorData.allMainActivegray,
                      ),
                    ),
                  ),
                  const SizedBox(width: 10),
                  Expanded(
                    child: LittleButton(
                      title: 'По расстоянию',
                      onTap: () {},
                    ),
                  ),
                ],
              ),
              SizedBox(height: 16),
              const ExecutorInfo(),
            ],
          ),
          Positioned(
            left: 0,
            right: 0,
            top: 0,
            child: CustomAppBar(
              title: Text(
                'Татуировки',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 18,
                  height: 21 / 18,
                  color: ColorData.allMainBlack,
                ),
              ),
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: TabSearch(),
          ),
        ],
      ),
    );
  }
}

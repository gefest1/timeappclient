import 'package:flutter/material.dart';
import 'package:li/components/molecules/credit_card_short.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/components/pages/add_new_card_payment.dart';
import 'package:li/components/pages/payment_way_page.dart';
import 'package:li/data/models/credit_card_model.dart';
import 'package:li/logic/providers/CreditCard.dart';
import 'package:li/main.dart';
import 'package:provider/provider.dart';

class CardData extends StatelessWidget {
  final ValueNotifier<CreditCardModel?> creditCard;

  const CardData({required this.creditCard, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<CreditCardProvider>(
      builder: (_, CreditCardProvider creditCardProvider, __) {
        if (creditCardProvider.creditCards.isEmpty)
          return SizedBox(
            width: double.infinity,
            child: CreditCardAdd(
              onTap: () {
                routerDelegate.push(
                  const AddNewCardPayment(),
                );
              },
            ),
          );
        if (creditCard.value == null)
          creditCard.value = creditCardProvider.creditCards.first;
        return ValueListenableBuilder(
          valueListenable: creditCard,
          builder: (ctx, _, __) {
            return SizeTapAnimation(
              child: GestureDetector(
                onTap: () {
                  routerDelegate.push(
                    PaymentWayPage(
                      creditCard: creditCard,
                    ),
                  );
                },
                child: CreditCard(
                  cartType: creditCard.value!.cardType!,
                  title: creditCard.value!.cardType! +
                      " *" +
                      creditCard.value!.last4Digits!,
                ),
              ),
            );
          },
        );
      },
    );
  }
}

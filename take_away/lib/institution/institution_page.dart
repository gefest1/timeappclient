import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/molecules/about_yourself_card.dart';
import 'package:li/components/molecules/accept_order_card.dart';
import 'package:li/components/molecules/address_card.dart';
import 'package:li/components/molecules/certificate_card.dart';
import 'package:li/components/molecules/contact_card.dart';
import 'package:li/components/molecules/institution_card.dart';
import 'package:li/components/molecules/options_card.dart';
import 'package:li/components/molecules/order_card.dart';
import 'package:li/components/molecules/raiting_feedback.dart';
import 'package:li/components/molecules/review_card.dart';
import 'package:li/components/molecules/tell_about_yourself.dart';
import 'package:li/components/molecules/video_yourself.dart';
import 'package:provider/provider.dart';
import 'package:take_away/logic/provider/institution/institution_provider.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/utils/color.dart';

class InstitutionPage extends StatelessWidget {
  final String institutionId;

  const InstitutionPage({
    required this.institutionId,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Consumer<InstitutionTakeAwayProvider>(
          builder: (ctx, institutionProvider, _) {
        final institution =
            institutionProvider.institutionTakeAwayMap[institutionId];
        if (institution == null) {
          institutionProvider.getinstitution(institutionId);
        }
        return Stack(
          children: [
            if (institution != null)
              CustomScrollView(
                slivers: [
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).padding.top + 56 - 16),
                      child: InstitutionCard(
                        institutionTakeAway: institution,
                      ),
                    ),
                  ),
                  SliverPadding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    sliver: SliverList(
                      delegate: SliverChildListDelegate([
                        ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all(
                              ColorData.mainGraybackground,
                            ),
                            elevation: MaterialStateProperty.all(0),
                            overlayColor: MaterialStateProperty.all(
                              Colors.black12,
                            ),
                            shape: MaterialStateProperty.all(
                              const StadiumBorder(),
                            ),
                          ),
                          onPressed: () {},
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              SvgPicture.asset(
                                  'assets/svg/icon/shieldWithCheck.svg'),
                              const SizedBox(width: 4),
                              const Text(
                                'Документы проверены',
                                style: TextStyle(
                                  color: ColorData.clientsButtonColorPressed,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                  height: 17 / 14,
                                ),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(height: 20),
                        const OrderCard(),
                        const SizedBox(height: 20),
                        const AdressCard(),
                        const SizedBox(height: 40),
                        const OptionsCard(),
                        const SizedBox(height: 40),
                        const AboutYourSelf(),
                        const SizedBox(height: 20),
                        const VideoYourSelf(),
                        const SizedBox(height: 20),
                        const CertificateCard(),
                        const SizedBox(height: 20),
                        const ContactCard(),
                        const SizedBox(height: 40),
                        const TellAboutYourSelf(),
                        const SizedBox(height: 40),
                        const RaingFeedback(),
                        const SizedBox(height: 20),
                        const ReviewCard(),
                        const SizedBox(height: 80),
                      ]),
                    ),
                  ),
                ],
              ),
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              child: AcceptOrderCard(
                institutionId: institutionId,
              ),
            ),
            Positioned(
              left: 0,
              right: 0,
              top: 0,
              child: CustomAppBar(
                leading: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: const Icon(
                    Icons.arrow_back,
                    color: Color(0xff14142B),
                  ),
                ),
                title: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Text(
                      'Наталья О.',
                      style: TextStyle(
                        color: ColorData.allMainBlack,
                        fontWeight: FontWeight.w500,
                        height: 21 / 18,
                        fontSize: 18,
                      ),
                    ),
                    Container(
                      height: 11,
                      width: 11,
                      alignment: Alignment.center,
                      decoration: const BoxDecoration(
                        color: Color(0xffBEFFC5),
                        shape: BoxShape.circle,
                      ),
                      child: Container(
                        height: 5,
                        width: 5,
                        alignment: Alignment.center,
                        decoration: const BoxDecoration(
                          color: Color(0xff0FF426),
                          shape: BoxShape.circle,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        );
      }),
    );
  }
}

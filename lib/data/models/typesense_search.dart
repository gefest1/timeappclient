import 'dart:convert';
import 'package:equatable/equatable.dart';
import 'package:li/data/models/service_model.dart';

class TypesenseSearch<T extends Equatable> extends Equatable {
  final T? document;
  final dynamic highlights;
  final int? textMatch;

  const TypesenseSearch({
    this.document,
    this.highlights,
    this.textMatch,
  });

  TypesenseSearch<T> copyWith({
    T? document,
    dynamic highlights,
    int? textMatch,
  }) {
    return TypesenseSearch<T>(
      document: document ?? this.document,
      highlights: highlights ?? this.highlights,
      textMatch: textMatch ?? this.textMatch,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'document': (document as dynamic).toMap(),
      'highlights': highlights,
      'text_match': textMatch,
    };
  }

  static TypesenseSearch<T>? fromMap<T extends Equatable>(
    Map<String, dynamic>? map,
  ) {
    if (map == null) return null;
    final dynamic _document;
    if (T is ServiceMinute) {
      _document = ServiceMinute.fromMap(map['document']);
    } else {
      _document = null;
    }

    return TypesenseSearch<T>(
      document: _document,
      highlights: map['highlights'],
      textMatch: map['text_match'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TypesenseSearch.fromJson(String source) =>
      TypesenseSearch.fromMap<T>(json.decode(source))!;

  @override
  bool get stringify => true;

  @override
  List<Object?> get props => [document, highlights];
}

class SerachTypesense<T extends Equatable> extends Equatable {
  final List<dynamic>? facetCounts;
  final int? found;
  final List<TypesenseSearch<T>>? hits;
  final int? outOf;
  final int? page;

  const SerachTypesense({
    this.facetCounts,
    this.found,
    this.hits,
    this.outOf,
    this.page,
  });

  SerachTypesense<T> copyWith({
    List<dynamic>? facetCounts,
    int? found,
    List<TypesenseSearch<T>>? hits,
    int? outOf,
    int? page,
  }) {
    return SerachTypesense<T>(
      facetCounts: facetCounts ?? this.facetCounts,
      found: found ?? this.found,
      hits: hits ?? this.hits,
      outOf: outOf ?? this.outOf,
      page: page ?? this.page,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'facet_counts': facetCounts,
      'found': found,
      'hits': hits?.map((x) => x.toMap()).toList(),
      'out_of': outOf,
      'page': page,
    };
  }

  static SerachTypesense<T>? fromMap<T extends Equatable>(
    Map<String, dynamic>? map,
  ) {
    if (map == null) return null;
    return SerachTypesense<T>(
      facetCounts: List<dynamic>.from(map['facet_counts']),
      found: map['found'],
      hits: List<TypesenseSearch<T>>.from(
        map['hits']?.map(
          (x) => TypesenseSearch.fromMap<T>(x),
        ),
      ),
      outOf: map['out_of'],
      page: map['page'],
    );
  }

  String toJson() => json.encode(toMap());

  factory SerachTypesense.fromJson(String source) =>
      SerachTypesense.fromMap<T>(json.decode(source))!;

  @override
  bool get stringify => true;

  @override
  List<Object?> get props => [facetCounts, found, hits, outOf, page];
}

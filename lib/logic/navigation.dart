import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:li/components/molecules/overlay_loader.dart';
import 'package:li/components/pages/no_internet_page.dart';
import 'package:li/components/pages/auth/your_phone_number_page.dart';
import 'package:li/main.dart';
import 'package:li/utils/color.dart';
import 'package:main_page/main_page.dart';

bool blockBackRoutes = false;
bool internetConnection = true;

class CustomRoutePath {
  CustomRoutePath();
}

class CustomRouteInformationParser
    extends RouteInformationParser<CustomRoutePath> {
  @override
  Future<CustomRoutePath> parseRouteInformation(
    RouteInformation routeInformation,
  ) async {
    return CustomRoutePath();
  }

  @override
  RouteInformation restoreRouteInformation(CustomRoutePath path) {
    return RouteInformation();
  }
}

class CustomRouterDelegate extends RouterDelegate<CustomRoutePath>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<CustomRoutePath> {
  final GlobalKey<NavigatorState> navigatorKey;
  // bool _blocWidgets = false;

  List<Page> pages = [
    const MaterialPage(
      child: MainPage(),
      // : const YourPhoneNumber()
      maintainState: true,
    ),
  ];

  CustomRouterDelegate({
    GlobalKey<NavigatorState>? navigatorKey,
  }) : this.navigatorKey = navigatorKey ?? GlobalKey<NavigatorState>();

  @override
  Future<void> setNewRoutePath(CustomRoutePath configuration) async {}

  void push(Widget child) {
    if (!internetConnection) {
      final child = NoInternetPage();
      pages = [
        ...pages,
        (Platform.isAndroid)
            ? MaterialPage(child: child)
            : CupertinoPage(child: child),
      ];
      notifyListeners();

      return;
    }
    pages = [
      ...pages,
      (Platform.isAndroid)
          ? MaterialPage(child: child)
          : CupertinoPage(child: child)
    ];

    notifyListeners();
  }

  void popToFirst() {
    pages = [
      pages[0],
    ];

    notifyListeners();
  }

  void pushAndRemoveUntil(Widget child) {
    if (!internetConnection) {
      final child = NoInternetPage();
      navigatorKey.currentState?.popUntil((route) => route.isFirst);
      pages = [
        ...pages,
        (Platform.isAndroid)
            ? MaterialPage(child: child)
            : CupertinoPage(child: child),
      ];
      notifyListeners();

      return;
    }
    final _initPage = pages[0];
    navigatorKey.currentState?.popUntil((route) => route.isFirst);
    pages = [
      _initPage,
      (Platform.isAndroid)
          ? MaterialPage(child: child)
          : CupertinoPage(child: child)
    ];

    notifyListeners();
  }

  void blockWidgets() {
    blockBackRoutes = true;

    notifyListeners();
  }

  void unBlockWidgets() {
    blockBackRoutes = false;

    notifyListeners();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Navigator(
          observers: [],
          key: navigatorKey,
          onPopPage: (route, result) {
            if (blockBackRoutes) return false;
            if (!route.didPop(result)) {
              return false;
            }
            pages.removeLast();

            notifyListeners();
            return true;
          },
          pages: pages,
        ),
        if (blockBackRoutes)
          OverlayLoaderWidget(
            child: AbsorbPointer(
              absorbing: true,
              child: DecoratedBox(
                decoration: BoxDecoration(color: Colors.black87),
                child: Center(
                  child: CircularProgressIndicator(
                    color: ColorData.clientsButtonColorDefault,
                  ),
                ),
              ),
            ),
          ),
      ],
    );
  }
}

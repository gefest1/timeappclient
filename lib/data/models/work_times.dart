import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:li/utils/date_time_parse.dart';

class WorkTimes extends Equatable {
  final DateTime? startTime;
  final DateTime? endTime;

  WorkTimes({
    this.startTime,
    this.endTime,
  });

  WorkTimes copyWith({
    DateTime? startTime,
    DateTime? endTime,
  }) {
    return WorkTimes(
      startTime: startTime ?? this.startTime,
      endTime: endTime ?? this.endTime,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'startTime': startTime,
      'endTime': endTime,
    };
  }

  factory WorkTimes.fromMap(Map<String, dynamic> map) {
    return WorkTimes(
      startTime:
          map['startTime'] != null ? dateTimeParse(map['startTime']) : null,
      endTime: map['endTime'] != null ? dateTimeParse(map['endTime']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory WorkTimes.fromJson(String source) =>
      WorkTimes.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object?> get props => [
        startTime,
        endTime,
      ];
}

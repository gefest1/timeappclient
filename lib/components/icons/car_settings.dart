// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';

class CarSettingsCustomPainter extends CustomPainter {
  const CarSettingsCustomPainter({Listenable? repaint})
      : super(repaint: repaint);
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.9715933, size.height * 0.4472967);
    path_0.cubicTo(
        size.width * 0.9700933,
        size.height * 0.4339533,
        size.width * 0.9545367,
        size.height * 0.4239167,
        size.width * 0.9410800,
        size.height * 0.4239167);
    path_0.cubicTo(
        size.width * 0.8975933,
        size.height * 0.4239167,
        size.width * 0.8589967,
        size.height * 0.3983933,
        size.width * 0.8428067,
        size.height * 0.3588867);
    path_0.cubicTo(
        size.width * 0.8262867,
        size.height * 0.3184320,
        size.width * 0.8369467,
        size.height * 0.2712617,
        size.width * 0.8693467,
        size.height * 0.2415373);
    path_0.cubicTo(
        size.width * 0.8795567,
        size.height * 0.2322070,
        size.width * 0.8807933,
        size.height * 0.2165833,
        size.width * 0.8722433,
        size.height * 0.2057380);
    path_0.cubicTo(
        size.width * 0.8499800,
        size.height * 0.1774710,
        size.width * 0.8246833,
        size.height * 0.1519340,
        size.width * 0.7970567,
        size.height * 0.1298040);
    path_0.cubicTo(
        size.width * 0.7862467,
        size.height * 0.1211343,
        size.width * 0.7703633,
        size.height * 0.1223413,
        size.width * 0.7609867,
        size.height * 0.1327227);
    path_0.cubicTo(
        size.width * 0.7326967,
        size.height * 0.1640520,
        size.width * 0.6818800,
        size.height * 0.1756937,
        size.width * 0.6426167,
        size.height * 0.1593107);
    path_0.cubicTo(
        size.width * 0.6017633,
        size.height * 0.1421233,
        size.width * 0.5759833,
        size.height * 0.1007163,
        size.width * 0.5785167,
        size.height * 0.05627167);
    path_0.cubicTo(
        size.width * 0.5793467,
        size.height * 0.04230233,
        size.width * 0.5691367,
        size.height * 0.03016407,
        size.width * 0.5552167,
        size.height * 0.02854260);
    path_0.cubicTo(
        size.width * 0.5197633,
        size.height * 0.02443763,
        size.width * 0.4839967,
        size.height * 0.02431857,
        size.width * 0.4484400,
        size.height * 0.02826347);
    path_0.cubicTo(
        size.width * 0.4346933,
        size.height * 0.02978230,
        size.width * 0.4244867,
        size.height * 0.04163333,
        size.width * 0.4249467,
        size.height * 0.05542600);
    path_0.cubicTo(
        size.width * 0.4264800,
        size.height * 0.09943567,
        size.width * 0.4004067,
        size.height * 0.1401117,
        size.width * 0.3599500,
        size.height * 0.1566837);
    path_0.cubicTo(
        size.width * 0.3211553,
        size.height * 0.1725330,
        size.width * 0.2706930,
        size.height * 0.1609773,
        size.width * 0.2424670,
        size.height * 0.1299233);
    path_0.cubicTo(
        size.width * 0.2331367,
        size.height * 0.1196937,
        size.width * 0.2175337,
        size.height * 0.1184373,
        size.width * 0.2066513,
        size.height * 0.1269223);
    path_0.cubicTo(
        size.width * 0.1782120,
        size.height * 0.1492413,
        size.width * 0.1523463,
        size.height * 0.1747947,
        size.width * 0.1298717,
        size.height * 0.2028317);
    path_0.cubicTo(
        size.width * 0.1211157,
        size.height * 0.2137303,
        size.width * 0.1224090,
        size.height * 0.2295263,
        size.width * 0.1327043,
        size.height * 0.2388897);
    path_0.cubicTo(
        size.width * 0.1657450,
        size.height * 0.2688110,
        size.width * 0.1764017,
        size.height * 0.3163877,
        size.width * 0.1592470,
        size.height * 0.3573133);
    path_0.cubicTo(
        size.width * 0.1428640,
        size.height * 0.3963333,
        size.width * 0.1023520,
        size.height * 0.4214633,
        size.width * 0.05598233,
        size.height * 0.4214633);
    path_0.cubicTo(
        size.width * 0.04092933,
        size.height * 0.4209767,
        size.width * 0.03022343,
        size.height * 0.4310900,
        size.width * 0.02856093,
        size.height * 0.4447733);
    path_0.cubicTo(
        size.width * 0.02438617,
        size.height * 0.4804200,
        size.width * 0.02434103,
        size.height * 0.5167567,
        size.width * 0.02835570,
        size.height * 0.5526933);
    path_0.cubicTo(
        size.width * 0.02985400,
        size.height * 0.5660933,
        size.width * 0.04587567,
        size.height * 0.5760433,
        size.width * 0.05948367,
        size.height * 0.5760433);
    path_0.cubicTo(
        size.width * 0.1008167,
        size.height * 0.5749933,
        size.width * 0.1404790,
        size.height * 0.6005667,
        size.width * 0.1571410,
        size.height * 0.6410700);
    path_0.cubicTo(
        size.width * 0.1737253,
        size.height * 0.6815267,
        size.width * 0.1630523,
        size.height * 0.7286767,
        size.width * 0.1305983,
        size.height * 0.7584400);
    path_0.cubicTo(
        size.width * 0.1204427,
        size.height * 0.7677700,
        size.width * 0.1191497,
        size.height * 0.7833567,
        size.width * 0.1277207,
        size.height * 0.7941900);
    path_0.cubicTo(
        size.width * 0.1497603,
        size.height * 0.8222800,
        size.width * 0.1750757,
        size.height * 0.8478333,
        size.width * 0.2027843,
        size.height * 0.8701533);
    path_0.cubicTo(
        size.width * 0.2136667,
        size.height * 0.8789133,
        size.width * 0.2294830,
        size.height * 0.8776867,
        size.width * 0.2389163,
        size.height * 0.8672933);
    path_0.cubicTo(
        size.width * 0.2673147,
        size.height * 0.8358900,
        size.width * 0.3181177,
        size.height * 0.8242667,
        size.width * 0.3572300,
        size.height * 0.8406833);
    path_0.cubicTo(
        size.width * 0.3981967,
        size.height * 0.8578267,
        size.width * 0.4239700,
        size.height * 0.8992300,
        size.width * 0.4214533,
        size.height * 0.9436867);
    path_0.cubicTo(
        size.width * 0.4206233,
        size.height * 0.9576533,
        size.width * 0.4308667,
        size.height * 0.9698300,
        size.width * 0.4447300,
        size.height * 0.9714133);
    path_0.cubicTo(
        size.width * 0.4628767,
        size.height * 0.9735367,
        size.width * 0.4811133,
        size.height * 0.9745867,
        size.width * 0.4994133,
        size.height * 0.9745867);
    path_0.cubicTo(
        size.width * 0.5167767,
        size.height * 0.9745867,
        size.width * 0.5341433,
        size.height * 0.9736400,
        size.width * 0.5515067,
        size.height * 0.9717067);
    path_0.cubicTo(
        size.width * 0.5652700,
        size.height * 0.9701867,
        size.width * 0.5754600,
        size.height * 0.9583233,
        size.width * 0.5749933,
        size.height * 0.9445267);
    path_0.cubicTo(
        size.width * 0.5734067,
        size.height * 0.9005367,
        size.width * 0.5995400,
        size.height * 0.8598567,
        size.width * 0.6399400,
        size.height * 0.8433200);
    path_0.cubicTo(
        size.width * 0.6790000,
        size.height * 0.8273667,
        size.width * 0.7292300,
        size.height * 0.8390400,
        size.width * 0.7574800,
        size.height * 0.8700500);
    path_0.cubicTo(
        size.width * 0.7668567,
        size.height * 0.8802600,
        size.width * 0.7823633,
        size.height * 0.8815000,
        size.width * 0.7932967,
        size.height * 0.8730467);
    path_0.cubicTo(
        size.width * 0.8216833,
        size.height * 0.8507867,
        size.width * 0.8475000,
        size.height * 0.8252433,
        size.width * 0.8700700,
        size.height * 0.7971333);
    path_0.cubicTo(
        size.width * 0.8788333,
        size.height * 0.7862567,
        size.width * 0.8775900,
        size.height * 0.7704400,
        size.width * 0.8672433,
        size.height * 0.7610600);
    path_0.cubicTo(
        size.width * 0.8342067,
        size.height * 0.7311600,
        size.width * 0.8234967,
        size.height * 0.6835600,
        size.width * 0.8406533,
        size.height * 0.6426767);
    path_0.cubicTo(
        size.width * 0.8567767,
        size.height * 0.6041833,
        size.width * 0.8957867,
        size.height * 0.5783400,
        size.width * 0.9377367,
        size.height * 0.5783400);
    path_0.lineTo(size.width * 0.9436067, size.height * 0.5784933);
    path_0.cubicTo(
        size.width * 0.9572267,
        size.height * 0.5796000,
        size.width * 0.9697467,
        size.height * 0.5691100,
        size.width * 0.9713867,
        size.height * 0.5552167);
    path_0.cubicTo(
        size.width * 0.9755800,
        size.height * 0.5195400,
        size.width * 0.9756233,
        size.height * 0.4832433,
        size.width * 0.9715933,
        size.height * 0.4472967);
    path_0.close();
    path_0.moveTo(size.width * 0.4999800, size.height * 0.7736000);
    path_0.cubicTo(
        size.width * 0.3488633,
        size.height * 0.7736000,
        size.width * 0.2263673,
        size.height * 0.6511033,
        size.width * 0.2263673,
        size.height * 0.4999900);
    path_0.cubicTo(
        size.width * 0.2263673,
        size.height * 0.3488800,
        size.width * 0.3488633,
        size.height * 0.2263820,
        size.width * 0.4999800,
        size.height * 0.2263820);
    path_0.cubicTo(
        size.width * 0.6510900,
        size.height * 0.2263820,
        size.width * 0.7735900,
        size.height * 0.3488800,
        size.width * 0.7735900,
        size.height * 0.4999900);
    path_0.cubicTo(
        size.width * 0.7735900,
        size.height * 0.6511033,
        size.width * 0.6510933,
        size.height * 0.7736000,
        size.width * 0.4999800,
        size.height * 0.7736000);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Colors.white;
    canvas.drawPath(path_0, paint_0_fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.6511267, size.height * 0.3618133);
    path_1.cubicTo(
        size.width * 0.6445800,
        size.height * 0.3458600,
        size.width * 0.6252267,
        size.height * 0.3328857,
        size.width * 0.6079833,
        size.height * 0.3328857);
    path_1.lineTo(size.width * 0.3916933, size.height * 0.3328857);
    path_1.cubicTo(
        size.width * 0.3744533,
        size.height * 0.3328857,
        size.width * 0.3550967,
        size.height * 0.3458600,
        size.width * 0.3485500,
        size.height * 0.3618100);
    path_1.lineTo(size.width * 0.3254680, size.height * 0.4180300);
    path_1.lineTo(size.width * 0.3034860, size.height * 0.4119767);
    path_1.cubicTo(
        size.width * 0.3015197,
        size.height * 0.4114333,
        size.width * 0.2995533,
        size.height * 0.4111600,
        size.width * 0.2976323,
        size.height * 0.4111600);
    path_1.cubicTo(
        size.width * 0.2871153,
        size.height * 0.4111600,
        size.width * 0.2794800,
        size.height * 0.4191933,
        size.width * 0.2794800,
        size.height * 0.4302633);
    path_1.lineTo(size.width * 0.2794800, size.height * 0.4433867);
    path_1.cubicTo(
        size.width * 0.2794800,
        size.height * 0.4563100,
        size.width * 0.2899930,
        size.height * 0.4668233,
        size.width * 0.3029153,
        size.height * 0.4668233);
    path_1.lineTo(size.width * 0.3054317, size.height * 0.4668233);
    path_1.lineTo(size.width * 0.3017043, size.height * 0.4759067);
    path_1.cubicTo(
        size.width * 0.2956250,
        size.height * 0.4907233,
        size.width * 0.2906700,
        size.height * 0.5158067,
        size.width * 0.2906700,
        size.height * 0.5318300);
    path_1.lineTo(size.width * 0.2906700, size.height * 0.6436733);
    path_1.cubicTo(
        size.width * 0.2906700,
        size.height * 0.6565967,
        size.width * 0.3011830,
        size.height * 0.6671133,
        size.width * 0.3141053,
        size.height * 0.6671133);
    path_1.lineTo(size.width * 0.3447233, size.height * 0.6671133);
    path_1.cubicTo(
        size.width * 0.3576467,
        size.height * 0.6671133,
        size.width * 0.3681600,
        size.height * 0.6565967,
        size.width * 0.3681600,
        size.height * 0.6436733);
    path_1.lineTo(size.width * 0.3681600, size.height * 0.6157433);
    path_1.lineTo(size.width * 0.6315100, size.height * 0.6157433);
    path_1.lineTo(size.width * 0.6315100, size.height * 0.6436733);
    path_1.cubicTo(
        size.width * 0.6315100,
        size.height * 0.6565967,
        size.width * 0.6420233,
        size.height * 0.6671133,
        size.width * 0.6549467,
        size.height * 0.6671133);
    path_1.lineTo(size.width * 0.6855567, size.height * 0.6671133);
    path_1.cubicTo(
        size.width * 0.6984800,
        size.height * 0.6671133,
        size.width * 0.7089933,
        size.height * 0.6565967,
        size.width * 0.7089933,
        size.height * 0.6436733);
    path_1.lineTo(size.width * 0.7089933, size.height * 0.5318300);
    path_1.cubicTo(
        size.width * 0.7089933,
        size.height * 0.5158167,
        size.width * 0.7040467,
        size.height * 0.4907267,
        size.width * 0.6979633,
        size.height * 0.4759067);
    path_1.lineTo(size.width * 0.6942333, size.height * 0.4668233);
    path_1.lineTo(size.width * 0.6970633, size.height * 0.4668233);
    path_1.cubicTo(
        size.width * 0.7099867,
        size.height * 0.4668233,
        size.width * 0.7204967,
        size.height * 0.4563067,
        size.width * 0.7204967,
        size.height * 0.4433867);
    path_1.lineTo(size.width * 0.7204967, size.height * 0.4302633);
    path_1.cubicTo(
        size.width * 0.7204967,
        size.height * 0.4191933,
        size.width * 0.7128667,
        size.height * 0.4111600,
        size.width * 0.7023467,
        size.height * 0.4111600);
    path_1.cubicTo(
        size.width * 0.7004233,
        size.height * 0.4111600,
        size.width * 0.6984600,
        size.height * 0.4114333,
        size.width * 0.6964867,
        size.height * 0.4119767);
    path_1.lineTo(size.width * 0.6742300, size.height * 0.4181100);
    path_1.lineTo(size.width * 0.6511267, size.height * 0.3618133);
    path_1.close();
    path_1.moveTo(size.width * 0.3422733, size.height * 0.4540967);
    path_1.lineTo(size.width * 0.3772367, size.height * 0.3689400);
    path_1.cubicTo(
        size.width * 0.3811533,
        size.height * 0.3594000,
        size.width * 0.3927933,
        size.height * 0.3515933,
        size.width * 0.4031067,
        size.height * 0.3515933);
    path_1.lineTo(size.width * 0.5965767, size.height * 0.3515933);
    path_1.cubicTo(
        size.width * 0.6068900,
        size.height * 0.3515933,
        size.width * 0.6185300,
        size.height * 0.3593967,
        size.width * 0.6224467,
        size.height * 0.3689400);
    path_1.lineTo(size.width * 0.6574100, size.height * 0.4540967);
    path_1.cubicTo(
        size.width * 0.6613233,
        size.height * 0.4636367,
        size.width * 0.6560900,
        size.height * 0.4714467,
        size.width * 0.6457800,
        size.height * 0.4714467);
    path_1.lineTo(size.width * 0.3539033, size.height * 0.4714467);
    path_1.cubicTo(
        size.width * 0.3435967,
        size.height * 0.4714467,
        size.width * 0.3383567,
        size.height * 0.4636400,
        size.width * 0.3422733,
        size.height * 0.4540967);
    path_1.close();
    path_1.moveTo(size.width * 0.4203133, size.height * 0.5618133);
    path_1.cubicTo(
        size.width * 0.4203133,
        size.height * 0.5669667,
        size.width * 0.4160967,
        size.height * 0.5711867,
        size.width * 0.4109367,
        size.height * 0.5711867);
    path_1.lineTo(size.width * 0.3445267, size.height * 0.5711867);
    path_1.cubicTo(
        size.width * 0.3393700,
        size.height * 0.5711867,
        size.width * 0.3351567,
        size.height * 0.5669667,
        size.width * 0.3351567,
        size.height * 0.5618133);
    path_1.lineTo(size.width * 0.3351567, size.height * 0.5299400);
    path_1.cubicTo(
        size.width * 0.3351567,
        size.height * 0.5247867,
        size.width * 0.3393700,
        size.height * 0.5205667,
        size.width * 0.3445267,
        size.height * 0.5205667);
    path_1.lineTo(size.width * 0.4109367, size.height * 0.5205667);
    path_1.cubicTo(
        size.width * 0.4160967,
        size.height * 0.5205667,
        size.width * 0.4203133,
        size.height * 0.5247867,
        size.width * 0.4203133,
        size.height * 0.5299400);
    path_1.lineTo(size.width * 0.4203133, size.height * 0.5618133);
    path_1.close();
    path_1.moveTo(size.width * 0.6638900, size.height * 0.5618133);
    path_1.cubicTo(
        size.width * 0.6638900,
        size.height * 0.5669667,
        size.width * 0.6596733,
        size.height * 0.5711867,
        size.width * 0.6545200,
        size.height * 0.5711867);
    path_1.lineTo(size.width * 0.5881100, size.height * 0.5711867);
    path_1.cubicTo(
        size.width * 0.5829500,
        size.height * 0.5711867,
        size.width * 0.5787333,
        size.height * 0.5669667,
        size.width * 0.5787333,
        size.height * 0.5618133);
    path_1.lineTo(size.width * 0.5787333, size.height * 0.5299400);
    path_1.cubicTo(
        size.width * 0.5787333,
        size.height * 0.5247867,
        size.width * 0.5829500,
        size.height * 0.5205667,
        size.width * 0.5881100,
        size.height * 0.5205667);
    path_1.lineTo(size.width * 0.6545200, size.height * 0.5205667);
    path_1.cubicTo(
        size.width * 0.6596733,
        size.height * 0.5205667,
        size.width * 0.6638900,
        size.height * 0.5247867,
        size.width * 0.6638900,
        size.height * 0.5299400);
    path_1.lineTo(size.width * 0.6638900, size.height * 0.5618133);
    path_1.close();

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = Colors.white;
    canvas.drawPath(path_1, paint_1_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}

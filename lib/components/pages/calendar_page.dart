import 'package:flutter/material.dart';
import 'package:li/components/atoms/little_button.dart';
import 'package:li/components/molecules/story_card.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/text_styles.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/utils/testStyles.dart';

class CalendarPage extends StatelessWidget {
  const CalendarPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          left: 0,
          right: 0,
          top: 56 + MediaQuery.of(context).padding.top,
          bottom: 0,
          child: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Column(
                  children: [
                    const SizedBox(height: 16),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            width: 100,
                            child: LittleButton(
                              title: 'Период',
                              onTap: () {},
                              right: true,
                            ),
                          ),
                          SizedBox(
                            width: 180,
                            child: LittleButton(
                              title: 'Вид деятельности',
                              onTap: () {},
                              right: true,
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 16),
                  ],
                ),
              ),
              SliverToBoxAdapter(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Text(
                        'Сегодня',
                        style: TextStyle(
                          fontSize: H2TextStyle.fontSize,
                          fontWeight: H2TextStyle.fontWeight,
                          height: H2TextStyle.height,
                          color: ColorData.allMainBlack,
                        ),
                      ),
                    ),
                    const SizedBox(height: 10),
                  ],
                ),
              ),
              SliverList(
                delegate: SliverChildBuilderDelegate(
                  (context, index) => index % 2 == 1
                      ? const SizedBox(height: 10)
                      : Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: const StoryCard(),
                        ),
                  childCount: 6,
                ),
              ),
            ],
          ),
        ),
        Positioned(
          left: 0,
          right: 0,
          top: 0,
          child: CustomAppBar(),
        ),
      ],
    );
  }
}

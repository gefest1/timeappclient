import 'package:flutter/material.dart';

late double devicePixelRatioFlow;

class FlowMenuDelegate extends FlowDelegate {
  FlowMenuDelegate({required this.offsetListen}) : super(repaint: offsetListen);

  final ValueNotifier<List<Offset>> offsetListen;

  @override
  bool shouldRepaint(FlowMenuDelegate oldDelegate) {
    return offsetListen != oldDelegate.offsetListen;
  }

  @override
  void paintChildren(FlowPaintingContext context) {
    final _offsets = offsetListen.value;
    for (int i = 0; i < context.childCount; ++i) {
      final childSize = context.getChildSize(i)!;

      if (_offsets.length <= i) break;
      context.paintChild(
        i,
        transform: Matrix4.translationValues(
          (_offsets[i].dx / devicePixelRatioFlow) - childSize.width / 2,
          (_offsets[i].dy / devicePixelRatioFlow) - childSize.height,
          0,
        ),
      );
    }
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class OverlayFadeTransition extends OverlayEntry {
  final Widget child;
  final double? left;
  final double? top;
  final double? right;
  final double? bottom;
  final double? width;
  final double? height;

  OverlayFadeTransition({
    required this.child,
    this.left,
    this.top,
    this.right,
    this.bottom,
    this.width,
    this.height,
  }) : super(
          builder: (_) {
            return SizedBox();
          },
        );

  Widget _buider(BuildContext context) {
    return OverlayFadeTransitionWidget(
      overlayEntry: this,
      child: this.child,
      left: left,
      top: top,
      right: right,
      bottom: bottom,
      width: width,
      height: height,
    );
  }

  WidgetBuilder get builder => _buider;
}

class OverlayFadeTransitionWidget extends StatefulWidget {
  final OverlayFadeTransition overlayEntry;
  final double? left;
  final double? top;
  final double? right;
  final double? bottom;
  final double? width;
  final double? height;

  final Widget child;

  const OverlayFadeTransitionWidget({
    required this.overlayEntry,
    required this.child,
    this.left,
    this.top,
    this.right,
    this.bottom,
    this.width,
    this.height,
    Key? key,
  }) : super(key: key);

  @override
  _OverlayFadeTransitionWidgetState createState() =>
      _OverlayFadeTransitionWidgetState();
}

class _OverlayFadeTransitionWidgetState
    extends State<OverlayFadeTransitionWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController animationController = AnimationController(
    vsync: this,
    duration: Duration(milliseconds: 400),
    value: 0,
  );

  void _init() async {
    await animationController.forward();
    await Future.delayed(
      Duration(seconds: 2, milliseconds: 600),
    );
    await animationController.reverse();
    widget.overlayEntry.remove();
  }

  @override
  void initState() {
    super.initState();
    _init();
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: widget.left,
      top: widget.top,
      right: widget.right,
      bottom: widget.bottom,
      width: widget.width,
      height: widget.height,
      child: FadeTransition(
        opacity: animationController,
        child: widget.child,
      ),
    );
  }
}

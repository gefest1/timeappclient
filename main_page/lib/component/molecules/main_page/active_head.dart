import 'package:flutter/material.dart';
import 'package:main_page/component/molecules/atoms/time_food.dart';
import 'package:main_page/component/molecules/atoms/time_home.dart';
import 'package:main_page/component/molecules/atoms/time_pay.dart';
import 'package:main_page/component/molecules/atoms/time_service.dart';
import 'package:main_page/component/molecules/main_page/active_chip.dart';

class ActiveHead extends StatelessWidget {
  final int index;
  final void Function(int) onTap;

  const ActiveHead({
    required this.index,
    required this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 148,
      child: ListView(
        scrollDirection: Axis.horizontal,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        children: [
          ActiveChip(
            onTap: () => onTap.call(0),
            title: 'Поминутная оплата',
            active: 0 == index,
            child: Positioned(
              top: 0,
              left: -25,
              child: CustomPaint(
                painter: TimePayCustomPainter(
                  active: 0 == index,
                ),
                size: const Size(139, 128),
              ),
            ),
          ),
          if (DateTime.now().isAfter(DateTime(2022, 3, 14))) ...[
            const SizedBox(width: 10),
            ActiveChip(
              onTap: () => onTap.call(1),
              title: 'Аренда жилья',
              subTitle: 'Апрель 2022',
              active: 1 == index,
              child: Positioned(
                top: 0,
                left: -41,
                child: CustomPaint(
                  painter: TimeHomeCustomPainter(active: 1 == index),
                  size: const Size(197, 100),
                ),
              ),
            ),
            const SizedBox(width: 10),
            ActiveChip(
              onTap: () => onTap.call(2),
              title: 'Домашняя еда',
              subTitle: 'Июль 2022',
              active: 2 == index,
              child: Positioned(
                top: 0,
                left: -8,
                child: CustomPaint(
                  painter: TimeFoodCustomPainter(active: 2 == index),
                  size: const Size(130, 101),
                ),
              ),
            ),
            const SizedBox(width: 10),
            ActiveChip(
              onTap: () => onTap.call(3),
              title: 'Услуги',
              subTitle: 'Сентябрь 2022\n',
              active: 3 == index,
              child: Positioned(
                top: 0,
                left: -3,
                child: CustomPaint(
                  painter: TimeServiceCustomPainter(active: 3 == index),
                  size: const Size(112, 92),
                ),
              ),
            ),
          ]
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:li/utils/restart_widget.dart';
import 'package:profile/bloc/profile_bloc.dart';
import 'package:profile/icon/sadFace.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class DeleteAccountAlertDialog extends StatelessWidget {
  const DeleteAccountAlertDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(color: Colors.black45),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12),
        child: Center(
          child: Material(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            child: Padding(
              padding: const EdgeInsets.all(15),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  CustomPaint(
                    size: const Size(40, 40),
                    painter: SadFace(),
                  ),
                  const SizedBox(height: 10),
                  const Text(
                    'Вы точно-точно хотите удалить свою страницу?',
                    style: TextStyle(
                      color: ColorData.allMainBlack,
                      fontWeight: H3TextStyle.fontWeight,
                      fontSize: H3TextStyle.fontSize,
                      height: H3TextStyle.height,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 10),
                  const Text(
                    'Нам очень жаль, что вы уходите. Мы всегда будем вас ждать. Вы можете зарегестрироваться по тому же номеру, что и сейчас в любой момент времени.',
                    style: TextStyle(
                      color: ColorData.allMainActivegray,
                      fontWeight: P3TextStyle.fontWeight,
                      fontSize: P3TextStyle.fontSize,
                      height: P3TextStyle.height,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(height: 40),
                  Row(
                    children: [
                      Expanded(
                        child: InkWell(
                          borderRadius: BorderRadius.circular(15),
                          onTap: () async {
                            BlocProvider.of<ProfileBloc>(context).add(
                              DeleteClientEvent(),
                            );
                            RestartWidget.restartApp(context);
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              border: Border.all(
                                width: 1,
                                color: ColorData.allMainActivegray,
                              ),
                              color: Colors.transparent,
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: Center(
                                  child: Text(
                                'Да, удалить',
                                style: TextStyle(
                                  fontSize: 16,
                                  color: ColorData.allMainActivegray,
                                ),
                              )),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      Expanded(
                        child: Material(
                          borderRadius: BorderRadius.circular(15),
                          color: ColorData.clientsButtonColorDefault,
                          child: InkWell(
                            borderRadius: BorderRadius.circular(15),
                            onTap: () => Navigator.pop(context),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: Colors.transparent,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Center(
                                  child: Text(
                                    'Нет',
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

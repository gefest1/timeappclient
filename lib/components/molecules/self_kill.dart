import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';

class SelfKill extends StatefulWidget {
  final Widget child;

  const SelfKill({
    required this.child,
    Key? key,
  }) : super(key: key);

  @override
  State<SelfKill> createState() => _SelfKillState();
}

class _SelfKillState extends State<SelfKill>
    with SingleTickerProviderStateMixin {
  late final AnimationController _anim = AnimationController(
    vsync: this,
    value: 1,
    duration: Duration(milliseconds: 400),
  );
  late final Animation<Offset> _offset = Tween<Offset>(
    begin: Offset(-1, 0),
    end: Offset.zero,
  ).animate(_anim);

  @override
  void initState() {
    Timer(Duration(seconds: 5), () {
      _anim.reverse();
    });
    super.initState();
  }

  @override
  void dispose() {
    _anim.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SlideTransition(
      position: _offset,
      child: SizeTransition(
        sizeFactor: _anim,
        child: widget.child,
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:li/components/molecules/draw_circle_painter.dart';
import 'package:li/components/molecules/order_card.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/utils/testStyles.dart';

class WaitingOrderPage extends StatefulWidget {
  const WaitingOrderPage({Key? key}) : super(key: key);

  @override
  _WaitingOrderPageState createState() => _WaitingOrderPageState();
}

class _WaitingOrderPageState extends State<WaitingOrderPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: Text(
          'Поданная заявка',
          style: TextStyle(
            color: ColorData.allMainBlack,
          ),
        ),
      ),
      body: ListView(
        padding: EdgeInsets.only(
          top: 20,
          bottom: 100,
          left: 20,
          right: 20,
        ),
        children: [
          InkWell(
            onTap: () {
              // routerDelegate.push(
              // PayOrderPage(),
              // );
            },
            child: const BookCard(),
          ),
          const SizedBox(
            height: 20,
          ),
          const OrderCard(),
          const SizedBox(height: 33),
          const Center(
            child: Text(
              'Отменить заказ',
              style: TextStyle(
                fontSize: P2TextStyle.fontSize,
                fontWeight: P2TextStyle.fontWeight,
                height: P2TextStyle.height,
                color: ColorData.allButtonsError,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/pages/auth/how_to_show_to_spec_page.dart';
import 'package:li/const.dart';
import 'package:li/main.dart';

class ChooseCategoryModal extends StatefulWidget {
  final String assetPath;
  final List<String> list;
  final String title;

  const ChooseCategoryModal({
    required this.assetPath,
    required this.list,
    required this.title,
    Key? key,
  }) : super(key: key);

  @override
  _ChooseCategoryModalState createState() => _ChooseCategoryModalState();
}

class _ChooseCategoryModalState extends State<ChooseCategoryModal> {
  final Map<String, bool> _choosen = Map<String, bool>();

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.all(Radius.circular(16)),
      color: Colors.white,
      child: Stack(
        children: [
          ListView(
            padding: const EdgeInsets.only(bottom: 96),
            children: [
              RepaintBoundary(
                child: SvgPicture.asset(
                  widget.assetPath,
                  width: double.infinity,
                  height: 340,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Text(
                            widget.title,
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 24,
                            ),
                          ),
                        ),
                        const SizedBox(width: 10),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              widget.list.forEach((element) {
                                _choosen[element] = true;
                              });
                            });
                          },
                          child: Text(
                            'Выбрать все',
                            style: TextStyle(
                              color: buttonsDefault,
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 20),
                    SizedBox(
                      width: double.infinity,
                      child: Wrap(
                        alignment: WrapAlignment.start,
                        spacing: 10,
                        runSpacing: 10,
                        children: widget.list.map(
                          (e) {
                            return TextButton(
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                  (_choosen[e] ?? false)
                                      ? buttonPresses
                                      : Colors.transparent,
                                ),
                                tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                foregroundColor: MaterialStateProperty.all(
                                  (_choosen[e] ?? false)
                                      ? Colors.white
                                      : Color(0xff959595),
                                ),
                                shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(6),
                                    ),
                                    side: (_choosen[e] ?? false)
                                        ? BorderSide.none
                                        : BorderSide(
                                            width: 0.5,
                                            color: Color(0xff959595),
                                          ),
                                  ),
                                ),
                              ),
                              onPressed: () {
                                setState(() {
                                  _choosen[e] = !(_choosen[e] ?? false);
                                });
                              },
                              child: Text(
                                e,
                                style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            );
                          },
                        ).toList(),
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(height: 16),
            ],
          ),
          if (_choosen.entries.where((element) => element.value).isNotEmpty)
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              child: Container(
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(0, -2),
                      blurRadius: 5,
                      color: Colors.black.withOpacity(0.06),
                    )
                  ],
                  color: Colors.white,
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 16,
                    vertical: 10,
                  ),
                  child: SizedBox(
                    height: 56,
                    width: double.infinity,
                    child: ElevatedButton(
                      onPressed: () {
                        routerDelegate.push(HowToShowToSpecPage());
                      },
                      style: ButtonStyle(
                        shape: MaterialStateProperty.all(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(16),
                          ),
                        ),
                      ),
                      child: Text(
                        'Выбрать',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}

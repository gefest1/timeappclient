// ignore_for_file: file_names

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class NotifSettingsPage extends StatefulWidget {
  final String phoneNum;
  const NotifSettingsPage({
    required this.phoneNum,
    Key? key,
  }) : super(key: key);

  @override
  State<NotifSettingsPage> createState() => _NotifSettingsPageState();
}

class _NotifSettingsPageState extends State<NotifSettingsPage> {
  bool _on = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        leading: InkWell(
          onTap: () => Navigator.pop(context),
          borderRadius: const BorderRadius.all(
            Radius.circular(40),
          ),
          child: const Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
        ),
        title: const Text(
          'Уведомления',
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500,
            color: Color(0xff424242),
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 30),
        child: ListView(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Из приложения',
                  style: TextStyle(
                    fontSize: P2TextStyle.fontSize,
                  ),
                ),
                CupertinoSwitch(
                    value: _on,
                    onChanged: (v) {
                      _on = v;
                    })
              ],
            ),
            const Divider(
              thickness: 1,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    SvgPicture.asset('assets/svg/icon/CallOutline.svg'),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'По телефону',
                          style: TextStyle(
                              fontSize: 14, color: ColorData.allMainGray),
                        ),
                        Text(
                          widget.phoneNum,
                          style: const TextStyle(
                            fontSize: P2TextStyle.fontSize,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                CupertinoSwitch(value: true, onChanged: (v) {})
              ],
            )
          ],
        ),
      ),
    );
  }
}

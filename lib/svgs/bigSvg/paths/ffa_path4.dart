import 'package:flutter/material.dart';

Path getPath4(Size size) {
  return Path()
    ..moveTo(size.width * 0.1689000, size.height * 0.1382740)
    ..cubicTo(
        size.width * 0.1695425,
        size.height * 0.1384677,
        size.width * 0.1705950,
        size.height * 0.1384677,
        size.width * 0.1712375,
        size.height * 0.1382740)
    ..cubicTo(
        size.width * 0.1718800,
        size.height * 0.1380804,
        size.width * 0.1713550,
        size.height * 0.1379214,
        size.width * 0.1700675,
        size.height * 0.1379214)
    ..cubicTo(
        size.width * 0.1687825,
        size.height * 0.1379214,
        size.width * 0.1682550,
        size.height * 0.1380804,
        size.width * 0.1689000,
        size.height * 0.1382740)
    ..moveTo(size.width * 0.1990950, size.height * 0.1410452)
    ..cubicTo(
        size.width * 0.1990950,
        size.height * 0.1431360,
        size.width * 0.1991775,
        size.height * 0.1439901,
        size.width * 0.1992800,
        size.height * 0.1429472)
    ..cubicTo(
        size.width * 0.1993800,
        size.height * 0.1419043,
        size.width * 0.1993800,
        size.height * 0.1401910,
        size.width * 0.1992800,
        size.height * 0.1391481)
    ..cubicTo(
        size.width * 0.1991775,
        size.height * 0.1381002,
        size.width * 0.1990950,
        size.height * 0.1389544,
        size.width * 0.1990950,
        size.height * 0.1410452)
    ..moveTo(size.width * 0.1319100, size.height * 0.1720343)
    ..cubicTo(
        size.width * 0.1323175,
        size.height * 0.1722479,
        size.width * 0.1329875,
        size.height * 0.1722479,
        size.width * 0.1333975,
        size.height * 0.1720343)
    ..cubicTo(
        size.width * 0.1338075,
        size.height * 0.1718208,
        size.width * 0.1334725,
        size.height * 0.1716469,
        size.width * 0.1326525,
        size.height * 0.1716469)
    ..cubicTo(
        size.width * 0.1318350,
        size.height * 0.1716469,
        size.width * 0.1315000,
        size.height * 0.1718208,
        size.width * 0.1319100,
        size.height * 0.1720343)
    ..moveTo(size.width * 0.9248425, size.height * 0.1807450)
    ..cubicTo(
        size.width * 0.9248425,
        size.height * 0.1823690,
        size.width * 0.9249300,
        size.height * 0.1830345,
        size.width * 0.9250375,
        size.height * 0.1822200)
    ..arcToPoint(Offset(size.width * 0.9250375, size.height * 0.1792651),
        radius: Radius.elliptical(
            size.width * 0.003470000, size.height * 0.006893093),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.9249300,
        size.height * 0.1784506,
        size.width * 0.9248425,
        size.height * 0.1791161,
        size.width * 0.9248425,
        size.height * 0.1807450)
    ..moveTo(size.width * 0.7628875, size.height * 0.1845442)
    ..cubicTo(
        size.width * 0.7628900,
        size.height * 0.1873302,
        size.width * 0.7629675,
        size.height * 0.1883682,
        size.width * 0.7630625,
        size.height * 0.1868535)
    ..arcToPoint(Offset(size.width * 0.7630600, size.height * 0.1817830),
        radius: Radius.elliptical(
            size.width * 0.01144250, size.height * 0.02273032),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.7629625,
        size.height * 0.1805166,
        size.width * 0.7628850,
        size.height * 0.1817581,
        size.width * 0.7628875,
        size.height * 0.1845442)
    ..moveTo(size.width * 0.7603250, size.height * 0.1845442)
    ..cubicTo(
        size.width * 0.7603300,
        size.height * 0.1868684,
        size.width * 0.7604125,
        size.height * 0.1877176,
        size.width * 0.7605100,
        size.height * 0.1864313)
    ..arcToPoint(Offset(size.width * 0.7605025, size.height * 0.1822101),
        radius: Radius.elliptical(
            size.width * 0.007535000, size.height * 0.01496814),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.7604025,
        size.height * 0.1811721,
        size.width * 0.7603225,
        size.height * 0.1822200,
        size.width * 0.7603250,
        size.height * 0.1845442)
    ..moveTo(size.width * 0.8742775, size.height * 0.1849663)
    ..cubicTo(
        size.width * 0.8742775,
        size.height * 0.1875189,
        size.width * 0.8743575,
        size.height * 0.1885668,
        size.width * 0.8744550,
        size.height * 0.1872905)
    ..arcToPoint(Offset(size.width * 0.8744550, size.height * 0.1826421),
        radius: Radius.elliptical(
            size.width * 0.009397500, size.height * 0.01866796),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8743575,
        size.height * 0.1813658,
        size.width * 0.8742775,
        size.height * 0.1824087,
        size.width * 0.8742775,
        size.height * 0.1849663)
    ..moveTo(size.width * 0.8717050, size.height * 0.1845442)
    ..cubicTo(
        size.width * 0.8717100,
        size.height * 0.1864015,
        size.width * 0.8717975,
        size.height * 0.1870620,
        size.width * 0.8719000,
        size.height * 0.1860142)
    ..cubicTo(
        size.width * 0.8720000,
        size.height * 0.1849613,
        size.width * 0.8719975,
        size.height * 0.1834417,
        size.width * 0.8718900,
        size.height * 0.1826322)
    ..cubicTo(
        size.width * 0.8717850,
        size.height * 0.1818277,
        size.width * 0.8717000,
        size.height * 0.1826868,
        size.width * 0.8717050,
        size.height * 0.1845442)
    ..moveTo(size.width * 0.05388000, size.height * 0.1881099)
    ..cubicTo(
        size.width * 0.05463500,
        size.height * 0.1882937,
        size.width * 0.05597500,
        size.height * 0.1882986,
        size.width * 0.05685750,
        size.height * 0.1881149)
    ..cubicTo(
        size.width * 0.05774000,
        size.height * 0.1879311,
        size.width * 0.05712250,
        size.height * 0.1877821,
        size.width * 0.05548500,
        size.height * 0.1877772)
    ..cubicTo(
        size.width * 0.05384750,
        size.height * 0.1877722,
        size.width * 0.05312500,
        size.height * 0.1879212,
        size.width * 0.05388000,
        size.height * 0.1881099)
    ..moveTo(size.width * 0.7922000, size.height * 0.1955245)
    ..cubicTo(
        size.width * 0.7922050,
        size.height * 0.1973818,
        size.width * 0.7922900,
        size.height * 0.1980423,
        size.width * 0.7923925,
        size.height * 0.1969895)
    ..arcToPoint(Offset(size.width * 0.7923850, size.height * 0.1936125),
        radius: Radius.elliptical(
            size.width * 0.004657500, size.height * 0.009252040),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.7922775,
        size.height * 0.1928079,
        size.width * 0.7921950,
        size.height * 0.1936671,
        size.width * 0.7922000,
        size.height * 0.1955245)
    ..moveTo(size.width * 0.7896400, size.height * 0.1959466)
    ..cubicTo(
        size.width * 0.7896400,
        size.height * 0.1975705,
        size.width * 0.7897275,
        size.height * 0.1982360,
        size.width * 0.7898350,
        size.height * 0.1974215)
    ..arcToPoint(Offset(size.width * 0.7898350, size.height * 0.1944667),
        radius: Radius.elliptical(
            size.width * 0.003550000, size.height * 0.007052011),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.7897275,
        size.height * 0.1936522,
        size.width * 0.7896400,
        size.height * 0.1943177,
        size.width * 0.7896400,
        size.height * 0.1959466)
    ..moveTo(size.width * 0.08928500, size.height * 0.2123251)
    ..cubicTo(
        size.width * 0.08928500,
        size.height * 0.2125039,
        size.width * 0.08994250,
        size.height * 0.2128068,
        size.width * 0.09074500,
        size.height * 0.2129906)
    ..cubicTo(
        size.width * 0.09154500,
        size.height * 0.2131743,
        size.width * 0.09210750,
        size.height * 0.2130303,
        size.width * 0.09199500,
        size.height * 0.2126628)
    ..cubicTo(
        size.width * 0.09178750,
        size.height * 0.2119974,
        size.width * 0.08928500,
        size.height * 0.2116895,
        size.width * 0.08928500,
        size.height * 0.2123251)
    ..moveTo(size.width * 0.7917650, size.height * 0.2162137)
    ..cubicTo(
        size.width * 0.7917650,
        size.height * 0.2178426,
        size.width * 0.7918525,
        size.height * 0.2185081,
        size.width * 0.7919600,
        size.height * 0.2176936)
    ..arcToPoint(Offset(size.width * 0.7919600, size.height * 0.2147387),
        radius: Radius.elliptical(
            size.width * 0.003470000, size.height * 0.006893093),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.7918525,
        size.height * 0.2139242,
        size.width * 0.7917650,
        size.height * 0.2145897,
        size.width * 0.7917650,
        size.height * 0.2162137)
    ..moveTo(size.width * 0.7892150, size.height * 0.2170579)
    ..cubicTo(
        size.width * 0.7892150,
        size.height * 0.2186868,
        size.width * 0.7893025,
        size.height * 0.2193523,
        size.width * 0.7894075,
        size.height * 0.2185378)
    ..arcToPoint(Offset(size.width * 0.7894075, size.height * 0.2155830),
        radius: Radius.elliptical(
            size.width * 0.003470000, size.height * 0.006893093),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.7893025,
        size.height * 0.2147685,
        size.width * 0.7892150,
        size.height * 0.2154340,
        size.width * 0.7892150,
        size.height * 0.2170579)
    ..moveTo(size.width * 0.05134750, size.height * 0.2210507)
    ..cubicTo(
        size.width * 0.05211000,
        size.height * 0.2212395,
        size.width * 0.05326000,
        size.height * 0.2212345,
        size.width * 0.05389750,
        size.height * 0.2210408)
    ..cubicTo(
        size.width * 0.05453750,
        size.height * 0.2208471,
        size.width * 0.05391250,
        size.height * 0.2206932,
        size.width * 0.05250750,
        size.height * 0.2206981)
    ..cubicTo(
        size.width * 0.05110500,
        size.height * 0.2207031,
        size.width * 0.05058250,
        size.height * 0.2208620,
        size.width * 0.05134750,
        size.height * 0.2210507)
    ..moveTo(size.width * 0.08896750, size.height * 0.2261262)
    ..cubicTo(
        size.width * 0.09007750,
        size.height * 0.2263000,
        size.width * 0.09189500,
        size.height * 0.2263000,
        size.width * 0.09300500,
        size.height * 0.2261262)
    ..cubicTo(
        size.width * 0.09411750,
        size.height * 0.2259524,
        size.width * 0.09320750,
        size.height * 0.2258084,
        size.width * 0.09098750,
        size.height * 0.2258084)
    ..cubicTo(
        size.width * 0.08876500,
        size.height * 0.2258084,
        size.width * 0.08785500,
        size.height * 0.2259524,
        size.width * 0.08896750,
        size.height * 0.2261262)
    ..moveTo(size.width * 0.4771575, size.height * 0.2390185)
    ..cubicTo(
        size.width * 0.4771575,
        size.height * 0.2411093,
        size.width * 0.4772400,
        size.height * 0.2419634,
        size.width * 0.4773400,
        size.height * 0.2409205)
    ..arcToPoint(Offset(size.width * 0.4773400, size.height * 0.2371214),
        radius: Radius.elliptical(
            size.width * 0.005985000, size.height * 0.01188909),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.4772400,
        size.height * 0.2360735,
        size.width * 0.4771575,
        size.height * 0.2369277,
        size.width * 0.4771575,
        size.height * 0.2390185)
    ..moveTo(size.width * 0.7824200, size.height * 0.2379110)
    ..cubicTo(
        size.width * 0.7828275,
        size.height * 0.2381246,
        size.width * 0.7834975,
        size.height * 0.2381246,
        size.width * 0.7839075,
        size.height * 0.2379110)
    ..cubicTo(
        size.width * 0.7843175,
        size.height * 0.2377024,
        size.width * 0.7839825,
        size.height * 0.2375286,
        size.width * 0.7831625,
        size.height * 0.2375286)
    ..cubicTo(
        size.width * 0.7823450,
        size.height * 0.2375286,
        size.width * 0.7820100,
        size.height * 0.2377024,
        size.width * 0.7824200,
        size.height * 0.2379110)
    ..moveTo(size.width * 0.8272800, size.height * 0.2379259)
    ..cubicTo(
        size.width * 0.8278075,
        size.height * 0.2381295,
        size.width * 0.8285750,
        size.height * 0.2381246,
        size.width * 0.8289800,
        size.height * 0.2379110)
    ..cubicTo(
        size.width * 0.8293875,
        size.height * 0.2376975,
        size.width * 0.8289550,
        size.height * 0.2375336,
        size.width * 0.8280175,
        size.height * 0.2375435)
    ..cubicTo(
        size.width * 0.8270825,
        size.height * 0.2375534,
        size.width * 0.8267500,
        size.height * 0.2377273,
        size.width * 0.8272800,
        size.height * 0.2379259)
    ..moveTo(size.width * 0.8568250, size.height * 0.2379110)
    ..cubicTo(
        size.width * 0.8572325,
        size.height * 0.2381246,
        size.width * 0.8579025,
        size.height * 0.2381246,
        size.width * 0.8583125,
        size.height * 0.2379110)
    ..cubicTo(
        size.width * 0.8587225,
        size.height * 0.2377024,
        size.width * 0.8583875,
        size.height * 0.2375286,
        size.width * 0.8575675,
        size.height * 0.2375286)
    ..cubicTo(
        size.width * 0.8567500,
        size.height * 0.2375286,
        size.width * 0.8564150,
        size.height * 0.2377024,
        size.width * 0.8568250,
        size.height * 0.2379110)
    ..moveTo(size.width * 0.9016850, size.height * 0.2379259)
    ..cubicTo(
        size.width * 0.9022125,
        size.height * 0.2381295,
        size.width * 0.9029775,
        size.height * 0.2381246,
        size.width * 0.9033850,
        size.height * 0.2379110)
    ..cubicTo(
        size.width * 0.9037925,
        size.height * 0.2376975,
        size.width * 0.9033600,
        size.height * 0.2375336,
        size.width * 0.9024225,
        size.height * 0.2375435)
    ..cubicTo(
        size.width * 0.9014875,
        size.height * 0.2375534,
        size.width * 0.9011550,
        size.height * 0.2377273,
        size.width * 0.9016850,
        size.height * 0.2379259)
    ..moveTo(size.width * 0.7743400, size.height * 0.2429815)
    ..cubicTo(
        size.width * 0.7747500,
        size.height * 0.2431951,
        size.width * 0.7754200,
        size.height * 0.2431951,
        size.width * 0.7758300,
        size.height * 0.2429815)
    ..cubicTo(
        size.width * 0.7762375,
        size.height * 0.2427680,
        size.width * 0.7759025,
        size.height * 0.2425941,
        size.width * 0.7750850,
        size.height * 0.2425941)
    ..cubicTo(
        size.width * 0.7742675,
        size.height * 0.2425941,
        size.width * 0.7739325,
        size.height * 0.2427680,
        size.width * 0.7743400,
        size.height * 0.2429815)
    ..moveTo(size.width * 0.7824200, size.height * 0.2429815)
    ..cubicTo(
        size.width * 0.7828275,
        size.height * 0.2431951,
        size.width * 0.7834975,
        size.height * 0.2431951,
        size.width * 0.7839075,
        size.height * 0.2429815)
    ..cubicTo(
        size.width * 0.7843175,
        size.height * 0.2427680,
        size.width * 0.7839825,
        size.height * 0.2425941,
        size.width * 0.7831625,
        size.height * 0.2425941)
    ..cubicTo(
        size.width * 0.7823450,
        size.height * 0.2425941,
        size.width * 0.7820100,
        size.height * 0.2427680,
        size.width * 0.7824200,
        size.height * 0.2429815)
    ..moveTo(size.width * 0.8194100, size.height * 0.2429815)
    ..cubicTo(
        size.width * 0.8198175,
        size.height * 0.2431951,
        size.width * 0.8204875,
        size.height * 0.2431951,
        size.width * 0.8208975,
        size.height * 0.2429815)
    ..cubicTo(
        size.width * 0.8213075,
        size.height * 0.2427680,
        size.width * 0.8209725,
        size.height * 0.2425941,
        size.width * 0.8201525,
        size.height * 0.2425941)
    ..cubicTo(
        size.width * 0.8193350,
        size.height * 0.2425941,
        size.width * 0.8190000,
        size.height * 0.2427680,
        size.width * 0.8194100,
        size.height * 0.2429815)
    ..moveTo(size.width * 0.8270625, size.height * 0.2429815)
    ..cubicTo(
        size.width * 0.8274725,
        size.height * 0.2431951,
        size.width * 0.8281400,
        size.height * 0.2431951,
        size.width * 0.8285500,
        size.height * 0.2429815)
    ..cubicTo(
        size.width * 0.8289600,
        size.height * 0.2427680,
        size.width * 0.8286250,
        size.height * 0.2425941,
        size.width * 0.8278050,
        size.height * 0.2425941)
    ..cubicTo(
        size.width * 0.8269875,
        size.height * 0.2425941,
        size.width * 0.8266525,
        size.height * 0.2427680,
        size.width * 0.8270625,
        size.height * 0.2429815)
    ..moveTo(size.width * 0.8487450, size.height * 0.2429815)
    ..cubicTo(
        size.width * 0.8491550,
        size.height * 0.2431951,
        size.width * 0.8498250,
        size.height * 0.2431951,
        size.width * 0.8502350,
        size.height * 0.2429815)
    ..cubicTo(
        size.width * 0.8506425,
        size.height * 0.2427680,
        size.width * 0.8503075,
        size.height * 0.2425941,
        size.width * 0.8494900,
        size.height * 0.2425941)
    ..cubicTo(
        size.width * 0.8486725,
        size.height * 0.2425941,
        size.width * 0.8483375,
        size.height * 0.2427680,
        size.width * 0.8487450,
        size.height * 0.2429815)
    ..moveTo(size.width * 0.8564000, size.height * 0.2429815)
    ..cubicTo(
        size.width * 0.8568075,
        size.height * 0.2431951,
        size.width * 0.8574775,
        size.height * 0.2431951,
        size.width * 0.8578875,
        size.height * 0.2429815)
    ..cubicTo(
        size.width * 0.8582950,
        size.height * 0.2427680,
        size.width * 0.8579625,
        size.height * 0.2425941,
        size.width * 0.8571425,
        size.height * 0.2425941)
    ..cubicTo(
        size.width * 0.8563250,
        size.height * 0.2425941,
        size.width * 0.8559900,
        size.height * 0.2427680,
        size.width * 0.8564000,
        size.height * 0.2429815)
    ..moveTo(size.width * 0.8938150, size.height * 0.2429815)
    ..cubicTo(
        size.width * 0.8942225,
        size.height * 0.2431951,
        size.width * 0.8948925,
        size.height * 0.2431951,
        size.width * 0.8953025,
        size.height * 0.2429815)
    ..cubicTo(
        size.width * 0.8957100,
        size.height * 0.2427680,
        size.width * 0.8953775,
        size.height * 0.2425941,
        size.width * 0.8945575,
        size.height * 0.2425941)
    ..cubicTo(
        size.width * 0.8937400,
        size.height * 0.2425941,
        size.width * 0.8934050,
        size.height * 0.2427680,
        size.width * 0.8938150,
        size.height * 0.2429815)
    ..moveTo(size.width * 0.9014675, size.height * 0.2429815)
    ..cubicTo(
        size.width * 0.9018750,
        size.height * 0.2431951,
        size.width * 0.9025450,
        size.height * 0.2431951,
        size.width * 0.9029550,
        size.height * 0.2429815)
    ..cubicTo(
        size.width * 0.9033650,
        size.height * 0.2427680,
        size.width * 0.9030300,
        size.height * 0.2425941,
        size.width * 0.9022100,
        size.height * 0.2425941)
    ..cubicTo(
        size.width * 0.9013925,
        size.height * 0.2425941,
        size.width * 0.9010575,
        size.height * 0.2427680,
        size.width * 0.9014675,
        size.height * 0.2429815)
    ..moveTo(size.width * 0.7914100, size.height * 0.2609790)
    ..cubicTo(
        size.width * 0.7914100,
        size.height * 0.2677132,
        size.width * 0.7914775,
        size.height * 0.2704695,
        size.width * 0.7915575,
        size.height * 0.2671024)
    ..cubicTo(
        size.width * 0.7916375,
        size.height * 0.2637353,
        size.width * 0.7916375,
        size.height * 0.2582228,
        size.width * 0.7915575,
        size.height * 0.2548557)
    ..cubicTo(
        size.width * 0.7914775,
        size.height * 0.2514886,
        size.width * 0.7914100,
        size.height * 0.2542449,
        size.width * 0.7914100,
        size.height * 0.2609790)
    ..moveTo(size.width * 0.7888600, size.height * 0.2630897)
    ..cubicTo(
        size.width * 0.7888600,
        size.height * 0.2700573,
        size.width * 0.7889275,
        size.height * 0.2727986,
        size.width * 0.7890075,
        size.height * 0.2691733)
    ..cubicTo(
        size.width * 0.7890875,
        size.height * 0.2655529,
        size.width * 0.7890875,
        size.height * 0.2598517,
        size.width * 0.7890050,
        size.height * 0.2565045)
    ..cubicTo(
        size.width * 0.7889250,
        size.height * 0.2531622,
        size.width * 0.7888600,
        size.height * 0.2561221,
        size.width * 0.7888600,
        size.height * 0.2630897)
    ..moveTo(size.width * 0.4967625, size.height * 0.2778691)
    ..cubicTo(
        size.width * 0.4967625,
        size.height * 0.2836796,
        size.width * 0.4968300,
        size.height * 0.2860534,
        size.width * 0.4969125,
        size.height * 0.2831482)
    ..cubicTo(
        size.width * 0.4969975,
        size.height * 0.2802479,
        size.width * 0.4969975,
        size.height * 0.2754953,
        size.width * 0.4969125,
        size.height * 0.2725900)
    ..cubicTo(
        size.width * 0.4968300,
        size.height * 0.2696898,
        size.width * 0.4967625,
        size.height * 0.2720636,
        size.width * 0.4967625,
        size.height * 0.2778691)
    ..moveTo(size.width * 0.4286700, size.height * 0.2930756)
    ..cubicTo(
        size.width * 0.4286700,
        size.height * 0.2946996,
        size.width * 0.4287575,
        size.height * 0.2953650,
        size.width * 0.4288650,
        size.height * 0.2945506)
    ..arcToPoint(Offset(size.width * 0.4288650, size.height * 0.2915957),
        radius: Radius.elliptical(
            size.width * 0.003470000, size.height * 0.006893093),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.4287575,
        size.height * 0.2907812,
        size.width * 0.4286700,
        size.height * 0.2914467,
        size.width * 0.4286700,
        size.height * 0.2930756)
    ..moveTo(size.width * 0.04262500, size.height * 0.2960305)
    ..cubicTo(
        size.width * 0.04263000,
        size.height * 0.2978879,
        size.width * 0.04271500,
        size.height * 0.2985484,
        size.width * 0.04281750,
        size.height * 0.2974955)
    ..arcToPoint(Offset(size.width * 0.04281000, size.height * 0.2941185),
        radius: Radius.elliptical(
            size.width * 0.004657500, size.height * 0.009252040),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.04270250,
        size.height * 0.2933140,
        size.width * 0.04262000,
        size.height * 0.2941732,
        size.width * 0.04262500,
        size.height * 0.2960305)
    ..moveTo(size.width * 0.4729175, size.height * 0.2972969)
    ..cubicTo(
        size.width * 0.4729175,
        size.height * 0.2998495,
        size.width * 0.4729950,
        size.height * 0.3008974,
        size.width * 0.4730925,
        size.height * 0.2996211)
    ..arcToPoint(Offset(size.width * 0.4730925, size.height * 0.2949727),
        radius: Radius.elliptical(
            size.width * 0.009397500, size.height * 0.01866796),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.4729950,
        size.height * 0.2936964,
        size.width * 0.4729175,
        size.height * 0.2947393,
        size.width * 0.4729175,
        size.height * 0.2972969)
    ..moveTo(size.width * 0.4911775, size.height * 0.3088582)
    ..cubicTo(
        size.width * 0.4915875,
        size.height * 0.3090718,
        size.width * 0.4922575,
        size.height * 0.3090718,
        size.width * 0.4926650,
        size.height * 0.3088582)
    ..cubicTo(
        size.width * 0.4930750,
        size.height * 0.3086447,
        size.width * 0.4927400,
        size.height * 0.3084709,
        size.width * 0.4919225,
        size.height * 0.3084709)
    ..cubicTo(
        size.width * 0.4911025,
        size.height * 0.3084709,
        size.width * 0.4907675,
        size.height * 0.3086447,
        size.width * 0.4911775,
        size.height * 0.3088582)
    ..moveTo(size.width * 0.07302250, size.height * 0.3122501)
    ..cubicTo(
        size.width * 0.07355000,
        size.height * 0.3124538,
        size.width * 0.07441000,
        size.height * 0.3124538,
        size.width * 0.07493500,
        size.height * 0.3122501)
    ..cubicTo(
        size.width * 0.07546250,
        size.height * 0.3120515,
        size.width * 0.07503250,
        size.height * 0.3118826,
        size.width * 0.07398000,
        size.height * 0.3118826)
    ..cubicTo(
        size.width * 0.07292750,
        size.height * 0.3118826,
        size.width * 0.07249750,
        size.height * 0.3120515,
        size.width * 0.07302250,
        size.height * 0.3122501)
    ..moveTo(size.width * 0.06069250, size.height * 0.3130944)
    ..arcToPoint(Offset(size.width * 0.06260750, size.height * 0.3130944),
        radius: Radius.elliptical(
            size.width * 0.006000000, size.height * 0.01191889),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.06313250,
        size.height * 0.3128957,
        size.width * 0.06270250,
        size.height * 0.3127269,
        size.width * 0.06165000,
        size.height * 0.3127269)
    ..cubicTo(
        size.width * 0.06059750,
        size.height * 0.3127269,
        size.width * 0.06016750,
        size.height * 0.3128957,
        size.width * 0.06069250,
        size.height * 0.3130944)
    ..moveTo(size.width * 0.04879250, size.height * 0.3139436)
    ..cubicTo(
        size.width * 0.04932250,
        size.height * 0.3141423,
        size.width * 0.05008750,
        size.height * 0.3141373,
        size.width * 0.05049500,
        size.height * 0.3139237)
    ..cubicTo(
        size.width * 0.05090000,
        size.height * 0.3137152,
        size.width * 0.05046750,
        size.height * 0.3135463,
        size.width * 0.04953250,
        size.height * 0.3135562)
    ..cubicTo(
        size.width * 0.04859750,
        size.height * 0.3135662,
        size.width * 0.04826500,
        size.height * 0.3137400,
        size.width * 0.04879250,
        size.height * 0.3139436)
    ..moveTo(size.width * 0.5353950, size.height * 0.3215270)
    ..cubicTo(
        size.width * 0.5358050,
        size.height * 0.3217406,
        size.width * 0.5364750,
        size.height * 0.3217406,
        size.width * 0.5368825,
        size.height * 0.3215270)
    ..cubicTo(
        size.width * 0.5372925,
        size.height * 0.3213135,
        size.width * 0.5369575,
        size.height * 0.3211396,
        size.width * 0.5361400,
        size.height * 0.3211396)
    ..cubicTo(
        size.width * 0.5353200,
        size.height * 0.3211396,
        size.width * 0.5349850,
        size.height * 0.3213135,
        size.width * 0.5353950,
        size.height * 0.3215270)
    ..moveTo(size.width * 0.7888525, size.height * 0.3526155)
    ..cubicTo(
        size.width * 0.7888525,
        size.height * 0.3581925,
        size.width * 0.7889225,
        size.height * 0.3603627,
        size.width * 0.7890050,
        size.height * 0.3574426)
    ..cubicTo(
        size.width * 0.7890900,
        size.height * 0.3545175,
        size.width * 0.7890875,
        size.height * 0.3499585,
        size.width * 0.7890050,
        size.height * 0.3473066)
    ..cubicTo(
        size.width * 0.7889200,
        size.height * 0.3446546,
        size.width * 0.7888525,
        size.height * 0.3470434,
        size.width * 0.7888525,
        size.height * 0.3526155)
    ..moveTo(size.width * 0.8836025, size.height * 0.3454393)
    ..cubicTo(
        size.width * 0.8836025,
        size.height * 0.3470632,
        size.width * 0.8836900,
        size.height * 0.3477287,
        size.width * 0.8837975,
        size.height * 0.3469142)
    ..arcToPoint(Offset(size.width * 0.8837975, size.height * 0.3439594),
        radius: Radius.elliptical(
            size.width * 0.003550000, size.height * 0.007052011),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8836900,
        size.height * 0.3431499,
        size.width * 0.8836025,
        size.height * 0.3438104,
        size.width * 0.8836025,
        size.height * 0.3454393)
    ..moveTo(size.width * 0.8861525, size.height * 0.3462835)
    ..cubicTo(
        size.width * 0.8861525,
        size.height * 0.3479075,
        size.width * 0.8862400,
        size.height * 0.3485730,
        size.width * 0.8863475,
        size.height * 0.3477635)
    ..arcToPoint(Offset(size.width * 0.8863475, size.height * 0.3448036),
        radius: Radius.elliptical(
            size.width * 0.003482500, size.height * 0.006917924),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8862400,
        size.height * 0.3439941,
        size.width * 0.8861525,
        size.height * 0.3446596,
        size.width * 0.8861525,
        size.height * 0.3462835)
    ..moveTo(size.width * 0.7914000, size.height * 0.3547311)
    ..cubicTo(
        size.width * 0.7914000,
        size.height * 0.3596079,
        size.width * 0.7914700,
        size.height * 0.3616043,
        size.width * 0.7915550,
        size.height * 0.3591609)
    ..cubicTo(
        size.width * 0.7916400,
        size.height * 0.3567225,
        size.width * 0.7916400,
        size.height * 0.3527346,
        size.width * 0.7915550,
        size.height * 0.3502962)
    ..cubicTo(
        size.width * 0.7914700,
        size.height * 0.3478578,
        size.width * 0.7914000,
        size.height * 0.3498493,
        size.width * 0.7914000,
        size.height * 0.3547311)
    ..moveTo(size.width * 0.5958750, size.height * 0.3614851)
    ..cubicTo(
        size.width * 0.5967900,
        size.height * 0.3633425,
        size.width * 0.5976325,
        size.height * 0.3648621,
        size.width * 0.5977500,
        size.height * 0.3648621)
    ..cubicTo(
        size.width * 0.5978650,
        size.height * 0.3648621,
        size.width * 0.5972150,
        size.height * 0.3633425,
        size.width * 0.5963000,
        size.height * 0.3614851)
    ..cubicTo(
        size.width * 0.5953875,
        size.height * 0.3596277,
        size.width * 0.5945450,
        size.height * 0.3581081,
        size.width * 0.5944275,
        size.height * 0.3581081)
    ..cubicTo(
        size.width * 0.5943100,
        size.height * 0.3581081,
        size.width * 0.5949625,
        size.height * 0.3596277,
        size.width * 0.5958750,
        size.height * 0.3614851)
    ..moveTo(size.width * 0.8840500, size.height * 0.3686662)
    ..cubicTo(
        size.width * 0.8840525,
        size.height * 0.3709854,
        size.width * 0.8841350,
        size.height * 0.3718396,
        size.width * 0.8842325,
        size.height * 0.3705534)
    ..cubicTo(
        size.width * 0.8843325,
        size.height * 0.3692671,
        size.width * 0.8843300,
        size.height * 0.3673700,
        size.width * 0.8842275,
        size.height * 0.3663321)
    ..cubicTo(
        size.width * 0.8841275,
        size.height * 0.3652892,
        size.width * 0.8840475,
        size.height * 0.3663420,
        size.width * 0.8840500,
        size.height * 0.3686662)
    ..moveTo(size.width * 0.8866000, size.height * 0.3695105)
    ..cubicTo(
        size.width * 0.8866050,
        size.height * 0.3718297,
        size.width * 0.8866875,
        size.height * 0.3726839,
        size.width * 0.8867850,
        size.height * 0.3713976)
    ..arcToPoint(Offset(size.width * 0.8867800, size.height * 0.3671764),
        radius: Radius.elliptical(
            size.width * 0.007632500, size.height * 0.01516182),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8866775,
        size.height * 0.3661384,
        size.width * 0.8865975,
        size.height * 0.3671863,
        size.width * 0.8866000,
        size.height * 0.3695105)
    ..moveTo(size.width * 0.3939000, size.height * 0.3819657)
    ..lineTo(size.width * 0.3922200, size.height * 0.3855563)
    ..lineTo(size.width * 0.3940275, size.height * 0.3822190)
    ..cubicTo(
        size.width * 0.3950200,
        size.height * 0.3803815,
        size.width * 0.3958325,
        size.height * 0.3787675,
        size.width * 0.3958325,
        size.height * 0.3786284)
    ..cubicTo(
        size.width * 0.3958325,
        size.height * 0.3779878,
        size.width * 0.3954625,
        size.height * 0.3786284,
        size.width * 0.3939000,
        size.height * 0.3819657)
    ..moveTo(size.width * 0.7892225, size.height * 0.3931595)
    ..cubicTo(
        size.width * 0.7892275,
        size.height * 0.3950169,
        size.width * 0.7893150,
        size.height * 0.3956774,
        size.width * 0.7894175,
        size.height * 0.3946246)
    ..cubicTo(
        size.width * 0.7895175,
        size.height * 0.3935767,
        size.width * 0.7895150,
        size.height * 0.3920571,
        size.width * 0.7894075,
        size.height * 0.3912476)
    ..cubicTo(
        size.width * 0.7893025,
        size.height * 0.3904381,
        size.width * 0.7892175,
        size.height * 0.3913022,
        size.width * 0.7892225,
        size.height * 0.3931595)
    ..moveTo(size.width * 0.7917750, size.height * 0.3940038)
    ..cubicTo(
        size.width * 0.7917775,
        size.height * 0.3958612,
        size.width * 0.7918650,
        size.height * 0.3965217,
        size.width * 0.7919675,
        size.height * 0.3954688)
    ..arcToPoint(Offset(size.width * 0.7919600, size.height * 0.3920918),
        radius: Radius.elliptical(
            size.width * 0.004657500, size.height * 0.009252040),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.7918525,
        size.height * 0.3912823,
        size.width * 0.7917700,
        size.height * 0.3921464,
        size.width * 0.7917750,
        size.height * 0.3940038)
    ..moveTo(size.width * 0.5279550, size.height * 0.4161332)
    ..cubicTo(
        size.width * 0.5284800,
        size.height * 0.4163368,
        size.width * 0.5293425,
        size.height * 0.4163368,
        size.width * 0.5298675,
        size.height * 0.4161332)
    ..cubicTo(
        size.width * 0.5303950,
        size.height * 0.4159346,
        size.width * 0.5299650,
        size.height * 0.4157707,
        size.width * 0.5289125,
        size.height * 0.4157707)
    ..cubicTo(
        size.width * 0.5278600,
        size.height * 0.4157707,
        size.width * 0.5274300,
        size.height * 0.4159346,
        size.width * 0.5279550,
        size.height * 0.4161332)
    ..moveTo(size.width * 0.3411725, size.height * 0.4191278)
    ..lineTo(size.width * 0.3399225, size.height * 0.4218741)
    ..lineTo(size.width * 0.3413050, size.height * 0.4193910)
    ..cubicTo(
        size.width * 0.3420650,
        size.height * 0.4180303,
        size.width * 0.3426875,
        size.height * 0.4167937,
        size.width * 0.3426875,
        size.height * 0.4166497)
    ..cubicTo(
        size.width * 0.3426875,
        size.height * 0.4159942,
        size.width * 0.3423375,
        size.height * 0.4165702,
        size.width * 0.3411725,
        size.height * 0.4191278)
    ..moveTo(size.width * 0.5407100, size.height * 0.4169824)
    ..cubicTo(
        size.width * 0.5412350,
        size.height * 0.4171811,
        size.width * 0.5420975,
        size.height * 0.4171811,
        size.width * 0.5426225,
        size.height * 0.4169824)
    ..cubicTo(
        size.width * 0.5431500,
        size.height * 0.4167788,
        size.width * 0.5427200,
        size.height * 0.4166149,
        size.width * 0.5416675,
        size.height * 0.4166149)
    ..cubicTo(
        size.width * 0.5406150,
        size.height * 0.4166149,
        size.width * 0.5401850,
        size.height * 0.4167788,
        size.width * 0.5407100,
        size.height * 0.4169824)
    ..moveTo(size.width * 0.5534650, size.height * 0.4178267)
    ..cubicTo(
        size.width * 0.5539925,
        size.height * 0.4180253,
        size.width * 0.5548525,
        size.height * 0.4180253,
        size.width * 0.5553775,
        size.height * 0.4178267)
    ..cubicTo(
        size.width * 0.5559050,
        size.height * 0.4176231,
        size.width * 0.5554750,
        size.height * 0.4174592,
        size.width * 0.5544225,
        size.height * 0.4174592)
    ..cubicTo(
        size.width * 0.5533700,
        size.height * 0.4174592,
        size.width * 0.5529400,
        size.height * 0.4176231,
        size.width * 0.5534650,
        size.height * 0.4178267)
    ..moveTo(size.width * 0.6577375, size.height * 0.4174940)
    ..cubicTo(
        size.width * 0.6577375,
        size.height * 0.4176380,
        size.width * 0.6583600,
        size.height * 0.4188746,
        size.width * 0.6591200,
        size.height * 0.4202403)
    ..lineTo(size.width * 0.6605025, size.height * 0.4227184)
    ..lineTo(size.width * 0.6592525, size.height * 0.4199721)
    ..cubicTo(
        size.width * 0.6580900,
        size.height * 0.4174145,
        size.width * 0.6577375,
        size.height * 0.4168384,
        size.width * 0.6577375,
        size.height * 0.4174940)
    ..moveTo(size.width * 0.5666500, size.height * 0.4186709)
    ..cubicTo(
        size.width * 0.5671800,
        size.height * 0.4188746,
        size.width * 0.5679450,
        size.height * 0.4188646,
        size.width * 0.5683500,
        size.height * 0.4186560)
    ..cubicTo(
        size.width * 0.5687575,
        size.height * 0.4184425,
        size.width * 0.5683250,
        size.height * 0.4182786,
        size.width * 0.5673900,
        size.height * 0.4182885)
    ..cubicTo(
        size.width * 0.5664550,
        size.height * 0.4182935,
        size.width * 0.5661225,
        size.height * 0.4184673,
        size.width * 0.5666500,
        size.height * 0.4186709)
    ..moveTo(size.width * 0.3815175, size.height * 0.4413019)
    ..cubicTo(
        size.width * 0.3815200,
        size.height * 0.4445498,
        size.width * 0.3815950,
        size.height * 0.4457765,
        size.width * 0.3816875,
        size.height * 0.4440234)
    ..arcToPoint(Offset(size.width * 0.3816850, size.height * 0.4381136),
        radius: Radius.elliptical(
            size.width * 0.01595750, size.height * 0.03169929),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.3815900,
        size.height * 0.4366138,
        size.width * 0.3815150,
        size.height * 0.4380491,
        size.width * 0.3815175,
        size.height * 0.4413019)
    ..moveTo(size.width * 0.6021400, size.height * 0.4611469)
    ..cubicTo(
        size.width * 0.6021400,
        size.height * 0.4627758,
        size.width * 0.6022275,
        size.height * 0.4634363,
        size.width * 0.6023350,
        size.height * 0.4626268)
    ..arcToPoint(Offset(size.width * 0.6023350, size.height * 0.4596719),
        radius: Radius.elliptical(
            size.width * 0.003550000, size.height * 0.007052011),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.6022275,
        size.height * 0.4588575,
        size.width * 0.6021400,
        size.height * 0.4595229,
        size.width * 0.6021400,
        size.height * 0.4611469)
    ..moveTo(size.width * 0.3589500, size.height * 0.4887590)
    ..cubicTo(
        size.width * 0.3593600,
        size.height * 0.4889676,
        size.width * 0.3600275,
        size.height * 0.4889676,
        size.width * 0.3604375,
        size.height * 0.4887590)
    ..cubicTo(
        size.width * 0.3608475,
        size.height * 0.4885454,
        size.width * 0.3605125,
        size.height * 0.4883716,
        size.width * 0.3596950,
        size.height * 0.4883716)
    ..cubicTo(
        size.width * 0.3588750,
        size.height * 0.4883716,
        size.width * 0.3585400,
        size.height * 0.4885454,
        size.width * 0.3589500,
        size.height * 0.4887590)
    ..moveTo(size.width * 0.6391375, size.height * 0.4887590)
    ..cubicTo(
        size.width * 0.6395450,
        size.height * 0.4889676,
        size.width * 0.6402150,
        size.height * 0.4889676,
        size.width * 0.6406250,
        size.height * 0.4887590)
    ..cubicTo(
        size.width * 0.6410350,
        size.height * 0.4885454,
        size.width * 0.6407000,
        size.height * 0.4883716,
        size.width * 0.6398800,
        size.height * 0.4883716)
    ..cubicTo(
        size.width * 0.6390625,
        size.height * 0.4883716,
        size.width * 0.6387275,
        size.height * 0.4885454,
        size.width * 0.6391375,
        size.height * 0.4887590)
    ..moveTo(size.width * 0.3660475, size.height * 0.5023217)
    ..lineTo(size.width * 0.3645825, size.height * 0.5054901)
    ..lineTo(size.width * 0.3661775, size.height * 0.5025799)
    ..cubicTo(
        size.width * 0.3670550,
        size.height * 0.5009759,
        size.width * 0.3677725,
        size.height * 0.4995506,
        size.width * 0.3677725,
        size.height * 0.4994115)
    ..cubicTo(
        size.width * 0.3677725,
        size.height * 0.4987659,
        size.width * 0.3674125,
        size.height * 0.4993718,
        size.width * 0.3660475,
        size.height * 0.5023217)
    ..moveTo(size.width * 0.6335025, size.height * 0.5021082)
    ..cubicTo(
        size.width * 0.6342975,
        size.height * 0.5037371,
        size.width * 0.6350425,
        size.height * 0.5050680,
        size.width * 0.6351575,
        size.height * 0.5050680)
    ..cubicTo(
        size.width * 0.6352750,
        size.height * 0.5050680,
        size.width * 0.6347225,
        size.height * 0.5037371,
        size.width * 0.6339275,
        size.height * 0.5021082)
    ..cubicTo(
        size.width * 0.6331350,
        size.height * 0.5004842,
        size.width * 0.6323900,
        size.height * 0.4991533,
        size.width * 0.6322725,
        size.height * 0.4991533)
    ..cubicTo(
        size.width * 0.6321575,
        size.height * 0.4991533,
        size.width * 0.6327100,
        size.height * 0.5004842,
        size.width * 0.6335025,
        size.height * 0.5021082)
    ..moveTo(size.width * 0.5613300, size.height * 0.5107593)
    ..cubicTo(
        size.width * 0.5626750,
        size.height * 0.5109281,
        size.width * 0.5648750,
        size.height * 0.5109281,
        size.width * 0.5662200,
        size.height * 0.5107593)
    ..cubicTo(
        size.width * 0.5675650,
        size.height * 0.5105904,
        size.width * 0.5664650,
        size.height * 0.5104514,
        size.width * 0.5637750,
        size.height * 0.5104514)
    ..cubicTo(
        size.width * 0.5610875,
        size.height * 0.5104514,
        size.width * 0.5599850,
        size.height * 0.5105904,
        size.width * 0.5613300,
        size.height * 0.5107593)
    ..moveTo(size.width * 0.2066325, size.height * 0.5526294)
    ..cubicTo(
        size.width * 0.2066325,
        size.height * 0.5527734,
        size.width * 0.2072550,
        size.height * 0.5540100,
        size.width * 0.2080150,
        size.height * 0.5553707)
    ..lineTo(size.width * 0.2093975, size.height * 0.5578538)
    ..lineTo(size.width * 0.2081475, size.height * 0.5551075)
    ..cubicTo(
        size.width * 0.2069825,
        size.height * 0.5525499,
        size.width * 0.2066325,
        size.height * 0.5519738,
        size.width * 0.2066325,
        size.height * 0.5526294)
    ..moveTo(size.width * 0.03760750, size.height * 0.5660878)
    ..lineTo(size.width * 0.03592750, size.height * 0.5696783)
    ..lineTo(size.width * 0.03773500, size.height * 0.5663410)
    ..cubicTo(
        size.width * 0.03872750,
        size.height * 0.5645036,
        size.width * 0.03954000,
        size.height * 0.5628895,
        size.width * 0.03954000,
        size.height * 0.5627505)
    ..cubicTo(
        size.width * 0.03954000,
        size.height * 0.5621098,
        size.width * 0.03917000,
        size.height * 0.5627505,
        size.width * 0.03760750,
        size.height * 0.5660878)
    ..moveTo(size.width * 0.3249675, size.height * 0.5724247)
    ..cubicTo(
        size.width * 0.3331100,
        size.height * 0.5725488,
        size.width * 0.3463125,
        size.height * 0.5725488,
        size.width * 0.3543050,
        size.height * 0.5724247)
    ..cubicTo(
        size.width * 0.3622950,
        size.height * 0.5722955,
        size.width * 0.3556325,
        size.height * 0.5721863,
        size.width * 0.3394975,
        size.height * 0.5721912)
    ..cubicTo(
        size.width * 0.3233625,
        size.height * 0.5721912,
        size.width * 0.3168250,
        size.height * 0.5722955,
        size.width * 0.3249675,
        size.height * 0.5724247)
    ..moveTo(size.width * 0.6459700, size.height * 0.5724247)
    ..cubicTo(
        size.width * 0.6541150,
        size.height * 0.5725488,
        size.width * 0.6673150,
        size.height * 0.5725488,
        size.width * 0.6753075,
        size.height * 0.5724247)
    ..cubicTo(
        size.width * 0.6833000,
        size.height * 0.5722955,
        size.width * 0.6766375,
        size.height * 0.5721863,
        size.width * 0.6605025,
        size.height * 0.5721912)
    ..cubicTo(
        size.width * 0.6443675,
        size.height * 0.5721912,
        size.width * 0.6378275,
        size.height * 0.5722955,
        size.width * 0.6459700,
        size.height * 0.5724247)
    ..moveTo(size.width * 0.3931800, size.height * 0.5774554)
    ..cubicTo(
        size.width * 0.3937100,
        size.height * 0.5776541,
        size.width * 0.3944750,
        size.height * 0.5776491,
        size.width * 0.3948825,
        size.height * 0.5774356)
    ..cubicTo(
        size.width * 0.3952875,
        size.height * 0.5772270,
        size.width * 0.3948550,
        size.height * 0.5770581,
        size.width * 0.3939200,
        size.height * 0.5770681)
    ..cubicTo(
        size.width * 0.3929850,
        size.height * 0.5770780,
        size.width * 0.3926525,
        size.height * 0.5772518,
        size.width * 0.3931800,
        size.height * 0.5774554)
    ..moveTo(size.width * 0.6076750, size.height * 0.5899504)
    ..cubicTo(
        size.width * 0.6076800,
        size.height * 0.5918077,
        size.width * 0.6077675,
        size.height * 0.5924683,
        size.width * 0.6078675,
        size.height * 0.5914154)
    ..arcToPoint(Offset(size.width * 0.6078600, size.height * 0.5880384),
        radius: Radius.elliptical(
            size.width * 0.004600000, size.height * 0.009137817),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.6077525,
        size.height * 0.5872289,
        size.width * 0.6076700,
        size.height * 0.5880881,
        size.width * 0.6076750,
        size.height * 0.5899504)
    ..moveTo(size.width * 0.02400250, size.height * 0.5939581)
    ..lineTo(size.width * 0.02232250, size.height * 0.5975487)
    ..lineTo(size.width * 0.02412750, size.height * 0.5942114)
    ..cubicTo(
        size.width * 0.02581000,
        size.height * 0.5911075,
        size.width * 0.02613000,
        size.height * 0.5903725,
        size.width * 0.02581000,
        size.height * 0.5903725)
    ..cubicTo(
        size.width * 0.02574000,
        size.height * 0.5903725,
        size.width * 0.02492750,
        size.height * 0.5919865,
        size.width * 0.02400250,
        size.height * 0.5939581)
    ..moveTo(size.width * 0.4845925, size.height * 0.6002602)
    ..cubicTo(
        size.width * 0.4851225,
        size.height * 0.6004589,
        size.width * 0.4858875,
        size.height * 0.6004539,
        size.width * 0.4862925,
        size.height * 0.6002404)
    ..cubicTo(
        size.width * 0.4867000,
        size.height * 0.6000318,
        size.width * 0.4862675,
        size.height * 0.5998629,
        size.width * 0.4853325,
        size.height * 0.5998729)
    ..cubicTo(
        size.width * 0.4843975,
        size.height * 0.5998828,
        size.width * 0.4840625,
        size.height * 0.6000566,
        size.width * 0.4845925,
        size.height * 0.6002602)
    ..moveTo(size.width * 0.4964925, size.height * 0.6002553)
    ..cubicTo(
        size.width * 0.4970175,
        size.height * 0.6004589,
        size.width * 0.4978800,
        size.height * 0.6004589,
        size.width * 0.4984050,
        size.height * 0.6002553)
    ..cubicTo(
        size.width * 0.4989325,
        size.height * 0.6000566,
        size.width * 0.4985025,
        size.height * 0.5998927,
        size.width * 0.4974500,
        size.height * 0.5998927)
    ..cubicTo(
        size.width * 0.4963975,
        size.height * 0.5998927,
        size.width * 0.4959650,
        size.height * 0.6000566,
        size.width * 0.4964925,
        size.height * 0.6002553)
    ..moveTo(size.width * 0.01039750, size.height * 0.6218334)
    ..lineTo(size.width * 0.008715000, size.height * 0.6254190)
    ..lineTo(size.width * 0.01052250, size.height * 0.6220817)
    ..cubicTo(
        size.width * 0.01151750,
        size.height * 0.6202442,
        size.width * 0.01233000,
        size.height * 0.6186302,
        size.width * 0.01233000,
        size.height * 0.6184912)
    ..cubicTo(
        size.width * 0.01233000,
        size.height * 0.6178555,
        size.width * 0.01196000,
        size.height * 0.6184912,
        size.width * 0.01039750,
        size.height * 0.6218334)
    ..moveTo(size.width * 0.8691625, size.height * 0.6714508)
    ..cubicTo(
        size.width * 0.8691625,
        size.height * 0.6735415,
        size.width * 0.8692475,
        size.height * 0.6743957,
        size.width * 0.8693475,
        size.height * 0.6733528)
    ..arcToPoint(Offset(size.width * 0.8693475, size.height * 0.6695487),
        radius: Radius.elliptical(
            size.width * 0.006000000, size.height * 0.01191889),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8692475,
        size.height * 0.6685058,
        size.width * 0.8691625,
        size.height * 0.6693600,
        size.width * 0.8691625,
        size.height * 0.6714508)
    ..moveTo(size.width * 0.6586950, size.height * 0.6777877)
    ..cubicTo(
        size.width * 0.6587000,
        size.height * 0.6796450,
        size.width * 0.6587875,
        size.height * 0.6803055,
        size.width * 0.6588900,
        size.height * 0.6792527)
    ..cubicTo(
        size.width * 0.6589900,
        size.height * 0.6782048,
        size.width * 0.6589875,
        size.height * 0.6766852,
        size.width * 0.6588800,
        size.height * 0.6758757)
    ..cubicTo(
        size.width * 0.6587750,
        size.height * 0.6750662,
        size.width * 0.6586900,
        size.height * 0.6759253,
        size.width * 0.6586950,
        size.height * 0.6777877)
    ..moveTo(size.width * 0.4529125, size.height * 0.6779466)
    ..cubicTo(
        size.width * 0.4533225,
        size.height * 0.6781601,
        size.width * 0.4539925,
        size.height * 0.6781601,
        size.width * 0.4544000,
        size.height * 0.6779466)
    ..cubicTo(
        size.width * 0.4548100,
        size.height * 0.6777330,
        size.width * 0.4544750,
        size.height * 0.6775592,
        size.width * 0.4536575,
        size.height * 0.6775592)
    ..cubicTo(
        size.width * 0.4528375,
        size.height * 0.6775592,
        size.width * 0.4525025,
        size.height * 0.6777330,
        size.width * 0.4529125,
        size.height * 0.6779466)
    ..moveTo(size.width * 0.4365475, size.height * 0.6796500)
    ..cubicTo(
        size.width * 0.4370775,
        size.height * 0.6798536,
        size.width * 0.4378425,
        size.height * 0.6798437,
        size.width * 0.4382500,
        size.height * 0.6796351)
    ..cubicTo(
        size.width * 0.4386550,
        size.height * 0.6794215,
        size.width * 0.4382225,
        size.height * 0.6792577,
        size.width * 0.4372875,
        size.height * 0.6792676)
    ..cubicTo(
        size.width * 0.4363525,
        size.height * 0.6792726,
        size.width * 0.4360200,
        size.height * 0.6794464,
        size.width * 0.4365475,
        size.height * 0.6796500)
    ..moveTo(size.width * 0.1704925, size.height * 0.7767939)
    ..arcToPoint(Offset(size.width * 0.1714425, size.height * 0.7782093),
        radius: Radius.elliptical(
            size.width * 0.002300000, size.height * 0.004568909),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..cubicTo(
        size.width * 0.1716350,
        size.height * 0.7787804,
        size.width * 0.1717575,
        size.height * 0.7785718,
        size.width * 0.1717625,
        size.height * 0.7776580)
    ..cubicTo(
        size.width * 0.1717675,
        size.height * 0.7766648,
        size.width * 0.1714550,
        size.height * 0.7762029,
        size.width * 0.1708125,
        size.height * 0.7762476)
    ..cubicTo(
        size.width * 0.1701725,
        size.height * 0.7762923,
        size.width * 0.1700675,
        size.height * 0.7764761,
        size.width * 0.1704925,
        size.height * 0.7767939)
    ..moveTo(size.width * 0.1330775, size.height * 0.7776779)
    ..cubicTo(
        size.width * 0.1330775,
        size.height * 0.7781944,
        size.width * 0.1323750,
        size.height * 0.7787307,
        size.width * 0.1315175,
        size.height * 0.7788748)
    ..cubicTo(
        size.width * 0.1306575,
        size.height * 0.7790188,
        size.width * 0.1299075,
        size.height * 0.7796197,
        size.width * 0.1298475,
        size.height * 0.7802156)
    ..cubicTo(
        size.width * 0.1297900,
        size.height * 0.7808066,
        size.width * 0.1298375,
        size.height * 0.7809010,
        size.width * 0.1299575,
        size.height * 0.7804242)
    ..cubicTo(
        size.width * 0.1300750,
        size.height * 0.7799475,
        size.width * 0.1308150,
        size.height * 0.7795601,
        size.width * 0.1316000,
        size.height * 0.7795601)
    ..cubicTo(
        size.width * 0.1329600,
        size.height * 0.7795601,
        size.width * 0.1338800,
        size.height * 0.7783334,
        size.width * 0.1333325,
        size.height * 0.7772508)
    ..cubicTo(
        size.width * 0.1331925,
        size.height * 0.7769727,
        size.width * 0.1330775,
        size.height * 0.7771664,
        size.width * 0.1330775,
        size.height * 0.7776779)
    ..moveTo(size.width * 0.9563150, size.height * 0.7995689)
    ..cubicTo(
        size.width * 0.9567225,
        size.height * 0.7997775,
        size.width * 0.9573925,
        size.height * 0.7997775,
        size.width * 0.9578025,
        size.height * 0.7995689)
    ..cubicTo(
        size.width * 0.9582100,
        size.height * 0.7993554,
        size.width * 0.9578775,
        size.height * 0.7991816,
        size.width * 0.9570575,
        size.height * 0.7991816)
    ..cubicTo(
        size.width * 0.9562400,
        size.height * 0.7991816,
        size.width * 0.9559050,
        size.height * 0.7993554,
        size.width * 0.9563150,
        size.height * 0.7995689)
    ..moveTo(size.width * 0.9465350, size.height * 0.8004132)
    ..cubicTo(
        size.width * 0.9469450,
        size.height * 0.8006218,
        size.width * 0.9476125,
        size.height * 0.8006218,
        size.width * 0.9480225,
        size.height * 0.8004132)
    ..cubicTo(
        size.width * 0.9484325,
        size.height * 0.8001996,
        size.width * 0.9480975,
        size.height * 0.8000258,
        size.width * 0.9472800,
        size.height * 0.8000258)
    ..cubicTo(
        size.width * 0.9464600,
        size.height * 0.8000258,
        size.width * 0.9461250,
        size.height * 0.8001996,
        size.width * 0.9465350,
        size.height * 0.8004132)
    ..moveTo(size.width * 0.9558875, size.height * 0.8046345)
    ..cubicTo(
        size.width * 0.9562975,
        size.height * 0.8048480,
        size.width * 0.9569675,
        size.height * 0.8048480,
        size.width * 0.9573775,
        size.height * 0.8046345)
    ..cubicTo(
        size.width * 0.9577850,
        size.height * 0.8044209,
        size.width * 0.9574500,
        size.height * 0.8042471,
        size.width * 0.9566325,
        size.height * 0.8042471)
    ..cubicTo(
        size.width * 0.9558150,
        size.height * 0.8042471,
        size.width * 0.9554800,
        size.height * 0.8044209,
        size.width * 0.9558875,
        size.height * 0.8046345)
    ..moveTo(size.width * 0.8809600, size.height * 0.8059505)
    ..cubicTo(
        size.width * 0.8809550,
        size.height * 0.8070282,
        size.width * 0.8812700,
        size.height * 0.8074056,
        size.width * 0.8821225,
        size.height * 0.8073311)
    ..cubicTo(
        size.width * 0.8830175,
        size.height * 0.8072566,
        size.width * 0.8830925,
        size.height * 0.8071374,
        size.width * 0.8824400,
        size.height * 0.8068146)
    ..cubicTo(
        size.width * 0.8819725,
        size.height * 0.8065812,
        size.width * 0.8814500,
        size.height * 0.8059604,
        size.width * 0.8812775,
        size.height * 0.8054340)
    ..cubicTo(
        size.width * 0.8810800,
        size.height * 0.8048232,
        size.width * 0.8809625,
        size.height * 0.8050169,
        size.width * 0.8809600,
        size.height * 0.8059505)
    ..moveTo(size.width * 0.4745950, size.height * 0.8063230)
    ..cubicTo(
        size.width * 0.4750050,
        size.height * 0.8065365,
        size.width * 0.4756750,
        size.height * 0.8065365,
        size.width * 0.4760850,
        size.height * 0.8063230)
    ..cubicTo(
        size.width * 0.4764925,
        size.height * 0.8061094,
        size.width * 0.4761575,
        size.height * 0.8059356,
        size.width * 0.4753400,
        size.height * 0.8059356)
    ..cubicTo(
        size.width * 0.4745225,
        size.height * 0.8059356,
        size.width * 0.4741875,
        size.height * 0.8061094,
        size.width * 0.4745950,
        size.height * 0.8063230)
    ..moveTo(size.width * 0.4856575, size.height * 0.8071921)
    ..cubicTo(
        size.width * 0.4863025,
        size.height * 0.8073857,
        size.width * 0.4872600,
        size.height * 0.8073808,
        size.width * 0.4877825,
        size.height * 0.8071821)
    ..cubicTo(
        size.width * 0.4883050,
        size.height * 0.8069785,
        size.width * 0.4877775,
        size.height * 0.8068196,
        size.width * 0.4866075,
        size.height * 0.8068295)
    ..cubicTo(
        size.width * 0.4854375,
        size.height * 0.8068345,
        size.width * 0.4850100,
        size.height * 0.8069984,
        size.width * 0.4856575,
        size.height * 0.8071921)
    ..moveTo(size.width * 0.5160500, size.height * 0.8071821)
    ..cubicTo(
        size.width * 0.5165775,
        size.height * 0.8073857,
        size.width * 0.5174375,
        size.height * 0.8073857,
        size.width * 0.5179625,
        size.height * 0.8071821)
    ..cubicTo(
        size.width * 0.5184900,
        size.height * 0.8069785,
        size.width * 0.5180600,
        size.height * 0.8068146,
        size.width * 0.5170075,
        size.height * 0.8068146)
    ..cubicTo(
        size.width * 0.5159550,
        size.height * 0.8068146,
        size.width * 0.5155250,
        size.height * 0.8069785,
        size.width * 0.5160500,
        size.height * 0.8071821)
    ..moveTo(size.width * 0.7456475, size.height * 0.8105641)
    ..cubicTo(
        size.width * 0.7461750,
        size.height * 0.8107628,
        size.width * 0.7469400,
        size.height * 0.8107578,
        size.width * 0.7473475,
        size.height * 0.8105442)
    ..cubicTo(
        size.width * 0.7477550,
        size.height * 0.8103357,
        size.width * 0.7473225,
        size.height * 0.8101668,
        size.width * 0.7463850,
        size.height * 0.8101767)
    ..cubicTo(
        size.width * 0.7454500,
        size.height * 0.8101867,
        size.width * 0.7451175,
        size.height * 0.8103605,
        size.width * 0.7456475,
        size.height * 0.8105641)
    ..moveTo(size.width * 0.7442900, size.height * 0.8157985)
    ..cubicTo(
        size.width * 0.7445525,
        size.height * 0.8163150,
        size.width * 0.7475600,
        size.height * 0.8159177,
        size.width * 0.7478625,
        size.height * 0.8153267)
    ..cubicTo(
        size.width * 0.7479750,
        size.height * 0.8151132,
        size.width * 0.7471650,
        size.height * 0.8150287,
        size.width * 0.7460650,
        size.height * 0.8151380)
    ..cubicTo(
        size.width * 0.7449650,
        size.height * 0.8152522,
        size.width * 0.7441650,
        size.height * 0.8155502,
        size.width * 0.7442900,
        size.height * 0.8157985)
    ..moveTo(size.width * 0.7673525, size.height * 0.8185945)
    ..cubicTo(
        size.width * 0.7677100,
        size.height * 0.8192997,
        size.width * 0.7975725,
        size.height * 0.8196274,
        size.width * 0.7979275,
        size.height * 0.8189272)
    ..cubicTo(
        size.width * 0.7980575,
        size.height * 0.8186640,
        size.width * 0.7911650,
        size.height * 0.8183462,
        size.width * 0.7826075,
        size.height * 0.8182220)
    ..cubicTo(
        size.width * 0.7740500,
        size.height * 0.8180929,
        size.width * 0.7671850,
        size.height * 0.8182617,
        size.width * 0.7673525,
        size.height * 0.8185945)
    ..moveTo(size.width * 0.8147250, size.height * 0.8369893)
    ..cubicTo(
        size.width * 0.8147250,
        size.height * 0.8386182,
        size.width * 0.8148125,
        size.height * 0.8392837,
        size.width * 0.8149200,
        size.height * 0.8384692)
    ..arcToPoint(Offset(size.width * 0.8149200, size.height * 0.8355143),
        radius: Radius.elliptical(
            size.width * 0.003550000, size.height * 0.007052011),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8148125,
        size.height * 0.8346999,
        size.width * 0.8147250,
        size.height * 0.8353653,
        size.width * 0.8147250,
        size.height * 0.8369893)
    ..moveTo(size.width * 0.6995975, size.height * 0.8498816)
    ..cubicTo(
        size.width * 0.6994825,
        size.height * 0.8504725,
        size.width * 0.6994450,
        size.height * 0.8612095,
        size.width * 0.6995100,
        size.height * 0.8737442)
    ..lineTo(size.width * 0.6996325, size.height * 0.8965341)
    ..lineTo(size.width * 0.6997300, size.height * 0.8731582)
    ..lineTo(size.width * 0.6998300, size.height * 0.8497773)
    ..lineTo(size.width * 0.7007875, size.height * 0.8494098)
    ..lineTo(size.width * 0.7017425, size.height * 0.8490472)
    ..lineTo(size.width * 0.7007725, size.height * 0.8489330)
    ..cubicTo(
        size.width * 0.7002400,
        size.height * 0.8488685,
        size.width * 0.6997100,
        size.height * 0.8492955,
        size.width * 0.6995975,
        size.height * 0.8498816)
    ..moveTo(size.width * 0.7021375, size.height * 0.8733071)
    ..cubicTo(
        size.width * 0.7021375,
        size.height * 0.8837610,
        size.width * 0.7021975,
        size.height * 0.8880369,
        size.width * 0.7022725,
        size.height * 0.8828125)
    ..cubicTo(
        size.width * 0.7023475,
        size.height * 0.8775830,
        size.width * 0.7023475,
        size.height * 0.8690312,
        size.width * 0.7022725,
        size.height * 0.8638068)
    ..cubicTo(
        size.width * 0.7021975,
        size.height * 0.8585823,
        size.width * 0.7021375,
        size.height * 0.8628582,
        size.width * 0.7021375,
        size.height * 0.8733071)
    ..moveTo(size.width * 0.8143175, size.height * 0.8597941)
    ..cubicTo(
        size.width * 0.8143175,
        size.height * 0.8618849,
        size.width * 0.8144000,
        size.height * 0.8627391,
        size.width * 0.8145000,
        size.height * 0.8616962)
    ..arcToPoint(Offset(size.width * 0.8145000, size.height * 0.8578970),
        radius: Radius.elliptical(
            size.width * 0.005985000, size.height * 0.01188909),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8144000,
        size.height * 0.8568491,
        size.width * 0.8143175,
        size.height * 0.8577033,
        size.width * 0.8143175,
        size.height * 0.8597941)
    ..moveTo(size.width * 0.1706475, size.height * 0.8657089)
    ..cubicTo(
        size.width * 0.1706475,
        size.height * 0.8701188,
        size.width * 0.1707175,
        size.height * 0.8719265,
        size.width * 0.1708050,
        size.height * 0.8697215)
    ..cubicTo(
        size.width * 0.1708925,
        size.height * 0.8675116,
        size.width * 0.1708925,
        size.height * 0.8639012,
        size.width * 0.1708050,
        size.height * 0.8616962)
    ..cubicTo(
        size.width * 0.1707175,
        size.height * 0.8594912,
        size.width * 0.1706475,
        size.height * 0.8612939,
        size.width * 0.1706475,
        size.height * 0.8657089)
    ..moveTo(size.width * 0.1310900, size.height * 0.8832346)
    ..cubicTo(
        size.width * 0.1408700,
        size.height * 0.8833587,
        size.width * 0.1567500,
        size.height * 0.8833587,
        size.width * 0.1663800,
        size.height * 0.8832346)
    ..cubicTo(
        size.width * 0.1760075,
        size.height * 0.8831055,
        size.width * 0.1680050,
        size.height * 0.8830012,
        size.width * 0.1485975,
        size.height * 0.8830012)
    ..cubicTo(
        size.width * 0.1291875,
        size.height * 0.8830012,
        size.width * 0.1213100,
        size.height * 0.8831055,
        size.width * 0.1310900,
        size.height * 0.8832346)
    ..moveTo(size.width * 0.8139025, size.height * 0.8893579)
    ..cubicTo(
        size.width * 0.8139025,
        size.height * 0.8919105,
        size.width * 0.8139825,
        size.height * 0.8929584,
        size.width * 0.8140800,
        size.height * 0.8916771)
    ..arcToPoint(Offset(size.width * 0.8140800, size.height * 0.8870337),
        radius: Radius.elliptical(
            size.width * 0.009375000, size.height * 0.01862327),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8139825,
        size.height * 0.8857574,
        size.width * 0.8139025,
        size.height * 0.8868003,
        size.width * 0.8139025,
        size.height * 0.8893579)
    ..moveTo(size.width * 0.8113525, size.height * 0.8902022)
    ..cubicTo(
        size.width * 0.8113525,
        size.height * 0.8927548,
        size.width * 0.8114325,
        size.height * 0.8938027,
        size.width * 0.8115300,
        size.height * 0.8925214)
    ..arcToPoint(Offset(size.width * 0.8115300, size.height * 0.8878780),
        radius: Radius.elliptical(
            size.width * 0.009375000, size.height * 0.01862327),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8114325,
        size.height * 0.8866017,
        size.width * 0.8113525,
        size.height * 0.8876446,
        size.width * 0.8113525,
        size.height * 0.8902022)
    ..moveTo(size.width * 0.7487250, size.height * 0.9487289)
    ..lineTo(size.width * 0.7489375, size.height * 0.9894418)
    ..lineTo(size.width * 0.7490475, size.height * 0.9493745)
    ..cubicTo(
        size.width * 0.7491075,
        size.height * 0.9273345,
        size.width * 0.7490125,
        size.height * 0.9090142,
        size.width * 0.7488350,
        size.height * 0.9086616)
    ..cubicTo(
        size.width * 0.7486575,
        size.height * 0.9083090,
        size.width * 0.7486075,
        size.height * 0.9263412,
        size.width * 0.7487250,
        size.height * 0.9487289)
    ..moveTo(size.width * 0.7514700, size.height * 0.9098385)
    ..cubicTo(
        size.width * 0.7513575,
        size.height * 0.9104196,
        size.width * 0.7513175,
        size.height * 0.9310343,
        size.width * 0.7513825,
        size.height * 0.9556568)
    ..lineTo(size.width * 0.7514950, size.height * 1.000422)
    ..lineTo(size.width * 0.7515975, size.height * 0.9550956)
    ..cubicTo(
        size.width * 0.7516775,
        size.height * 0.9197759,
        size.width * 0.7518175,
        size.height * 0.9096747,
        size.width * 0.7522325,
        size.height * 0.9093419)
    ..cubicTo(
        size.width * 0.7526475,
        size.height * 0.9090092,
        size.width * 0.7526450,
        size.height * 0.9088999,
        size.width * 0.7522200,
        size.height * 0.9088503)
    ..arcToPoint(Offset(size.width * 0.7514700, size.height * 0.9098385),
        radius: Radius.elliptical(
            size.width * 0.0008100000, size.height * 0.001609050),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..moveTo(size.width * 0.8134725, size.height * 0.9210274)
    ..cubicTo(
        size.width * 0.8134750,
        size.height * 0.9233516,
        size.width * 0.8135575,
        size.height * 0.9242008,
        size.width * 0.8136550,
        size.height * 0.9229146)
    ..arcToPoint(Offset(size.width * 0.8136500, size.height * 0.9186933),
        radius: Radius.elliptical(
            size.width * 0.007632500, size.height * 0.01516182),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8135475,
        size.height * 0.9176554,
        size.width * 0.8134700,
        size.height * 0.9187032,
        size.width * 0.8134725,
        size.height * 0.9210274)
    ..moveTo(size.width * 0.8109200, size.height * 0.9218717)
    ..cubicTo(
        size.width * 0.8109250,
        size.height * 0.9241958,
        size.width * 0.8110075,
        size.height * 0.9250451,
        size.width * 0.8111050,
        size.height * 0.9237638)
    ..arcToPoint(Offset(size.width * 0.8110975, size.height * 0.9195375),
        radius: Radius.elliptical(
            size.width * 0.007550000, size.height * 0.01499794),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8109975,
        size.height * 0.9184996,
        size.width * 0.8109175,
        size.height * 0.9195524,
        size.width * 0.8109200,
        size.height * 0.9218717)
    ..moveTo(size.width * 0.8104900, size.height * 0.9501691)
    ..cubicTo(
        size.width * 0.8104900,
        size.height * 0.9522549,
        size.width * 0.8105725,
        size.height * 0.9531141,
        size.width * 0.8106750,
        size.height * 0.9520662)
    ..cubicTo(
        size.width * 0.8107750,
        size.height * 0.9510233,
        size.width * 0.8107750,
        size.height * 0.9493099,
        size.width * 0.8106750,
        size.height * 0.9482670)
    ..cubicTo(
        size.width * 0.8105725,
        size.height * 0.9472192,
        size.width * 0.8104900,
        size.height * 0.9480783,
        size.width * 0.8104900,
        size.height * 0.9501691)
    ..moveTo(size.width * 0.8130400, size.height * 0.9501691)
    ..cubicTo(
        size.width * 0.8130400,
        size.height * 0.9522549,
        size.width * 0.8131250,
        size.height * 0.9531141,
        size.width * 0.8132250,
        size.height * 0.9520662)
    ..cubicTo(
        size.width * 0.8133275,
        size.height * 0.9510233,
        size.width * 0.8133275,
        size.height * 0.9493099,
        size.width * 0.8132250,
        size.height * 0.9482670)
    ..cubicTo(
        size.width * 0.8131250,
        size.height * 0.9472192,
        size.width * 0.8130400,
        size.height * 0.9480783,
        size.width * 0.8130400,
        size.height * 0.9501691)
    ..moveTo(size.width * 0.8100550, size.height * 0.9733911)
    ..cubicTo(
        size.width * 0.8100600,
        size.height * 0.9752534,
        size.width * 0.8101475,
        size.height * 0.9759139,
        size.width * 0.8102500,
        size.height * 0.9748611)
    ..arcToPoint(Offset(size.width * 0.8102400, size.height * 0.9714841),
        radius: Radius.elliptical(
            size.width * 0.004600000, size.height * 0.009137817),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8101350,
        size.height * 0.9706746,
        size.width * 0.8100525,
        size.height * 0.9715337,
        size.width * 0.8100550,
        size.height * 0.9733911)
    ..moveTo(size.width * 0.8125975, size.height * 0.9729689)
    ..cubicTo(
        size.width * 0.8125975,
        size.height * 0.9745979,
        size.width * 0.8126850,
        size.height * 0.9752633,
        size.width * 0.8127925,
        size.height * 0.9744489)
    ..arcToPoint(Offset(size.width * 0.8127925, size.height * 0.9714940),
        radius: Radius.elliptical(
            size.width * 0.003470000, size.height * 0.006893093),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8126850,
        size.height * 0.9706795,
        size.width * 0.8125975,
        size.height * 0.9713450,
        size.width * 0.8125975,
        size.height * 0.9729689);
}

import 'package:flutter/material.dart';
import 'package:li/data/models/tag_minute.dart';
import 'package:time_app_components/utils/testStyles.dart';

import 'additional_tile.dart';

class AdditionalContainer extends StatelessWidget {
  final List<TagMinute> tags;
  const AdditionalContainer({
    required this.tags,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _media = MediaQuery.of(context);
    final double? width;
    if ((_media.size.width - 40 - 10) / 2 < 162) {
      width = (_media.size.width - 40 - 10) / 2;
    } else {
      width = null;
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Дополнительно',
          style: const TextStyle(
            fontSize: H2TextStyle.fontSize,
            fontWeight: H2TextStyle.fontWeight,
            height: H2TextStyle.height,
          ),
        ),
        const SizedBox(height: 20),
        SizedBox(
          width: double.infinity,
          child: Wrap(
            spacing: 10.0,
            runSpacing: 10.0,
            alignment: WrapAlignment.start,
            crossAxisAlignment: WrapCrossAlignment.start,
            runAlignment: WrapAlignment.start,
            children: tags
                .map(
                  (_v) => AdditionalTile(
                    title: _v.name,
                    svgPath: _v.iconSVGPath,
                    width: width,
                  ),
                )
                .toList(),
          ),
        ),
      ],
    );
  }
}

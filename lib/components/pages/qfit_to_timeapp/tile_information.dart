import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:time_app_components/utils/testStyles.dart';

class TileInformation extends StatelessWidget {
  final bool? isStar;
  final String? title, svgPath;

  const TileInformation({
    this.isStar,
    this.title,
    this.svgPath,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        height: 30,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          color: Colors.white,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: title == null
              ? []
              : [
                  if (isStar == true)
                    Icon(
                      Icons.star_rate_rounded,
                      size: 20,
                      color: Color(0xff00DF31),
                    ),
                  if (svgPath != null) SvgPicture.asset(svgPath!),
                  if (title != null)
                    Text(
                      title!,
                      style: TextStyle(
                        color: (isStar ?? false)
                            ? Color(0xff00DF31)
                            : Color(0xff434343),
                        fontSize: P4TextStyle.fontSize,
                        fontWeight: P4TextStyle.fontWeight,
                        height: P4TextStyle.height,
                      ),
                    )
                ],
        ),
      ),
    );
  }
}

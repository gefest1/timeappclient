import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/data/models/allowed_institution_types.dart';
import 'package:li/data/models/institution_minute.dart';
import 'package:li/logic/providers/get_club_data/api/getInstituion.dart';
import 'package:li/network/graphql_provider.dart';

class ClubDataProvider extends ChangeNotifier {
  final GraphqlMinuteProvider graphqlMinuteProvider;

  //
  final Map<AllowedInstitutionTypes, int> page = {};
  final Map<AllowedInstitutionTypes, List<InstitutionsMinute>>
      institutionsTypeMap = {};
  final Map<AllowedInstitutionTypes, Map<String, InstitutionsMinute>>
      institutionsTypeMapFastCheck = {};

  //

  ClubDataProvider({required this.graphqlMinuteProvider});

  final List<InstitutionsMinute> institutions = [];
  final Map<String, InstitutionsMinute> institutionsMap = {};

  Future<List<InstitutionsMinute>> getInstitution(
    AllowedInstitutionTypes type, {
    double? longitude,
    double? latitude,
  }) async {
    if (page[type] == -1) return institutionsTypeMap[type]!;
    try {
      page[type] = page[type] ?? 1;

      final _mutationResult = await graphqlMinuteProvider.client.query(
        QueryOptions(
          document: getFilteredInstitutions,
          variables: {
            'city': 'Almaty',
            'page': page[type],
            'latitude': latitude,
            'longtitude': longitude,
            'type': type.toString(),
          },
        ),
      );

      if (_mutationResult.hasException) throw _mutationResult.exception!;

      final List<InstitutionsMinute> _data = (_mutationResult
                  .data!['listFilteredDistanceInstitution'] as List<dynamic>?)
              ?.map((e) => InstitutionsMinute.fromMap(e))
              .toList() ??
          [];
      if (_data.length < 10) {
        page[type] = -1;
      } else {
        if (longitude != null && latitude != null) {
        } else {
          page[type] = page[type]! + 1;
        }
      }
      institutionsTypeMap[type] = institutionsTypeMap[type] ?? [];

      institutionsTypeMapFastCheck[type] =
          institutionsTypeMapFastCheck[type] ?? {};

      _data.forEach((element) {
        if (institutionsTypeMapFastCheck[type]![element.id!] == null) {
          institutionsTypeMap[type]!.add(element);
        }
        institutionsTypeMapFastCheck[type]![element.id!] = element;
      });
      final returnList = institutionsTypeMap[type]!;
      return returnList;
    } catch (e) {
      log(e.toString());
      return institutions;
    }
  }
}

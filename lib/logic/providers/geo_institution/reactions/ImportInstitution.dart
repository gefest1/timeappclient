part of '../GeoInstitutionProvider.dart';

extension ImportData on GeoInstitutionProvider {
  void importInst(List<InstitutionsMinute> institutions) {
    for (final _newIns in institutions) {
      institutionsMinuteMap[_newIns.id!] = _newIns;
    }
    institutionsMinuteList = institutionsMinuteMap.values.toList();
    // ignore: invalid_use_of_protected_member, invalid_use_of_visible_for_testing_member
    notifyListeners();
  }
}

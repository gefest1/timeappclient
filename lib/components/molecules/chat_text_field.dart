import 'package:flutter/material.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class ChatTextField extends StatefulWidget {
  final FocusNode? focusNode;
  final TextEditingController? controller;
  final String? hintText;

  const ChatTextField({
    this.focusNode,
    this.controller,
    this.hintText,
    Key? key,
  }) : super(key: key);

  @override
  _ChatTextFieldState createState() => _ChatTextFieldState();
}

class _ChatTextFieldState extends State<ChatTextField> {
  bool isChoosen = false;
  late final FocusNode focusNode = (widget.focusNode ?? FocusNode())
    ..addListener(_check);
  late final TextEditingController controller =
      widget.controller ?? TextEditingController();

  _check() {
    if (focusNode.hasFocus && !isChoosen) {
      setState(() {
        isChoosen = true;
      });
    } else if (!focusNode.hasFocus && isChoosen && controller.text.isEmpty) {
      setState(() {
        isChoosen = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        color:
            isChoosen ? ColorData.specsInnerSucces : ColorData.allInnerDefault,
        borderRadius: BorderRadius.circular(15),
        border: Border.all(
          width: 0.5,
          color: isChoosen
              ? ColorData.specsStrokeActive
              : ColorData.specsInnerSucces,
        ),
      ),
      child: TextField(
        controller: controller,
        focusNode: focusNode,
        decoration: InputDecoration(
          isDense: true,
          contentPadding: EdgeInsets.only(
            left: 12,
            right: 12,
            top: 9.5,
            bottom: 11.5,
          ),
          border: InputBorder.none,
          hintText: widget.hintText,
          hintStyle: TextStyle(
            fontSize: P4TextStyle.fontSize,
            fontWeight: P4TextStyle.fontWeight,
            height: P4TextStyle.height,
            color: Color(0xff979797),
          ),
        ),
      ),
    );
  }
}

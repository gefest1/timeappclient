import 'package:flutter/material.dart';
import 'package:li/components/atoms/leading_icon.dart';
import 'package:li/components/molecules/divider_text_tab.dart';
import 'package:li/components/molecules/payment_card.dart';
import 'package:li/components/pages/add_new_card_payment.dart';
import 'package:li/data/models/credit_card_model.dart';
import 'package:li/logic/providers/CreditCard.dart';
import 'package:li/main.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/text_styles.dart';
import 'package:provider/provider.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/utils/testStyles.dart';

class PaymentWayPage extends StatefulWidget {
  final ValueNotifier<CreditCardModel?>? creditCard;

  const PaymentWayPage({
    this.creditCard,
    Key? key,
  }) : super(key: key);

  @override
  State<PaymentWayPage> createState() => _PaymentWayPageState();
}

class _PaymentWayPageState extends State<PaymentWayPage> {
  void listenCreditCard() {
    setState(() {});
  }

  @override
  void initState() {
    widget.creditCard?.addListener(listenCreditCard);
    super.initState();
  }

  @override
  void dispose() {
    widget.creditCard?.removeListener(listenCreditCard);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: CustomScrollView(
              clipBehavior: Clip.none,
              slivers: [
                SliverToBoxAdapter(
                  child: SizedBox(
                    height: MediaQuery.of(context).padding.top + 56,
                  ),
                ),
                SliverToBoxAdapter(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 40),
                      // Text(
                      //   'Способ оплаты',
                      //   style: TextStyle(
                      //     fontSize: H2TextStyle.fontSize,
                      //     fontWeight: H2TextStyle.fontWeight,
                      //     height: H2TextStyle.height,
                      //     color: ColorData.allMainBlack,
                      //   ),
                      // ),
                      // const SizedBox(height: 10),
                      Consumer<CreditCardProvider>(
                        builder: (ctx, creditCardProvider, _) {
                          return Column(
                            children: List.generate(
                              creditCardProvider.creditCards.length * 2,
                              (index) {
                                if (index % 2 == 1)
                                  return const SizedBox(height: 6);
                                final card =
                                    creditCardProvider.creditCards[index ~/ 2];
                                return PaymentCard(
                                  onTap: () {
                                    widget.creditCard?.value = card;
                                    setState(() {});
                                    Navigator.pop(context);
                                  },
                                  title: card.cardType,
                                  label: card.last4Digits == null
                                      ? null
                                      : '****' + card.last4Digits!,
                                  choosen:
                                      card.id == widget.creditCard?.value?.id,
                                  deleteTap: () {
                                    if (widget.creditCard?.value?.id ==
                                        card.id) {
                                      widget.creditCard?.value = null;
                                    }

                                    creditCardProvider.removeCard(
                                      cardId: card.id!,
                                    );
                                  },
                                );
                              },
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
                SliverToBoxAdapter(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 40),
                      Text(
                        'Добавить новый',
                        style: TextStyle(
                          fontSize: H2TextStyle.fontSize,
                          fontWeight: H2TextStyle.fontWeight,
                          height: H2TextStyle.height,
                          color: ColorData.allMainBlack,
                        ),
                      ),
                      const SizedBox(height: 10),
                      DividerTapTile(
                        assetSvg: 'assets/svg/icon/Card.svg',
                        onTap: () {
                          routerDelegate.push(const AddNewCardPayment());
                        },
                        title: 'Банковские карты',
                        subTitle: 'Visa, MasterCard, Maestro',
                      ),
                      Divider(
                        height: 0,
                        thickness: 0.5,
                        color: ColorData.allMainGray,
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: CustomAppBar(
              leading: const LeadingIcon(),
              title: Text(
                'Способ оплаты',
                style: TextStyle(
                  color: ColorData.allMainBlack,
                  fontWeight: FontWeight.w500,
                  fontSize: 18,
                  height: 21 / 18,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:li/const.dart';
import 'package:flutter/material.dart';

class SexChoose extends StatefulWidget {
  final void Function(int) onTapSex;

  const SexChoose({
    required this.onTapSex,
    Key? key,
  }) : super(key: key);

  @override
  _SexChooseState createState() => _SexChooseState();
}

final _choosen = BoxDecoration(
  color: innerSucess,
  borderRadius: BorderRadius.all(
    Radius.circular(5),
  ),
  border: Border.all(
    color: strokeSucess,
    width: 0.5,
  ),
);

final _notChoosen = BoxDecoration(
  color: Color(0xfffafafa),
  borderRadius: BorderRadius.all(
    Radius.circular(5),
  ),
  border: Border.all(
    color: Color(0xffC9C9C9),
    width: 0.5,
  ),
);

class _SexChooseState extends State<SexChoose> {
  int? choosenIndex;
  List<String> _list = [
    'М',
    'Ж',
  ];

  @override
  Widget build(BuildContext context) {
    final _widgets = List.generate(
      _list.length,
      (_) => Expanded(
        child: Padding(
          padding: EdgeInsets.only(right: _ == 0 ? 10 : 0),
          child: MaterialButton(
            padding: EdgeInsets.zero,
            onPressed: () {
              setState(() {
                choosenIndex = _;
              });
              widget.onTapSex(_);
            },
            child: Container(
              height: 75,
              decoration: choosenIndex == _ ? _choosen : _notChoosen,
              child: Center(
                child: Text(
                  _list[_],
                  style: TextStyle(
                    fontSize: 24,
                    color: choosenIndex == _ ? mainBlack : Color(0xffAAAAAA),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
    return Row(
      children: [
        _widgets[0],
        const SizedBox(width: 10),
        _widgets[1],
      ],
    );
  }
}

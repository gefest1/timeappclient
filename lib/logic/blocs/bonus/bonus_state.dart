part of 'bonus_bloc.dart';

abstract class BonusState extends Equatable {
  const BonusState();
  
  @override
  List<Object> get props => [];
}

class BonusInitial extends BonusState {}

// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';

class NailsCustomPainter extends CustomPainter {
  const NailsCustomPainter({Listenable? repaint}) : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.5000000, 0);
    path_0.cubicTo(
        size.width * 0.4558143,
        size.height * 0.00004962714,
        size.width * 0.4134500,
        size.height * 0.01762504,
        size.width * 0.3822036,
        size.height * 0.04887036);
    path_0.cubicTo(
        size.width * 0.3509600,
        size.height * 0.08011571,
        size.width * 0.3333843,
        size.height * 0.1224793,
        size.width * 0.3333346,
        size.height * 0.1666668);
    path_0.lineTo(size.width * 0.3333346, size.height * 0.4583321);
    path_0.cubicTo(
        size.width * 0.3333721,
        size.height * 0.4914750,
        size.width * 0.3465536,
        size.height * 0.5232464,
        size.width * 0.3699893,
        size.height * 0.5466821);
    path_0.cubicTo(
        size.width * 0.3934214,
        size.height * 0.5701143,
        size.width * 0.4251929,
        size.height * 0.5832964,
        size.width * 0.4583357,
        size.height * 0.5833321);
    path_0.lineTo(size.width * 0.5416679, size.height * 0.5833321);
    path_0.cubicTo(
        size.width * 0.5748071,
        size.height * 0.5832964,
        size.width * 0.6065821,
        size.height * 0.5701143,
        size.width * 0.6300143,
        size.height * 0.5466821);
    path_0.cubicTo(
        size.width * 0.6534500,
        size.height * 0.5232464,
        size.width * 0.6666321,
        size.height * 0.4914750,
        size.width * 0.6666679,
        size.height * 0.4583321);
    path_0.lineTo(size.width * 0.6666679, size.height * 0.1666668);
    path_0.cubicTo(
        size.width * 0.6666179,
        size.height * 0.1224793,
        size.width * 0.6490429,
        size.height * 0.08011571,
        size.width * 0.6177964,
        size.height * 0.04887036);
    path_0.cubicTo(
        size.width * 0.5865536,
        size.height * 0.01762504,
        size.width * 0.5441893,
        size.height * 0.00004962714,
        size.width * 0.5000000,
        0);
    path_0.lineTo(size.width * 0.5000000, 0);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Colors.white;
    canvas.drawPath(path_0, paint_0_fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.7083357, size.height * 0.2128471);
    path_1.lineTo(size.width * 0.7083357, size.height * 0.4583321);
    path_1.cubicTo(
        size.width * 0.7082857,
        size.height * 0.5025179,
        size.width * 0.6907107,
        size.height * 0.5448821,
        size.width * 0.6594643,
        size.height * 0.5761286);
    path_1.cubicTo(
        size.width * 0.6282179,
        size.height * 0.6073714,
        size.width * 0.5858571,
        size.height * 0.6249500,
        size.width * 0.5416679,
        size.height * 0.6249964);
    path_1.lineTo(size.width * 0.4583357, size.height * 0.6249964);
    path_1.cubicTo(
        size.width * 0.4141464,
        size.height * 0.6249500,
        size.width * 0.3717821,
        size.height * 0.6073714,
        size.width * 0.3405386,
        size.height * 0.5761286);
    path_1.cubicTo(
        size.width * 0.3092932,
        size.height * 0.5448821,
        size.width * 0.2917179,
        size.height * 0.5025179,
        size.width * 0.2916682,
        size.height * 0.4583321);
    path_1.lineTo(size.width * 0.2916682, size.height * 0.2141961);
    path_1.cubicTo(
        size.width * 0.2381661,
        size.height * 0.2697807,
        size.width * 0.2082954,
        size.height * 0.3439418,
        size.width * 0.2083350,
        size.height * 0.4210929);
    path_1.lineTo(size.width * 0.2083350, size.height * 0.9999964);
    path_1.lineTo(size.width * 0.7916679, size.height * 0.9999964);
    path_1.lineTo(size.width * 0.7916679, size.height * 0.4166643);
    path_1.cubicTo(
        size.width * 0.7917000,
        size.height * 0.3404250,
        size.width * 0.7617714,
        size.height * 0.2672261,
        size.width * 0.7083357,
        size.height * 0.2128471);
    path_1.close();
    path_1.moveTo(size.width * 0.6250000, size.height * 0.9166643);
    path_1.lineTo(size.width * 0.3750000, size.height * 0.9166643);
    path_1.lineTo(size.width * 0.3750000, size.height * 0.8749964);
    path_1.lineTo(size.width * 0.6250000, size.height * 0.8749964);
    path_1.lineTo(size.width * 0.6250000, size.height * 0.9166643);
    path_1.close();
    path_1.moveTo(size.width * 0.6250000, size.height * 0.8333321);
    path_1.lineTo(size.width * 0.3750000, size.height * 0.8333321);
    path_1.lineTo(size.width * 0.3750000, size.height * 0.7916643);
    path_1.lineTo(size.width * 0.6250000, size.height * 0.7916643);
    path_1.lineTo(size.width * 0.6250000, size.height * 0.8333321);
    path_1.close();

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = Colors.white;
    canvas.drawPath(path_1, paint_1_fill);

    Path path_2 = Path();
    path_2.moveTo(size.width * 0.1875000, size.height * 0.2500029);
    path_2.cubicTo(
        size.width * 0.1709296,
        size.height * 0.2499850,
        size.width * 0.1550429,
        size.height * 0.2433943,
        size.width * 0.1433257,
        size.height * 0.2316771);
    path_2.cubicTo(
        size.width * 0.1316086,
        size.height * 0.2199600,
        size.width * 0.1250179,
        size.height * 0.2040736,
        size.width * 0.1250000,
        size.height * 0.1875029);
    path_2.cubicTo(
        size.width * 0.1250000,
        size.height * 0.1819775,
        size.width * 0.1228050,
        size.height * 0.1766786,
        size.width * 0.1188982,
        size.height * 0.1727714);
    path_2.cubicTo(
        size.width * 0.1149911,
        size.height * 0.1688646,
        size.width * 0.1096921,
        size.height * 0.1666696,
        size.width * 0.1041668,
        size.height * 0.1666696);
    path_2.cubicTo(
        size.width * 0.09864143,
        size.height * 0.1666696,
        size.width * 0.09334214,
        size.height * 0.1688646,
        size.width * 0.08943536,
        size.height * 0.1727714);
    path_2.cubicTo(
        size.width * 0.08552821,
        size.height * 0.1766786,
        size.width * 0.08333321,
        size.height * 0.1819775,
        size.width * 0.08333321,
        size.height * 0.1875029);
    path_2.cubicTo(
        size.width * 0.08331536,
        size.height * 0.2040736,
        size.width * 0.07672500,
        size.height * 0.2199600,
        size.width * 0.06500786,
        size.height * 0.2316771);
    path_2.cubicTo(
        size.width * 0.05329071,
        size.height * 0.2433943,
        size.width * 0.03740393,
        size.height * 0.2499850,
        size.width * 0.02083332,
        size.height * 0.2500029);
    path_2.cubicTo(
        size.width * 0.01530800,
        size.height * 0.2500029,
        size.width * 0.01000896,
        size.height * 0.2521979,
        size.width * 0.006101929,
        size.height * 0.2561050);
    path_2.cubicTo(size.width * 0.002194936, size.height * 0.2600118, 0,
        size.height * 0.2653107, 0, size.height * 0.2708361);
    path_2.cubicTo(
        0,
        size.height * 0.2763614,
        size.width * 0.002194936,
        size.height * 0.2816607,
        size.width * 0.006101929,
        size.height * 0.2855675);
    path_2.cubicTo(
        size.width * 0.01000896,
        size.height * 0.2894746,
        size.width * 0.01530800,
        size.height * 0.2916696,
        size.width * 0.02083332,
        size.height * 0.2916696);
    path_2.cubicTo(
        size.width * 0.03740393,
        size.height * 0.2916875,
        size.width * 0.05329071,
        size.height * 0.2982782,
        size.width * 0.06500786,
        size.height * 0.3099954);
    path_2.cubicTo(
        size.width * 0.07672500,
        size.height * 0.3217125,
        size.width * 0.08331536,
        size.height * 0.3375989,
        size.width * 0.08333321,
        size.height * 0.3541696);
    path_2.cubicTo(
        size.width * 0.08333321,
        size.height * 0.3596964,
        size.width * 0.08552821,
        size.height * 0.3649929,
        size.width * 0.08943536,
        size.height * 0.3689000);
    path_2.cubicTo(
        size.width * 0.09334214,
        size.height * 0.3728071,
        size.width * 0.09864143,
        size.height * 0.3750036,
        size.width * 0.1041668,
        size.height * 0.3750036);
    path_2.cubicTo(
        size.width * 0.1096921,
        size.height * 0.3750036,
        size.width * 0.1149911,
        size.height * 0.3728071,
        size.width * 0.1188982,
        size.height * 0.3689000);
    path_2.cubicTo(
        size.width * 0.1228050,
        size.height * 0.3649929,
        size.width * 0.1250000,
        size.height * 0.3596964,
        size.width * 0.1250000,
        size.height * 0.3541696);
    path_2.cubicTo(
        size.width * 0.1250179,
        size.height * 0.3375989,
        size.width * 0.1316086,
        size.height * 0.3217125,
        size.width * 0.1433257,
        size.height * 0.3099954);
    path_2.cubicTo(
        size.width * 0.1550429,
        size.height * 0.2982782,
        size.width * 0.1709296,
        size.height * 0.2916875,
        size.width * 0.1875000,
        size.height * 0.2916696);
    path_2.cubicTo(
        size.width * 0.1930254,
        size.height * 0.2916696,
        size.width * 0.1983243,
        size.height * 0.2894746,
        size.width * 0.2022314,
        size.height * 0.2855675);
    path_2.cubicTo(
        size.width * 0.2061382,
        size.height * 0.2816607,
        size.width * 0.2083332,
        size.height * 0.2763614,
        size.width * 0.2083332,
        size.height * 0.2708361);
    path_2.cubicTo(
        size.width * 0.2083332,
        size.height * 0.2653107,
        size.width * 0.2061382,
        size.height * 0.2600118,
        size.width * 0.2022314,
        size.height * 0.2561050);
    path_2.cubicTo(
        size.width * 0.1983243,
        size.height * 0.2521979,
        size.width * 0.1930254,
        size.height * 0.2500029,
        size.width * 0.1875000,
        size.height * 0.2500029);
    path_2.close();

    Paint paint_2_fill = Paint()..style = PaintingStyle.fill;
    paint_2_fill.color = Colors.white;
    canvas.drawPath(path_2, paint_2_fill);

    Path path_3 = Path();
    path_3.moveTo(size.width * 0.9531250, size.height * 0.08333321);
    path_3.cubicTo(
        size.width * 0.9365536,
        size.height * 0.08331536,
        size.width * 0.9206679,
        size.height * 0.07672500,
        size.width * 0.9089500,
        size.height * 0.06500786);
    path_3.cubicTo(
        size.width * 0.8972321,
        size.height * 0.05329071,
        size.width * 0.8906429,
        size.height * 0.03740393,
        size.width * 0.8906250,
        size.height * 0.02083332);
    path_3.cubicTo(
        size.width * 0.8906250,
        size.height * 0.01530800,
        size.width * 0.8884286,
        size.height * 0.01000896,
        size.width * 0.8845214,
        size.height * 0.006101929);
    path_3.cubicTo(size.width * 0.8806143, size.height * 0.002194936,
        size.width * 0.8753179, 0, size.width * 0.8697929, 0);
    path_3.cubicTo(
        size.width * 0.8642679,
        0,
        size.width * 0.8589679,
        size.height * 0.002194936,
        size.width * 0.8550607,
        size.height * 0.006101929);
    path_3.cubicTo(
        size.width * 0.8511536,
        size.height * 0.01000896,
        size.width * 0.8489571,
        size.height * 0.01530800,
        size.width * 0.8489571,
        size.height * 0.02083332);
    path_3.cubicTo(
        size.width * 0.8489393,
        size.height * 0.03740393,
        size.width * 0.8423500,
        size.height * 0.05329071,
        size.width * 0.8306321,
        size.height * 0.06500786);
    path_3.cubicTo(
        size.width * 0.8189143,
        size.height * 0.07672500,
        size.width * 0.8030286,
        size.height * 0.08331536,
        size.width * 0.7864571,
        size.height * 0.08333321);
    path_3.cubicTo(
        size.width * 0.7809321,
        size.height * 0.08333321,
        size.width * 0.7756357,
        size.height * 0.08552821,
        size.width * 0.7717286,
        size.height * 0.08943536);
    path_3.cubicTo(
        size.width * 0.7678214,
        size.height * 0.09334214,
        size.width * 0.7656250,
        size.height * 0.09864143,
        size.width * 0.7656250,
        size.height * 0.1041668);
    path_3.cubicTo(
        size.width * 0.7656250,
        size.height * 0.1096921,
        size.width * 0.7678214,
        size.height * 0.1149911,
        size.width * 0.7717286,
        size.height * 0.1188982);
    path_3.cubicTo(
        size.width * 0.7756357,
        size.height * 0.1228050,
        size.width * 0.7809321,
        size.height * 0.1250000,
        size.width * 0.7864571,
        size.height * 0.1250000);
    path_3.cubicTo(
        size.width * 0.8030286,
        size.height * 0.1250179,
        size.width * 0.8189143,
        size.height * 0.1316086,
        size.width * 0.8306321,
        size.height * 0.1433257);
    path_3.cubicTo(
        size.width * 0.8423500,
        size.height * 0.1550429,
        size.width * 0.8489393,
        size.height * 0.1709296,
        size.width * 0.8489571,
        size.height * 0.1875000);
    path_3.cubicTo(
        size.width * 0.8489571,
        size.height * 0.1930254,
        size.width * 0.8511536,
        size.height * 0.1983243,
        size.width * 0.8550607,
        size.height * 0.2022314);
    path_3.cubicTo(
        size.width * 0.8589679,
        size.height * 0.2061382,
        size.width * 0.8642679,
        size.height * 0.2083332,
        size.width * 0.8697929,
        size.height * 0.2083332);
    path_3.cubicTo(
        size.width * 0.8753179,
        size.height * 0.2083332,
        size.width * 0.8806143,
        size.height * 0.2061382,
        size.width * 0.8845214,
        size.height * 0.2022314);
    path_3.cubicTo(
        size.width * 0.8884286,
        size.height * 0.1983243,
        size.width * 0.8906250,
        size.height * 0.1930254,
        size.width * 0.8906250,
        size.height * 0.1875000);
    path_3.cubicTo(
        size.width * 0.8906429,
        size.height * 0.1709296,
        size.width * 0.8972321,
        size.height * 0.1550429,
        size.width * 0.9089500,
        size.height * 0.1433257);
    path_3.cubicTo(
        size.width * 0.9206679,
        size.height * 0.1316086,
        size.width * 0.9365536,
        size.height * 0.1250179,
        size.width * 0.9531250,
        size.height * 0.1250000);
    path_3.cubicTo(
        size.width * 0.9586500,
        size.height * 0.1250000,
        size.width * 0.9639500,
        size.height * 0.1228050,
        size.width * 0.9678571,
        size.height * 0.1188982);
    path_3.cubicTo(
        size.width * 0.9717643,
        size.height * 0.1149911,
        size.width * 0.9739571,
        size.height * 0.1096921,
        size.width * 0.9739571,
        size.height * 0.1041668);
    path_3.cubicTo(
        size.width * 0.9739571,
        size.height * 0.09864143,
        size.width * 0.9717643,
        size.height * 0.09334214,
        size.width * 0.9678571,
        size.height * 0.08943536);
    path_3.cubicTo(
        size.width * 0.9639500,
        size.height * 0.08552821,
        size.width * 0.9586500,
        size.height * 0.08333321,
        size.width * 0.9531250,
        size.height * 0.08333321);
    path_3.close();

    Paint paint_3_fill = Paint()..style = PaintingStyle.fill;
    paint_3_fill.color = Colors.white;
    canvas.drawPath(path_3, paint_3_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}

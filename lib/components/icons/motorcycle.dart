// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';

class MotorCycleCustomPainter extends CustomPainter {
  final Color color;

  const MotorCycleCustomPainter({
    this.color = const Color(0xff434343),
    Listenable? repaint,
  }) : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.9377538, size.height * 0.7340942);
    path_0.cubicTo(
        size.width * 0.9401846,
        size.height * 0.7205392,
        size.width * 0.9393846,
        size.height * 0.7051125,
        size.width * 0.9352692,
        size.height * 0.6890583);
    path_0.cubicTo(
        size.width * 0.9082923,
        size.height * 0.5837842,
        size.width * 0.8275692,
        size.height * 0.3682433,
        size.width * 0.7604908,
        size.height * 0.2794708);
    path_0.cubicTo(
        size.width * 0.7428877,
        size.height * 0.2561767,
        size.width * 0.7089769,
        size.height * 0.2581892,
        size.width * 0.6893169,
        size.height * 0.2794708);
    path_0.cubicTo(
        size.width * 0.6806677,
        size.height * 0.2888458,
        size.width * 0.6758615,
        size.height * 0.3007858,
        size.width * 0.6748362,
        size.height * 0.3130300);
    path_0.cubicTo(
        size.width * 0.6695700,
        size.height * 0.3077533,
        size.width * 0.6628908,
        size.height * 0.3041092,
        size.width * 0.6555192,
        size.height * 0.3030667);
    path_0.cubicTo(
        size.width * 0.6555192,
        size.height * 0.3030667,
        size.width * 0.4853831,
        size.height * 0.2800992,
        size.width * 0.4833192,
        size.height * 0.2803917);
    path_0.lineTo(size.width * 0.4059485, size.height * 0.2803917);
    path_0.cubicTo(
        size.width * 0.3807377,
        size.height * 0.2803917,
        size.width * 0.3733592,
        size.height * 0.2962292,
        size.width * 0.3733592,
        size.height * 0.3156967);
    path_0.lineTo(size.width * 0.3733592, size.height * 0.5847700);
    path_0.lineTo(size.width * 0.3165746, size.height * 0.5847700);
    path_0.lineTo(size.width * 0.3165746, size.height * 0.2962225);
    path_0.cubicTo(
        size.width * 0.3165746,
        size.height * 0.2819467,
        size.width * 0.3058523,
        size.height * 0.2703317,
        size.width * 0.2926754,
        size.height * 0.2703317);
    path_0.lineTo(size.width * 0.06237008, size.height * 0.2703317);
    path_0.cubicTo(
        size.width * 0.04919300,
        size.height * 0.2703317,
        size.width * 0.03847092,
        size.height * 0.2819467,
        size.width * 0.03847092,
        size.height * 0.2962225);
    path_0.cubicTo(
        size.width * 0.03847092,
        size.height * 0.2962225,
        size.width * 0.03855346,
        size.height * 0.7014008,
        size.width * 0.03855346,
        size.height * 0.7276092);
    path_0.cubicTo(
        size.width * 0.03855346,
        size.height * 0.7577158,
        size.width * 0.06108169,
        size.height * 0.7821192,
        size.width * 0.08887000,
        size.height * 0.7821192);
    path_0.lineTo(size.width * 0.1382454, size.height * 0.7821192);
    path_0.cubicTo(
        size.width * 0.1356946,
        size.height * 0.7933717,
        size.width * 0.1343154,
        size.height * 0.8051192,
        size.width * 0.1343154,
        size.height * 0.8172125);
    path_0.cubicTo(
        size.width * 0.1343154,
        size.height * 0.8984250,
        size.width * 0.1953146,
        size.height * 0.9644917,
        size.width * 0.2702908,
        size.height * 0.9644917);
    path_0.cubicTo(
        size.width * 0.3452623,
        size.height * 0.9644917,
        size.width * 0.4062577,
        size.height * 0.8984250,
        size.width * 0.4062577,
        size.height * 0.8172125);
    path_0.cubicTo(
        size.width * 0.4062577,
        size.height * 0.8051192,
        size.width * 0.4048754,
        size.height * 0.7933717,
        size.width * 0.4023269,
        size.height * 0.7821192);
    path_0.lineTo(size.width * 0.5545038, size.height * 0.7821192);
    path_0.lineTo(size.width * 0.5545038, size.height * 0.8182342);
    path_0.cubicTo(
        size.width * 0.5545038,
        size.height * 0.8467833,
        size.width * 0.5759454,
        size.height * 0.8700167,
        size.width * 0.6023023,
        size.height * 0.8700167);
    path_0.lineTo(size.width * 0.6032169, size.height * 0.8700167);
    path_0.cubicTo(
        size.width * 0.6159638,
        size.height * 0.8700167,
        size.width * 0.6279308,
        size.height * 0.8646000,
        size.width * 0.6369123,
        size.height * 0.8547750);
    path_0.cubicTo(
        size.width * 0.6458938,
        size.height * 0.8449417,
        size.width * 0.6507677,
        size.height * 0.8319258,
        size.width * 0.6506392,
        size.height * 0.8181167);
    path_0.lineTo(size.width * 0.6503008, size.height * 0.7821192);
    path_0.lineTo(size.width * 0.6935408, size.height * 0.7821192);
    path_0.cubicTo(
        size.width * 0.6909900,
        size.height * 0.7933717,
        size.width * 0.6896100,
        size.height * 0.8051192,
        size.width * 0.6896100,
        size.height * 0.8172125);
    path_0.cubicTo(
        size.width * 0.6896100,
        size.height * 0.8984250,
        size.width * 0.7506092,
        size.height * 0.9644917,
        size.width * 0.8255846,
        size.height * 0.9644917);
    path_0.cubicTo(
        size.width * 0.9005538,
        size.height * 0.9644917,
        size.width * 0.9615538,
        size.height * 0.8984250,
        size.width * 0.9615538,
        size.height * 0.8172125);
    path_0.cubicTo(
        size.width * 0.9615462,
        size.height * 0.7863958,
        size.width * 0.9527462,
        size.height * 0.7577700,
        size.width * 0.9377538,
        size.height * 0.7340942);
    path_0.close();
    path_0.moveTo(size.width * 0.8255846, size.height * 0.7616250);
    path_0.cubicTo(
        size.width * 0.8538769,
        size.height * 0.7616250,
        size.width * 0.8768923,
        size.height * 0.7865625,
        size.width * 0.8768923,
        size.height * 0.8172125);
    path_0.cubicTo(
        size.width * 0.8768923,
        size.height * 0.8478667,
        size.width * 0.8538769,
        size.height * 0.8728000,
        size.width * 0.8255846,
        size.height * 0.8728000);
    path_0.cubicTo(
        size.width * 0.7972769,
        size.height * 0.8728000,
        size.width * 0.7742538,
        size.height * 0.8478667,
        size.width * 0.7742538,
        size.height * 0.8172125);
    path_0.cubicTo(
        size.width * 0.7742538,
        size.height * 0.7865625,
        size.width * 0.7972769,
        size.height * 0.7616250,
        size.width * 0.8255846,
        size.height * 0.7616250);
    path_0.close();
    path_0.moveTo(size.width * 0.3215977, size.height * 0.8172125);
    path_0.cubicTo(
        size.width * 0.3215977,
        size.height * 0.8478667,
        size.width * 0.2985808,
        size.height * 0.8728000,
        size.width * 0.2702885,
        size.height * 0.8728000);
    path_0.cubicTo(
        size.width * 0.2419854,
        size.height * 0.8728000,
        size.width * 0.2189592,
        size.height * 0.8478667,
        size.width * 0.2189592,
        size.height * 0.8172125);
    path_0.cubicTo(
        size.width * 0.2189592,
        size.height * 0.7865625,
        size.width * 0.2419854,
        size.height * 0.7616250,
        size.width * 0.2702885,
        size.height * 0.7616250);
    path_0.cubicTo(
        size.width * 0.2985808,
        size.height * 0.7616250,
        size.width * 0.3215977,
        size.height * 0.7865625,
        size.width * 0.3215977,
        size.height * 0.8172125);
    path_0.close();
    path_0.moveTo(size.width * 0.6485077, size.height * 0.5927042);
    path_0.cubicTo(
        size.width * 0.6482408,
        size.height * 0.5642175,
        size.width * 0.6266292,
        size.height * 0.5410450,
        size.width * 0.6003338,
        size.height * 0.5410450);
    path_0.lineTo(size.width * 0.5207992, size.height * 0.5410450);
    path_0.lineTo(size.width * 0.5207992, size.height * 0.3650992);
    path_0.lineTo(size.width * 0.5596900, size.height * 0.3702742);
    path_0.cubicTo(
        size.width * 0.5628554,
        size.height * 0.3860325,
        size.width * 0.5757869,
        size.height * 0.3978392,
        size.width * 0.5912785,
        size.height * 0.3978392);
    path_0.lineTo(size.width * 0.7274123, size.height * 0.3978392);
    path_0.lineTo(size.width * 0.7379646, size.height * 0.4930792);
    path_0.lineTo(size.width * 0.6487623, size.height * 0.6195975);
    path_0.lineTo(size.width * 0.6485077, size.height * 0.5927042);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = color;
    canvas.drawPath(path_0, paint_0_fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.4352738, size.height * 0.2426433);
    path_1.cubicTo(
        size.width * 0.4879885,
        size.height * 0.2426433,
        size.width * 0.5308746,
        size.height * 0.1961833,
        size.width * 0.5308746,
        size.height * 0.1390733);
    path_1.cubicTo(
        size.width * 0.5308746,
        size.height * 0.1318117,
        size.width * 0.5301731,
        size.height * 0.1247250,
        size.width * 0.5288546,
        size.height * 0.1178800);
    path_1.lineTo(size.width * 0.5498292, size.height * 0.1174333);
    path_1.cubicTo(
        size.width * 0.5623238,
        size.height * 0.1171650,
        size.width * 0.5757946,
        size.height * 0.1077408,
        size.width * 0.5811592,
        size.height * 0.09550833);
    path_1.lineTo(size.width * 0.5881938, size.height * 0.07947008);
    path_1.cubicTo(
        size.width * 0.5915354,
        size.height * 0.07185117,
        size.width * 0.5913615,
        size.height * 0.06389558,
        size.width * 0.5877115,
        size.height * 0.05764183);
    path_1.cubicTo(
        size.width * 0.5840662,
        size.height * 0.05139275,
        size.width * 0.5775177,
        size.height * 0.04780808,
        size.width * 0.5696738,
        size.height * 0.04780808);
    path_1.lineTo(size.width * 0.4808062, size.height * 0.04803167);
    path_1.cubicTo(
        size.width * 0.4672592,
        size.height * 0.04004792,
        size.width * 0.4517469,
        size.height * 0.03551225,
        size.width * 0.4352715,
        size.height * 0.03551225);
    path_1.cubicTo(
        size.width * 0.3825562,
        size.height * 0.03551225,
        size.width * 0.3396700,
        size.height * 0.08196975,
        size.width * 0.3396700,
        size.height * 0.1390775);
    path_1.cubicTo(
        size.width * 0.3396723,
        size.height * 0.1961833,
        size.width * 0.3825585,
        size.height * 0.2426433,
        size.width * 0.4352738,
        size.height * 0.2426433);
    path_1.close();

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = color;
    canvas.drawPath(path_1, paint_1_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}

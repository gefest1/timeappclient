import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

import 'package:li/components/molecules/minute/all_institutions_page.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/components/pages/qfit_to_timeapp/qfit_detail_page.dart';
import 'package:li/components/pages/qfit_to_timeapp/qfit_tile.dart';
import 'package:li/logic/blocs/comments_of_club/commentsofclub_bloc.dart';
import 'package:li/logic/blocs/qfit_bloc/mainPage/MainPageBloc.dart';
import 'package:li/logic/providers/location/location_provider.dart';
import 'package:li/main.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/locationsBetween.dart';
import 'package:time_app_components/utils/testStyles.dart';

class MainPageCards extends StatefulWidget {
  const MainPageCards({Key? key}) : super(key: key);

  @override
  State<MainPageCards> createState() => _MainPageCardsState();
}

class _MainPageCardsState extends State<MainPageCards>
    with SingleTickerProviderStateMixin {
  void initState() {
    super.initState();

    final _locationProvider =
        Provider.of<LocationProvider>(context, listen: false);
    if (_locationProvider.location == null) {
      _locationProvider.onceGet();
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MainPageMinuteBloc, MainPageMinuteState>(
      builder: (_, state) {
        return state.when(
          initial: () {
            return Material(
              color: Colors.white,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          },
          loaded: (state) {
            return Column(
                children: List.generate(state.mainPageModel.length * 2,
                    (int indexCount) {
              if (indexCount % 2 == 1) return const SizedBox(height: 40);
              final index = indexCount ~/ 2;
              final single = state.mainPageModel[index];
              return Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => AllInstitutionsPage(
                              clubsCount: single.numberOfInstitutions!,
                              title: single.title!.ru!,
                              type: single.slug!,
                            ),
                          ),
                        );
                      },
                      child: Row(
                        children: [
                          Expanded(
                            child: Text(
                              single.title!.ru!,
                              style: TextStyle(
                                color: ColorData.mainBlack,
                                fontWeight: H2TextStyle.fontWeight,
                                fontSize: H2TextStyle.fontSize,
                                height: H2TextStyle.height,
                              ),
                            ),
                          ),
                          Text(
                            'Все ' +
                                single.numberOfInstitutions!.toStringAsFixed(0),
                            style: TextStyle(
                              color: Color(0xff00D42B),
                              fontWeight: FontWeight.w400,
                              fontSize: 16,
                              height: 29 / 24,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(height: 15),
                  SizedBox(
                    height: 203,
                    child: Consumer<LocationProvider>(
                      builder: (context, locationProvider, _) {
                        final myLocation = locationProvider.location;
                        return ListView.separated(
                          clipBehavior: Clip.none,
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (ctx, _) {
                            String? labelLocation;
                            if (myLocation != null) {
                              labelLocation = locationBetweenText(
                                myLocation.latitude,
                                myLocation.longitude,
                                single.institutions![_].location!.latitude!
                                    .toDouble(),
                                single.institutions![_].location!.longitude!
                                    .toDouble(),
                              );
                            }
                            return SizeTapAnimation(
                              child: QfitTile(
                                onTap: () {
                                  final commentBloc =
                                      BlocProvider.of<CommentsofclubBloc>(
                                          context,
                                          listen: false);
                                  commentBloc.add(GetCommentsOfClubEvent(
                                      institutionId:
                                          single.institutions![_].id!));
                                  routerDelegate.push(
                                    QfitDetailPage(
                                      institution: single.institutions![_],
                                      //   ),
                                    ),
                                  );
                                },
                                locationLabel: labelLocation,
                                photoUrl: single.institutions?[_].avatarURL?.xl,
                                rating: single.institutions![_].rating
                                    ?.toStringAsFixed(1),
                                title: single.institutions?[_].name,
                                label: single.institutions![_].averagePrice!
                                        .toString() +
                                    " тг/мин",
                              ),
                            );
                          },
                          separatorBuilder: (ctx, _) =>
                              const SizedBox(width: 16),
                          itemCount: single.institutions!.length,
                        );
                      },
                    ),
                  )
                ],
              );
            }));
          },
        );
      },
    );
  }
}

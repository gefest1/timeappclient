import 'package:graphql_flutter/graphql_flutter.dart';

final getRadiusQuery = gql('''
query(
  \$latitude: Float!
  \$longitude: Float!
  \$maxMetres: Float!
  \$minMetres: Float!
) {
  getInstitutionCord(
    latitude: \$latitude
    longitude: \$longitude
    maxMetres: \$maxMetres
    minMetres: \$minMetres
  ) {
    _id
    avatarURL {
      XL
      M
      thumbnail
    }
    averagePrice
    city
    contacts {
      instagram
      whatsApp
      email
      phoneNumber
    }
    dateAdded
    description
    galleryURLs {
      XL
      M
      thumbnail
    }
    tags {
      iconSVGPath
      name
    }
    location {
      address
      latitude
      longitude
    }
    name
    numOfWorkers
    rating
    ratingCount
    type
  }
}''');

import 'dart:convert';

import 'package:equatable/equatable.dart';

class CreditCardModel extends Equatable {
  final String? id;
  final String? cardHolderName;
  final String? cardType;
  final String? last4Digits;

  CreditCardModel({
    this.id,
    this.cardHolderName,
    this.cardType,
    this.last4Digits,
  });

  CreditCardModel copyWith({
    String? id,
    String? cardHolderName,
    String? cardType,
    String? last4Digits,
  }) {
    return CreditCardModel(
      id: id ?? this.id,
      cardHolderName: cardHolderName ?? this.cardHolderName,
      cardType: cardType ?? this.cardType,
      last4Digits: last4Digits ?? this.last4Digits,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'cardHolderName': cardHolderName,
      'cardType': cardType,
      'last4Digits': last4Digits,
    };
  }

  static CreditCardModel? fromMap(Map<String, dynamic>? map) {
    if (map == null) return null;
    return CreditCardModel(
      id: map['_id'],
      cardHolderName: map['cardHolderName'],
      cardType: map['cardType'],
      last4Digits: map['last4Digits'],
    );
  }

  String toJson() => json.encode(toMap());

  factory CreditCardModel.fromJson(String source) =>
      CreditCardModel.fromMap(json.decode(source))!;

  @override
  bool get stringify => true;

  @override
  List<Object?> get props => [
        id,
        cardHolderName,
        cardType,
        last4Digits,
      ];
}

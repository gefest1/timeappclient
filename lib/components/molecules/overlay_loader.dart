import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class OverlayLoader extends OverlayEntry {
  final Widget child;
  final double? left;
  final double? top;
  final double? right;
  final double? bottom;
  final double? width;
  final double? height;

  OverlayLoader({
    required this.child,
    this.left,
    this.top,
    this.right,
    this.bottom,
    this.width,
    this.height,
  }) : super(
          builder: (_) {
            return SizedBox();
          },
        );

  Widget _buider(BuildContext context) {
    return OverlayLoaderWidget(
      overlayEntry: this,
      child: this.child,
      left: left,
      top: top,
      right: right,
      bottom: bottom,
      width: width,
      height: height,
    );
  }

  WidgetBuilder get builder => _buider;
}

class OverlayLoaderWidget extends StatefulWidget {
  final OverlayLoader? overlayEntry;
  final double? left;
  final double? top;
  final double? right;
  final double? bottom;
  final double? width;
  final double? height;

  final Widget child;

  const OverlayLoaderWidget({
    this.overlayEntry,
    required this.child,
    this.left,
    this.top,
    this.right,
    this.bottom,
    this.width,
    this.height,
    Key? key,
  }) : super(key: key);

  @override
  _OverlayLoaderWidgetState createState() => _OverlayLoaderWidgetState();
}

class _OverlayLoaderWidgetState extends State<OverlayLoaderWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController animationController = AnimationController(
    vsync: this,
    duration: Duration(milliseconds: 140),
    value: 0,
  );

  void _init() async {
    await animationController.forward();
  }

  @override
  void initState() {
    super.initState();
    _init();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: widget.left,
      top: widget.top,
      right: widget.right,
      bottom: widget.bottom,
      width: widget.width,
      height: widget.height,
      child: FadeTransition(
        opacity: animationController,
        child: widget.child,
      ),
    );
  }
}

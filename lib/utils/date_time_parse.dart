DateTime? dateTimeParse(dynamic data) {
  if (data == null) return null;
  if (data is int) return DateTime.fromMillisecondsSinceEpoch(data);
  return DateTime.tryParse(data);
}

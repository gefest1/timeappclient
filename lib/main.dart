import 'dart:developer';
import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:li/logic/firebase_token/firebase_token_device.dart';
import 'package:li/utils/flow_menu_delegate.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:time_app_components/time_app_components.dart';

import 'package:li/components/molecules/update_available.dart';
import 'package:li/const.dart';
import 'package:li/logic/navigation.dart';
import 'package:li/logic/provider_template.dart';
import 'package:li/template/bloc_template.dart';
import 'package:li/utils/Notification.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/my_behavior.dart';
import 'package:li/utils/restart_widget.dart';

import 'package:li/network/graphql_provider.dart';
import 'package:provider/provider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:li/logic/providers/location/location_provider.dart';

late SharedPreferences sharedPreferences;

late GlobalKey<NavigatorState> navigatorKey;
late CustomRouterDelegate routerDelegate;
late PackageInfo packageInfo;

bool isRegistered() =>
    sharedPreferences.containsKey('client') &&
    sharedPreferences.containsKey('token');

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final List<dynamic> results = await Future.wait([
    SharedPreferences.getInstance(),
    Firebase.initializeApp(),
    PackageInfo.fromPlatform(),
  ]);
  sharedPreferences = results[0];
  packageInfo = results[2];
  textFieldMode = TextFieldEnum.client;

  runApp(
    const RestartWidget(
      child: const ProviderTemplate(
        child: const BlocTemplate(
          child: const MyApp(),
        ),
      ),
    ),
  );
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late final graphqlProvider = Provider.of<GraphqlProvider>(
    context,
    listen: false,
  );
  late LocationProvider locationProvider = Provider.of(context, listen: false);
  String deviceId = "";
  @override
  void initState() {
    super.initState();
    navigatorKey = GlobalKey<NavigatorState>();
    routerDelegate = CustomRouterDelegate(
      navigatorKey: navigatorKey,
    );

    Future.microtask(() {
      _initS();
      opnedUpdate();
    });
  }

  _initS() async {
    locationProvider.activate();
    if (kReleaseMode) await initFirebaseMessanger();
    if (isRegistered()) {
      final token = await FirebaseMessaging.instance.getToken();
      if (token == null) return;
      final id = await getId();
      sendId(
        fireBaseToken: token.toString(),
        deviceId: id,
        provider: graphqlProvider,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = ThemeData(
      fontFamily: Platform.isIOS ? 'SfProDisplay' : 'Roboto',
    );
    return MaterialApp.router(
      routerDelegate: routerDelegate,
      routeInformationParser: CustomRouteInformationParser(),
      theme: theme.copyWith(
        colorScheme: theme.colorScheme.copyWith(
          secondary: ColorData.clientsButtonColorDefault,
          primary: ColorData.clientsButtonColorDefault,
        ),
        buttonTheme: ButtonThemeData(
          buttonColor: buttonsDefault,
          textTheme: ButtonTextTheme.primary,
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(buttonsDefault),
          ),
        ),
      ),
      builder: (ctx, child) {
        if (child == null) return SizedBox();
        final _media = MediaQuery.of(ctx);
        devicePixelRatioFlow = _media.devicePixelRatio;
        return ScrollConfiguration(
          behavior: MyBehavior(),
          child: MediaQuery(
            data: _media.copyWith(textScaleFactor: 1.0),
            child: child,
          ),
        );
      },
      debugShowCheckedModeBanner: false,
    );
  }
}

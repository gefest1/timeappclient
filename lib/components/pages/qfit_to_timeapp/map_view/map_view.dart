import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:li/components/pages/qfit_to_timeapp/map_view/mylocation.dart';
import 'package:li/data/models/allowed_institution_types.dart';
import 'package:provider/provider.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

import 'package:li/components/pages/qfit_to_timeapp/map_view/flow_marker.dart';
import 'package:li/components/pages/qfit_to_timeapp/map_view/flow_marker_limited.dart';
import 'package:li/components/pages/qfit_to_timeapp/map_view/reaction/location_observer.dart';
import 'package:li/data/models/institution_minute.dart';
import 'package:li/logic/providers/geo_institution/GeoInstitutionProvider.dart';
import 'package:li/utils/determinePosition.dart';

part 'reaction/on_map_create.dart';

class MapView extends StatefulWidget {
  final List<InstitutionsMinute>? institutions;
  final AllowedInstitutionTypes? type;

  const MapView({
    this.institutions,
    this.type,
    Key? key,
  }) : super(key: key);

  @override
  _MapViewState createState() => _MapViewState();
}

class _MapViewState extends State<MapView>
    with
        AutomaticKeepAliveClientMixin,
        LocationObserver,
        WidgetsBindingObserver {
  final GlobalKey _mapKey = GlobalKey();

  YandexMapController? yandexMapController;
  final StreamController<Null> streamController =
      StreamController<Null>.broadcast();
  bool initialized = false;

  void geoInitialized(Position position) {
    if (initialized) return;
    initialized = true;
    Provider.of<GeoInstitutionProvider>(context, listen: false).getInstituion(
      point: Point(
        latitude: position.latitude,
        longitude: position.longitude,
      ),
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (AppLifecycleState.resumed == state) {
      getPermsissionsLocation().then((value) {
        if (value) locationProvider.activate(callBack: geoInitialized);
      });
    }
    super.didChangeAppLifecycleState(state);
  }

  @override
  void initState() {
    WidgetsBinding.instance!.addObserver(this);
    locationStart(callBack: geoInitialized);

    super.initState();
  }

  @override
  void dispose() {
    locationEnd();
    streamController.close();
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Future.delayed(Duration(seconds: 1), () {
      log((_mapKey.currentContext?.size).toString());
    });
    // FUture
    // log((_mapKey.currentContext?.size).toString());
    super.build(context);
    return Stack(
      children: [
        YandexMap(
          key: _mapKey,
          onMapCreated: onMapCreated,
          tiltGesturesEnabled: false,
          onCameraPositionChanged: (cameraPos, _, finis) =>
              cameraTracking(cameraPos, finis),
        ),
        Positioned(
          bottom: 150,
          right: 10,
          child: FloatingActionButton(
            backgroundColor: Colors.white,
            onPressed: () async {
              yandexMapController?.moveCamera(
                CameraUpdate.newCameraPosition(CameraPosition(
                  target: Point(
                    latitude: locationProvider.location!.latitude.toDouble(),
                    longitude: locationProvider.location!.longitude.toDouble(),
                  ),
                )),
                animation: MapAnimation(
                  duration: 1.0,
                ),
              );
            },
            child: SvgPicture.asset('assets/svg/icon/send.svg'),
          ),
        ),
        if (yandexMapController != null)
          if (widget.institutions == null)
            FlowMarker(
              type: widget.type,
              streamController: streamController,
              yandexMapController: yandexMapController!,
            )
          else
            FlowMarkerLimited(
              yandexMapController: yandexMapController!,
              streamController: streamController,
              institutions: widget.institutions!,
            ),
        if (yandexMapController != null)
          LocationMarker(
            yandexMapController: yandexMapController!,
            streamController: streamController,
          ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void setState(VoidCallback fn) {
    super.setState(fn);
  }
}

import 'dart:convert';

import 'package:equatable/equatable.dart';

class Location extends Equatable {
  final String? address;
  final num? latitude;
  final num? longitude;

  Location({
    this.address,
    this.latitude,
    this.longitude,
  });

  Location copyWith({
    String? address,
    num? latitude,
    num? longitude,
  }) {
    return Location(
      address: address ?? this.address,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'address': address,
      'latitude': latitude,
      'longitude': longitude,
    };
  }

  static Location? fromMap(Map<String, dynamic>? map) {
    if (map == null) return null;
    return Location(
      address: map['address'] != null ? map['address'] : null,
      latitude: map['latitude'] != null ? map['latitude'] : null,
      longitude: map['longitude'] != null ? map['longitude'] : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Location.fromJson(String source) =>
      Location.fromMap(json.decode(source))!;

  @override
  bool get stringify => true;

  @override
  List<Object?> get props => [address, latitude, longitude];
}

import 'package:flutter/material.dart';
import 'package:li/components/molecules/chat_card.dart';
import 'package:li/components/pages/messenger/in_chat_page.dart';
import 'package:li/main.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/utils/color.dart';

class ChatsPage extends StatefulWidget {
  const ChatsPage({Key? key}) : super(key: key);

  @override
  _ChatsPageState createState() => _ChatsPageState();
}

class _ChatsPageState extends State<ChatsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(
        title: Text(
          'Сообщения',
          style: TextStyle(
            fontSize: 18,
            color: ColorData.allMainBlack,
            fontWeight: FontWeight.w500,
            height: 21 / 18,
          ),
        ),
      ),
      body: ListView.builder(
        padding: EdgeInsets.only(
          top: 30,
        ),
        itemCount: 2,
        itemBuilder: (context, index) => InkWell(
          onTap: () {
            routerDelegate.push(const InChatPage());
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 4),
            child: const ChatCard(),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

Path getPath3(Size size) {
  return Path()
    ..moveTo(size.width * 0.9014675, size.height * 0.1314952)
    ..cubicTo(
        size.width * 0.9018750,
        size.height * 0.1317087,
        size.width * 0.9025450,
        size.height * 0.1317087,
        size.width * 0.9029550,
        size.height * 0.1314952)
    ..cubicTo(
        size.width * 0.9033650,
        size.height * 0.1312816,
        size.width * 0.9030300,
        size.height * 0.1311078,
        size.width * 0.9022100,
        size.height * 0.1311078)
    ..cubicTo(
        size.width * 0.9013925,
        size.height * 0.1311078,
        size.width * 0.9010575,
        size.height * 0.1312816,
        size.width * 0.9014675,
        size.height * 0.1314952)
    ..moveTo(size.width * 0.8061000, size.height * 0.1391481)
    ..lineTo(size.width * 0.8046350, size.height * 0.1423116)
    ..lineTo(size.width * 0.8062275, size.height * 0.1394014)
    ..cubicTo(
        size.width * 0.8077125,
        size.height * 0.1366948,
        size.width * 0.8080175,
        size.height * 0.1359797,
        size.width * 0.8076950,
        size.height * 0.1359797)
    ..cubicTo(
        size.width * 0.8076225,
        size.height * 0.1359797,
        size.width * 0.8069050,
        size.height * 0.1374050,
        size.width * 0.8061000,
        size.height * 0.1391481)
    ..moveTo(size.width * 0.8354950, size.height * 0.1388352)
    ..lineTo(size.width * 0.8344700, size.height * 0.1414673)
    ..lineTo(size.width * 0.8356850, size.height * 0.1391729)
    ..cubicTo(
        size.width * 0.8363550,
        size.height * 0.1379065,
        size.width * 0.8368175,
        size.height * 0.1367246,
        size.width * 0.8367125,
        size.height * 0.1365408)
    ..cubicTo(
        size.width * 0.8366075,
        size.height * 0.1363521,
        size.width * 0.8360600,
        size.height * 0.1373851,
        size.width * 0.8354950,
        size.height * 0.1388352)
    ..moveTo(size.width * 0.7932650, size.height * 0.1796525)
    ..cubicTo(
        size.width * 0.7937950,
        size.height * 0.1798511,
        size.width * 0.7945600,
        size.height * 0.1798461,
        size.width * 0.7949675,
        size.height * 0.1796326)
    ..cubicTo(
        size.width * 0.7953725,
        size.height * 0.1794240,
        size.width * 0.7949400,
        size.height * 0.1792552,
        size.width * 0.7940050,
        size.height * 0.1792651)
    ..cubicTo(
        size.width * 0.7930700,
        size.height * 0.1792750,
        size.width * 0.7927375,
        size.height * 0.1794489,
        size.width * 0.7932650,
        size.height * 0.1796525)
    ..moveTo(size.width * 0.7998800, size.height * 0.1849663)
    ..cubicTo(
        size.width * 0.7998800,
        size.height * 0.1879858,
        size.width * 0.7999575,
        size.height * 0.1892223,
        size.width * 0.8000525,
        size.height * 0.1877126)
    ..arcToPoint(Offset(size.width * 0.8000525, size.height * 0.1822200),
        radius: Radius.elliptical(
            size.width * 0.01345000, size.height * 0.02671818),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.7999575,
        size.height * 0.1807103,
        size.width * 0.7998800,
        size.height * 0.1819469,
        size.width * 0.7998800,
        size.height * 0.1849663)
    ..moveTo(size.width * 0.8292125, size.height * 0.1845442)
    ..cubicTo(
        size.width * 0.8292150,
        size.height * 0.1873302,
        size.width * 0.8292950,
        size.height * 0.1883682,
        size.width * 0.8293900,
        size.height * 0.1868535)
    ..arcToPoint(Offset(size.width * 0.8293850, size.height * 0.1817830),
        radius: Radius.elliptical(
            size.width * 0.01128750, size.height * 0.02242242),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8292875,
        size.height * 0.1805166,
        size.width * 0.8292100,
        size.height * 0.1817581,
        size.width * 0.8292125,
        size.height * 0.1845442)
    ..moveTo(size.width * 0.8053925, size.height * 0.1845442)
    ..cubicTo(
        size.width * 0.8053975,
        size.height * 0.1868684,
        size.width * 0.8054800,
        size.height * 0.1877176,
        size.width * 0.8055775,
        size.height * 0.1864313)
    ..arcToPoint(Offset(size.width * 0.8055725, size.height * 0.1822101),
        radius: Radius.elliptical(
            size.width * 0.007632500, size.height * 0.01516182),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8054700,
        size.height * 0.1811721,
        size.width * 0.8053900,
        size.height * 0.1822200,
        size.width * 0.8053925,
        size.height * 0.1845442)
    ..moveTo(size.width * 0.7921900, size.height * 0.1900318)
    ..cubicTo(
        size.width * 0.7921900,
        size.height * 0.1916607,
        size.width * 0.7922775,
        size.height * 0.1923262,
        size.width * 0.7923850,
        size.height * 0.1915118)
    ..arcToPoint(Offset(size.width * 0.7923850, size.height * 0.1885569),
        radius: Radius.elliptical(
            size.width * 0.003470000, size.height * 0.006893093),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.7922775,
        size.height * 0.1877424,
        size.width * 0.7921900,
        size.height * 0.1884079,
        size.width * 0.7921900,
        size.height * 0.1900318)
    ..moveTo(size.width * 0.09687000, size.height * 0.1972030)
    ..cubicTo(
        size.width * 0.09585500,
        size.height * 0.2006794,
        size.width * 0.09481000,
        size.height * 0.2035300,
        size.width * 0.09454500,
        size.height * 0.2035349)
    ..cubicTo(
        size.width * 0.09418250,
        size.height * 0.2035449,
        size.width * 0.09133500,
        size.height * 0.1974613,
        size.width * 0.08983000,
        size.height * 0.1934635)
    ..cubicTo(
        size.width * 0.08975250,
        size.height * 0.1932599,
        size.width * 0.08937250,
        size.height * 0.1937217,
        size.width * 0.08898500,
        size.height * 0.1944915)
    ..cubicTo(
        size.width * 0.08832750,
        size.height * 0.1957976,
        size.width * 0.08833250,
        size.height * 0.1960062,
        size.width * 0.08903750,
        size.height * 0.1975556)
    ..cubicTo(
        size.width * 0.08946250,
        size.height * 0.1984942,
        size.width * 0.08965500,
        size.height * 0.1996663,
        size.width * 0.08947500,
        size.height * 0.2002473)
    ..cubicTo(
        size.width * 0.08927750,
        size.height * 0.2008780,
        size.width * 0.08933250,
        size.height * 0.2010568,
        size.width * 0.08961250,
        size.height * 0.2007092)
    ..cubicTo(
        size.width * 0.09098000,
        size.height * 0.1990306,
        size.width * 0.09408500,
        size.height * 0.2090325,
        size.width * 0.09284750,
        size.height * 0.2111233)
    ..cubicTo(
        size.width * 0.09240750,
        size.height * 0.2118682,
        size.width * 0.09200000,
        size.height * 0.2129360,
        size.width * 0.09194250,
        size.height * 0.2135071)
    ..cubicTo(
        size.width * 0.09188250,
        size.height * 0.2140732,
        size.width * 0.09127500,
        size.height * 0.2163229,
        size.width * 0.09059000,
        size.height * 0.2185081)
    ..cubicTo(
        size.width * 0.08936500,
        size.height * 0.2224165,
        size.width * 0.08936000,
        size.height * 0.2224959,
        size.width * 0.09019500,
        size.height * 0.2236530)
    ..cubicTo(
        size.width * 0.09066000,
        size.height * 0.2243036,
        size.width * 0.09109250,
        size.height * 0.2247009,
        size.width * 0.09115750,
        size.height * 0.2245370)
    ..cubicTo(
        size.width * 0.09122000,
        size.height * 0.2243731,
        size.width * 0.09189000,
        size.height * 0.2220639,
        size.width * 0.09264250,
        size.height * 0.2194020)
    ..cubicTo(
        size.width * 0.09339750,
        size.height * 0.2167401,
        size.width * 0.09419750,
        size.height * 0.2143364,
        size.width * 0.09442000,
        size.height * 0.2140633)
    ..arcToPoint(Offset(size.width * 0.09458250, size.height * 0.2127771),
        radius: Radius.elliptical(
            size.width * 0.0004725000, size.height * 0.0009386127),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.09374000,
        size.height * 0.2100754,
        size.width * 0.09541750,
        size.height * 0.2127572,
        size.width * 0.09694500,
        size.height * 0.2165563)
    ..cubicTo(
        size.width * 0.09861250,
        size.height * 0.2207081,
        size.width * 0.1003800,
        size.height * 0.2218553,
        size.width * 0.1003325,
        size.height * 0.2187564)
    ..cubicTo(
        size.width * 0.1003225,
        size.height * 0.2180114,
        size.width * 0.09950250,
        size.height * 0.2157617,
        size.width * 0.09949750,
        size.height * 0.2164570)
    ..cubicTo(
        size.width * 0.09949000,
        size.height * 0.2170281,
        size.width * 0.09847000,
        size.height * 0.2148281,
        size.width * 0.09622000,
        size.height * 0.2094000)
    ..cubicTo(
        size.width * 0.09575750,
        size.height * 0.2082876,
        size.width * 0.09592750,
        size.height * 0.2072944,
        size.width * 0.09729500,
        size.height * 0.2030681)
    ..cubicTo(
        size.width * 0.1004375,
        size.height * 0.1933691,
        size.width * 0.1006175,
        size.height * 0.1925894,
        size.width * 0.09991000,
        size.height * 0.1917005)
    ..cubicTo(
        size.width * 0.09889000,
        size.height * 0.1904192,
        size.width * 0.09880000,
        size.height * 0.1905781,
        size.width * 0.09687000,
        size.height * 0.1972030)
    ..moveTo(size.width * 0.4361225, size.height * 0.1965425)
    ..cubicTo(
        size.width * 0.4366525,
        size.height * 0.1967461,
        size.width * 0.4374175,
        size.height * 0.1967362,
        size.width * 0.4378250,
        size.height * 0.1965276)
    ..cubicTo(
        size.width * 0.4382300,
        size.height * 0.1963141,
        size.width * 0.4377975,
        size.height * 0.1961502,
        size.width * 0.4368625,
        size.height * 0.1961601)
    ..cubicTo(
        size.width * 0.4359275,
        size.height * 0.1961701,
        size.width * 0.4355950,
        size.height * 0.1963389,
        size.width * 0.4361225,
        size.height * 0.1965425)
    ..moveTo(size.width * 0.7896400, size.height * 0.2010121)
    ..cubicTo(
        size.width * 0.7896400,
        size.height * 0.2026410,
        size.width * 0.7897275,
        size.height * 0.2033065,
        size.width * 0.7898350,
        size.height * 0.2024920)
    ..arcToPoint(Offset(size.width * 0.7898350, size.height * 0.1995371),
        radius: Radius.elliptical(
            size.width * 0.003550000, size.height * 0.007052011),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.7897275,
        size.height * 0.1987227,
        size.width * 0.7896400,
        size.height * 0.1993882,
        size.width * 0.7896400,
        size.height * 0.2010121)
    ..moveTo(size.width * 0.1633650, size.height * 0.2035449)
    ..cubicTo(
        size.width * 0.1633650,
        size.height * 0.2051738,
        size.width * 0.1634525,
        size.height * 0.2058393,
        size.width * 0.1635575,
        size.height * 0.2050248)
    ..arcToPoint(Offset(size.width * 0.1635575, size.height * 0.2020699),
        radius: Radius.elliptical(
            size.width * 0.003470000, size.height * 0.006893093),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.1634525,
        size.height * 0.2012555,
        size.width * 0.1633650,
        size.height * 0.2019209,
        size.width * 0.1633650,
        size.height * 0.2035449)
    ..moveTo(size.width * 0.7917650, size.height * 0.2111481)
    ..cubicTo(
        size.width * 0.7917650,
        size.height * 0.2127721,
        size.width * 0.7918525,
        size.height * 0.2134376,
        size.width * 0.7919600,
        size.height * 0.2126281)
    ..arcToPoint(Offset(size.width * 0.7919600, size.height * 0.2096682),
        radius: Radius.elliptical(
            size.width * 0.003482500, size.height * 0.006917924),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.7918525,
        size.height * 0.2088587,
        size.width * 0.7917650,
        size.height * 0.2095242,
        size.width * 0.7917650,
        size.height * 0.2111481)
    ..moveTo(size.width * 0.8601200, size.height * 0.2088786)
    ..cubicTo(
        size.width * 0.8601200,
        size.height * 0.2090226,
        size.width * 0.8607400,
        size.height * 0.2102592,
        size.width * 0.8615000,
        size.height * 0.2116249)
    ..lineTo(size.width * 0.8628825, size.height * 0.2141030)
    ..lineTo(size.width * 0.8616350, size.height * 0.2113617)
    ..cubicTo(
        size.width * 0.8604700,
        size.height * 0.2087991,
        size.width * 0.8601200,
        size.height * 0.2082280,
        size.width * 0.8601200,
        size.height * 0.2088786)
    ..moveTo(size.width * 0.1918600, size.height * 0.2208620)
    ..cubicTo(
        size.width * 0.1918625,
        size.height * 0.2227194,
        size.width * 0.1919500,
        size.height * 0.2233799,
        size.width * 0.1920525,
        size.height * 0.2223271)
    ..arcToPoint(Offset(size.width * 0.1920450, size.height * 0.2189500),
        radius: Radius.elliptical(
            size.width * 0.004657500, size.height * 0.009252040),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.1919375,
        size.height * 0.2181455,
        size.width * 0.1918550,
        size.height * 0.2190047,
        size.width * 0.1918600,
        size.height * 0.2208620)
    ..moveTo(size.width * 0.03879750, size.height * 0.2201966)
    ..cubicTo(
        size.width * 0.03944000,
        size.height * 0.2203902,
        size.width * 0.04049250,
        size.height * 0.2203902,
        size.width * 0.04113500,
        size.height * 0.2201966)
    ..cubicTo(
        size.width * 0.04177750,
        size.height * 0.2200078,
        size.width * 0.04125250,
        size.height * 0.2198489,
        size.width * 0.03996500,
        size.height * 0.2198489)
    ..cubicTo(
        size.width * 0.03868000,
        size.height * 0.2198489,
        size.width * 0.03815250,
        size.height * 0.2200078,
        size.width * 0.03879750,
        size.height * 0.2201966)
    ..moveTo(size.width * 0.1643075, size.height * 0.2231813)
    ..lineTo(size.width * 0.1626275, size.height * 0.2267718)
    ..lineTo(size.width * 0.1644350, size.height * 0.2234345)
    ..cubicTo(
        size.width * 0.1661150,
        size.height * 0.2203306,
        size.width * 0.1664375,
        size.height * 0.2195957,
        size.width * 0.1661150,
        size.height * 0.2195957)
    ..cubicTo(
        size.width * 0.1660450,
        size.height * 0.2195957,
        size.width * 0.1652325,
        size.height * 0.2212097,
        size.width * 0.1643075,
        size.height * 0.2231813)
    ..moveTo(size.width * 0.7892150, size.height * 0.2221284)
    ..cubicTo(
        size.width * 0.7892150,
        size.height * 0.2237524,
        size.width * 0.7893025,
        size.height * 0.2244178,
        size.width * 0.7894075,
        size.height * 0.2236083)
    ..arcToPoint(Offset(size.width * 0.7894075, size.height * 0.2206485),
        radius: Radius.elliptical(
            size.width * 0.003482500, size.height * 0.006917924),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.7893025,
        size.height * 0.2198390,
        size.width * 0.7892150,
        size.height * 0.2205045,
        size.width * 0.7892150,
        size.height * 0.2221284)
    ..moveTo(size.width * 0.05537750, size.height * 0.2210210)
    ..cubicTo(
        size.width * 0.05578750,
        size.height * 0.2212345,
        size.width * 0.05645750,
        size.height * 0.2212345,
        size.width * 0.05686750,
        size.height * 0.2210210)
    ..cubicTo(
        size.width * 0.05727500,
        size.height * 0.2208074,
        size.width * 0.05694000,
        size.height * 0.2206336,
        size.width * 0.05612250,
        size.height * 0.2206336)
    ..cubicTo(
        size.width * 0.05530500,
        size.height * 0.2206336,
        size.width * 0.05497000,
        size.height * 0.2208074,
        size.width * 0.05537750,
        size.height * 0.2210210)
    ..moveTo(size.width * 0.8898800, size.height * 0.2267718)
    ..cubicTo(
        size.width * 0.8906750,
        size.height * 0.2284007,
        size.width * 0.8914200,
        size.height * 0.2297317,
        size.width * 0.8915350,
        size.height * 0.2297317)
    ..cubicTo(
        size.width * 0.8916525,
        size.height * 0.2297317,
        size.width * 0.8911000,
        size.height * 0.2284007,
        size.width * 0.8903050,
        size.height * 0.2267718)
    ..cubicTo(
        size.width * 0.8895125,
        size.height * 0.2251479,
        size.width * 0.8887675,
        size.height * 0.2238169,
        size.width * 0.8886500,
        size.height * 0.2238169)
    ..cubicTo(
        size.width * 0.8885350,
        size.height * 0.2238169,
        size.width * 0.8890875,
        size.height * 0.2251479,
        size.width * 0.8898800,
        size.height * 0.2267718)
    ..moveTo(size.width * 0.8146250, size.height * 0.2249244)
    ..cubicTo(
        size.width * 0.8146250,
        size.height * 0.2250734,
        size.width * 0.8152475,
        size.height * 0.2263050,
        size.width * 0.8160075,
        size.height * 0.2276707)
    ..lineTo(size.width * 0.8173900, size.height * 0.2301538)
    ..lineTo(size.width * 0.8161400, size.height * 0.2274075)
    ..cubicTo(
        size.width * 0.8149775,
        size.height * 0.2248499,
        size.width * 0.8146250,
        size.height * 0.2242738,
        size.width * 0.8146250,
        size.height * 0.2249244)
    ..moveTo(size.width * 0.09663250, size.height * 0.2261262)
    ..cubicTo(
        size.width * 0.09775000,
        size.height * 0.2263050,
        size.width * 0.09947250,
        size.height * 0.2263000,
        size.width * 0.1004600,
        size.height * 0.2261212)
    ..cubicTo(
        size.width * 0.1014450,
        size.height * 0.2259474,
        size.width * 0.1005325,
        size.height * 0.2258034,
        size.width * 0.09842750,
        size.height * 0.2258084)
    ..cubicTo(
        size.width * 0.09632250,
        size.height * 0.2258084,
        size.width * 0.09551500,
        size.height * 0.2259524,
        size.width * 0.09663250,
        size.height * 0.2261262)
    ..moveTo(size.width * 0.7612675, size.height * 0.2305759)
    ..cubicTo(
        size.width * 0.7621800,
        size.height * 0.2324333,
        size.width * 0.7630225,
        size.height * 0.2339530,
        size.width * 0.7631400,
        size.height * 0.2339530)
    ..cubicTo(
        size.width * 0.7632575,
        size.height * 0.2339530,
        size.width * 0.7626050,
        size.height * 0.2324333,
        size.width * 0.7616925,
        size.height * 0.2305759)
    ..cubicTo(
        size.width * 0.7607800,
        size.height * 0.2287136,
        size.width * 0.7599350,
        size.height * 0.2271939,
        size.width * 0.7598200,
        size.height * 0.2271939)
    ..cubicTo(
        size.width * 0.7597025,
        size.height * 0.2271939,
        size.width * 0.7603525,
        size.height * 0.2287136,
        size.width * 0.7612675,
        size.height * 0.2305759)
    ..moveTo(size.width * 0.7967450, size.height * 0.2303624)
    ..lineTo(size.width * 0.7952800, size.height * 0.2335308)
    ..lineTo(size.width * 0.7968750, size.height * 0.2306206)
    ..cubicTo(
        size.width * 0.7977525,
        size.height * 0.2290165,
        size.width * 0.7984700,
        size.height * 0.2275912,
        size.width * 0.7984700,
        size.height * 0.2274522)
    ..cubicTo(
        size.width * 0.7984700,
        size.height * 0.2268066,
        size.width * 0.7981100,
        size.height * 0.2274125,
        size.width * 0.7967450,
        size.height * 0.2303624)
    ..moveTo(size.width * 0.7913800, size.height * 0.2436668)
    ..cubicTo(
        size.width * 0.7913825,
        size.height * 0.2469147,
        size.width * 0.7914600,
        size.height * 0.2481414,
        size.width * 0.7915525,
        size.height * 0.2463933)
    ..arcToPoint(Offset(size.width * 0.7915475, size.height * 0.2404785),
        radius: Radius.elliptical(
            size.width * 0.01576250, size.height * 0.03131192),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.7914550,
        size.height * 0.2389787,
        size.width * 0.7913800,
        size.height * 0.2404140,
        size.width * 0.7913800,
        size.height * 0.2436668)
    ..moveTo(size.width * 0.4950425, size.height * 0.2715372)
    ..cubicTo(
        size.width * 0.4950450,
        size.height * 0.2752519,
        size.width * 0.4951200,
        size.height * 0.2766673,
        size.width * 0.4952100,
        size.height * 0.2746808)
    ..cubicTo(
        size.width * 0.4953000,
        size.height * 0.2726943,
        size.width * 0.4952975,
        size.height * 0.2696550,
        size.width * 0.4952075,
        size.height * 0.2679268)
    ..cubicTo(
        size.width * 0.4951150,
        size.height * 0.2661936,
        size.width * 0.4950425,
        size.height * 0.2678225,
        size.width * 0.4950425,
        size.height * 0.2715372)
    ..moveTo(size.width * 0.1585875, size.height * 0.2680013)
    ..cubicTo(
        size.width * 0.1585875,
        size.height * 0.2681453,
        size.width * 0.1592100,
        size.height * 0.2693819,
        size.width * 0.1599700,
        size.height * 0.2707476)
    ..lineTo(size.width * 0.1613525, size.height * 0.2732257)
    ..lineTo(size.width * 0.1601025, size.height * 0.2704794)
    ..cubicTo(
        size.width * 0.1589400,
        size.height * 0.2679218,
        size.width * 0.1585875,
        size.height * 0.2673457,
        size.width * 0.1585875,
        size.height * 0.2680013)
    ..moveTo(size.width * 0.7888850, size.height * 0.3091214)
    ..cubicTo(
        size.width * 0.7888850,
        size.height * 0.3279334,
        size.width * 0.7889400,
        size.height * 0.3356310,
        size.width * 0.7890100,
        size.height * 0.3262250)
    ..cubicTo(
        size.width * 0.7890775,
        size.height * 0.3168190,
        size.width * 0.7890775,
        size.height * 0.3014238,
        size.width * 0.7890100,
        size.height * 0.2920178)
    ..cubicTo(
        size.width * 0.7889400,
        size.height * 0.2826118,
        size.width * 0.7888850,
        size.height * 0.2903094,
        size.width * 0.7888850,
        size.height * 0.3091214)
    ..moveTo(size.width * 0.4711950, size.height * 0.2960305)
    ..cubicTo(
        size.width * 0.4712000,
        size.height * 0.2978879,
        size.width * 0.4712875,
        size.height * 0.2985484,
        size.width * 0.4713900,
        size.height * 0.2974955)
    ..cubicTo(
        size.width * 0.4714900,
        size.height * 0.2964477,
        size.width * 0.4714875,
        size.height * 0.2949280,
        size.width * 0.4713800,
        size.height * 0.2941185)
    ..cubicTo(
        size.width * 0.4712750,
        size.height * 0.2933140,
        size.width * 0.4711900,
        size.height * 0.2941732,
        size.width * 0.4711950,
        size.height * 0.2960305)
    ..moveTo(size.width * 0.4954450, size.height * 0.3089029)
    ..cubicTo(
        size.width * 0.4967975,
        size.height * 0.3090718,
        size.width * 0.4989025,
        size.height * 0.3090668,
        size.width * 0.5001225,
        size.height * 0.3088979)
    ..cubicTo(
        size.width * 0.5013400,
        size.height * 0.3087291,
        size.width * 0.5002350,
        size.height * 0.3085900,
        size.width * 0.4976625,
        size.height * 0.3085900)
    ..cubicTo(
        size.width * 0.4950900,
        size.height * 0.3085950,
        size.width * 0.4940925,
        size.height * 0.3087341,
        size.width * 0.4954450,
        size.height * 0.3089029)
    ..moveTo(size.width * 0.08120750, size.height * 0.3111427)
    ..cubicTo(
        size.width * 0.08120750,
        size.height * 0.3113264,
        size.width * 0.08197250,
        size.height * 0.3116244,
        size.width * 0.08290750,
        size.height * 0.3117982)
    ..cubicTo(
        size.width * 0.08384250,
        size.height * 0.3119770,
        size.width * 0.08461000,
        size.height * 0.3118280,
        size.width * 0.08461000,
        size.height * 0.3114655)
    ..cubicTo(
        size.width * 0.08461000,
        size.height * 0.3111029,
        size.width * 0.08384250,
        size.height * 0.3108099,
        size.width * 0.08290750,
        size.height * 0.3108099)
    ..cubicTo(
        size.width * 0.08197250,
        size.height * 0.3108099,
        size.width * 0.08120750,
        size.height * 0.3109589,
        size.width * 0.08120750,
        size.height * 0.3111427)
    ..moveTo(size.width * 0.07005250, size.height * 0.3122501)
    ..cubicTo(
        size.width * 0.07058000,
        size.height * 0.3124538,
        size.width * 0.07134500,
        size.height * 0.3124488,
        size.width * 0.07175250,
        size.height * 0.3122352)
    ..cubicTo(
        size.width * 0.07216000,
        size.height * 0.3120217,
        size.width * 0.07172500,
        size.height * 0.3118578,
        size.width * 0.07079000,
        size.height * 0.3118677)
    ..cubicTo(
        size.width * 0.06985500,
        size.height * 0.3118777,
        size.width * 0.06952250,
        size.height * 0.3120515,
        size.width * 0.07005250,
        size.height * 0.3122501)
    ..moveTo(size.width * 0.4792650, size.height * 0.3141870)
    ..cubicTo(
        size.width * 0.4792650,
        size.height * 0.3158159,
        size.width * 0.4793525,
        size.height * 0.3164813,
        size.width * 0.4794600,
        size.height * 0.3156669)
    ..arcToPoint(Offset(size.width * 0.4794600, size.height * 0.3127120),
        radius: Radius.elliptical(
            size.width * 0.003470000, size.height * 0.006893093),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.4793525,
        size.height * 0.3118975,
        size.width * 0.4792650,
        size.height * 0.3125630,
        size.width * 0.4792650,
        size.height * 0.3141870)
    ..moveTo(size.width * 0.05772250, size.height * 0.3130944)
    ..cubicTo(
        size.width * 0.05825000,
        size.height * 0.3132980,
        size.width * 0.05901500,
        size.height * 0.3132930,
        size.width * 0.05942250,
        size.height * 0.3130795)
    ..cubicTo(
        size.width * 0.05983000,
        size.height * 0.3128659,
        size.width * 0.05939750,
        size.height * 0.3127021,
        size.width * 0.05846000,
        size.height * 0.3127120)
    ..cubicTo(
        size.width * 0.05752500,
        size.height * 0.3127219,
        size.width * 0.05719250,
        size.height * 0.3128957,
        size.width * 0.05772250,
        size.height * 0.3130944)
    ..moveTo(size.width * 0.04538750, size.height * 0.3139386)
    ..cubicTo(
        size.width * 0.04591250,
        size.height * 0.3141423,
        size.width * 0.04677500,
        size.height * 0.3141423,
        size.width * 0.04730000,
        size.height * 0.3139386)
    ..cubicTo(
        size.width * 0.04782750,
        size.height * 0.3137400,
        size.width * 0.04739500,
        size.height * 0.3135761,
        size.width * 0.04634250,
        size.height * 0.3135761)
    ..cubicTo(
        size.width * 0.04529000,
        size.height * 0.3135761,
        size.width * 0.04486000,
        size.height * 0.3137400,
        size.width * 0.04538750,
        size.height * 0.3139386)
    ..moveTo(size.width * 0.8857275, size.height * 0.3319262)
    ..cubicTo(
        size.width * 0.8857275,
        size.height * 0.3335502,
        size.width * 0.8858150,
        size.height * 0.3342157,
        size.width * 0.8859225,
        size.height * 0.3334012)
    ..arcToPoint(Offset(size.width * 0.8859225, size.height * 0.3304463),
        radius: Radius.elliptical(
            size.width * 0.003470000, size.height * 0.006893093),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8858150,
        size.height * 0.3296319,
        size.width * 0.8857275,
        size.height * 0.3302973,
        size.width * 0.8857275,
        size.height * 0.3319262)
    ..moveTo(size.width * 0.4640475, size.height * 0.3388938)
    ..lineTo(size.width * 0.4627975, size.height * 0.3416401)
    ..lineTo(size.width * 0.4641800, size.height * 0.3391570)
    ..cubicTo(
        size.width * 0.4649400,
        size.height * 0.3377913,
        size.width * 0.4655600,
        size.height * 0.3365597,
        size.width * 0.4655600,
        size.height * 0.3364107)
    ..cubicTo(
        size.width * 0.4655600,
        size.height * 0.3357602,
        size.width * 0.4652100,
        size.height * 0.3363362,
        size.width * 0.4640475,
        size.height * 0.3388938)
    ..moveTo(size.width * 0.8836025, size.height * 0.3403688)
    ..cubicTo(
        size.width * 0.8836025,
        size.height * 0.3419977,
        size.width * 0.8836900,
        size.height * 0.3426632,
        size.width * 0.8837975,
        size.height * 0.3418487)
    ..arcToPoint(Offset(size.width * 0.8837975, size.height * 0.3388938),
        radius: Radius.elliptical(
            size.width * 0.003550000, size.height * 0.007052011),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8836900,
        size.height * 0.3380794,
        size.width * 0.8836025,
        size.height * 0.3387448,
        size.width * 0.8836025,
        size.height * 0.3403688)
    ..moveTo(size.width * 0.4563150, size.height * 0.3451761)
    ..cubicTo(
        size.width * 0.4567225,
        size.height * 0.3453896,
        size.width * 0.4573925,
        size.height * 0.3453896,
        size.width * 0.4578025,
        size.height * 0.3451761)
    ..cubicTo(
        size.width * 0.4582100,
        size.height * 0.3449625,
        size.width * 0.4578775,
        size.height * 0.3447887,
        size.width * 0.4570575,
        size.height * 0.3447887)
    ..cubicTo(
        size.width * 0.4562400,
        size.height * 0.3447887,
        size.width * 0.4559050,
        size.height * 0.3449625,
        size.width * 0.4563150,
        size.height * 0.3451761)
    ..moveTo(size.width * 0.8861625, size.height * 0.3517712)
    ..cubicTo(
        size.width * 0.8861675,
        size.height * 0.3536286,
        size.width * 0.8862525,
        size.height * 0.3542940,
        size.width * 0.8863550,
        size.height * 0.3532412)
    ..arcToPoint(Offset(size.width * 0.8863475, size.height * 0.3498642),
        radius: Radius.elliptical(
            size.width * 0.004655000, size.height * 0.009247074),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8862400,
        size.height * 0.3490547,
        size.width * 0.8861575,
        size.height * 0.3499138,
        size.width * 0.8861625,
        size.height * 0.3517712)
    ..moveTo(size.width * 0.8840350, size.height * 0.3619072)
    ..cubicTo(
        size.width * 0.8840400,
        size.height * 0.3637646,
        size.width * 0.8841275,
        size.height * 0.3644251,
        size.width * 0.8842300,
        size.height * 0.3633772)
    ..arcToPoint(Offset(size.width * 0.8842200, size.height * 0.3600002),
        radius: Radius.elliptical(
            size.width * 0.004650000, size.height * 0.009237141),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8841150,
        size.height * 0.3591907,
        size.width * 0.8840300,
        size.height * 0.3600499,
        size.width * 0.8840350,
        size.height * 0.3619072)
    ..moveTo(size.width * 0.7913850, size.height * 0.3690884)
    ..cubicTo(
        size.width * 0.7913850,
        size.height * 0.3725697,
        size.width * 0.7914600,
        size.height * 0.3739950,
        size.width * 0.7915500,
        size.height * 0.3722518)
    ..cubicTo(
        size.width * 0.7916425,
        size.height * 0.3705137,
        size.width * 0.7916425,
        size.height * 0.3676631,
        size.width * 0.7915500,
        size.height * 0.3659199)
    ..cubicTo(
        size.width * 0.7914600,
        size.height * 0.3641768,
        size.width * 0.7913850,
        size.height * 0.3656021,
        size.width * 0.7913850,
        size.height * 0.3690884)
    ..moveTo(size.width * 0.8866075, size.height * 0.3775309)
    ..cubicTo(
        size.width * 0.8866075,
        size.height * 0.3800885,
        size.width * 0.8866875,
        size.height * 0.3811314,
        size.width * 0.8867850,
        size.height * 0.3798551)
    ..arcToPoint(Offset(size.width * 0.8867850, size.height * 0.3752117),
        radius: Radius.elliptical(
            size.width * 0.009375000, size.height * 0.01862327),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8866875,
        size.height * 0.3739304,
        size.width * 0.8866075,
        size.height * 0.3749783,
        size.width * 0.8866075,
        size.height * 0.3775309)
    ..moveTo(size.width * 0.7892375, size.height * 0.3864005)
    ..cubicTo(
        size.width * 0.7892400,
        size.height * 0.3887247,
        size.width * 0.7893225,
        size.height * 0.3895739,
        size.width * 0.7894200,
        size.height * 0.3882877)
    ..arcToPoint(Offset(size.width * 0.7894150, size.height * 0.3840664),
        radius: Radius.elliptical(
            size.width * 0.007632500, size.height * 0.01516182),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.7893150,
        size.height * 0.3830285,
        size.width * 0.7892350,
        size.height * 0.3840764,
        size.width * 0.7892375,
        size.height * 0.3864005)
    ..moveTo(size.width * 0.8845050, size.height * 0.3999136)
    ..cubicTo(
        size.width * 0.8845025,
        size.height * 0.4040951,
        size.width * 0.8845750,
        size.height * 0.4059128,
        size.width * 0.8846650,
        size.height * 0.4039511)
    ..cubicTo(
        size.width * 0.8847525,
        size.height * 0.4019895,
        size.width * 0.8847550,
        size.height * 0.3985727,
        size.width * 0.8846675,
        size.height * 0.3963528)
    ..cubicTo(
        size.width * 0.8845800,
        size.height * 0.3941280,
        size.width * 0.8845050,
        size.height * 0.3957320,
        size.width * 0.8845050,
        size.height * 0.3999136)
    ..moveTo(size.width * 0.7917750, size.height * 0.3999136)
    ..cubicTo(
        size.width * 0.7917775,
        size.height * 0.4017709,
        size.width * 0.7918650,
        size.height * 0.4024315,
        size.width * 0.7919675,
        size.height * 0.4013836)
    ..arcToPoint(Offset(size.width * 0.7919600, size.height * 0.3980066),
        radius: Radius.elliptical(
            size.width * 0.004655000, size.height * 0.009247074),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.7918525,
        size.height * 0.3971971,
        size.width * 0.7917700,
        size.height * 0.3980562,
        size.width * 0.7917750,
        size.height * 0.3999136)
    ..moveTo(size.width * 0.05290750, size.height * 0.4047705)
    ..lineTo(size.width * 0.05165750, size.height * 0.4075168)
    ..lineTo(size.width * 0.05304000, size.height * 0.4050337)
    ..cubicTo(
        size.width * 0.05380000,
        size.height * 0.4036730,
        size.width * 0.05442250,
        size.height * 0.4024364,
        size.width * 0.05442250,
        size.height * 0.4022924)
    ..cubicTo(
        size.width * 0.05442250,
        size.height * 0.4016369,
        size.width * 0.05407000,
        size.height * 0.4022129,
        size.width * 0.05290750,
        size.height * 0.4047705)
    ..moveTo(size.width * 0.5313600, size.height * 0.4161382)
    ..cubicTo(
        size.width * 0.5318900,
        size.height * 0.4163368,
        size.width * 0.5326550,
        size.height * 0.4163319,
        size.width * 0.5330625,
        size.height * 0.4161183)
    ..cubicTo(
        size.width * 0.5334675,
        size.height * 0.4159097,
        size.width * 0.5330350,
        size.height * 0.4157409,
        size.width * 0.5321000,
        size.height * 0.4157508)
    ..cubicTo(
        size.width * 0.5311650,
        size.height * 0.4157607,
        size.width * 0.5308325,
        size.height * 0.4159346,
        size.width * 0.5313600,
        size.height * 0.4161382)
    ..moveTo(size.width * 0.5441175, size.height * 0.4169824)
    ..cubicTo(
        size.width * 0.5446450,
        size.height * 0.4171811,
        size.width * 0.5454100,
        size.height * 0.4171761,
        size.width * 0.5458175,
        size.height * 0.4169626)
    ..cubicTo(
        size.width * 0.5462225,
        size.height * 0.4167540,
        size.width * 0.5457900,
        size.height * 0.4165901,
        size.width * 0.5448550,
        size.height * 0.4165951)
    ..cubicTo(
        size.width * 0.5439200,
        size.height * 0.4166050,
        size.width * 0.5435875,
        size.height * 0.4167788,
        size.width * 0.5441175,
        size.height * 0.4169824)
    ..moveTo(size.width * 0.5568675, size.height * 0.4178267)
    ..cubicTo(
        size.width * 0.5573925,
        size.height * 0.4180253,
        size.width * 0.5582525,
        size.height * 0.4180253,
        size.width * 0.5587800,
        size.height * 0.4178267)
    ..cubicTo(
        size.width * 0.5593050,
        size.height * 0.4176231,
        size.width * 0.5588750,
        size.height * 0.4174592,
        size.width * 0.5578225,
        size.height * 0.4174592)
    ..cubicTo(
        size.width * 0.5567700,
        size.height * 0.4174592,
        size.width * 0.5563400,
        size.height * 0.4176231,
        size.width * 0.5568675,
        size.height * 0.4178267)
    ..moveTo(size.width * 0.5696225, size.height * 0.4186709)
    ..cubicTo(
        size.width * 0.5701475,
        size.height * 0.4188696,
        size.width * 0.5710075,
        size.height * 0.4188696,
        size.width * 0.5715350,
        size.height * 0.4186709)
    ..cubicTo(
        size.width * 0.5720600,
        size.height * 0.4184673,
        size.width * 0.5716300,
        size.height * 0.4183034,
        size.width * 0.5705775,
        size.height * 0.4183034)
    ..cubicTo(
        size.width * 0.5695250,
        size.height * 0.4183034,
        size.width * 0.5690950,
        size.height * 0.4184673,
        size.width * 0.5696225,
        size.height * 0.4186709)
    ..moveTo(size.width * 0.8185575, size.height * 0.4364152)
    ..cubicTo(
        size.width * 0.8192025,
        size.height * 0.4366089,
        size.width * 0.8202550,
        size.height * 0.4366089,
        size.width * 0.8208975,
        size.height * 0.4364152)
    ..cubicTo(
        size.width * 0.8215400,
        size.height * 0.4362215,
        size.width * 0.8210150,
        size.height * 0.4360626,
        size.width * 0.8197275,
        size.height * 0.4360626)
    ..cubicTo(
        size.width * 0.8184425,
        size.height * 0.4360626,
        size.width * 0.8179150,
        size.height * 0.4362215,
        size.width * 0.8185575,
        size.height * 0.4364152)
    ..moveTo(size.width * 0.8211900, size.height * 0.4436212)
    ..lineTo(size.width * 0.8199400, size.height * 0.4463675)
    ..lineTo(size.width * 0.8213225, size.height * 0.4438844)
    ..cubicTo(
        size.width * 0.8226100,
        size.height * 0.4415751,
        size.width * 0.8229000,
        size.height * 0.4408798,
        size.width * 0.8225700,
        size.height * 0.4408798)
    ..cubicTo(
        size.width * 0.8224975,
        size.height * 0.4408798,
        size.width * 0.8218750,
        size.height * 0.4421114,
        size.width * 0.8211900,
        size.height * 0.4436212)
    ..moveTo(size.width * 0.2960950, size.height * 0.4538515)
    ..cubicTo(
        size.width * 0.2959725,
        size.height * 0.4544822,
        size.width * 0.2959325,
        size.height * 0.4572385,
        size.width * 0.2960075,
        size.height * 0.4599749)
    ..lineTo(size.width * 0.2961425, size.height * 0.4649460)
    ..lineTo(size.width * 0.2963500, size.height * 0.4588227)
    ..cubicTo(
        size.width * 0.2965575,
        size.height * 0.4526795,
        size.width * 0.2965075,
        size.height * 0.4517161,
        size.width * 0.2960950,
        size.height * 0.4538515)
    ..moveTo(size.width * 0.7036525, size.height * 0.4588624)
    ..lineTo(size.width * 0.7038600, size.height * 0.4649460)
    ..lineTo(size.width * 0.7039925, size.height * 0.4595428)
    ..cubicTo(
        size.width * 0.7040650,
        size.height * 0.4565730,
        size.width * 0.7039725,
        size.height * 0.4538317,
        size.width * 0.7037850,
        size.height * 0.4534592)
    ..cubicTo(
        size.width * 0.7035975,
        size.height * 0.4530867,
        size.width * 0.7035375,
        size.height * 0.4555152,
        size.width * 0.7036525,
        size.height * 0.4588624)
    ..moveTo(size.width * 0.8495975, size.height * 0.4573477)
    ..cubicTo(
        size.width * 0.8496025,
        size.height * 0.4592051,
        size.width * 0.8496900,
        size.height * 0.4598656,
        size.width * 0.8497900,
        size.height * 0.4588128)
    ..arcToPoint(Offset(size.width * 0.8497825, size.height * 0.4554358),
        radius: Radius.elliptical(
            size.width * 0.004657500, size.height * 0.009252040),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8496750,
        size.height * 0.4546312,
        size.width * 0.8495925,
        size.height * 0.4554904,
        size.width * 0.8495975,
        size.height * 0.4573477)
    ..moveTo(size.width * 0.1886700, size.height * 0.4820149)
    ..cubicTo(
        size.width * 0.1891950,
        size.height * 0.4822135,
        size.width * 0.1900575,
        size.height * 0.4822135,
        size.width * 0.1905825,
        size.height * 0.4820149)
    ..cubicTo(
        size.width * 0.1911075,
        size.height * 0.4818113,
        size.width * 0.1906775,
        size.height * 0.4816474,
        size.width * 0.1896250,
        size.height * 0.4816474)
    ..cubicTo(
        size.width * 0.1885725,
        size.height * 0.4816474,
        size.width * 0.1881425,
        size.height * 0.4818113,
        size.width * 0.1886700,
        size.height * 0.4820149)
    ..moveTo(size.width * 0.2960175, size.height * 0.4932435)
    ..cubicTo(
        size.width * 0.2960175,
        size.height * 0.4948674,
        size.width * 0.2961050,
        size.height * 0.4955329,
        size.width * 0.2962125,
        size.height * 0.4947184)
    ..arcToPoint(Offset(size.width * 0.2962125, size.height * 0.4917635),
        radius: Radius.elliptical(
            size.width * 0.003550000, size.height * 0.007052011),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.2961050,
        size.height * 0.4909491,
        size.width * 0.2960175,
        size.height * 0.4916146,
        size.width * 0.2960175,
        size.height * 0.4932435)
    ..moveTo(size.width * 0.04165250, size.height * 0.5580624)
    ..lineTo(size.width * 0.03932750, size.height * 0.5629193)
    ..lineTo(size.width * 0.04177250, size.height * 0.5583057)
    ..cubicTo(
        size.width * 0.04311750,
        size.height * 0.5557680,
        size.width * 0.04421750,
        size.height * 0.5535779,
        size.width * 0.04421750,
        size.height * 0.5534488)
    ..cubicTo(
        size.width * 0.04421750,
        size.height * 0.5528230,
        size.width * 0.04381000,
        size.height * 0.5535531,
        size.width * 0.04165250,
        size.height * 0.5580624)
    ..moveTo(size.width * 0.04101500, size.height * 0.5660878)
    ..lineTo(size.width * 0.03847750, size.height * 0.5713668)
    ..lineTo(size.width * 0.04113500, size.height * 0.5663262)
    ..cubicTo(
        size.width * 0.04360250,
        size.height * 0.5616430,
        size.width * 0.04398750,
        size.height * 0.5608087,
        size.width * 0.04367250,
        size.height * 0.5608087)
    ..cubicTo(
        size.width * 0.04360750,
        size.height * 0.5608087,
        size.width * 0.04241250,
        size.height * 0.5631825,
        size.width * 0.04101500,
        size.height * 0.5660878)
    ..moveTo(size.width * 0.3122400, size.height * 0.5656209)
    ..lineTo(size.width * 0.3134675, size.height * 0.5683226)
    ..lineTo(size.width * 0.3107525, size.height * 0.5686255)
    ..lineTo(size.width * 0.3080350, size.height * 0.5689284)
    ..lineTo(size.width * 0.3109050, size.height * 0.5690923)
    ..cubicTo(
        size.width * 0.3141625,
        size.height * 0.5692761,
        size.width * 0.3143275,
        size.height * 0.5688738,
        size.width * 0.3123925,
        size.height * 0.5654024)
    ..lineTo(size.width * 0.3110125, size.height * 0.5629193)
    ..lineTo(size.width * 0.3122400, size.height * 0.5656209)
    ..moveTo(size.width * 0.3635200, size.height * 0.5686205)
    ..cubicTo(
        size.width * 0.3635200,
        size.height * 0.5689682,
        size.width * 0.3651950,
        size.height * 0.5691966,
        size.width * 0.3672400,
        size.height * 0.5691271)
    ..cubicTo(
        size.width * 0.3711300,
        size.height * 0.5689980,
        size.width * 0.3706950,
        size.height * 0.5686255,
        size.width * 0.3661775,
        size.height * 0.5682232)
    ..cubicTo(
        size.width * 0.3647150,
        size.height * 0.5680941,
        size.width * 0.3635200,
        size.height * 0.5682729,
        size.width * 0.3635200,
        size.height * 0.5686205)
    ..moveTo(size.width * 0.6326525, size.height * 0.5684120)
    ..lineTo(size.width * 0.6290400, size.height * 0.5689384)
    ..lineTo(size.width * 0.6327600, size.height * 0.5690973)
    ..cubicTo(
        size.width * 0.6350100,
        size.height * 0.5691917,
        size.width * 0.6364800,
        size.height * 0.5689235,
        size.width * 0.6364800,
        size.height * 0.5684120)
    ..cubicTo(
        size.width * 0.6364800,
        size.height * 0.5679451,
        size.width * 0.6364325,
        size.height * 0.5676372,
        size.width * 0.6363725,
        size.height * 0.5677217)
    ..cubicTo(
        size.width * 0.6363150,
        size.height * 0.5678110,
        size.width * 0.6346400,
        size.height * 0.5681190,
        size.width * 0.6326525,
        size.height * 0.5684120)
    ..moveTo(size.width * 0.6873850, size.height * 0.5690228)
    ..cubicTo(
        size.width * 0.6881400,
        size.height * 0.5692065,
        size.width * 0.6894775,
        size.height * 0.5692115,
        size.width * 0.6903600,
        size.height * 0.5690278)
    ..cubicTo(
        size.width * 0.6912425,
        size.height * 0.5688440,
        size.width * 0.6906250,
        size.height * 0.5686901,
        size.width * 0.6889875,
        size.height * 0.5686901)
    ..cubicTo(
        size.width * 0.6873500,
        size.height * 0.5686851,
        size.width * 0.6866300,
        size.height * 0.5688341,
        size.width * 0.6873850,
        size.height * 0.5690228)
    ..moveTo(size.width * 0.02826000, size.height * 0.5855156)
    ..lineTo(size.width * 0.02572250, size.height * 0.5907946)
    ..lineTo(size.width * 0.02838000, size.height * 0.5857539)
    ..cubicTo(
        size.width * 0.03084750,
        size.height * 0.5810708,
        size.width * 0.03123250,
        size.height * 0.5802365,
        size.width * 0.03091750,
        size.height * 0.5802365)
    ..cubicTo(
        size.width * 0.03085250,
        size.height * 0.5802365,
        size.width * 0.02965500,
        size.height * 0.5826103,
        size.width * 0.02826000,
        size.height * 0.5855156)
    ..moveTo(size.width * 0.6242575, size.height * 0.5899504)
    ..cubicTo(
        size.width * 0.6242625,
        size.height * 0.5918077,
        size.width * 0.6243475,
        size.height * 0.5924683,
        size.width * 0.6244500,
        size.height * 0.5914154)
    ..arcToPoint(Offset(size.width * 0.6244425, size.height * 0.5880384),
        radius: Radius.elliptical(
            size.width * 0.004657500, size.height * 0.009252040),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.6243350,
        size.height * 0.5872289,
        size.width * 0.6242525,
        size.height * 0.5880881,
        size.width * 0.6242575,
        size.height * 0.5899504)
    ..moveTo(size.width * 0.3755250, size.height * 0.5903725)
    ..cubicTo(
        size.width * 0.3755250,
        size.height * 0.5919965,
        size.width * 0.3756125,
        size.height * 0.5926619,
        size.width * 0.3757175,
        size.height * 0.5918475)
    ..arcToPoint(Offset(size.width * 0.3757175, size.height * 0.5888926),
        radius: Radius.elliptical(
            size.width * 0.003470000, size.height * 0.006893093),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.3756125,
        size.height * 0.5880781,
        size.width * 0.3755250,
        size.height * 0.5887436,
        size.width * 0.3755250,
        size.height * 0.5903725)
    ..moveTo(size.width * 0.02719750, size.height * 0.5943802)
    ..lineTo(size.width * 0.02444750, size.height * 0.6000814)
    ..lineTo(size.width * 0.02731750, size.height * 0.5946186)
    ..cubicTo(
        size.width * 0.02998250,
        size.height * 0.5895432,
        size.width * 0.03038250,
        size.height * 0.5886790,
        size.width * 0.03006750,
        size.height * 0.5886790)
    ..cubicTo(
        size.width * 0.03000250,
        size.height * 0.5886790,
        size.width * 0.02871250,
        size.height * 0.5912466,
        size.width * 0.02719750,
        size.height * 0.5943802)
    ..moveTo(size.width * 0.4890675, size.height * 0.6002851)
    ..arcToPoint(Offset(size.width * 0.4937450, size.height * 0.6002801),
        radius: Radius.elliptical(
            size.width * 0.04227500, size.height * 0.08397853),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.4949625,
        size.height * 0.6001112,
        size.width * 0.4938575,
        size.height * 0.5999772,
        size.width * 0.4912850,
        size.height * 0.5999772)
    ..cubicTo(
        size.width * 0.4887125,
        size.height * 0.5999772,
        size.width * 0.4877150,
        size.height * 0.6001162,
        size.width * 0.4890675,
        size.height * 0.6002851)
    ..moveTo(size.width * 0.01465500, size.height * 0.6133859)
    ..lineTo(size.width * 0.01211750, size.height * 0.6186650)
    ..lineTo(size.width * 0.01477500, size.height * 0.6136243)
    ..cubicTo(
        size.width * 0.01724250,
        size.height * 0.6089412,
        size.width * 0.01762750,
        size.height * 0.6081068,
        size.width * 0.01731250,
        size.height * 0.6081068)
    ..cubicTo(
        size.width * 0.01724750,
        size.height * 0.6081068,
        size.width * 0.01605000,
        size.height * 0.6104807,
        size.width * 0.01465500,
        size.height * 0.6133859)
    ..moveTo(size.width * 0.01338000, size.height * 0.6226777)
    ..lineTo(size.width * 0.01084250, size.height * 0.6279568)
    ..lineTo(size.width * 0.01350000, size.height * 0.6229111)
    ..cubicTo(
        size.width * 0.01596750,
        size.height * 0.6182329,
        size.width * 0.01635000,
        size.height * 0.6173986,
        size.width * 0.01603750,
        size.height * 0.6173986)
    ..cubicTo(
        size.width * 0.01597000,
        size.height * 0.6173986,
        size.width * 0.01477500,
        size.height * 0.6197724,
        size.width * 0.01338000,
        size.height * 0.6226777)
    ..moveTo(size.width * 0.5856450, size.height * 0.6226777)
    ..lineTo(size.width * 0.5843975, size.height * 0.6254190)
    ..lineTo(size.width * 0.5857775, size.height * 0.6229409)
    ..cubicTo(
        size.width * 0.5870650,
        size.height * 0.6206266,
        size.width * 0.5873550,
        size.height * 0.6199314,
        size.width * 0.5870275,
        size.height * 0.6199314)
    ..cubicTo(
        size.width * 0.5869525,
        size.height * 0.6199314,
        size.width * 0.5863325,
        size.height * 0.6211680,
        size.width * 0.5856450,
        size.height * 0.6226777)
    ..moveTo(size.width * 0.3193025, size.height * 0.6353961)
    ..cubicTo(
        size.width * 0.3193025,
        size.height * 0.6355451,
        size.width * 0.3199250,
        size.height * 0.6367767,
        size.width * 0.3206850,
        size.height * 0.6381424)
    ..lineTo(size.width * 0.3220675, size.height * 0.6406255)
    ..lineTo(size.width * 0.3208175, size.height * 0.6378792)
    ..cubicTo(
        size.width * 0.3196525,
        size.height * 0.6353216,
        size.width * 0.3193025,
        size.height * 0.6347456,
        size.width * 0.3193025,
        size.height * 0.6353961)
    ..moveTo(size.width * 0.6791825, size.height * 0.6378792)
    ..lineTo(size.width * 0.6779325, size.height * 0.6406255)
    ..lineTo(size.width * 0.6793150, size.height * 0.6381424)
    ..cubicTo(
        size.width * 0.6806025,
        size.height * 0.6358282,
        size.width * 0.6808925,
        size.height * 0.6351329,
        size.width * 0.6805650,
        size.height * 0.6351329)
    ..cubicTo(
        size.width * 0.6804900,
        size.height * 0.6351329,
        size.width * 0.6798700,
        size.height * 0.6363695,
        size.width * 0.6791825,
        size.height * 0.6378792)
    ..moveTo(size.width * 0.001700000, size.height * 0.6397813)
    ..cubicTo(
        size.width * 0.0006675000,
        size.height * 0.6418671,
        size.width * -0.00008250000,
        size.height * 0.6435804,
        size.width * 0.00003500000,
        size.height * 0.6435804)
    ..cubicTo(
        size.width * 0.0001525000,
        size.height * 0.6435804,
        size.width * 0.001092500,
        size.height * 0.6418671,
        size.width * 0.002125000,
        size.height * 0.6397813)
    ..cubicTo(
        size.width * 0.003157500,
        size.height * 0.6376905,
        size.width * 0.003907500,
        size.height * 0.6359772,
        size.width * 0.003790000,
        size.height * 0.6359772)
    ..cubicTo(
        size.width * 0.003675000,
        size.height * 0.6359772,
        size.width * 0.002732500,
        size.height * 0.6376905,
        size.width * 0.001700000,
        size.height * 0.6397813)
    ..moveTo(size.width * 0.001035000, size.height * 0.6480153)
    ..lineTo(size.width * -0.0002125000, size.height * 0.6507566)
    ..lineTo(size.width * 0.001170000, size.height * 0.6482785)
    ..cubicTo(
        size.width * 0.001930000,
        size.height * 0.6469128,
        size.width * 0.002550000,
        size.height * 0.6456762,
        size.width * 0.002550000,
        size.height * 0.6455322)
    ..cubicTo(
        size.width * 0.002550000,
        size.height * 0.6448816,
        size.width * 0.002200000,
        size.height * 0.6454527,
        size.width * 0.001035000,
        size.height * 0.6480153)
    ..moveTo(size.width * 0.8691875, size.height * 0.6625811)
    ..cubicTo(
        size.width * 0.8691900,
        size.height * 0.6658340,
        size.width * 0.8692650,
        size.height * 0.6670607,
        size.width * 0.8693575,
        size.height * 0.6653076)
    ..arcToPoint(Offset(size.width * 0.8693550, size.height * 0.6593978),
        radius: Radius.elliptical(
            size.width * 0.01595500, size.height * 0.03169432),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8692600,
        size.height * 0.6578980,
        size.width * 0.8691850,
        size.height * 0.6593332,
        size.width * 0.8691875,
        size.height * 0.6625811)
    ..moveTo(size.width * 0.3436725, size.height * 0.6798983)
    ..cubicTo(
        size.width * 0.3436725,
        size.height * 0.6829177,
        size.width * 0.3437500,
        size.height * 0.6841494,
        size.width * 0.3438450,
        size.height * 0.6826396)
    ..arcToPoint(Offset(size.width * 0.3438450, size.height * 0.6771520),
        radius: Radius.elliptical(
            size.width * 0.01342500, size.height * 0.02666852),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.3437500,
        size.height * 0.6756423,
        size.width * 0.3436725,
        size.height * 0.6768788,
        size.width * 0.3436725,
        size.height * 0.6798983)
    ..moveTo(size.width * 0.4556800, size.height * 0.6779615)
    ..cubicTo(
        size.width * 0.4562100,
        size.height * 0.6781651,
        size.width * 0.4569750,
        size.height * 0.6781552,
        size.width * 0.4573825,
        size.height * 0.6779416)
    ..cubicTo(
        size.width * 0.4577875,
        size.height * 0.6777330,
        size.width * 0.4573550,
        size.height * 0.6775691,
        size.width * 0.4564200,
        size.height * 0.6775741)
    ..cubicTo(
        size.width * 0.4554850,
        size.height * 0.6775840,
        size.width * 0.4551525,
        size.height * 0.6777579,
        size.width * 0.4556800,
        size.height * 0.6779615)
    ..moveTo(size.width * 0.4393075, size.height * 0.6796351)
    ..cubicTo(
        size.width * 0.4397150,
        size.height * 0.6798486,
        size.width * 0.4403850,
        size.height * 0.6798486,
        size.width * 0.4407950,
        size.height * 0.6796351)
    ..cubicTo(
        size.width * 0.4412050,
        size.height * 0.6794215,
        size.width * 0.4408700,
        size.height * 0.6792477,
        size.width * 0.4400500,
        size.height * 0.6792477)
    ..cubicTo(
        size.width * 0.4392325,
        size.height * 0.6792477,
        size.width * 0.4388975,
        size.height * 0.6794215,
        size.width * 0.4393075,
        size.height * 0.6796351)
    ..moveTo(size.width * 0.6441600, size.height * 0.7326195)
    ..lineTo(size.width * 0.6426975, size.height * 0.7360611)
    ..lineTo(size.width * 0.6443500, size.height * 0.7329225)
    ..cubicTo(
        size.width * 0.6452600,
        size.height * 0.7311992,
        size.width * 0.6459175,
        size.height * 0.7296497,
        size.width * 0.6458125,
        size.height * 0.7294809)
    ..cubicTo(
        size.width * 0.6457075,
        size.height * 0.7293120,
        size.width * 0.6449625,
        size.height * 0.7307274,
        size.width * 0.6441600,
        size.height * 0.7326195)
    ..moveTo(size.width * 0.5426225, size.height * 0.7370692)
    ..cubicTo(
        size.width * 0.5430325,
        size.height * 0.7372778,
        size.width * 0.5437025,
        size.height * 0.7372778,
        size.width * 0.5441125,
        size.height * 0.7370692)
    ..cubicTo(
        size.width * 0.5445200,
        size.height * 0.7368557,
        size.width * 0.5441850,
        size.height * 0.7366819,
        size.width * 0.5433675,
        size.height * 0.7366819)
    ..cubicTo(
        size.width * 0.5425500,
        size.height * 0.7366819,
        size.width * 0.5422150,
        size.height * 0.7368557,
        size.width * 0.5426225,
        size.height * 0.7370692)
    ..moveTo(size.width * 0.02676250, size.height * 0.7489434)
    ..lineTo(size.width * 0.02529750, size.height * 0.7521119)
    ..lineTo(size.width * 0.02689250, size.height * 0.7491967)
    ..cubicTo(
        size.width * 0.02777000,
        size.height * 0.7475976,
        size.width * 0.02848750,
        size.height * 0.7461723,
        size.width * 0.02848750,
        size.height * 0.7460332)
    ..cubicTo(
        size.width * 0.02848750,
        size.height * 0.7453876,
        size.width * 0.02812750,
        size.height * 0.7459935,
        size.width * 0.02676250,
        size.height * 0.7489434)
    ..moveTo(size.width * 0.8234525, size.height * 0.7835331)
    ..cubicTo(
        size.width * 0.8239825,
        size.height * 0.7837367,
        size.width * 0.8247475,
        size.height * 0.7837267,
        size.width * 0.8251550,
        size.height * 0.7835182)
    ..cubicTo(
        size.width * 0.8255600,
        size.height * 0.7833046,
        size.width * 0.8251275,
        size.height * 0.7831407,
        size.width * 0.8241925,
        size.height * 0.7831507)
    ..cubicTo(
        size.width * 0.8232575,
        size.height * 0.7831606,
        size.width * 0.8229250,
        size.height * 0.7833344,
        size.width * 0.8234525,
        size.height * 0.7835331)
    ..moveTo(size.width * 0.4512125, size.height * 0.7902772)
    ..cubicTo(
        size.width * 0.4516200,
        size.height * 0.7904907,
        size.width * 0.4522900,
        size.height * 0.7904907,
        size.width * 0.4527000,
        size.height * 0.7902772)
    ..cubicTo(
        size.width * 0.4531100,
        size.height * 0.7900636,
        size.width * 0.4527750,
        size.height * 0.7898898,
        size.width * 0.4519550,
        size.height * 0.7898898)
    ..cubicTo(
        size.width * 0.4511375,
        size.height * 0.7898898,
        size.width * 0.4508025,
        size.height * 0.7900636,
        size.width * 0.4512125,
        size.height * 0.7902772)
    ..moveTo(size.width * 0.9588650, size.height * 0.7995689)
    ..cubicTo(
        size.width * 0.9592750,
        size.height * 0.7997775,
        size.width * 0.9599425,
        size.height * 0.7997775,
        size.width * 0.9603525,
        size.height * 0.7995689)
    ..cubicTo(
        size.width * 0.9607625,
        size.height * 0.7993554,
        size.width * 0.9604275,
        size.height * 0.7991816,
        size.width * 0.9596100,
        size.height * 0.7991816)
    ..cubicTo(
        size.width * 0.9587900,
        size.height * 0.7991816,
        size.width * 0.9584550,
        size.height * 0.7993554,
        size.width * 0.9588650,
        size.height * 0.7995689)
    ..moveTo(size.width * 0.9490850, size.height * 0.8004132)
    ..cubicTo(
        size.width * 0.9494950,
        size.height * 0.8006218,
        size.width * 0.9501650,
        size.height * 0.8006218,
        size.width * 0.9505750,
        size.height * 0.8004132)
    ..cubicTo(
        size.width * 0.9509825,
        size.height * 0.8001996,
        size.width * 0.9506475,
        size.height * 0.8000258,
        size.width * 0.9498300,
        size.height * 0.8000258)
    ..cubicTo(
        size.width * 0.9490125,
        size.height * 0.8000258,
        size.width * 0.9486775,
        size.height * 0.8001996,
        size.width * 0.9490850,
        size.height * 0.8004132)
    ..moveTo(size.width * 0.9397325, size.height * 0.8012574)
    ..cubicTo(
        size.width * 0.9401425,
        size.height * 0.8014660,
        size.width * 0.9408100,
        size.height * 0.8014660,
        size.width * 0.9412200,
        size.height * 0.8012574)
    ..cubicTo(
        size.width * 0.9416300,
        size.height * 0.8010439,
        size.width * 0.9412950,
        size.height * 0.8008701,
        size.width * 0.9404750,
        size.height * 0.8008701)
    ..cubicTo(
        size.width * 0.9396575,
        size.height * 0.8008701,
        size.width * 0.9393225,
        size.height * 0.8010439,
        size.width * 0.9397325,
        size.height * 0.8012574)
    ..moveTo(size.width * 0.9308025, size.height * 0.8021017)
    ..cubicTo(
        size.width * 0.9312125,
        size.height * 0.8023152,
        size.width * 0.9318825,
        size.height * 0.8023152,
        size.width * 0.9322925,
        size.height * 0.8021017)
    ..cubicTo(
        size.width * 0.9327000,
        size.height * 0.8018882,
        size.width * 0.9323650,
        size.height * 0.8017143,
        size.width * 0.9315475,
        size.height * 0.8017143)
    ..cubicTo(
        size.width * 0.9307300,
        size.height * 0.8017143,
        size.width * 0.9303950,
        size.height * 0.8018882,
        size.width * 0.9308025,
        size.height * 0.8021017)
    ..moveTo(size.width * 0.9637600, size.height * 0.8038051)
    ..cubicTo(
        size.width * 0.9642875,
        size.height * 0.8040087,
        size.width * 0.9650525,
        size.height * 0.8039988,
        size.width * 0.9654600,
        size.height * 0.8037902)
    ..cubicTo(
        size.width * 0.9658675,
        size.height * 0.8035767,
        size.width * 0.9654325,
        size.height * 0.8034128,
        size.width * 0.9644975,
        size.height * 0.8034227)
    ..cubicTo(
        size.width * 0.9635625,
        size.height * 0.8034277,
        size.width * 0.9632300,
        size.height * 0.8036015,
        size.width * 0.9637600,
        size.height * 0.8038051)
    ..moveTo(size.width * 0.9533375, size.height * 0.8046345)
    ..cubicTo(
        size.width * 0.9537475,
        size.height * 0.8048480,
        size.width * 0.9544175,
        size.height * 0.8048480,
        size.width * 0.9548250,
        size.height * 0.8046345)
    ..cubicTo(
        size.width * 0.9552350,
        size.height * 0.8044209,
        size.width * 0.9549000,
        size.height * 0.8042471,
        size.width * 0.9540825,
        size.height * 0.8042471)
    ..cubicTo(
        size.width * 0.9532625,
        size.height * 0.8042471,
        size.width * 0.9529275,
        size.height * 0.8044209,
        size.width * 0.9533375,
        size.height * 0.8046345)
    ..moveTo(size.width * 0.9439850, size.height * 0.8054787)
    ..cubicTo(
        size.width * 0.9443925,
        size.height * 0.8056923,
        size.width * 0.9450625,
        size.height * 0.8056923,
        size.width * 0.9454725,
        size.height * 0.8054787)
    ..cubicTo(
        size.width * 0.9458800,
        size.height * 0.8052652,
        size.width * 0.9455475,
        size.height * 0.8050914,
        size.width * 0.9447275,
        size.height * 0.8050914)
    ..cubicTo(
        size.width * 0.9439100,
        size.height * 0.8050914,
        size.width * 0.9435750,
        size.height * 0.8052652,
        size.width * 0.9439850,
        size.height * 0.8054787)
    ..moveTo(size.width * 0.4771475, size.height * 0.8063230)
    ..cubicTo(
        size.width * 0.4775575,
        size.height * 0.8065365,
        size.width * 0.4782250,
        size.height * 0.8065365,
        size.width * 0.4786350,
        size.height * 0.8063230)
    ..cubicTo(
        size.width * 0.4790450,
        size.height * 0.8061094,
        size.width * 0.4787100,
        size.height * 0.8059356,
        size.width * 0.4778900,
        size.height * 0.8059356)
    ..cubicTo(
        size.width * 0.4770725,
        size.height * 0.8059356,
        size.width * 0.4767375,
        size.height * 0.8061094,
        size.width * 0.4771475,
        size.height * 0.8063230)
    ..moveTo(size.width * 0.4896975, size.height * 0.8071970)
    ..cubicTo(
        size.width * 0.4904625,
        size.height * 0.8073857,
        size.width * 0.4916100,
        size.height * 0.8073808,
        size.width * 0.4922475,
        size.height * 0.8071871)
    ..cubicTo(
        size.width * 0.4928875,
        size.height * 0.8069984,
        size.width * 0.4922625,
        size.height * 0.8068444,
        size.width * 0.4908600,
        size.height * 0.8068494)
    ..cubicTo(
        size.width * 0.4894550,
        size.height * 0.8068544,
        size.width * 0.4889325,
        size.height * 0.8070083,
        size.width * 0.4896975,
        size.height * 0.8071970)
    ..moveTo(size.width * 0.5120100, size.height * 0.8071921)
    ..cubicTo(
        size.width * 0.5126550,
        size.height * 0.8073857,
        size.width * 0.5137075,
        size.height * 0.8073857,
        size.width * 0.5143500,
        size.height * 0.8071921)
    ..cubicTo(
        size.width * 0.5149925,
        size.height * 0.8069984,
        size.width * 0.5144675,
        size.height * 0.8068395,
        size.width * 0.5131800,
        size.height * 0.8068395)
    ..cubicTo(
        size.width * 0.5118950,
        size.height * 0.8068395,
        size.width * 0.5113675,
        size.height * 0.8069984,
        size.width * 0.5120100,
        size.height * 0.8071921)
    ..moveTo(size.width * 0.7581650, size.height * 0.8114332)
    ..cubicTo(
        size.width * 0.7597350,
        size.height * 0.8115971,
        size.width * 0.7624125,
        size.height * 0.8115971,
        size.width * 0.7641175,
        size.height * 0.8114382)
    ..cubicTo(
        size.width * 0.7658250,
        size.height * 0.8112743,
        size.width * 0.7645400,
        size.height * 0.8111452,
        size.width * 0.7612675,
        size.height * 0.8111402)
    ..cubicTo(
        size.width * 0.7579925,
        size.height * 0.8111402,
        size.width * 0.7565975,
        size.height * 0.8112743,
        size.width * 0.7581650,
        size.height * 0.8114332)
    ..moveTo(size.width * 0.7486175, size.height * 0.8156296)
    ..cubicTo(
        size.width * 0.7491450,
        size.height * 0.8158283,
        size.width * 0.7500050,
        size.height * 0.8158283,
        size.width * 0.7505325,
        size.height * 0.8156296)
    ..cubicTo(
        size.width * 0.7510575,
        size.height * 0.8154260,
        size.width * 0.7506275,
        size.height * 0.8152621,
        size.width * 0.7495750,
        size.height * 0.8152621)
    ..cubicTo(
        size.width * 0.7485225,
        size.height * 0.8152621,
        size.width * 0.7480925,
        size.height * 0.8154260,
        size.width * 0.7486175,
        size.height * 0.8156296)
    ..moveTo(size.width * 0.8143075, size.height * 0.8534622)
    ..cubicTo(
        size.width * 0.8143125,
        size.height * 0.8553196,
        size.width * 0.8144000,
        size.height * 0.8559801,
        size.width * 0.8145025,
        size.height * 0.8549272)
    ..cubicTo(
        size.width * 0.8146025,
        size.height * 0.8538794,
        size.width * 0.8146000,
        size.height * 0.8523597,
        size.width * 0.8144925,
        size.height * 0.8515502)
    ..cubicTo(
        size.width * 0.8143875,
        size.height * 0.8507457,
        size.width * 0.8143025,
        size.height * 0.8516048,
        size.width * 0.8143075,
        size.height * 0.8534622)
    ..moveTo(size.width * 0.1966400, size.height * 0.8572613)
    ..lineTo(size.width * 0.1945150, size.height * 0.8578424)
    ..lineTo(size.width * 0.1969850, size.height * 0.8579715)
    ..cubicTo(
        size.width * 0.1985400,
        size.height * 0.8580559,
        size.width * 0.1993575,
        size.height * 0.8577927,
        size.width * 0.1991925,
        size.height * 0.8572613)
    ..cubicTo(
        size.width * 0.1990475,
        size.height * 0.8567995,
        size.width * 0.1988925,
        size.height * 0.8564767,
        size.width * 0.1988475,
        size.height * 0.8565512)
    ..cubicTo(
        size.width * 0.1988025,
        size.height * 0.8566207,
        size.width * 0.1978100,
        size.height * 0.8569435,
        size.width * 0.1966400,
        size.height * 0.8572613)
    ..moveTo(size.width * 0.1349600, size.height * 0.8583837)
    ..cubicTo(
        size.width * 0.1348425,
        size.height * 0.8589995,
        size.width * 0.1348075,
        size.height * 0.8628980,
        size.width * 0.1348875,
        size.height * 0.8670398)
    ..lineTo(size.width * 0.1350300, size.height * 0.8745735)
    ..lineTo(size.width * 0.1351175, size.height * 0.8663545)
    ..lineTo(size.width * 0.1352050, size.height * 0.8581304)
    ..lineTo(size.width * 0.1480650, size.height * 0.8578970)
    ..lineTo(size.width * 0.1609275, size.height * 0.8576586)
    ..lineTo(size.width * 0.1480525, size.height * 0.8574600)
    ..cubicTo(
        size.width * 0.1376650,
        size.height * 0.8573011,
        size.width * 0.1351350,
        size.height * 0.8574798,
        size.width * 0.1349600,
        size.height * 0.8583837)
    ..moveTo(size.width * 0.1750875, size.height * 0.8578920)
    ..cubicTo(
        size.width * 0.1774950,
        size.height * 0.8580410,
        size.width * 0.1813225,
        size.height * 0.8580410,
        size.width * 0.1835900,
        size.height * 0.8578920)
    ..cubicTo(
        size.width * 0.1858575,
        size.height * 0.8577381,
        size.width * 0.1838850,
        size.height * 0.8576139,
        size.width * 0.1792100,
        size.height * 0.8576189)
    ..cubicTo(
        size.width * 0.1745325,
        size.height * 0.8576189,
        size.width * 0.1726775,
        size.height * 0.8577431,
        size.width * 0.1750875,
        size.height * 0.8578920)
    ..moveTo(size.width * 0.8474850, size.height * 0.8652867)
    ..cubicTo(
        size.width * 0.8474875,
        size.height * 0.8676059,
        size.width * 0.8475725,
        size.height * 0.8684601,
        size.width * 0.8476700,
        size.height * 0.8671739)
    ..arcToPoint(Offset(size.width * 0.8476625, size.height * 0.8629526),
        radius: Radius.elliptical(
            size.width * 0.007532500, size.height * 0.01496318),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8475625,
        size.height * 0.8619097,
        size.width * 0.8474825,
        size.height * 0.8629625,
        size.width * 0.8474850,
        size.height * 0.8652867)
    ..moveTo(size.width * 0.8117650, size.height * 0.8682416)
    ..cubicTo(
        size.width * 0.8117650,
        size.height * 0.8703324,
        size.width * 0.8118475,
        size.height * 0.8711866,
        size.width * 0.8119500,
        size.height * 0.8701437)
    ..arcToPoint(Offset(size.width * 0.8119500, size.height * 0.8663396),
        radius: Radius.elliptical(
            size.width * 0.006150000, size.height * 0.01221686),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8118475,
        size.height * 0.8652967,
        size.width * 0.8117650,
        size.height * 0.8661508,
        size.width * 0.8117650,
        size.height * 0.8682416)
    ..moveTo(size.width * 0.9592775, size.height * 0.9055626)
    ..cubicTo(
        size.width * 0.9592275,
        size.height * 0.9258595,
        size.width * 0.9593275,
        size.height * 0.9426403,
        size.width * 0.9595025,
        size.height * 0.9428588)
    ..cubicTo(
        size.width * 0.9597925,
        size.height * 0.9432164,
        size.width * 0.9597875,
        size.height * 0.9151375,
        size.width * 0.9594900,
        size.height * 0.8817547)
    ..cubicTo(
        size.width * 0.9594250,
        size.height * 0.8745537,
        size.width * 0.9593300,
        size.height * 0.8852707,
        size.width * 0.9592775,
        size.height * 0.9055626)
    ..moveTo(size.width * 0.8474850, size.height * 0.8737293)
    ..cubicTo(
        size.width * 0.8474875,
        size.height * 0.8760535,
        size.width * 0.8475725,
        size.height * 0.8769027,
        size.width * 0.8476700,
        size.height * 0.8756214)
    ..arcToPoint(Offset(size.width * 0.8476625, size.height * 0.8713952),
        radius: Radius.elliptical(
            size.width * 0.007550000, size.height * 0.01499794),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8475625,
        size.height * 0.8703572,
        size.width * 0.8474825,
        size.height * 0.8714101,
        size.width * 0.8474850,
        size.height * 0.8737293)
    ..moveTo(size.width * 0.8138975, size.height * 0.8813325)
    ..cubicTo(
        size.width * 0.8139000,
        size.height * 0.8836567,
        size.width * 0.8139825,
        size.height * 0.8845059,
        size.width * 0.8140800,
        size.height * 0.8832197)
    ..arcToPoint(Offset(size.width * 0.8140750, size.height * 0.8789984),
        radius: Radius.elliptical(
            size.width * 0.007632500, size.height * 0.01516182),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8139750,
        size.height * 0.8779605,
        size.width * 0.8138950,
        size.height * 0.8790083,
        size.width * 0.8138975,
        size.height * 0.8813325)
    ..moveTo(size.width * 0.8113400, size.height * 0.8978005)
    ..cubicTo(
        size.width * 0.8113400,
        size.height * 0.8998912,
        size.width * 0.8114225,
        size.height * 0.9007454,
        size.width * 0.8115250,
        size.height * 0.8997025)
    ..cubicTo(
        size.width * 0.8116250,
        size.height * 0.8986596,
        size.width * 0.8116250,
        size.height * 0.8969463,
        size.width * 0.8115250,
        size.height * 0.8959034)
    ..cubicTo(
        size.width * 0.8114225,
        size.height * 0.8948555,
        size.width * 0.8113400,
        size.height * 0.8957097,
        size.width * 0.8113400,
        size.height * 0.8978005)
    ..moveTo(size.width * 0.8134725, size.height * 0.9134291)
    ..cubicTo(
        size.width * 0.8134750,
        size.height * 0.9157483,
        size.width * 0.8135575,
        size.height * 0.9165976,
        size.width * 0.8136550,
        size.height * 0.9153163)
    ..arcToPoint(Offset(size.width * 0.8136500, size.height * 0.9110950),
        radius: Radius.elliptical(
            size.width * 0.007632500, size.height * 0.01516182),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8135475,
        size.height * 0.9100521,
        size.width * 0.8134700,
        size.height * 0.9111049,
        size.width * 0.8134725,
        size.height * 0.9134291)
    ..moveTo(size.width * 0.8109200, size.height * 0.9294749)
    ..cubicTo(
        size.width * 0.8109250,
        size.height * 0.9317991,
        size.width * 0.8110075,
        size.height * 0.9326483,
        size.width * 0.8111050,
        size.height * 0.9313621)
    ..arcToPoint(Offset(size.width * 0.8110975, size.height * 0.9271408),
        radius: Radius.elliptical(
            size.width * 0.007535000, size.height * 0.01496814),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8109975,
        size.height * 0.9261029,
        size.width * 0.8109175,
        size.height * 0.9271507,
        size.width * 0.8109200,
        size.height * 0.9294749)
    ..moveTo(size.width * 0.8130400, size.height * 0.9434101)
    ..cubicTo(
        size.width * 0.8130400,
        size.height * 0.9455009,
        size.width * 0.8131250,
        size.height * 0.9463551,
        size.width * 0.8132250,
        size.height * 0.9453122)
    ..arcToPoint(Offset(size.width * 0.8132250, size.height * 0.9415080),
        radius: Radius.elliptical(
            size.width * 0.006000000, size.height * 0.01191889),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8131250,
        size.height * 0.9404651,
        size.width * 0.8130400,
        size.height * 0.9413193,
        size.width * 0.8130400,
        size.height * 0.9434101)
    ..moveTo(size.width * 0.8104900, size.height * 0.9569231)
    ..cubicTo(
        size.width * 0.8104900,
        size.height * 0.9590139,
        size.width * 0.8105725,
        size.height * 0.9598681,
        size.width * 0.8106750,
        size.height * 0.9588252)
    ..arcToPoint(Offset(size.width * 0.8106750, size.height * 0.9550211),
        radius: Radius.elliptical(
            size.width * 0.006150000, size.height * 0.01221686),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8105725,
        size.height * 0.9539782,
        size.width * 0.8104900,
        size.height * 0.9548324,
        size.width * 0.8104900,
        size.height * 0.9569231)
    ..moveTo(size.width * 0.9593700, size.height * 0.9763509)
    ..cubicTo(
        size.width * 0.9593700,
        size.height * 0.9886572,
        size.width * 0.9594300,
        size.height * 0.9936929,
        size.width * 0.9595025,
        size.height * 0.9875398)
    ..cubicTo(
        size.width * 0.9595750,
        size.height * 0.9813867,
        size.width * 0.9595750,
        size.height * 0.9713152,
        size.width * 0.9595025,
        size.height * 0.9651571)
    ..cubicTo(
        size.width * 0.9594300,
        size.height * 0.9590040,
        size.width * 0.9593700,
        size.height * 0.9640397,
        size.width * 0.9593700,
        size.height * 0.9763509)
    ..moveTo(size.width * 0.8125975, size.height * 0.9679034)
    ..cubicTo(
        size.width * 0.8125975,
        size.height * 0.9695274,
        size.width * 0.8126850,
        size.height * 0.9701928,
        size.width * 0.8127925,
        size.height * 0.9693833)
    ..arcToPoint(Offset(size.width * 0.8127925, size.height * 0.9664235),
        radius: Radius.elliptical(
            size.width * 0.003482500, size.height * 0.006917924),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8126850,
        size.height * 0.9656140,
        size.width * 0.8125975,
        size.height * 0.9662795,
        size.width * 0.8125975,
        size.height * 0.9679034)
    ..moveTo(size.width * 0.8100475, size.height * 0.9788837)
    ..cubicTo(
        size.width * 0.8100475,
        size.height * 0.9805076,
        size.width * 0.8101350,
        size.height * 0.9811731,
        size.width * 0.8102425,
        size.height * 0.9803587)
    ..arcToPoint(Offset(size.width * 0.8102425, size.height * 0.9774038),
        radius: Radius.elliptical(
            size.width * 0.003470000, size.height * 0.006893093),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8101350,
        size.height * 0.9765943,
        size.width * 0.8100475,
        size.height * 0.9772548,
        size.width * 0.8100475,
        size.height * 0.9788837)
    ..moveTo(size.width * 0.7352400, size.height * 0.9919746)
    ..cubicTo(
        size.width * 0.7352450,
        size.height * 0.9942988,
        size.width * 0.7353275,
        size.height * 0.9951480,
        size.width * 0.7354250,
        size.height * 0.9938618)
    ..arcToPoint(Offset(size.width * 0.7354175, size.height * 0.9896405),
        radius: Radius.elliptical(
            size.width * 0.007535000, size.height * 0.01496814),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.7353175,
        size.height * 0.9886026,
        size.width * 0.7352375,
        size.height * 0.9896504,
        size.width * 0.7352400,
        size.height * 0.9919746);
}

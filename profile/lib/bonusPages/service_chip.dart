import 'package:flutter/material.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class ServiceChip extends StatelessWidget {
  final String title;
  final Widget child;
  final bool active;
  final VoidCallback? onTap;
  final String? subTitle;

  const ServiceChip({
    required this.title,
    required this.child,
    this.active = false,
    this.onTap,
    this.subTitle,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      active: onTap != null,
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          width: 163,
          height: 113,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Colors.white,
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(13),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 15),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 6),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        flex: 2,
                        child: Text(
                          title,
                          style: TextStyle(
                            color: active
                                ? ColorData.allMainActivegray
                                : ColorData.allMainBlack,
                            fontSize: 14,
                            height: 15 / 14,
                            fontWeight: FontWeight.w500,
                          ),
                          textAlign: TextAlign.start,
                        ),
                      ),
                      active
                          ? Flexible(
                              flex: 1,
                              child: Text(
                                'Скоро',
                                style: const TextStyle(
                                  color: ColorData.clientsButtonColorPressed,
                                  fontSize: 14,
                                  height: 15 / 14,
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            )
                          : SizedBox(),
                    ],
                  ),
                ),
                const SizedBox(height: 10),
                Expanded(
                  child: Stack(
                    clipBehavior: Clip.none,
                    children: [child],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

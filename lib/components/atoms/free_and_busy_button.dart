import 'package:flutter/material.dart';

class FreeAndBusyButton extends StatefulWidget {
  final bool show;
  final String text;

  const FreeAndBusyButton({
    required this.show,
    required this.text,
    Key? key,
  }) : super(key: key);

  @override
  _FreeAndBusyButtonState createState() => _FreeAndBusyButtonState();
}

class _FreeAndBusyButtonState extends State<FreeAndBusyButton>
    with SingleTickerProviderStateMixin {
  late final AnimationController _animationController = AnimationController(
    vsync: this,
    duration: Duration(
      milliseconds: 200,
    ),
    value: widget.show ? 1 : 0,
  );

  @override
  void didUpdateWidget(covariant FreeAndBusyButton oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.show) {
      _animationController.forward();
    } else {
      _animationController.reverse();
    }
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      opacity: _animationController,
      child: Text(
        widget.text,
        style: TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: 20,
          color: Colors.white,
        ),
      ),
    );
  }
}

import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/data/models/session_minute.dart';

mixin FinishSession {
  static Future<SessionMinute> finishSession({
    required String sessionId,
    required GraphQLClient client,
  }) async {
    final _mutationResult = await client.mutate(
      MutationOptions(
        document: gql('''
            mutation(
              \$sessionId: String!
            ) {
              endSession(
                sessionId: \$sessionId
              ){
                _id
  					    endTime
  					    price
                institutionId
                institution {
                  _id
                  location {
                    address
                    latitude
                    longitude
                  }
                  name
                  rating
                  description
                }
  					    service {
                  _id
  					    	endTime
  					    	name
  					    	price
                  bufferTimeMinutes
  					    	startTime
                }
  					    startTime
                status
                usedNumOfBonus
                bonusEarnt
              }
              }'''),
        variables: {
          "sessionId": sessionId,
        },
      ),
    );
    if (_mutationResult.hasException) throw _mutationResult.exception!;
    return SessionMinute.fromMap(_mutationResult.data!['endSession'])!;
  }
}

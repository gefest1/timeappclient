import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/main.dart';

class GraphqlProvider {
  late Link link;
  late GraphQLClient client;

  GraphqlProvider() {
    final HttpLink httpLink = HttpLink('http://167.172.162.8:3000/graphql');
    final AuthLink authLink = AuthLink(
      getToken: () {
        return sharedPreferences.getString('token');
      },
    );
    final WebSocketLink websocketLink = WebSocketLink(
      'ws://167.172.162.8:3000/graphql',
      config: SocketClientConfig(
        autoReconnect: true,
        inactivityTimeout: Duration(seconds: 30),
      ),
    );
    link = authLink.concat(httpLink).concat(websocketLink);
    client = GraphQLClient(
      link: link,
      cache: GraphQLCache(),
    );
  }
}

class GraphqlMinuteProvider {
  late Link link;
  late GraphQLClient client;

  GraphqlMinuteProvider() {
    final HttpLink httpLink = HttpLink('http://167.172.162.8:3001/graphql');
    final AuthLink authLink = AuthLink(
      getToken: () {
        return sharedPreferences.getString('token');
      },
    );
    final WebSocketLink websocketLink = WebSocketLink(
      'ws://167.172.162.8:3001/graphql',
      config: SocketClientConfig(
        autoReconnect: true,
        inactivityTimeout: Duration(seconds: 30),
      ),
    );
    link = authLink.concat(httpLink).concat(websocketLink);
    client = GraphQLClient(
      link: link,
      cache: GraphQLCache(),
    );
  }
}

class GraphqlTakeAwayProvider {
  late Link link;
  late GraphQLClient client;

  GraphqlTakeAwayProvider() {
    final HttpLink httpLink = HttpLink('http://167.172.162.8:3003/graphql');
    final AuthLink authLink = AuthLink(
      getToken: () {
        return sharedPreferences.getString('token');
      },
    );
    final WebSocketLink websocketLink = WebSocketLink(
      'ws://167.172.162.8:3003/graphql',
      config: SocketClientConfig(
        autoReconnect: true,
        inactivityTimeout: Duration(seconds: 30),
      ),
    );
    link = authLink.concat(httpLink).concat(websocketLink);
    client = GraphQLClient(
      link: link,
      cache: GraphQLCache(),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:li/const.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class OrderCard extends StatelessWidget {
  const OrderCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
        boxShadow: bigBlur,
      ),
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            Row(
              children: [
                Flexible(
                  child: Text(
                    'Выбранные блюда',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w600,
                      color: ColorData.allMainBlack,
                    ),
                  ),
                ),
                SizedBox(width: 20),
                Text(
                  'Смотреть все',
                  style: TextStyle(
                    height: P3TextStyle.height,
                    fontSize: P3TextStyle.fontSize,
                    fontWeight: P3TextStyle.fontWeight,
                    color: ColorData.clientsButtonColorPressed,
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            Container(
              decoration: BoxDecoration(
                color: ColorData.allMessagesbackground,
                borderRadius: BorderRadius.circular(15),
              ),
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Паста «Фетучини»',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            SizedBox(height: 5),
                            Text(
                              '1550 ₸ — 300 г. ',
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            Text(
                              'Итальянская кухня — второе',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: ColorData.allMainActivegray),
                            ),
                          ],
                        ),
                        CircleAvatar(minRadius: 20),
                      ],
                    ),
                    SizedBox(height: 15),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Описание блюда, состав',
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        SizedBox(height: 2),
                        Text(
                          'Куриное филе, паста феттучини, шампиньоны и мой секретный соус))',
                          style: TextStyle(
                            fontSize: P4TextStyle.fontSize,
                            color: ColorData.allMainActivegray,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 15),
                    SizedBox(
                      height: 130,
                      child: ListView.separated(
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (ctx, _) => Container(
                          width: 130,
                          height: 130,
                          decoration: BoxDecoration(
                            color: Colors.grey,
                            borderRadius: BorderRadius.circular(16),
                          ),
                        ),
                        separatorBuilder: (ctx, _) => SizedBox(width: 8),
                        itemCount: 10,
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

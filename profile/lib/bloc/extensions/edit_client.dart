part of '../profile_bloc.dart';

extension EditDataClient on ProfileBloc {
  void editClient(
    EditClient event,
    Emitter<ProfileState> emit,
  ) async {
    MultipartFile? _file;
    if (event.avatar != null) {
      _file = MultipartFile.fromBytes(
        '',
        await event.avatar!.readAsBytes(),
        filename: event.avatar!.path,
        contentType: MediaType("image", "jpg"),
      );
    }
    final _mutationResult = await graphqlProvider.client.mutate(
      MutationOptions(document: gql('''
              mutation(
                \$avatar: Upload
                \$fullName: String
                \$sex: SexEnum
              ){
                editClient(
                  avatar: \$avatar
                  fullName: \$fullName
                  sex: \$sex
                  ) {
                  debt
                  email,
                  city,
                  creditCards{
                    cardHolderName,
                    cardType,
                    last4Digits,
                  }
                  _id,
                  dateOfBirth,
                  fullName,
                  phoneNumber,
                  photoURL{
                    M,
                    XL,
                    thumbnail
                  }
                  sex
              }
            }'''), variables: {
        'avatar': _file,
        'fullName': event.name,
        'sex': event.sex.toString()
      }),
    );
    if (_mutationResult.hasException) throw _mutationResult.exception!;
    final Client _data = Client.fromMap(_mutationResult.data!['editClient']);
    emit(ProfileInformation(client: _data));
  }
}

import 'package:flutter/material.dart';
import 'package:li/components/molecules/bloc_widget.dart';
import 'package:li/components/molecules/overlay_loader.dart';
import 'package:li/utils/color.dart';

OverlayLoader getOverlayLoader([BuildContext? context, bool? valueBool]) {
  final _child = AbsorbPointer(
    absorbing: true,
    child: DecoratedBox(
      decoration: BoxDecoration(color: Colors.black87),
      child: Center(
        child: CircularProgressIndicator(
          color: ColorData.clientsButtonColorDefault,
        ),
      ),
    ),
  );

  return OverlayLoader(
    bottom: 0,
    top: 0,
    left: 0,
    right: 0,
    child: context == null
        ? _child
        : WillPopScope(
            onWillPop: () async {
              return true;
            },
            child: BlocWidget(
              parentContext: context,
              child: _child,
            ),
          ),
  );
}

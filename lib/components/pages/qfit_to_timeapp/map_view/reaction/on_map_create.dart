part of '../map_view.dart';

extension OnMapCreate on _MapViewState {
  void cameraTracking(
    CameraPosition cameraPosition,
    bool finished,
  ) async {
    streamController.add(null);
  }

  void onMapCreated(YandexMapController controller) async {
    setState(() {
      yandexMapController = controller;
    });

    await yandexMapController!.moveCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: Point(
            latitude: 43.2353544,
            longitude: 76.896372,
          ),
          zoom: 12.2,
        ),
      ),
      animation: MapAnimation(
        duration: 0,
      ),
    );
    mapCreatedLoc();
  }
}

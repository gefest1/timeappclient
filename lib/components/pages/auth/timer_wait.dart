import 'dart:async';

import 'package:flutter/material.dart';
import 'package:li/utils/color.dart';

class TimerWait extends StatefulWidget {
  final Timer timer;
  final DateTime dateTime;

  const TimerWait({
    required this.timer,
    required this.dateTime,
    Key? key,
  }) : super(key: key);

  @override
  _TimerWaitState createState() => _TimerWaitState();
}

class _TimerWaitState extends State<TimerWait> {
  late Timer _timer;

  @override
  void initState() {
    super.initState();
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {});
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Duration _duration = DateTime.now().difference(widget.dateTime);
    return FittedBox(
      child: Text(
        (60 - _duration.inSeconds).toString(),
        style: TextStyle(
          color: ColorData.clientsButtonColorDefault,
        ),
      ),
    );
  }
}

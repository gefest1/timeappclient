import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:li/components/icons/car_settings.dart';
import 'package:li/components/icons/cut.dart';
import 'package:li/components/icons/message.dart';
import 'package:li/components/icons/motorcycle1.dart';
import 'package:li/components/icons/nails.dart';
import 'package:li/components/icons/playstation.dart';
import 'package:li/components/icons/time.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/text_styles.dart';
import 'package:sliver_tools/sliver_tools.dart';
import 'package:li/components/atoms/wait_button.dart';
import 'package:time_app_components/utils/testStyles.dart';

class _AllClasses extends StatelessWidget {
  const _AllClasses({Key? key}) : super(key: key);

  Widget _fastBuid({
    required String text,
    required String subText,
    required Widget icon,
    double height = 160,
  }) {
    return Container(
      padding: EdgeInsets.all(15),
      height: height,
      width: double.infinity,
      decoration: BoxDecoration(
        color: ColorData.allMainLightgray,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                icon,
                SizedBox(height: 10),
                Text(
                  text,
                  style: TextStyle(
                    color: ColorData.allMainBlack,
                    fontWeight: FontWeight.w500,
                    fontSize: 16,
                    height: 19 / 16,
                  ),
                ),
              ],
            ),
          ),
          Text(
            subText,
            style: TextStyle(
              fontSize: P4TextStyle.fontSize,
              fontWeight: P4TextStyle.fontWeight,
              height: P4TextStyle.height,
              color: ColorData.allMainActivegray,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _fastBuid(
                    text: 'Ремонт двигателя',
                    subText: '100 м. от вас',
                    height: 180,
                    icon: CircleAvatar(
                      radius: 23,
                      backgroundColor: ColorData.clientsIcons,
                      child: CustomPaint(
                        painter: CarSettingsCustomPainter(),
                        size: Size(28, 28),
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),
                  _fastBuid(
                    text: 'Снятие лака',
                    subText: '180 м. от вас',
                    icon: CircleAvatar(
                      radius: 23,
                      backgroundColor: ColorData.clientsIcons,
                      child: CustomPaint(
                        painter: NailsCustomPainter(),
                        size: Size(28, 28),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _fastBuid(
                    text: 'Подстричься',
                    subText: '127 м. от вас',
                    icon: CircleAvatar(
                      radius: 23,
                      backgroundColor: ColorData.clientsIcons,
                      child: CustomPaint(
                        painter: CutCustomPainter(),
                        size: Size(28, 28),
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),
                  _fastBuid(
                    height: 180,
                    text: 'Ремонт игровых приставок',
                    subText: '250 м. от вас',
                    icon: CircleAvatar(
                      radius: 23,
                      backgroundColor: ColorData.clientsIcons,
                      child: CustomPaint(
                        painter: PlaystationCustomPainter(),
                        size: Size(28, 28),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        const SizedBox(height: 10),
        DecoratedBox(
          position: DecorationPosition.foreground,
          decoration: BoxDecoration(
              gradient: LinearGradient(
            colors: [
              Colors.white.withOpacity(0),
              Colors.white.withOpacity(0.85),
              Colors.white,
              Colors.white,
            ],
            stops: [0, 0.8, 0.95, 1.4],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            // tileMode:
          )),
          child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _fastBuid(
                      text: 'Массаж шейно-воротниковой зоны',
                      subText: '500 м. от вас',
                      height: 180,
                      icon: CircleAvatar(
                        radius: 23,
                        backgroundColor: ColorData.clientsIcons,
                        child: CustomPaint(
                          painter: MessageCustomPainter(),
                          size: Size(28, 28),
                        ),
                      ),
                    ),
                    const SizedBox(height: 10),
                    _fastBuid(
                      text: 'Ремонт мотоциклов',
                      subText: '805 м. от вас',
                      icon: CircleAvatar(
                        radius: 23,
                        backgroundColor: ColorData.clientsIcons,
                        child: CustomPaint(
                          painter: Motorcycle1CustomPainter(),
                          size: Size(28, 28),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _fastBuid(
                      text: 'Ремонт\nмотоциклов',
                      subText: '805 м. от вас',
                      icon: CircleAvatar(
                        radius: 23,
                        backgroundColor: ColorData.clientsIcons,
                        child: CustomPaint(
                          painter: Motorcycle1CustomPainter(),
                          size: Size(28, 28),
                        ),
                      ),
                    ),
                    const SizedBox(height: 10),
                    _fastBuid(
                      height: 180,
                      text: 'Ремонт игровых приставок',
                      subText: '250 м. от вас',
                      icon: CircleAvatar(
                        radius: 23,
                        backgroundColor: ColorData.clientsIcons,
                        child: CustomPaint(
                          painter: PlaystationCustomPainter(),
                          size: Size(28, 28),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class MainServicePage extends StatefulWidget {
  const MainServicePage({Key? key}) : super(key: key);

  @override
  State<MainServicePage> createState() => _MainServicePageState();
}

class _MainServicePageState extends State<MainServicePage> {
  bool status = false;
  @override
  Widget build(BuildContext context) {
    return SliverPadding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      sliver: MultiSliver(children: [
        SliverList(
          delegate: SliverChildListDelegate([
            const SizedBox(height: 40),
            const Text(
              'Поможем со всем, неподалеку от вас',
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 38,
                  height: 40 / 38,
                  color: ColorData.allMainBlack,
                  letterSpacing: -1),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 20),
            Center(
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    status = !status;
                  });
                  FirebaseAnalytics().logEvent(
                      name: 'WaitButton',
                      parameters: <String, dynamic>{
                        'Type': 'Services',
                      });
                },
                child: WaitButton(
                  image: AssetImage('assets/service.png'),
                  active: status,
                ),
              ),
            ),
            const SizedBox(height: 10),
            const Center(
              child: Text(
                'Август 2022',
                style: TextStyle(
                  color: ColorData.allMainGray,
                  fontSize: P3TextStyle.fontSize,
                  fontWeight: P3TextStyle.fontWeight,
                  height: P3TextStyle.height,
                ),
              ),
            ),
            const SizedBox(height: 40),
            const _AllClasses(),
            // const SizedBox(height: 100),
          ]),
        ),
      ]),
    );
  }
}

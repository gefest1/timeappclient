import 'dart:io';

import 'package:flutter/material.dart';
import 'package:li/components/atoms/error_overlay.dart';
import 'package:li/main.dart';
import 'package:profile/about/about_page.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfileModel {
  final String title, subtitle, link, svg;
  final VoidCallback onTap;
  final bool isItSmall;
  final bool versionType;
  const ProfileModel({
    this.versionType = false,
    this.isItSmall = false,
    required this.onTap,
    required this.title,
    required this.subtitle,
    required this.link,
    required this.svg,
  });
}

class ProfileRepository {
  static List<ProfileModel> getSettingsList() => [
        ProfileModel(
          title: 'Поддержка',
          subtitle: 'Справочный центр и связь со специалистом',
          link: 'https://timeapp.kz/client/help',
          svg: 'assets/svg/profile/support.svg',
          onTap: () {
            launchLink('https://timeapp.kz/client/help');
          },
        ),
        ProfileModel(
          title: 'Приложение для исполнителей',
          subtitle: '',
          link: 'http://timeapp.kz/business',
          svg: 'assets/svg/profile/timeAppLogoBlue.svg',
          onTap: () {
            launchLink('http://timeapp.kz/business');
          },
          isItSmall: true,
        ),
        ProfileModel(
          title: 'О приложении',
          subtitle: 'Версия ',
          link: '',
          svg: 'assets/svg/profile/aboutApp.svg',
          onTap: () {
            routerDelegate.push(const AboutPage());
          },
          versionType: true,
        ),
        ProfileModel(
          title: 'Новости',
          subtitle: 'Будьте в курсе обновлений и изменений в TimeApp',
          link: 'https://timeapp.kz/news',
          svg: 'assets/svg/profile/news.svg',
          onTap: () {
            launchLink('https://timeapp.kz/news');
          },
        ),
        ProfileModel(
          title: 'Оценить приложение',
          subtitle:
              'Переход в ' + (Platform.isIOS ? 'App Store' : 'Play Marekt'),
          link: 'https://apps.apple.com/ru/app/timeapp/id1514221318',
          svg: 'assets/svg/profile/appStore.svg',
          onTap: () {
            launchLink('https://apps.apple.com/ru/app/timeapp/id1514221318');
          },
        ),
      ];
  static List<ProfileModel> getAboutApp() => [
        ProfileModel(
          title: 'Условия использования',
          subtitle: '',
          link: 'https://timeapp.kz/client/terms',
          svg: 'assets/svg/profile/support.svg',
          onTap: () {
            launchLink('https://timeapp.kz/client/terms');
          },
          isItSmall: true,
        ),
        ProfileModel(
          title: 'Политика конфиденциальности',
          subtitle: '',
          link: 'https://timeapp.kz/client/privacy',
          svg: 'assets/svg/profile/support.svg',
          onTap: () {
            launchLink('https://timeapp.kz/client/privacy');
          },
          isItSmall: true,
        ),
      ];
}

void launchLink(String link) async {
  if (link == '') return;
  if (!await launch('$link/')) {
    navigatorKey.currentState?.overlay?.insert(
      ErrorOverlayLoader(text: 'Не удалось открыть сайт'),
    );
  }
}

import 'dart:developer';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:li/data/models/client_model.dart';
import 'package:li/data/models/Sex.dart';
import 'package:li/logic/firebase_token/firebase_token_device.dart';
import 'package:li/main.dart';
import 'package:li/network/graphql_provider.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:http/http.dart' show MultipartFile;
import 'package:http_parser/http_parser.dart';
import 'package:li/utils/restart_widget.dart';

part './extensions/get_client.dart';
part './extensions/edit_client.dart';
part './extensions/delete_client.dart';
part 'profile_event.dart';
part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final GraphqlProvider graphqlProvider;

  ProfileBloc({required this.graphqlProvider})
      : super(isRegistered() ? ProfileInitial() : NotRegistered()) {
    on<GetClient>(getClient);
    on<EditClient>(editClient);
    on<DeleteClientEvent>(deleteClient);

    if (isRegistered()) add(GetClient());

    emit(state);
  }
}

import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/data/models/session_minute.dart';

mixin GetSessionApi {
  static Future<List<SessionMinute>?> getActiveSessions({
    required GraphQLClient client,
  }) async {
    final _queryResult = await client.query(
      QueryOptions(
        document: gql(''' 
        {
        	getActiveSessionsAsClient {
            _id
            client { 
              _id
              sex            
              email
            }
            clientId
            endTime
            institution {
              description
              type
              _id
              name
              iconPath
              tags {
                iconSVGPath
                name
              }
              ratingCount
              rating
            }
            institutionId
            price
            service {
              _id
    					endTime
    					name
    					price
    					startTime
              bufferTimeMinutes
            }
            serviceId
            startTime
            usedNumOfBonus
            bonusEarnt
          }
        }'''),
      ),
    );
    if (_queryResult.hasException) throw _queryResult.exception!;

    return (_queryResult.data!['getActiveSessionsAsClient'] as List<dynamic>?)
        ?.map((_) => SessionMinute.fromMap(_)!)
        .toList();
  }
}

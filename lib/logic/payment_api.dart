import 'dart:convert';

import 'package:dio/dio.dart';

const cloud_payments_public_id = 'pk_2fcd58fc42c0b842f6a0823d3a493';
const cloud_payments_secret_key = 'b35fdbe2c69d5479cc0e1703d8f5c025';

class PaymentApi {
  late Dio _client;

  PaymentApi() {
    String publicId = cloud_payments_public_id;
    String secretKey = cloud_payments_secret_key;
    final authCode = base64Encode(
      utf8.encode('$publicId:$secretKey'),
    );
    this._client = Dio(
      BaseOptions(
        baseUrl: 'https://api.cloudpayments.ru/',
        headers: {
          'Authorization': 'Basic $authCode',
        },
      ),
    );
  }

  Future<dynamic> testPaymentsApi() {
    return this._client.get('test');
  }

  Future<dynamic> charge({
    required int amount,
    required String currency,
    required String ipAddress,
    required String cardHolderName,
    required String cryptogram,
  }) =>
      this._client.post(
        'payments/cards/charge',
        data: {
          'Amount': amount,
          'Currency': currency,
          'ipAddress': ipAddress,
          'cardHolderName': cardHolderName,
          'cryptogram': cryptogram,
        },
      );

  Future<dynamic> auth({
    required int amount,
    required String currency,
    required String ipAddress,
    required String cardHolderName,
    required String cryptogram,
    required String email,
    required String accountId,
  }) =>
      this._client.post('payments/cards/auth',
          data: {
            'Amount': amount,
            'Currency': currency,
            'ipAddress': ipAddress,
            'Name': cardHolderName,
            'CardCryptogramPacket': cryptogram,
            'Email': email,
            'AccountId': accountId,
          },
          options: Options(
            sendTimeout: 10000,
            receiveTimeout: 10000,
          ));

  Future<dynamic> post3ds({
    required String transactionId,
    required String paRes,
  }) =>
      this._client.post(
        'payments/cards/post3ds',
        data: {
          'TransactionId': transactionId,
          'PaRes': paRes,
        },
      );

  Future<dynamic> confirmPayment({
    required String transactionId,
    required int amount,
  }) =>
      this._client.post(
        'payments/confirm',
        data: {
          'TransactionId': transactionId,
          'Amount': amount,
        },
      );

  Future<dynamic> getTransactonStatus({
    required String transactionId,
  }) =>
      this._client.post(
        'payments/get',
        data: {
          'TransactionId': transactionId,
        },
      );

  Future<dynamic> cancelPayment({
    required String transactionId,
  }) =>
      this._client.post(
        'payments/void',
        data: {
          'TransactionId': transactionId,
        },
      );

  Future<Response<dynamic>> makePreOrder(
      String amount, String token, String id) {
    return this._client.post(
      'payments/tokens/auth',
      data: {
        "Amount": amount,
        'Token': token,
        "AccountId": id,
        "Currency": "KZT"
      },
    );
  }
}

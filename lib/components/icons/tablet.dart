// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';

class TabletCustomPainter extends CustomPainter {
  const TabletCustomPainter({Listenable? repaint}) : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.7708357, 0);
    path_0.lineTo(size.width * 0.2291661, 0);
    path_0.cubicTo(
        size.width * 0.1717500,
        0,
        size.width * 0.1250000,
        size.height * 0.04675000,
        size.width * 0.1250000,
        size.height * 0.1041661);
    path_0.lineTo(size.width * 0.1250000, size.height * 0.8958321);
    path_0.cubicTo(
        size.width * 0.1250000,
        size.height * 0.9532500,
        size.width * 0.1717500,
        size.height,
        size.width * 0.2291661,
        size.height);
    path_0.lineTo(size.width * 0.7708321, size.height);
    path_0.cubicTo(
        size.width * 0.8282500,
        size.height,
        size.width * 0.8750000,
        size.height * 0.9532500,
        size.width * 0.8750000,
        size.height * 0.8958357);
    path_0.lineTo(size.width * 0.8750000, size.height * 0.1041661);
    path_0.cubicTo(size.width * 0.8750000, size.height * 0.04675000,
        size.width * 0.8282500, 0, size.width * 0.7708357, 0);
    path_0.close();
    path_0.moveTo(size.width * 0.5000000, size.height * 0.9375000);
    path_0.cubicTo(
        size.width * 0.4770393,
        size.height * 0.9375000,
        size.width * 0.4583357,
        size.height * 0.9187893,
        size.width * 0.4583357,
        size.height * 0.8958357);
    path_0.cubicTo(
        size.width * 0.4583357,
        size.height * 0.8728786,
        size.width * 0.4770429,
        size.height * 0.8541679,
        size.width * 0.5000000,
        size.height * 0.8541679);
    path_0.cubicTo(
        size.width * 0.5229571,
        size.height * 0.8541679,
        size.width * 0.5416643,
        size.height * 0.8728786,
        size.width * 0.5416643,
        size.height * 0.8958357);
    path_0.cubicTo(
        size.width * 0.5416643,
        size.height * 0.9187893,
        size.width * 0.5229607,
        size.height * 0.9375000,
        size.width * 0.5000000,
        size.height * 0.9375000);
    path_0.close();
    path_0.moveTo(size.width * 0.7916643, size.height * 0.8125000);
    path_0.cubicTo(
        size.width * 0.7916643,
        size.height * 0.8240000,
        size.width * 0.7823321,
        size.height * 0.8333357,
        size.width * 0.7708321,
        size.height * 0.8333357);
    path_0.lineTo(size.width * 0.2291661, size.height * 0.8333357);
    path_0.cubicTo(
        size.width * 0.2176661,
        size.height * 0.8333357,
        size.width * 0.2083321,
        size.height * 0.8240000,
        size.width * 0.2083321,
        size.height * 0.8125000);
    path_0.lineTo(size.width * 0.2083321, size.height * 0.1041661);
    path_0.cubicTo(
        size.width * 0.2083321,
        size.height * 0.09266607,
        size.width * 0.2176661,
        size.height * 0.08333214,
        size.width * 0.2291661,
        size.height * 0.08333214);
    path_0.lineTo(size.width * 0.7708321, size.height * 0.08333214);
    path_0.cubicTo(
        size.width * 0.7823321,
        size.height * 0.08333214,
        size.width * 0.7916643,
        size.height * 0.09266607,
        size.width * 0.7916643,
        size.height * 0.1041661);
    path_0.lineTo(size.width * 0.7916643, size.height * 0.8125000);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_0, paint_0_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}

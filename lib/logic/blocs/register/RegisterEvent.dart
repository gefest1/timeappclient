import 'package:equatable/equatable.dart';

class RegisterEvent extends Equatable {
  const RegisterEvent();
  @override
  List<Object?> get props => [];
}

class SendCodeRegisterEvent extends RegisterEvent {
  final String phoneNumber;

  const SendCodeRegisterEvent({required this.phoneNumber});

  @override
  List<Object?> get props => [this.phoneNumber];
}

class CheckSMSRegisterEvent extends RegisterEvent {
  final String phoneNumber;
  final String code;

  const CheckSMSRegisterEvent({
    required this.phoneNumber,
    required this.code,
  });

  @override
  List<Object?> get props => [
        this.phoneNumber,
        this.code,
      ];
}

import 'package:flutter_svg/svg.dart';
import 'package:li/const.dart';
import 'package:li/utils/color.dart';
import 'package:flutter/material.dart';

class TelegramContact extends StatelessWidget {
  const TelegramContact({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(16)),
        boxShadow: bigBlur,
      ),
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          RepaintBoundary(
              child: SvgPicture.asset('assets/svg/icon/telegram.svg')),
          Text(
            'Telegram',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 16,
              color: ColorData.allMainBlack,
            ),
          ),
          const SizedBox(height: 2),
          Text(
            '@ibahdrums',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 16,
              color: ColorData.allMainBlack,
            ),
          ),
        ],
      ),
    );
  }
}

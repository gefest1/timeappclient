import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:time_app_components/utils/color.dart';

class TallInfoCard extends StatelessWidget {
  final String assetString;
  final String title;
  final String subTitle;

  const TallInfoCard({
    required this.assetString,
    required this.title,
    required this.subTitle,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 104,
      width: 76,
      padding: EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 4,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: ColorData.allMessagesbackground,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          RepaintBoundary(child: SvgPicture.asset(assetString)),
          Text(
            title,
            style: TextStyle(
              color: ColorData.allMainBlack,
              fontSize: 24,
              height: 28 / 24,
              fontWeight: FontWeight.w400,
            ),
          ),
          Text(
            subTitle,
            style: TextStyle(
              color: ColorData.allMainBlack,
              fontSize: 12,
              height: 14 / 12,
              fontWeight: FontWeight.w400,
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/utils/color.dart';
import 'package:li/components/icons/time.dart';

class WaitButton extends StatelessWidget {
  final ImageProvider<Object> image;
  final bool active;
  WaitButton({
    Key? key,
    required this.image,
    required this.active,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: Container(
        height: 70,
        width: 220,
        decoration: BoxDecoration(
          color:
              active ? ColorData.allInnerInactive : ColorData.grayScaleOffWhite,
          borderRadius: BorderRadius.circular(15),
          image: active
              ? null
              : DecorationImage(
                  image: image,
                ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomPaint(
              painter: TimeCustomPainter(
                color: active ? ColorData.grayScale : ColorData.lightGray,
              ),
              size: Size(18, 18),
            ),
            SizedBox(width: 5),
            Text(
              active ? "Ждем" : "Жду!",
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 18,
                height: 21 / 18,
                color: active ? ColorData.grayScale : ColorData.lightGray,
              ),
            )
          ],
        ),
      ),
    );
  }
}

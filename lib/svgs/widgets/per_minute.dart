import 'package:flutter/material.dart';
import 'package:li/svgs/bigSvg/ffaa.dart';

class PerMinueSvgWidget extends StatelessWidget {
  const PerMinueSvgWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) => ClipRRect(
        child: CustomPaint(
          painter: RPSCustomPainter(),
          isComplex: true,
          willChange: false,
          size: Size(
            constraints.maxWidth,
            constraints.maxWidth / 294 * 148,
          ),
        ),
      ),
    );
  }
}

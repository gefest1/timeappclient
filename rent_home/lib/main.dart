import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:li/utils/color.dart';
import 'package:li/components/atoms/wait_button.dart';
import 'package:time_app_components/utils/testStyles.dart';

class RentHomePage extends StatefulWidget {
  const RentHomePage({Key? key}) : super(key: key);

  @override
  State<RentHomePage> createState() => _RentHomePageState();
}

class _RentHomePageState extends State<RentHomePage> {
  bool status = false;
  @override
  Widget build(BuildContext context) {
    return SliverList(
      delegate: SliverChildListDelegate(
        [
          const SizedBox(height: 40),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Text(
              'Аренда квартир, домов, коттеджей посуточно',
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 38,
                height: 40 / 38,
                color: ColorData.allMainBlack,
                letterSpacing: -1,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          const SizedBox(height: 20),
          Center(
            child: GestureDetector(
              onTap: () {
                setState(() {
                  status = !status;
                });
                FirebaseAnalytics()
                    .logEvent(name: 'WaitButton', parameters: <String, dynamic>{
                  'Type': 'Rental',
                });
              },
              child: WaitButton(
                active: status,
                image: const AssetImage('assets/rent_photo.png'),
              ),
            ),
          ),
          const SizedBox(height: 10),
          const Text(
            'Март 2022',
            style: TextStyle(
              fontWeight: P3TextStyle.fontWeight,
              fontSize: P3TextStyle.fontSize,
              height: P3TextStyle.height,
              color: ColorData.allMainGray,
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Container(
              padding: const EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: ColorData.allMainLightgray,
                borderRadius: BorderRadius.circular(15),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  Text(
                    'Только посуточно,\nздесь и сейчас',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 24,
                      height: 26 / 24,
                      color: ColorData.allMainBlack,
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    'Сервис будет нужен, чтобы в течение 5 минут найти ночлег, место для свидания, вечеринки или официальной встречи',
                    style: TextStyle(
                      fontWeight: P2TextStyle.fontWeight,
                      fontSize: P2TextStyle.fontSize,
                      height: P2TextStyle.height,
                      color: ColorData.allMainBlack,
                    ),
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(height: 10),
          LayoutBuilder(builder: ((context, constraints) {
            return SizedBox(
              width: constraints.maxWidth,
              height: 200,
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  Positioned(
                    top: 0,
                    left: 0,
                    right: 0,
                    height: (235 / 417) * constraints.maxWidth,
                    child: const Image(
                      image: AssetImage(
                        'assets/rent.png',
                      ),
                      fit: BoxFit.fill,
                    ),
                  ),
                ],
              ),
            );
          })),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Container(
              padding: const EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: ColorData.allMainLightgray,
                borderRadius: BorderRadius.circular(15),
              ),
              child: Column(
                children: const [
                  Text(
                    'Не нужно перезванивать и узнавать свободно ли жилье',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 24,
                      height: 26 / 24,
                      color: ColorData.allMainBlack,
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    'Если объявление есть в Time — жилье сейчас свободно. Как только в него заезжают, оно автоматически скрывается из выдачи',
                    style: TextStyle(
                      fontWeight: P2TextStyle.fontWeight,
                      fontSize: P2TextStyle.fontSize,
                      height: P2TextStyle.height,
                      color: ColorData.allMainBlack,
                    ),
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(height: 100),
        ],
      ),
    );
  }
}

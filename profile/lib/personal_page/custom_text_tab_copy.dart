import 'package:flutter/material.dart';

class CustomTextTabCopy extends StatefulWidget {
  final String firstElement, secondElement;
  final String? thirdElement;
  final int lengthElements;
  final Color? color;
  final Color? colorBackground;
  final ValueNotifier<int> valueNotifier;
  final Color? unFocusColor;

  const CustomTextTabCopy({
    this.thirdElement,
    this.unFocusColor = Colors.black,
    required this.valueNotifier,
    required this.firstElement,
    required this.secondElement,
    required this.lengthElements,
    required this.color,
    this.colorBackground,
    Key? key,
  }) : super(key: key);

  @override
  State<CustomTextTabCopy> createState() => _CustomTextTabCopyState();
}

class _CustomTextTabCopyState extends State<CustomTextTabCopy>
    with SingleTickerProviderStateMixin {
  late int prevValue = widget.valueNotifier.value;
  late final TabController _tabController = TabController(
      length: widget.lengthElements,
      vsync: this,
      initialIndex: widget.valueNotifier.value)
    ..addListener(() {
      if (prevValue == _tabController.index) return;
      prevValue = _tabController.index;
      widget.valueNotifier.value = _tabController.index;
    });

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // log(_tabController.index.toString());
    return widget.lengthElements == 2
        ? Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              color: widget.colorBackground ?? Colors.transparent,
            ),
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 10,
            ),
            height: 71,
            child: TabBar(
              controller: _tabController,
              labelColor: widget.color ?? Colors.blue,
              labelStyle: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
              unselectedLabelColor: widget.unFocusColor,
              unselectedLabelStyle: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
              indicator: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.all(
                  Radius.circular(15),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    spreadRadius: 0,
                    blurRadius: 5,
                    offset: const Offset(
                      1,
                      1,
                    ),
                  ),
                ],
              ),
              tabs: [
                Text(
                  widget.firstElement,
                ),
                Text(
                  widget.secondElement,
                ),
              ],
            ),
          )
        : Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 10,
            ),
            height: 71,
            color: Colors.grey[300],
            child: TabBar(
              controller: _tabController,
              onTap: (_) {
                // log(_.toString());
              },
              labelColor: widget.color ?? Colors.blue,
              labelStyle: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
              unselectedLabelColor: Colors.black,
              unselectedLabelStyle: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
              indicator: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.all(Radius.circular(15)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    spreadRadius: 0,
                    blurRadius: 5,
                    offset: const Offset(
                      1,
                      1,
                    ),
                  ),
                ],
              ),
              tabs: [
                Text(
                  widget.firstElement,
                ),
                Text(
                  widget.secondElement,
                ),
                Text(
                  widget.thirdElement!,
                )
              ],
            ),
          );
  }
}

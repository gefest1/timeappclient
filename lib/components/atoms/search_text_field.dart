import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class SearchTextField extends StatefulWidget {
  final FocusNode? focusNode;
  final TextEditingController? controller;
  final String? hintText;

  const SearchTextField({
    this.focusNode,
    this.controller,
    this.hintText,
    Key? key,
  }) : super(key: key);

  @override
  _SearchTextFieldState createState() => _SearchTextFieldState();
}

class _SearchTextFieldState extends State<SearchTextField> {
  bool isChoosen = false;
  late final FocusNode focusNode = (widget.focusNode ?? FocusNode())
    ..addListener(_check);
  late final TextEditingController controller =
      widget.controller ?? TextEditingController();

  _check() {
    if (focusNode.hasFocus && !isChoosen) {
      setState(() {
        isChoosen = true;
      });
    } else if (!focusNode.hasFocus && isChoosen && controller.text.isEmpty) {
      setState(() {
        isChoosen = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      focusNode: focusNode,
      controller: controller,
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.only(
          left: 12,
          right: 12,
          top: 9.5,
          bottom: 11.5,
        ),
        prefixIcon: Center(
          child: Padding(
            padding: const EdgeInsets.only(
              left: 15,
              right: 5,
            ),
            child: RepaintBoundary(
              child: SvgPicture.asset(
                'assets/svg/icon/search.svg',
              ),
            ),
          ),
        ),
        prefixIconConstraints: BoxConstraints(
          minWidth: 40,
          maxWidth: 40,
          maxHeight: 40,
          minHeight: 40,
        ),
        fillColor: ColorData.allInnerDefault,
        filled: true,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(16),
          borderSide: BorderSide.none,
        ),
        hintText: 'Поиск',
        hintStyle: TextStyle(
          fontSize: P3TextStyle.fontSize,
          fontWeight: P3TextStyle.fontWeight,
          height: P3TextStyle.height,
          color: ColorData.allMainActivegray,
        ),
      ),
    );
  }
}

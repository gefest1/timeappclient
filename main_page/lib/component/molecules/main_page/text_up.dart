import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:profile/bloc/profile_bloc.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class TextUp extends StatelessWidget {
  const TextUp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        return RichText(
          maxLines: 2,
          text: TextSpan(
            style: TextStyle(
              fontSize: H2TextStyle.fontSize,
              fontWeight: H2TextStyle.fontWeight,
              height: H2TextStyle.height,
              color: ColorData.allMainBlack,
              fontFamily: Platform.isIOS ? 'SfProDisplay' : 'Roboto',
            ),
            children: state is NotRegistered
                ? [
                    const TextSpan(
                      text: 'Здравствуйте!',
                    ),
                  ]
                : [
                    const TextSpan(
                      text: 'Здравствуйте, ',
                    ),
                    if (state is ProfileInformation)
                      TextSpan(text: state.client.fullName),
                    const TextSpan(
                      text: '!',
                    ),
                  ],
          ),
        );
      },
    );
  }
}

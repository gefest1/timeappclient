import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/pages/qfit_to_timeapp/qfit_page.dart';
import 'package:li/main.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/text_styles.dart';
import 'package:time_app_components/utils/testStyles.dart';

class SoonOverlayTransition extends OverlayEntry {
  SoonOverlayTransition()
      : super(
          builder: (_) {
            return SizedBox();
          },
        );

  Widget _buider(BuildContext context) {
    return SoonOverlayTransitionWidget(
      overlayEntry: this,
    );
  }

  WidgetBuilder get builder => _buider;
}

class SoonOverlayTransitionWidget extends StatefulWidget {
  final SoonOverlayTransition overlayEntry;

  const SoonOverlayTransitionWidget({
    required this.overlayEntry,
    Key? key,
  }) : super(key: key);

  @override
  _SoonOverlayTransitionWidgetState createState() =>
      _SoonOverlayTransitionWidgetState();
}

class _SoonOverlayTransitionWidgetState
    extends State<SoonOverlayTransitionWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController animationController = AnimationController(
    vsync: this,
    duration: Duration(milliseconds: 300),
    value: 0,
  );

  void _init() async {
    await animationController.forward();
  }

  @override
  void initState() {
    super.initState();
    _init();
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      child: AnnotatedRegion(
        value: SystemUiOverlayStyle(statusBarBrightness: Brightness.dark),
        child: FadeTransition(
          opacity: animationController,
          child: Material(
            color: ColorData.allMainBlack,
            child: ListView(
              children: [
                Align(
                  alignment: Alignment.centerRight,
                  child: IconButton(
                    onPressed: () {
                      widget.overlayEntry.remove();
                    },
                    icon: SvgPicture.asset('assets/svg/icon/close.svg'),
                  ),
                ),
                SvgPicture.asset(
                  'assets/svg/soonIcon.svg',
                  height: MediaQuery.of(context).size.height * 0.3,
                ),
                const SizedBox(height: 40),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    'Уже скоро!',
                    style: TextStyle(
                      fontSize: H1TextStyle.fontSize,
                      height: H1TextStyle.height,
                      fontWeight: H1TextStyle.fontWeight,
                      color: Colors.white,
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    'Совсем скоро вы сможете мгновенно записываться на множество услуг здесь и сейчас: подстричься у хорошего барбера в пару шагов от вас или забронировать квартиру посуточно, точно зная, что она свободна. Пока вы ждете, попробуйте поминутную оплату фитнес-клубов и развлечений, вам понравится! :)',
                    style: TextStyle(
                      fontSize: 18,
                      height: 21 / 18,
                      fontWeight: FontWeight.w400,
                      color: Colors.white,
                    ),
                  ),
                ),
                const SizedBox(height: 40),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      fixedSize:
                          MaterialStateProperty.all(Size(double.infinity, 60)),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(16),
                        ),
                      ),
                      backgroundColor: MaterialStateProperty.all(
                        ColorData.clientsButtonColorDefault,
                      ),
                    ),
                    onPressed: () {
                      // log('DWADWAD');
                      widget.overlayEntry.remove();
                      routerDelegate.push(const QfitPage());
                    },
                    child: Text(
                      'К поминутной оплате!',
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 18,
                        height: 21 / 18,
                        color: ColorData.grayScaleOffWhite,
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 40),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

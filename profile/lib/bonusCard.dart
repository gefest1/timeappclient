import 'package:flutter/material.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/data/models/client_model.dart';
import 'package:li/main.dart';
import 'package:profile/BonusPage.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class BonusCard extends StatelessWidget {
  final double bonus;
  const BonusCard({required this.bonus, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: InkWell(
        // onTap: () {
        //   showModalBottomSheet(
        //     isScrollControlled: true,
        //     context: context,
        //     builder: (context) {
        //       return Wrap(
        //         children: [
        //           Container(
        //             child: Container(
        //                 decoration: new BoxDecoration(
        //                     color: Color(0xFF737373),
        //                     borderRadius: new BorderRadius.only(
        //                         topLeft: const Radius.circular(25.0),
        //                         topRight: const Radius.circular(25.0))),
        //                 child: (
        //                   appBar: CustomAppBar(
        //                     title: Text('Промокоды и баллы'),
        //                   ),
        //                 )),
        //           )
        //         ],
        //       );
        //     },
        //   );
        // },
        child: Container(
          decoration: BoxDecoration(
            image: const DecorationImage(
              image: AssetImage(
                'assets/Bonus.png',
              ),
              fit: BoxFit.cover,
            ),
            color: ColorData.clientsButtonColorDefault,
            borderRadius: BorderRadius.circular(15),
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              // ignore: prefer_const_literals_to_create_immutables
              children: [
                const Text(
                  'Промокоды и баллы',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                  ),
                ),
                SizedBox(height: 12),
                Text(
                  bonus.toStringAsFixed(0),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: H1TextStyle.fontSize,
                    height: H1TextStyle.height,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

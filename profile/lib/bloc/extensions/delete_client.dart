part of '../profile_bloc.dart';

extension DeleteClient on ProfileBloc {
  void deleteClient(
    DeleteClientEvent event,
    Emitter<ProfileState> emit,
  ) async {
    final _mutationResult = await graphqlProvider.client.mutate(
      MutationOptions(
        document: gql('''
      mutation{ 
	      deleteAccountClient
      }
    '''),
      ),
    );
    if (_mutationResult.hasException) throw _mutationResult.exception!;
    Future.wait([
      deleteID(
        deviceId: await getId(),
        provider: graphqlProvider,
      ),
      sharedPreferences.clear()
    ]);

    emit(DeleteAccountState());
  }
}

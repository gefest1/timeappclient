import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:li/data/models/institution_minute.dart';

import 'package:li/utils/date_time_parse.dart';

class ServiceMinute extends Equatable {
  final String? id;
  final InstitutionsMinute? institution;
  final String? name;
  final num? price;
  final DateTime? startTime;
  final DateTime? endTime;
  final String? description;
  final String? institutionId;
  final String? specialistId;
  final num? bufferTimeMinutes;

  const ServiceMinute({
    this.id,
    this.institution,
    this.name,
    this.price,
    this.startTime,
    this.endTime,
    this.description,
    this.institutionId,
    this.specialistId,
    this.bufferTimeMinutes,
  });

  ServiceMinute copyWith({
    String? id,
    InstitutionsMinute? institution,
    String? name,
    num? price,
    DateTime? startTime,
    DateTime? endTime,
  }) {
    return ServiceMinute(
      id: id ?? this.id,
      institution: institution ?? this.institution,
      name: name ?? this.name,
      price: price ?? this.price,
      startTime: startTime ?? this.startTime,
      endTime: endTime ?? this.endTime,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'institution': institution?.toMap(),
      'name': name,
      'price': price,
      'startTime': startTime?.toIso8601String(),
      'endTime': endTime?.toIso8601String(),
      'description': description,
      'institutionId': institutionId,
      'specialistId': specialistId,
      'bufferTimeMinutes': bufferTimeMinutes?.toInt(),
    };
  }

  static ServiceMinute? fromMap(Map<String, dynamic>? map) {
    if (map == null) return null;
    return ServiceMinute(
      id: map['_id'],
      institution: map['institution'] != null
          ? InstitutionsMinute.fromMap(map['institution'])
          : null,
      name: map['name'],
      price: map['price'] != null ? map['price'] : null,
      startTime: dateTimeParse(map['startTime']),
      endTime: dateTimeParse(map['endTime']),
      description: map['description'],
      institutionId: map['institutionId'],
      specialistId: map['specialistId'],
      bufferTimeMinutes: map['bufferTimeMinutes'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ServiceMinute.fromJson(String source) =>
      ServiceMinute.fromMap(json.decode(source))!;

  @override
  bool get stringify => true;

  @override
  List<Object?> get props {
    return [
      id,
      institution,
      name,
      price,
      startTime,
      endTime,
    ];
  }
}

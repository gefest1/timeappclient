part of '../session_minute_bloc.dart';

extension FinishReaction on SessionMinuteBloc {
  void finishSession(
    FinishSessionEvent event,
    Emitter<SessionState> emit,
  ) async {
    final _initBlock = !blockBackRoutes;
    if (_initBlock) routerDelegate.blockWidgets();

    try {
      final _sessionMinute = await FinishSession.finishSession(
        sessionId: event.sessionId.id!,
        client: graphqlMinuteProvider.client,
      );
      final List<SessionMinute> _activeSessions;
      final _state = state;
      if (_state is StartedSessionState) {
        _activeSessions = [..._state.sessions]..removeWhere((SessionMinute _v) {
            return _v.id == event.sessionId.id;
          });
      } else {
        _activeSessions = [];
      }

      if (_initBlock) routerDelegate.unBlockWidgets();
      navigatorKey.currentState?.popUntil(
        (route) => route.isFirst,
      );

      event.completer.complete(_sessionMinute);
      emit(StartedSessionState(sessions: _activeSessions));

      return;
    } catch (e) {
      // log(e.toString());
      event.completer.completeError(e);
    }
    if (_initBlock) routerDelegate.unBlockWidgets();
  }
}

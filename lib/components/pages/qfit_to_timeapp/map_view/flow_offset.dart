import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

import 'package:li/components/atoms/map_pointer.dart';
import 'package:li/components/pages/qfit_to_timeapp/qfit_detail_page.dart';
import 'package:li/data/models/institution_minute.dart';
import 'package:li/logic/providers/geo_institution/GeoInstitutionProvider.dart';
import 'package:li/main.dart';
import 'package:li/utils/flow_menu_delegate.dart';
import 'package:li/utils/locationsBetween.dart';
import 'package:li/utils/offset_flow_mixin.dart';

extension ListenFlow on _FlowOffsetsState {
  void listenChanges([Null _]) async {
    if (yandexMapController == null) return;
    if (notFinished) return;

    notFinished = true;
    final _insts = _geoProvider.institutionsMinuteList;
    final List<Offset?> _newOffsets = await Future.wait(
      _insts.map(
        (e) => getOffset(
          latitude: e.location!.latitude!.toDouble(),
          longitude: e.location!.longitude!.toDouble(),
        ),
      ),
    );
    setState(() {
      _institutions = _insts;
    });
    offsetListen.value = _newOffsets.map((e) => e!).toList();
    notFinished = false;
  }
}

class FlowOffsets extends StatefulWidget {
  final YandexMapController yandexMapController;
  final StreamController<Null> streamController;
  final Point? myLocation;

  const FlowOffsets({
    required this.yandexMapController,
    required this.streamController,
    this.myLocation,
    Key? key,
  }) : super(key: key);

  @override
  _FlowOffsetsState createState() => _FlowOffsetsState();
}

class _FlowOffsetsState extends State<FlowOffsets> with OffsetExtension {
  late GeoInstitutionProvider _geoProvider =
      Provider.of<GeoInstitutionProvider>(context, listen: false);
  final ValueNotifier<List<Offset>> offsetListen =
      ValueNotifier<List<Offset>>([]);
  late StreamSubscription<Null> streamSubscription;
  bool notFinished = false;
  List<InstitutionsMinute> _institutions = [];

  @override
  void initState() {
    super.initState();
    yandexMapController = widget.yandexMapController;
    _initS();
    streamSubscription = widget.streamController.stream.listen(listenChanges);
  }

  @override
  void dispose() {
    _geoProvider.removeListener(listenChanges);
    streamSubscription.cancel();
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant FlowOffsets oldWidget) {
    if (oldWidget.myLocation == null && widget.myLocation != null)
      _geoProvider.getInstituion(
        point: widget.myLocation!,
        maxRadius: 5000,
        minRadius: 0,
      );

    super.didUpdateWidget(oldWidget);
  }

  void _initS() async {
    if (widget.myLocation != null)
      _geoProvider.getInstituion(
        point: widget.myLocation!,
        maxRadius: 5000,
        minRadius: 0,
      );
    _geoProvider.addListener(listenChanges);
  }

  @override
  void setState(VoidCallback fn) => super.setState(fn);

  @override
  Widget build(BuildContext context) {
    return Flow(
        delegate: FlowMenuDelegate(
          offsetListen: offsetListen,
        ),
        children: _institutions
            .map(
              (club) => MapPointer(
                key: ValueKey(club.id),
                title: '${club.averagePrice} ₸/мин',
                label: club.name,
                tile: widget.myLocation == null
                    ? null
                    : locationBetweenText(
                        club.location!.latitude!.toDouble(),
                        club.location!.longitude!.toDouble(),
                        widget.myLocation!.latitude,
                        widget.myLocation!.longitude,
                      ),
                onTap: () {
                  routerDelegate.push(
                    QfitDetailPage(
                      institution: club,
                    ),
                  );
                },
              ),
            )
            .toList());
  }
}

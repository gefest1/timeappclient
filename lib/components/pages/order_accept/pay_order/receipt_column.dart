import 'package:flutter/material.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class ReceiptAtom extends StatelessWidget {
  final String title;
  final TextSpan subTitle;

  const ReceiptAtom({
    required this.title,
    required this.subTitle,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Text(
            title,
            style: TextStyle(
              fontSize: P3TextStyle.fontSize,
              fontWeight: P3TextStyle.fontWeight,
              height: P3TextStyle.height,
              color: ColorData.allMainActivegray,
            ),
          ),
        ),
        const SizedBox(width: 10),
        RichText(text: subTitle),
      ],
    );
  }
}

class ReceiptColumn extends StatelessWidget {
  const ReceiptColumn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Счет',
          style: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.w600,
            color: ColorData.allMainBlack,
          ),
        ),
        SizedBox(height: 10),
        Column(
          children: [
            ReceiptAtom(
              title: 'Сумма заказа',
              subTitle: TextSpan(
                children: [
                  TextSpan(
                    text: '1550 ',
                    style: TextStyle(
                      fontSize: P3TextStyle.fontSize,
                      fontWeight: P3TextStyle.fontWeight,
                      height: P3TextStyle.height,
                      color: ColorData.allMainBlack,
                    ),
                  ),
                  TextSpan(
                    text: '₸',
                    style: TextStyle(
                      fontFamily: null,
                      fontSize: P3TextStyle.fontSize,
                      fontWeight: P3TextStyle.fontWeight,
                      height: P3TextStyle.height,
                      color: ColorData.allMainBlack,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 10),
            ReceiptAtom(
              title: 'Доставка',
              subTitle: TextSpan(
                children: [
                  TextSpan(
                    text: '400 ',
                    style: TextStyle(
                      fontSize: P3TextStyle.fontSize,
                      fontWeight: P3TextStyle.fontWeight,
                      height: P3TextStyle.height,
                      color: ColorData.allMainBlack,
                    ),
                  ),
                  TextSpan(
                    text: '₸',
                    style: TextStyle(
                      fontFamily: null,
                      fontSize: P3TextStyle.fontSize,
                      fontWeight: P3TextStyle.fontWeight,
                      height: P3TextStyle.height,
                      color: ColorData.allMainBlack,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 20),
            Divider(
              height: 0,
              thickness: 0.5,
              color: ColorData.allMainGray,
            ),
            const SizedBox(height: 15),
            ReceiptAtom(
              title: 'Всего',
              subTitle: TextSpan(
                children: [
                  TextSpan(
                    text: '${(1550 / 0.962).toStringAsFixed(1)} ',
                    style: TextStyle(
                      fontSize: P3TextStyle.fontSize,
                      fontWeight: P3TextStyle.fontWeight,
                      height: P3TextStyle.height,
                      color: ColorData.allMainBlack,
                    ),
                  ),
                  TextSpan(
                    text: '₸',
                    style: TextStyle(
                      fontFamily: null,
                      fontSize: P3TextStyle.fontSize,
                      fontWeight: P3TextStyle.fontWeight,
                      height: P3TextStyle.height,
                      color: ColorData.allMainBlack,
                    ),
                  ),
                ],
              ),
            ),
          ],
        )
      ],
    );
  }
}

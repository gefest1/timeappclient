import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:main_page/main_app_categories_tile.dart';

import 'package:main_page/model/main_slug_model.dart';

const List<String> ready = ['minute'];

class WrapCategory extends StatelessWidget {
  final List<MainSlugModel> list;
  final void Function(int index) onTap;

  const WrapCategory({
    required this.list,
    required this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (list.length < 4) return const SizedBox();
    return IntrinsicHeight(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: Column(
              children: [
                MainAppCategoriesTile(
                  onTap: () => onTap(0),
                  title: list[0].title!.ru!,
                  subtitle: list[0].subTitle!.ru!,
                  svgPath: list[0].photoSvg!,
                  notReady: !ready.contains(list[0].slug),
                  height: 160,
                ),
                const SizedBox(height: 10),
                AbsorbPointer(
                  child: MainAppCategoriesTile(
                    onTap: () => onTap(2),
                    title: list[2].title!.ru!,
                    notReady: !ready.contains(list[2].slug),
                    subtitle: list[2].subTitle!.ru!,
                    svgPath: list[2].photoSvg!,
                    height: 140,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(width: 10),
          Expanded(
            child: Column(
              children: [
                MainAppCategoriesTile(
                  onTap: () => onTap(1),
                  title: list[1].title!.ru!,
                  subtitle: list[1].subTitle!.ru!,
                  svgPath: list[1].photoSvg!,
                  height: 160,
                  notReady: !ready.contains(list[1].slug),
                ),
                const SizedBox(height: 10),
                MainAppCategoriesTile(
                  onTap: () => onTap(3),
                  title: list[3].title!.ru!,
                  subtitle: list[3].subTitle!.ru!,
                  svgPath: list[3].photoSvg!,
                  height: 160,
                  notReady: !ready.contains(list[3].slug),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:li/const.dart';

class FastCard extends StatelessWidget {
  const FastCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 168,
      width: 264,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: bigBlur,
        borderRadius: BorderRadius.circular(16),
      ),
      padding: EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 20,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 24,
            width: 24,
            color: Colors.blue,
          ),
          const SizedBox(height: 20),
          const Text(
            'Недвижимость и туризм',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              height: 19 / 16,
              color: Colors.black,
            ),
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
          ),
          const SizedBox(height: 4),
          const Text(
            'Снять квартиру посуточно, студия, 1 комн., до 15 тыс. тенге',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w400,
              height: 19 / 16,
              color: Colors.black,
            ),
            overflow: TextOverflow.ellipsis,
            maxLines: 3,
          ),
        ],
      ),
    );
  }
}

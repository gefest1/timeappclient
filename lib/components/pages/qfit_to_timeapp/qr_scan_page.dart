import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/pages/qfit_to_timeapp/qfit_start_bottom.dart';
import 'package:li/data/models/client_model.dart';
import 'package:li/logic/blocs/minute/MinuteInstitutionEvent.dart';
import 'package:li/logic/blocs/minute/MinuteInstitutionState.dart';
import 'package:li/logic/blocs/minute/MinuteInstiutionBloc.dart';
import 'package:li/logic/navigation.dart';
import 'package:li/utils/color.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'dart:math' as math;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:time_app_components/utils/testStyles.dart';

class QrScanPage extends StatefulWidget {
  const QrScanPage({Key? key}) : super(key: key);

  @override
  _QrScanPageState createState() => _QrScanPageState();
}

class _QrScanPageState extends State<QrScanPage> {
  StreamSubscription<Barcode>? subscription;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  bool finish = false;
  late QRViewController _controller;
  CollectionReference session =
      FirebaseFirestore.instance.collection('testingminute');
  Client? client;

  @override
  void dispose() {
    subscription?.cancel();
    _controller.dispose();
    super.dispose();
  }

  Future<void> fetchedReaction({
    required FetchedService result,
    required String institutionId,
  }) async {
    if (!mounted) return;
    final _mediaQuery = MediaQuery.of(context);
    await showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      useRootNavigator: true,
      isScrollControlled: true,
      builder: (_ctx) => WillPopScope(
        onWillPop: () async {
          return !blockBackRoutes;
        },
        child: SizedBox(
          height: _mediaQuery.size.height - _mediaQuery.padding.top - 20,
          child: QFitStartBottom(
            service: result.data,
            institutionId: institutionId,
          ),
        ),
      ),
    );
  }

  Future<void> startByInstituton(String institutionId) async {
    finish = true;
    _controller.pauseCamera();
    try {
      final Completer<MinuteInstitutionState> _completer =
          Completer<MinuteInstitutionState>();
      BlocProvider.of<MinuteInstitutionBloc>(context, listen: false).add(
        GetCurrentServiceEvent(
          institutionId: institutionId,
          state: _completer,
        ),
      );
      final result = await _completer.future;

      if (result is FetchedService)
        await fetchedReaction(institutionId: institutionId, result: result);

      //
    } catch (e) {}
    if (!mounted) return;
    _controller.resumeCamera();
    finish = false;
  }

  Future<void> startByService(String serviceId) async {
    finish = true;
    _controller.pauseCamera();
    try {
      final Completer<MinuteInstitutionState> _completer =
          Completer<MinuteInstitutionState>();
      BlocProvider.of<MinuteInstitutionBloc>(context, listen: false).add(
        GetServiceEvent(
          serviceId: serviceId,
          state: _completer,
        ),
      );
      final result = await _completer.future;

      if (result is FetchedService)
        await fetchedReaction(
          institutionId: result.data.first.institution!.id!,
          result: result,
        );

      //
    } catch (e) {}
    if (!mounted) return;
    _controller.resumeCamera();
    finish = false;
  }

  void _listen(Barcode event) async {
    if (finish) return;
    if (event.code == null) return;
    if (event.code!.contains('https://timeapp.kz/minutePay/')) {
      final _uri = Uri.dataFromString(event.code ?? '');

      if (_uri.queryParameters['serviceId'] != null) {
        startByService(_uri.queryParameters['serviceId']!);
        return;
      }

      if (_uri.queryParameters['institutionId'] != null) {
        startByInstituton(_uri.queryParameters['institutionId']!);
        return;
      }
    }
  }

  void _onCretead(QRViewController contreller) {
    _controller = contreller;
    subscription = contreller.scannedDataStream.listen(_listen);
  }

  @override
  Widget build(BuildContext context) {
    final _mediaQuery = MediaQuery.of(context);
    final _cubit =
        math.min(_mediaQuery.size.height, _mediaQuery.size.width) * 0.64;
    return Material(
      color: Colors.black,
      child: Stack(
        children: [
          QRView(
            key: qrKey,
            onQRViewCreated: _onCretead,
            cameraFacing: kReleaseMode ? CameraFacing.back : CameraFacing.back,
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: CustomAppBar(
              title: Text(
                'Начало сессии',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 18,
                  height: 21 / 18,
                  color: ColorData.allMainBlack,
                ),
              ),
              leading: IconButton(
                icon: SvgPicture.asset('assets/svg/icon/backArrow.svg'),
                onPressed: () => Navigator.pop(context),
              ),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).padding.top + 56,
            bottom: 0,
            right: 0,
            left: 0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                  decoration: BoxDecoration(
                    color: Color(0xb3000000),
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: Text(
                    'Наведите камеру на QR-код',
                    style: TextStyle(
                      fontSize: P3TextStyle.fontSize,
                      fontWeight: P3TextStyle.fontWeight,
                      height: P3TextStyle.height,
                      color: Colors.white,
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                CustomPaint(
                  child: SizedBox(
                    height: _cubit,
                    width: _cubit,
                  ),
                  painter: ScanAreaPainter(),
                ),
                const SizedBox(height: 100),
              ],
            ),
          ),
          Positioned(
            bottom: MediaQuery.of(context).padding.bottom + 56,
            left: 32,
            right: 32,
            child: Text(
              'Убедитесь, что вы взяли все необходимое для начала сеанса',
              style: TextStyle(
                fontSize: P3TextStyle.fontSize,
                height: P3TextStyle.height,
                fontWeight: P3TextStyle.fontWeight,
                color: Colors.white,
              ),
              textAlign: TextAlign.center,
            ),
          )
        ],
      ),
    );
  }
}

class ScanAreaPainter extends CustomPainter {
  draw(Canvas canvas, Size size, bool x, bool y) {
    canvas.drawPath(
      Path()
        ..moveTo((x ? 0 : 1) * size.width, (y ? 0.4 : 0.6) * size.height)
        ..lineTo((x ? 0 : 1) * size.width, (y ? 0.1 : 0.9) * size.height)
        ..cubicTo(
          (x ? 0 : 1) * size.width,
          (y ? 0.05 : 0.95) * size.height,
          (x ? 0.05 : 0.95) * size.width,
          (y ? 0 : 1) * size.height,
          (x ? 0.1 : 0.9) * size.width,
          (y ? 0 : 1) * size.height,
        )
        ..lineTo((x ? 0.4 : 0.6) * size.width, (y ? 0 : 1) * size.height),
      Paint()
        ..strokeWidth = 4
        ..color = Colors.white
        ..strokeCap = StrokeCap.round
        ..style = PaintingStyle.stroke,
    );
  }

  @override
  void paint(Canvas canvas, Size size) {
    draw(canvas, size, false, false);
    draw(canvas, size, false, true);
    draw(canvas, size, true, false);
    draw(canvas, size, true, true);
  }

  @override
  bool shouldRepaint(ScanAreaPainter oldDelegate) => false;

  @override
  bool shouldRebuildSemantics(ScanAreaPainter oldDelegate) => false;
}

import 'package:flutter/material.dart';

class BlocWidget extends StatefulWidget {
  final Widget child;
  final BuildContext parentContext;

  const BlocWidget({
    required this.child,
    required this.parentContext,
    Key? key,
  }) : super(key: key);

  @override
  BlocWidgetState createState() => BlocWidgetState();
}

class BlocWidgetState extends State<BlocWidget> {
  ModalRoute<dynamic>? _route;

  Future<bool> _blocAction() async {
    // log('message');
    return false;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _route?.removeScopedWillPopCallback(_blocAction);
    _route = ModalRoute.of(widget.parentContext);

    _route?.addScopedWillPopCallback(_blocAction);
  }

  @override
  void dispose() {
    _route?.removeScopedWillPopCallback(_blocAction);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}

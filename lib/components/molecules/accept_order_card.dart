import 'package:flutter/material.dart';
import 'package:li/components/pages/order_accept/shipping_page.dart';
import 'package:li/const.dart';
import 'package:li/main.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class AcceptOrderCard extends StatefulWidget {
  final String institutionId;

  const AcceptOrderCard({
    required this.institutionId,
    Key? key,
  }) : super(key: key);

  @override
  _AcceptOrderCardState createState() => _AcceptOrderCardState();
}

class _AcceptOrderCardState extends State<AcceptOrderCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).padding.bottom + 40 + 32,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.vertical(top: Radius.circular(16)),
        boxShadow: bigBlur,
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: ElevatedButton(
                onPressed: () {
                  routerDelegate.push(ShippingPage(
                    institutionId: widget.institutionId,
                  ));
                },
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                      ColorData.clientsButtonColorDefault),
                  fixedSize: MaterialStateProperty.all(
                    Size(80, 40),
                  ),
                  elevation: MaterialStateProperty.all(0),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16),
                    ),
                  ),
                ),
                child: Text(
                  'Подтвердить заказ',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: P4TextStyle.fontSize,
                    fontWeight: P4TextStyle.fontWeight,
                    height: P4TextStyle.height,
                  ),
                ),
              ),
            ),
            const SizedBox(width: 10),
            Expanded(
              flex: 1,
              child: ElevatedButton(
                onPressed: () {},
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                    Colors.transparent,
                  ),
                  fixedSize: MaterialStateProperty.all(
                    Size(80, 40),
                  ),
                  elevation: MaterialStateProperty.all(0),
                  overlayColor: MaterialStateProperty.all(
                    ColorData.clientsButtonColorDefault.withOpacity(0.16),
                  ),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16),
                      side: BorderSide(
                        color: ColorData.clientsButtonColorDefault,
                      ),
                    ),
                  ),
                ),
                child: Text(
                  'Написать',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: ColorData.clientsButtonColorDefault,
                    fontSize: P4TextStyle.fontSize,
                    fontWeight: P4TextStyle.fontWeight,
                    height: P4TextStyle.height,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

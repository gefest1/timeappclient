// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';

// Size(WIDTH, (WIDTH*0.8947368421052632).toDouble()),

class UnionCustomPainter extends CustomPainter {
  const UnionCustomPainter({Listenable? repaint}) : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.08055211, size.height * 0.09031471);
    path_0.cubicTo(
        size.width * -0.02685084,
        size.height * 0.2107341,
        size.width * -0.02685074,
        size.height * 0.4059724,
        size.width * 0.08055263,
        size.height * 0.5263918);
    path_0.lineTo(size.width * 0.4971347, size.height * 0.9934588);
    path_0.lineTo(size.width * 0.4999984, size.height * 0.9902471);
    path_0.lineTo(size.width * 0.5028653, size.height * 0.9934647);
    path_0.lineTo(size.width * 0.9194474, size.height * 0.5263959);
    path_0.cubicTo(
        size.width * 1.026853,
        size.height * 0.4059765,
        size.width * 1.026853,
        size.height * 0.2107376,
        size.width * 0.9194474,
        size.height * 0.09031824);
    path_0.cubicTo(
        size.width * 0.8120421,
        size.height * -0.03010094,
        size.width * 0.6379105,
        size.height * -0.03010082,
        size.width * 0.5305053,
        size.height * 0.09031882);
    path_0.lineTo(size.width * 0.5186395, size.height * 0.1036235);
    path_0.cubicTo(
        size.width * 0.5083558,
        size.height * 0.1151535,
        size.width * 0.4916479,
        size.height * 0.1151535,
        size.width * 0.4813642,
        size.height * 0.1036235);
    path_0.lineTo(size.width * 0.4694937, size.height * 0.09031471);
    path_0.cubicTo(
        size.width * 0.3620905,
        size.height * -0.03010488,
        size.width * 0.1879553,
        size.height * -0.03010482,
        size.width * 0.08055211,
        size.height * 0.09031471);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Color(0xff979797).withOpacity(1.0);
    canvas.drawPath(path_0, paint_0_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}

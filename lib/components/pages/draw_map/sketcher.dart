import 'package:flutter/material.dart';

import 'drawn_line.dart';

class Sketcher extends CustomPainter {
  final DrawnLine? line;

  Sketcher({this.line});

  @override
  void paint(Canvas canvas, Size size) {
    final _path = new Path();
    if (line == null) return;
    final _line = line!;
    _path.moveTo(
      _line.path.first.dx,
      _line.path.first.dy,
    );

    for (int j = 0; j < _line.path.length - 1; ++j) {
      _path.lineTo(
        _line.path[j + 1].dx,
        _line.path[j + 1].dy,
      );
    }

    canvas.drawPath(
      _path,
      Paint()
        ..color = _line.color
        ..style = PaintingStyle.stroke
        ..strokeWidth = _line.width,
    );

    canvas.drawPath(
      _path,
      Paint()
        ..color = _line.fillColor
        ..style = PaintingStyle.fill,
    );
  }

  @override
  bool shouldRepaint(Sketcher oldDelegate) => true;
}

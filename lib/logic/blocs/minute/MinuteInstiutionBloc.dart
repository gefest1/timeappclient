import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/data/models/service_model.dart';
import 'package:li/logic/blocs/minute/MinuteInstitutionEvent.dart';
import 'package:li/logic/blocs/minute/MinuteInstitutionState.dart';
import 'package:li/main.dart';
import 'package:li/network/graphql_provider.dart';

part 'reactions/get_data.dart';
part 'reactions/get_service.dart';

class MinuteInstitutionBloc
    extends Bloc<MinuteInstitutionEvent, MinuteInstitutionState> {
  final GraphqlMinuteProvider graphqlProvider;

  MinuteInstitutionBloc(this.graphqlProvider)
      : super(MinuteInstitutionState()) {
    on<GetCurrentServiceEvent>(getCurrentService);
    on<GetServiceEvent>(getService);
  }
}

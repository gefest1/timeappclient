import 'package:flutter/material.dart';
import 'package:li/components/molecules/chat_body.dart';

class ChatCard extends StatefulWidget {
  const ChatCard({Key? key}) : super(key: key);

  @override
  _ChatCardState createState() => _ChatCardState();
}

class _ChatCardState extends State<ChatCard> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CircleAvatar(
              minRadius: 30,
              child: Text(
                'N',
                style: TextStyle(fontSize: 35),
              ),
            ),
            const SizedBox(width: 10),
            Expanded(
              child: ChatBody(),
            ),
          ],
        ),
        Divider(
          height: 0,
          thickness: 0.2,
          color: Color(0xffB7B7B7),
          indent: 70,
        ),
      ],
    );
  }
}

import 'package:geolocator/geolocator.dart';

String locationBetweenText(double lat1, double lon1, double lat2, double lon2) {
  final _dis = Geolocator.distanceBetween(lat1, lon1, lat2, lon2);
  if (_dis < 1000) {
    return _dis.toStringAsFixed(0) + 'м';
  } else {
    return (_dis / 1000).toStringAsFixed(1) + 'км';
  }
}

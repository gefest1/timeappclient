import 'package:flutter_svg/flutter_svg.dart';
import 'package:li/components/pages/auth/choose_location_page.dart';
import 'package:flutter/material.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class MapWorPage extends StatefulWidget {
  const MapWorPage({Key? key}) : super(key: key);

  @override
  _MapWorPageState createState() => _MapWorPageState();
}

class _MapWorPageState extends State<MapWorPage> {
  late final _mediaQuery = MediaQuery.of(context);
  String? location;

  Widget get _bottomButton => GestureDetector(
        onTap: () async {
          final res = await showModalBottomSheet<String>(
            context: context,
            isScrollControlled: true,
            useRootNavigator: true,
            backgroundColor: Colors.transparent,
            builder: (BuildContext _context) {
              return GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {
                  Navigator.pop(_context);
                },
                child: Padding(
                  padding: EdgeInsets.only(top: _mediaQuery.padding.top),
                  child: const ChooseLocationPage(),
                ),
              );
            },
          );
          if (res == null) return;
          setState(() {
            location = res;
          });
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              const SizedBox(height: 20),
              SizedBox(
                height: 60,
                child: Container(
                  decoration: BoxDecoration(
                    color: location == null
                        ? Color(0xffFAFAFA)
                        : Color(0xfffF9FBFF),
                    borderRadius: BorderRadius.circular(16),
                    border: location == null
                        ? Border.all(
                            color: Color(0xffC9C9C9),
                            width: 1,
                          )
                        : Border.all(
                            color: Color(0xff0F84F4),
                            width: 1,
                          ),
                  ),
                  child: Row(
                    children: [
                      const SizedBox(width: 20),
                      SvgPicture.asset('assets/svg/icon/location.svg'),
                      const SizedBox(width: 10),
                      Expanded(
                        child: location == null
                            ? Text(
                                'Укажите улицу и дом',
                                style: TextStyle(
                                  color: Color(0xff979797),
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400,
                                ),
                              )
                            : Text(
                                location!,
                                style: TextStyle(
                                  color: Color(0xff434343),
                                  fontSize: 18,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                      ),
                    ],
                  ),
                ),
              ),
              if (location != null) ...[
                const SizedBox(height: 10),
                SizedBox(
                  height: 60,
                  width: double.infinity,
                  child: ElevatedButton(
                    onPressed: () {},
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(Color(0xfff007AFF)),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(16),
                        ),
                      ),
                      elevation: MaterialStateProperty.resolveWith(
                        (states) =>
                            states.contains(MaterialState.pressed) ? 2 : 0,
                      ),
                    ),
                    child: Text(
                      'Завершить регистрацию',
                      style: TextStyle(
                        color: Color(0xffFCFCFC),
                        fontWeight: FontWeight.w400,
                        fontSize: 18,
                      ),
                    ),
                  ),
                )
              ]
            ],
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        children: [
          YandexMap(),
          Align(
            alignment: Alignment.topCenter,
            child: Column(
              children: [
                SizedBox(height: _mediaQuery.padding.top),
                Padding(
                  padding: EdgeInsets.all(20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          height: 34,
                          width: 34,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(6),
                              boxShadow: [
                                BoxShadow(
                                  offset: Offset(1, 1),
                                  blurRadius: 5,
                                  color: Colors.black.withOpacity(0.1),
                                )
                              ]),
                          alignment: Alignment.center,
                          child:
                              SvgPicture.asset('assets/svg/icon/backArrow.svg'),
                        ),
                      ),
                      IgnorePointer(
                        child: RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: 'Адрес —',
                                style: TextStyle(
                                  color: Color(0xff323232),
                                  fontSize: 14,
                                ),
                              ),
                              TextSpan(
                                text: ' 6 из 6',
                                style: TextStyle(
                                  color: Color(0xff323232),
                                  fontSize: 14,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                IgnorePointer(
                  child: Text(
                    'Где вы\nработаете?',
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontSize: 38,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                )
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Column(
              children: [
                Align(
                  alignment: Alignment.centerRight,
                  child: Container(
                    margin: const EdgeInsets.only(right: 10),
                    height: 44,
                    width: 44,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.circle,
                    ),
                    alignment: Alignment.center,
                    child: SvgPicture.asset('assets/svg/icon/send.svg'),
                  ),
                ),
                const SizedBox(height: 20),
                Material(
                  color: Colors.white,
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(16),
                  ),
                  child: Padding(
                    padding:
                        EdgeInsets.only(bottom: _mediaQuery.padding.bottom),
                    child: _bottomButton,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

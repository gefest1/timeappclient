class AllowedInstitutionTypes {
  final String _value;

  const AllowedInstitutionTypes._internal(this._value);

  String toString() => _value;
  String toMap() => _value;

  static const ActiveLeisure =
      const AllowedInstitutionTypes._internal('ActiveLeisure');
  static const Entertainment =
      const AllowedInstitutionTypes._internal('Entertainment');
  static const Fitness = const AllowedInstitutionTypes._internal('Fitness');
  static const BusinessCenter =
      AllowedInstitutionTypes._internal('BusinessCenter');

  static AllowedInstitutionTypes? fromMap(String? value) {
    if ('ActiveLeisure' == value) return ActiveLeisure;
    if ('Entertainment' == value) return Entertainment;
    if ('Fitness' == value) return Fitness;
    if ('BusinessCenter' == value) return BusinessCenter;
    return null;
  }
}

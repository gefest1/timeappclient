// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:li/logic/provider_template.dart';
import 'package:li/main.dart';
import 'package:li/template/bloc_template.dart';
import 'package:li/utils/restart_widget.dart';

// import 'package:tube/main.dart';

void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    await tester.pumpWidget(const RestartWidget(
      child: const ProviderTemplate(
        child: const BlocTemplate(
          child: const MyApp(),
        ),
      ),
    ));
  });
}

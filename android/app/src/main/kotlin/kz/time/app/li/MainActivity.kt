package kz.time.app

import io.flutter.embedding.android.FlutterActivity
import androidx.annotation.NonNull
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugins.GeneratedPluginRegistrant
import com.yandex.mapkit.MapKitFactory
import io.flutter.embedding.android.FlutterFragmentActivity

class MainActivity:  FlutterFragmentActivity(){
  override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
    MapKitFactory.setApiKey("b027f11a-013d-4eb2-ad19-9550bc6832af")
    super.configureFlutterEngine(flutterEngine)
  }
}
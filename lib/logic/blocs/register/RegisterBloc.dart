import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/components/pages/auth/how_to_show_to_spec_page.dart';
import 'package:li/data/models/client_model.dart';
import 'package:li/logic/blocs/register/RegisterEvent.dart';
import 'package:li/logic/blocs/register/RegisterState.dart';
import 'package:li/main.dart';
import 'package:li/network/graphql_provider.dart';
import 'package:li/utils/restart_widget.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final GraphqlProvider graphqlProvider;

  RegisterBloc(
    this.graphqlProvider,
  ) : super(RegisterState()) {
    on<SendCodeRegisterEvent>(sendCode);
    on<CheckSMSRegisterEvent>(checkSMS);
  }
  static RegisterBloc of(
    BuildContext context, {
    bool listen = false,
  }) {
    return BlocProvider.of<RegisterBloc>(context, listen: listen);
  }

  void sendCode(
    SendCodeRegisterEvent event,
    Emitter<RegisterState> emit,
  ) async {
    try {
      final _data = await graphqlProvider.client.mutate(
        MutationOptions(
          document: gql(''' 
          mutation(\$phoneNumber:String!){
            sendVerificationSMS(phoneNumber: \$phoneNumber)
          }'''),
          variables: {'phoneNumber': event.phoneNumber},
        ),
      );
      if (_data.hasException) throw _data.exception!;

      return;
    } catch (e) {
      navigatorKey.currentState?.popUntil((route) => route.isFirst);
    }
  }

  void checkSMS(
    CheckSMSRegisterEvent event,
    Emitter<RegisterState> emit,
  ) async {
    try {
      emit(CheckSMSLoadingState());
      final _mutationResult = await graphqlProvider.client.mutate(
        MutationOptions(
          document: gql(''' 
          mutation(\$phoneNumber: String! \$code: String!) {
            checkSMSVerificationCodeAsClient(
              phoneNumber: \$phoneNumber
              code: \$code
            ) {
              token
              client
              {
                _id     
                dateOfBirth
                email
                fullName
                phoneNumber
                photoURL {
                  M
                  XL
                  thumbnail
                }
                sex
              }
            }
          }'''),
          variables: {
            'phoneNumber': event.phoneNumber,
            'code': event.code,
          },
        ),
      );
      emit(RegisterState());
      if (_mutationResult.hasException) throw _mutationResult.exception!;
      await sharedPreferences.setString(
        'token',
        _mutationResult.data!['checkSMSVerificationCodeAsClient']['token'],
      );
      final _dirtData =
          _mutationResult.data!['checkSMSVerificationCodeAsClient']['client'];
      final _client = _dirtData == null ? null : Client.fromMap(_dirtData);
      if (_client == null) {
        navigatorKey.currentState?.push(
          MaterialPageRoute(
            builder: (_) => HowToShowToSpecPage(phone: event.phoneNumber),
          ),
        );
        return;
      } else {
        await sharedPreferences.setString('client', _client.toJson());
        RestartWidget.restartApp(navigatorKey.currentContext!);
      }
    } catch (e) {}
  }

  void registerData() {}
}

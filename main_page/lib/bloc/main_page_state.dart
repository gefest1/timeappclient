part of 'main_page_bloc.dart';

abstract class MainPageState extends Equatable {
  const MainPageState();

  @override
  List<Object> get props => [];
}

class MainPageInitial extends MainPageState {
  const MainPageInitial();
}

class FetchedData extends MainPageState {
  final List<MainSlugModel> mainAppCategoriesModel;
  final List<CategoryModel> specialCategoriesMain;
  final List<CategoryModel> categoriesMain;

  const FetchedData({
    required this.specialCategoriesMain,
    required this.categoriesMain,
    required this.mainAppCategoriesModel,
  });
}

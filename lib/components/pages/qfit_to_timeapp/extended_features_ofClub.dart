import 'package:flutter/material.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/const.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/text_styles.dart';
import 'package:time_app_components/utils/testStyles.dart';

class ExtendedFeaturesCard extends StatelessWidget {
  final VoidCallback onTap;
  final String price;
  final String title;
  final String subTitle;

  const ExtendedFeaturesCard({
    required this.onTap,
    required this.price,
    required this.title,
    required this.subTitle,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: bigBlur,
            borderRadius: BorderRadius.circular(15),
          ),
          padding: const EdgeInsets.all(20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: TextStyle(
                      fontSize: H3TextStyle.fontSize,
                      fontWeight: H3TextStyle.fontWeight,
                      height: H3TextStyle.height,
                    ),
                  ),
                  Text(
                    subTitle,
                    style: TextStyle(
                      color: ColorData.clientsButtonColorPressed,
                    ),
                  ),
                ],
              ),
              Container(
                decoration: BoxDecoration(
                  color: ColorData.allMainLightgray,
                  borderRadius: BorderRadius.circular(6),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                    vertical: 10,
                  ),
                  child: Text(
                    price + ' ₸/мин',
                    style: TextStyle(
                      color: ColorData.allMainGray,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

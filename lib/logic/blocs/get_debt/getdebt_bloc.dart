import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/data/models/client_model.dart';
import 'package:li/logic/blocs/get_debt/debt_api/pay_debt.dart';
import 'package:li/main.dart';

import 'package:li/network/graphql_provider.dart';

part 'getdebt_event.dart';
part 'getdebt_state.dart';

// REACTIONS
part 'reactions/close_debt.dart';
part 'reactions/get_data.dart';

class GetdebtBloc extends Bloc<GetdebtEvent, GetdebtState> {
  final GraphqlProvider graphqlProvider;
  final GraphqlMinuteProvider graphqlMinuteProvider;

  GetdebtBloc(
    this.graphqlProvider,
    this.graphqlMinuteProvider,
  ) : super(LoadingDebtState()) {
    on<GetCurrentDebtOfUser>(getClient);
    on<CloseDebtEvent>(closeDebtEvent);

    if (isRegistered()) add(GetCurrentDebtOfUser());
  }

  Future<void> closeDebt(String cerditCard) async {
    Completer<void> completer = Completer<void>();

    if (state is DebtState || state is LoadingDebtState) {
      add(
        CloseDebtEvent(completer: completer, creditCardId: cerditCard),
      );
    }
    if (state is NoDebtState) {
      completer.complete();
    }
    return completer.future;
  }
}

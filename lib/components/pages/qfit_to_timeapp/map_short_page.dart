import 'dart:async';

import 'package:flutter/material.dart';
import 'package:li/components/molecules/flow_offset/flow_offsets.dart';
import 'package:li/utils/determinePosition.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class MapShortPage extends StatefulWidget {
  const MapShortPage({Key? key}) : super(key: key);

  @override
  _MapShortPageState createState() => _MapShortPageState();
}

class _MapShortPageState extends State<MapShortPage>
    with AutomaticKeepAliveClientMixin {
  YandexMapController? yandexMapController;
  final StreamController<Null> streamController = StreamController<Null>();
  Point? initPosition;

  @override
  bool get wantKeepAlive => true;

  @override
  void dispose() {
    streamController.close();
    super.dispose();
  }

  void cameraTracking(
    CameraPosition cameraPosition,
    bool finished,
  ) async {
    streamController.add(null);
  }

  void _initS() async {
    final _initPosition = await determinePosition();
    await yandexMapController!.moveCamera(
      CameraUpdate.newCameraPosition(CameraPosition(
        target: Point(
          latitude: _initPosition.latitude,
          longitude: _initPosition.longitude,
        ),
        zoom: 12.2,
      )),
      animation: MapAnimation(
        duration: 1.1,
      ),
    );
    // yandexMapController!.updateMapObjects([
    //   Placemark(
    //     mapId: MapObjectId('my_location'),
    //     point: Point(
    //       latitude: _initPosition.latitude,
    //       longitude: _initPosition.longitude,
    //     ),
    //     style: PlacemarkStyle(
    //       icon: PlacemarkIcon.fromIconName(iconName: 'assets/location.png'),
    //     ),
    //   )
    // ]);
    setState(() {
      initPosition = Point(
        latitude: _initPosition.latitude,
        longitude: _initPosition.longitude,
      );
    });
  }

  @override
  void initState() {
    super.initState();
  }

  void onMapCreated(YandexMapController yandexMapController) async {
    setState(() {
      this.yandexMapController = yandexMapController;
    });
    yandexMapController.moveCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: Point(latitude: 43.2255328, longitude: 76.9029686),
          zoom: 12,
        ),
      ),
      animation: MapAnimation(
        duration: 0,
      ),
    );

    _initS();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Stack(
      children: [
        YandexMap(
          onMapCreated: onMapCreated,
          tiltGesturesEnabled: false,
          onCameraPositionChanged: (cameraPosition, reason, finished) =>
              cameraTracking(cameraPosition, finished),
        ),
        FlowOffsets(
          yandexMapController: yandexMapController,
          streamController: streamController,
          myLocation: initPosition,
        ),
      ],
    );
  }
}

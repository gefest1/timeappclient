import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/text_styles.dart';
import 'package:time_app_components/utils/testStyles.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

import 'drawing_page.dart';

bool areaValue = false;

class CustomMapPainter extends StatefulWidget {
  const CustomMapPainter({Key? key}) : super(key: key);

  @override
  _CustomMapPainterState createState() => _CustomMapPainterState();
}

const point = Point(latitude: 43.238949, longitude: 76.889709);

class _CustomMapPainterState extends State<CustomMapPainter>
    with TickerProviderStateMixin {
  // YandexMapController? _yandexMapController;
  late final TabController tabController =
      TabController(vsync: this, length: 2);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
      child: Center(
        child: Stack(
          children: [
            YandexMap(
              tiltGesturesEnabled: false,
              zoomGesturesEnabled: false,
              rotateGesturesEnabled: false,
              onMapCreated: (YandexMapController yandexMapController) {
                // _yandexMapController = yandexMapController;
              },
              onMapTap: (_) {},
            ),
            Positioned(
              bottom: 160.0,
              right: 10.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            areaValue = !areaValue;
                          });
                        },
                        child: SvgPicture.asset('assets/svg/draw_map_area.svg'),
                      )
                    ],
                  ),
                ],
              ),
            ),
            if (areaValue) DrawingPage(),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Material(
                color: Colors.white,
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(16),
                ),
                child: Padding(
                  padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).padding.bottom + 20,
                    top: 20,
                    left: 20,
                    right: 20,
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: SizedBox(
                          height: 50,
                          child: DecoratedBox(
                            decoration: BoxDecoration(
                              color: Color(0xffF8F8F8),
                              borderRadius: BorderRadius.circular(6),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: TabBar(
                                controller: tabController,
                                labelColor: ColorData.clientsButtonColorPressed,
                                labelStyle: const TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                ),
                                unselectedLabelColor: Colors.black,
                                unselectedLabelStyle: const TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400,
                                ),
                                indicator: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(6),
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black.withOpacity(0.1),
                                      spreadRadius: 0,
                                      blurRadius: 5,
                                      offset: const Offset(1, 1),
                                    ),
                                  ],
                                ),
                                tabs: [
                                  Text(
                                    'Списком',
                                  ),
                                  Text(
                                    'На карте',
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 10),
                      GestureDetector(
                        onTap: () {
                          // log('message');
                        },
                        behavior: HitTestBehavior.opaque,
                        child: IgnorePointer(
                          child: SizedBox(
                            height: 50,
                            child: Center(
                              child: Text(
                                'Фильтры',
                                style: TextStyle(
                                  fontSize: P3TextStyle.fontSize,
                                  fontWeight: P3TextStyle.fontWeight,
                                  height: P3TextStyle.height,
                                  color: ColorData.clientsButtonColorPressed,
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    ));
  }
}

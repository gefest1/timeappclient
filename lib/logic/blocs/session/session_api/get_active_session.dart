import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/logic/blocs/session/models/global_session_model.dart';

mixin GetActiveSession {
  static Future<List<GlobalSessionModel>> getActiveSession(
      {required GraphQLClient client}) async {
    final queryResult = await client.query(
      QueryOptions(
        document: gql('''
          {
            getActiveSessions{
              _id
              type
              clientId
              isActive
              sessionId
            }
          }'''),
      ),
    );
    if (queryResult.hasException) throw queryResult.exception!;

    return (queryResult.data!['getActiveSessions'] as List<dynamic>)
        .map((_v) => GlobalSessionModel.fromMap(_v))
        .toList();
  }
}

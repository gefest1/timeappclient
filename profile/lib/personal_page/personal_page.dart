import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_exif_rotation/flutter_exif_rotation.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:li/components/atoms/error_overlay.dart';
import 'package:li/const.dart';
import 'package:li/utils/restart_widget.dart';
import 'package:profile/bloc/profile_bloc.dart';
import 'package:profile/icon/trash_bin.dart';
import 'package:profile/personal_page/action_sheet.dart';
import 'package:profile/profile_deleteAccount_dialog.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/components/molecules/CustomTextField.dart';

import 'package:li/data/models/client_model.dart';
import 'package:li/data/models/Sex.dart';
import 'package:li/main.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/text_styles.dart';
import 'package:time_app_components/utils/testStyles.dart';

import 'custom_text_tab_copy.dart';

class PersonalPage extends StatefulWidget {
  final Client client;
  const PersonalPage({required this.client, Key? key}) : super(key: key);

  @override
  _PersonalPageState createState() => _PersonalPageState();
}

class _PersonalPageState extends State<PersonalPage> {
  XFile? _image;
  final StreamController<bool> _streamChanged = StreamController();
  late TextEditingController controllerName =
      TextEditingController(text: widget.client.fullName)
        ..addListener(_latestChanges);
  late final ValueNotifier<int> sexGender = ValueNotifier<int>(
    (widget.client.sex?.isMale() ?? true) ? 0 : 1,
  )..addListener(_latestChanges);

  File? image;
  late int index;

  @override
  void dispose() {
    controllerName.dispose();
    _streamChanged.close();

    super.dispose();
  }

  void _latestChanges() {
    bool changed = (controllerName.text != widget.client.fullName ||
        sexGender.value != ((widget.client.sex?.isMale() ?? true) ? 0 : 1) ||
        image?.path != _image?.path);
    _streamChanged.add(changed);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(
        leading: InkWell(
          onTap: () => Navigator.pop(context),
          borderRadius: const BorderRadius.all(
            Radius.circular(40),
          ),
          child: const Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
        ),
        title: const Text(
          'Личная информация',
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500,
            color: Color(0xff424242),
          ),
        ),
      ),
      bottomNavigationBar: StreamBuilder<bool>(
          initialData: false,
          stream: _streamChanged.stream,
          builder: (ctx, _val) {
            if (!(_val.data ?? false)) return SizedBox();
            return Container(
              height: 121,
              padding: const EdgeInsets.only(bottom: 40, top: 20),
              decoration: const BoxDecoration(
                boxShadow: bigBlur,
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15),
                  topRight: Radius.circular(15),
                ),
              ),
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: SizedBox(
                    height: 64,
                    width: double.infinity,
                    child: ElevatedButton(
                      onPressed: () async {
                        log(controllerName.text);
                        controllerName.text.isNotEmpty
                            ? {
                                BlocProvider.of<ProfileBloc>(context).add(
                                  EditClient(
                                      image,
                                      controllerName.text,
                                      sexGender.value == 0
                                          ? SexEnum.Male
                                          : SexEnum.Female),
                                ),
                                Navigator.of(context, rootNavigator: true).pop()
                              }
                            : navigatorKey.currentState?.overlay?.insert(
                                ErrorOverlayLoader(
                                  text:
                                      'Пожалуйста, укажите свое имя. Мы не можем сохранить профиль без имени',
                                ),
                              );
                        controllerName.text;
                      },
                      child: const Text(
                        'Сохранить изменения',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                          ColorData.clientsButtonColorDefault,
                        ),
                        shape: MaterialStateProperty.all(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(16),
                          ),
                        ),
                        elevation: MaterialStateProperty.all(0.0),
                      ),
                    ),
                  ),
                ),
              ),
            );
          }),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        children: [
          const SizedBox(height: 20),
          Column(
            children: [
              SizedBox(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(60.0),
                  child: _image != null
                      ? Image.file(
                          File(_image!.path),
                          fit: BoxFit.cover,
                          height: double.infinity,
                          width: double.infinity,
                        )
                      : widget.client.photoURL?.xl == null
                          ? Container(
                              decoration: BoxDecoration(
                                color: const Color(0xffFF8282),
                                borderRadius: BorderRadius.circular(60),
                              ),
                              child: Center(
                                child: Text(
                                  widget.client.fullName!
                                      .toUpperCase()
                                      .substring(0, 1),
                                  style: const TextStyle(
                                    fontSize: 56,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                            )
                          : Image.network(widget.client.photoURL!.xl!),
                ),
                width: 120,
                height: 120,
              ),
              InkWell(
                onTap: () async {
                  final pickedImage =
                      await ActionSheet.showActionSheet(context);
                  if (pickedImage == null) return;
                  setState(() {
                    _image = pickedImage;
                  });
                  _latestChanges();
                  image = await FlutterExifRotation.rotateImage(
                      path: pickedImage.path);

                  // BlocProvider.of<ProfileBloc>(context).add(
                  //   EditClient(image),
                  // );
                },
                child: Container(
                  margin: const EdgeInsets.all(10),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100.0),
                    border: Border.all(
                      color: ColorData.clientsButtonColorPressed,
                      width: 1,
                    ),
                  ),
                  child: Text(
                    (_image == null && widget.client.photoURL?.xl == null)
                        ? 'Добавить фотографию'
                        : 'Изменить фотографию',
                    style: const TextStyle(
                        color: ColorData.clientsButtonColorPressed),
                  ),
                ),
              )
            ],
          ),
          const SizedBox(height: 40),
          CustomTextField(
            label: 'Имя',
            enabledDecoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(16)),
              color: Color(0xffF9FFFA),
              border: Border.symmetric(
                horizontal: BorderSide(
                  width: 0,
                  color: Color(0xff00AB28),
                ),
                vertical: BorderSide(
                  width: 0,
                  color: Color(0xff00AB28),
                ),
              ),
            ),
            controller: controllerName,
          ),
          (widget.client.city != null)
              ? Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Город',
                            style: TextStyle(
                              fontSize: 14,
                              color: ColorData.allMainActivegray,
                              height: 17 / 14,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          const SizedBox(height: 2),
                          Text(
                            (widget.client.city.toString() == 'Almaty')
                                ? 'Алматы'
                                : 'Нур-Султан',
                            style: const TextStyle(
                              fontSize: P2TextStyle.fontSize,
                              fontWeight: P2TextStyle.fontWeight,
                              height: P2TextStyle.height,
                              color: ColorData.allMainBlack,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              : const SizedBox(height: 0),
          const SizedBox(height: 20),
          Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Номер телефона',
                      style: TextStyle(
                        fontSize: 14,
                        color: ColorData.allMainActivegray,
                        height: 17 / 14,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    const SizedBox(height: 2),
                    Text(
                      '+${widget.client.phoneNumber}',
                      style: const TextStyle(
                        fontSize: P2TextStyle.fontSize,
                        fontWeight: P2TextStyle.fontWeight,
                        height: P2TextStyle.height,
                        color: ColorData.allMainBlack,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 20),
          const Divider(
            height: 0,
            thickness: 0.5,
            color: ColorData.allMainGray,
          ),
          SizedBox(
            height: widget.client.dateOfBirth == null ? 0 : 20,
          ),
          widget.client.dateOfBirth == null
              ? const SizedBox(
                  height: 0,
                )
              : CustomTextField(
                  label: "Дата рождения",
                  controller: TextEditingController(
                      text: '${widget.client.dateOfBirth}'),
                ),
          const SizedBox(height: 20),
          Center(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  'Рейтинг ',
                  style: TextStyle(
                    fontSize: P4TextStyle.fontSize,
                    fontWeight: P4TextStyle.fontWeight,
                    color: ColorData.allMainGray,
                  ),
                ),
                RepaintBoundary(
                  child:
                      SvgPicture.asset('assets/svg/icon/FilledStarSmall.svg'),
                ),
                const Text(
                  "5.0",
                  style: TextStyle(
                    fontSize: P4TextStyle.fontSize,
                    fontWeight: P4TextStyle.fontWeight,
                    color: ColorData.allMainGray,
                  ),
                )
              ],
            ),
          ),
          const SizedBox(height: 40),
          Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: SizedBox(
                height: 64,
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: () async {
                    showDialog(
                      context: context,
                      barrierDismissible: true,
                      useSafeArea: true,
                      builder: (BuildContext context) {
                        return const DeleteAccountAlertDialog();
                      },
                    );
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CustomPaint(
                        size: const Size(20, 20),
                        painter: DeleteTrash(),
                      ),
                      const SizedBox(width: 10),
                      const Text(
                        'Удалить профиль',
                        style: TextStyle(
                            color: ColorData.allButtonsError,
                            fontSize: P3TextStyle.fontSize,
                            fontWeight: P3TextStyle.fontWeight,
                            height: P3TextStyle.height),
                      ),
                    ],
                  ),
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      const Color(0xffF8F8F8),
                    ),
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16),
                      ),
                    ),
                    elevation: MaterialStateProperty.all(0.0),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

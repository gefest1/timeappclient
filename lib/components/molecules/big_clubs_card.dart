// import 'package:auto_size_text/auto_size_text.dart';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:li/components/atoms/club_load_location.dart';
import 'package:li/const.dart';
import 'package:li/data/models/photo_url.dart';
import 'package:li/utils/auto_size_group_text.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class BigClubsCard extends StatefulWidget {
  final String? title;
  final String? address;
  final num? price;
  final String? desc;
  final String? rating;
  final String? ratingCount;
  final DateTime? startTime;
  final DateTime? endTime;
  final List<PhotoUrl>? galleryURLs;
  final String? locationLabel;
  final String? iconPath;

  final VoidCallback? onTap;
  final VoidCallback? onInstagramTap;
  final VoidCallback? onPhoneTap;

  const BigClubsCard({
    this.onTap,
    this.endTime,
    this.startTime,
    this.title,
    this.address,
    this.price,
    this.desc,
    this.rating,
    this.ratingCount,
    this.galleryURLs,
    this.onInstagramTap,
    this.onPhoneTap,
    this.locationLabel,
    this.iconPath,
    Key? key,
  }) : super(key: key);

  @override
  _BigClubsCardState createState() => _BigClubsCardState();
}

class _BigClubsCardState extends State<BigClubsCard> {
  @override
  Widget build(BuildContext context) {
    final galleryURLs = widget.galleryURLs;
    return GestureDetector(
      onTap: widget.onTap,
      behavior: HitTestBehavior.opaque,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Colors.white,
          boxShadow: bigBlur,
        ),
        child: Padding(
          padding: EdgeInsets.all(15),
          child: Column(
            children: [
              if (galleryURLs != null &&
                  galleryURLs.isNotEmpty &&
                  galleryURLs.length != 1)
                SizedBox(
                  height: 127,
                  child: ListView.separated(
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      if (galleryURLs[index].m == null) SizedBox();
                      return Container(
                        height: 127,
                        width: 154,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                          color: Colors.grey,
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(16),
                          child: Image.network(
                            galleryURLs[index].m!,
                            fit: BoxFit.cover,
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (context, index) {
                      return const SizedBox(width: 6);
                    },
                    itemCount: galleryURLs.length,
                  ),
                ),
              if (galleryURLs != null &&
                  galleryURLs.isNotEmpty &&
                  galleryURLs.length == 1)
                Container(
                  height: 127,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Colors.grey,
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(16),
                    child: Image.network(
                      galleryURLs[0].xl!,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              const SizedBox(height: 10),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (widget.title != null)
                          Text(
                            widget.title!,
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                              color: ColorData.allMainBlack,
                            ),
                          ),
                        SizedBox(height: 5),
                        if (widget.address != null)
                          Text(
                            widget.address!,
                            style: TextStyle(
                              fontSize: 14,
                              color: ColorData.allMainBlack,
                            ),
                          ),
                        const SizedBox(height: 2),
                        if (widget.desc != null)
                          Text(
                            widget.desc!,
                            style: TextStyle(
                              fontSize: 14,
                              color: ColorData.allMainActivegray,
                            ),
                          ),
                      ],
                    ),
                  ),
                  const SizedBox(width: 12),
                  (widget.iconPath == null)
                      ? SizedBox()
                      : RepaintBoundary(
                          child: SvgPicture.network(
                            widget.iconPath.toString(),
                            width: 40,
                            height: 40,
                          ),
                        ),
                ],
              ),
              const SizedBox(height: 10),
              if (widget.startTime != null && widget.endTime != null)
                Container(
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    color: ColorData.allMainLightgray,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Row(
                      children: [
                        Expanded(
                          child: Row(
                            children: [
                              const SizedBox(width: 5),
                              Flexible(
                                child: Text(
                                  'Сегодня ' +
                                      DateFormat('Hm')
                                          .format(widget.startTime!) +
                                      ' - ' +
                                      DateFormat('Hm').format(widget.endTime!),
                                  style: TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                              const SizedBox(width: 4),
                              CircleAvatar(
                                backgroundColor: Color(0xFFBEFFC5),
                                maxRadius: 7,
                                child: CircleAvatar(
                                  backgroundColor:
                                      ColorData.clientsStrokeSucces,
                                  maxRadius: 3.18,
                                ),
                              )
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            const SizedBox(width: 4),
                            if (widget.onInstagramTap != null)
                              GestureDetector(
                                onTap: widget.onInstagramTap,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(6),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Center(
                                      child: RepaintBoundary(
                                        child: SvgPicture.asset(
                                            'assets/svg/icon/worldNet.svg'),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            const SizedBox(width: 6),
                            if (widget.onPhoneTap != null)
                              GestureDetector(
                                behavior: HitTestBehavior.opaque,
                                onTap: widget.onPhoneTap,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(6),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: Center(
                                      child: RepaintBoundary(
                                        child: SvgPicture.asset(
                                          'assets/svg/icon/greenCall.svg',
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              const SizedBox(height: 10),
              Center(
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        height: 30,
                        decoration: BoxDecoration(
                          color: ColorData.allMainBlack,
                          borderRadius: BorderRadius.circular(6),
                        ),
                        padding: EdgeInsets.symmetric(
                          vertical: 6,
                          horizontal: 10,
                        ),
                        alignment: Alignment.center,
                        child: FittedBox(
                          child: AutoSizeText.rich(
                            TextSpan(
                              children: [
                                TextSpan(
                                  text: widget.price.toString(),
                                  style: TextStyle(
                                    fontFamily: Platform.isIOS
                                        ? 'SfProDisplay'
                                        : 'Roboto',
                                    fontSize: 20,
                                  ),
                                ),
                                TextSpan(
                                  text: ' ₸',
                                ),
                                TextSpan(
                                  text: '/мин',
                                  style: TextStyle(
                                    fontFamily: Platform.isIOS
                                        ? 'SfProDisplay'
                                        : 'Roboto',
                                  ),
                                ),
                              ],
                            ),

                            maxFontSize: P3TextStyle.fontSize,
                            minFontSize: 1,
                            style: TextStyle(
                              fontSize: P3TextStyle.fontSize,
                              fontWeight: P3TextStyle.fontWeight,
                              height: P3TextStyle.height,
                              color: Colors.white,
                            ),
                            // maxFontSize: P3TextStyle.fontSize,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(width: 10),
                    Expanded(
                      child: Container(
                        height: 30,
                        decoration: BoxDecoration(
                          color: Color(0xffF9F9F9),
                          borderRadius: BorderRadius.circular(6),
                        ),
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: widget.rating == null
                              ? []
                              : [
                                  RepaintBoundary(
                                    child: SvgPicture.asset(
                                      'assets/svg/icon/star.svg',
                                      color: Color(0xff00DF31),
                                      height: 16,
                                      width: 16,
                                    ),
                                  ),
                                  const SizedBox(width: 2),
                                  Flexible(
                                    child: Text(
                                      widget.rating!,
                                      style: TextStyle(
                                        color: const Color(0xff00DF31),
                                        fontSize: P3TextStyle.fontSize,
                                        fontWeight: P3TextStyle.fontWeight,
                                        height: P3TextStyle.height,
                                      ),
                                    ),
                                  ),
                                ],
                        ),
                      ),
                    ),
                    const SizedBox(width: 10),
                    Expanded(
                      child: Container(
                        height: 30,
                        decoration: BoxDecoration(
                          color: Color(0xffF9F9F9),
                          borderRadius: BorderRadius.circular(6),
                        ),
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: widget.ratingCount == null
                              ? []
                              : [
                                  RepaintBoundary(
                                    child: SvgPicture.asset(
                                      'assets/svg/icon/bullHorn.svg',
                                      color: ColorData.allMainBlack,
                                      height: 16,
                                      width: 16,
                                    ),
                                  ),
                                  const SizedBox(width: 2),
                                  Flexible(
                                    child: Text(
                                      widget.ratingCount ?? 0.toString(),
                                      style: TextStyle(
                                        color: ColorData.allMainBlack,
                                        fontSize: P3TextStyle.fontSize,
                                        fontWeight: P3TextStyle.fontWeight,
                                        height: P3TextStyle.height,
                                      ),
                                    ),
                                  )
                                ],
                        ),
                      ),
                    ),
                    const SizedBox(width: 10),
                    Expanded(
                      child: widget.locationLabel == null
                          ? const ClubLoadLocation()
                          : Container(
                              height: 30,
                              decoration: BoxDecoration(
                                color: Color(0xffF9F9F9),
                                borderRadius: BorderRadius.circular(6),
                              ),
                              alignment: Alignment.center,
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  RepaintBoundary(
                                    child: SvgPicture.asset(
                                      'assets/svg/icon/sendFilled.svg',
                                      color: ColorData.allMainBlack,
                                      height: 16,
                                      width: 16,
                                    ),
                                  ),
                                  const SizedBox(width: 2),
                                  Flexible(
                                    child: Text(
                                      widget.locationLabel!,
                                      style: TextStyle(
                                        color: ColorData.allMainBlack,
                                        fontSize: P3TextStyle.fontSize,
                                        fontWeight: P3TextStyle.fontWeight,
                                        height: P3TextStyle.height,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

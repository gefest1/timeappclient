class AllowedSessionTypes {
  final String _value;

  const AllowedSessionTypes._internal(this._value);

  String toString() => _value;
  String toMap() => _value;

  static const HOMEFOOD = const AllowedSessionTypes._internal('HOMEFOOD');
  static const HOUSES = const AllowedSessionTypes._internal('HOUSES');
  static const MINUTEPAY = const AllowedSessionTypes._internal('MINUTEPAY');
  static const SERVICES = const AllowedSessionTypes._internal('SERVICES');

  static AllowedSessionTypes? fromMap(String? value) {
    if ('HOMEFOOD' == value) return HOMEFOOD;
    if ('HOUSES' == value) return HOUSES;
    if ('MINUTEPAY' == value) return MINUTEPAY;
    if ('SERVICES' == value) return SERVICES;

    return null;
  }
}

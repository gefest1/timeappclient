part of '../session_minute_bloc.dart';

extension FeedbackReaction on SessionMinuteBloc {
  void leaveComment(
    LeaveCommentEvent event,
    Emitter<SessionState> emit,
  ) async {
    final _initBlock = !blockBackRoutes;
    if (_initBlock) routerDelegate.blockWidgets();

    try {
      final mutationResult = await graphqlMinuteProvider.client.mutate(
        MutationOptions(
          document: gql(''' 
          mutation(
            \$institutionId: String!,
            \$rating: Float!,
            \$text: String!)
            {
            leaveComment(
              institutionId:\$institutionId,
              rating:\$rating,
              text:\$text)
              {
            }
          }
          '''),
          variables: {
            'institutionId': event.institutionId,
            'rating': event.rating,
            'text': event.text,
          },
        ),
      );

      if (mutationResult.hasException) throw mutationResult.exception!;

      if (_initBlock) routerDelegate.unBlockWidgets();
      routerDelegate.popToFirst();

      event.completer.complete(LeaveCommentSuccess());
      return;
    } catch (e) {
      event.completer.completeError(e);
    }
    if (_initBlock) routerDelegate.unBlockWidgets();
  }
}

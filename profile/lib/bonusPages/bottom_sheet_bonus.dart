import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/components/molecules/sliver_footer.dart';
import 'package:li/const.dart';
import 'package:li/logic/blocs/get_debt/getdebt_bloc.dart';
import 'package:li/main.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/components/molecules/CustomTextField.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:time_app_components/utils/testStyles.dart';
import 'package:li/logic/blocs/bonus/bonus_bloc.dart';
import 'package:li/components/atoms/promo_code_overlay.dart';
import 'package:li/components/atoms/error_promo_code_overlay.dart';

class BonnusBottomSheet extends StatefulWidget {
  const BonnusBottomSheet({Key? key}) : super(key: key);

  @override
  State<BonnusBottomSheet> createState() => _BonnusBottomSheetState();
}

class _BonnusBottomSheetState extends State<BonnusBottomSheet> {
  final TextEditingController controller = TextEditingController();
  late final _media = MediaQuery.of(navigatorKey.currentContext!);

  @override
  Widget build(BuildContext context) {
    // final _media = MediaQuery.of(context);
    return GestureDetector(
      onTap: () {
        final _focus = FocusScope.of(context);
        if (_focus.hasFocus || _focus.hasPrimaryFocus) {
          _focus.unfocus();
        }
      },
      child: Container(
        height: (_media.size.height - _media.padding.top) * 0.98,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15.0),
            topRight: Radius.circular(15.0),
          ),
        ),
        child: ClipRRect(
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(15.0),
            topRight: Radius.circular(15.0),
          ),
          child: Scaffold(
            backgroundColor: ColorData.clientsButtonColorDefault,
            appBar: CustomAppBar(
              title: const Text('Промокоды и баллы'),
              backgroundColor: ColorData.clientsButtonColorDefault,
              actions: [
                IconButton(
                  onPressed: () => Navigator.pop(context),
                  icon: SvgPicture.asset('assets/svg/icon/close.svg'),
                ),
              ],
            ),
            body: Stack(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 30),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: BlocBuilder<GetdebtBloc, GetdebtState>(
                        builder: (context, state) {
                          final double bonus;
                          if (state is DebtState || state is NoDebtState) {
                            bonus = (state as dynamic).client.bonus ?? 0;
                          } else {
                            bonus = 0;
                          }
                          return Text(
                            'Ваши баллы: ' + bonus.toStringAsFixed(0),
                            style: const TextStyle(
                              fontSize: 38,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                            textAlign: TextAlign.left,
                          );
                        },
                      ),
                    ),
                    const SizedBox(height: 5),
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20.0),
                      child: Text(
                        '1 балл = 1 тенге. Получить их можно кэшбеком при использовании TimeApp и с помощью промокодов',
                        style: TextStyle(
                          fontSize: P2TextStyle.fontSize,
                          fontWeight: P2TextStyle.fontWeight,
                          height: P2TextStyle.height,
                          color: Colors.white,
                        ),
                      ),
                    )
                  ],
                ),
                Positioned(
                  left: 0,
                  right: 0,
                  bottom: 0,
                  child: Container(
                    decoration: const BoxDecoration(
                      boxShadow: bigBlur,
                      borderRadius:
                          BorderRadius.vertical(top: Radius.circular(15)),
                      color: Colors.white,
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          CustomTextField(
                            label: 'Промокод',
                            controller: controller,
                            onSubmitted: (s) {
                              controller.text =
                                  controller.text.replaceAll(" ", "");
                              setState(() {});
                            },
                          ),
                          const SizedBox(height: 10),
                          SizeTapAnimation(
                            active: controller.text.isNotEmpty,
                            child: GestureDetector(
                              onTap: () async {
                                try {
                                  if (controller.text.isEmpty) return;

                                  final _text =
                                      controller.text.replaceAll(" ", "");
                                  final Completer<num> completer =
                                      Completer<num>();
                                  final bonusBloc = BlocProvider.of<BonusBloc>(
                                    context,
                                    listen: false,
                                  );
                                  bonusBloc.add(
                                    EnterPromoCode(
                                      promoCode: _text,
                                      completer: completer,
                                    ),
                                  );
                                  final _doubleBonus = await completer.future;
                                  navigatorKey.currentState!.overlay!.insert(
                                    PromoCodeOverlayLoader(
                                      text:
                                          'Промокод $_text применен. Добавили баллов: ${_doubleBonus.toStringAsFixed(0)}!',
                                    ),
                                  );
                                  controller.clear();
                                } catch (e) {
                                  navigatorKey.currentState!.overlay!.insert(
                                    ErrorPromoCodeOverlayLoader(
                                      text:
                                          'Не можем найти такой промокод. Перепроверьте его, пожалуйста',
                                    ),
                                  );
                                }
                              },
                              child: Container(
                                height: 60,
                                decoration: BoxDecoration(
                                  color: controller.value.text.isEmpty
                                      ? ColorData.allInnerInactive
                                      : ColorData.clientsButtonColorDefault,
                                  borderRadius: BorderRadius.circular(16),
                                ),
                                width: double.infinity,
                                alignment: Alignment.center,
                                child: Text(
                                  'Применить',
                                  style: TextStyle(
                                    color: controller.text.isEmpty
                                        ? ColorData.allTextInactive
                                        : Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            // CustomScrollView(
            //   slivers: [
            //     SliverToBoxAdapter(
            //       child: Column(
            //         crossAxisAlignment: CrossAxisAlignment.start,
            //         children: [
            //           const SizedBox(height: 30),
            //           Padding(
            //             padding: const EdgeInsets.symmetric(horizontal: 20.0),
            //             child: BlocBuilder<GetdebtBloc, GetdebtState>(
            //               builder: (context, state) {
            //                 final double bonus;
            //                 if (state is DebtState || state is NoDebtState) {
            //                   bonus = (state as dynamic).client.bonus ?? 0;
            //                 } else {
            //                   bonus = 0;
            //                 }
            //                 return Text(
            //                   'Ваши баллы: ' + bonus.toStringAsFixed(0),
            //                   style: const TextStyle(
            //                     fontSize: 38,
            //                     fontWeight: FontWeight.bold,
            //                     color: Colors.white,
            //                   ),
            //                   textAlign: TextAlign.left,
            //                 );
            //               },
            //             ),
            //           ),
            //           const SizedBox(height: 5),
            //           const Padding(
            //             padding: EdgeInsets.symmetric(horizontal: 20.0),
            //             child: Text(
            //               '1 балл = 1 тенге. Получить их можно кэшбеком при использовании TimeApp и с помощью промокодов',
            //               style: TextStyle(
            //                 fontSize: P2TextStyle.fontSize,
            //                 fontWeight: P2TextStyle.fontWeight,
            //                 height: P2TextStyle.height,
            //                 color: Colors.white,
            //               ),
            //             ),
            //           )
            //         ],
            //       ),
            //     ),
            //     SliverFooter(
            //       child: Align(
            //         alignment: Alignment.bottomCenter,
            //         child: Padding(
            //           padding: EdgeInsets.only(
            //               bottom: MediaQuery.of(context).viewInsets.bottom),
            //           child: Container(
            //             decoration: BoxDecoration(
            //               boxShadow: bigBlur,
            //               borderRadius: BorderRadius.circular(15),
            //               color: Colors.white,
            //             ),
            //             child: Padding(
            //               padding: const EdgeInsets.all(20),
            //               child: Column(
            //                 mainAxisAlignment: MainAxisAlignment.end,
            //                 children: [
            //                   CustomTextField(
            //                     label: 'Промокод',
            //                     controller: controller,
            //                     onSubmitted: (s) {
            //                       final bonusBloc = BlocProvider.of<BonusBloc>(
            //                         context,
            //                         listen: false,
            //                       );
            //                       bonusBloc.add(EnterPromoCode(promoCode: s));
            //                     },
            //                   ),
            //                   const SizedBox(height: 10),
            //                   Center(
            //                     child: SizedBox(
            //                       height: 64,
            //                       width: double.infinity,
            //                       child: ElevatedButton(
            //                         onPressed: () {
            //                           if (controller.text.isEmpty) return;
            //                           final bonusBloc =
            //                               BlocProvider.of<BonusBloc>(
            //                             context,
            //                             listen: false,
            //                           );
            //                           bonusBloc.add(
            //                             EnterPromoCode(
            //                               promoCode: controller.text,
            //                             ),
            //                           );
            //                           Navigator.pop(context);
            //                         },
            //                         child: Text(
            //                           'Применить',
            //                           style: TextStyle(
            //                             color: controller.text.isEmpty
            //                                 ? ColorData.allTextInactive
            //                                 : Colors.white,
            //                             fontSize: 20,
            //                             fontWeight: FontWeight.w400,
            //                           ),
            //                         ),
            //                         style: ButtonStyle(
            //                           // TODO
            //                           backgroundColor:
            //                               MaterialStateProperty.all(
            //                             controller.value.text.isEmpty
            //                                 ? ColorData.allInnerInactive
            //                                 : ColorData
            //                                     .clientsButtonColorDefault,
            //                           ),
            //                           shape: MaterialStateProperty.all(
            //                             RoundedRectangleBorder(
            //                               borderRadius:
            //                                   BorderRadius.circular(16),
            //                             ),
            //                           ),
            //                           elevation: MaterialStateProperty.all(0.0),
            //                         ),
            //                       ),
            //                     ),
            //                   ),
            //                 ],
            //               ),
            //             ),
            //           ),
            //         ),
            //       ),
            //     ),
            //   ],
            // ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/utils/color.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../const.dart';

class PhoneContact extends StatelessWidget {
  final String label;
  const PhoneContact({required this.label, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: GestureDetector(
        onTap: () {
          launch("tel://$label");
        },
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(16)),
            boxShadow: bigBlur,
          ),
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              RepaintBoundary(
                child: SvgPicture.asset('assets/svg/icon/ic_phone.svg'),
              ),
              Text(
                'Телефон',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  color: ColorData.allMainBlack,
                ),
              ),
              const SizedBox(height: 2),
              Text(
                label,
                style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 16,
                  color: ColorData.allMainBlack,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

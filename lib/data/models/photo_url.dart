import 'dart:convert';

import 'package:equatable/equatable.dart';

class PhotoUrl extends Equatable {
  final String? m;
  final String? xl;
  final String? thumbnail;

  const PhotoUrl({
    this.m,
    this.xl,
    this.thumbnail,
  });

  PhotoUrl copyWith({
    String? m,
    String? xl,
    String? thumbnail,
  }) {
    return PhotoUrl(
      m: m ?? this.m,
      xl: xl ?? this.xl,
      thumbnail: thumbnail ?? this.thumbnail,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'M': m,
      'XL': xl,
      'thumbnail': thumbnail,
    };
  }

  static PhotoUrl? fromMap(Map<String, dynamic>? map) {
    if (map == null) return null;
    return PhotoUrl(
      m: map['M'],
      xl: map['XL'],
      thumbnail: map['thumbnail'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PhotoUrl.fromJson(String source) =>
      PhotoUrl.fromMap(json.decode(source))!;

  @override
  bool get stringify => true;

  @override
  List<Object?> get props => [m, xl, thumbnail];
}

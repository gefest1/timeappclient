import 'dart:async';
import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'package:li/components/pages/qfit_to_timeapp/active_session/active_minute_page.dart';
import 'package:li/data/models/session_minute.dart';
import 'package:li/logic/blocs/qfit_bloc/session/session_api/session_api.dart';
import 'package:li/logic/blocs/session/GlobalSessionBloc.dart';
import 'package:li/logic/navigation.dart';
import 'package:li/main.dart';
import 'package:li/network/graphql_provider.dart';

part 'session_minute_event.dart';
part 'session_minute_state.dart';

part './session_reaction/finish_reaction.dart';
part './session_reaction/get_reaction.dart';
part './session_reaction/leave_comment.dart';
part './session_reaction/start_reaction.dart';

class SessionMinuteBloc extends Bloc<SessionEvent, SessionState> {
  final GraphqlMinuteProvider graphqlMinuteProvider;
  final GlobalSessionBloc globalSessionBloc;

  SessionMinuteBloc(
    this.graphqlMinuteProvider,
    this.globalSessionBloc,
  ) : super(SessionState()) {
    on<StartSessionEvent>(startSession);
    on<FinishSessionEvent>(finishSession);
    on<GetActiveSessionEvent>(getSessions);
    on<LeaveCommentEvent>(leaveComment);
  }
}

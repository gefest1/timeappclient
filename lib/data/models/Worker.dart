import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'institution_minute.dart';

class Worker extends Equatable {
  final String? id;
  final String? fullName;
  final InstitutionsMinute? institution;
  final String? login;
  final String? token;
  
  Worker({
    this.id,
    this.fullName,
    this.institution,
    this.login,
    this.token,
  });

  Worker copyWith({
    String? id,
    String? fullName,
    InstitutionsMinute? institution,
    String? login,
    String? token,
  }) {
    return Worker(
      id: id ?? this.id,
      fullName: fullName ?? this.fullName,
      institution: institution ?? this.institution,
      login: login ?? this.login,
      token: token ?? this.token,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'fullName': fullName,
      'institution': institution?.toMap(),
      'login': login,
      'token': token,
    };
  }

  factory Worker.fromMap(Map<String, dynamic> map) {
    return Worker(
      id: map['_id'] != null ? map['_id'] : null,
      fullName: map['fullName'] != null ? map['fullName'] : null,
      institution: map['institution'] != null ? InstitutionsMinute.fromMap(map['institution']) : null,
      login: map['login'] != null ? map['login'] : null,
      token: map['token'] != null ? map['token'] : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Worker.fromJson(String source) => Worker.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object?> get props {
    return [
      id,
      fullName,
      institution,
      login,
      token,
    ];
  }
}

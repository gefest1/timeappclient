part of 'getdebt_bloc.dart';

abstract class GetdebtState extends Equatable {
  const GetdebtState();

  @override
  List<Object?> get props => [];
}

class LoadingDebtState extends GetdebtState {
  const LoadingDebtState();

  @override
  List<Object?> get props => [];
}

class NoDebtState extends GetdebtState {
  final Client client;

  const NoDebtState(this.client);
  List<Object?> get props => [client.bonus, client.debt];
}

class DebtState extends GetdebtState {
  final Client client;

  const DebtState(this.client);

  List<Object?> get props => [client.bonus, client.debt];
}

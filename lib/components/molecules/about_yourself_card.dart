import 'package:flutter/material.dart';
import 'package:li/const.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class AboutYourSelf extends StatelessWidget {
  const AboutYourSelf({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 20,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: Colors.white,
        boxShadow: bigBlur,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'О себе',
                      style: TextStyle(
                        fontSize: 24,
                        color: ColorData.allMainBlack,
                        height: 29 / 24,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Text(
                      '26 лет, Мужчина В TimeApp с августа 2021г.',
                      style: TextStyle(
                        fontSize: P4TextStyle.fontSize,
                        color: ColorData.allMainBlack,
                        height: P4TextStyle.height,
                        fontWeight: P4TextStyle.fontWeight,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(width: 16),
              CircleAvatar(
                radius: 20,
                backgroundColor: Color(0xff4589FA),
              ),
            ],
          ),
          const SizedBox(height: 10),
          Text(
            'Делаю татуировки более 15 лет, начинал с рисования в художественной школе «Умай».  Сейчас забиваю более 100 довольных клиентов в течение месяца.',
            style: TextStyle(
              fontSize: P3TextStyle.fontSize,
              fontWeight: P3TextStyle.fontWeight,
              height: P3TextStyle.height,
              color: ColorData.allMainBlack,
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:li/components/atoms/shipping_type_card.dart';
import 'package:li/components/map/yandex_map.dart';
import 'package:li/components/pages/order_accept/pay_order_page.dart';
import 'package:li/main.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';

class ShippingPage extends StatefulWidget {
  final String institutionId;
  const ShippingPage({required this.institutionId, Key? key}) : super(key: key);

  @override
  _ShippingPageState createState() => _ShippingPageState();
}

class _ShippingPageState extends State<ShippingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
          color: Color(0xff14142B),
        ),
        title: Text(
          'Оформление заказа',
          style: TextStyle(
            color: ColorData.allMainBlack,
          ),
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.only(
          top: 24,
          bottom: 100,
          left: 20,
          right: 20,
        ),
        children: [
          ShowMapMarkers(),
          const SizedBox(
            height: 20,
          ),
          Text(
            'Как выхотите получить заказ?',
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.w600,
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 20,
          ),
          ShippingTypeCard(
            subTitle: 'будет у вас ~ через 15 минут',
            title: 'Доставкой Яндекса',
            svg: 'assets/svg/icon/motorcycle.svg',
            ontap: () {
              // routerDelegate.push(
              //   const WaitingOrderPage(),
              // );
              routerDelegate.push(PayOrderPage(
                institutionId: widget.institutionId,
              ));
            },
          ),
          const SizedBox(
            height: 10,
          ),
          ShippingTypeCard(
            ontap: () {},
            subTitle: 'будет готов ~ через 10 минут',
            title: 'Заберу навынос',
            svg: 'assets/svg/icon/FilledProfile.svg',
          ),
        ],
      ),
    );
  }
}

import 'package:flutter_svg/svg.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:time_app_components/utils/testStyles.dart';

class TellAboutYourSelf extends StatelessWidget {
  const TellAboutYourSelf({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _style = ButtonStyle(
      shape: MaterialStateProperty.all(RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(16)),
      )),
      minimumSize: MaterialStateProperty.all(Size(0, 112)),
      backgroundColor: MaterialStateProperty.all(Colors.white),
      elevation: MaterialStateProperty.all(0),
      overlayColor: MaterialStateProperty.all(Color(0x3d007aff)),
    );
    return Container(
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: ColorData.clientsButtonColorPressed,
        borderRadius: BorderRadius.circular(16),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Text(
            'Расскажите о себе',
            style: TextStyle(
              fontSize: P1TextStyle.fontSize,
              fontWeight: FontWeight.w700,
              height: P1TextStyle.height,
              color: Colors.white,
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 4),
          const Text(
            'Поделитесь профилем в своих социальных сетях, отправьте ссылку друзьям, и получайте больше заказов.',
            style: TextStyle(
              fontWeight: P2TextStyle.fontWeight,
              fontSize: P2TextStyle.fontSize,
              height: P2TextStyle.height,
              color: Colors.white,
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 30),
          Row(
            children: [
              Expanded(
                child: ElevatedButton(
                  style: _style,
                  onPressed: () {},
                  child: Column(
                    children: [
                      SvgPicture.asset('assets/svg/icon/copyShare.svg'),
                      const SizedBox(height: 5),
                      Text(
                        'Скопировать ссылку профиля',
                        style: TextStyle(
                          fontSize: P4TextStyle.fontSize,
                          fontWeight: P4TextStyle.fontWeight,
                          height: P4TextStyle.height,
                          color: ColorData.clientsButtonColorPressed,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: ElevatedButton(
                  style: _style,
                  onPressed: () {},
                  child: Column(
                    children: [
                      SvgPicture.asset('assets/svg/icon/export.svg'),
                      const SizedBox(height: 5),
                      Text(
                        'Поделиться профилем',
                        style: TextStyle(
                          fontSize: P4TextStyle.fontSize,
                          fontWeight: P4TextStyle.fontWeight,
                          height: P4TextStyle.height,
                          color: ColorData.clientsButtonColorPressed,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

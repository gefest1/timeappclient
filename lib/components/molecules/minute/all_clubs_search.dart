import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:li/components/molecules/big_club_wrapper.dart';
import 'package:li/data/models/allowed_institution_types.dart';
import 'package:li/data/models/institution_minute.dart';
import 'package:li/logic/providers/geo_institution/GeoInstitutionProvider.dart';
import 'package:li/logic/providers/get_club_data/GetClubData.dart';
import 'package:li/logic/providers/location/location_provider.dart';
import 'package:provider/provider.dart';

class AllClubsSearch extends StatefulWidget {
  final AllowedInstitutionTypes type;

  const AllClubsSearch({
    required this.type,
    Key? key,
  }) : super(key: key);

  @override
  _AllClubsSearchState createState() => _AllClubsSearchState();
}

class _AllClubsSearchState extends State<AllClubsSearch> {
  late final ClubDataProvider clubDataProvider = Provider.of<ClubDataProvider>(
    context,
    listen: false,
  );
  bool finish = false;
  late final ScrollController _scrollController = ScrollController()
    ..addListener(
      () async {
        if (finish || clubDataProvider.page[widget.type] == -1) return;
        try {
          if (_scrollController.offset >=
              (_scrollController.position.maxScrollExtent - 80)) {
            finish = true;
            try {
              setState(
                () {
                  futureList = clubDataProvider.getInstitution(widget.type);
                },
              );
              final _newInst = await futureList;
              _geoProvider.importInst(_newInst);
            } catch (e) {
              log('error');
            }
          }
        } catch (e) {}
        finish = false;
      },
    );
  late final GeoInstitutionProvider _geoProvider =
      Provider.of<GeoInstitutionProvider>(
    context,
    listen: false,
  );
  late Future<List<InstitutionsMinute>> futureList;
  final List<InstitutionsMinute> listInstitution = [];

  @override
  void initState() {
    final _location = Provider.of<LocationProvider>(context, listen: false);

    futureList = clubDataProvider.getInstitution(
      widget.type,
      latitude: _location.location?.latitude,
      longitude: _location.location?.longitude,
    )..then((value) {
        _geoProvider.importInst(value);
      });
    super.initState();
    final _locationProvider =
        Provider.of<LocationProvider>(context, listen: false);
    if (_locationProvider.location == null) {
      _locationProvider.onceGet();
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      controller: _scrollController,
      slivers: [
        SliverToBoxAdapter(child: const SizedBox(height: 20.5)),
        // SliverToBoxAdapter(
        //   child: Padding(
        //     padding: const EdgeInsets.symmetric(horizontal: 20.0),
        //     child: Row(
        //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //       children: [
        //         Text(
        //           'Найдено: ' + [1].length.toString(),
        //           style: TextStyle(
        //             fontSize: 16,
        //             color: ColorData.allMainActivegray,
        //             fontWeight: FontWeight.w500,
        //           ),
        //         ),
        //         Container(
        //           decoration: BoxDecoration(
        //             color: Colors.white,
        //             boxShadow: bigBlur,
        //             borderRadius: BorderRadius.circular(
        //               15,
        //             ),
        //           ),
        //           child: Padding(
        //             padding: const EdgeInsets.only(
        //                 left: 10, right: 20, top: 11, bottom: 11),
        //             child: Row(
        //               children: [
        //                 SvgPicture.asset(
        //                   'assets/svg/icon/arrowChevronDown.svg',
        //                 ),
        //                 Text('По расстоянию'),
        //               ],
        //             ),
        //           ),
        //         ),
        //       ],
        //     ),
        //   ),
        // ),
        FutureBuilder<List<InstitutionsMinute>>(
          future: futureList,
          builder: (_, snapshot) {
            final clubs =
                clubDataProvider.institutionsTypeMap[widget.type] ?? [];
            bool isLoading = snapshot.connectionState != ConnectionState.done;
            return SliverPadding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20),
              sliver: SliverList(
                delegate: SliverChildBuilderDelegate(
                  (context, _index) {
                    if (clubs.length * 2 == _index) {
                      return isLoading
                          ? Center(child: CircularProgressIndicator())
                          : SizedBox.shrink();
                    }
                    if (_index % 2 == 1) return const SizedBox(height: 20);
                    return BugClubCardWrapper(
                      club: clubs[_index ~/ 2],
                    );
                  },
                  childCount: clubs.length * 2 + 1,
                ),
              ),
            );
          },
        ),
        const SliverToBoxAdapter(
          child: SizedBox(height: 100),
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:li/logic/blocs/qfit_bloc/session/session_minute_bloc.dart';
import 'package:li/logic/blocs/session/GlobalSessionBloc.dart';
import 'package:li/main.dart';
import 'package:li/network/graphql_provider.dart';
import 'package:provider/provider.dart';

class SessionBlocProvider extends StatefulWidget {
  final Widget child;

  const SessionBlocProvider({
    required this.child,
    Key? key,
  }) : super(key: key);

  @override
  _SessionBlocProviderState createState() => _SessionBlocProviderState();
}

class _SessionBlocProviderState extends State<SessionBlocProvider> {
  late final graphqlMinuteProvider = Provider.of<GraphqlMinuteProvider>(
    context,
    listen: false,
  );
  late final graphqlProvider = Provider.of<GraphqlProvider>(
    context,
    listen: false,
  );
  late final GlobalSessionBloc globalSessionBloc;

  late final SessionMinuteBloc sessionMinuteBloc;

  @override
  void initState() {
    super.initState();
    globalSessionBloc = GlobalSessionBloc(
      graphqlProvider,
    );
    sessionMinuteBloc = SessionMinuteBloc(
      graphqlMinuteProvider,
      globalSessionBloc,
    );
    globalSessionBloc.sessionMinuteBloc = sessionMinuteBloc;
    if (isRegistered()) globalSessionBloc..add(GetSessionEvent());
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<GlobalSessionBloc>(
          create: (_) => globalSessionBloc,
        ),
        BlocProvider<SessionMinuteBloc>(
          create: (_) => sessionMinuteBloc,
        ),
      ],
      child: widget.child,
    );
  }
}

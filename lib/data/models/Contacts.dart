import 'dart:convert';

import 'package:equatable/equatable.dart';

class Contacts extends Equatable {
  final String? email;
  final String? instagram;
  final String? phoneNumber;
  final String? whatsApp;

  Contacts({
    this.email,
    this.instagram,
    this.phoneNumber,
    this.whatsApp,
  });

  Contacts copyWith({
    String? email,
    String? instagram,
    String? phoneNumber,
    String? whatsApp,
  }) {
    return Contacts(
      email: email ?? this.email,
      instagram: instagram ?? this.instagram,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      whatsApp: whatsApp ?? this.whatsApp,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'email': email,
      'instagram': instagram,
      'phoneNumber': phoneNumber,
      'whatsApp': whatsApp,
    };
  }

  static Contacts? fromMap(Map<String, dynamic>? map) {
    if (map == null) return null;
    return Contacts(
      email: map['email'] != null ? map['email'] : null,
      instagram: map['instagram'] != null ? map['instagram'] : null,
      phoneNumber: map['phoneNumber'] != null ? map['phoneNumber'] : null,
      whatsApp: map['whatsApp'] != null ? map['whatsApp'] : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Contacts.fromJson(String source) =>
      Contacts.fromMap(json.decode(source))!;

  @override
  bool get stringify => true;

  @override
  List<Object?> get props => [email, instagram, phoneNumber, whatsApp];
}

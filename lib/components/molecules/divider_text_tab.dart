import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class DividerTapTile extends StatelessWidget {
  final VoidCallback onTap;
  final String assetSvg;
  final String title;
  final String? subTitle;

  const DividerTapTile({
    required this.onTap,
    required this.assetSvg,
    required this.title,
    this.subTitle,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: this.onTap,
      behavior: HitTestBehavior.translucent,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Row(
          children: [
            Expanded(
              child: Row(
                children: [
                  RepaintBoundary(
                    child: SvgPicture.asset(
                      this.assetSvg,
                    ),
                  ),
                  const SizedBox(width: 10),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          title,
                          style: TextStyle(
                            fontSize: P3TextStyle.fontSize,
                            fontWeight: P3TextStyle.fontWeight,
                            height: P3TextStyle.height,
                            color: ColorData.allMainBlack,
                          ),
                        ),
                        if (this.subTitle != null)
                          Text(
                            subTitle!,
                            style: TextStyle(
                              fontSize: P4TextStyle.fontSize,
                              fontWeight: P4TextStyle.fontWeight,
                              height: P4TextStyle.height,
                              color: ColorData.allMainGray,
                            ),
                          ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            RepaintBoundary(
              child: SvgPicture.asset(
                'assets/svg/icon/arrowChevronForward.svg',
              ),
            ),
          ],
        ),
      ),
    );
  }
}

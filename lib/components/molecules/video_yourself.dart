import 'package:flutter/material.dart';
import 'package:time_app_components/utils/color.dart';

class VideoYourSelf extends StatelessWidget {
  const VideoYourSelf({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 20,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: Colors.white,
        boxShadow: const [
          BoxShadow(
            offset: Offset(0, 20),
            blurRadius: 40,
            color: Color(0x0d000000),
          ),
          BoxShadow(
            offset: Offset(0, -1),
            blurRadius: 40,
            color: Color(0x0d000000),
          ),
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Text(
                  'Видеоролик о вашей работе',
                  style: TextStyle(
                    fontSize: 24,
                    color: ColorData.allMainBlack,
                    height: 29 / 24,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              const SizedBox(width: 16),
              CircleAvatar(
                radius: 20,
                backgroundColor: Color(0xff4589FA),
              ),
            ],
          ),
          const SizedBox(height: 20),
          Container(
            height: 180,
            decoration: BoxDecoration(
              color: Colors.grey[400],
              borderRadius: BorderRadius.circular(16),
            ),
          ),
        ],
      ),
    );
  }
}

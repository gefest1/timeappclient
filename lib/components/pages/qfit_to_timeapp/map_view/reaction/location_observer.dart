import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

import 'package:li/logic/providers/location/location_provider.dart';

mixin LocationObserver<T extends StatefulWidget> on State<T> {
  YandexMapController? yandexMapController;
  Position? prevLocation;
  late LocationProvider locationProvider = Provider.of(context, listen: false);

  void _listenLocProvider() {
    if (locationProvider.location == null) return;
    final myLocation = locationProvider.location!;

    /// INITAL MOVE YADNEX MAP
    if (yandexMapController != null && prevLocation == null) {
      yandexMapController!.moveCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: Point(
              latitude: myLocation.latitude,
              longitude: myLocation.longitude,
            ),
            zoom: 12.2,
          ),
        ),
        animation: MapAnimation(
          duration: 1.1,
        ),
      );
    }

    prevLocation = myLocation;
  }

  void mapCreatedLoc() {
    _listenLocProvider();
  }

  void locationStart({void Function(Position)? callBack}) {
    // не обращай внимание
    locationProvider.activate(callBack: callBack);
    locationProvider.addListener(_listenLocProvider);
  }

  void locationEnd() {
    locationProvider.disactivate();
    locationProvider.removeListener(_listenLocProvider);
  }
}

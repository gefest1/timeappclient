part of 'bottom_nav_bar.dart';

final double circleSize = 5;
final sizeWidth = 48.0;
final sizeHeight = 48.0;

extension ElementGap on _BottomNavBarState {
  double get elementGap =>
      (mediaQuery.size.width - sizeWidth * widget.length) / (widget.length + 1);

  double get hhdwa => mediaQuery.size.width;
}

extension BottomNavupdate on _BottomNavBarState {
  _didChangeMetrics() {
    mediaQuery = MediaQueryData.fromWindow(WidgetsBinding.instance!.window);
    firsLeftPadding = (elementGap + (sizeWidth - circleSize) / 2);
    paddingSpread = (mediaQuery.size.width - 2 * elementGap - sizeWidth);
  }
}

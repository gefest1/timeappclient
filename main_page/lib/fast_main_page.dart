import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/svgs/bigSvg/fast_main.dart';
import 'package:main_page/component/atoms/qr_button.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/components/pages/qfit_to_timeapp/qfit_page.dart';
import 'package:li/const.dart';
import 'package:li/logic/blocs/qfit_bloc/session/session_minute_bloc.dart';
import 'package:li/main.dart';
import 'package:main_page/component/molecules/wrap_services.dart';
import 'package:profile/bloc/profile_bloc.dart';
import 'package:li/svgs/widgets/per_minute.dart';
import 'package:take_away/main.dart';

class LittleCardFast extends StatelessWidget {
  final double? height;
  final String? title;
  final String? label;
  final String? stringAsset;

  const LittleCardFast({
    this.height,
    this.label,
    this.title,
    this.stringAsset,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (stringAsset != null)
          Container(
            width: double.infinity,
            height: height,
            child: RepaintBoundary(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(15),
                child: SvgPicture.string(stringAsset!),
              ),
            ),
            decoration: BoxDecoration(
              color: ColorData.allMainLightgray,
              borderRadius: BorderRadius.circular(15),
            ),
          ),
        const SizedBox(height: 5),
        if (title != null)
          Text(
            title!,
            style: const TextStyle(
              fontSize: P4TextStyle.fontSize,
              fontWeight: P4TextStyle.fontWeight,
              height: P4TextStyle.height,
              color: Colors.black,
            ),
            textAlign: TextAlign.center,
          ),
        if (label != null)
          Text(
            label!,
            style: const TextStyle(
              fontSize: P4TextStyle.fontSize,
              fontWeight: P4TextStyle.fontWeight,
              height: P4TextStyle.height,
              color: ColorData.allMainActivegray,
            ),
          )
      ],
    );
  }
}

class FastMainPage extends StatelessWidget {
  const FastMainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      children: [
        SizedBox(height: MediaQuery.of(context).padding.top),
        const SizedBox(height: 20),
        Row(
          children: [
            Expanded(
              child: BlocBuilder<ProfileBloc, ProfileState>(
                builder: (context, state) {
                  return RichText(
                    text: TextSpan(
                      style: TextStyle(
                        fontSize: H2TextStyle.fontSize,
                        fontWeight: H2TextStyle.fontWeight,
                        height: H2TextStyle.height,
                        color: ColorData.allMainBlack,
                        fontFamily: Platform.isIOS ? 'SfProDisplay' : 'Roboto',
                      ),
                      children: [
                        const TextSpan(
                          text: 'Добрый день, ',
                        ),
                        if (state is ProfileInformation)
                          TextSpan(text: state.client.fullName),
                        const TextSpan(
                          text: '!',
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
            const RepaintBoundary(child: QrButton()),
          ],
        ),
        BlocBuilder<SessionMinuteBloc, SessionState>(
          builder: (ctx, state) {
            if (state is StartedSessionState) {
              if (state.sessions.isEmpty) return const SizedBox(height: 20);
              return Padding(
                padding: const EdgeInsets.only(top: 20, bottom: 20),
                child: ActiveServices(sessions: state.sessions),
              );
            } else {
              return const SizedBox(height: 20);
            }
          },
        ),
        RepaintBoundary(
          child: SizeTapAnimation(
            child: GestureDetector(
              onTap: () {
                routerDelegate.push(const QfitPage());
              },
              child: Container(
                decoration: BoxDecoration(
                  boxShadow: bigBlur,
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                ),
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Поминутная оплата',
                      style: TextStyle(
                        height: H3TextStyle.height,
                        fontSize: H3TextStyle.fontSize,
                        fontWeight: H3TextStyle.fontWeight,
                        color: ColorData.clientsButtonColorPressed,
                      ),
                    ),
                    const SizedBox(height: 2),
                    const Text(
                      'Спортивные залы, катки, развлечения, коворкинг',
                      style: TextStyle(
                        height: P3TextStyle.height,
                        fontSize: P3TextStyle.fontSize,
                        fontWeight: P3TextStyle.fontWeight,
                      ),
                    ),
                    const SizedBox(height: 15),
                    Container(
                      decoration: BoxDecoration(
                        color: ColorData.allMainLightgray,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      width: double.infinity,
                      // height: 148,
                      child: const PerMinueSvgWidget(),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        const SizedBox(height: 36),
        const Text(
          'Дальше — больше',
          style: TextStyle(
            fontSize: H2TextStyle.fontSize,
            fontWeight: H2TextStyle.fontWeight,
            height: H2TextStyle.height,
            color: ColorData.allMainBlack,
          ),
        ),
        const SizedBox(height: 15),
        RepaintBoundary(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    routerDelegate.push(const SearchPage());
                  },
                  child: const RepaintBoundary(
                    child: LittleCardFast(
                      title: 'Домашняя еда',
                      stringAsset: fast_main_food_svg,
                      height: 105,
                      label: 'Февраль 2022',
                    ),
                  ),
                ),
              ),
              const SizedBox(width: 10),
              const Expanded(
                child: RepaintBoundary(
                  child: LittleCardFast(
                    title: 'Услуги',
                    stringAsset: fast_main_service_svg,
                    height: 105,
                    label: 'Апрель 2022',
                  ),
                ),
              ),
              const SizedBox(width: 10),
              const Expanded(
                child: RepaintBoundary(
                  child: LittleCardFast(
                    title: 'Аренда недвижимости',
                    stringAsset: fast_main_stand_svg,
                    height: 105,
                    label: 'Июнь 2022',
                  ),
                ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 40),
      ],
    );
  }
}

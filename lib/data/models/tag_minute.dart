import 'dart:convert';

class TagMinute {
  final String? id;
  final String? iconSVGPath;
  final String? name;

  const TagMinute({
    this.id,
    this.iconSVGPath,
    this.name,
  });

  TagMinute copyWith({
    String? id,
    String? iconSVGPath,
    String? name,
  }) {
    return TagMinute(
      id: id ?? this.id,
      iconSVGPath: iconSVGPath ?? this.iconSVGPath,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'iconSVGPath': iconSVGPath,
      'name': name,
    };
  }

  factory TagMinute.fromMap(Map<String, dynamic> map) {
    return TagMinute(
      id: map['_id'],
      iconSVGPath: map['iconSVGPath'],
      name: map['name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TagMinute.fromJson(String source) =>
      TagMinute.fromMap(json.decode(source));

  @override
  String toString() =>
      'TagMinute(id: $id, iconSVGPath: $iconSVGPath, name: $name)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is TagMinute &&
        other.id == id &&
        other.iconSVGPath == iconSVGPath &&
        other.name == name;
  }

  @override
  int get hashCode => id.hashCode ^ iconSVGPath.hashCode ^ name.hashCode;
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/molecules/extended_l_card.dart';
import 'package:li/data/models/service_model.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/utils/testStyles.dart';

class ExtendedFeaturesPage extends StatefulWidget {
  final List<ServiceMinute> listServices;
  final ValueNotifier<ServiceMinute?> serviceMinute;
  const ExtendedFeaturesPage({
    required this.listServices,
    required this.serviceMinute,
    Key? key,
  }) : super(key: key);

  @override
  State<ExtendedFeaturesPage> createState() => _ExtendedFeaturesPageState();
}

class _ExtendedFeaturesPageState extends State<ExtendedFeaturesPage> {
  final bool ischoosen = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: Text('Услуги',
            style: TextStyle(
              color: ColorData.allMainBlack,
            )),
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: SvgPicture.asset('assets/svg/icon/backArrow.svg'),
        ),
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.only(
                left: 20,
                right: 20,
                top: 40,
              ),
              child: Text(
                'Выберите услугу',
                style: TextStyle(
                  color: ColorData.allMainBlack,
                  fontSize: H2TextStyle.fontSize,
                  fontWeight: H2TextStyle.fontWeight,
                  height: H2TextStyle.height,
                ),
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: SizedBox(height: 15),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate((context, index) {
              final currentService = widget.listServices[index];
              return Padding(
                padding: const EdgeInsets.only(bottom: 20, left: 20, right: 20),
                child: ExtendedLittleServiceCard(
                  title: currentService.name,
                  label: currentService.description,
                  subtitle: currentService.price.toString(),
                  onTap: () {
                    widget.serviceMinute.value = currentService;
                    setState(() {});
                  },
                  choosen: widget.serviceMinute.value?.id == currentService.id,
                ),
              );
            }, childCount: widget.listServices.length),
          ),
        ],
      ),
    );
  }
}

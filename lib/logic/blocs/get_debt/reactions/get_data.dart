part of '../getdebt_bloc.dart';

extension GetData on GetdebtBloc {
  void getClient(
    GetCurrentDebtOfUser event,
    Emitter<GetdebtState> emit,
  ) async {
    try {
      final _mutationResult = await graphqlProvider.client.query(
        QueryOptions(
          document: gql('''
              query{
                getClient{
                  debt
                  email,
                  creditCards{
                    cardHolderName,
                    cardType,
                    last4Digits,
                  }
                  _id,
                  dateOfBirth,
                  fullName,
                  bonus,
                  phoneNumber,
                  photoURL{
                    M,
                    XL,
                    thumbnail
                  }
                  sex
                }
              }'''),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );
      if (_mutationResult.hasException) throw _mutationResult.exception!;

      final Client _data = Client.fromMap(_mutationResult.data!['getClient']);

      if (_data.debt == null || _data.debt == 0) {
        emit(NoDebtState(_data));
      } else {
        emit(DebtState(_data));
      }
    } catch (e) {
      add(GetCurrentDebtOfUser());
      // on<GetCurrentDebtOfUser>(getClient);
    }
  }
}

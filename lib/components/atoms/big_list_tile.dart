import 'package:flutter_svg/svg.dart';
import 'package:li/const.dart';
import 'package:flutter/material.dart';

class BigListTile extends StatelessWidget {
  final VoidCallback onTap;
  final Widget leading;
  final Widget title;
  final Widget? subTitle;
  final double? space;

  const BigListTile({
    required this.onTap,
    required this.leading,
    required this.title,
    this.subTitle,
    this.space,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          ...bigBlur,
        ],
        borderRadius: BorderRadius.circular(16),
      ),
      child: SizedBox(
        height: 80,
        width: double.infinity,
        child: Material(
          borderRadius: BorderRadius.circular(16),
          color: Colors.white,
          child: InkWell(
            borderRadius: BorderRadius.circular(16),
            onTap: this.onTap,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      leading,
                      if (this.space != null)
                        SizedBox(
                          width: this.space,
                        ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          title,
                          if (subTitle != null) subTitle!,
                        ],
                      ),
                    ],
                  ),
                  RepaintBoundary(
                    child: SvgPicture.asset(
                        'assets/svg/icon/arrowChevronForward.svg'),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

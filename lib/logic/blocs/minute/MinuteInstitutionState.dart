import 'package:equatable/equatable.dart';
import 'package:li/data/models/institution_minute.dart';
import 'package:li/data/models/service_model.dart';

class MinuteInstitutionState extends Equatable {
  const MinuteInstitutionState();

  @override
  List<Object?> get props => [];
}

class LoadingMinuteInstitutionState extends MinuteInstitutionState {
  const LoadingMinuteInstitutionState();

  @override
  List<Object?> get props => [];
}

class SuccessMinuteInstitutionState extends MinuteInstitutionState {
  final List<InstitutionsMinute> institutionsMinute;

  const SuccessMinuteInstitutionState(this.institutionsMinute);

  @override
  List<Object?> get props => [this.institutionsMinute];
}

class FetchedService extends MinuteInstitutionState {
  final List<ServiceMinute> data;

  const FetchedService(this.data);

  @override
  List<Object?> get props => [this.data];
}

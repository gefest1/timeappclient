import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ChooseLocationPage extends StatefulWidget {
  const ChooseLocationPage({Key? key}) : super(key: key);

  @override
  _ChooseLocationPageState createState() => _ChooseLocationPageState();
}

class _ChooseLocationPageState extends State<ChooseLocationPage> {
  @override
  Widget build(BuildContext context) {
    final _media = MediaQuery.of(context);

    return Stack(
      children: [
        Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(
              flex: 1,
              child: SizedBox(),
            ),
            Expanded(
              flex: 12,
              child: GestureDetector(
                behavior: HitTestBehavior.translucent,
                onTap: () {},
                child: Material(
                  color: Colors.white,
                  borderRadius: const BorderRadius.vertical(
                    top: Radius.circular(16),
                  ),
                  child: SizedBox(
                    width: double.infinity,
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                            left: 20,
                            top: 20,
                            bottom: 20,
                            right: 12,
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                child: SizedBox(
                                  height: 40,
                                  child: TextField(
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                    ),
                                    decoration: InputDecoration(
                                      prefixIconConstraints:
                                          const BoxConstraints(
                                        maxWidth: 40,
                                        minWidth: 40,
                                        maxHeight: 20,
                                        minHeight: 20,
                                      ),
                                      prefixIcon: Padding(
                                        padding: const EdgeInsets.only(
                                            left: 15, right: 5),
                                        child: SvgPicture.asset(
                                          'assets/svg/icon/search.svg',
                                          height: 20,
                                          width: 20,
                                        ),
                                      ),
                                      fillColor: Color(0xffFAFAFA),
                                      filled: true,
                                      contentPadding:
                                          EdgeInsets.symmetric(vertical: 10),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(16),
                                        borderSide: BorderSide.none,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              IconButton(
                                padding: EdgeInsets.zero,
                                iconSize: 24,
                                onPressed: () {},
                                icon: Icon(
                                  Icons.close,
                                ),
                              )
                            ],
                          ),
                        ),
                        Expanded(
                          child: ListView.separated(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            itemBuilder: (_, idnex) => Container(
                              padding: EdgeInsets.symmetric(vertical: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'data',
                                    style: TextStyle(
                                      color: Color(0xff434343),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                  const SizedBox(height: 2),
                                  Text(
                                    'data',
                                    style: TextStyle(
                                      color: Color(0xff979797),
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            separatorBuilder: (_, incex) => Divider(
                              color: Color(0xffB7B7B7),
                              height: 1,
                            ),
                            itemCount: 40,
                            physics: ClampingScrollPhysics(),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
        Positioned(
          bottom: _media.viewInsets.bottom,
          left: 0,
          right: 0,
          child: DecoratedBox(
            decoration: BoxDecoration(
              boxShadow: [
                const BoxShadow(
                  offset: Offset(0, -20),
                  blurRadius: 40,
                  color: Color(0x14000000),
                )
              ],
              color: Colors.white,
            ),
            child: Padding(
              padding: EdgeInsets.only(bottom: _media.padding.bottom),
              child: Container(
                height: 40,
                alignment: Alignment.centerRight,
                padding: EdgeInsets.only(right: 20),
                child: GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () {
                    Navigator.pop(context, 'READY');
                  },
                  child: IgnorePointer(
                    child: Text(
                      'Готово',
                      style: const TextStyle(
                        fontSize: 16,
                        color: Color(0xff007AFF),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}

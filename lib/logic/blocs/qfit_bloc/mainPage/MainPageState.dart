part of './MainPageBloc.dart';

class MainPageMinuteState extends Equatable {
  const MainPageMinuteState();

  @override
  List<Object?> get props => [];

  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(MainPageMinuteLodedState state) loaded,
  }) {
    if (this is MainPageMinuteLodedState)
      return loaded(this as MainPageMinuteLodedState);

    return initial();
  }
}

class MainPageMinuteLodedState extends MainPageMinuteState {
  final List<MainPageModel> mainPageModel;

  const MainPageMinuteLodedState(this.mainPageModel);

  @override
  List<Object?> get props => [mainPageModel];
}

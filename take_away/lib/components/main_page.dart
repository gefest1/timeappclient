import 'dart:developer';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/main.dart';
import 'package:take_away/components/molecules/choose_rotate.dart';
import 'package:take_away/main.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';
import 'package:li/components/icons/time.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:li/components/atoms/wait_button.dart';

class TakeAwayMainPage extends StatefulWidget {
  const TakeAwayMainPage({Key? key}) : super(key: key);

  @override
  State<TakeAwayMainPage> createState() => _TakeAwayMainPageState();
}

class _TakeAwayMainPageState extends State<TakeAwayMainPage> {
  bool status = false;
  @override
  Widget build(BuildContext context) {
    return SliverPadding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      sliver: SliverList(
        delegate: SliverChildListDelegate([
          const SizedBox(height: 40),
          const Text(
            'Вкусная\nдомашняя едa\nза полчаса',
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 38,
              height: 40 / 38,
              color: ColorData.allMainBlack,
              letterSpacing: -1,
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 20),
          Center(
            child: GestureDetector(
              onTap: () {
                setState(() {
                  status = !status;
                });
                FirebaseAnalytics()
                    .logEvent(name: 'WaitButton', parameters: <String, dynamic>{
                  'Type': 'Take Away',
                });
              },
              child: WaitButton(
                image: const AssetImage('assets/take_away.png'),
                active: status,
              ),
            ),
          ),
          const SizedBox(height: 10),
          const Center(
            child: Text(
              'Июнь 2022',
              style: TextStyle(
                color: ColorData.allMainGray,
                fontSize: P3TextStyle.fontSize,
                fontWeight: P3TextStyle.fontWeight,
                height: P3TextStyle.height,
              ),
            ),
          ),
          const SizedBox(height: 20),
          const ChooseRotate(),
          const SizedBox(height: 20),
          Container(
            decoration: BoxDecoration(
              color: const Color(0xffF8F8F8),
              borderRadius: BorderRadius.circular(15),
            ),
            padding: const EdgeInsets.only(
              left: 20,
              right: 20,
              top: 20,
              bottom: 30,
            ),
            child: Column(
              children: [
                const Text(
                  "Еда приготовленная дома с любовью",
                  style: TextStyle(
                    color: ColorData.allMainBlack,
                    fontSize: 24,
                    fontWeight: FontWeight.w700,
                    height: 26 / 24,
                  ),
                ),
                const SizedBox(height: 10),
                const Text(
                  "Поварами будут домохозяйки, мамы, папы и все, кому хочется сделать готовку своим источником дохода",
                  style: TextStyle(
                    color: ColorData.allMainBlack,
                    fontSize: P2TextStyle.fontSize,
                    fontWeight: P2TextStyle.fontWeight,
                    height: P2TextStyle.height,
                  ),
                ),
                const SizedBox(height: 10),
                const Text(
                  "Чтобы всех накормить, в первую очередь нам нужны повара. Им может стать каждый",
                  style: TextStyle(
                    color: ColorData.allMainBlack,
                    fontSize: P2TextStyle.fontSize,
                    fontWeight: P2TextStyle.fontWeight,
                    height: P2TextStyle.height,
                  ),
                ),
                Image.asset('assets/food2.png'),
                SizedBox(height: 10),
                SizeTapAnimation(
                  child: GestureDetector(
                    onTap: () {
                      launch('https://timeapp.kz/business/homefood');
                    },
                    child: Container(
                      height: 60,
                      decoration: BoxDecoration(
                        color: ColorData.clientsButtonColorDefault,
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: const [
                          BoxShadow(
                            offset: Offset(0, 5),
                            blurRadius: 10,
                            color: Color(0x1a000000),
                          )
                        ],
                      ),
                      alignment: Alignment.center,
                      child: const Text(
                        'Стать поваром',
                        style: TextStyle(
                          fontSize: 18,
                          color: Color(0xffFCFCFC),
                          height: 21 / 18,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          const SizedBox(height: 40),
        ]),
      ),
    );
  }
}

import 'package:li/components/molecules/clients_finds_you_page.dart';
import 'package:li/components/molecules/description_page.dart';
import 'package:li/components/molecules/next_page.dart';
import 'package:li/components/pages/finds_client_page.dart';
import 'package:li/const.dart';
import 'package:flutter/material.dart';
import 'package:page_view_indicators/page_view_indicators.dart';

class HelpPage extends StatefulWidget {
  final VoidCallback removeOverlay;

  const HelpPage({
    required this.removeOverlay,
    Key? key,
  }) : super(key: key);

  @override
  _HelpPageState createState() => _HelpPageState();
}

class _HelpPageState extends State<HelpPage>
    with SingleTickerProviderStateMixin {
  late final AnimationController animationController = AnimationController(
    vsync: this,
    duration: Duration(
      milliseconds: 100,
    ),
  );

  late final ValueNotifier<int> _valueNotifier = ValueNotifier(0);
  late final PageController _pageController = PageController()
    ..addListener(() {
      _valueNotifier.value = _pageController.page!.round();
    });
  late final _curved = CurvedAnimation(
    parent: animationController,
    curve: Curves.easeIn,
  );

  @override
  void initState() {
    animationController.forward();
    super.initState();
  }

  @override
  void dispose() {
    _valueNotifier.dispose();
    _pageController.dispose();
    super.dispose();
  }

  void nextPage() {
    _pageController.nextPage(
      duration: Duration(
        milliseconds: 300,
      ),
      curve: Curves.easeIn,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: FadeTransition(
        opacity: _curved,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                blurRadius: 20,
                offset: Offset(0, 4),
                color: Color(0xff007AFF).withOpacity(0.14),
              ),
            ],
          ),
          child: Column(
            children: [
              Expanded(
                child: PageView(
                  controller: _pageController,
                  children: [
                    RepaintBoundary(
                      child: DescriptionPage(
                        nextPage: nextPage,
                      ),
                    ),
                    RepaintBoundary(
                      child: FindsClientsPage(
                        nextPage: nextPage,
                      ),
                    ),
                    RepaintBoundary(
                      child: ClientsFindsYouPage(
                        nextPage: nextPage,
                      ),
                    ),
                    RepaintBoundary(
                      child: NextPage(
                        nextPage: widget.removeOverlay,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 12.0),
                child: CirclePageIndicator(
                  dotColor: Color(0xffEFEFEF),
                  selectedDotColor: mainBlack,
                  itemCount: 4,
                  currentPageNotifier: _valueNotifier,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

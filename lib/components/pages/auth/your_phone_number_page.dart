import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:li/components/molecules/register_app_bar.dart';
import 'package:li/components/pages/auth/verify_phone_number_page.dart';
import 'package:li/const.dart';
import 'package:li/logic/blocs/register/RegisterBloc.dart';
import 'package:li/logic/blocs/register/RegisterEvent.dart';
import 'package:li/main.dart';
import 'package:li/utils/mask_text_input_formatter.dart';
import 'package:time_app_components/components/molecules/CustomTextField.dart';
import 'package:time_app_components/utils/testStyles.dart';

class YourPhoneNumber extends StatefulWidget {
  const YourPhoneNumber({Key? key}) : super(key: key);

  @override
  _YourPhoneNumberState createState() => _YourPhoneNumberState();
}

class _YourPhoneNumberState extends State<YourPhoneNumber> {
  final StreamController<Null> streamController = StreamController<Null>();
  late final TextEditingController controller = TextEditingController(
    text: "+",
  )..addListener(() {
      streamController.add(null);
    });

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final _focus = FocusScope.of(context);
        if (_focus.hasFocus || _focus.hasPrimaryFocus) {
          _focus.unfocus();
        }
      },
      child: Material(
        color: Colors.white,
        child: AnnotatedRegion(
          value: SystemUiOverlayStyle(statusBarBrightness: Brightness.light),
          child: Stack(
            children: [
              CustomScrollView(
                physics: ClampingScrollPhysics(),
                slivers: [
                  SliverList(
                    delegate: SliverChildListDelegate([
                      const RegisterAppBar(),
                      const SizedBox(height: 40),
                    ]),
                  ),
                  SliverPadding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    sliver: SliverList(
                      delegate: SliverChildListDelegate([
                        const Text(
                          'Ваш номер телефона',
                          style: TextStyle(
                            fontSize: H1TextStyle.fontSize,
                            color: Color(0xff323232),
                            fontWeight: H1TextStyle.fontWeight,
                            height: H1TextStyle.height,
                          ),
                        ),
                        const SizedBox(height: 20),
                        SizedBox(
                          height: 64,
                          child: CustomTextField(
                            label: 'Номер телефона',
                            controller: controller,
                            keyboardType: TextInputType.phone,
                            inputFormatters: [
                              MaskTextInputFormatter(
                                mask: '+# ### ### ## ##',
                                filter: {
                                  "#": RegExp(r'[0-9]'),
                                },
                                initialText: "+",
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(height: 10),
                        Text(
                          'Мы отправим код подтверждения для входа на этот номер',
                          style: const TextStyle(
                            color: Color(0xff323232),
                            fontSize: 18,
                          ),
                        ),
                      ]),
                    ),
                  ),
                ],
              ),
              // ValueListenableBuilder(valueListenable: streamController, builder: builder)
              StreamBuilder(
                stream: streamController.stream,
                builder: (_, asyn) {
                  if (controller.text.length < 4) return const SizedBox();
                  return Positioned(
                    bottom: 16 + MediaQuery.of(context).viewInsets.bottom,
                    left: 20,
                    right: 20,
                    height: 64,
                    child: ElevatedButton(
                      onPressed: () async {
                        if (controller.text.length != 16) return;
                        RegisterBloc.of(context).add(
                          SendCodeRegisterEvent(phoneNumber: controller.text),
                        );

                        routerDelegate.push(
                          VerifyPhoneNumberPage(
                            phoneNumber: controller.text,
                          ),
                        );
                      },
                      child: Text(
                        'Получить СМС с кодом',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(buttonsDefault),
                        shape: MaterialStateProperty.all(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(16),
                          ),
                        ),
                        elevation: MaterialStateProperty.all(0.0),
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

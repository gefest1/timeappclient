import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:provider/provider.dart';
import 'package:time_app_components/components/molecules/CustomTextField.dart';

import 'package:li/components/molecules/bottom_auth_navigation.dart';
import 'package:li/components/molecules/register_app_bar.dart';
import 'package:li/components/molecules/sex_choose.dart';
import 'package:li/const.dart';
import 'package:li/data/models/client_model.dart';
import 'package:li/data/models/sex.dart';
import 'package:li/main.dart';
import 'package:li/network/graphql_provider.dart';
import 'package:li/utils/restart_widget.dart';
import 'package:li/utils/pin_code_text_field.dart';
import 'package:time_app_components/utils/testStyles.dart';

class HowToShowToSpecPage extends StatefulWidget {
  final String? phone;
  const HowToShowToSpecPage({
    Key? key,
    this.phone,
  }) : super(key: key);

  @override
  _HowToShowToSpecPageState createState() => _HowToShowToSpecPageState();
}

class _HowToShowToSpecPageState extends State<HowToShowToSpecPage> {
  late final TextEditingController fullNameTextEditingController =
      TextEditingController()..addListener(validate);
  late final TextEditingController yearTextEditingController =
      TextEditingController()..addListener(validate);
  CollectionReference session =
      FirebaseFirestore.instance.collection('Registered Clients');
  SexEnum? sex;
  bool _isValid = false;
  bool isLoading = false;

  @override
  void dispose() {
    fullNameTextEditingController.dispose();
    super.dispose();
  }

  void validate() {
    final _newValue = fullNameTextEditingController.text.isNotEmpty &&
        (yearTextEditingController.text.length == 4 ||
            yearTextEditingController.text.isEmpty);
    if (_isValid != _newValue)
      setState(() {
        _isValid = _newValue;
      });
  }

  void registerUser({
    required String fullName,
    SexEnum? sex,
    DateTime? dateTime,
  }) async {
    setState(() {
      isLoading = true;
    });
    try {
      final graphqlProvider =
          Provider.of<GraphqlProvider>(context, listen: false);
      final mutationResult = await graphqlProvider.client.mutate(
        MutationOptions(
          document: gql(''' 
          mutation(
          	\$dateOfBirth: DateTime
            \$fullName: String!
            \$sex: SexEnum
          ) {
            registerClient(
              dateOfBirth: \$dateOfBirth
              fullName: \$fullName
              sex: \$sex
            ) {
              _id
              fullName
              dateOfBirth
              sex
            }
            refreshToken
          }'''),
          variables: {
            'fullName': fullName,
            'dateOfBirth': dateTime?.toIso8601String(),
            'sex': sex?.toString(),
          },
        ),
      );

      if (mutationResult.hasException) throw mutationResult.exception!;
      final _client = Client.fromMap(mutationResult.data!['registerClient']);
      await Future.wait([
        sharedPreferences.setString(
          'client',
          _client.toJson(),
        ),
        sharedPreferences.setString(
          'token',
          mutationResult.data!['refreshToken'],
        )
      ]);

      RestartWidget.restartApp(context);
      return;
    } catch (e) {}
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final _focus = FocusScope.of(context);
        if (_focus.hasFocus || _focus.hasPrimaryFocus) {
          _focus.unfocus();
        }
      },
      child: Material(
        color: Colors.white,
        child: AnnotatedRegion(
          value: SystemUiOverlayStyle(statusBarBrightness: Brightness.light),
          child: isLoading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : Stack(
                  children: [
                    CustomScrollView(
                      physics: ClampingScrollPhysics(),
                      slivers: [
                        SliverList(
                          delegate: SliverChildListDelegate([
                            const RegisterAppBar(),
                            const SizedBox(height: 40),
                          ]),
                        ),
                        SliverPadding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          sliver: SliverList(
                            delegate: SliverChildListDelegate([
                              const Text(
                                'Как вас представлять исполнителям?',
                                style: TextStyle(
                                  fontSize: H1TextStyle.fontSize,
                                  color: Color(0xff323232),
                                  fontWeight: H1TextStyle.fontWeight,
                                  height: H1TextStyle.height,
                                ),
                              ),
                              const SizedBox(height: 10),
                              Text(
                                'Ваше имя будет видно компаниям и людям, которые будут оказывать вам услуги',
                                style: const TextStyle(
                                  color: Color(0xff323232),
                                  fontSize: 18,
                                ),
                              ),
                              const SizedBox(height: 20),
                              SizedBox(
                                height: 64,
                                child: CustomTextField(
                                  label: 'Имя',
                                  controller: fullNameTextEditingController,
                                ),
                              ),
                            ]),
                          ),
                        ),
                        SliverToBoxAdapter(
                          child: const SizedBox(height: 200),
                        ),
                      ],
                    ),
                    if (_isValid)
                      BottomAuthNav(
                        onTap: () {
                          session.add({
                            'Name': fullNameTextEditingController.text,
                            'BirthYear': yearTextEditingController.text,
                            'Sex': sex?.toString(),
                            'PhoneNumber': widget.phone,
                            'RegistrationDate': Timestamp.now(),
                          }).then((value) => print("client info logged"));
                          FirebaseAnalytics()
                              .logEvent(name: 'RegistrationCount');
                          registerUser(
                            dateTime: yearTextEditingController.text.isEmpty
                                ? null
                                : new DateTime(
                                    int.parse(yearTextEditingController.text),
                                  ),
                            fullName: fullNameTextEditingController.text,
                            sex: sex,
                          );
                        },
                      ),
                  ],
                ),
        ),
      ),
    );
  }
}

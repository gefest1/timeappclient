import 'dart:convert';

import 'package:equatable/equatable.dart';

class Translate extends Equatable {
  final String? ru;

  const Translate({
    required this.ru,
  });

  Translate copyWith({
    String? ru,
  }) {
    return Translate(
      ru: ru ?? this.ru,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'ru': ru,
    };
  }

  static Translate? fromMap(Map<String, dynamic>? map) {
    if (map == null) return null;
    return Translate(
      ru: map['ru'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Translate.fromJson(String source) =>
      Translate.fromMap(json.decode(source))!;

  @override
  bool get stringify => true;

  @override
  List<Object?> get props => [ru];
}

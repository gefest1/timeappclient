import 'package:flutter/material.dart';

//Copy this CustomPainter code to the Bottom of the File
class Text1TypeCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.4451791, size.height * 0.8764294);
    path_0.lineTo(size.width * 0.4547902, size.height * 0.8771718);
    path_0.lineTo(size.width * 0.4526712, size.height * 0.9045890);
    path_0.lineTo(size.width * 0.4844638, size.height * 0.9070429);
    path_0.lineTo(size.width * 0.4865828, size.height * 0.8796258);
    path_0.lineTo(size.width * 0.4961939, size.height * 0.8803681);
    path_0.lineTo(size.width * 0.4914080, size.height * 0.9422822);
    path_0.lineTo(size.width * 0.4817969, size.height * 0.9415399);
    path_0.lineTo(size.width * 0.4838202, size.height * 0.9153681);
    path_0.lineTo(size.width * 0.4520276, size.height * 0.9129080);
    path_0.lineTo(size.width * 0.4500043, size.height * 0.9390798);
    path_0.lineTo(size.width * 0.4403939, size.height * 0.9383374);
    path_0.lineTo(size.width * 0.4451791, size.height * 0.8764294);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_0, paint_0_fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.4192687, size.height * 0.8776748);
    path_1.cubicTo(
        size.width * 0.4160736,
        size.height * 0.8769755,
        size.width * 0.4131859,
        size.height * 0.8773558,
        size.width * 0.4106049,
        size.height * 0.8788221);
    path_1.cubicTo(
        size.width * 0.4079902,
        size.height * 0.8803067,
        size.width * 0.4063699,
        size.height * 0.8824785,
        size.width * 0.4057454,
        size.height * 0.8853313);
    path_1.lineTo(size.width * 0.4049822, size.height * 0.8888221);
    path_1.lineTo(size.width * 0.4160178, size.height * 0.8905337);
    path_1.cubicTo(
        size.width * 0.4187798,
        size.height * 0.8909264,
        size.width * 0.4210288,
        size.height * 0.8907178,
        size.width * 0.4227650,
        size.height * 0.8898896);
    path_1.cubicTo(
        size.width * 0.4244675,
        size.height * 0.8890859,
        size.width * 0.4255270,
        size.height * 0.8877362,
        size.width * 0.4259436,
        size.height * 0.8858282);
    path_1.cubicTo(
        size.width * 0.4263663,
        size.height * 0.8838957,
        size.width * 0.4259779,
        size.height * 0.8821779,
        size.width * 0.4247779,
        size.height * 0.8806871);
    path_1.cubicTo(
        size.width * 0.4235436,
        size.height * 0.8792086,
        size.width * 0.4217067,
        size.height * 0.8782086,
        size.width * 0.4192687,
        size.height * 0.8776748);
    path_1.close();
    path_1.moveTo(size.width * 0.4233626, size.height * 0.8710368);
    path_1.cubicTo(
        size.width * 0.4277067,
        size.height * 0.8719877,
        size.width * 0.4309828,
        size.height * 0.8739939,
        size.width * 0.4331902,
        size.height * 0.8770613);
    path_1.cubicTo(
        size.width * 0.4353693,
        size.height * 0.8801227,
        size.width * 0.4360276,
        size.height * 0.8836258,
        size.width * 0.4351638,
        size.height * 0.8875828);
    path_1.cubicTo(
        size.width * 0.4343184,
        size.height * 0.8914479,
        size.width * 0.4322178,
        size.height * 0.8942025,
        size.width * 0.4288632,
        size.height * 0.8958466);
    path_1.cubicTo(
        size.width * 0.4255018,
        size.height * 0.8975215,
        size.width * 0.4211706,
        size.height * 0.8979264,
        size.width * 0.4158681,
        size.height * 0.8970613);
    path_1.lineTo(size.width * 0.4036043, size.height * 0.8951288);
    path_1.lineTo(size.width * 0.4028595, size.height * 0.8985337);
    path_1.cubicTo(
        size.width * 0.4023209,
        size.height * 0.9010000,
        size.width * 0.4026859,
        size.height * 0.9030920,
        size.width * 0.4039552,
        size.height * 0.9048098);
    path_1.cubicTo(
        size.width * 0.4052184,
        size.height * 0.9065521,
        size.width * 0.4072374,
        size.height * 0.9077301,
        size.width * 0.4100117,
        size.height * 0.9083374);
    path_1.cubicTo(
        size.width * 0.4125902,
        size.height * 0.9089018,
        size.width * 0.4148178,
        size.height * 0.9087853,
        size.width * 0.4166945,
        size.height * 0.9079877);
    path_1.cubicTo(
        size.width * 0.4185436,
        size.height * 0.9071902,
        size.width * 0.4198988,
        size.height * 0.9058282,
        size.width * 0.4207601,
        size.height * 0.9039018);
    path_1.lineTo(size.width * 0.4293362, size.height * 0.9057730);
    path_1.cubicTo(
        size.width * 0.4281896,
        size.height * 0.9098098,
        size.width * 0.4256951,
        size.height * 0.9127607,
        size.width * 0.4218521,
        size.height * 0.9146258);
    path_1.cubicTo(
        size.width * 0.4180092,
        size.height * 0.9164847,
        size.width * 0.4134117,
        size.height * 0.9168282,
        size.width * 0.4080583,
        size.height * 0.9156626);
    path_1.cubicTo(
        size.width * 0.4027049,
        size.height * 0.9144908,
        size.width * 0.3987331,
        size.height * 0.9122454,
        size.width * 0.3961417,
        size.height * 0.9089141);
    path_1.cubicTo(
        size.width * 0.3935227,
        size.height * 0.9055828,
        size.width * 0.3926969,
        size.height * 0.9016994,
        size.width * 0.3936650,
        size.height * 0.8972761);
    path_1.lineTo(size.width * 0.4003270, size.height * 0.8667975);
    path_1.lineTo(size.width * 0.4090294, size.height * 0.8686994);
    path_1.lineTo(size.width * 0.4074399, size.height * 0.8759693);
    path_1.lineTo(size.width * 0.4076497, size.height * 0.8760184);
    path_1.cubicTo(
        size.width * 0.4094442,
        size.height * 0.8738528,
        size.width * 0.4117896,
        size.height * 0.8723252,
        size.width * 0.4146865,
        size.height * 0.8714294);
    path_1.cubicTo(
        size.width * 0.4175558,
        size.height * 0.8705337,
        size.width * 0.4204479,
        size.height * 0.8703988,
        size.width * 0.4233626,
        size.height * 0.8710368);
    path_1.close();

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_1, paint_1_fill);

    Path path_2 = Path();
    path_2.moveTo(size.width * 0.3638031, size.height * 0.8953742);
    path_2.lineTo(size.width * 0.3728037, size.height * 0.8984417);
    path_2.lineTo(size.width * 0.3766399, size.height * 0.8872025);
    path_2.lineTo(size.width * 0.3684945, size.height * 0.8844233);
    path_2.cubicTo(
        size.width * 0.3635258,
        size.height * 0.8827301,
        size.width * 0.3603926,
        size.height * 0.8837791,
        size.width * 0.3590957,
        size.height * 0.8875828);
    path_2.cubicTo(
        size.width * 0.3584933,
        size.height * 0.8893497,
        size.width * 0.3586000,
        size.height * 0.8909018,
        size.width * 0.3594160,
        size.height * 0.8922393);
    path_2.cubicTo(
        size.width * 0.3602227,
        size.height * 0.8936074,
        size.width * 0.3616853,
        size.height * 0.8946503,
        size.width * 0.3638031,
        size.height * 0.8953742);
    path_2.close();
    path_2.moveTo(size.width * 0.3700227, size.height * 0.8780798);
    path_2.lineTo(size.width * 0.3787387, size.height * 0.8810552);
    path_2.lineTo(size.width * 0.3830613, size.height * 0.8683865);
    path_2.lineTo(size.width * 0.3733681, size.height * 0.8650798);
    path_2.cubicTo(
        size.width * 0.3708706,
        size.height * 0.8642270,
        size.width * 0.3687890,
        size.height * 0.8641104,
        size.width * 0.3671245,
        size.height * 0.8647239);
    path_2.cubicTo(
        size.width * 0.3654503,
        size.height * 0.8653620,
        size.width * 0.3642613,
        size.height * 0.8667178,
        size.width * 0.3635571,
        size.height * 0.8687791);
    path_2.cubicTo(
        size.width * 0.3621117,
        size.height * 0.8730123,
        size.width * 0.3642669,
        size.height * 0.8761166,
        size.width * 0.3700227,
        size.height * 0.8780798);
    path_2.close();
    path_2.moveTo(size.width * 0.3939184, size.height * 0.8649571);
    path_2.lineTo(size.width * 0.3792957, size.height * 0.9077975);
    path_2.lineTo(size.width * 0.3598288, size.height * 0.9011534);
    path_2.cubicTo(
        size.width * 0.3556472,
        size.height * 0.8997301,
        size.width * 0.3527092,
        size.height * 0.8976319,
        size.width * 0.3510141,
        size.height * 0.8948712);
    path_2.cubicTo(
        size.width * 0.3492828,
        size.height * 0.8921288,
        size.width * 0.3489963,
        size.height * 0.8890613,
        size.width * 0.3501546,
        size.height * 0.8856687);
    path_2.cubicTo(
        size.width * 0.3509147,
        size.height * 0.8834417,
        size.width * 0.3523061,
        size.height * 0.8816748,
        size.width * 0.3543294,
        size.height * 0.8803620);
    path_2.cubicTo(
        size.width * 0.3563166,
        size.height * 0.8790675,
        size.width * 0.3584067,
        size.height * 0.8785890,
        size.width * 0.3606000,
        size.height * 0.8789080);
    path_2.lineTo(size.width * 0.3607110, size.height * 0.8785828);
    path_2.cubicTo(
        size.width * 0.3580712,
        size.height * 0.8772577,
        size.width * 0.3562202,
        size.height * 0.8753988,
        size.width * 0.3551577,
        size.height * 0.8730061);
    path_2.cubicTo(
        size.width * 0.3540859,
        size.height * 0.8706380,
        size.width * 0.3540086,
        size.height * 0.8681104,
        size.width * 0.3549264,
        size.height * 0.8654233);
    path_2.cubicTo(
        size.width * 0.3562141,
        size.height * 0.8616503,
        size.width * 0.3585957,
        size.height * 0.8591166,
        size.width * 0.3620706,
        size.height * 0.8578160);
    path_2.cubicTo(
        size.width * 0.3655184,
        size.height * 0.8565031,
        size.width * 0.3695638,
        size.height * 0.8566442,
        size.width * 0.3742067,
        size.height * 0.8582270);
    path_2.lineTo(size.width * 0.3939184, size.height * 0.8649571);
    path_2.close();

    Paint paint_2_fill = Paint()..style = PaintingStyle.fill;
    paint_2_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_2, paint_2_fill);

    Path path_3 = Path();
    path_3.moveTo(size.width * 0.3173166, size.height * 0.8331227);
    path_3.lineTo(size.width * 0.2970810, size.height * 0.8736196);
    path_3.lineTo(size.width * 0.2888429, size.height * 0.8695031);
    path_3.lineTo(size.width * 0.3090785, size.height * 0.8290061);
    path_3.lineTo(size.width * 0.3173166, size.height * 0.8331227);
    path_3.close();
    path_3.moveTo(size.width * 0.3273706, size.height * 0.8649877);
    path_3.lineTo(size.width * 0.3343761, size.height * 0.8684908);
    path_3.lineTo(size.width * 0.3418399, size.height * 0.8535521);
    path_3.lineTo(size.width * 0.3348337, size.height * 0.8500552);
    path_3.cubicTo(
        size.width * 0.3324472,
        size.height * 0.8488589,
        size.width * 0.3302080,
        size.height * 0.8485951,
        size.width * 0.3281172,
        size.height * 0.8492454);
    path_3.cubicTo(
        size.width * 0.3259871,
        size.height * 0.8499141,
        size.width * 0.3243650,
        size.height * 0.8513620,
        size.width * 0.3232491,
        size.height * 0.8535951);
    path_3.cubicTo(
        size.width * 0.3221209,
        size.height * 0.8558589,
        size.width * 0.3219350,
        size.height * 0.8580245,
        size.width * 0.3226926,
        size.height * 0.8601043);
    path_3.cubicTo(
        size.width * 0.3234503,
        size.height * 0.8621779,
        size.width * 0.3250092,
        size.height * 0.8638098,
        size.width * 0.3273706,
        size.height * 0.8649877);
    path_3.close();
    path_3.moveTo(size.width * 0.3533859, size.height * 0.8511472);
    path_3.lineTo(size.width * 0.3331503, size.height * 0.8916442);
    path_3.lineTo(size.width * 0.3248742, size.height * 0.8875092);
    path_3.lineTo(size.width * 0.3311258, size.height * 0.8749939);
    path_3.lineTo(size.width * 0.3233883, size.height * 0.8711288);
    path_3.cubicTo(
        size.width * 0.3186920,
        size.height * 0.8687853,
        size.width * 0.3155926,
        size.height * 0.8656442,
        size.width * 0.3140902,
        size.height * 0.8617239);
    path_3.cubicTo(
        size.width * 0.3125497,
        size.height * 0.8578098,
        size.width * 0.3128442,
        size.height * 0.8537239,
        size.width * 0.3149730,
        size.height * 0.8494601);
    path_3.cubicTo(
        size.width * 0.3171141,
        size.height * 0.8451779,
        size.width * 0.3202184,
        size.height * 0.8424601,
        size.width * 0.3242853,
        size.height * 0.8413190);
    path_3.cubicTo(
        size.width * 0.3283135,
        size.height * 0.8401902,
        size.width * 0.3326761,
        size.height * 0.8407975,
        size.width * 0.3373724,
        size.height * 0.8431411);
    path_3.lineTo(size.width * 0.3533859, size.height * 0.8511472);
    path_3.close();

    Paint paint_3_fill = Paint()..style = PaintingStyle.fill;
    paint_3_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_3, paint_3_fill);

    Path path_4 = Path();
    path_4.moveTo(size.width * 0.2783405, size.height * 0.8083497);
    path_4.lineTo(size.width * 0.2672773, size.height * 0.8246564);
    path_4.lineTo(size.width * 0.2842294, size.height * 0.8361595);
    path_4.lineTo(size.width * 0.2952926, size.height * 0.8198466);
    path_4.lineTo(size.width * 0.3029497, size.height * 0.8250368);
    path_4.lineTo(size.width * 0.2775380, size.height * 0.8625031);
    path_4.lineTo(size.width * 0.2698810, size.height * 0.8573129);
    path_4.lineTo(size.width * 0.2801233, size.height * 0.8422086);
    path_4.lineTo(size.width * 0.2631706, size.height * 0.8307117);
    path_4.lineTo(size.width * 0.2529288, size.height * 0.8458160);
    path_4.lineTo(size.width * 0.2452718, size.height * 0.8406196);
    path_4.lineTo(size.width * 0.2706834, size.height * 0.8031534);
    path_4.lineTo(size.width * 0.2783405, size.height * 0.8083497);
    path_4.close();

    Paint paint_4_fill = Paint()..style = PaintingStyle.fill;
    paint_4_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_4, paint_4_fill);

    Path path_5 = Path();
    path_5.moveTo(size.width * 0.2354288, size.height * 0.7801350);
    path_5.cubicTo(
        size.width * 0.2411215,
        size.height * 0.7795337,
        size.width * 0.2464239,
        size.height * 0.7813742,
        size.width * 0.2513356,
        size.height * 0.7856503);
    path_5.cubicTo(
        size.width * 0.2562472,
        size.height * 0.7899264,
        size.width * 0.2587994,
        size.height * 0.7949264,
        size.width * 0.2589920,
        size.height * 0.8006442);
    path_5.cubicTo(
        size.width * 0.2591656,
        size.height * 0.8063926,
        size.width * 0.2568693,
        size.height * 0.8120000,
        size.width * 0.2521031,
        size.height * 0.8174724);
    path_5.cubicTo(
        size.width * 0.2473558,
        size.height * 0.8229264,
        size.width * 0.2421362,
        size.height * 0.8259509,
        size.width * 0.2364429,
        size.height * 0.8265460);
    path_5.cubicTo(
        size.width * 0.2307313,
        size.height * 0.8271656,
        size.width * 0.2254190,
        size.height * 0.8253374,
        size.width * 0.2205074,
        size.height * 0.8210613);
    path_5.cubicTo(
        size.width * 0.2155957,
        size.height * 0.8167853,
        size.width * 0.2130534,
        size.height * 0.8117730,
        size.width * 0.2128798,
        size.height * 0.8060307);
    path_5.cubicTo(
        size.width * 0.2126871,
        size.height * 0.8003129,
        size.width * 0.2149650,
        size.height * 0.7947239,
        size.width * 0.2197123,
        size.height * 0.7892699);
    path_5.cubicTo(
        size.width * 0.2244779,
        size.height * 0.7837975,
        size.width * 0.2297172,
        size.height * 0.7807485,
        size.width * 0.2354288,
        size.height * 0.7801350);
    path_5.close();
    path_5.moveTo(size.width * 0.2463061, size.height * 0.7914294);
    path_5.cubicTo(
        size.width * 0.2435147,
        size.height * 0.7890000,
        size.width * 0.2403945,
        size.height * 0.7881288,
        size.width * 0.2369442,
        size.height * 0.7888098);
    path_5.cubicTo(
        size.width * 0.2334939,
        size.height * 0.7894969,
        size.width * 0.2301301,
        size.height * 0.7917239,
        size.width * 0.2268521,
        size.height * 0.7954908);
    path_5.cubicTo(
        size.width * 0.2235933,
        size.height * 0.7992331,
        size.width * 0.2218724,
        size.height * 0.8028712,
        size.width * 0.2216896,
        size.height * 0.8063988);
    path_5.cubicTo(
        size.width * 0.2214853,
        size.height * 0.8099080,
        size.width * 0.2227675,
        size.height * 0.8128712,
        size.width * 0.2255374,
        size.height * 0.8152822);
    path_5.cubicTo(
        size.width * 0.2283067,
        size.height * 0.8176933,
        size.width * 0.2314276,
        size.height * 0.8185644,
        size.width * 0.2348994,
        size.height * 0.8179018);
    path_5.cubicTo(
        size.width * 0.2383491,
        size.height * 0.8172147,
        size.width * 0.2417037,
        size.height * 0.8150000,
        size.width * 0.2449626,
        size.height * 0.8112577);
    path_5.cubicTo(
        size.width * 0.2482405,
        size.height * 0.8074908,
        size.width * 0.2499816,
        size.height * 0.8038528,
        size.width * 0.2501865,
        size.height * 0.8003436);
    path_5.cubicTo(
        size.width * 0.2503693,
        size.height * 0.7968098,
        size.width * 0.2490755,
        size.height * 0.7938405,
        size.width * 0.2463061,
        size.height * 0.7914294);
    path_5.close();

    Paint paint_5_fill = Paint()..style = PaintingStyle.fill;
    paint_5_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_5, paint_5_fill);

    Path path_6 = Path();
    path_6.moveTo(size.width * 0.1826055, size.height * 0.7609141);
    path_6.lineTo(size.width * 0.1886288, size.height * 0.7675337);
    path_6.cubicTo(
        size.width * 0.1873982,
        size.height * 0.7695092,
        size.width * 0.1868613,
        size.height * 0.7716258,
        size.width * 0.1870190,
        size.height * 0.7738896);
    path_6.cubicTo(
        size.width * 0.1871761,
        size.height * 0.7761472,
        size.width * 0.1881810,
        size.height * 0.7783006,
        size.width * 0.1900344,
        size.height * 0.7803374);
    path_6.cubicTo(
        size.width * 0.1924472,
        size.height * 0.7829877,
        size.width * 0.1954491,
        size.height * 0.7841534,
        size.width * 0.1990380,
        size.height * 0.7838405);
    path_6.cubicTo(
        size.width * 0.2025871,
        size.height * 0.7835215,
        size.width * 0.2061227,
        size.height * 0.7817546,
        size.width * 0.2096454,
        size.height * 0.7785521);
    path_6.cubicTo(
        size.width * 0.2132534,
        size.height * 0.7752699,
        size.width * 0.2153730,
        size.height * 0.7718896,
        size.width * 0.2160043,
        size.height * 0.7684049);
    path_6.cubicTo(
        size.width * 0.2165957,
        size.height * 0.7649202,
        size.width * 0.2156656,
        size.height * 0.7618282,
        size.width * 0.2132135,
        size.height * 0.7591350);
    path_6.cubicTo(
        size.width * 0.2113798,
        size.height * 0.7571166,
        size.width * 0.2093871,
        size.height * 0.7558834,
        size.width * 0.2072350,
        size.height * 0.7554356);
    path_6.cubicTo(
        size.width * 0.2050620,
        size.height * 0.7550123,
        size.width * 0.2028724,
        size.height * 0.7553558,
        size.width * 0.2006669,
        size.height * 0.7564663);
    path_6.lineTo(size.width * 0.1946436, size.height * 0.7498466);
    path_6.cubicTo(
        size.width * 0.1984834,
        size.height * 0.7472454,
        size.width * 0.2026301,
        size.height * 0.7463067,
        size.width * 0.2070847,
        size.height * 0.7470245);
    path_6.cubicTo(
        size.width * 0.2114982,
        size.height * 0.7477423,
        size.width * 0.2154810,
        size.height * 0.7500491,
        size.width * 0.2190331,
        size.height * 0.7539571);
    path_6.cubicTo(
        size.width * 0.2234153,
        size.height * 0.7587730,
        size.width * 0.2253405,
        size.height * 0.7640245,
        size.width * 0.2248080,
        size.height * 0.7697055);
    path_6.cubicTo(
        size.width * 0.2242755,
        size.height * 0.7753865,
        size.width * 0.2213350,
        size.height * 0.7806564,
        size.width * 0.2159871,
        size.height * 0.7855215);
    path_6.cubicTo(
        size.width * 0.2107245,
        size.height * 0.7903129,
        size.width * 0.2052288,
        size.height * 0.7927117,
        size.width * 0.1995012,
        size.height * 0.7927239);
    path_6.cubicTo(
        size.width * 0.1937331,
        size.height * 0.7927362,
        size.width * 0.1886871,
        size.height * 0.7903681,
        size.width * 0.1843626,
        size.height * 0.7856135);
    path_6.cubicTo(
        size.width * 0.1807521,
        size.height * 0.7816442,
        size.width * 0.1788712,
        size.height * 0.7773988,
        size.width * 0.1787190,
        size.height * 0.7728834);
    path_6.cubicTo(
        size.width * 0.1785264,
        size.height * 0.7683681,
        size.width * 0.1798221,
        size.height * 0.7643804,
        size.width * 0.1826055,
        size.height * 0.7609141);
    path_6.close();

    Paint paint_6_fill = Paint()..style = PaintingStyle.fill;
    paint_6_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_6, paint_6_fill);

    Path path_7 = Path();
    path_7.moveTo(size.width * 0.1529810, size.height * 0.6561043);
    path_7.lineTo(size.width * 0.1575117, size.height * 0.6646135);
    path_7.lineTo(size.width * 0.1332399, size.height * 0.6775337);
    path_7.lineTo(size.width * 0.1482282, size.height * 0.7056810);
    path_7.lineTo(size.width * 0.1724994, size.height * 0.6927546);
    path_7.lineTo(size.width * 0.1770301, size.height * 0.7012638);
    path_7.lineTo(size.width * 0.1222202, size.height * 0.7304540);
    path_7.lineTo(size.width * 0.1176896, size.height * 0.7219448);
    path_7.lineTo(size.width * 0.1408595, size.height * 0.7096074);
    path_7.lineTo(size.width * 0.1258712, size.height * 0.6814601);
    path_7.lineTo(size.width * 0.1027012, size.height * 0.6937975);
    path_7.lineTo(size.width * 0.09817055, size.height * 0.6852883);
    path_7.lineTo(size.width * 0.1529810, size.height * 0.6561043);
    path_7.close();

    Paint paint_7_fill = Paint()..style = PaintingStyle.fill;
    paint_7_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_7, paint_7_fill);

    Path path_8 = Path();
    path_8.moveTo(size.width * 0.1380902, size.height * 0.6350552);
    path_8.cubicTo(
        size.width * 0.1369644,
        size.height * 0.6319877,
        size.width * 0.1350920,
        size.height * 0.6297546,
        size.width * 0.1324730,
        size.height * 0.6283620);
    path_8.cubicTo(
        size.width * 0.1298172,
        size.height * 0.6269509,
        size.width * 0.1271160,
        size.height * 0.6267546,
        size.width * 0.1243681,
        size.height * 0.6277607);
    path_8.lineTo(size.width * 0.1210147, size.height * 0.6289877);
    path_8.lineTo(size.width * 0.1254982, size.height * 0.6392147);
    path_8.cubicTo(
        size.width * 0.1266448,
        size.height * 0.6417607,
        size.width * 0.1280319,
        size.height * 0.6435460,
        size.width * 0.1296601,
        size.height * 0.6445644);
    path_8.cubicTo(
        size.width * 0.1312509,
        size.height * 0.6455706,
        size.width * 0.1329620,
        size.height * 0.6457362,
        size.width * 0.1347939,
        size.height * 0.6450675);
    path_8.cubicTo(
        size.width * 0.1366521,
        size.height * 0.6443865,
        size.width * 0.1378902,
        size.height * 0.6431350,
        size.width * 0.1385080,
        size.height * 0.6413190);
    path_8.cubicTo(
        size.width * 0.1390890,
        size.height * 0.6394908,
        size.width * 0.1389497,
        size.height * 0.6373988,
        size.width * 0.1380902,
        size.height * 0.6350552);
    path_8.close();
    path_8.moveTo(size.width * 0.1458883, size.height * 0.6349509);
    path_8.cubicTo(
        size.width * 0.1474190,
        size.height * 0.6391227,
        size.width * 0.1474834,
        size.height * 0.6429632,
        size.width * 0.1460810,
        size.height * 0.6464724);
    path_8.cubicTo(
        size.width * 0.1446687,
        size.height * 0.6499571,
        size.width * 0.1420638,
        size.height * 0.6523926,
        size.width * 0.1382656,
        size.height * 0.6537853);
    path_8.cubicTo(
        size.width * 0.1345485,
        size.height * 0.6551472,
        size.width * 0.1310957,
        size.height * 0.6548589,
        size.width * 0.1279067,
        size.height * 0.6529080);
    path_8.cubicTo(
        size.width * 0.1246902,
        size.height * 0.6509693,
        size.width * 0.1220245,
        size.height * 0.6475337,
        size.width * 0.1199080,
        size.height * 0.6425951);
    path_8.lineTo(size.width * 0.1149546, size.height * 0.6312086);
    path_8.lineTo(size.width * 0.1116816, size.height * 0.6324110);
    path_8.cubicTo(
        size.width * 0.1093117,
        size.height * 0.6332822,
        size.width * 0.1077429,
        size.height * 0.6347117,
        size.width * 0.1069761,
        size.height * 0.6367055);
    path_8.cubicTo(
        size.width * 0.1061828,
        size.height * 0.6387055,
        size.width * 0.1062748,
        size.height * 0.6410429,
        size.width * 0.1072528,
        size.height * 0.6437055);
    path_8.cubicTo(
        size.width * 0.1081613,
        size.height * 0.6461840,
        size.width * 0.1094540,
        size.height * 0.6480000,
        size.width * 0.1111313,
        size.height * 0.6491595);
    path_8.cubicTo(
        size.width * 0.1127988,
        size.height * 0.6502883,
        size.width * 0.1146761,
        size.height * 0.6506994,
        size.width * 0.1167632,
        size.height * 0.6503926);
    path_8.lineTo(size.width * 0.1197859, size.height * 0.6586380);
    path_8.cubicTo(
        size.width * 0.1157644,
        size.height * 0.6598344,
        size.width * 0.1119368,
        size.height * 0.6593129,
        size.width * 0.1083031,
        size.height * 0.6570736);
    path_8.cubicTo(
        size.width * 0.1046693,
        size.height * 0.6548282,
        size.width * 0.1019098,
        size.height * 0.6511350,
        size.width * 0.1000233,
        size.height * 0.6459939);
    path_8.cubicTo(
        size.width * 0.09813681,
        size.height * 0.6408466,
        size.width * 0.09790123,
        size.height * 0.6362883,
        size.width * 0.09931718,
        size.height * 0.6323190);
    path_8.cubicTo(
        size.width * 0.1007233,
        size.height * 0.6283190,
        size.width * 0.1035540,
        size.height * 0.6255399,
        size.width * 0.1078098,
        size.height * 0.6239755);
    path_8.lineTo(size.width * 0.1371018, size.height * 0.6132362);
    path_8.lineTo(size.width * 0.1401681, size.height * 0.6216012);
    path_8.lineTo(size.width * 0.1331785, size.height * 0.6241595);
    path_8.lineTo(size.width * 0.1332528, size.height * 0.6243620);
    path_8.cubicTo(
        size.width * 0.1360405,
        size.height * 0.6247178,
        size.width * 0.1385890,
        size.height * 0.6258773,
        size.width * 0.1408982,
        size.height * 0.6278405);
    path_8.cubicTo(
        size.width * 0.1431975,
        size.height * 0.6297791,
        size.width * 0.1448613,
        size.height * 0.6321472,
        size.width * 0.1458883,
        size.height * 0.6349509);
    path_8.close();

    Paint paint_8_fill = Paint()..style = PaintingStyle.fill;
    paint_8_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_8, paint_8_fill);

    Path path_9 = Path();
    path_9.moveTo(size.width * 0.09326748, size.height * 0.5972221);
    path_9.lineTo(size.width * 0.09547485, size.height * 0.6064724);
    path_9.lineTo(size.width * 0.1070276, size.height * 0.6037160);
    path_9.lineTo(size.width * 0.1050301, size.height * 0.5953442);
    path_9.cubicTo(
        size.width * 0.1038110,
        size.height * 0.5902374,
        size.width * 0.1012485,
        size.height * 0.5881503,
        size.width * 0.09734172,
        size.height * 0.5890828);
    path_9.cubicTo(
        size.width * 0.09552822,
        size.height * 0.5895153,
        size.width * 0.09427239,
        size.height * 0.5904344,
        size.width * 0.09357546,
        size.height * 0.5918393);
    path_9.cubicTo(
        size.width * 0.09285092,
        size.height * 0.5932509,
        size.width * 0.09274785,
        size.height * 0.5950454,
        size.width * 0.09326748,
        size.height * 0.5972221);
    path_9.close();
    path_9.moveTo(size.width * 0.1112104, size.height * 0.5932497);
    path_9.lineTo(size.width * 0.1133479, size.height * 0.6022074);
    path_9.lineTo(size.width * 0.1263656, size.height * 0.5991012);
    path_9.lineTo(size.width * 0.1239890, size.height * 0.5891393);
    path_9.cubicTo(
        size.width * 0.1233761,
        size.height * 0.5865718,
        size.width * 0.1223656,
        size.height * 0.5847485,
        size.width * 0.1209577,
        size.height * 0.5836687);
    path_9.cubicTo(
        size.width * 0.1195221,
        size.height * 0.5825951,
        size.width * 0.1177436,
        size.height * 0.5823117,
        size.width * 0.1156227,
        size.height * 0.5828178);
    path_9.cubicTo(
        size.width * 0.1112699,
        size.height * 0.5838564,
        size.width * 0.1097988,
        size.height * 0.5873344,
        size.width * 0.1112104,
        size.height * 0.5932497);
    path_9.close();
    path_9.moveTo(size.width * 0.1350650, size.height * 0.6064491);
    path_9.lineTo(size.width * 0.09103067, size.height * 0.6169571);
    path_9.lineTo(size.width * 0.08625644, size.height * 0.5969485);
    path_9.cubicTo(
        size.width * 0.08523067,
        size.height * 0.5926509,
        size.width * 0.08543313,
        size.height * 0.5890485,
        size.width * 0.08686258,
        size.height * 0.5861417);
    path_9.cubicTo(
        size.width * 0.08825828,
        size.height * 0.5832129,
        size.width * 0.09070000,
        size.height * 0.5813325,
        size.width * 0.09418773,
        size.height * 0.5805000);
    path_9.cubicTo(
        size.width * 0.09647607,
        size.height * 0.5799540,
        size.width * 0.09871411,
        size.height * 0.5801871,
        size.width * 0.1009025,
        size.height * 0.5811988);
    path_9.cubicTo(
        size.width * 0.1030558,
        size.height * 0.5821890,
        size.width * 0.1045810,
        size.height * 0.5836982,
        size.width * 0.1054773,
        size.height * 0.5857258);
    path_9.lineTo(size.width * 0.1058123, size.height * 0.5856454);
    path_9.cubicTo(
        size.width * 0.1055239,
        size.height * 0.5827061,
        size.width * 0.1061080,
        size.height * 0.5801479,
        size.width * 0.1075650,
        size.height * 0.5779718);
    path_9.cubicTo(
        size.width * 0.1089939,
        size.height * 0.5758025,
        size.width * 0.1110896,
        size.height * 0.5743877,
        size.width * 0.1138521,
        size.height * 0.5737288);
    path_9.cubicTo(
        size.width * 0.1177307,
        size.height * 0.5728031,
        size.width * 0.1211472,
        size.height * 0.5734626,
        size.width * 0.1241018,
        size.height * 0.5757067);
    path_9.cubicTo(
        size.width * 0.1270491,
        size.height * 0.5779233,
        size.width * 0.1290920,
        size.height * 0.5814178,
        size.width * 0.1302307,
        size.height * 0.5861896);
    path_9.lineTo(size.width * 0.1350650, size.height * 0.6064491);
    path_9.close();

    Paint paint_9_fill = Paint()..style = PaintingStyle.fill;
    paint_9_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_9, paint_9_fill);

    Path path_10 = Path();
    path_10.moveTo(size.width * 0.1209571, size.height * 0.5252761);
    path_10.lineTo(size.width * 0.07593129, size.height * 0.5299804);
    path_10.lineTo(size.width * 0.07497423, size.height * 0.5208209);
    path_10.lineTo(size.width * 0.1200000, size.height * 0.5161172);
    path_10.lineTo(size.width * 0.1209571, size.height * 0.5252761);
    path_10.close();
    path_10.moveTo(size.width * 0.09948896, size.height * 0.5508834);
    path_10.lineTo(size.width * 0.1003025, size.height * 0.5586730);
    path_10.lineTo(size.width * 0.1169092, size.height * 0.5569380);
    path_10.lineTo(size.width * 0.1160951, size.height * 0.5491485);
    path_10.cubicTo(
        size.width * 0.1158178,
        size.height * 0.5464951,
        size.width * 0.1148411,
        size.height * 0.5444626,
        size.width * 0.1131650,
        size.height * 0.5430509);
    path_10.cubicTo(
        size.width * 0.1114577,
        size.height * 0.5416141,
        size.width * 0.1093626,
        size.height * 0.5410252,
        size.width * 0.1068798,
        size.height * 0.5412847);
    path_10.cubicTo(
        size.width * 0.1043687,
        size.height * 0.5415472,
        size.width * 0.1024411,
        size.height * 0.5425564,
        size.width * 0.1010957,
        size.height * 0.5443123);
    path_10.cubicTo(
        size.width * 0.09975031,
        size.height * 0.5460681,
        size.width * 0.09921472,
        size.height * 0.5482583,
        size.width * 0.09948896,
        size.height * 0.5508834);
    path_10.close();
    path_10.moveTo(size.width * 0.1251466, size.height * 0.5653798);
    path_10.lineTo(size.width * 0.08012086, size.height * 0.5700840);
    path_10.lineTo(size.width * 0.07915951, size.height * 0.5608822);
    path_10.lineTo(size.width * 0.09306933, size.height * 0.5594288);
    path_10.lineTo(size.width * 0.09217055, size.height * 0.5508258);
    path_10.cubicTo(
        size.width * 0.09162515,
        size.height * 0.5456043,
        size.width * 0.09260368,
        size.height * 0.5413049,
        size.width * 0.09510675,
        size.height * 0.5379282);
    path_10.cubicTo(
        size.width * 0.09757791,
        size.height * 0.5345264,
        size.width * 0.1011822,
        size.height * 0.5325773,
        size.width * 0.1059184,
        size.height * 0.5320828);
    path_10.cubicTo(
        size.width * 0.1106834,
        size.height * 0.5315847,
        size.width * 0.1146411,
        size.height * 0.5327436,
        size.width * 0.1177908,
        size.height * 0.5355583);
    path_10.cubicTo(
        size.width * 0.1209092,
        size.height * 0.5383479,
        size.width * 0.1227411,
        size.height * 0.5423534,
        size.width * 0.1232865,
        size.height * 0.5475755);
    path_10.lineTo(size.width * 0.1251466, size.height * 0.5653798);
    path_10.close();

    Paint paint_10_fill = Paint()..style = PaintingStyle.fill;
    paint_10_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_10, paint_10_fill);

    Path path_11 = Path();
    path_11.moveTo(size.width * 0.1210086, size.height * 0.4766638);
    path_11.lineTo(size.width * 0.1013110, size.height * 0.4759791);
    path_11.lineTo(size.width * 0.1006000, size.height * 0.4964509);
    path_11.lineTo(size.width * 0.1202969, size.height * 0.4971350);
    path_11.lineTo(size.width * 0.1199761, size.height * 0.5063816);
    path_11.lineTo(size.width * 0.07473252, size.height * 0.5048092);
    path_11.lineTo(size.width * 0.07505337, size.height * 0.4955632);
    path_11.lineTo(size.width * 0.09328896, size.height * 0.4961963);
    path_11.lineTo(size.width * 0.09400000, size.height * 0.4757252);
    path_11.lineTo(size.width * 0.07576503, size.height * 0.4750914);
    path_11.lineTo(size.width * 0.07608650, size.height * 0.4658448);
    path_11.lineTo(size.width * 0.1213301, size.height * 0.4674172);
    path_11.lineTo(size.width * 0.1210086, size.height * 0.4766638);
    path_11.close();

    Paint paint_11_fill = Paint()..style = PaintingStyle.fill;
    paint_11_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_11, paint_11_fill);

    Path path_12 = Path();
    path_12.moveTo(size.width * 0.1219411, size.height * 0.4253706);
    path_12.cubicTo(
        size.width * 0.1254816,
        size.height * 0.4298681,
        size.width * 0.1267521,
        size.height * 0.4353350,
        size.width * 0.1257534,
        size.height * 0.4417699);
    path_12.cubicTo(
        size.width * 0.1247540,
        size.height * 0.4482055,
        size.width * 0.1218859,
        size.height * 0.4530294,
        size.width * 0.1171485,
        size.height * 0.4562423);
    path_12.cubicTo(
        size.width * 0.1123828,
        size.height * 0.4594509,
        size.width * 0.1064135,
        size.height * 0.4604988,
        size.width * 0.09924110,
        size.height * 0.4593853);
    path_12.cubicTo(
        size.width * 0.09209693,
        size.height * 0.4582761,
        size.width * 0.08675521,
        size.height * 0.4554724,
        size.width * 0.08321472,
        size.height * 0.4509742);
    path_12.cubicTo(
        size.width * 0.07964601,
        size.height * 0.4464718,
        size.width * 0.07836074,
        size.height * 0.4410031,
        size.width * 0.07936012,
        size.height * 0.4345675);
    path_12.cubicTo(
        size.width * 0.08035890,
        size.height * 0.4281325,
        size.width * 0.08324172,
        size.height * 0.4233104,
        size.width * 0.08800736,
        size.height * 0.4201018);
    path_12.cubicTo(
        size.width * 0.09274479,
        size.height * 0.4168890,
        size.width * 0.09868589,
        size.height * 0.4158374,
        size.width * 0.1058294,
        size.height * 0.4169466);
    path_12.cubicTo(
        size.width * 0.1130018,
        size.height * 0.4180601,
        size.width * 0.1183724,
        size.height * 0.4208681,
        size.width * 0.1219411,
        size.height * 0.4253706);
    path_12.close();
    path_12.moveTo(size.width * 0.1181840, size.height * 0.4405951);
    path_12.cubicTo(
        size.width * 0.1187521,
        size.height * 0.4369380,
        size.width * 0.1178258,
        size.height * 0.4338331,
        size.width * 0.1154061,
        size.height * 0.4312798);
    path_12.cubicTo(
        size.width * 0.1129865,
        size.height * 0.4287270,
        size.width * 0.1093098,
        size.height * 0.4270675,
        size.width * 0.1043773,
        size.height * 0.4263018);
    path_12.cubicTo(
        size.width * 0.09947301,
        size.height * 0.4255399,
        size.width * 0.09547853,
        size.height * 0.4260233,
        size.width * 0.09239387,
        size.height * 0.4277509);
    path_12.cubicTo(
        size.width * 0.08931411,
        size.height * 0.4294503,
        size.width * 0.08749264,
        size.height * 0.4321141,
        size.width * 0.08692945,
        size.height * 0.4357429);
    path_12.cubicTo(
        size.width * 0.08636564,
        size.height * 0.4393718,
        size.width * 0.08729202,
        size.height * 0.4424767,
        size.width * 0.08970736,
        size.height * 0.4450583);
    path_12.cubicTo(
        size.width * 0.09212699,
        size.height * 0.4476110,
        size.width * 0.09578896,
        size.height * 0.4492681,
        size.width * 0.1006933,
        size.height * 0.4500301);
    path_12.cubicTo(
        size.width * 0.1056264,
        size.height * 0.4507957,
        size.width * 0.1096325,
        size.height * 0.4503288,
        size.width * 0.1127123,
        size.height * 0.4486294);
    path_12.cubicTo(
        size.width * 0.1157969,
        size.height * 0.4469018,
        size.width * 0.1176209,
        size.height * 0.4442239,
        size.width * 0.1181840,
        size.height * 0.4405951);
    path_12.close();

    Paint paint_12_fill = Paint()..style = PaintingStyle.fill;
    paint_12_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_12, paint_12_fill);

    Path path_13 = Path();
    path_13.moveTo(size.width * 0.1101209, size.height * 0.3702196);
    path_13.lineTo(size.width * 0.1077141, size.height * 0.3788411);
    path_13.cubicTo(
        size.width * 0.1053890,
        size.height * 0.3788472,
        size.width * 0.1033104,
        size.height * 0.3795178,
        size.width * 0.1014779,
        size.height * 0.3808534);
    path_13.cubicTo(
        size.width * 0.09964601,
        size.height * 0.3821883,
        size.width * 0.09835951,
        size.height * 0.3841822,
        size.width * 0.09761902,
        size.height * 0.3868350);
    path_13.cubicTo(
        size.width * 0.09665460,
        size.height * 0.3902890,
        size.width * 0.09726135,
        size.height * 0.3934521,
        size.width * 0.09943804,
        size.height * 0.3963233);
    path_13.cubicTo(
        size.width * 0.1015951,
        size.height * 0.3991595,
        size.width * 0.1049675,
        size.height * 0.4012178,
        size.width * 0.1095540,
        size.height * 0.4024982);
    path_13.cubicTo(
        size.width * 0.1142515,
        size.height * 0.4038092,
        size.width * 0.1182436,
        size.height * 0.4038067,
        size.width * 0.1215301,
        size.height * 0.4024902);
    path_13.cubicTo(
        size.width * 0.1247969,
        size.height * 0.4011387,
        size.width * 0.1269196,
        size.height * 0.3987080,
        size.width * 0.1278994,
        size.height * 0.3951988);
    path_13.cubicTo(
        size.width * 0.1286325,
        size.height * 0.3925736,
        size.width * 0.1286160,
        size.height * 0.3902307,
        size.width * 0.1278509,
        size.height * 0.3881706);
    path_13.cubicTo(
        size.width * 0.1270583,
        size.height * 0.3861025,
        size.width * 0.1256037,
        size.height * 0.3844307,
        size.width * 0.1234877,
        size.height * 0.3831546);
    path_13.lineTo(size.width * 0.1258939, size.height * 0.3745337);
    path_13.cubicTo(
        size.width * 0.1301380,
        size.height * 0.3764031,
        size.width * 0.1331393,
        size.height * 0.3794153,
        size.width * 0.1348988,
        size.height * 0.3835699);
    path_13.cubicTo(
        size.width * 0.1366380,
        size.height * 0.3876896,
        size.width * 0.1367975,
        size.height * 0.3922914,
        size.width * 0.1353785,
        size.height * 0.3973755);
    path_13.cubicTo(
        size.width * 0.1336276,
        size.height * 0.4036485,
        size.width * 0.1302043,
        size.height * 0.4080687,
        size.width * 0.1251086,
        size.height * 0.4106380);
    path_13.cubicTo(
        size.width * 0.1200129,
        size.height * 0.4132067,
        size.width * 0.1139834,
        size.height * 0.4135190,
        size.width * 0.1070202,
        size.height * 0.4115755);
    path_13.cubicTo(
        size.width * 0.1001675,
        size.height * 0.4096626,
        size.width * 0.09521288,
        size.height * 0.4062834,
        size.width * 0.09215706,
        size.height * 0.4014393);
    path_13.cubicTo(
        size.width * 0.08908098,
        size.height * 0.3965595,
        size.width * 0.08840736,
        size.height * 0.3910252,
        size.width * 0.09013497,
        size.height * 0.3848356);
    path_13.cubicTo(
        size.width * 0.09157730,
        size.height * 0.3796681,
        size.width * 0.09417055,
        size.height * 0.3758196,
        size.width * 0.09791472,
        size.height * 0.3732908);
    path_13.cubicTo(
        size.width * 0.1016393,
        size.height * 0.3707264,
        size.width * 0.1057080,
        size.height * 0.3697025,
        size.width * 0.1101209,
        size.height * 0.3702196);
    path_13.close();

    Paint paint_13_fill = Paint()..style = PaintingStyle.fill;
    paint_13_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_13, paint_13_fill);

    Path path_14 = Path();
    path_14.moveTo(size.width * 0.1957460, size.height * 0.2718656);
    path_14.lineTo(size.width * 0.1904528, size.height * 0.2799215);
    path_14.lineTo(size.width * 0.1674718, size.height * 0.2648215);
    path_14.lineTo(size.width * 0.1499613, size.height * 0.2914706);
    path_14.lineTo(size.width * 0.1729423, size.height * 0.3065712);
    path_14.lineTo(size.width * 0.1676491, size.height * 0.3146270);
    path_14.lineTo(size.width * 0.1157528, size.height * 0.2805276);
    path_14.lineTo(size.width * 0.1210460, size.height * 0.2724712);
    path_14.lineTo(size.width * 0.1429840, size.height * 0.2868865);
    path_14.lineTo(size.width * 0.1604945, size.height * 0.2602368);
    path_14.lineTo(size.width * 0.1385564, size.height * 0.2458221);
    path_14.lineTo(size.width * 0.1438497, size.height * 0.2377656);
    path_14.lineTo(size.width * 0.1957460, size.height * 0.2718656);
    path_14.close();

    Paint paint_14_fill = Paint()..style = PaintingStyle.fill;
    paint_14_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_14, paint_14_fill);

    Path path_15 = Path();
    path_15.moveTo(size.width * 0.2063712, size.height * 0.2492988);
    path_15.cubicTo(
        size.width * 0.2085190,
        size.height * 0.2468319,
        size.width * 0.2095718,
        size.height * 0.2441160,
        size.width * 0.2095288,
        size.height * 0.2411497);
    path_15.cubicTo(
        size.width * 0.2094840,
        size.height * 0.2381436,
        size.width * 0.2083577,
        size.height * 0.2356798,
        size.width * 0.2061509,
        size.height * 0.2337583);
    path_15.lineTo(size.width * 0.2034571, size.height * 0.2314129);
    path_15.lineTo(size.width * 0.1966577, size.height * 0.2402712);
    path_15.cubicTo(
        size.width * 0.1949816,
        size.height * 0.2425018,
        size.width * 0.1940883,
        size.height * 0.2445767,
        size.width * 0.1939767,
        size.height * 0.2464957);
    path_15.cubicTo(
        size.width * 0.1938626,
        size.height * 0.2483742,
        size.width * 0.1945417,
        size.height * 0.2499546,
        size.width * 0.1960129,
        size.height * 0.2512356);
    path_15.cubicTo(
        size.width * 0.1975055,
        size.height * 0.2525350,
        size.width * 0.1991963,
        size.height * 0.2530178,
        size.width * 0.2010847,
        size.height * 0.2526840);
    path_15.cubicTo(
        size.width * 0.2029706,
        size.height * 0.2523098,
        size.width * 0.2047325,
        size.height * 0.2511810,
        size.width * 0.2063712,
        size.height * 0.2492988);
    path_15.close();
    path_15.moveTo(size.width * 0.2102258, size.height * 0.2560779);
    path_15.cubicTo(
        size.width * 0.2073061,
        size.height * 0.2594319,
        size.width * 0.2039712,
        size.height * 0.2613399,
        size.width * 0.2002209,
        size.height * 0.2618031);
    path_15.cubicTo(
        size.width * 0.1964896,
        size.height * 0.2622442,
        size.width * 0.1930982,
        size.height * 0.2611362,
        size.width * 0.1900472,
        size.height * 0.2584804);
    path_15.cubicTo(
        size.width * 0.1870613,
        size.height * 0.2558804,
        size.width * 0.1856528,
        size.height * 0.2527141,
        size.width * 0.1858221,
        size.height * 0.2489816);
    path_15.cubicTo(
        size.width * 0.1859693,
        size.height * 0.2452294,
        size.width * 0.1876957,
        size.height * 0.2412368,
        size.width * 0.1910018,
        size.height * 0.2370018);
    path_15.lineTo(size.width * 0.1985883, size.height * 0.2271742);
    path_15.lineTo(size.width * 0.1959595, size.height * 0.2248853);
    path_15.cubicTo(
        size.width * 0.1940552,
        size.height * 0.2232276,
        size.width * 0.1920454,
        size.height * 0.2225429,
        size.width * 0.1899307,
        size.height * 0.2228319);
    path_15.cubicTo(
        size.width * 0.1877939,
        size.height * 0.2231018,
        size.width * 0.1857926,
        size.height * 0.2243074,
        size.width * 0.1839282,
        size.height * 0.2264497);
    path_15.cubicTo(
        size.width * 0.1821951,
        size.height * 0.2284405,
        size.width * 0.1812258,
        size.height * 0.2304491,
        size.width * 0.1810202,
        size.height * 0.2324767);
    path_15.cubicTo(
        size.width * 0.1808337,
        size.height * 0.2344822,
        size.width * 0.1813779,
        size.height * 0.2363258,
        size.width * 0.1826534,
        size.height * 0.2380067);
    path_15.lineTo(size.width * 0.1768890, size.height * 0.2446276);
    path_15.cubicTo(
        size.width * 0.1738994,
        size.height * 0.2416822,
        size.width * 0.1725110,
        size.height * 0.2380779,
        size.width * 0.1727245,
        size.height * 0.2338129);
    path_15.cubicTo(
        size.width * 0.1729380,
        size.height * 0.2295485,
        size.width * 0.1748436,
        size.height * 0.2253497,
        size.width * 0.1784417,
        size.height * 0.2212166);
    path_15.cubicTo(
        size.width * 0.1820399,
        size.height * 0.2170840,
        size.width * 0.1859202,
        size.height * 0.2146804,
        size.width * 0.1900828,
        size.height * 0.2140061);
    path_15.cubicTo(
        size.width * 0.1942638,
        size.height * 0.2133104,
        size.width * 0.1980644,
        size.height * 0.2144509,
        size.width * 0.2014828,
        size.height * 0.2174270);
    path_15.lineTo(size.width * 0.2250135, size.height * 0.2379135);
    path_15.lineTo(size.width * 0.2191644, size.height * 0.2446319);
    path_15.lineTo(size.width * 0.2135497, size.height * 0.2397436);
    path_15.lineTo(size.width * 0.2134080, size.height * 0.2399055);
    path_15.cubicTo(
        size.width * 0.2144429,
        size.height * 0.2425184,
        size.width * 0.2146564,
        size.height * 0.2453098,
        size.width * 0.2140485,
        size.height * 0.2482798);
    path_15.cubicTo(
        size.width * 0.2134595,
        size.height * 0.2512282,
        size.width * 0.2121853,
        size.height * 0.2538276,
        size.width * 0.2102258,
        size.height * 0.2560779);
    path_15.close();

    Paint paint_15_fill = Paint()..style = PaintingStyle.fill;
    paint_15_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_15, paint_15_fill);

    Path path_16 = Path();
    path_16.moveTo(size.width * 0.2168546, size.height * 0.1925258);
    path_16.lineTo(size.width * 0.2098429, size.height * 0.1989509);
    path_16.lineTo(size.width * 0.2178675, size.height * 0.2077074);
    path_16.lineTo(size.width * 0.2242129, size.height * 0.2018926);
    path_16.cubicTo(
        size.width * 0.2280834,
        size.height * 0.1983454,
        size.width * 0.2286620,
        size.height * 0.1950914,
        size.width * 0.2259485,
        size.height * 0.1921307);
    path_16.cubicTo(
        size.width * 0.2246883,
        size.height * 0.1907558,
        size.width * 0.2232755,
        size.height * 0.1901049,
        size.width * 0.2217086,
        size.height * 0.1901791);
    path_16.cubicTo(
        size.width * 0.2201227,
        size.height * 0.1902313,
        size.width * 0.2185043,
        size.height * 0.1910135,
        size.width * 0.2168546,
        size.height * 0.1925258);
    path_16.close();
    path_16.moveTo(size.width * 0.2290472, size.height * 0.2062761);
    path_16.lineTo(size.width * 0.2222577, size.height * 0.2124982);
    path_16.lineTo(size.width * 0.2312994, size.height * 0.2223650);
    path_16.lineTo(size.width * 0.2388503, size.height * 0.2154454);
    path_16.cubicTo(
        size.width * 0.2407963,
        size.height * 0.2136620,
        size.width * 0.2418988,
        size.height * 0.2118933,
        size.width * 0.2421583,
        size.height * 0.2101380);
    path_16.cubicTo(
        size.width * 0.2423982,
        size.height * 0.2083613,
        size.width * 0.2417816,
        size.height * 0.2066693,
        size.width * 0.2403086,
        size.height * 0.2050620);
    path_16.cubicTo(
        size.width * 0.2372847,
        size.height * 0.2017626,
        size.width * 0.2335313,
        size.height * 0.2021669,
        size.width * 0.2290472,
        size.height * 0.2062761);
    path_16.close();
    path_16.moveTo(size.width * 0.2291061, size.height * 0.2335387);
    path_16.lineTo(size.width * 0.1985209, size.height * 0.2001626);
    path_16.lineTo(size.width * 0.2136859, size.height * 0.1862650);
    path_16.cubicTo(
        size.width * 0.2169436,
        size.height * 0.1832804,
        size.width * 0.2201896,
        size.height * 0.1817061,
        size.width * 0.2234258,
        size.height * 0.1815429);
    path_16.cubicTo(
        size.width * 0.2266632,
        size.height * 0.1813387,
        size.width * 0.2294933,
        size.height * 0.1825589,
        size.width * 0.2319160,
        size.height * 0.1852025);
    path_16.cubicTo(
        size.width * 0.2335055,
        size.height * 0.1869368,
        size.width * 0.2343896,
        size.height * 0.1890061,
        size.width * 0.2345687,
        size.height * 0.1914098);
    path_16.cubicTo(
        size.width * 0.2347503,
        size.height * 0.1937736,
        size.width * 0.2341724,
        size.height * 0.1958399,
        size.width * 0.2328368,
        size.height * 0.1976086);
    path_16.lineTo(size.width * 0.2330693, size.height * 0.1978626);
    path_16.cubicTo(
        size.width * 0.2354982,
        size.height * 0.1961816,
        size.width * 0.2380172,
        size.height * 0.1954491,
        size.width * 0.2406270,
        size.height * 0.1956644);
    path_16.cubicTo(
        size.width * 0.2432178,
        size.height * 0.1958589,
        size.width * 0.2454724,
        size.height * 0.1970025,
        size.width * 0.2473914,
        size.height * 0.1990969);
    path_16.cubicTo(
        size.width * 0.2500859,
        size.height * 0.2020368,
        size.width * 0.2511699,
        size.height * 0.2053429,
        size.width * 0.2506442,
        size.height * 0.2090153);
    path_16.cubicTo(
        size.width * 0.2501393,
        size.height * 0.2126687,
        size.width * 0.2480785,
        size.height * 0.2161528,
        size.width * 0.2444620,
        size.height * 0.2194669);
    path_16.lineTo(size.width * 0.2291061, size.height * 0.2335387);
    path_16.close();

    Paint paint_16_fill = Paint()..style = PaintingStyle.fill;
    paint_16_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_16, paint_16_fill);

    Path path_17 = Path();
    path_17.moveTo(size.width * 0.2937270, size.height * 0.1811773);
    path_17.lineTo(size.width * 0.2677699, size.height * 0.1440877);
    path_17.lineTo(size.width * 0.2753147, size.height * 0.1388074);
    path_17.lineTo(size.width * 0.3012718, size.height * 0.1758975);
    path_17.lineTo(size.width * 0.2937270, size.height * 0.1811773);
    path_17.close();
    path_17.moveTo(size.width * 0.2609202, size.height * 0.1748282);
    path_17.lineTo(size.width * 0.2545037, size.height * 0.1793190);
    path_17.lineTo(size.width * 0.2640773, size.height * 0.1929988);
    path_17.lineTo(size.width * 0.2704939, size.height * 0.1885080);
    path_17.cubicTo(
        size.width * 0.2726798,
        size.height * 0.1869779,
        size.width * 0.2739834,
        size.height * 0.1851380,
        size.width * 0.2744043,
        size.height * 0.1829877);
    path_17.cubicTo(
        size.width * 0.2748319,
        size.height * 0.1807969,
        size.width * 0.2743307,
        size.height * 0.1786798,
        size.width * 0.2728994,
        size.height * 0.1766344);
    path_17.cubicTo(
        size.width * 0.2714521,
        size.height * 0.1745663,
        size.width * 0.2696344,
        size.height * 0.1733699,
        size.width * 0.2674460,
        size.height * 0.1730454);
    path_17.cubicTo(
        size.width * 0.2652577,
        size.height * 0.1727209,
        size.width * 0.2630828,
        size.height * 0.1733147,
        size.width * 0.2609202,
        size.height * 0.1748282);
    path_17.close();
    path_17.moveTo(size.width * 0.2606914, size.height * 0.2042969);
    path_17.lineTo(size.width * 0.2347344, size.height * 0.1672074);
    path_17.lineTo(size.width * 0.2423147, size.height * 0.1619025);
    path_17.lineTo(size.width * 0.2503337, size.height * 0.1733607);
    path_17.lineTo(size.width * 0.2574202, size.height * 0.1684012);
    path_17.cubicTo(
        size.width * 0.2617215,
        size.height * 0.1653908,
        size.width * 0.2659558,
        size.height * 0.1641607,
        size.width * 0.2701227,
        size.height * 0.1647117);
    path_17.cubicTo(
        size.width * 0.2742969,
        size.height * 0.1652221,
        size.width * 0.2777491,
        size.height * 0.1674282,
        size.width * 0.2804798,
        size.height * 0.1713294);
    path_17.cubicTo(
        size.width * 0.2832264,
        size.height * 0.1752552,
        size.width * 0.2841331,
        size.height * 0.1792773,
        size.width * 0.2832000,
        size.height * 0.1833975);
    path_17.cubicTo(
        size.width * 0.2822736,
        size.height * 0.1874773,
        size.width * 0.2796595,
        size.height * 0.1910227,
        size.width * 0.2753583,
        size.height * 0.1940325);
    path_17.lineTo(size.width * 0.2606914, size.height * 0.2042969);
    path_17.close();

    Paint paint_17_fill = Paint()..style = PaintingStyle.fill;
    paint_17_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_17, paint_17_fill);

    Path path_18 = Path();
    path_18.moveTo(size.width * 0.3351417, size.height * 0.1580319);
    path_18.lineTo(size.width * 0.3434098, size.height * 0.1537847);
    path_18.lineTo(size.width * 0.3227252, size.height * 0.1135160);
    path_18.lineTo(size.width * 0.3144571, size.height * 0.1177626);
    path_18.lineTo(size.width * 0.3227939, size.height * 0.1339926);
    path_18.lineTo(size.width * 0.3046497, size.height * 0.1433129);
    path_18.lineTo(size.width * 0.2963129, size.height * 0.1270828);
    path_18.lineTo(size.width * 0.2880448, size.height * 0.1313294);
    path_18.lineTo(size.width * 0.3087294, size.height * 0.1715988);
    path_18.lineTo(size.width * 0.3169975, size.height * 0.1673515);
    path_18.lineTo(size.width * 0.3080117, size.height * 0.1498583);
    path_18.lineTo(size.width * 0.3261558, size.height * 0.1405387);
    path_18.lineTo(size.width * 0.3351417, size.height * 0.1580319);
    path_18.close();

    Paint paint_18_fill = Paint()..style = PaintingStyle.fill;
    paint_18_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_18, paint_18_fill);

    Path path_19 = Path();
    path_19.moveTo(size.width * 0.3669374, size.height * 0.1455319);
    path_19.cubicTo(
        size.width * 0.3790816,
        size.height * 0.1410270,
        size.width * 0.3839571,
        size.height * 0.1296718,
        size.width * 0.3788834,
        size.height * 0.1159939);
    path_19.lineTo(size.width * 0.3788540, size.height * 0.1159135);
    path_19.cubicTo(
        size.width * 0.3738098,
        size.height * 0.1023166,
        size.width * 0.3626423,
        size.height * 0.09695828,
        size.width * 0.3505791,
        size.height * 0.1014331);
    path_19.cubicTo(
        size.width * 0.3385153,
        size.height * 0.1059080,
        size.width * 0.3335589,
        size.height * 0.1172933,
        size.width * 0.3385877,
        size.height * 0.1308503);
    path_19.lineTo(size.width * 0.3386178, size.height * 0.1309307);
    path_19.cubicTo(
        size.width * 0.3436761,
        size.height * 0.1445681,
        size.width * 0.3547528,
        size.height * 0.1500521,
        size.width * 0.3669374,
        size.height * 0.1455319);
    path_19.close();
    path_19.moveTo(size.width * 0.3642687, size.height * 0.1382141);
    path_19.cubicTo(
        size.width * 0.3574503,
        size.height * 0.1407436,
        size.width * 0.3510215,
        size.height * 0.1370239,
        size.width * 0.3475344,
        size.height * 0.1276233);
    path_19.lineTo(size.width * 0.3475043, size.height * 0.1275423);
    path_19.cubicTo(
        size.width * 0.3440620,
        size.height * 0.1182626,
        size.width * 0.3465497,
        size.height * 0.1112356,
        size.width * 0.3532877,
        size.height * 0.1087362);
    path_19.cubicTo(
        size.width * 0.3600663,
        size.height * 0.1062215,
        size.width * 0.3665350,
        size.height * 0.1099264,
        size.width * 0.3699773,
        size.height * 0.1192061);
    path_19.lineTo(size.width * 0.3700074, size.height * 0.1192865);
    path_19.cubicTo(
        size.width * 0.3734798,
        size.height * 0.1286472,
        size.width * 0.3710871,
        size.height * 0.1356853,
        size.width * 0.3642687,
        size.height * 0.1382141);
    path_19.close();

    Paint paint_19_fill = Paint()..style = PaintingStyle.fill;
    paint_19_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_19, paint_19_fill);

    Path path_20 = Path();
    path_20.moveTo(size.width * 0.4096908, size.height * 0.1323466);
    path_20.cubicTo(
        size.width * 0.4202712,
        size.height * 0.1297798,
        size.width * 0.4259877,
        size.height * 0.1212190,
        size.width * 0.4248086,
        size.height * 0.1121613);
    path_20.lineTo(size.width * 0.4247791, size.height * 0.1118589);
    path_20.lineTo(size.width * 0.4161227, size.height * 0.1139589);
    path_20.lineTo(size.width * 0.4161202, size.height * 0.1143141);
    path_20.cubicTo(
        size.width * 0.4161896,
        size.height * 0.1193454,
        size.width * 0.4134583,
        size.height * 0.1234178,
        size.width * 0.4078957,
        size.height * 0.1247675);
    path_20.cubicTo(
        size.width * 0.4008706,
        size.height * 0.1264724,
        size.width * 0.3948583,
        size.height * 0.1219528,
        size.width * 0.3925245,
        size.height * 0.1123344);
    path_20.lineTo(size.width * 0.3925141, size.height * 0.1122926);
    path_20.cubicTo(
        size.width * 0.3902313,
        size.height * 0.1028828,
        size.width * 0.3933687,
        size.height * 0.09592209,
        size.width * 0.4003945,
        size.height * 0.09421779);
    path_20.cubicTo(
        size.width * 0.4062914,
        size.height * 0.09278650,
        size.width * 0.4105196,
        size.height * 0.09561350,
        size.width * 0.4125264,
        size.height * 0.09968712);
    path_20.lineTo(size.width * 0.4126914, size.height * 0.1000018);
    path_20.lineTo(size.width * 0.4213061, size.height * 0.09791104);
    path_20.lineTo(size.width * 0.4211933, size.height * 0.09762883);
    path_20.cubicTo(
        size.width * 0.4184313,
        size.height * 0.08953067,
        size.width * 0.4098074,
        size.height * 0.08391840,
        size.width * 0.3985160,
        size.height * 0.08665828);
    path_20.cubicTo(
        size.width * 0.3859282,
        size.height * 0.08971288,
        size.width * 0.3798730,
        size.height * 0.1005258,
        size.width * 0.3832521,
        size.height * 0.1144515);
    path_20.lineTo(size.width * 0.3832620, size.height * 0.1144933);
    path_20.cubicTo(
        size.width * 0.3866816,
        size.height * 0.1285865,
        size.width * 0.3968521,
        size.height * 0.1354620,
        size.width * 0.4096908,
        size.height * 0.1323466);
    path_20.close();

    Paint paint_20_fill = Paint()..style = PaintingStyle.fill;
    paint_20_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_20, paint_20_fill);

    Path path_21 = Path();
    path_21.moveTo(size.width * 0.5000669, size.height * 0.1196239);
    path_21.lineTo(size.width * 0.5096804, size.height * 0.1203301);
    path_21.lineTo(size.width * 0.5116939, size.height * 0.09290552);
    path_21.lineTo(size.width * 0.5434957, size.height * 0.09524049);
    path_21.lineTo(size.width * 0.5414822, size.height * 0.1226650);
    path_21.lineTo(size.width * 0.5510957, size.height * 0.1233712);
    path_21.lineTo(size.width * 0.5556429, size.height * 0.06144110);
    path_21.lineTo(size.width * 0.5460294, size.height * 0.06073497);
    path_21.lineTo(size.width * 0.5441074, size.height * 0.08691472);
    path_21.lineTo(size.width * 0.5123055, size.height * 0.08457975);
    path_21.lineTo(size.width * 0.5142276, size.height * 0.05839994);
    path_21.lineTo(size.width * 0.5046141, size.height * 0.05769405);
    path_21.lineTo(size.width * 0.5000669, size.height * 0.1196239);
    path_21.close();

    Paint paint_21_fill = Paint()..style = PaintingStyle.fill;
    paint_21_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_21, paint_21_fill);

    Path path_22 = Path();
    path_22.moveTo(size.width * 0.5737497, size.height * 0.1286178);
    path_22.cubicTo(
        size.width * 0.5796405,
        size.height * 0.1298816,
        size.width * 0.5853816,
        size.height * 0.1279448,
        size.width * 0.5889681,
        size.height * 0.1237411);
    path_22.lineTo(size.width * 0.5891785, size.height * 0.1237859);
    path_22.lineTo(size.width * 0.5876798, size.height * 0.1307706);
    path_22.lineTo(size.width * 0.5967681, size.height * 0.1327209);
    path_22.lineTo(size.width * 0.6033141, size.height * 0.1022166);
    path_22.cubicTo(
        size.width * 0.6052190,
        size.height * 0.09333865,
        size.width * 0.5995890,
        size.height * 0.08614479,
        size.width * 0.5887755,
        size.height * 0.08382393);
    path_22.cubicTo(
        size.width * 0.5778779,
        size.height * 0.08148589,
        size.width * 0.5700172,
        size.height * 0.08591656,
        size.width * 0.5677515,
        size.height * 0.09339693);
    path_22.lineTo(size.width * 0.5676104, size.height * 0.09385031);
    path_22.lineTo(size.width * 0.5761939, size.height * 0.09569264);
    path_22.lineTo(size.width * 0.5763589, size.height * 0.09533190);
    path_22.cubicTo(
        size.width * 0.5778748,
        size.height * 0.09196012,
        size.width * 0.5816472,
        size.height * 0.09017301,
        size.width * 0.5869067,
        size.height * 0.09130123);
    path_22.cubicTo(
        size.width * 0.5925448,
        size.height * 0.09251104,
        size.width * 0.5951104,
        size.height * 0.09614294,
        size.width * 0.5940632,
        size.height * 0.1010233);
    path_22.lineTo(size.width * 0.5933503, size.height * 0.1043472);
    path_22.lineTo(size.width * 0.5811209, size.height * 0.1024712);
    path_22.cubicTo(
        size.width * 0.5705086,
        size.height * 0.1008546,
        size.width * 0.5635227,
        size.height * 0.1042847,
        size.width * 0.5618706,
        size.height * 0.1119847);
    path_22.lineTo(size.width * 0.5618528, size.height * 0.1120687);
    path_22.cubicTo(
        size.width * 0.5601460,
        size.height * 0.1200209,
        size.width * 0.5649141,
        size.height * 0.1267215,
        size.width * 0.5737497,
        size.height * 0.1286178);
    path_22.close();
    path_22.moveTo(size.width * 0.5710699, size.height * 0.1138264);
    path_22.lineTo(size.width * 0.5710883, size.height * 0.1137423);
    path_22.cubicTo(
        size.width * 0.5718914,
        size.height * 0.1099975,
        size.width * 0.5754356,
        size.height * 0.1082497,
        size.width * 0.5809779,
        size.height * 0.1090865);
    path_22.lineTo(size.width * 0.5919779, size.height * 0.1107429);
    path_22.lineTo(size.width * 0.5912374, size.height * 0.1141933);
    path_22.cubicTo(
        size.width * 0.5900006,
        size.height * 0.1199571,
        size.width * 0.5841896,
        size.height * 0.1232436,
        size.width * 0.5777939,
        size.height * 0.1218712);
    path_22.cubicTo(
        size.width * 0.5729135,
        size.height * 0.1208239,
        size.width * 0.5702393,
        size.height * 0.1176975,
        size.width * 0.5710699,
        size.height * 0.1138264);
    path_22.close();

    Paint paint_22_fill = Paint()..style = PaintingStyle.fill;
    paint_22_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_22, paint_22_fill);

    Path path_23 = Path();
    path_23.moveTo(size.width * 0.6322761, size.height * 0.1035460);
    path_23.lineTo(size.width * 0.6232638, size.height * 0.1005080);
    path_23.lineTo(size.width * 0.6194724, size.height * 0.1117632);
    path_23.lineTo(size.width * 0.6276258, size.height * 0.1145123);
    path_23.cubicTo(
        size.width * 0.6326012,
        size.height * 0.1161896,
        size.width * 0.6357301,
        size.height * 0.1151245,
        size.width * 0.6370123,
        size.height * 0.1113190);
    path_23.cubicTo(
        size.width * 0.6376074,
        size.height * 0.1095515,
        size.width * 0.6374969,
        size.height * 0.1080000,
        size.width * 0.6366748,
        size.height * 0.1066638);
    path_23.cubicTo(
        size.width * 0.6358650,
        size.height * 0.1053000,
        size.width * 0.6343988,
        size.height * 0.1042607,
        size.width * 0.6322761,
        size.height * 0.1035460);
    path_23.close();
    path_23.moveTo(size.width * 0.6261227, size.height * 0.1208626);
    path_23.lineTo(size.width * 0.6173926, size.height * 0.1179209);
    path_23.lineTo(size.width * 0.6131190, size.height * 0.1306031);
    path_23.lineTo(size.width * 0.6228221, size.height * 0.1338742);
    path_23.cubicTo(
        size.width * 0.6253252,
        size.height * 0.1347172,
        size.width * 0.6274049,
        size.height * 0.1348288,
        size.width * 0.6290675,
        size.height * 0.1342086);
    path_23.cubicTo(
        size.width * 0.6307423,
        size.height * 0.1335607,
        size.width * 0.6319264,
        size.height * 0.1322043,
        size.width * 0.6326196,
        size.height * 0.1301380);
    path_23.cubicTo(
        size.width * 0.6340491,
        size.height * 0.1258969,
        size.width * 0.6318834,
        size.height * 0.1228049,
        size.width * 0.6261227,
        size.height * 0.1208626);
    path_23.close();
    path_23.moveTo(size.width * 0.6022755, size.height * 0.1340773);
    path_23.lineTo(size.width * 0.6167362, size.height * 0.09117791);
    path_23.lineTo(size.width * 0.6362270, size.height * 0.09774847);
    path_23.cubicTo(
        size.width * 0.6404172,
        size.height * 0.09916012,
        size.width * 0.6433620,
        size.height * 0.1012429,
        size.width * 0.6450675,
        size.height * 0.1039975);
    path_23.cubicTo(
        size.width * 0.6468098,
        size.height * 0.1067344,
        size.width * 0.6471043,
        size.height * 0.1098018,
        size.width * 0.6459632,
        size.height * 0.1132000);
    path_23.cubicTo(
        size.width * 0.6452086,
        size.height * 0.1154288,
        size.width * 0.6438221,
        size.height * 0.1172025,
        size.width * 0.6418037,
        size.height * 0.1185202);
    path_23.cubicTo(
        size.width * 0.6398221,
        size.height * 0.1198196,
        size.width * 0.6377362,
        size.height * 0.1203117,
        size.width * 0.6355399,
        size.height * 0.1199957);
    path_23.lineTo(size.width * 0.6354294, size.height * 0.1203221);
    path_23.cubicTo(
        size.width * 0.6380736,
        size.height * 0.1216374,
        size.width * 0.6399325,
        size.height * 0.1234902,
        size.width * 0.6410061,
        size.height * 0.1258798);
    path_23.cubicTo(
        size.width * 0.6420859,
        size.height * 0.1282423,
        size.width * 0.6421718,
        size.height * 0.1307687,
        size.width * 0.6412638,
        size.height * 0.1334601);
    path_23.cubicTo(
        size.width * 0.6399939,
        size.height * 0.1372393,
        size.width * 0.6376196,
        size.height * 0.1397853,
        size.width * 0.6341472,
        size.height * 0.1410975);
    path_23.cubicTo(
        size.width * 0.6307055,
        size.height * 0.1424196,
        size.width * 0.6266626,
        size.height * 0.1422975,
        size.width * 0.6220123,
        size.height * 0.1407301);
    path_23.lineTo(size.width * 0.6022755, size.height * 0.1340773);
    path_23.close();

    Paint paint_23_fill = Paint()..style = PaintingStyle.fill;
    paint_23_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_23, paint_23_fill);

    Path path_24 = Path();
    path_24.moveTo(size.width * 0.6446135, size.height * 0.1482601);
    path_24.lineTo(size.width * 0.6606564, size.height * 0.1562031);
    path_24.cubicTo(
        size.width * 0.6700245,
        size.height * 0.1608423,
        size.width * 0.6787730,
        size.height * 0.1584025,
        size.width * 0.6830123,
        size.height * 0.1498405);
    path_24.lineTo(size.width * 0.6830491, size.height * 0.1497632);
    path_24.cubicTo(
        size.width * 0.6872699,
        size.height * 0.1412405,
        size.width * 0.6839080,
        size.height * 0.1328049,
        size.width * 0.6745337,
        size.height * 0.1281656);
    path_24.lineTo(size.width * 0.6668221, size.height * 0.1243472);
    path_24.lineTo(size.width * 0.6730245, size.height * 0.1118129);
    path_24.lineTo(size.width * 0.6646994, size.height * 0.1076890);
    path_24.lineTo(size.width * 0.6446135, size.height * 0.1482601);
    path_24.close();
    path_24.moveTo(size.width * 0.6809018, size.height * 0.1662264);
    path_24.lineTo(size.width * 0.6892699, size.height * 0.1703693);
    path_24.lineTo(size.width * 0.7093558, size.height * 0.1297982);
    path_24.lineTo(size.width * 0.7009877, size.height * 0.1256552);
    path_24.lineTo(size.width * 0.6809018, size.height * 0.1662264);
    path_24.close();
    path_24.moveTo(size.width * 0.6704601, size.height * 0.1343595);
    path_24.cubicTo(
        size.width * 0.6751656,
        size.height * 0.1366890,
        size.width * 0.6767423,
        size.height * 0.1410706,
        size.width * 0.6745276,
        size.height * 0.1455442);
    path_24.lineTo(size.width * 0.6744908, size.height * 0.1456209);
    path_24.cubicTo(
        size.width * 0.6722761,
        size.height * 0.1500945,
        size.width * 0.6678528,
        size.height * 0.1515558,
        size.width * 0.6631104,
        size.height * 0.1492074);
    path_24.lineTo(size.width * 0.6562086, size.height * 0.1457896);
    path_24.lineTo(size.width * 0.6635583, size.height * 0.1309417);
    path_24.lineTo(size.width * 0.6704601, size.height * 0.1343595);
    path_24.close();

    Paint paint_24_fill = Paint()..style = PaintingStyle.fill;
    paint_24_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_24, paint_24_fill);

    Path path_25 = Path();
    path_25.moveTo(size.width * 0.7188589, size.height * 0.1900607);
    path_25.lineTo(size.width * 0.7298589, size.height * 0.1737092);
    path_25.lineTo(size.width * 0.7128650, size.height * 0.1622730);
    path_25.lineTo(size.width * 0.7018650, size.height * 0.1786252);
    path_25.lineTo(size.width * 0.6941840, size.height * 0.1734595);
    path_25.lineTo(size.width * 0.7194601, size.height * 0.1359006);
    path_25.lineTo(size.width * 0.7271350, size.height * 0.1410663);
    path_25.lineTo(size.width * 0.7169509, size.height * 0.1562037);
    path_25.lineTo(size.width * 0.7339448, size.height * 0.1676399);
    path_25.lineTo(size.width * 0.7441288, size.height * 0.1525018);
    path_25.lineTo(size.width * 0.7518098, size.height * 0.1576675);
    path_25.lineTo(size.width * 0.7265337, size.height * 0.1952264);
    path_25.lineTo(size.width * 0.7188589, size.height * 0.1900607);
    path_25.close();

    Paint paint_25_fill = Paint()..style = PaintingStyle.fill;
    paint_25_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_25, paint_25_fill);

    Path path_26 = Path();
    path_26.moveTo(size.width * 0.7465276, size.height * 0.2129528);
    path_26.cubicTo(
        size.width * 0.7563252,
        size.height * 0.2214227,
        size.width * 0.7686258,
        size.height * 0.2202221,
        size.width * 0.7781656,
        size.height * 0.2091847);
    path_26.lineTo(size.width * 0.7782209, size.height * 0.2091196);
    path_26.cubicTo(
        size.width * 0.7877055,
        size.height * 0.1981472,
        size.width * 0.7870184,
        size.height * 0.1857798,
        size.width * 0.7772822,
        size.height * 0.1773663);
    path_26.cubicTo(
        size.width * 0.7675460,
        size.height * 0.1689528,
        size.width * 0.7551840,
        size.height * 0.1700975,
        size.width * 0.7457239,
        size.height * 0.1810368);
    path_26.lineTo(size.width * 0.7456687, size.height * 0.1811018);
    path_26.cubicTo(
        size.width * 0.7361595,
        size.height * 0.1921067,
        size.width * 0.7366933,
        size.height * 0.2044552,
        size.width * 0.7465276,
        size.height * 0.2129528);
    path_26.close();
    path_26.moveTo(size.width * 0.7516503, size.height * 0.2070877);
    path_26.cubicTo(
        size.width * 0.7461472,
        size.height * 0.2023325,
        size.width * 0.7463067,
        size.height * 0.1949067,
        size.width * 0.7528650,
        size.height * 0.1873209);
    path_26.lineTo(size.width * 0.7529202, size.height * 0.1872558);
    path_26.cubicTo(
        size.width * 0.7593926,
        size.height * 0.1797669,
        size.width * 0.7667485,
        size.height * 0.1785601,
        size.width * 0.7721902,
        size.height * 0.1832595);
    path_26.cubicTo(
        size.width * 0.7776564,
        size.height * 0.1879865,
        size.width * 0.7775276,
        size.height * 0.1954405,
        size.width * 0.7710552,
        size.height * 0.2029288);
    path_26.lineTo(size.width * 0.7710000, size.height * 0.2029939);
    path_26.cubicTo(
        size.width * 0.7644724,
        size.height * 0.2105479,
        size.width * 0.7571534,
        size.height * 0.2118436,
        size.width * 0.7516503,
        size.height * 0.2070877);
    path_26.close();

    Paint paint_26_fill = Paint()..style = PaintingStyle.fill;
    paint_26_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_26, paint_26_fill);

    Path path_27 = Path();
    path_27.moveTo(size.width * 0.7791902, size.height * 0.2446221);
    path_27.cubicTo(
        size.width * 0.7865460,
        size.height * 0.2526485,
        size.width * 0.7968037,
        size.height * 0.2535227,
        size.width * 0.8041656,
        size.height * 0.2481178);
    path_27.lineTo(size.width * 0.8044172, size.height * 0.2479460);
    path_27.lineTo(size.width * 0.7983988, size.height * 0.2413785);
    path_27.lineTo(size.width * 0.7980859, size.height * 0.2415479);
    path_27.cubicTo(
        size.width * 0.7937117,
        size.height * 0.2440374,
        size.width * 0.7888282,
        size.height * 0.2436110,
        size.width * 0.7849632,
        size.height * 0.2393914);
    path_27.cubicTo(
        size.width * 0.7800736,
        size.height * 0.2340613,
        size.width * 0.7811350,
        size.height * 0.2266147,
        size.width * 0.7884294,
        size.height * 0.2199276);
    path_27.lineTo(size.width * 0.7884601, size.height * 0.2198988);
    path_27.cubicTo(
        size.width * 0.7956012,
        size.height * 0.2133571,
        size.width * 0.8032086,
        size.height * 0.2127448,
        size.width * 0.8080920,
        size.height * 0.2180748);
    path_27.cubicTo(
        size.width * 0.8121963,
        size.height * 0.2225485,
        size.width * 0.8117607,
        size.height * 0.2276160,
        size.width * 0.8091595,
        size.height * 0.2313405);
    path_27.lineTo(size.width * 0.8089632, size.height * 0.2316362);
    path_27.lineTo(size.width * 0.8149571, size.height * 0.2381718);
    path_27.lineTo(size.width * 0.8151472, size.height * 0.2379368);
    path_27.cubicTo(
        size.width * 0.8209080,
        size.height * 0.2316092,
        size.width * 0.8216564,
        size.height * 0.2213472,
        size.width * 0.8138098,
        size.height * 0.2127810);
    path_27.cubicTo(
        size.width * 0.8050552,
        size.height * 0.2032313,
        size.width * 0.7926626,
        size.height * 0.2031472,
        size.width * 0.7820982,
        size.height * 0.2128288);
    path_27.lineTo(size.width * 0.7820675, size.height * 0.2128583);
    path_27.cubicTo(
        size.width * 0.7713742,
        size.height * 0.2226558,
        size.width * 0.7702638,
        size.height * 0.2348822,
        size.width * 0.7791902,
        size.height * 0.2446221);
    path_27.close();

    Paint paint_27_fill = Paint()..style = PaintingStyle.fill;
    paint_27_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_27, paint_27_fill);

    Path path_28 = Path();
    path_28.moveTo(size.width * 0.8328957, size.height * 0.3160908);
    path_28.lineTo(size.width * 0.8369448, size.height * 0.3248374);
    path_28.lineTo(size.width * 0.8619018, size.height * 0.3132822);
    path_28.lineTo(size.width * 0.8753006, size.height * 0.3422178);
    path_28.lineTo(size.width * 0.8503436, size.height * 0.3537730);
    path_28.lineTo(size.width * 0.8543988, size.height * 0.3625202);
    path_28.lineTo(size.width * 0.9107423, size.height * 0.3364258);
    path_28.lineTo(size.width * 0.9066933, size.height * 0.3276785);
    path_28.lineTo(size.width * 0.8828773, size.height * 0.3387098);
    path_28.lineTo(size.width * 0.8694724, size.height * 0.3097742);
    path_28.lineTo(size.width * 0.8932945, size.height * 0.2987429);
    path_28.lineTo(size.width * 0.8892454, size.height * 0.2899963);
    path_28.lineTo(size.width * 0.8328957, size.height * 0.3160908);
    path_28.close();

    Paint paint_28_fill = Paint()..style = PaintingStyle.fill;
    paint_28_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_28, paint_28_fill);

    Path path_29 = Path();
    path_29.moveTo(size.width * 0.8684233, size.height * 0.3849417);
    path_29.cubicTo(
        size.width * 0.8693742,
        size.height * 0.3880712,
        size.width * 0.8711166,
        size.height * 0.3904049,
        size.width * 0.8736503,
        size.height * 0.3919429);
    path_29.cubicTo(
        size.width * 0.8762209,
        size.height * 0.3935006,
        size.width * 0.8789080,
        size.height * 0.3938540,
        size.width * 0.8817117,
        size.height * 0.3930031);
    path_29.lineTo(size.width * 0.8851288, size.height * 0.3919650);
    path_29.lineTo(size.width * 0.8812270, size.height * 0.3815006);
    path_29.cubicTo(
        size.width * 0.8802270,
        size.height * 0.3788963,
        size.width * 0.8789448,
        size.height * 0.3770380,
        size.width * 0.8773742,
        size.height * 0.3759252);
    path_29.cubicTo(
        size.width * 0.8758466,
        size.height * 0.3748313,
        size.width * 0.8741472,
        size.height * 0.3745681,
        size.width * 0.8722761,
        size.height * 0.3751350);
    path_29.cubicTo(
        size.width * 0.8703865,
        size.height * 0.3757104,
        size.width * 0.8690798,
        size.height * 0.3768871,
        size.width * 0.8683558,
        size.height * 0.3786644);
    path_29.cubicTo(
        size.width * 0.8676748,
        size.height * 0.3804613,
        size.width * 0.8676933,
        size.height * 0.3825540,
        size.width * 0.8684233,
        size.height * 0.3849417);
    path_29.close();
    path_29.moveTo(size.width * 0.8606319, size.height * 0.3846104);
    path_29.cubicTo(
        size.width * 0.8593374,
        size.height * 0.3803558,
        size.width * 0.8594908,
        size.height * 0.3765160,
        size.width * 0.8610859,
        size.height * 0.3730920);
    path_29.cubicTo(
        size.width * 0.8626933,
        size.height * 0.3696957,
        size.width * 0.8654356,
        size.height * 0.3674098,
        size.width * 0.8693067,
        size.height * 0.3662337);
    path_29.cubicTo(
        size.width * 0.8730920,
        size.height * 0.3650828,
        size.width * 0.8765215,
        size.height * 0.3655699,
        size.width * 0.8795951,
        size.height * 0.3676945);
    path_29.cubicTo(
        size.width * 0.8826994,
        size.height * 0.3698104,
        size.width * 0.8851656,
        size.height * 0.3733933,
        size.width * 0.8870000,
        size.height * 0.3784429);
    path_29.lineTo(size.width * 0.8913006, size.height * 0.3900890);
    path_29.lineTo(size.width * 0.8946380, size.height * 0.3890755);
    path_29.cubicTo(
        size.width * 0.8970552,
        size.height * 0.3883417,
        size.width * 0.8986994,
        size.height * 0.3870018,
        size.width * 0.8995767,
        size.height * 0.3850558);
    path_29.cubicTo(
        size.width * 0.9004847,
        size.height * 0.3831018,
        size.width * 0.9005215,
        size.height * 0.3807663,
        size.width * 0.8996994,
        size.height * 0.3780485);
    path_29.cubicTo(
        size.width * 0.8989325,
        size.height * 0.3755233,
        size.width * 0.8977423,
        size.height * 0.3736356,
        size.width * 0.8961350,
        size.height * 0.3723853);
    path_29.cubicTo(
        size.width * 0.8945337,
        size.height * 0.3711626,
        size.width * 0.8926810,
        size.height * 0.3706454,
        size.width * 0.8905828,
        size.height * 0.3708337);
    path_29.lineTo(size.width * 0.8880307, size.height * 0.3624344);
    path_29.cubicTo(
        size.width * 0.8921104,
        size.height * 0.3614638,
        size.width * 0.8959018,
        size.height * 0.3622006,
        size.width * 0.8994049,
        size.height * 0.3646454);
    path_29.cubicTo(
        size.width * 0.9029080,
        size.height * 0.3670896,
        size.width * 0.9054540,
        size.height * 0.3709331,
        size.width * 0.9070429,
        size.height * 0.3761761);
    path_29.cubicTo(
        size.width * 0.9086380,
        size.height * 0.3814196,
        size.width * 0.9086135,
        size.height * 0.3859834,
        size.width * 0.9069755,
        size.height * 0.3898693);
    path_29.cubicTo(
        size.width * 0.9053497,
        size.height * 0.3937828,
        size.width * 0.9023620,
        size.height * 0.3963982,
        size.width * 0.8980245,
        size.height * 0.3977153);
    path_29.lineTo(size.width * 0.8681779, size.height * 0.4067847);
    path_29.lineTo(size.width * 0.8655890, size.height * 0.3982613);
    path_29.lineTo(size.width * 0.8727117, size.height * 0.3960969);
    path_29.lineTo(size.width * 0.8726442, size.height * 0.3958914);
    path_29.cubicTo(
        size.width * 0.8698834,
        size.height * 0.3953816,
        size.width * 0.8674049,
        size.height * 0.3940810,
        size.width * 0.8652086,
        size.height * 0.3919890);
    path_29.cubicTo(
        size.width * 0.8630245,
        size.height * 0.3899245,
        size.width * 0.8614969,
        size.height * 0.3874650,
        size.width * 0.8606319,
        size.height * 0.3846104);
    path_29.close();

    Paint paint_29_fill = Paint()..style = PaintingStyle.fill;
    paint_29_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_29, paint_29_fill);

    Path path_30 = Path();
    path_30.moveTo(size.width * 0.9109571, size.height * 0.4243595);
    path_30.lineTo(size.width * 0.9092454, size.height * 0.4150043);
    path_30.lineTo(size.width * 0.8975583, size.height * 0.4171405);
    path_30.lineTo(size.width * 0.8991104, size.height * 0.4256067);
    path_30.cubicTo(
        size.width * 0.9000552,
        size.height * 0.4307712,
        size.width * 0.9025031,
        size.height * 0.4329920,
        size.width * 0.9064540,
        size.height * 0.4322699);
    path_30.cubicTo(
        size.width * 0.9082883,
        size.height * 0.4319344,
        size.width * 0.9095890,
        size.height * 0.4310834,
        size.width * 0.9103620,
        size.height * 0.4297178);
    path_30.cubicTo(
        size.width * 0.9111595,
        size.height * 0.4283466,
        size.width * 0.9113558,
        size.height * 0.4265607,
        size.width * 0.9109571,
        size.height * 0.4243595);
    path_30.close();
    path_30.moveTo(size.width * 0.8928282, size.height * 0.4273675);
    path_30.lineTo(size.width * 0.8911718, size.height * 0.4183086);
    path_30.lineTo(size.width * 0.8780061, size.height * 0.4207160);
    path_30.lineTo(size.width * 0.8798466, size.height * 0.4307908);
    path_30.cubicTo(
        size.width * 0.8803190,
        size.height * 0.4333871,
        size.width * 0.8812331,
        size.height * 0.4352620,
        size.width * 0.8825828,
        size.height * 0.4364153);
    path_30.cubicTo(
        size.width * 0.8839571,
        size.height * 0.4375632,
        size.width * 0.8857178,
        size.height * 0.4379417,
        size.width * 0.8878650,
        size.height * 0.4375491);
    path_30.cubicTo(
        size.width * 0.8922638,
        size.height * 0.4367442,
        size.width * 0.8939202,
        size.height * 0.4333503,
        size.width * 0.8928282,
        size.height * 0.4273675);
    path_30.close();
    path_30.moveTo(size.width * 0.8697117, size.height * 0.4129141);
    path_30.lineTo(size.width * 0.9142393, size.height * 0.4047724);
    path_30.lineTo(size.width * 0.9179387, size.height * 0.4250067);
    path_30.cubicTo(
        size.width * 0.9187362,
        size.height * 0.4293528,
        size.width * 0.9183436,
        size.height * 0.4329393,
        size.width * 0.9167607,
        size.height * 0.4357663);
    path_30.cubicTo(
        size.width * 0.9152086,
        size.height * 0.4386160,
        size.width * 0.9126687,
        size.height * 0.4403632,
        size.width * 0.9091411,
        size.height * 0.4410080);
    path_30.cubicTo(
        size.width * 0.9068282,
        size.height * 0.4414313,
        size.width * 0.9046074,
        size.height * 0.4410791,
        size.width * 0.9024724,
        size.height * 0.4399521);
    path_30.cubicTo(
        size.width * 0.9003804,
        size.height * 0.4388485,
        size.width * 0.8989325,
        size.height * 0.4372601,
        size.width * 0.8981472,
        size.height * 0.4351877);
    path_30.lineTo(size.width * 0.8978098, size.height * 0.4352497);
    path_30.cubicTo(
        size.width * 0.8979387,
        size.height * 0.4382000,
        size.width * 0.8972209,
        size.height * 0.4407233,
        size.width * 0.8956503,
        size.height * 0.4428190);
    path_30.cubicTo(
        size.width * 0.8941104,
        size.height * 0.4449092,
        size.width * 0.8919387,
        size.height * 0.4462092,
        size.width * 0.8891472,
        size.height * 0.4467202);
    path_30.cubicTo(
        size.width * 0.8852209,
        size.height * 0.4474374,
        size.width * 0.8818466,
        size.height * 0.4465963,
        size.width * 0.8790184,
        size.height * 0.4441975);
    path_30.cubicTo(
        size.width * 0.8761902,
        size.height * 0.4418264,
        size.width * 0.8743374,
        size.height * 0.4382282,
        size.width * 0.8734540,
        size.height * 0.4334025);
    path_30.lineTo(size.width * 0.8697117, size.height * 0.4129141);
    path_30.close();

    Paint paint_30_fill = Paint()..style = PaintingStyle.fill;
    paint_30_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_30, paint_30_fill);

    Path path_31 = Path();
    path_31.moveTo(size.width * 0.8812761, size.height * 0.4974166);
    path_31.lineTo(size.width * 0.9264847, size.height * 0.4951466);
    path_31.lineTo(size.width * 0.9269509, size.height * 0.5043442);
    path_31.lineTo(size.width * 0.8817362, size.height * 0.5066141);
    path_31.lineTo(size.width * 0.8812761, size.height * 0.4974166);
    path_31.close();
    path_31.moveTo(size.width * 0.9040920, size.height * 0.4730037);
    path_31.lineTo(size.width * 0.9036994, size.height * 0.4651816);
    path_31.lineTo(size.width * 0.8870245, size.height * 0.4660190);
    path_31.lineTo(size.width * 0.8874172, size.height * 0.4738411);
    path_31.cubicTo(
        size.width * 0.8875460,
        size.height * 0.4765055,
        size.width * 0.8884172,
        size.height * 0.4785877,
        size.width * 0.8900123,
        size.height * 0.4800877);
    path_31.cubicTo(
        size.width * 0.8916380,
        size.height * 0.4816141,
        size.width * 0.8936994,
        size.height * 0.4823153,
        size.width * 0.8961902,
        size.height * 0.4821902);
    path_31.cubicTo(
        size.width * 0.8987117,
        size.height * 0.4820632,
        size.width * 0.9006933,
        size.height * 0.4811595,
        size.width * 0.9021350,
        size.height * 0.4794791);
    path_31.cubicTo(
        size.width * 0.9035706,
        size.height * 0.4777982,
        size.width * 0.9042209,
        size.height * 0.4756399,
        size.width * 0.9040920,
        size.height * 0.4730037);
    path_31.close();
    path_31.moveTo(size.width * 0.8792515, size.height * 0.4571454);
    path_31.lineTo(size.width * 0.9244663, size.height * 0.4548748);
    path_31.lineTo(size.width * 0.9249325, size.height * 0.4641153);
    path_31.lineTo(size.width * 0.9109632, size.height * 0.4648172);
    path_31.lineTo(size.width * 0.9113926, size.height * 0.4734558);
    path_31.cubicTo(
        size.width * 0.9116564,
        size.height * 0.4786994,
        size.width * 0.9104479,
        size.height * 0.4829393,
        size.width * 0.9077669,
        size.height * 0.4861761);
    path_31.cubicTo(
        size.width * 0.9051166,
        size.height * 0.4894405,
        size.width * 0.9014110,
        size.height * 0.4911920,
        size.width * 0.8966564,
        size.height * 0.4914307);
    path_31.cubicTo(
        size.width * 0.8918712,
        size.height * 0.4916706,
        size.width * 0.8879816,
        size.height * 0.4903006,
        size.width * 0.8849877,
        size.height * 0.4873196);
    path_31.cubicTo(
        size.width * 0.8820245,
        size.height * 0.4843663,
        size.width * 0.8804110,
        size.height * 0.4802681,
        size.width * 0.8801472,
        size.height * 0.4750245);
    path_31.lineTo(size.width * 0.8792515, size.height * 0.4571454);
    path_31.close();

    Paint paint_31_fill = Paint()..style = PaintingStyle.fill;
    paint_31_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_31, paint_31_fill);

    Path path_32 = Path();
    path_32.moveTo(size.width * 0.8768834, size.height * 0.5441233);
    path_32.lineTo(size.width * 0.8965153, size.height * 0.5458693);
    path_32.lineTo(size.width * 0.8983313, size.height * 0.5254663);
    path_32.lineTo(size.width * 0.8786994, size.height * 0.5237202);
    path_32.lineTo(size.width * 0.8795215, size.height * 0.5145043);
    path_32.lineTo(size.width * 0.9246135, size.height * 0.5185147);
    path_32.lineTo(size.width * 0.9237914, size.height * 0.5277307);
    path_32.lineTo(size.width * 0.9056196, size.height * 0.5261141);
    path_32.lineTo(size.width * 0.9038037, size.height * 0.5465178);
    path_32.lineTo(size.width * 0.9219816, size.height * 0.5481337);
    path_32.lineTo(size.width * 0.9211595, size.height * 0.5573497);
    path_32.lineTo(size.width * 0.8760675, size.height * 0.5533393);
    path_32.lineTo(size.width * 0.8768834, size.height * 0.5441233);
    path_32.close();

    Paint paint_32_fill = Paint()..style = PaintingStyle.fill;
    paint_32_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_32, paint_32_fill);

    Path path_33 = Path();
    path_33.moveTo(size.width * 0.8732393, size.height * 0.5949828);
    path_33.cubicTo(
        size.width * 0.8699448,
        size.height * 0.5903018,
        size.width * 0.8689693,
        size.height * 0.5847748,
        size.width * 0.8703129,
        size.height * 0.5784025);
    path_33.cubicTo(
        size.width * 0.8716564,
        size.height * 0.5720294,
        size.width * 0.8747730,
        size.height * 0.5673663,
        size.width * 0.8796810,
        size.height * 0.5644110);
    path_33.cubicTo(
        size.width * 0.8846074,
        size.height * 0.5614626,
        size.width * 0.8906258,
        size.height * 0.5607362,
        size.width * 0.8977301,
        size.height * 0.5622319);
    path_33.cubicTo(
        size.width * 0.9048037,
        size.height * 0.5637215,
        size.width * 0.9099877,
        size.height * 0.5668074,
        size.width * 0.9132822,
        size.height * 0.5714883);
    path_33.cubicTo(
        size.width * 0.9166074,
        size.height * 0.5761755,
        size.width * 0.9175951,
        size.height * 0.5817049,
        size.width * 0.9162515,
        size.height * 0.5880773);
    path_33.cubicTo(
        size.width * 0.9149080,
        size.height * 0.5944503,
        size.width * 0.9117730,
        size.height * 0.5991110,
        size.width * 0.9068405,
        size.height * 0.6020595);
    path_33.cubicTo(
        size.width * 0.9019387,
        size.height * 0.6050141,
        size.width * 0.8959509,
        size.height * 0.6057466,
        size.width * 0.8888773,
        size.height * 0.6042571);
    path_33.cubicTo(
        size.width * 0.8817730,
        size.height * 0.6027613,
        size.width * 0.8765644,
        size.height * 0.5996699,
        size.width * 0.8732393,
        size.height * 0.5949828);
    path_33.close();
    path_33.moveTo(size.width * 0.8778098, size.height * 0.5799810);
    path_33.cubicTo(
        size.width * 0.8770429,
        size.height * 0.5836025,
        size.width * 0.8778037,
        size.height * 0.5867521,
        size.width * 0.8800798,
        size.height * 0.5894313);
    path_33.cubicTo(
        size.width * 0.8823620,
        size.height * 0.5921104,
        size.width * 0.8859448,
        size.height * 0.5939644,
        size.width * 0.8908282,
        size.height * 0.5949926);
    path_33.cubicTo(
        size.width * 0.8956871,
        size.height * 0.5960160,
        size.width * 0.8996994,
        size.height * 0.5957472,
        size.width * 0.9028712,
        size.height * 0.5941871);
    path_33.cubicTo(
        size.width * 0.9060368,
        size.height * 0.5926552,
        size.width * 0.9080000,
        size.height * 0.5900920,
        size.width * 0.9087546,
        size.height * 0.5864988);
    path_33.cubicTo(
        size.width * 0.9095153,
        size.height * 0.5829055,
        size.width * 0.9087546,
        size.height * 0.5797552,
        size.width * 0.9064847,
        size.height * 0.5770485);
    path_33.cubicTo(
        size.width * 0.9042025,
        size.height * 0.5743693,
        size.width * 0.9006319,
        size.height * 0.5725184,
        size.width * 0.8957791,
        size.height * 0.5714957);
    path_33.cubicTo(
        size.width * 0.8908896,
        size.height * 0.5704669,
        size.width * 0.8868650,
        size.height * 0.5707184,
        size.width * 0.8836994,
        size.height * 0.5722509);
    path_33.cubicTo(
        size.width * 0.8805276,
        size.height * 0.5738110,
        size.width * 0.8785644,
        size.height * 0.5763877,
        size.width * 0.8778098,
        size.height * 0.5799810);
    path_33.close();

    Paint paint_33_fill = Paint()..style = PaintingStyle.fill;
    paint_33_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_33, paint_33_fill);

    Path path_34 = Path();
    path_34.moveTo(size.width * 0.8821534, size.height * 0.6504663);
    path_34.lineTo(size.width * 0.8850123, size.height * 0.6419877);
    path_34.cubicTo(
        size.width * 0.8873313,
        size.height * 0.6421043,
        size.width * 0.8894417,
        size.height * 0.6415399,
        size.width * 0.8913436,
        size.height * 0.6403067);
    path_34.cubicTo(
        size.width * 0.8932454,
        size.height * 0.6390675,
        size.width * 0.8946319,
        size.height * 0.6371472,
        size.width * 0.8955153,
        size.height * 0.6345337);
    path_34.cubicTo(
        size.width * 0.8966564,
        size.height * 0.6311411,
        size.width * 0.8962209,
        size.height * 0.6279509,
        size.width * 0.8941963,
        size.height * 0.6249632);
    path_34.cubicTo(
        size.width * 0.8921963,
        size.height * 0.6220184,
        size.width * 0.8889387,
        size.height * 0.6197853,
        size.width * 0.8844233,
        size.height * 0.6182638);
    path_34.cubicTo(
        size.width * 0.8798037,
        size.height * 0.6167055,
        size.width * 0.8758160,
        size.height * 0.6164969,
        size.width * 0.8724663,
        size.height * 0.6176380);
    path_34.cubicTo(
        size.width * 0.8691288,
        size.height * 0.6188160,
        size.width * 0.8668834,
        size.height * 0.6211288,
        size.width * 0.8657178,
        size.height * 0.6245828);
    path_34.cubicTo(
        size.width * 0.8648466,
        size.height * 0.6271656,
        size.width * 0.8647423,
        size.height * 0.6295092,
        size.width * 0.8653926,
        size.height * 0.6316074);
    path_34.cubicTo(
        size.width * 0.8660798,
        size.height * 0.6337117,
        size.width * 0.8674417,
        size.height * 0.6354601,
        size.width * 0.8694847,
        size.height * 0.6368466);
    path_34.lineTo(size.width * 0.8666258, size.height * 0.6453252);
    path_34.cubicTo(
        size.width * 0.8624908,
        size.height * 0.6432331,
        size.width * 0.8596503,
        size.height * 0.6400675,
        size.width * 0.8581104,
        size.height * 0.6358282);
    path_34.cubicTo(
        size.width * 0.8565951,
        size.height * 0.6316196,
        size.width * 0.8566810,
        size.height * 0.6270184,
        size.width * 0.8583620,
        size.height * 0.6220123);
    path_34.cubicTo(
        size.width * 0.8604417,
        size.height * 0.6158405,
        size.width * 0.8640982,
        size.height * 0.6116092,
        size.width * 0.8693190,
        size.height * 0.6093135);
    path_34.cubicTo(
        size.width * 0.8745460,
        size.height * 0.6070178,
        size.width * 0.8805828,
        size.height * 0.6070245,
        size.width * 0.8874356,
        size.height * 0.6093337);
    path_34.cubicTo(
        size.width * 0.8941779,
        size.height * 0.6116067,
        size.width * 0.8989448,
        size.height * 0.6152454,
        size.width * 0.9017423,
        size.height * 0.6202393);
    path_34.cubicTo(
        size.width * 0.9045521,
        size.height * 0.6252761,
        size.width * 0.9049325,
        size.height * 0.6308405,
        size.width * 0.9028834,
        size.height * 0.6369325);
    path_34.cubicTo(
        size.width * 0.9011656,
        size.height * 0.6420123,
        size.width * 0.8983742,
        size.height * 0.6457178,
        size.width * 0.8945031,
        size.height * 0.6480491);
    path_34.cubicTo(
        size.width * 0.8906442,
        size.height * 0.6504110,
        size.width * 0.8865276,
        size.height * 0.6512147,
        size.width * 0.8821534,
        size.height * 0.6504663);
    path_34.close();

    Paint paint_34_fill = Paint()..style = PaintingStyle.fill;
    paint_34_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_34, paint_34_fill);

    Path path_35 = Path();
    path_35.moveTo(size.width * 0.7907485, size.height * 0.7449141);
    path_35.lineTo(size.width * 0.7964785, size.height * 0.7371656);
    path_35.lineTo(size.width * 0.8185951, size.height * 0.7535031);
    path_35.lineTo(size.width * 0.8375399, size.height * 0.7278528);
    path_35.lineTo(size.width * 0.8154233, size.height * 0.7115153);
    path_35.lineTo(size.width * 0.8211472, size.height * 0.7037607);
    path_35.lineTo(size.width * 0.8710982, size.height * 0.7406564);
    path_35.lineTo(size.width * 0.8653681, size.height * 0.7484110);
    path_35.lineTo(size.width * 0.8442577, size.height * 0.7328098);
    path_35.lineTo(size.width * 0.8253067, size.height * 0.7584601);
    path_35.lineTo(size.width * 0.8464233, size.height * 0.7740552);
    path_35.lineTo(size.width * 0.8406994, size.height * 0.7818098);
    path_35.lineTo(size.width * 0.7907485, size.height * 0.7449141);
    path_35.close();

    Paint paint_35_fill = Paint()..style = PaintingStyle.fill;
    paint_35_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_35, paint_35_fill);

    Path path_36 = Path();
    path_36.moveTo(size.width * 0.7782025, size.height * 0.7675215);
    path_36.cubicTo(
        size.width * 0.7759264,
        size.height * 0.7698712,
        size.width * 0.7747301,
        size.height * 0.7725215,
        size.width * 0.7746074,
        size.height * 0.7754908);
    path_36.cubicTo(
        size.width * 0.7744908,
        size.height * 0.7784908,
        size.width * 0.7754785,
        size.height * 0.7810123,
        size.width * 0.7775767,
        size.height * 0.7830552);
    path_36.lineTo(size.width * 0.7801411, size.height * 0.7855399);
    path_36.lineTo(size.width * 0.7874110, size.height * 0.7770675);
    path_36.cubicTo(
        size.width * 0.7892086,
        size.height * 0.7749325,
        size.width * 0.7902147,
        size.height * 0.7729080,
        size.width * 0.7904294,
        size.height * 0.7710000);
    path_36.cubicTo(
        size.width * 0.7906442,
        size.height * 0.7691288,
        size.width * 0.7900552,
        size.height * 0.7675153,
        size.width * 0.7886564,
        size.height * 0.7661534);
    path_36.cubicTo(
        size.width * 0.7872331,
        size.height * 0.7647730,
        size.width * 0.7855706,
        size.height * 0.7642025,
        size.width * 0.7836687,
        size.height * 0.7644294);
    path_36.cubicTo(
        size.width * 0.7817669,
        size.height * 0.7646994,
        size.width * 0.7799448,
        size.height * 0.7657301,
        size.width * 0.7782025,
        size.height * 0.7675215);
    path_36.close();
    path_36.moveTo(size.width * 0.7747239, size.height * 0.7605460);
    path_36.cubicTo(
        size.width * 0.7778221,
        size.height * 0.7573558,
        size.width * 0.7812577,
        size.height * 0.7556319,
        size.width * 0.7850307,
        size.height * 0.7553742);
    path_36.cubicTo(
        size.width * 0.7887791,
        size.height * 0.7551350,
        size.width * 0.7921043,
        size.height * 0.7564294,
        size.width * 0.7950061,
        size.height * 0.7592454);
    path_36.cubicTo(
        size.width * 0.7978466,
        size.height * 0.7620061,
        size.width * 0.7990798,
        size.height * 0.7652393,
        size.width * 0.7987055,
        size.height * 0.7689571);
    path_36.cubicTo(
        size.width * 0.7983558,
        size.height * 0.7726994,
        size.width * 0.7964110,
        size.height * 0.7765890,
        size.width * 0.7928834,
        size.height * 0.7806380);
    path_36.lineTo(size.width * 0.7847730, size.height * 0.7900368);
    path_36.lineTo(size.width * 0.7872699, size.height * 0.7924663);
    path_36.cubicTo(
        size.width * 0.7890798,
        size.height * 0.7942270,
        size.width * 0.7910491,
        size.height * 0.7950184,
        size.width * 0.7931779,
        size.height * 0.7948466);
    path_36.cubicTo(
        size.width * 0.7953252,
        size.height * 0.7946933,
        size.width * 0.7973926,
        size.height * 0.7936012,
        size.width * 0.7993681,
        size.height * 0.7915644);
    path_36.cubicTo(
        size.width * 0.8012086,
        size.height * 0.7896687,
        size.width * 0.8022883,
        size.height * 0.7877178,
        size.width * 0.8026012,
        size.height * 0.7856994);
    path_36.cubicTo(
        size.width * 0.8028957,
        size.height * 0.7837117,
        size.width * 0.8024540,
        size.height * 0.7818405,
        size.width * 0.8012699,
        size.height * 0.7800920);
    path_36.lineTo(size.width * 0.8073865, size.height * 0.7737914);
    path_36.cubicTo(
        size.width * 0.8102147,
        size.height * 0.7768957,
        size.width * 0.8114049,
        size.height * 0.7805706,
        size.width * 0.8109571,
        size.height * 0.7848221);
    path_36.cubicTo(
        size.width * 0.8105153,
        size.height * 0.7890675,
        size.width * 0.8083804,
        size.height * 0.7931534,
        size.width * 0.8045644,
        size.height * 0.7970859);
    path_36.cubicTo(
        size.width * 0.8007423,
        size.height * 0.8010184,
        size.width * 0.7967423,
        size.height * 0.8032025,
        size.width * 0.7925460,
        size.height * 0.8036503);
    path_36.cubicTo(
        size.width * 0.7883313,
        size.height * 0.8041166,
        size.width * 0.7846012,
        size.height * 0.8027730,
        size.width * 0.7813497,
        size.height * 0.7996135);
    path_36.lineTo(size.width * 0.7589693, size.height * 0.7778773);
    path_36.lineTo(size.width * 0.7651779, size.height * 0.7714847);
    path_36.lineTo(size.width * 0.7705153, size.height * 0.7766748);
    path_36.lineTo(size.width * 0.7706687, size.height * 0.7765215);
    path_36.cubicTo(
        size.width * 0.7697730,
        size.height * 0.7738528,
        size.width * 0.7697178,
        size.height * 0.7710552,
        size.width * 0.7704847,
        size.height * 0.7681227);
    path_36.cubicTo(
        size.width * 0.7712331,
        size.height * 0.7652086,
        size.width * 0.7726442,
        size.height * 0.7626871,
        size.width * 0.7747239,
        size.height * 0.7605460);
    path_36.close();

    Paint paint_36_fill = Paint()..style = PaintingStyle.fill;
    paint_36_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_36, paint_36_fill);

    Path path_37 = Path();
    path_37.moveTo(size.width * 0.7639755, size.height * 0.8240000);
    path_37.lineTo(size.width * 0.7713252, size.height * 0.8179693);
    path_37.lineTo(size.width * 0.7637914, size.height * 0.8087853);
    path_37.lineTo(size.width * 0.7571350, size.height * 0.8142454);
    path_37.cubicTo(
        size.width * 0.7530798,
        size.height * 0.8175767,
        size.width * 0.7523252,
        size.height * 0.8207975,
        size.width * 0.7548712,
        size.height * 0.8239018);
    path_37.cubicTo(
        size.width * 0.7560552,
        size.height * 0.8253436,
        size.width * 0.7574294,
        size.height * 0.8260675,
        size.width * 0.7590000,
        size.height * 0.8260798);
    path_37.cubicTo(
        size.width * 0.7605828,
        size.height * 0.8261166,
        size.width * 0.7622454,
        size.height * 0.8254233,
        size.width * 0.7639755,
        size.height * 0.8240000);
    path_37.close();
    path_37.moveTo(size.width * 0.7525521, size.height * 0.8096074);
    path_37.lineTo(size.width * 0.7596687, size.height * 0.8037669);
    path_37.lineTo(size.width * 0.7511779, size.height * 0.7934172);
    path_37.lineTo(size.width * 0.7432577, size.height * 0.7999141);
    path_37.cubicTo(
        size.width * 0.7412209,
        size.height * 0.8015890,
        size.width * 0.7400245,
        size.height * 0.8032945,
        size.width * 0.7396687,
        size.height * 0.8050368);
    path_37.cubicTo(
        size.width * 0.7393313,
        size.height * 0.8067975,
        size.width * 0.7398528,
        size.height * 0.8085215,
        size.width * 0.7412393,
        size.height * 0.8102025);
    path_37.cubicTo(
        size.width * 0.7440798,
        size.height * 0.8136626,
        size.width * 0.7478466,
        size.height * 0.8134663,
        size.width * 0.7525521,
        size.height * 0.8096074);
    path_37.close();
    path_37.moveTo(size.width * 0.7539755, size.height * 0.7823804);
    path_37.lineTo(size.width * 0.7826994, size.height * 0.8173742);
    path_37.lineTo(size.width * 0.7667975, size.height * 0.8304233);
    path_37.cubicTo(
        size.width * 0.7633804,
        size.height * 0.8332270,
        size.width * 0.7600552,
        size.height * 0.8346196,
        size.width * 0.7568160,
        size.height * 0.8346074);
    path_37.cubicTo(
        size.width * 0.7535706,
        size.height * 0.8346380,
        size.width * 0.7508098,
        size.height * 0.8332638,
        size.width * 0.7485337,
        size.height * 0.8304908);
    path_37.cubicTo(
        size.width * 0.7470429,
        size.height * 0.8286748,
        size.width * 0.7462699,
        size.height * 0.8265583,
        size.width * 0.7462270,
        size.height * 0.8241472);
    path_37.cubicTo(
        size.width * 0.7461718,
        size.height * 0.8217791,
        size.width * 0.7468650,
        size.height * 0.8197485,
        size.width * 0.7482945,
        size.height * 0.8180552);
    path_37.lineTo(size.width * 0.7480736, size.height * 0.8177853);
    path_37.cubicTo(
        size.width * 0.7455583,
        size.height * 0.8193313,
        size.width * 0.7430000,
        size.height * 0.8199264,
        size.width * 0.7404049,
        size.height * 0.8195706);
    path_37.cubicTo(
        size.width * 0.7378344,
        size.height * 0.8192331,
        size.width * 0.7356442,
        size.height * 0.8179693,
        size.width * 0.7338405,
        size.height * 0.8157730);
    path_37.cubicTo(
        size.width * 0.7313129,
        size.height * 0.8126933,
        size.width * 0.7304110,
        size.height * 0.8093313,
        size.width * 0.7311350,
        size.height * 0.8056933);
    path_37.cubicTo(
        size.width * 0.7318344,
        size.height * 0.8020736,
        size.width * 0.7340859,
        size.height * 0.7987055,
        size.width * 0.7378773,
        size.height * 0.7955951);
    path_37.lineTo(size.width * 0.7539755, size.height * 0.7823804);
    path_37.close();

    Paint paint_37_fill = Paint()..style = PaintingStyle.fill;
    paint_37_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_37, paint_37_fill);

    Path path_38 = Path();
    path_38.moveTo(size.width * 0.6864601, size.height * 0.8308957);
    path_38.lineTo(size.width * 0.7103374, size.height * 0.8693558);
    path_38.lineTo(size.width * 0.7025153, size.height * 0.8742147);
    path_38.lineTo(size.width * 0.6786380, size.height * 0.8357546);
    path_38.lineTo(size.width * 0.6864601, size.height * 0.8308957);
    path_38.close();
    path_38.moveTo(size.width * 0.7188650, size.height * 0.8390368);
    path_38.lineTo(size.width * 0.7255215, size.height * 0.8349080);
    path_38.lineTo(size.width * 0.7167117, size.height * 0.8207239);
    path_38.lineTo(size.width * 0.7100613, size.height * 0.8248528);
    path_38.cubicTo(
        size.width * 0.7077914,
        size.height * 0.8262577,
        size.width * 0.7063926,
        size.height * 0.8280245,
        size.width * 0.7058528,
        size.height * 0.8301472);
    path_38.cubicTo(
        size.width * 0.7053067,
        size.height * 0.8323129,
        size.width * 0.7056871,
        size.height * 0.8344540,
        size.width * 0.7070061,
        size.height * 0.8365767);
    path_38.cubicTo(
        size.width * 0.7083374,
        size.height * 0.8387178,
        size.width * 0.7100859,
        size.height * 0.8400123,
        size.width * 0.7122515,
        size.height * 0.8404601);
    path_38.cubicTo(
        size.width * 0.7144233,
        size.height * 0.8409018,
        size.width * 0.7166258,
        size.height * 0.8404294,
        size.width * 0.7188650,
        size.height * 0.8390368);
    path_38.close();
    path_38.moveTo(size.width * 0.7207178, size.height * 0.8096258);
    path_38.lineTo(size.width * 0.7445951, size.height * 0.8480859);
    path_38.lineTo(size.width * 0.7367362, size.height * 0.8529632);
    path_38.lineTo(size.width * 0.7293558, size.height * 0.8410859);
    path_38.lineTo(size.width * 0.7220061, size.height * 0.8456442);
    path_38.cubicTo(
        size.width * 0.7175460,
        size.height * 0.8484172,
        size.width * 0.7132515,
        size.height * 0.8494110,
        size.width * 0.7091227,
        size.height * 0.8486319);
    path_38.cubicTo(
        size.width * 0.7049816,
        size.height * 0.8478957,
        size.width * 0.7016564,
        size.height * 0.8455031,
        size.width * 0.6991472,
        size.height * 0.8414540);
    path_38.cubicTo(
        size.width * 0.6966196,
        size.height * 0.8373865,
        size.width * 0.6959325,
        size.height * 0.8333190,
        size.width * 0.6970920,
        size.height * 0.8292577);
    path_38.cubicTo(
        size.width * 0.6982393,
        size.height * 0.8252331,
        size.width * 0.7010491,
        size.height * 0.8218405,
        size.width * 0.7055092,
        size.height * 0.8190675);
    path_38.lineTo(size.width * 0.7207178, size.height * 0.8096258);
    path_38.close();

    Paint paint_38_fill = Paint()..style = PaintingStyle.fill;
    paint_38_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_38, paint_38_fill);

    Path path_39 = Path();
    path_39.moveTo(size.width * 0.6447301, size.height * 0.8510123);
    path_39.lineTo(size.width * 0.6527485, size.height * 0.8690184);
    path_39.lineTo(size.width * 0.6714601, size.height * 0.8606933);
    path_39.lineTo(size.width * 0.6634479, size.height * 0.8426871);
    path_39.lineTo(size.width * 0.6718957, size.height * 0.8389202);
    path_39.lineTo(size.width * 0.6903067, size.height * 0.8802822);
    path_39.lineTo(size.width * 0.6818528, size.height * 0.8840429);
    path_39.lineTo(size.width * 0.6744356, size.height * 0.8673742);
    path_39.lineTo(size.width * 0.6557239, size.height * 0.8757055);
    path_39.lineTo(size.width * 0.6631411, size.height * 0.8923742);
    path_39.lineTo(size.width * 0.6546871, size.height * 0.8961350);
    path_39.lineTo(size.width * 0.6362822, size.height * 0.8547791);
    path_39.lineTo(size.width * 0.6447301, size.height * 0.8510123);
    path_39.close();

    Paint paint_39_fill = Paint()..style = PaintingStyle.fill;
    paint_39_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_39, paint_39_fill);

    Path path_40 = Path();
    path_40.moveTo(size.width * 0.5981595, size.height * 0.8726012);
    path_40.cubicTo(
        size.width * 0.6006558,
        size.height * 0.8674479,
        size.width * 0.6050160,
        size.height * 0.8639141,
        size.width * 0.6112399,
        size.height * 0.8620000);
    path_40.cubicTo(
        size.width * 0.6174663,
        size.height * 0.8600798,
        size.width * 0.6230552,
        size.height * 0.8605521,
        size.width * 0.6280184,
        size.height * 0.8634049);
    path_40.cubicTo(
        size.width * 0.6329877,
        size.height * 0.8662883,
        size.width * 0.6365399,
        size.height * 0.8711963,
        size.width * 0.6386748,
        size.height * 0.8781350);
    path_40.cubicTo(
        size.width * 0.6408037,
        size.height * 0.8850429,
        size.width * 0.6406196,
        size.height * 0.8910736,
        size.width * 0.6381227,
        size.height * 0.8962270);
    path_40.cubicTo(
        size.width * 0.6356380,
        size.height * 0.9014049,
        size.width * 0.6312822,
        size.height * 0.9049509,
        size.width * 0.6250552,
        size.height * 0.9068712);
    path_40.cubicTo(
        size.width * 0.6188344,
        size.height * 0.9087853,
        size.width * 0.6132356,
        size.height * 0.9083006,
        size.width * 0.6082656,
        size.height * 0.9054172);
    path_40.cubicTo(
        size.width * 0.6033043,
        size.height * 0.9025644,
        size.width * 0.5997595,
        size.height * 0.8976810,
        size.width * 0.5976319,
        size.height * 0.8907730);
    path_40.cubicTo(
        size.width * 0.5954957,
        size.height * 0.8838344,
        size.width * 0.5956718,
        size.height * 0.8777791,
        size.width * 0.5981595,
        size.height * 0.8726012);
    path_40.close();
    path_40.moveTo(size.width * 0.6134939, size.height * 0.8693190);
    path_40.cubicTo(
        size.width * 0.6099571,
        size.height * 0.8704049,
        size.width * 0.6075699,
        size.height * 0.8726012,
        size.width * 0.6063325,
        size.height * 0.8758896);
    path_40.cubicTo(
        size.width * 0.6050951,
        size.height * 0.8791840,
        size.width * 0.6052110,
        size.height * 0.8832147,
        size.width * 0.6066798,
        size.height * 0.8879877);
    path_40.cubicTo(
        size.width * 0.6081405,
        size.height * 0.8927301,
        size.width * 0.6103221,
        size.height * 0.8961104,
        size.width * 0.6132252,
        size.height * 0.8981288);
    path_40.cubicTo(
        size.width * 0.6160982,
        size.height * 0.9001534,
        size.width * 0.6192945,
        size.height * 0.9006258,
        size.width * 0.6228037,
        size.height * 0.8995460);
    path_40.cubicTo(
        size.width * 0.6263129,
        size.height * 0.8984663,
        size.width * 0.6286994,
        size.height * 0.8962761,
        size.width * 0.6299632,
        size.height * 0.8929755);
    path_40.cubicTo(
        size.width * 0.6312025,
        size.height * 0.8896810,
        size.width * 0.6310920,
        size.height * 0.8856626,
        size.width * 0.6296319,
        size.height * 0.8809202);
    path_40.cubicTo(
        size.width * 0.6281595,
        size.height * 0.8761472,
        size.width * 0.6259877,
        size.height * 0.8727485,
        size.width * 0.6231104,
        size.height * 0.8707239);
    path_40.cubicTo(
        size.width * 0.6202086,
        size.height * 0.8687055,
        size.width * 0.6170061,
        size.height * 0.8682393,
        size.width * 0.6134939,
        size.height * 0.8693190);
    path_40.close();

    Paint paint_40_fill = Paint()..style = PaintingStyle.fill;
    paint_40_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_40, paint_40_fill);

    Path path_41 = Path();
    path_41.moveTo(size.width * 0.5538883, size.height * 0.9072638);
    path_41.lineTo(size.width * 0.5626933, size.height * 0.9056564);
    path_41.cubicTo(
        size.width * 0.5637166,
        size.height * 0.9077423,
        size.width * 0.5652294,
        size.height * 0.9093190,
        size.width * 0.5672319,
        size.height * 0.9103804);
    path_41.cubicTo(
        size.width * 0.5692344,
        size.height * 0.9114417,
        size.width * 0.5715902,
        size.height * 0.9117301,
        size.width * 0.5742994,
        size.height * 0.9112331);
    path_41.cubicTo(
        size.width * 0.5778270,
        size.height * 0.9105890,
        size.width * 0.5804055,
        size.height * 0.9086564,
        size.width * 0.5820344,
        size.height * 0.9054417);
    path_41.cubicTo(
        size.width * 0.5836399,
        size.height * 0.9022638,
        size.width * 0.5840147,
        size.height * 0.8983313,
        size.width * 0.5831583,
        size.height * 0.8936442);
    path_41.cubicTo(
        size.width * 0.5822810,
        size.height * 0.8888466,
        size.width * 0.5805313,
        size.height * 0.8852577,
        size.width * 0.5779092,
        size.height * 0.8828834);
    path_41.cubicTo(
        size.width * 0.5752638,
        size.height * 0.8805337,
        size.width * 0.5721491,
        size.height * 0.8796933,
        size.width * 0.5685650,
        size.height * 0.8803497);
    path_41.cubicTo(
        size.width * 0.5658840,
        size.height * 0.8808344,
        size.width * 0.5637853,
        size.height * 0.8818773,
        size.width * 0.5622675,
        size.height * 0.8834663);
    path_41.cubicTo(
        size.width * 0.5607552,
        size.height * 0.8850859,
        size.width * 0.5598883,
        size.height * 0.8871227,
        size.width * 0.5596675,
        size.height * 0.8895828);
    path_41.lineTo(size.width * 0.5508626, size.height * 0.8911963);
    path_41.cubicTo(
        size.width * 0.5506865,
        size.height * 0.8865583,
        size.width * 0.5520810,
        size.height * 0.8825460,
        size.width * 0.5550466,
        size.height * 0.8791411);
    path_41.cubicTo(
        size.width * 0.5579890,
        size.height * 0.8757791,
        size.width * 0.5620564,
        size.height * 0.8736196,
        size.width * 0.5672491,
        size.height * 0.8726687);
    path_41.cubicTo(
        size.width * 0.5736552,
        size.height * 0.8714969,
        size.width * 0.5791282,
        size.height * 0.8726380,
        size.width * 0.5836687,
        size.height * 0.8760982);
    path_41.cubicTo(
        size.width * 0.5882086,
        size.height * 0.8795583,
        size.width * 0.5911288,
        size.height * 0.8848405,
        size.width * 0.5924288,
        size.height * 0.8919509);
    path_41.cubicTo(
        size.width * 0.5937086,
        size.height * 0.8989509,
        size.width * 0.5928393,
        size.height * 0.9048834,
        size.width * 0.5898209,
        size.height * 0.9097546);
    path_41.cubicTo(
        size.width * 0.5867798,
        size.height * 0.9146503,
        size.width * 0.5820988,
        size.height * 0.9176810,
        size.width * 0.5757773,
        size.height * 0.9188405);
    path_41.cubicTo(
        size.width * 0.5705000,
        size.height * 0.9198037,
        size.width * 0.5659049,
        size.height * 0.9191534,
        size.width * 0.5619920,
        size.height * 0.9168957);
    path_41.cubicTo(
        size.width * 0.5580564,
        size.height * 0.9146687,
        size.width * 0.5553552,
        size.height * 0.9114601,
        size.width * 0.5538883,
        size.height * 0.9072638);
    path_41.close();

    Paint paint_41_fill = Paint()..style = PaintingStyle.fill;
    paint_41_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_41, paint_41_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}

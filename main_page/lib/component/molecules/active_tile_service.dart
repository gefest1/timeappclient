import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/components/pages/qfit_to_timeapp/active_session/active_minute_page.dart';
import 'package:li/const.dart';
import 'package:li/data/models/session_minute.dart';
import 'package:li/main.dart';
import 'package:li/utils/color.dart';

class ActiveTileService extends StatelessWidget {
  final SessionMinute session;
  const ActiveTileService({
    required this.session,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: GestureDetector(
        onTap: () {
          routerDelegate.push(ActiveMinutePage(session: session));
        },
        child: Container(
          height: 96,
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: bigBlur,
            borderRadius: BorderRadius.circular(16),
          ),
          child: Center(
            child: ListTile(
              trailing: const Icon(
                Icons.arrow_forward_ios_rounded,
              ),
              title: Text(
                session.institution!.name!,
                style: const TextStyle(
                  color: ColorData.mainBlack,
                  fontWeight: FontWeight.w500,
                  fontSize: 18,
                  height: 29 / 24,
                ),
              ),
              subtitle: Text(
                session.service!.name!,
                style: const TextStyle(
                  color: Color(0xFF979797),
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                  height: 29 / 24,
                ),
              ),
              leading: session.institution?.iconPath == null
                  ? null
                  : RepaintBoundary(
                      child: SvgPicture.network(
                        session.institution!.iconPath!,
                        width: 60,
                        height: 60,
                      ),
                    ),
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

Path getPath2(Size size) {
  return Path()
    ..moveTo(size.width * 0.04166750, size.height * 0.001306112)
    ..cubicTo(
        size.width * 0.02100750,
        size.height * 0.008770318,
        size.width * 0.004235000,
        size.height * 0.04235180,
        size.width * 0.0006250000,
        size.height * 0.08347694)
    ..cubicTo(
        size.width * -0.0002325000,
        size.height * 0.09323553,
        size.width * -0.0003200000,
        size.height * 0.2466068,
        size.width * 0.0005325000,
        size.height * 0.2466466)
    ..cubicTo(
        size.width * 0.002472500,
        size.height * 0.2467360,
        size.width * 0.04155000,
        size.height * 0.2309534,
        size.width * 0.04139250,
        size.height * 0.2301389)
    ..cubicTo(
        size.width * 0.04127500,
        size.height * 0.2295281,
        size.width * 0.04314750,
        size.height * 0.2283858,
        size.width * 0.04664500,
        size.height * 0.2269456)
    ..cubicTo(
        size.width * 0.04963750,
        size.height * 0.2257140,
        size.width * 0.05313500,
        size.height * 0.2240404,
        size.width * 0.05442250,
        size.height * 0.2232309)
    ..lineTo(size.width * 0.05676000, size.height * 0.2217560)
    ..lineTo(size.width * 0.05293250, size.height * 0.2211898)
    ..cubicTo(
        size.width * 0.05083000,
        size.height * 0.2208720,
        size.width * 0.04632250,
        size.height * 0.2205790,
        size.width * 0.04291750,
        size.height * 0.2205293)
    ..lineTo(size.width * 0.03672750, size.height * 0.2204399)
    ..lineTo(size.width * 0.03292500, size.height * 0.2097179)
    ..arcToPoint(Offset(size.width * 0.02880500, size.height * 0.1979629),
        radius:
            Radius.elliptical(size.width * 0.3797950, size.height * 0.7544559),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..cubicTo(
        size.width * 0.02838500,
        size.height * 0.1966071,
        size.width * 0.03362750,
        size.height * 0.1931158,
        size.width * 0.03943750,
        size.height * 0.1908761)
    ..cubicTo(
        size.width * 0.05408500,
        size.height * 0.1852345,
        size.width * 0.07129750,
        size.height * 0.1878616,
        size.width * 0.08627750,
        size.height * 0.1980274)
    ..cubicTo(
        size.width * 0.08801500,
        size.height * 0.1992044,
        size.width * 0.08951000,
        size.height * 0.2001679,
        size.width * 0.08960250,
        size.height * 0.2001679)
    ..cubicTo(
        size.width * 0.08969250,
        size.height * 0.2001679,
        size.width * 0.08938250,
        size.height * 0.1990902,
        size.width * 0.08890750,
        size.height * 0.1977692)
    ..cubicTo(
        size.width * 0.08825750,
        size.height * 0.1959515,
        size.width * 0.08816750,
        size.height * 0.1951272,
        size.width * 0.08854000,
        size.height * 0.1943872)
    ..cubicTo(
        size.width * 0.08940750,
        size.height * 0.1926639,
        size.width * 0.08996250,
        size.height * 0.1933095,
        size.width * 0.09205750,
        size.height * 0.1984992)
    ..cubicTo(
        size.width * 0.09453250,
        size.height * 0.2046275,
        size.width * 0.09453000,
        size.height * 0.2046275,
        size.width * 0.09689000,
        size.height * 0.1965872)
    ..cubicTo(
        size.width * 0.09856500,
        size.height * 0.1908910,
        size.width * 0.09885750,
        size.height * 0.1902901,
        size.width * 0.09955500,
        size.height * 0.1911542)
    ..cubicTo(
        size.width * 0.1006200,
        size.height * 0.1924752,
        size.width * 0.1005650,
        size.height * 0.1930910,
        size.width * 0.09886500,
        size.height * 0.1986929)
    ..cubicTo(
        size.width * 0.09805250,
        size.height * 0.2013597,
        size.width * 0.09748750,
        size.height * 0.2035449,
        size.width * 0.09760750,
        size.height * 0.2035449)
    ..cubicTo(
        size.width * 0.09854250,
        size.height * 0.2035449,
        size.width * 0.1014450,
        size.height * 0.1993484,
        size.width * 0.1037425,
        size.height * 0.1946703)
    ..cubicTo(
        size.width * 0.1120450,
        size.height * 0.1777554,
        size.width * 0.1180025,
        size.height * 0.1724117,
        size.width * 0.1295225,
        size.height * 0.1715427)
    ..lineTo(size.width * 0.1368075, size.height * 0.1709914)
    ..lineTo(size.width * 0.1399400, size.height * 0.1678180)
    ..cubicTo(
        size.width * 0.1416600,
        size.height * 0.1660699,
        size.width * 0.1445050,
        size.height * 0.1621317,
        size.width * 0.1462575,
        size.height * 0.1590675)
    ..cubicTo(
        size.width * 0.1593400,
        size.height * 0.1362031,
        size.width * 0.1729975,
        size.height * 0.1316690,
        size.width * 0.1835850,
        size.height * 0.1466818)
    ..lineTo(size.width * 0.1855875, size.height * 0.1495175)
    ..lineTo(size.width * 0.1859775, size.height * 0.1469053)
    ..cubicTo(
        size.width * 0.1862550,
        size.height * 0.1450430,
        size.width * 0.1861775,
        size.height * 0.1431260,
        size.width * 0.1857050,
        size.height * 0.1401960)
    ..lineTo(size.width * 0.1850450, size.height * 0.1360889)
    ..lineTo(size.width * 0.1878625, size.height * 0.1328658)
    ..cubicTo(
        size.width * 0.1905650,
        size.height * 0.1297769,
        size.width * 0.1948250,
        size.height * 0.1222084,
        size.width * 0.1958575,
        size.height * 0.1186675)
    ..cubicTo(
        size.width * 0.1963175,
        size.height * 0.1170882,
        size.width * 0.1963950,
        size.height * 0.1171875,
        size.width * 0.1970400,
        size.height * 0.1201822)
    ..cubicTo(
        size.width * 0.1991375,
        size.height * 0.1299110,
        size.width * 0.1997750,
        size.height * 0.1445464,
        size.width * 0.1984250,
        size.height * 0.1519957)
    ..cubicTo(
        size.width * 0.1980500,
        size.height * 0.1540666,
        size.width * 0.1978075,
        size.height * 0.1558892,
        size.width * 0.1978875,
        size.height * 0.1560531)
    ..cubicTo(
        size.width * 0.1982675,
        size.height * 0.1568079,
        size.width * 0.2015150,
        size.height * 0.1454353,
        size.width * 0.2024125,
        size.height * 0.1401960)
    ..cubicTo(
        size.width * 0.2035100,
        size.height * 0.1337945,
        size.width * 0.2035075,
        size.height * 0.1337995,
        size.width * 0.2046325,
        size.height * 0.1354930)
    ..cubicTo(
        size.width * 0.2053650,
        size.height * 0.1366004,
        size.width * 0.2053625,
        size.height * 0.1368388,
        size.width * 0.2045450,
        size.height * 0.1414475)
    ..cubicTo(
        size.width * 0.2018200,
        size.height * 0.1568576,
        size.width * 0.1956450,
        size.height * 0.1677783,
        size.width * 0.1891100,
        size.height * 0.1687516)
    ..lineTo(size.width * 0.1866825, size.height * 0.1691191)
    ..lineTo(size.width * 0.1875025, size.height * 0.1723025)
    ..cubicTo(
        size.width * 0.1990150,
        size.height * 0.2171374,
        size.width * 0.1958950,
        size.height * 0.2485834,
        size.width * 0.1796425,
        size.height * 0.2515035)
    ..lineTo(size.width * 0.1762525, size.height * 0.2521094)
    ..lineTo(size.width * 0.1722775, size.height * 0.2660048)
    ..cubicTo(
        size.width * 0.1700900,
        size.height * 0.2736478,
        size.width * 0.1682825,
        size.height * 0.2804863,
        size.width * 0.1682575,
        size.height * 0.2812064)
    ..cubicTo(
        size.width * 0.1682350,
        size.height * 0.2819265,
        size.width * 0.1748775,
        size.height * 0.3065688,
        size.width * 0.1830175,
        size.height * 0.3359687)
    ..cubicTo(
        size.width * 0.1957350,
        size.height * 0.3818813,
        size.width * 0.1977325,
        size.height * 0.3895640,
        size.width * 0.1971825,
        size.height * 0.3904430)
    ..cubicTo(
        size.width * 0.1966300,
        size.height * 0.3913320,
        size.width * 0.1969250,
        size.height * 0.3928069,
        size.width * 0.1993700,
        size.height * 0.4013389)
    ..lineTo(size.width * 0.2021975, size.height * 0.4112117)
    ..lineTo(size.width * 0.2253550, size.height * 0.3845283)
    ..lineTo(size.width * 0.2485125, size.height * 0.3578399)
    ..lineTo(size.width * 0.2533875, size.height * 0.3402397)
    ..cubicTo(
        size.width * 0.2560675,
        size.height * 0.3305556,
        size.width * 0.2583825,
        size.height * 0.3226345,
        size.width * 0.2585300,
        size.height * 0.3226345)
    ..cubicTo(
        size.width * 0.2595450,
        size.height * 0.3226345,
        size.width * 0.2615125,
        size.height * 0.3301881,
        size.width * 0.2618550,
        size.height * 0.3354026)
    ..lineTo(size.width * 0.2621175, size.height * 0.3393706)
    ..lineTo(size.width * 0.2687075, size.height * 0.3271934)
    ..cubicTo(
        size.width * 0.2768100,
        size.height * 0.3122203,
        size.width * 0.2791700,
        size.height * 0.3104077,
        size.width * 0.2809500,
        size.height * 0.3177875)
    ..cubicTo(
        size.width * 0.2812575,
        size.height * 0.3190588,
        size.width * 0.2817525,
        size.height * 0.3201017,
        size.width * 0.2820500,
        size.height * 0.3201017)
    ..cubicTo(
        size.width * 0.2833225,
        size.height * 0.3201017,
        size.width * 0.2844975,
        size.height * 0.3240200,
        size.width * 0.2843600,
        size.height * 0.3278142)
    ..cubicTo(
        size.width * 0.2842425,
        size.height * 0.3310572,
        size.width * 0.2843675,
        size.height * 0.3316779,
        size.width * 0.2853950,
        size.height * 0.3329691)
    ..cubicTo(
        size.width * 0.2867625,
        size.height * 0.3346825,
        size.width * 0.2869850,
        size.height * 0.3388392,
        size.width * 0.2859275,
        size.height * 0.3428966)
    ..cubicTo(
        size.width * 0.2855275,
        size.height * 0.3444411,
        size.width * 0.2854475,
        size.height * 0.3455336,
        size.width * 0.2857150,
        size.height * 0.3458614)
    ..cubicTo(
        size.width * 0.2879850,
        size.height * 0.3486475,
        size.width * 0.2839400,
        size.height * 0.3631935,
        size.width * 0.2773450,
        size.height * 0.3759715)
    ..cubicTo(
        size.width * 0.2745700,
        size.height * 0.3813499,
        size.width * 0.2020650,
        size.height * 0.4777986,
        size.width * 0.1962350,
        size.height * 0.4838673)
    ..cubicTo(
        size.width * 0.1927700,
        size.height * 0.4874727,
        size.width * 0.1861600,
        size.height * 0.4871400,
        size.width * 0.1824775,
        size.height * 0.4831720)
    ..cubicTo(
        size.width * 0.1784325,
        size.height * 0.4788167,
        size.width * 0.1768000,
        size.height * 0.4744514,
        size.width * 0.1711475,
        size.height * 0.4528881)
    ..lineTo(size.width * 0.1660600, size.height * 0.4334901)
    ..lineTo(size.width * 0.1644425, size.height * 0.4356206)
    ..cubicTo(
        size.width * 0.1624550,
        size.height * 0.4382328,
        size.width * 0.1636950,
        size.height * 0.4421114,
        size.width * 0.1554525,
        size.height * 0.4075268)
    ..lineTo(size.width * 0.1484075, size.height * 0.3779779)
    ..lineTo(size.width * 0.1460525, size.height * 0.3830335)
    ..cubicTo(
        size.width * 0.1394050,
        size.height * 0.3973113,
        size.width * 0.1237150,
        size.height * 0.4521283,
        size.width * 0.1223550,
        size.height * 0.4658300)
    ..cubicTo(
        size.width * 0.1222225,
        size.height * 0.4671610,
        size.width * 0.1721750,
        size.height * 0.5157950,
        size.width * 0.1805950,
        size.height * 0.5225342)
    ..cubicTo(
        size.width * 0.2005025,
        size.height * 0.5384608,
        size.width * 0.2126550,
        size.height * 0.5580177,
        size.width * 0.2137175,
        size.height * 0.5758464)
    ..cubicTo(
        size.width * 0.2139400,
        size.height * 0.5795661,
        size.width * 0.1788625,
        size.height * 0.7605793,
        size.width * 0.1772925,
        size.height * 0.7638321)
    ..cubicTo(
        size.width * 0.1770950,
        size.height * 0.7642344,
        size.width * 0.1757425,
        size.height * 0.7649992,
        size.width * 0.1742875,
        size.height * 0.7655206)
    ..cubicTo(
        size.width * 0.1718000,
        size.height * 0.7664096,
        size.width * 0.1716100,
        size.height * 0.7666430,
        size.width * 0.1711275,
        size.height * 0.7694241)
    ..cubicTo(
        size.width * 0.1699500,
        size.height * 0.7762129,
        size.width * 0.1699500,
        size.height * 0.7761831,
        size.width * 0.1708900,
        size.height * 0.7761831)
    ..cubicTo(
        size.width * 0.1713725,
        size.height * 0.7761831,
        size.width * 0.1717675,
        size.height * 0.7762824,
        size.width * 0.1717675,
        size.height * 0.7764115)
    ..cubicTo(
        size.width * 0.1717675,
        size.height * 0.7765357,
        size.width * 0.1707275,
        size.height * 0.7865128,
        size.width * 0.1694525,
        size.height * 0.7985807)
    ..cubicTo(
        size.width * 0.1673850,
        size.height * 0.8181823,
        size.width * 0.1672100,
        size.height * 0.8206306,
        size.width * 0.1678025,
        size.height * 0.8215295)
    ..cubicTo(
        size.width * 0.1681675,
        size.height * 0.8220857,
        size.width * 0.1755425,
        size.height * 0.8304488,
        size.width * 0.1841925,
        size.height * 0.8401130)
    ..lineTo(size.width * 0.1999175, size.height * 0.8576835)
    ..lineTo(size.width * 0.1975925, size.height * 0.8579318)
    ..lineTo(size.width * 0.1952675, size.height * 0.8581851)
    ..lineTo(size.width * 0.1950275, size.height * 0.8628285)
    ..cubicTo(
        size.width * 0.1944950,
        size.height * 0.8730837,
        size.width * 0.1906925,
        size.height * 0.8804833,
        size.width * 0.1848625,
        size.height * 0.8826237)
    ..cubicTo(
        size.width * 0.1832950,
        size.height * 0.8831998,
        size.width * 0.1703400,
        size.height * 0.8833935,
        size.width * 0.1470425,
        size.height * 0.8831899)
    ..lineTo(size.width * 0.1116075, size.height * 0.8828770)
    ..lineTo(size.width * 0.09767500, size.height * 0.8799370)
    ..cubicTo(
        size.width * 0.08398250,
        size.height * 0.8770417,
        size.width * 0.08254000,
        size.height * 0.8764359,
        size.width * 0.08305000,
        size.height * 0.8737889)
    ..cubicTo(
        size.width * 0.08337500,
        size.height * 0.8721153,
        size.width * 0.08512000,
        size.height * 0.8721053,
        size.width * 0.09268750,
        size.height * 0.8737293)
    ..cubicTo(
        size.width * 0.09584500,
        size.height * 0.8744096,
        size.width * 0.09871500,
        size.height * 0.8749410,
        size.width * 0.09906500,
        size.height * 0.8749162)
    ..cubicTo(
        size.width * 0.09941500,
        size.height * 0.8748914,
        size.width * 0.09568500,
        size.height * 0.8726019,
        size.width * 0.09077500,
        size.height * 0.8698258)
    ..cubicTo(
        size.width * 0.08140500,
        size.height * 0.8645319,
        size.width * 0.08010000,
        size.height * 0.8633201,
        size.width * 0.08126250,
        size.height * 0.8610108)
    ..cubicTo(
        size.width * 0.08186500,
        size.height * 0.8598189,
        size.width * 0.08299750,
        size.height * 0.8602957,
        size.width * 0.09331750,
        size.height * 0.8661012)
    ..cubicTo(
        size.width * 0.09958250,
        size.height * 0.8696272,
        size.width * 0.1047750,
        size.height * 0.8723834,
        size.width * 0.1048550,
        size.height * 0.8722195)
    ..cubicTo(
        size.width * 0.1049375,
        size.height * 0.8720606,
        size.width * 0.1020375,
        size.height * 0.8685843,
        size.width * 0.09841250,
        size.height * 0.8644921)
    ..cubicTo(
        size.width * 0.09134250,
        size.height * 0.8565114,
        size.width * 0.09127500,
        size.height * 0.8564121,
        size.width * 0.09166750,
        size.height * 0.8543809)
    ..cubicTo(
        size.width * 0.09211250,
        size.height * 0.8520766,
        size.width * 0.09286500,
        size.height * 0.8527371,
        size.width * 0.1032200,
        size.height * 0.8645120)
    ..cubicTo(
        size.width * 0.1114900,
        size.height * 0.8739180,
        size.width * 0.1134400,
        size.height * 0.8758200,
        size.width * 0.1140450,
        size.height * 0.8750701)
    ..cubicTo(
        size.width * 0.1144700,
        size.height * 0.8745437,
        size.width * 0.1169625,
        size.height * 0.8741514,
        size.width * 0.1198925,
        size.height * 0.8741514)
    ..lineTo(size.width * 0.1250000, size.height * 0.8741514)
    ..lineTo(size.width * 0.1250000, size.height * 0.8661310)
    ..cubicTo(
        size.width * 0.1250000,
        size.height * 0.8595607,
        size.width * 0.1248850,
        size.height * 0.8581056,
        size.width * 0.1243625,
        size.height * 0.8581056)
    ..cubicTo(
        size.width * 0.1234850,
        size.height * 0.8581056,
        size.width * 0.1233975,
        size.height * 0.8595855,
        size.width * 0.1268225,
        size.height * 0.8171443)
    ..lineTo(size.width * 0.1298900, size.height * 0.7791380)
    ..lineTo(size.width * 0.1314750, size.height * 0.7788748)
    ..cubicTo(
        size.width * 0.1329050,
        size.height * 0.7786413,
        size.width * 0.1331000,
        size.height * 0.7783136,
        size.width * 0.1334700,
        size.height * 0.7754977)
    ..cubicTo(
        size.width * 0.1344375,
        size.height * 0.7681279,
        size.width * 0.1357025,
        size.height * 0.7566212,
        size.width * 0.1355650,
        size.height * 0.7564176)
    ..cubicTo(
        size.width * 0.1354825,
        size.height * 0.7562934,
        size.width * 0.1342675,
        size.height * 0.7553051,
        size.width * 0.1328650,
        size.height * 0.7542225)
    ..cubicTo(
        size.width * 0.1295375,
        size.height * 0.7516451,
        size.width * 0.1220250,
        size.height * 0.7442305,
        size.width * 0.1220250,
        size.height * 0.7435154)
    ..cubicTo(
        size.width * 0.1220250,
        size.height * 0.7432124,
        size.width * 0.1274925,
        size.height * 0.7131619,
        size.width * 0.1341775,
        size.height * 0.6767348)
    ..cubicTo(
        size.width * 0.1408625,
        size.height * 0.6403077,
        size.width * 0.1462675,
        size.height * 0.6104012,
        size.width * 0.1461900,
        size.height * 0.6102771)
    ..cubicTo(
        size.width * 0.1460675,
        size.height * 0.6100883,
        size.width * 0.1019300,
        size.height * 0.5853269,
        size.width * 0.1011175,
        size.height * 0.5849991)
    ..cubicTo(
        size.width * 0.1009625,
        size.height * 0.5849345,
        size.width * 0.08541250,
        size.height * 0.6207955,
        size.width * 0.06656750,
        size.height * 0.6646967)
    ..cubicTo(
        size.width * 0.04772000,
        size.height * 0.7085930,
        size.width * 0.03091500,
        size.height * 0.7474089,
        size.width * 0.02922500,
        size.height * 0.7509548)
    ..cubicTo(
        size.width * 0.02278000,
        size.height * 0.7644579,
        size.width * 0.01592000,
        size.height * 0.7701938,
        size.width * 0.004570000,
        size.height * 0.7715645)
    ..lineTo(0, size.height * 0.7721207)
    ..lineTo(0, size.height * 0.8407636)
    ..cubicTo(
        0,
        size.height * 0.9150779,
        size.width * 0.00002750000,
        size.height * 0.9161705,
        size.width * 0.002352500,
        size.height * 0.9303291)
    ..cubicTo(
        size.width * 0.008112500,
        size.height * 0.9654451,
        size.width * 0.02375750,
        size.height * 0.9924315,
        size.width * 0.04202250,
        size.height * 0.9987584)
    ..cubicTo(
        size.width * 0.04515250,
        size.height * 0.9998411,
        size.width * 0.08896000,
        size.height,
        size.width * 0.3899225,
        size.height)
    ..lineTo(size.width * 0.7342400, size.height)
    ..lineTo(size.width * 0.7347375, size.height * 0.9981426)
    ..cubicTo(
        size.width * 0.7360125,
        size.height * 0.9934148,
        size.width * 0.7341425,
        size.height * 0.9717075,
        size.width * 0.7300250,
        size.height * 0.9434399)
    ..cubicTo(
        size.width * 0.7285500,
        size.height * 0.9333138,
        size.width * 0.7236925,
        size.height * 0.9027617,
        size.width * 0.7234875,
        size.height * 0.9023197)
    ..cubicTo(
        size.width * 0.7234525,
        size.height * 0.9022452,
        size.width * 0.7185475,
        size.height * 0.9009937,
        size.width * 0.7125850,
        size.height * 0.8995386)
    ..cubicTo(
        size.width * 0.7066225,
        size.height * 0.8980835,
        size.width * 0.7012150,
        size.height * 0.8966831,
        size.width * 0.7005675,
        size.height * 0.8964298)
    ..lineTo(size.width * 0.6993925, size.height * 0.8959729)
    ..lineTo(size.width * 0.6995050, size.height * 0.8726069)
    ..lineTo(size.width * 0.6996175, size.height * 0.8492359)
    ..lineTo(size.width * 0.7017850, size.height * 0.8494992)
    ..cubicTo(
        size.width * 0.7033375,
        size.height * 0.8496829,
        size.width * 0.7043625,
        size.height * 0.8492906,
        size.width * 0.7053975,
        size.height * 0.8481136)
    ..cubicTo(
        size.width * 0.7061950,
        size.height * 0.8472097,
        size.width * 0.7081375,
        size.height * 0.8456355,
        size.width * 0.7097150,
        size.height * 0.8446174)
    ..cubicTo(
        size.width * 0.7112925,
        size.height * 0.8435993,
        size.width * 0.7125850,
        size.height * 0.8424720,
        size.width * 0.7125850,
        size.height * 0.8421094)
    ..cubicTo(
        size.width * 0.7125850,
        size.height * 0.8415185,
        size.width * 0.7094775,
        size.height * 0.8262126,
        size.width * 0.7060600,
        size.height * 0.8099632)
    ..cubicTo(
        size.width * 0.7021775,
        size.height * 0.7915137,
        size.width * 0.7061800,
        size.height * 0.7692354,
        size.width * 0.7148450,
        size.height * 0.7610659)
    ..cubicTo(
        size.width * 0.7217900,
        size.height * 0.7545205,
        size.width * 0.7380750,
        size.height * 0.7489335,
        size.width * 0.7605600,
        size.height * 0.7453876)
    ..cubicTo(
        size.width * 0.7694400,
        size.height * 0.7439872,
        size.width * 0.7745550,
        size.height * 0.7422093,
        size.width * 0.7825250,
        size.height * 0.7377595)
    ..cubicTo(
        size.width * 0.7877325,
        size.height * 0.7348494,
        size.width * 0.8047425,
        size.height * 0.7234618,
        size.width * 0.8051825,
        size.height * 0.7225878)
    ..cubicTo(
        size.width * 0.8053150,
        size.height * 0.7223246,
        size.width * 0.8046300,
        size.height * 0.7149845,
        size.width * 0.8036625,
        size.height * 0.7062788)
    ..cubicTo(
        size.width * 0.7921450,
        size.height * 0.6028178,
        size.width * 0.7904200,
        size.height * 0.5448622,
        size.width * 0.7978400,
        size.height * 0.5106153)
    ..lineTo(size.width * 0.7992925, size.height * 0.5039208)
    ..lineTo(size.width * 0.7975825, size.height * 0.4926873)
    ..cubicTo(
        size.width * 0.7898525,
        size.height * 0.4418284,
        size.width * 0.7884225,
        size.height * 0.4092749,
        size.width * 0.7887575,
        size.height * 0.2917844)
    ..lineTo(size.width * 0.7889025, size.height * 0.2410795)
    ..lineTo(size.width * 0.7865650, size.height * 0.2420081)
    ..cubicTo(
        size.width * 0.7829875,
        size.height * 0.2434285,
        size.width * 0.7722375,
        size.height * 0.2428971,
        size.width * 0.7696300,
        size.height * 0.2411788)
    ..cubicTo(
        size.width * 0.7479100,
        size.height * 0.2268463,
        size.width * 0.7466850,
        size.height * 0.1497807,
        size.width * 0.7678575,
        size.height * 0.1297272)
    ..lineTo(size.width * 0.7706200, size.height * 0.1271100)
    ..lineTo(size.width * 0.7876275, size.height * 0.1271100)
    ..lineTo(size.width * 0.7903900, size.height * 0.1297272)
    ..cubicTo(
        size.width * 0.7942550,
        size.height * 0.1333873,
        size.width * 0.7972550,
        size.height * 0.1383883,
        size.width * 0.7996500,
        size.height * 0.1451671)
    ..lineTo(size.width * 0.8017025, size.height * 0.1509677)
    ..lineTo(size.width * 0.8025975, size.height * 0.1479085)
    ..cubicTo(
        size.width * 0.8072825,
        size.height * 0.1319223,
        size.width * 0.8130700,
        size.height * 0.1265091,
        size.width * 0.8250800,
        size.height * 0.1268766)
    ..lineTo(size.width * 0.8327525, size.height * 0.1271100)
    ..lineTo(size.width * 0.8358100, size.height * 0.1301295)
    ..lineTo(size.width * 0.8388700, size.height * 0.1331440)
    ..lineTo(size.width * 0.8404600, size.height * 0.1312667)
    ..cubicTo(
        size.width * 0.8437400,
        size.height * 0.1273881,
        size.width * 0.8462825,
        size.height * 0.1265886,
        size.width * 0.8544750,
        size.height * 0.1268617)
    ..lineTo(size.width * 0.8620800, size.height * 0.1271100)
    ..lineTo(size.width * 0.8653500, size.height * 0.1303232)
    ..cubicTo(
        size.width * 0.8688950,
        size.height * 0.1338045,
        size.width * 0.8732750,
        size.height * 0.1416560,
        size.width * 0.8747775,
        size.height * 0.1472231)
    ..cubicTo(
        size.width * 0.8758475,
        size.height * 0.1511862,
        size.width * 0.8759225,
        size.height * 0.1511564,
        size.width * 0.8775200,
        size.height * 0.1462150)
    ..cubicTo(
        size.width * 0.8823625,
        size.height * 0.1312220,
        size.width * 0.8877150,
        size.height * 0.1265091,
        size.width * 0.8994900,
        size.height * 0.1268816)
    ..lineTo(size.width * 0.9071000, size.height * 0.1271199)
    ..lineTo(size.width * 0.9100775, size.height * 0.1299904)
    ..cubicTo(
        size.width * 0.9281700,
        size.height * 0.1474466,
        size.width * 0.9302225,
        size.height * 0.2115156,
        size.width * 0.9134500,
        size.height * 0.2352889)
    ..cubicTo(
        size.width * 0.9060475,
        size.height * 0.2457825,
        size.width * 0.8922250,
        size.height * 0.2460606,
        size.width * 0.8835000,
        size.height * 0.2358898)
    ..cubicTo(
        size.width * 0.8833825,
        size.height * 0.2357507,
        size.width * 0.8837025,
        size.height * 0.2494177,
        size.width * 0.8842100,
        size.height * 0.2662581)
    ..cubicTo(
        size.width * 0.8871450,
        size.height * 0.3634169,
        size.width * 0.8876075,
        size.height * 0.4097169,
        size.width * 0.8858950,
        size.height * 0.4346770)
    ..cubicTo(
        size.width * 0.8837600,
        size.height * 0.4657357,
        size.width * 0.8810675,
        size.height * 0.4901297,
        size.width * 0.8779800,
        size.height * 0.5063344)
    ..lineTo(size.width * 0.8763725, size.height * 0.5147769)
    ..lineTo(size.width * 0.8729425, size.height * 0.5869905)
    ..cubicTo(
        size.width * 0.8709375,
        size.height * 0.6292629,
        size.width * 0.8693375,
        size.height * 0.6675275,
        size.width * 0.8690875,
        size.height * 0.6792775)
    ..lineTo(size.width * 0.8686575, size.height * 0.6993559)
    ..lineTo(size.width * 0.8705775, size.height * 0.7028670)
    ..cubicTo(
        size.width * 0.8804175,
        size.height * 0.7208844,
        size.width * 0.8849675,
        size.height * 0.7600926,
        size.width * 0.8817775,
        size.height * 0.7993802)
    ..cubicTo(
        size.width * 0.8814925,
        size.height * 0.8029013,
        size.width * 0.8813775,
        size.height * 0.8060200,
        size.width * 0.8815225,
        size.height * 0.8063081)
    ..cubicTo(
        size.width * 0.8816675,
        size.height * 0.8065961,
        size.width * 0.8913075,
        size.height * 0.8058214,
        size.width * 0.9029475,
        size.height * 0.8045798)
    ..cubicTo(
        size.width * 0.9237975,
        size.height * 0.8023599,
        size.width * 0.9433425,
        size.height * 0.8004480,
        size.width * 0.9587575,
        size.height * 0.7991120)
    ..cubicTo(
        size.width * 0.9659750,
        size.height * 0.7984863,
        size.width * 0.9933775,
        size.height * 0.8015356,
        size.width * 0.9988300,
        size.height * 0.8035717)
    ..lineTo(size.width * 1.000000, size.height * 0.8040087)
    ..lineTo(size.width * 1.000000, size.height * 0.4473011)
    ..cubicTo(
        size.width * 1.000000,
        size.height * 0.05859625,
        size.width * 1.000165,
        size.height * 0.08500653,
        size.width * 0.9976475,
        size.height * 0.06967089)
    ..cubicTo(
        size.width * 0.9918875,
        size.height * 0.03455485,
        size.width * 0.9762425,
        size.height * 0.007568496,
        size.width * 0.9579775,
        size.height * 0.001241551)
    ..cubicTo(
        size.width * 0.9527725,
        size.height * -0.0005611812,
        size.width * 0.04665750,
        size.height * -0.0005015867,
        size.width * 0.04166750,
        size.height * 0.001306112)
    ..moveTo(size.width * 0.7706150, size.height * 0.1332780)
    ..cubicTo(
        size.width * 0.7514750,
        size.height * 0.1464236,
        size.width * 0.7497075,
        size.height * 0.2162882,
        size.width * 0.7680700,
        size.height * 0.2339678)
    ..cubicTo(
        size.width * 0.7730725,
        size.height * 0.2387801,
        size.width * 0.7732900,
        size.height * 0.2382040,
        size.width * 0.7694000,
        size.height * 0.2304468)
    ..cubicTo(
        size.width * 0.7564800,
        size.height * 0.2046871,
        size.width * 0.7577525,
        size.height * 0.1544341,
        size.width * 0.7718375,
        size.height * 0.1341670)
    ..cubicTo(
        size.width * 0.7737650,
        size.height * 0.1313909,
        size.width * 0.7735675,
        size.height * 0.1312469,
        size.width * 0.7706150,
        size.height * 0.1332780)
    ..moveTo(size.width * 0.7781525, size.height * 0.1333029)
    ..cubicTo(
        size.width * 0.7647700,
        size.height * 0.1427883,
        size.width * 0.7588550,
        size.height * 0.1831934,
        size.width * 0.7664050,
        size.height * 0.2135766)
    ..cubicTo(
        size.width * 0.7708775,
        size.height * 0.2315791,
        size.width * 0.7792900,
        size.height * 0.2408858,
        size.width * 0.7871400,
        size.height * 0.2365205)
    ..lineTo(size.width * 0.7889850, size.height * 0.2354925)
    ..lineTo(size.width * 0.7892700, size.height * 0.2184634)
    ..cubicTo(
        size.width * 0.7894250,
        size.height * 0.2090971,
        size.width * 0.7896725,
        size.height * 0.1964680,
        size.width * 0.7898200,
        size.height * 0.1903944)
    ..lineTo(size.width * 0.7900875, size.height * 0.1793495)
    ..lineTo(size.width * 0.7880075, size.height * 0.1773035)
    ..cubicTo(
        size.width * 0.7817275,
        size.height * 0.1711106,
        size.width * 0.7856200,
        size.height * 0.1625687,
        size.width * 0.7947075,
        size.height * 0.1625985)
    ..lineTo(size.width * 0.7992375, size.height * 0.1626134)
    ..lineTo(size.width * 0.7996875, size.height * 0.1596983)
    ..cubicTo(
        size.width * 0.8000825,
        size.height * 0.1571556,
        size.width * 0.7999925,
        size.height * 0.1562467,
        size.width * 0.7989850,
        size.height * 0.1525469)
    ..cubicTo(
        size.width * 0.7946700,
        size.height * 0.1366998,
        size.width * 0.7854175,
        size.height * 0.1281529,
        size.width * 0.7781525,
        size.height * 0.1333029)
    ..moveTo(size.width * 0.8152275, size.height * 0.1334072)
    ..cubicTo(
        size.width * 0.7944350,
        size.height * 0.1481121,
        size.width * 0.7957775,
        size.height * 0.2281872,
        size.width * 0.8169650,
        size.height * 0.2369178)
    ..lineTo(size.width * 0.8182400, size.height * 0.2374442)
    ..lineTo(size.width * 0.8169650, size.height * 0.2355471)
    ..cubicTo(
        size.width * 0.8026500,
        size.height * 0.2142619,
        size.width * 0.8013550,
        size.height * 0.1644807,
        size.width * 0.8144475,
        size.height * 0.1387260)
    ..cubicTo(
        size.width * 0.8163975,
        size.height * 0.1348921,
        size.width * 0.8178550,
        size.height * 0.1317832,
        size.width * 0.8176900,
        size.height * 0.1318180)
    ..cubicTo(
        size.width * 0.8175250,
        size.height * 0.1318527,
        size.width * 0.8164175,
        size.height * 0.1325679,
        size.width * 0.8152275,
        size.height * 0.1334072)
    ..moveTo(size.width * 0.8232525, size.height * 0.1332830)
    ..cubicTo(
        size.width * 0.8011525,
        size.height * 0.1489564,
        size.width * 0.8041850,
        size.height * 0.2345390,
        size.width * 0.8269325,
        size.height * 0.2371562)
    ..cubicTo(
        size.width * 0.8335600,
        size.height * 0.2379160,
        size.width * 0.8373650,
        size.height * 0.2341268,
        size.width * 0.8349700,
        size.height * 0.2291506)
    ..cubicTo(
        size.width * 0.8317425,
        size.height * 0.2224512,
        size.width * 0.8273800,
        size.height * 0.2047268,
        size.width * 0.8273800,
        size.height * 0.1983155)
    ..cubicTo(
        size.width * 0.8273800,
        size.height * 0.1977692,
        size.width * 0.8269750,
        size.height * 0.1956536,
        size.width * 0.8264775,
        size.height * 0.1936174)
    ..cubicTo(
        size.width * 0.8253175,
        size.height * 0.1888548,
        size.width * 0.8252475,
        size.height * 0.1806308,
        size.width * 0.8263300,
        size.height * 0.1764691)
    ..cubicTo(
        size.width * 0.8267475,
        size.height * 0.1748700,
        size.width * 0.8274450,
        size.height * 0.1707133,
        size.width * 0.8278775,
        size.height * 0.1672270)
    ..cubicTo(
        size.width * 0.8289600,
        size.height * 0.1585709,
        size.width * 0.8314800,
        size.height * 0.1484250,
        size.width * 0.8341125,
        size.height * 0.1421378)
    ..cubicTo(
        size.width * 0.8367675,
        size.height * 0.1358009,
        size.width * 0.8367725,
        size.height * 0.1365855,
        size.width * 0.8340775,
        size.height * 0.1342614)
    ..cubicTo(
        size.width * 0.8309925,
        size.height * 0.1315945,
        size.width * 0.8262350,
        size.height * 0.1311674,
        size.width * 0.8232525,
        size.height * 0.1332830)
    ..moveTo(size.width * 0.8445100, size.height * 0.1334469)
    ..cubicTo(
        size.width * 0.8247375,
        size.height * 0.1474119,
        size.width * 0.8245800,
        size.height * 0.2210061,
        size.width * 0.8442900,
        size.height * 0.2356514)
    ..cubicTo(
        size.width * 0.8474350,
        size.height * 0.2379905,
        size.width * 0.8478100,
        size.height * 0.2374641,
        size.width * 0.8454775,
        size.height * 0.2339927)
    ..cubicTo(
        size.width * 0.8309925,
        size.height * 0.2124344,
        size.width * 0.8314375,
        size.height * 0.1555068,
        size.width * 0.8462575,
        size.height * 0.1341471)
    ..cubicTo(
        size.width * 0.8481575,
        size.height * 0.1314058,
        size.width * 0.8476700,
        size.height * 0.1312121,
        size.width * 0.8445100,
        size.height * 0.1334469)
    ..moveTo(size.width * 0.8525575, size.height * 0.1333029)
    ..cubicTo(
        size.width * 0.8389300,
        size.height * 0.1429621,
        size.width * 0.8331275,
        size.height * 0.1832977,
        size.width * 0.8408800,
        size.height * 0.2144755)
    ..cubicTo(
        size.width * 0.8452675,
        size.height * 0.2321254,
        size.width * 0.8546125,
        size.height * 0.2413923,
        size.width * 0.8624425,
        size.height * 0.2358649)
    ..lineTo(size.width * 0.8645500, size.height * 0.2343751)
    ..lineTo(size.width * 0.8652225, size.height * 0.2266477)
    ..lineTo(size.width * 0.8658950, size.height * 0.2189153)
    ..lineTo(size.width * 0.8636750, size.height * 0.2155035)
    ..cubicTo(
        size.width * 0.8609375,
        size.height * 0.2113021,
        size.width * 0.8581975,
        size.height * 0.2045629,
        size.width * 0.8568325,
        size.height * 0.1986830)
    ..cubicTo(
        size.width * 0.8555550,
        size.height * 0.1931804,
        size.width * 0.8543050,
        size.height * 0.1614762,
        size.width * 0.8552425,
        size.height * 0.1583623)
    ..cubicTo(
        size.width * 0.8564925,
        size.height * 0.1542156,
        size.width * 0.8591800,
        size.height * 0.1576025,
        size.width * 0.8610800,
        size.height * 0.1657173)
    ..cubicTo(
        size.width * 0.8620075,
        size.height * 0.1696754,
        size.width * 0.8620175,
        size.height * 0.1696903,
        size.width * 0.8628775,
        size.height * 0.1680812)
    ..cubicTo(
        size.width * 0.8641225,
        size.height * 0.1657570,
        size.width * 0.8665825,
        size.height * 0.1638997,
        size.width * 0.8702550,
        size.height * 0.1625042)
    ..cubicTo(
        size.width * 0.8720150,
        size.height * 0.1618337,
        size.width * 0.8734900,
        size.height * 0.1611980,
        size.width * 0.8735325,
        size.height * 0.1610888)
    ..cubicTo(
        size.width * 0.8746250,
        size.height * 0.1582928,
        size.width * 0.8745775,
        size.height * 0.1568874,
        size.width * 0.8732375,
        size.height * 0.1522092)
    ..cubicTo(
        size.width * 0.8687150,
        size.height * 0.1364117,
        size.width * 0.8597425,
        size.height * 0.1282125,
        size.width * 0.8525575,
        size.height * 0.1333029)
    ..moveTo(size.width * 0.8894825, size.height * 0.1334717)
    ..cubicTo(
        size.width * 0.8688550,
        size.height * 0.1482065,
        size.width * 0.8701450,
        size.height * 0.2279041,
        size.width * 0.8911575,
        size.height * 0.2369128)
    ..lineTo(size.width * 0.8926450, size.height * 0.2375534)
    ..lineTo(size.width * 0.8903925, size.height * 0.2340125)
    ..cubicTo(
        size.width * 0.8762850,
        size.height * 0.2118335,
        size.width * 0.8762475,
        size.height * 0.1587745,
        size.width * 0.8903250,
        size.height * 0.1355426)
    ..cubicTo(
        size.width * 0.8913675,
        size.height * 0.1338243,
        size.width * 0.8923000,
        size.height * 0.1322699,
        size.width * 0.8923975,
        size.height * 0.1320862)
    ..cubicTo(
        size.width * 0.8927775,
        size.height * 0.1313760,
        size.width * 0.8916925,
        size.height * 0.1318925,
        size.width * 0.8894825,
        size.height * 0.1334717)
    ..moveTo(size.width * 0.8981725, size.height * 0.1328907)
    ..cubicTo(
        size.width * 0.8748525,
        size.height * 0.1482660,
        size.width * 0.8784675,
        size.height * 0.2369078,
        size.width * 0.9024150,
        size.height * 0.2369078)
    ..cubicTo(
        size.width * 0.9246350,
        size.height * 0.2369078,
        size.width * 0.9301025,
        size.height * 0.1554869,
        size.width * 0.9092500,
        size.height * 0.1350957)
    ..cubicTo(
        size.width * 0.9059475,
        size.height * 0.1318676,
        size.width * 0.9011700,
        size.height * 0.1309141,
        size.width * 0.8981725,
        size.height * 0.1328907)
    ..moveTo(size.width * 0.8575825, size.height * 0.1661047)
    ..cubicTo(
        size.width * 0.8573875,
        size.height * 0.1923362,
        size.width * 0.8597775,
        size.height * 0.2041160,
        size.width * 0.8675050,
        size.height * 0.2150416)
    ..lineTo(size.width * 0.8687275, size.height * 0.2167699)
    ..lineTo(size.width * 0.8659150, size.height * 0.2492191)
    ..cubicTo(
        size.width * 0.8522475,
        size.height * 0.4069507,
        size.width * 0.8503125,
        size.height * 0.4442966,
        size.width * 0.8535575,
        size.height * 0.4877509)
    ..cubicTo(
        size.width * 0.8575850,
        size.height * 0.5416590,
        size.width * 0.8695800,
        size.height * 0.5447579,
        size.width * 0.8773050,
        size.height * 0.4938792)
    ..cubicTo(
        size.width * 0.8851875,
        size.height * 0.4419624,
        size.width * 0.8858350,
        size.height * 0.4010260,
        size.width * 0.8811225,
        size.height * 0.2521293)
    ..lineTo(size.width * 0.8804275, size.height * 0.2301836)
    ..lineTo(size.width * 0.8788300, size.height * 0.2265781)
    ..cubicTo(
        size.width * 0.8731025,
        size.height * 0.2136561,
        size.width * 0.8700775,
        size.height * 0.1850905,
        size.width * 0.8726975,
        size.height * 0.1686921)
    ..cubicTo(
        size.width * 0.8729525,
        size.height * 0.1670929,
        size.width * 0.8728425,
        size.height * 0.1670085,
        size.width * 0.8710700,
        size.height * 0.1674902)
    ..cubicTo(
        size.width * 0.8668075,
        size.height * 0.1686523,
        size.width * 0.8646025,
        size.height * 0.1710808,
        size.width * 0.8627600,
        size.height * 0.1766578)
    ..lineTo(size.width * 0.8610550, size.height * 0.1818078)
    ..lineTo(size.width * 0.8599675, size.height * 0.1747310)
    ..cubicTo(
        size.width * 0.8587775,
        size.height * 0.1669737,
        size.width * 0.8576075,
        size.height * 0.1627425,
        size.width * 0.8575825,
        size.height * 0.1661047)
    ..moveTo(size.width * 0.7910300, size.height * 0.1680117)
    ..cubicTo(
        size.width * 0.7870350,
        size.height * 0.1699286,
        size.width * 0.7872300,
        size.height * 0.1722429,
        size.width * 0.7915375,
        size.height * 0.1740208)
    ..cubicTo(
        size.width * 0.7954575,
        size.height * 0.1756348,
        size.width * 0.7980450,
        size.height * 0.1744578,
        size.width * 0.7980450,
        size.height * 0.1710560)
    ..cubicTo(
        size.width * 0.7980450,
        size.height * 0.1674008,
        size.width * 0.7950300,
        size.height * 0.1660947,
        size.width * 0.7910300,
        size.height * 0.1680117)
    ..moveTo(size.width * 0.1817275, size.height * 0.1710510)
    ..cubicTo(
        size.width * 0.1777175,
        size.height * 0.1729084,
        size.width * 0.1750025,
        size.height * 0.1793744,
        size.width * 0.1734900,
        size.height * 0.1906725)
    ..lineTo(size.width * 0.1730100, size.height * 0.1942630)
    ..lineTo(size.width * 0.1717975, size.height * 0.1930165)
    ..cubicTo(
        size.width * 0.1684675,
        size.height * 0.1895998,
        size.width * 0.1650550,
        size.height * 0.1909506,
        size.width * 0.1641625,
        size.height * 0.1960459)
    ..cubicTo(
        size.width * 0.1629800,
        size.height * 0.2027801,
        size.width * 0.1641925,
        size.height * 0.2145003,
        size.width * 0.1662025,
        size.height * 0.2157667)
    ..cubicTo(
        size.width * 0.1683325,
        size.height * 0.2171125,
        size.width * 0.1631525,
        size.height * 0.2278147,
        size.width * 0.1568550,
        size.height * 0.2350753)
    ..cubicTo(
        size.width * 0.1557850,
        size.height * 0.2363069,
        size.width * 0.1542975,
        size.height * 0.2390135,
        size.width * 0.1535475,
        size.height * 0.2410894)
    ..lineTo(size.width * 0.1521825, size.height * 0.2448637)
    ..lineTo(size.width * 0.1537400, size.height * 0.2510218)
    ..cubicTo(
        size.width * 0.1571500,
        size.height * 0.2645249,
        size.width * 0.1595825,
        size.height * 0.2705241,
        size.width * 0.1633900,
        size.height * 0.2748347)
    ..cubicTo(
        size.width * 0.1661000,
        size.height * 0.2778989,
        size.width * 0.1657075,
        size.height * 0.2786289,
        size.width * 0.1706350,
        size.height * 0.2613813)
    ..lineTo(size.width * 0.1749750, size.height * 0.2461996)
    ..lineTo(size.width * 0.1785800, size.height * 0.2461301)
    ..cubicTo(
        size.width * 0.1931525,
        size.height * 0.2458569,
        size.width * 0.1959050,
        size.height * 0.2191040,
        size.width * 0.1856625,
        size.height * 0.1772985)
    ..lineTo(size.width * 0.1838850, size.height * 0.1700528)
    ..lineTo(size.width * 0.1817275, size.height * 0.1710510)
    ..moveTo(size.width * 0.7946100, size.height * 0.1796674)
    ..lineTo(size.width * 0.7926650, size.height * 0.1799802)
    ..lineTo(size.width * 0.7921375, size.height * 0.2057846)
    ..cubicTo(
        size.width * 0.7918475,
        size.height * 0.2199780,
        size.width * 0.7917100,
        size.height * 0.2317877,
        size.width * 0.7918300,
        size.height * 0.2320261)
    ..cubicTo(
        size.width * 0.7922650,
        size.height * 0.2328902,
        size.width * 0.7972675,
        size.height * 0.2225257,
        size.width * 0.7987425,
        size.height * 0.2177085)
    ..cubicTo(
        size.width * 0.8002275,
        size.height * 0.2128515,
        size.width * 0.8002400,
        size.height * 0.2127274,
        size.width * 0.7996050,
        size.height * 0.2095689)
    ..cubicTo(
        size.width * 0.7984950,
        size.height * 0.2040614,
        size.width * 0.7976200,
        size.height * 0.1937813,
        size.width * 0.7976200,
        size.height * 0.1862675)
    ..cubicTo(
        size.width * 0.7976200,
        size.height * 0.1809784,
        size.width * 0.7974775,
        size.height * 0.1790963,
        size.width * 0.7970875,
        size.height * 0.1792055)
    ..cubicTo(
        size.width * 0.7967950,
        size.height * 0.1792899,
        size.width * 0.7956800,
        size.height * 0.1794985,
        size.width * 0.7946100,
        size.height * 0.1796674)
    ..moveTo(size.width * 0.4618775, size.height * 0.1938210)
    ..cubicTo(
        size.width * 0.4738650,
        size.height * 0.1988667,
        size.width * 0.4782625,
        size.height * 0.2150963,
        size.width * 0.4770225,
        size.height * 0.2497058)
    ..cubicTo(
        size.width * 0.4766900,
        size.height * 0.2589379,
        size.width * 0.4767300,
        size.height * 0.2613167,
        size.width * 0.4772575,
        size.height * 0.2638247)
    ..cubicTo(
        size.width * 0.4785000,
        size.height * 0.2697345,
        size.width * 0.4776275,
        size.height * 0.2756442,
        size.width * 0.4745675,
        size.height * 0.2820357)
    ..lineTo(size.width * 0.4728350, size.height * 0.2856511)
    ..lineTo(size.width * 0.4770175, size.height * 0.2980716)
    ..lineTo(size.width * 0.4812000, size.height * 0.3104971)
    ..lineTo(size.width * 0.4836925, size.height * 0.3098614)
    ..cubicTo(
        size.width * 0.4979975,
        size.height * 0.3062162,
        size.width * 0.5093925,
        size.height * 0.3083467,
        size.width * 0.5225175,
        size.height * 0.3171220)
    ..cubicTo(
        size.width * 0.5275250,
        size.height * 0.3204692,
        size.width * 0.5286925,
        size.height * 0.3209460,
        size.width * 0.5318700,
        size.height * 0.3209509)
    ..cubicTo(
        size.width * 0.5615425,
        size.height * 0.3209857,
        size.width * 0.5867500,
        size.height * 0.3386902,
        size.width * 0.5992450,
        size.height * 0.3682789)
    ..cubicTo(
        size.width * 0.6122000,
        size.height * 0.3989501,
        size.width * 0.6150675,
        size.height * 0.4211441,
        size.width * 0.6117875,
        size.height * 0.4653682)
    ..cubicTo(
        size.width * 0.6081750,
        size.height * 0.5140469,
        size.width * 0.6117500,
        size.height * 0.5394441,
        size.width * 0.6266425,
        size.height * 0.5709447)
    ..cubicTo(
        size.width * 0.6296550,
        size.height * 0.5773164,
        size.width * 0.6299650,
        size.height * 0.5773909,
        size.width * 0.6292875,
        size.height * 0.5715804)
    ..lineTo(size.width * 0.6289150, size.height * 0.5684120)
    ..lineTo(size.width * 0.6361875, size.height * 0.5684120)
    ..lineTo(size.width * 0.6335775, size.height * 0.5650647)
    ..lineTo(size.width * 0.6309700, size.height * 0.5617125)
    ..lineTo(size.width * 0.6327050, size.height * 0.5553310)
    ..cubicTo(
        size.width * 0.6353575,
        size.height * 0.5455575,
        size.width * 0.6344325,
        size.height * 0.5437547,
        size.width * 0.6303150,
        size.height * 0.5506727)
    ..cubicTo(
        size.width * 0.6289325,
        size.height * 0.5529969,
        size.width * 0.6276450,
        size.height * 0.5548989,
        size.width * 0.6274575,
        size.height * 0.5548989)
    ..cubicTo(
        size.width * 0.6264100,
        size.height * 0.5548989,
        size.width * 0.6210350,
        size.height * 0.5400351,
        size.width * 0.6186450,
        size.height * 0.5305446)
    ..cubicTo(
        size.width * 0.6161900,
        size.height * 0.5207910,
        size.width * 0.6161650,
        size.height * 0.5214714,
        size.width * 0.6190850,
        size.height * 0.5185413)
    ..cubicTo(
        size.width * 0.6221275,
        size.height * 0.5154921,
        size.width * 0.6223200,
        size.height * 0.5135304,
        size.width * 0.6196825,
        size.height * 0.5124975)
    ..cubicTo(
        size.width * 0.6155800,
        size.height * 0.5108884,
        size.width * 0.6159275,
        size.height * 0.5117327,
        size.width * 0.6161825,
        size.height * 0.5039655)
    ..cubicTo(
        size.width * 0.6163975,
        size.height * 0.4973853,
        size.width * 0.6186000,
        size.height * 0.4812203,
        size.width * 0.6194175,
        size.height * 0.4802171)
    ..cubicTo(
        size.width * 0.6201600,
        size.height * 0.4793083,
        size.width * 0.6339400,
        size.height * 0.5011298,
        size.width * 0.6353825,
        size.height * 0.5055050)
    ..cubicTo(
        size.width * 0.6360150,
        size.height * 0.5074220,
        size.width * 0.6359875,
        size.height * 0.5078342,
        size.width * 0.6349275,
        size.height * 0.5121250)
    ..cubicTo(
        size.width * 0.6330675,
        size.height * 0.5196637,
        size.width * 0.6338800,
        size.height * 0.5210791,
        size.width * 0.6373850,
        size.height * 0.5163959)
    ..lineTo(size.width * 0.6396525, size.height * 0.5133665)
    ..lineTo(size.width * 0.6413950, size.height * 0.5150451)
    ..lineTo(size.width * 0.6431375, size.height * 0.5167287)
    ..lineTo(size.width * 0.6417500, size.height * 0.5097809)
    ..cubicTo(
        size.width * 0.6394800,
        size.height * 0.4984282,
        size.width * 0.6394025,
        size.height * 0.4992873,
        size.width * 0.6430675,
        size.height * 0.4949767)
    ..cubicTo(
        size.width * 0.6468800,
        size.height * 0.4904872,
        size.width * 0.6477075,
        size.height * 0.4866682,
        size.width * 0.6446650,
        size.height * 0.4876019)
    ..cubicTo(
        size.width * 0.6387550,
        size.height * 0.4894195,
        size.width * 0.6386600,
        size.height * 0.4894096,
        size.width * 0.6381850,
        size.height * 0.4869314)
    ..cubicTo(
        size.width * 0.6374675,
        size.height * 0.4831819,
        size.width * 0.6356300,
        size.height * 0.4622146,
        size.width * 0.6356300,
        size.height * 0.4577798)
    ..lineTo(size.width * 0.6356300, size.height * 0.4536827)
    ..lineTo(size.width * 0.6385000, size.height * 0.4518253)
    ..cubicTo(
        size.width * 0.6435200,
        size.height * 0.4485725,
        size.width * 0.6436200,
        size.height * 0.4460546,
        size.width * 0.6387975,
        size.height * 0.4442469)
    ..cubicTo(
        size.width * 0.6372825,
        size.height * 0.4436808,
        size.width * 0.6359500,
        size.height * 0.4429160,
        size.width * 0.6358375,
        size.height * 0.4425534)
    ..cubicTo(
        size.width * 0.6351700,
        size.height * 0.4404080,
        size.width * 0.6392950,
        size.height * 0.4162077,
        size.width * 0.6424975,
        size.height * 0.4034893)
    ..lineTo(size.width * 0.6441500, size.height * 0.3969289)
    ..lineTo(size.width * 0.6474350, size.height * 0.4012992)
    ..arcToPoint(Offset(size.width * 0.6523125, size.height * 0.4081327),
        radius:
            Radius.elliptical(size.width * 0.1124200, size.height * 0.2233203),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..lineTo(size.width * 0.6539000, size.height * 0.4105959)
    ..lineTo(size.width * 0.6517800, size.height * 0.4158005)
    ..cubicTo(
        size.width * 0.6494700,
        size.height * 0.4214669,
        size.width * 0.6495025,
        size.height * 0.4213378,
        size.width * 0.6500525,
        size.height * 0.4230660)
    ..cubicTo(
        size.width * 0.6505175,
        size.height * 0.4245261,
        size.width * 0.6522625,
        size.height * 0.4233342,
        size.width * 0.6551025,
        size.height * 0.4196195)
    ..cubicTo(
        size.width * 0.6576150,
        size.height * 0.4163319,
        size.width * 0.6578250,
        size.height * 0.4164610,
        size.width * 0.6605225,
        size.height * 0.4229319)
    ..cubicTo(
        size.width * 0.6630650,
        size.height * 0.4290205,
        size.width * 0.6656200,
        size.height * 0.4386450,
        size.width * 0.6671100,
        size.height * 0.4477332)
    ..lineTo(size.width * 0.6680250, size.height * 0.4533251)
    ..lineTo(size.width * 0.6659650, size.height * 0.4570945)
    ..cubicTo(
        size.width * 0.6648300,
        size.height * 0.4591654,
        size.width * 0.6631550,
        size.height * 0.4621948,
        size.width * 0.6622425,
        size.height * 0.4638237)
    ..cubicTo(
        size.width * 0.6598875,
        size.height * 0.4680201,
        size.width * 0.6604000,
        size.height * 0.4691524,
        size.width * 0.6642825,
        size.height * 0.4683280)
    ..cubicTo(
        size.width * 0.6697375,
        size.height * 0.4671659,
        size.width * 0.6693650,
        size.height * 0.4669375,
        size.width * 0.6697325,
        size.height * 0.4717050)
    ..cubicTo(
        size.width * 0.6699125,
        size.height * 0.4740292,
        size.width * 0.6701100,
        size.height * 0.4799043,
        size.width * 0.6701700,
        size.height * 0.4847612)
    ..lineTo(size.width * 0.6702800, size.height * 0.4935911)
    ..lineTo(size.width * 0.6678775, size.height * 0.4958954)
    ..cubicTo(
        size.width * 0.6640450,
        size.height * 0.4995754,
        size.width * 0.6641900,
        size.height * 0.5021280,
        size.width * 0.6682875,
        size.height * 0.5030915)
    ..cubicTo(
        size.width * 0.6704875,
        size.height * 0.5036129,
        size.width * 0.6704925,
        size.height * 0.5036228,
        size.width * 0.6705025,
        size.height * 0.5066622)
    ..cubicTo(
        size.width * 0.6705100,
        size.height * 0.5094532,
        size.width * 0.6705950,
        size.height * 0.5092347,
        size.width * 0.6715075,
        size.height * 0.5040996)
    ..cubicTo(
        size.width * 0.6738200,
        size.height * 0.4910832,
        size.width * 0.6757250,
        size.height * 0.4865441,
        size.width * 0.6778300,
        size.height * 0.4890073)
    ..cubicTo(
        size.width * 0.6804300,
        size.height * 0.4920566,
        size.width * 0.6811250,
        size.height * 0.4904177,
        size.width * 0.6801000,
        size.height * 0.4836488)
    ..lineTo(size.width * 0.6795000, size.height * 0.4797006)
    ..lineTo(size.width * 0.6822325, size.height * 0.4748586)
    ..cubicTo(
        size.width * 0.6837325,
        size.height * 0.4721967,
        size.width * 0.6862375,
        size.height * 0.4686210,
        size.width * 0.6877950,
        size.height * 0.4669127)
    ..lineTo(size.width * 0.6906275, size.height * 0.4638088)
    ..lineTo(size.width * 0.6924325, size.height * 0.4662770)
    ..cubicTo(
        size.width * 0.6950950,
        size.height * 0.4699122,
        size.width * 0.6962350,
        size.height * 0.4686508,
        size.width * 0.6949050,
        size.height * 0.4635406)
    ..cubicTo(
        size.width * 0.6945325,
        size.height * 0.4621203,
        size.width * 0.6943900,
        size.height * 0.4606552,
        size.width * 0.6945850,
        size.height * 0.4602877)
    ..cubicTo(
        size.width * 0.6949500,
        size.height * 0.4596074,
        size.width * 0.7014025,
        size.height * 0.4542339,
        size.width * 0.7031250,
        size.height * 0.4531761)
    ..lineTo(size.width * 0.7040825, size.height * 0.4525901)
    ..lineTo(size.width * 0.7040825, size.height * 0.4967844)
    ..lineTo(size.width * 0.7015600, size.height * 0.4962033)
    ..cubicTo(
        size.width * 0.6965925,
        size.height * 0.4950611,
        size.width * 0.6957600,
        size.height * 0.4983934,
        size.width * 0.6998300,
        size.height * 0.5031362)
    ..lineTo(size.width * 0.7027450, size.height * 0.5065380)
    ..cubicTo(
        size.width * 0.7034275,
        size.height * 0.5073326,
        size.width * 0.7011175,
        size.height * 0.5219183,
        size.width * 0.6988350,
        size.height * 0.5312498)
    ..lineTo(size.width * 0.6974925, size.height * 0.5367375)
    ..lineTo(size.width * 0.6942550, size.height * 0.5364594)
    ..cubicTo(
        size.width * 0.6899225,
        size.height * 0.5360820,
        size.width * 0.6895800,
        size.height * 0.5374477,
        size.width * 0.6925750,
        size.height * 0.5431787)
    ..lineTo(size.width * 0.6946975, size.height * 0.5472460)
    ..lineTo(size.width * 0.6925875, size.height * 0.5535133)
    ..cubicTo(
        size.width * 0.6914250,
        size.height * 0.5569599,
        size.width * 0.6895975,
        size.height * 0.5617225,
        size.width * 0.6885225,
        size.height * 0.5640963)
    ..lineTo(size.width * 0.6865700, size.height * 0.5684120)
    ..lineTo(size.width * 0.6891625, size.height * 0.5684120)
    ..cubicTo(
        size.width * 0.6923200,
        size.height * 0.5684120,
        size.width * 0.6923675,
        size.height * 0.5649555,
        size.width * 0.6887625,
        size.height * 0.5974990)
    ..cubicTo(
        size.width * 0.6847550,
        size.height * 0.6336629,
        size.width * 0.6830675,
        size.height * 0.6399154,
        size.width * 0.6752375,
        size.height * 0.6476080)
    ..lineTo(size.width * 0.6720300, size.height * 0.6507566)
    ..lineTo(size.width * 0.6644225, size.height * 0.6510397)
    ..lineTo(size.width * 0.6568125, size.height * 0.6513178)
    ..lineTo(size.width * 0.6571025, size.height * 0.6535724)
    ..cubicTo(
        size.width * 0.6680625,
        size.height * 0.7387578,
        size.width * 0.6171200,
        size.height * 0.7972447,
        size.width * 0.5238525,
        size.height * 0.8065663)
    ..cubicTo(
        size.width * 0.4401725,
        size.height * 0.8149244,
        size.width * 0.3697675,
        size.height * 0.7820184,
        size.width * 0.3507975,
        size.height * 0.7256768)
    ..cubicTo(
        size.width * 0.3437925,
        size.height * 0.7048783,
        size.width * 0.3415000,
        size.height * 0.6732088,
        size.width * 0.3454975,
        size.height * 0.6525146)
    ..cubicTo(
        size.width * 0.3456950,
        size.height * 0.6514916,
        size.width * 0.3443425,
        size.height * 0.6512632,
        size.width * 0.3368500,
        size.height * 0.6510297)
    ..lineTo(size.width * 0.3279700, size.height * 0.6507566)
    ..lineTo(size.width * 0.3247450, size.height * 0.6475882)
    ..cubicTo(
        size.width * 0.3168825,
        size.height * 0.6398657,
        size.width * 0.3152750,
        size.height * 0.6339013,
        size.width * 0.3112450,
        size.height * 0.5975487)
    ..cubicTo(
        size.width * 0.3076300,
        size.height * 0.5649555,
        size.width * 0.3076775,
        size.height * 0.5684120,
        size.width * 0.3108375,
        size.height * 0.5684120)
    ..lineTo(size.width * 0.3134300, size.height * 0.5684120)
    ..lineTo(size.width * 0.3114200, size.height * 0.5639771)
    ..cubicTo(
        size.width * 0.3103150,
        size.height * 0.5615387,
        size.width * 0.3084650,
        size.height * 0.5567662,
        size.width * 0.3073100,
        size.height * 0.5533693)
    ..lineTo(size.width * 0.3052100, size.height * 0.5471914)
    ..lineTo(size.width * 0.3071550, size.height * 0.5437945)
    ..cubicTo(
        size.width * 0.3103625,
        size.height * 0.5381876,
        size.width * 0.3099875,
        size.height * 0.5361465,
        size.width * 0.3058025,
        size.height * 0.5364346)
    ..lineTo(size.width * 0.3025075, size.height * 0.5366580)
    ..lineTo(size.width * 0.3011600, size.height * 0.5312101)
    ..cubicTo(
        size.width * 0.2989025,
        size.height * 0.5221021,
        size.width * 0.2965675,
        size.height * 0.5073376,
        size.width * 0.2972550,
        size.height * 0.5065380)
    ..lineTo(size.width * 0.3001700, size.height * 0.5031362)
    ..cubicTo(
        size.width * 0.3042400,
        size.height * 0.4983934,
        size.width * 0.3034075,
        size.height * 0.4950611,
        size.width * 0.2984400,
        size.height * 0.4962033)
    ..lineTo(size.width * 0.2959175, size.height * 0.4967844)
    ..lineTo(size.width * 0.2959175, size.height * 0.4747444)
    ..cubicTo(
        size.width * 0.2959175,
        size.height * 0.4626219,
        size.width * 0.2960250,
        size.height * 0.4526994,
        size.width * 0.2961575,
        size.height * 0.4526994)
    ..cubicTo(
        size.width * 0.2965900,
        size.height * 0.4526994,
        size.width * 0.3050500,
        size.height * 0.4596620,
        size.width * 0.3054150,
        size.height * 0.4603175)
    ..cubicTo(
        size.width * 0.3056100,
        size.height * 0.4606652,
        size.width * 0.3054500,
        size.height * 0.4621848,
        size.width * 0.3050575,
        size.height * 0.4636896)
    ..cubicTo(
        size.width * 0.3038350,
        size.height * 0.4683827,
        size.width * 0.3050475,
        size.height * 0.4697186,
        size.width * 0.3074950,
        size.height * 0.4663713)
    ..lineTo(size.width * 0.3093775, size.height * 0.4637988)
    ..lineTo(size.width * 0.3121875, size.height * 0.4669077)
    ..cubicTo(
        size.width * 0.3137325,
        size.height * 0.4686161,
        size.width * 0.3162300,
        size.height * 0.4721868,
        size.width * 0.3177375,
        size.height * 0.4748338)
    ..lineTo(size.width * 0.3204750, size.height * 0.4796559)
    ..lineTo(size.width * 0.3198150, size.height * 0.4843589)
    ..cubicTo(
        size.width * 0.3189275,
        size.height * 0.4906958,
        size.width * 0.3194700,
        size.height * 0.4919473,
        size.width * 0.3219000,
        size.height * 0.4891712)
    ..cubicTo(
        size.width * 0.3242700,
        size.height * 0.4864596,
        size.width * 0.3260900,
        size.height * 0.4905866,
        size.width * 0.3284925,
        size.height * 0.5040996)
    ..cubicTo(
        size.width * 0.3294050,
        size.height * 0.5092347,
        size.width * 0.3294900,
        size.height * 0.5094532,
        size.width * 0.3294975,
        size.height * 0.5066622)
    ..cubicTo(
        size.width * 0.3295075,
        size.height * 0.5036228,
        size.width * 0.3295125,
        size.height * 0.5036129,
        size.width * 0.3317125,
        size.height * 0.5030915)
    ..cubicTo(
        size.width * 0.3358500,
        size.height * 0.5021181,
        size.width * 0.3359525,
        size.height * 0.4996846,
        size.width * 0.3320275,
        size.height * 0.4957464)
    ..lineTo(size.width * 0.3295075, size.height * 0.4932186)
    ..lineTo(size.width * 0.3295050, size.height * 0.4892159)
    ..cubicTo(
        size.width * 0.3295050,
        size.height * 0.4870208,
        size.width * 0.3297300,
        size.height * 0.4810961,
        size.width * 0.3300050,
        size.height * 0.4760505)
    ..lineTo(size.width * 0.3305050, size.height * 0.4668878)
    ..lineTo(size.width * 0.3320250, size.height * 0.4674142)
    ..cubicTo(
        size.width * 0.3392525,
        size.height * 0.4699122,
        size.width * 0.3410550,
        size.height * 0.4683231,
        size.width * 0.3373475,
        size.height * 0.4627212)
    ..arcToPoint(Offset(size.width * 0.3339225, size.height * 0.4569256),
        radius:
            Radius.elliptical(size.width * 0.05825000, size.height * 0.1157126),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..lineTo(size.width * 0.3319700, size.height * 0.4533500)
    ..lineTo(size.width * 0.3328875, size.height * 0.4477481)
    ..cubicTo(
        size.width * 0.3344325,
        size.height * 0.4383222,
        size.width * 0.3369650,
        size.height * 0.4289311,
        size.width * 0.3397025,
        size.height * 0.4224999)
    ..lineTo(size.width * 0.3423100, size.height * 0.4163666)
    ..lineTo(size.width * 0.3442050, size.height * 0.4189590)
    ..cubicTo(
        size.width * 0.3470350,
        size.height * 0.4228277,
        size.width * 0.3490050,
        size.height * 0.4242927,
        size.width * 0.3497400,
        size.height * 0.4230809)
    ..cubicTo(
        size.width * 0.3505925,
        size.height * 0.4216755,
        size.width * 0.3505700,
        size.height * 0.4215662,
        size.width * 0.3482150,
        size.height * 0.4157856)
    ..lineTo(size.width * 0.3460900, size.height * 0.4105711)
    ..lineTo(size.width * 0.3485325, size.height * 0.4068414)
    ..arcToPoint(Offset(size.width * 0.3534400, size.height * 0.3999831),
        radius:
            Radius.elliptical(size.width * 0.08290000, size.height * 0.1646794),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..lineTo(size.width * 0.3559025, size.height * 0.3968594)
    ..lineTo(size.width * 0.3581775, size.height * 0.4064094)
    ..cubicTo(
        size.width * 0.3615100,
        size.height * 0.4203992,
        size.width * 0.3649225,
        size.height * 0.4417936,
        size.width * 0.3640025,
        size.height * 0.4429209)
    ..cubicTo(
        size.width * 0.3638525,
        size.height * 0.4430997,
        size.width * 0.3625400,
        size.height * 0.4437105,
        size.width * 0.3610825,
        size.height * 0.4442817)
    ..cubicTo(
        size.width * 0.3563625,
        size.height * 0.4461241,
        size.width * 0.3565200,
        size.height * 0.4485973,
        size.width * 0.3615600,
        size.height * 0.4518651)
    ..lineTo(size.width * 0.3644900, size.height * 0.4537621)
    ..lineTo(size.width * 0.3642425, size.height * 0.4606205)
    ..cubicTo(
        size.width * 0.3639300,
        size.height * 0.4693362,
        size.width * 0.3620125,
        size.height * 0.4873933,
        size.width * 0.3613000,
        size.height * 0.4883567)
    ..cubicTo(
        size.width * 0.3607750,
        size.height * 0.4890669,
        size.width * 0.3593625,
        size.height * 0.4889030,
        size.width * 0.3555475,
        size.height * 0.4876863)
    ..cubicTo(
        size.width * 0.3517775,
        size.height * 0.4864894,
        size.width * 0.3547425,
        size.height * 0.4944254,
        size.width * 0.3593750,
        size.height * 0.4979266)
    ..cubicTo(
        size.width * 0.3603825,
        size.height * 0.4986914,
        size.width * 0.3603375,
        size.height * 0.4992079,
        size.width * 0.3584600,
        size.height * 0.5086188)
    ..lineTo(size.width * 0.3568225, size.height * 0.5168181)
    ..lineTo(size.width * 0.3585150, size.height * 0.5151047)
    ..lineTo(size.width * 0.3602075, size.height * 0.5133914)
    ..lineTo(size.width * 0.3625400, size.height * 0.5164655)
    ..cubicTo(
        size.width * 0.3660975,
        size.height * 0.5211585,
        size.width * 0.3669625,
        size.height * 0.5194849,
        size.width * 0.3649850,
        size.height * 0.5117277)
    ..lineTo(size.width * 0.3638975, size.height * 0.5074667)
    ..lineTo(size.width * 0.3649500, size.height * 0.5047253)
    ..cubicTo(
        size.width * 0.3672375,
        size.height * 0.4987659,
        size.width * 0.3802250,
        size.height * 0.4789160,
        size.width * 0.3807450,
        size.height * 0.4805846)
    ..cubicTo(
        size.width * 0.3808975,
        size.height * 0.4810763,
        size.width * 0.3815575,
        size.height * 0.4845973,
        size.width * 0.3822125,
        size.height * 0.4884163)
    ..cubicTo(
        size.width * 0.3840950,
        size.height * 0.4994016,
        size.width * 0.3842950,
        size.height * 0.4942268,
        size.width * 0.3828500,
        size.height * 0.4717895)
    ..cubicTo(
        size.width * 0.3808675,
        size.height * 0.4409791,
        size.width * 0.3812550,
        size.height * 0.4252810,
        size.width * 0.3843850,
        size.height * 0.4097665)
    ..cubicTo(
        size.width * 0.3908350,
        size.height * 0.3777643,
        size.width * 0.4140075,
        size.height * 0.3456528,
        size.width * 0.4399575,
        size.height * 0.3327506)
    ..lineTo(size.width * 0.4417775, size.height * 0.3318468)
    ..lineTo(size.width * 0.4403250, size.height * 0.3282959)
    ..cubicTo(
        size.width * 0.4379725,
        size.height * 0.3225451,
        size.width * 0.4340850,
        size.height * 0.3102239,
        size.width * 0.4314775,
        size.height * 0.3002617)
    ..lineTo(size.width * 0.4290475, size.height * 0.2909848)
    ..lineTo(size.width * 0.4289150, size.height * 0.2956183)
    ..cubicTo(
        size.width * 0.4287500,
        size.height * 0.3014089,
        size.width * 0.4276675,
        size.height * 0.3020595,
        size.width * 0.4240500,
        size.height * 0.2985434)
    ..cubicTo(
        size.width * 0.4119400,
        size.height * 0.2867735,
        size.width * 0.4096900,
        size.height * 0.2567528,
        size.width * 0.4199300,
        size.height * 0.2435675)
    ..cubicTo(
        size.width * 0.4219925,
        size.height * 0.2409156,
        size.width * 0.4221600,
        size.height * 0.2404338,
        size.width * 0.4226175,
        size.height * 0.2358649)
    ..cubicTo(
        size.width * 0.4251125,
        size.height * 0.2108601,
        size.width * 0.4280550,
        size.height * 0.2000536,
        size.width * 0.4331700,
        size.height * 0.1970987)
    ..cubicTo(
        size.width * 0.4380725,
        size.height * 0.1942680,
        size.width * 0.4422275,
        size.height * 0.1967710,
        size.width * 0.4459050,
        size.height * 0.2047616)
    ..lineTo(size.width * 0.4480625, size.height * 0.2094497)
    ..lineTo(size.width * 0.4487800, size.height * 0.2051539)
    ..cubicTo(
        size.width * 0.4506450,
        size.height * 0.1940048,
        size.width * 0.4544650,
        size.height * 0.1907023,
        size.width * 0.4618775,
        size.height * 0.1938210)
    ..moveTo(size.width * 0.08439750, size.height * 0.2154439)
    ..cubicTo(
        size.width * 0.08058000,
        size.height * 0.2180114,
        size.width * 0.07414750,
        size.height * 0.2239560,
        size.width * 0.07334250,
        size.height * 0.2256644)
    ..cubicTo(
        size.width * 0.07304000,
        size.height * 0.2263050,
        size.width * 0.07602000,
        size.height * 0.2264143,
        size.width * 0.08354500,
        size.height * 0.2260368)
    ..arcToPoint(Offset(size.width * 0.1011900, size.height * 0.2254955),
        radius:
            Radius.elliptical(size.width * 0.9433500, size.height * 1.873948),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..lineTo(size.width * 0.1082050, size.height * 0.2254856)
    ..lineTo(size.width * 0.1062925, size.height * 0.2232756)
    ..cubicTo(
        size.width * 0.1031300,
        size.height * 0.2196304,
        size.width * 0.09992750,
        size.height * 0.2169834,
        size.width * 0.1001650,
        size.height * 0.2182200)
    ..cubicTo(
        size.width * 0.1008825,
        size.height * 0.2219347,
        size.width * 0.09891250,
        size.height * 0.2215672,
        size.width * 0.09720500,
        size.height * 0.2176688)
    ..cubicTo(
        size.width * 0.09500500,
        size.height * 0.2126529,
        size.width * 0.09474250,
        size.height * 0.2126827,
        size.width * 0.09315000,
        size.height * 0.2181157)
    ..cubicTo(
        size.width * 0.09106500,
        size.height * 0.2252373,
        size.width * 0.09121000,
        size.height * 0.2249592,
        size.width * 0.09016500,
        size.height * 0.2238467)
    ..lineTo(size.width * 0.08923750, size.height * 0.2228584)
    ..lineTo(size.width * 0.09053750, size.height * 0.2182846)
    ..cubicTo(
        size.width * 0.09125250,
        size.height * 0.2157667,
        size.width * 0.09183750,
        size.height * 0.2135121,
        size.width * 0.09183750,
        size.height * 0.2132737)
    ..cubicTo(
        size.width * 0.09183750,
        size.height * 0.2119974,
        size.width * 0.08774500,
        size.height * 0.2131942,
        size.width * 0.08439750,
        size.height * 0.2154439)
    ..moveTo(size.width * 0.7999950, size.height * 0.2233402)
    ..cubicTo(
        size.width * 0.7982375,
        size.height * 0.2284107,
        size.width * 0.7951525,
        size.height * 0.2344545,
        size.width * 0.7930975,
        size.height * 0.2368631)
    ..lineTo(size.width * 0.7917650, size.height * 0.2384225)
    ..lineTo(size.width * 0.7914400, size.height * 0.2735634)
    ..cubicTo(
        size.width * 0.7910950,
        size.height * 0.3111129,
        size.width * 0.7917750,
        size.height * 0.3995759,
        size.width * 0.7925525,
        size.height * 0.4180750)
    ..cubicTo(
        size.width * 0.7932625,
        size.height * 0.4350147,
        size.width * 0.7982650,
        size.height * 0.4826406,
        size.width * 0.8005725,
        size.height * 0.4944403)
    ..lineTo(size.width * 0.8010525, size.height * 0.4969036)
    ..lineTo(size.width * 0.8026500, size.height * 0.4914854)
    ..cubicTo(
        size.width * 0.8065025,
        size.height * 0.4784094,
        size.width * 0.8121325,
        size.height * 0.4634165,
        size.width * 0.8183675,
        size.height * 0.4496352)
    ..lineTo(size.width * 0.8222400, size.height * 0.4410785)
    ..lineTo(size.width * 0.8185100, size.height * 0.4407259)
    ..cubicTo(
        size.width * 0.8084625,
        size.height * 0.4397723,
        size.width * 0.8050375,
        size.height * 0.4272376,
        size.width * 0.8037800,
        size.height * 0.3868227)
    ..cubicTo(
        size.width * 0.8031450,
        size.height * 0.3665059,
        size.width * 0.8020775,
        size.height * 0.3407363,
        size.width * 0.8016575,
        size.height * 0.3357155)
    ..cubicTo(
        size.width * 0.8014675,
        size.height * 0.3334558,
        size.width * 0.8012050,
        size.height * 0.3330536,
        size.width * 0.7993025,
        size.height * 0.3321597)
    ..cubicTo(
        size.width * 0.7938175,
        size.height * 0.3295723,
        size.width * 0.7923100,
        size.height * 0.3215171,
        size.width * 0.7957775,
        size.height * 0.3133328)
    ..cubicTo(
        size.width * 0.7969225,
        size.height * 0.3106262,
        size.width * 0.8015975,
        size.height * 0.3056401,
        size.width * 0.8077125,
        size.height * 0.3006044)
    ..cubicTo(
        size.width * 0.8096375,
        size.height * 0.2990152,
        size.width * 0.8112150,
        size.height * 0.2973366,
        size.width * 0.8112150,
        size.height * 0.2968748)
    ..cubicTo(
        size.width * 0.8112175,
        size.height * 0.2964079,
        size.width * 0.8093975,
        size.height * 0.2802578,
        size.width * 0.8071725,
        size.height * 0.2609790)
    ..arcToPoint(Offset(size.width * 0.8029600, size.height * 0.2241000),
        radius:
            Radius.elliptical(size.width * 2.343900, size.height * 4.656115),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..cubicTo(
        size.width * 0.8027950,
        size.height * 0.2223171,
        size.width * 0.8019650,
        size.height * 0.2187514,
        size.width * 0.8017150,
        size.height * 0.2187514)
    ..cubicTo(
        size.width * 0.8016450,
        size.height * 0.2187514,
        size.width * 0.8008700,
        size.height * 0.2208173,
        size.width * 0.7999950,
        size.height * 0.2233402)
    ..moveTo(size.width * 0.8067325, size.height * 0.2335755)
    ..cubicTo(
        size.width * 0.8069075,
        size.height * 0.2349462,
        size.width * 0.8085700,
        size.height * 0.2493432,
        size.width * 0.8104250,
        size.height * 0.2655777)
    ..cubicTo(
        size.width * 0.8143525,
        size.height * 0.2999290,
        size.width * 0.8135475,
        size.height * 0.2960951,
        size.width * 0.8163675,
        size.height * 0.2938851)
    ..cubicTo(
        size.width * 0.8325775,
        size.height * 0.2811915,
        size.width * 0.8444375,
        size.height * 0.2816633,
        size.width * 0.8531100,
        size.height * 0.2953501)
    ..cubicTo(
        size.width * 0.8542850,
        size.height * 0.2971976,
        size.width * 0.8557200,
        size.height * 0.2989159,
        size.width * 0.8563000,
        size.height * 0.2991642)
    ..cubicTo(
        size.width * 0.8568800,
        size.height * 0.2994125,
        size.width * 0.8576900,
        size.height * 0.2998942,
        size.width * 0.8581000,
        size.height * 0.3002369)
    ..cubicTo(
        size.width * 0.8585075,
        size.height * 0.3005746,
        size.width * 0.8588450,
        size.height * 0.3005299,
        size.width * 0.8588450,
        size.height * 0.3001326)
    ..cubicTo(
        size.width * 0.8588475,
        size.height * 0.2997353,
        size.width * 0.8599800,
        size.height * 0.2863017,
        size.width * 0.8613675,
        size.height * 0.2702807)
    ..cubicTo(
        size.width * 0.8627525,
        size.height * 0.2542598,
        size.width * 0.8638400,
        size.height * 0.2410596,
        size.width * 0.8637825,
        size.height * 0.2409454)
    ..cubicTo(
        size.width * 0.8637250,
        size.height * 0.2408311,
        size.width * 0.8630200,
        size.height * 0.2411341,
        size.width * 0.8622175,
        size.height * 0.2416158)
    ..cubicTo(
        size.width * 0.8575675,
        size.height * 0.2444167,
        size.width * 0.8455125,
        size.height * 0.2428822,
        size.width * 0.8415225,
        size.height * 0.2389837)
    ..lineTo(size.width * 0.8388700, size.height * 0.2363914)
    ..lineTo(size.width * 0.8362000, size.height * 0.2389241)
    ..cubicTo(
        size.width * 0.8282550,
        size.height * 0.2464529,
        size.width * 0.8144750,
        size.height * 0.2440790,
        size.width * 0.8082875,
        size.height * 0.2341069)
    ..lineTo(size.width * 0.8064125, size.height * 0.2310874)
    ..lineTo(size.width * 0.8067325, size.height * 0.2335755)
    ..moveTo(size.width * 0.05059500, size.height * 0.2354428)
    ..cubicTo(
        size.width * 0.04249750,
        size.height * 0.2363864,
        size.width * 0.008037500,
        size.height * 0.2479825,
        size.width * 0.0007450000,
        size.height * 0.2522137)
    ..cubicTo(
        size.width * 0.00006500000,
        size.height * 0.2526110,
        size.width * -4.302114e-18,
        size.height * 0.2542747,
        size.width * -4.302114e-18,
        size.height * 0.2715918)
    ..lineTo(size.width * -4.302114e-18, size.height * 0.2905379)
    ..lineTo(size.width * 0.001595000, size.height * 0.2905230)
    ..cubicTo(
        size.width * 0.002472500,
        size.height * 0.2905131,
        size.width * 0.01145000,
        size.height * 0.2890182,
        size.width * 0.02154750,
        size.height * 0.2872056)
    ..lineTo(size.width * 0.03990500, size.height * 0.2839080)
    ..lineTo(size.width * 0.04535500, size.height * 0.2855121)
    ..cubicTo(
        size.width * 0.05338250,
        size.height * 0.2878810,
        size.width * 0.05277250,
        size.height * 0.2881640,
        size.width * 0.05500000,
        size.height * 0.2810375)
    ..cubicTo(
        size.width * 0.05883500,
        size.height * 0.2687660,
        size.width * 0.06096750,
        size.height * 0.2505748,
        size.width * 0.06002500,
        size.height * 0.2381742)
    ..lineTo(size.width * 0.05973750, size.height * 0.2343751)
    ..lineTo(size.width * 0.05059500, size.height * 0.2354428)
    ..moveTo(size.width * 0.4541050, size.height * 0.2400763)
    ..cubicTo(
        size.width * 0.4517575,
        size.height * 0.2558192,
        size.width * 0.4491700,
        size.height * 0.2615899,
        size.width * 0.4407225,
        size.height * 0.2699033)
    ..cubicTo(
        size.width * 0.4284800,
        size.height * 0.2819563,
        size.width * 0.4283225,
        size.height * 0.2856164,
        size.width * 0.4386675,
        size.height * 0.3171468)
    ..cubicTo(
        size.width * 0.4532000,
        size.height * 0.3614255,
        size.width * 0.4708750,
        size.height * 0.3498592,
        size.width * 0.4708750,
        size.height * 0.2960653)
    ..lineTo(size.width * 0.4708750, size.height * 0.2834263)
    ..lineTo(size.width * 0.4727150, size.height * 0.2800592)
    ..cubicTo(
        size.width * 0.4765850,
        size.height * 0.2729724,
        size.width * 0.4767600,
        size.height * 0.2637452,
        size.width * 0.4731100,
        size.height * 0.2591415)
    ..cubicTo(
        size.width * 0.4715475,
        size.height * 0.2571749,
        size.width * 0.4694550,
        size.height * 0.2613118,
        size.width * 0.4678550,
        size.height * 0.2695209)
    ..cubicTo(
        size.width * 0.4674600,
        size.height * 0.2715571,
        size.width * 0.4670025,
        size.height * 0.2735286,
        size.width * 0.4668400,
        size.height * 0.2738961)
    ..cubicTo(
        size.width * 0.4666800,
        size.height * 0.2742587,
        size.width * 0.4652450,
        size.height * 0.2698884,
        size.width * 0.4636525,
        size.height * 0.2641822)
    ..cubicTo(
        size.width * 0.4601225,
        size.height * 0.2515234,
        size.width * 0.4554050,
        size.height * 0.2364857,
        size.width * 0.4549650,
        size.height * 0.2364857)
    ..cubicTo(
        size.width * 0.4547875,
        size.height * 0.2364857,
        size.width * 0.4544000,
        size.height * 0.2380997,
        size.width * 0.4541050,
        size.height * 0.2400763)
    ..moveTo(size.width * 0.4963525, size.height * 0.2640233)
    ..cubicTo(
        size.width * 0.4977450,
        size.height * 0.2650911,
        size.width * 0.4971050,
        size.height * 0.2956084,
        size.width * 0.4956875,
        size.height * 0.2956084)
    ..cubicTo(
        size.width * 0.4948825,
        size.height * 0.2956084,
        size.width * 0.4944025,
        size.height * 0.2656224,
        size.width * 0.4951825,
        size.height * 0.2640779)
    ..cubicTo(
        size.width * 0.4955275,
        size.height * 0.2633926,
        size.width * 0.4955175,
        size.height * 0.2633926,
        size.width * 0.4963525,
        size.height * 0.2640233)
    ..moveTo(size.width * 0.5082675, size.height * 0.2735733)
    ..cubicTo(
        size.width * 0.5088125,
        size.height * 0.2753264,
        size.width * 0.5048400,
        size.height * 0.2951714,
        size.width * 0.5038875,
        size.height * 0.2954544)
    ..cubicTo(
        size.width * 0.5025700,
        size.height * 0.2958468,
        size.width * 0.5030050,
        size.height * 0.2922214,
        size.width * 0.5063300,
        size.height * 0.2751278)
    ..cubicTo(
        size.width * 0.5067725,
        size.height * 0.2728532,
        size.width * 0.5077900,
        size.height * 0.2720388,
        size.width * 0.5082675,
        size.height * 0.2735733)
    ..moveTo(size.width * 0.01988000, size.height * 0.2926684)
    ..cubicTo(
        size.width * 0.009182500,
        size.height * 0.2945903,
        size.width * 0.0003325000,
        size.height * 0.2963533,
        size.width * 0.0002150000,
        size.height * 0.2965917)
    ..cubicTo(
        size.width * 0.00009500000,
        size.height * 0.2968251,
        size.width * 0.00004500000,
        size.height * 0.3748442,
        size.width * 0.0001050000,
        size.height * 0.4699619)
    ..lineTo(size.width * 0.0002125000, size.height * 0.6429050)
    ..lineTo(size.width * 0.02218000, size.height * 0.5978516)
    ..lineTo(size.width * 0.04414750, size.height * 0.5527982)
    ..lineTo(size.width * 0.04135750, size.height * 0.5498731)
    ..cubicTo(
        size.width * 0.01989500,
        size.height * 0.5273663,
        size.width * 0.02462500,
        size.height * 0.4646878,
        size.width * 0.05220000,
        size.height * 0.4062256)
    ..cubicTo(
        size.width * 0.05543250,
        size.height * 0.3993723,
        size.width * 0.08455750,
        size.height * 0.3129603,
        size.width * 0.08400500,
        size.height * 0.3118628)
    ..cubicTo(
        size.width * 0.08383750,
        size.height * 0.3115300,
        size.width * 0.04449250,
        size.height * 0.3139585,
        size.width * 0.04233750,
        size.height * 0.3144303)
    ..cubicTo(
        size.width * 0.04119000,
        size.height * 0.3146836,
        size.width * 0.04041250,
        size.height * 0.3084013,
        size.width * 0.04029750,
        size.height * 0.2979524)
    ..cubicTo(
        size.width * 0.04024500,
        size.height * 0.2931898,
        size.width * 0.04002250,
        size.height * 0.2896191,
        size.width * 0.03976750,
        size.height * 0.2894553)
    ..cubicTo(
        size.width * 0.03952750,
        size.height * 0.2893063,
        size.width * 0.03057500,
        size.height * 0.2907514,
        size.width * 0.01988000,
        size.height * 0.2926684)
    ..moveTo(size.width * 0.04294750, size.height * 0.2975055)
    ..cubicTo(
        size.width * 0.04296000,
        size.height * 0.3107156,
        size.width * 0.04336500,
        size.height * 0.3109440,
        size.width * 0.04783000,
        size.height * 0.3002518)
    ..lineTo(size.width * 0.05100250, size.height * 0.2926535)
    ..lineTo(size.width * 0.04793000, size.height * 0.2916503)
    ..cubicTo(
        size.width * 0.04258000,
        size.height * 0.2899022,
        size.width * 0.04294250,
        size.height * 0.2894801,
        size.width * 0.04294750,
        size.height * 0.2975055)
    ..moveTo(size.width * 0.4730050, size.height * 0.3016175)
    ..cubicTo(
        size.width * 0.4721275,
        size.height * 0.3240647,
        size.width * 0.4673425,
        size.height * 0.3423851,
        size.width * 0.4611475,
        size.height * 0.3470384)
    ..lineTo(size.width * 0.4589700, size.height * 0.3486673)
    ..lineTo(size.width * 0.4608850, size.height * 0.3504154)
    ..cubicTo(
        size.width * 0.4681275,
        size.height * 0.3570205,
        size.width * 0.4757350,
        size.height * 0.3464822,
        size.width * 0.4783425,
        size.height * 0.3262300)
    ..cubicTo(
        size.width * 0.4797825,
        size.height * 0.3150511,
        size.width * 0.4796050,
        size.height * 0.3130547,
        size.width * 0.4762400,
        size.height * 0.3027001)
    ..lineTo(size.width * 0.4733125, size.height * 0.2936914)
    ..lineTo(size.width * 0.4730050, size.height * 0.3016175)
    ..moveTo(size.width * 0.2667225, size.height * 0.3378460)
    ..cubicTo(
        size.width * 0.2611925,
        size.height * 0.3480664,
        size.width * 0.2566050,
        size.height * 0.3563053,
        size.width * 0.2565250,
        size.height * 0.3561514)
    ..cubicTo(
        size.width * 0.2564475,
        size.height * 0.3559925,
        size.width * 0.2570525,
        size.height * 0.3519251,
        size.width * 0.2578700,
        size.height * 0.3471079)
    ..cubicTo(
        size.width * 0.2610250,
        size.height * 0.3284896,
        size.width * 0.2596400,
        size.height * 0.3284052,
        size.width * 0.2544725,
        size.height * 0.3469093)
    ..lineTo(size.width * 0.2502850, size.height * 0.3619072)
    ..lineTo(size.width * 0.2260775, size.height * 0.3897776)
    ..cubicTo(
        size.width * 0.2127625,
        size.height * 0.4051082,
        size.width * 0.2017225,
        size.height * 0.4177621,
        size.width * 0.2015450,
        size.height * 0.4178962)
    ..cubicTo(
        size.width * 0.2013675,
        size.height * 0.4180303,
        size.width * 0.1997300,
        size.height * 0.4128952,
        size.width * 0.1979100,
        size.height * 0.4064938)
    ..arcToPoint(Offset(size.width * 0.1944700, size.height * 0.3945402),
        radius:
            Radius.elliptical(size.width * 0.3680750, size.height * 0.7311744),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.1943625,
        size.height * 0.3942720,
        size.width * 0.1696225,
        size.height * 0.4281713,
        size.width * 0.1686375,
        size.height * 0.4299393)
    ..cubicTo(
        size.width * 0.1680925,
        size.height * 0.4309176,
        size.width * 0.1785050,
        size.height * 0.4700116,
        size.width * 0.1803075,
        size.height * 0.4737511)
    ..cubicTo(
        size.width * 0.1835275,
        size.height * 0.4804257,
        size.width * 0.1893000,
        size.height * 0.4832366,
        size.width * 0.1936875,
        size.height * 0.4802668)
    ..cubicTo(
        size.width * 0.1956900,
        size.height * 0.4789110,
        size.width * 0.2705800,
        size.height * 0.3809625,
        size.width * 0.2743475,
        size.height * 0.3747747)
    ..cubicTo(
        size.width * 0.2790600,
        size.height * 0.3670274,
        size.width * 0.2842175,
        size.height * 0.3518556,
        size.width * 0.2832175,
        size.height * 0.3486773)
    ..cubicTo(
        size.width * 0.2825750,
        size.height * 0.3466312,
        size.width * 0.2826350,
        size.height * 0.3437855,
        size.width * 0.2833750,
        size.height * 0.3412130)
    ..cubicTo(
        size.width * 0.2843625,
        size.height * 0.3377864,
        size.width * 0.2841700,
        size.height * 0.3369918,
        size.width * 0.2823525,
        size.height * 0.3369918)
    ..lineTo(size.width * 0.2806900, size.height * 0.3369918)
    ..lineTo(size.width * 0.2812825, size.height * 0.3325569)
    ..cubicTo(
        size.width * 0.2822150,
        size.height * 0.3256142,
        size.width * 0.2821475,
        size.height * 0.3251672,
        size.width * 0.2801875,
        size.height * 0.3251672)
    ..cubicTo(
        size.width * 0.2785275,
        size.height * 0.3251672,
        size.width * 0.2784875,
        size.height * 0.3250977,
        size.width * 0.2784875,
        size.height * 0.3222123)
    ..cubicTo(
        size.width * 0.2784875,
        size.height * 0.3167396,
        size.width * 0.2772600,
        size.height * 0.3183685,
        size.width * 0.2667225,
        size.height * 0.3378460)
    ..moveTo(size.width * 0.8171775, size.height * 0.3317127)
    ..cubicTo(
        size.width * 0.8102775,
        size.height * 0.3338184,
        size.width * 0.8045750,
        size.height * 0.3356410,
        size.width * 0.8045000,
        size.height * 0.3357602)
    ..cubicTo(
        size.width * 0.8044250,
        size.height * 0.3358793,
        size.width * 0.8046625,
        size.height * 0.3421815,
        size.width * 0.8050250,
        size.height * 0.3497599)
    ..cubicTo(
        size.width * 0.8053875,
        size.height * 0.3573383,
        size.width * 0.8058950,
        size.height * 0.3711543,
        size.width * 0.8061525,
        size.height * 0.3804610)
    ..cubicTo(
        size.width * 0.8074500,
        size.height * 0.4273270,
        size.width * 0.8101250,
        size.height * 0.4366486,
        size.width * 0.8219825,
        size.height * 0.4355908)
    ..cubicTo(
        size.width * 0.8248350,
        size.height * 0.4353326,
        size.width * 0.8276775,
        size.height * 0.4347317,
        size.width * 0.8283025,
        size.height * 0.4342499)
    ..cubicTo(
        size.width * 0.8291725,
        size.height * 0.4335745,
        size.width * 0.8294850,
        size.height * 0.4336341,
        size.width * 0.8296525,
        size.height * 0.4345082)
    ..cubicTo(
        size.width * 0.8297750,
        size.height * 0.4351289,
        size.width * 0.8306500,
        size.height * 0.4359037,
        size.width * 0.8316000,
        size.height * 0.4362215)
    ..cubicTo(
        size.width * 0.8325525,
        size.height * 0.4365393,
        size.width * 0.8334300,
        size.height * 0.4371254,
        size.width * 0.8335525,
        size.height * 0.4375177)
    ..cubicTo(
        size.width * 0.8339925,
        size.height * 0.4389331,
        size.width * 0.8316475,
        size.height * 0.4581622,
        size.width * 0.8309275,
        size.height * 0.4590363)
    ..cubicTo(
        size.width * 0.8167475,
        size.height * 0.4762491,
        size.width * 0.8109975,
        size.height * 0.5047502,
        size.width * 0.8150375,
        size.height * 0.5377903)
    ..cubicTo(
        size.width * 0.8191750,
        size.height * 0.5716350,
        size.width * 0.8244450,
        size.height * 0.5643397,
        size.width * 0.8371675,
        size.height * 0.5071786)
    ..cubicTo(
        size.width * 0.8427375,
        size.height * 0.4821390,
        size.width * 0.8446100,
        size.height * 0.4750374,
        size.width * 0.8467825,
        size.height * 0.4707118)
    ..cubicTo(
        size.width * 0.8477550,
        size.height * 0.4687750,
        size.width * 0.8487600,
        size.height * 0.4659244,
        size.width * 0.8490150,
        size.height * 0.4643799)
    ..cubicTo(
        size.width * 0.8495000,
        size.height * 0.4614200,
        size.width * 0.8497900,
        size.height * 0.4188746,
        size.width * 0.8493325,
        size.height * 0.4174046)
    ..cubicTo(
        size.width * 0.8491925,
        size.height * 0.4169576,
        size.width * 0.8497550,
        size.height * 0.4092203,
        size.width * 0.8505825,
        size.height * 0.4002165)
    ..lineTo(size.width * 0.8520900, size.height * 0.3838479)
    ..lineTo(size.width * 0.8497375, size.height * 0.3768654)
    ..cubicTo(
        size.width * 0.8457375,
        size.height * 0.3650012,
        size.width * 0.8431400,
        size.height * 0.3522579,
        size.width * 0.8431175,
        size.height * 0.3444162)
    ..cubicTo(
        size.width * 0.8431125,
        size.height * 0.3424894,
        size.width * 0.8428575,
        size.height * 0.3419878,
        size.width * 0.8415175,
        size.height * 0.3412528)
    ..cubicTo(
        size.width * 0.8389375,
        size.height * 0.3398424,
        size.width * 0.8356300,
        size.height * 0.3356112,
        size.width * 0.8339075,
        size.height * 0.3315140)
    ..cubicTo(
        size.width * 0.8319425,
        size.height * 0.3268458,
        size.width * 0.8331650,
        size.height * 0.3268309,
        size.width * 0.8171775,
        size.height * 0.3317127)
    ..moveTo(size.width * 0.5664325, size.height * 0.3612368)
    ..cubicTo(
        size.width * 0.5648975,
        size.height * 0.3618228,
        size.width * 0.5652325,
        size.height * 0.3624237,
        size.width * 0.5706650,
        size.height * 0.3688748)
    ..cubicTo(
        size.width * 0.5763700,
        size.height * 0.3756537,
        size.width * 0.5766650,
        size.height * 0.3757530,
        size.width * 0.5753600,
        size.height * 0.3704193)
    ..arcToPoint(Offset(size.width * 0.5699400, size.height * 0.3615745),
        radius: Radius.elliptical(
            size.width * 0.008475000, size.height * 0.01683543),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.5681950,
        size.height * 0.3606259,
        size.width * 0.5680650,
        size.height * 0.3606110,
        size.width * 0.5664325,
        size.height * 0.3612368)
    ..moveTo(size.width * 0.05248750, size.height * 0.4132181)
    ..cubicTo(
        size.width * 0.03063000,
        size.height * 0.4599550,
        size.width * 0.02435000,
        size.height * 0.5119164,
        size.width * 0.03742250,
        size.height * 0.5378847)
    ..cubicTo(
        size.width * 0.04152500,
        size.height * 0.5460392,
        size.width * 0.04116250,
        size.height * 0.5457959,
        size.width * 0.09736500,
        size.height * 0.5773412)
    ..cubicTo(
        size.width * 0.1260100,
        size.height * 0.5934218,
        size.width * 0.1495150,
        size.height * 0.6067064,
        size.width * 0.1495950,
        size.height * 0.6068603)
    ..cubicTo(
        size.width * 0.1496775,
        size.height * 0.6070192,
        size.width * 0.1444350,
        size.height * 0.6360666,
        size.width * 0.1379475,
        size.height * 0.6714110)
    ..cubicTo(
        size.width * 0.1314600,
        size.height * 0.7067555,
        size.width * 0.1262425,
        size.height * 0.7358575,
        size.width * 0.1263550,
        size.height * 0.7360810)
    ..cubicTo(
        size.width * 0.1264675,
        size.height * 0.7362995,
        size.width * 0.1285525,
        size.height * 0.7359221,
        size.width * 0.1309875,
        size.height * 0.7352317)
    ..cubicTo(
        size.width * 0.1457225,
        size.height * 0.7310701,
        size.width * 0.1609475,
        size.height * 0.7369948,
        size.width * 0.1724775,
        size.height * 0.7513769)
    ..cubicTo(
        size.width * 0.1744925,
        size.height * 0.7538898,
        size.width * 0.1761900,
        size.height * 0.7558465,
        size.width * 0.1762450,
        size.height * 0.7557173)
    ..cubicTo(
        size.width * 0.1773625,
        size.height * 0.7532193,
        size.width * 0.2110450,
        size.height * 0.5798740,
        size.width * 0.2110150,
        size.height * 0.5767949)
    ..cubicTo(
        size.width * 0.2108625,
        size.height * 0.5621545,
        size.width * 0.1989125,
        size.height * 0.5430346,
        size.width * 0.1806825,
        size.height * 0.5282552)
    ..cubicTo(
        size.width * 0.1739175,
        size.height * 0.5227725,
        size.width * 0.1173125,
        size.height * 0.4682833,
        size.width * 0.08365250,
        size.height * 0.4348558)
    ..cubicTo(
        size.width * 0.06828000,
        size.height * 0.4195847,
        size.width * 0.05562250,
        size.height * 0.4070947,
        size.width * 0.05552500,
        size.height * 0.4070947)
    ..cubicTo(
        size.width * 0.05543000,
        size.height * 0.4070947,
        size.width * 0.05406250,
        size.height * 0.4098510,
        size.width * 0.05248750,
        size.height * 0.4132181)
    ..moveTo(size.width * 0.5080775, size.height * 0.4135806)
    ..cubicTo(
        size.width * 0.5046400,
        size.height * 0.4159693,
        size.width * 0.5039450,
        size.height * 0.4172307,
        size.width * 0.5024175,
        size.height * 0.4238706)
    ..cubicTo(
        size.width * 0.4980775,
        size.height * 0.4427223,
        size.width * 0.4968975,
        size.height * 0.4510109,
        size.width * 0.4985475,
        size.height * 0.4510109)
    ..cubicTo(
        size.width * 0.4994900,
        size.height * 0.4510109,
        size.width * 0.5016525,
        size.height * 0.4473706,
        size.width * 0.5050375,
        size.height * 0.4400902)
    ..cubicTo(
        size.width * 0.5116025,
        size.height * 0.4259762,
        size.width * 0.5139175,
        size.height * 0.4258421,
        size.width * 0.5153075,
        size.height * 0.4394942)
    ..cubicTo(
        size.width * 0.5169775,
        size.height * 0.4558976,
        size.width * 0.5145900,
        size.height * 0.4663415,
        size.width * 0.5083450,
        size.height * 0.4699470)
    ..cubicTo(
        size.width * 0.5059700,
        size.height * 0.4713226,
        size.width * 0.5049475,
        size.height * 0.4738008,
        size.width * 0.5057250,
        size.height * 0.4763038)
    ..cubicTo(
        size.width * 0.5074550,
        size.height * 0.4818609,
        size.width * 0.5209650,
        size.height * 0.4658549,
        size.width * 0.5220800,
        size.height * 0.4569256)
    ..cubicTo(
        size.width * 0.5234100,
        size.height * 0.4462582,
        size.width * 0.5235000,
        size.height * 0.4458063,
        size.width * 0.5241925,
        size.height * 0.4462582)
    ..cubicTo(
        size.width * 0.5245675,
        size.height * 0.4465016,
        size.width * 0.5282200,
        size.height * 0.4490294,
        size.width * 0.5323125,
        size.height * 0.4518651)
    ..cubicTo(
        size.width * 0.5538850,
        size.height * 0.4668332,
        size.width * 0.5699850,
        size.height * 0.4698378,
        size.width * 0.5838150,
        size.height * 0.4614747)
    ..lineTo(size.width * 0.5874850, size.height * 0.4592548)
    ..lineTo(size.width * 0.5802300, size.height * 0.4392956)
    ..arcToPoint(Offset(size.width * 0.5729350, size.height * 0.4191775),
        radius:
            Radius.elliptical(size.width * 2.356380, size.height * 4.680906),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..cubicTo(
        size.width * 0.5729125,
        size.height * 0.4190881,
        size.width * 0.5614200,
        size.height * 0.4182141,
        size.width * 0.5473950,
        size.height * 0.4172407)
    ..cubicTo(
        size.width * 0.5310200,
        size.height * 0.4160985,
        size.width * 0.5207550,
        size.height * 0.4150406,
        size.width * 0.5187075,
        size.height * 0.4142808)
    ..cubicTo(
        size.width * 0.5155825,
        size.height * 0.4131187,
        size.width * 0.5093375,
        size.height * 0.4127065,
        size.width * 0.5080775,
        size.height * 0.4135806)
    ..moveTo(size.width * 0.3840350, size.height * 0.4262394)
    ..cubicTo(
        size.width * 0.3836200,
        size.height * 0.4282508,
        size.width * 0.3836175,
        size.height * 0.4565879,
        size.width * 0.3840325,
        size.height * 0.4574123)
    ..cubicTo(
        size.width * 0.3844625,
        size.height * 0.4582665,
        size.width * 0.3874600,
        size.height * 0.4564042,
        size.width * 0.3885950,
        size.height * 0.4545766)
    ..cubicTo(
        size.width * 0.3895125,
        size.height * 0.4531016,
        size.width * 0.3895125,
        size.height * 0.4530768,
        size.width * 0.3887000,
        size.height * 0.4509960)
    ..cubicTo(
        size.width * 0.3877375,
        size.height * 0.4485327,
        size.width * 0.3878000,
        size.height * 0.4480609,
        size.width * 0.3900000,
        size.height * 0.4409146)
    ..lineTo(size.width * 0.3916800, size.height * 0.4354617)
    ..lineTo(size.width * 0.3908950, size.height * 0.4325614)
    ..cubicTo(
        size.width * 0.3894650,
        size.height * 0.4272873,
        size.width * 0.3847200,
        size.height * 0.4229170,
        size.width * 0.3840350,
        size.height * 0.4262394)
    ..moveTo(size.width * 0.6071550, size.height * 0.4464370)
    ..cubicTo(
        size.width * 0.6009525,
        size.height * 0.4517806,
        size.width * 0.6007950,
        size.height * 0.4698080,
        size.width * 0.6068975,
        size.height * 0.4758320)
    ..cubicTo(
        size.width * 0.6090050,
        size.height * 0.4779078,
        size.width * 0.6089725,
        size.height * 0.4780519,
        size.width * 0.6100675,
        size.height * 0.4621153)
    ..cubicTo(
        size.width * 0.6113625,
        size.height * 0.4432835,
        size.width * 0.6112875,
        size.height * 0.4428762,
        size.width * 0.6071550,
        size.height * 0.4464370)
    ..moveTo(size.width * 0.4979200, size.height * 0.4606205)
    ..cubicTo(
        size.width * 0.4972050,
        size.height * 0.4623239,
        size.width * 0.4978125,
        size.height * 0.4645239,
        size.width * 0.4989950,
        size.height * 0.4645239)
    ..cubicTo(
        size.width * 0.5002100,
        size.height * 0.4645239,
        size.width * 0.5007400,
        size.height * 0.4624977,
        size.width * 0.4999950,
        size.height * 0.4607148)
    ..cubicTo(
        size.width * 0.4993400,
        size.height * 0.4591455,
        size.width * 0.4985500,
        size.height * 0.4591108,
        size.width * 0.4979200,
        size.height * 0.4606205)
    ..moveTo(size.width * 0.5042525, size.height * 0.4648418)
    ..cubicTo(
        size.width * 0.5042525,
        size.height * 0.4673894,
        size.width * 0.5046450,
        size.height * 0.4679854,
        size.width * 0.5059000,
        size.height * 0.4673298)
    ..cubicTo(
        size.width * 0.5073125,
        size.height * 0.4665998,
        size.width * 0.5069525,
        size.height * 0.4633420,
        size.width * 0.5054200,
        size.height * 0.4629894)
    ..cubicTo(
        size.width * 0.5044200,
        size.height * 0.4627609,
        size.width * 0.5042525,
        size.height * 0.4630291,
        size.width * 0.5042525,
        size.height * 0.4648418)
    ..moveTo(size.width * 0.4702375, size.height * 0.4704387)
    ..cubicTo(
        size.width * 0.4697125,
        size.height * 0.4714816,
        size.width * 0.4693875,
        size.height * 0.4732545,
        size.width * 0.4693875,
        size.height * 0.4750821)
    ..cubicTo(
        size.width * 0.4693875,
        size.height * 0.4809472,
        size.width * 0.4728300,
        size.height * 0.4837133,
        size.width * 0.4748400,
        size.height * 0.4794623)
    ..cubicTo(
        size.width * 0.4768500,
        size.height * 0.4752112,
        size.width * 0.4754850,
        size.height * 0.4687502,
        size.width * 0.4725775,
        size.height * 0.4687502)
    ..cubicTo(
        size.width * 0.4716550,
        size.height * 0.4687502,
        size.width * 0.4707650,
        size.height * 0.4693908,
        size.width * 0.4702375,
        size.height * 0.4704387)
    ..moveTo(size.width * 0.4964175, size.height * 0.4717646)
    ..cubicTo(
        size.width * 0.4956725,
        size.height * 0.4732396,
        size.width * 0.4961825,
        size.height * 0.4763484,
        size.width * 0.4971675,
        size.height * 0.4763484)
    ..cubicTo(
        size.width * 0.4978575,
        size.height * 0.4763484,
        size.width * 0.4987250,
        size.height * 0.4746301,
        size.width * 0.4987250,
        size.height * 0.4732545)
    ..cubicTo(
        size.width * 0.4987250,
        size.height * 0.4712978,
        size.width * 0.4971600,
        size.height * 0.4702897,
        size.width * 0.4964175,
        size.height * 0.4717646)
    ..moveTo(size.width * 0.4738425, size.height * 0.4729516)
    ..cubicTo(
        size.width * 0.4753225,
        size.height * 0.4761995,
        size.width * 0.4730800,
        size.height * 0.4804356,
        size.width * 0.4714400,
        size.height * 0.4774857)
    ..cubicTo(
        size.width * 0.4704875,
        size.height * 0.4757724,
        size.width * 0.4704550,
        size.height * 0.4743520,
        size.width * 0.4713325,
        size.height * 0.4726089)
    ..cubicTo(
        size.width * 0.4722225,
        size.height * 0.4708409,
        size.width * 0.4729250,
        size.height * 0.4709353,
        size.width * 0.4738425,
        size.height * 0.4729516)
    ..moveTo(size.width * 0.5019850, size.height * 0.4752261)
    ..cubicTo(
        size.width * 0.5014650,
        size.height * 0.4762541,
        size.width * 0.5016925,
        size.height * 0.4789309,
        size.width * 0.5023375,
        size.height * 0.4794225)
    ..cubicTo(
        size.width * 0.5036175,
        size.height * 0.4803959,
        size.width * 0.5042925,
        size.height * 0.4791444,
        size.width * 0.5037550,
        size.height * 0.4768004)
    ..cubicTo(
        size.width * 0.5032775,
        size.height * 0.4747195,
        size.width * 0.5025600,
        size.height * 0.4740789,
        size.width * 0.5019850,
        size.height * 0.4752261)
    ..moveTo(size.width * 0.4634350, size.height * 0.4875919)
    ..cubicTo(
        size.width * 0.4606275,
        size.height * 0.4898913,
        size.width * 0.4559625,
        size.height * 0.4957812,
        size.width * 0.4512750,
        size.height * 0.5029574)
    ..lineTo(size.width * 0.4478325, size.height * 0.5082265)
    ..lineTo(size.width * 0.4312925, size.height * 0.5130735)
    ..cubicTo(
        size.width * 0.4173500,
        size.height * 0.5171607,
        size.width * 0.4144550,
        size.height * 0.5177865,
        size.width * 0.4128550,
        size.height * 0.5170614)
    ..cubicTo(
        size.width * 0.4112700,
        size.height * 0.5163413,
        size.width * 0.4096250,
        size.height * 0.5166691,
        size.width * 0.4028625,
        size.height * 0.5190727)
    ..cubicTo(
        size.width * 0.3940200,
        size.height * 0.5222213,
        size.width * 0.3930475,
        size.height * 0.5224050,
        size.width * 0.3926225,
        size.height * 0.5210393)
    ..cubicTo(
        size.width * 0.3920850,
        size.height * 0.5193161,
        size.width * 0.3930775,
        size.height * 0.5186555,
        size.width * 0.3998750,
        size.height * 0.5162171)
    ..lineTo(size.width * 0.4066750, size.height * 0.5137738)
    ..lineTo(size.width * 0.4047625, size.height * 0.5127209)
    ..cubicTo(
        size.width * 0.3883550,
        size.height * 0.5036725,
        size.width * 0.3846800,
        size.height * 0.5016860,
        size.width * 0.3843525,
        size.height * 0.5016860)
    ..cubicTo(
        size.width * 0.3838650,
        size.height * 0.5016860,
        size.width * 0.3816800,
        size.height * 0.5151842,
        size.width * 0.3807300,
        size.height * 0.5240687)
    ..cubicTo(
        size.width * 0.3782525,
        size.height * 0.5471963,
        size.width * 0.3815175,
        size.height * 0.5690029,
        size.width * 0.3883925,
        size.height * 0.5752703)
    ..cubicTo(
        size.width * 0.3966125,
        size.height * 0.5827593,
        size.width * 0.4287225,
        size.height * 0.5619609,
        size.width * 0.4529500,
        size.height * 0.5334548)
    ..cubicTo(
        size.width * 0.4574800,
        size.height * 0.5281261,
        size.width * 0.4589300,
        size.height * 0.5274209,
        size.width * 0.4679000,
        size.height * 0.5262240)
    ..cubicTo(
        size.width * 0.4791725,
        size.height * 0.5247193,
        size.width * 0.4839650,
        size.height * 0.5200113,
        size.width * 0.4872300,
        size.height * 0.5072382)
    ..cubicTo(
        size.width * 0.4899200,
        size.height * 0.4967049,
        size.width * 0.4887800,
        size.height * 0.4955180,
        size.width * 0.4846100,
        size.height * 0.5045118)
    ..cubicTo(
        size.width * 0.4788775,
        size.height * 0.5168826,
        size.width * 0.4752550,
        size.height * 0.5190975,
        size.width * 0.4696775,
        size.height * 0.5136496)
    ..cubicTo(
        size.width * 0.4600025,
        size.height * 0.5041940,
        size.width * 0.4617525,
        size.height * 0.4941473,
        size.width * 0.4737450,
        size.height * 0.4902985)
    ..cubicTo(
        size.width * 0.4749100,
        size.height * 0.4899261,
        size.width * 0.4753400,
        size.height * 0.4893698,
        size.width * 0.4753400,
        size.height * 0.4882524)
    ..cubicTo(
        size.width * 0.4753400,
        size.height * 0.4850592,
        size.width * 0.4670900,
        size.height * 0.4846072,
        size.width * 0.4634350,
        size.height * 0.4875919)
    ..moveTo(size.width * 0.4808825, size.height * 0.4889626)
    ..arcToPoint(Offset(size.width * 0.4825975, size.height * 0.4923645),
        radius: Radius.elliptical(
            size.width * 0.001250000, size.height * 0.002483102),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.4835325,
        size.height * 0.4913712,
        size.width * 0.4836525,
        size.height * 0.4898218,
        size.width * 0.4829075,
        size.height * 0.4883418)
    ..cubicTo(
        size.width * 0.4821625,
        size.height * 0.4868619,
        size.width * 0.4813850,
        size.height * 0.4871003,
        size.width * 0.4808825,
        size.height * 0.4889626)
    ..moveTo(size.width * 0.4728750, size.height * 0.4967893)
    ..cubicTo(
        size.width * 0.4722050,
        size.height * 0.4981203,
        size.width * 0.4722300,
        size.height * 0.4987659,
        size.width * 0.4730150,
        size.height * 0.5003253)
    ..cubicTo(
        size.width * 0.4741825,
        size.height * 0.5026445,
        size.width * 0.4759250,
        size.height * 0.4994612,
        size.width * 0.4748700,
        size.height * 0.4969334)
    ..cubicTo(
        size.width * 0.4742725,
        size.height * 0.4955081,
        size.width * 0.4735475,
        size.height * 0.4954534,
        size.width * 0.4728750,
        size.height * 0.4967893)
    ..moveTo(size.width * 0.4784825, size.height * 0.5012043)
    ..cubicTo(
        size.width * 0.4780575,
        size.height * 0.5033994,
        size.width * 0.4786525,
        size.height * 0.5052071,
        size.width * 0.4796950,
        size.height * 0.5049141)
    ..cubicTo(
        size.width * 0.4804000,
        size.height * 0.5047154,
        size.width * 0.4806550,
        size.height * 0.5040797,
        size.width * 0.4806550,
        size.height * 0.5025303)
    ..cubicTo(
        size.width * 0.4806550,
        size.height * 0.5000372,
        size.width * 0.4789125,
        size.height * 0.4989745,
        size.width * 0.4784825,
        size.height * 0.5012043)
    ..moveTo(size.width * 0.4177075, size.height * 0.5067714)
    ..cubicTo(
        size.width * 0.4166550,
        size.height * 0.5074121,
        size.width * 0.4149650,
        size.height * 0.5117426,
        size.width * 0.4149650,
        size.height * 0.5138036)
    ..cubicTo(
        size.width * 0.4149650,
        size.height * 0.5140022,
        size.width * 0.4237000,
        size.height * 0.5115191,
        size.width * 0.4254875,
        size.height * 0.5108089)
    ..cubicTo(
        size.width * 0.4265850,
        size.height * 0.5103769,
        size.width * 0.4228325,
        size.height * 0.5067714,
        size.width * 0.4208575,
        size.height * 0.5063642)
    ..cubicTo(
        size.width * 0.4198375,
        size.height * 0.5061506,
        size.width * 0.4184200,
        size.height * 0.5063344,
        size.width * 0.4177075,
        size.height * 0.5067714)
    ..moveTo(size.width * 0.5531475, size.height * 0.5115539)
    ..cubicTo(
        size.width * 0.5486525,
        size.height * 0.5124677,
        size.width * 0.5387425,
        size.height * 0.5155318,
        size.width * 0.5348175,
        size.height * 0.5172203)
    ..lineTo(size.width * 0.5303075, size.height * 0.5191571)
    ..lineTo(size.width * 0.5261025, size.height * 0.5259807)
    ..cubicTo(
        size.width * 0.5073325,
        size.height * 0.5564434,
        size.width * 0.4853525,
        size.height * 0.5658643,
        size.width * 0.4580125,
        size.height * 0.5551671)
    ..cubicTo(
        size.width * 0.4511200,
        size.height * 0.5524704,
        size.width * 0.4451300,
        size.height * 0.5544818,
        size.width * 0.4307875,
        size.height * 0.5643049)
    ..cubicTo(
        size.width * 0.4236375,
        size.height * 0.5692065,
        size.width * 0.4232350,
        size.height * 0.5697081,
        size.width * 0.4259050,
        size.height * 0.5704133)
    ..cubicTo(
        size.width * 0.4290150,
        size.height * 0.5712328,
        size.width * 0.4401400,
        size.height * 0.5769687,
        size.width * 0.4474925,
        size.height * 0.5815426)
    ..cubicTo(
        size.width * 0.4510000,
        size.height * 0.5837228,
        size.width * 0.4544625,
        size.height * 0.5854758,
        size.width * 0.4551900,
        size.height * 0.5854361)
    ..cubicTo(
        size.width * 0.4559175,
        size.height * 0.5853964,
        size.width * 0.4609875,
        size.height * 0.5886294,
        size.width * 0.4664575,
        size.height * 0.5926222)
    ..lineTo(size.width * 0.4764025, size.height * 0.5998828)
    ..lineTo(size.width * 0.4904325, size.height * 0.6000616)
    ..cubicTo(
        size.width * 0.5048050,
        size.height * 0.6002453,
        size.width * 0.5116525,
        size.height * 0.6012485,
        size.width * 0.5225050,
        size.height * 0.6047546)
    ..lineTo(size.width * 0.5273675, size.height * 0.6063289)
    ..lineTo(size.width * 0.5312250, size.height * 0.5994060)
    ..cubicTo(
        size.width * 0.5502550,
        size.height * 0.5652485,
        size.width * 0.5501250,
        size.height * 0.5654620,
        size.width * 0.5522400,
        size.height * 0.5653677)
    ..cubicTo(
        size.width * 0.5533225,
        size.height * 0.5653180,
        size.width * 0.5578775,
        size.height * 0.5639026,
        size.width * 0.5623625,
        size.height * 0.5622191)
    ..cubicTo(
        size.width * 0.5765575,
        size.height * 0.5568904,
        size.width * 0.5944200,
        size.height * 0.5523612,
        size.width * 0.6012500,
        size.height * 0.5523612)
    ..cubicTo(
        size.width * 0.6047150,
        size.height * 0.5523612,
        size.width * 0.6047900,
        size.height * 0.5520086,
        size.width * 0.6029525,
        size.height * 0.5443408)
    ..cubicTo(
        size.width * 0.5969550,
        size.height * 0.5192912,
        size.width * 0.5776200,
        size.height * 0.5065579,
        size.width * 0.5531475,
        size.height * 0.5115539)
    ..moveTo(size.width * 0.3785450, size.height * 0.5139923)
    ..arcToPoint(Offset(size.width * 0.3787625, size.height * 0.5162569),
        radius: Radius.elliptical(
            size.width * 0.001160000, size.height * 0.002304319),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.3793175,
        size.height * 0.5173594,
        size.width * 0.3794700,
        size.height * 0.5171309,
        size.width * 0.3798650,
        size.height * 0.5145684)
    ..cubicTo(
        size.width * 0.3802125,
        size.height * 0.5123236,
        size.width * 0.3789700,
        size.height * 0.5117823,
        size.width * 0.3785450,
        size.height * 0.5139923)
    ..moveTo(size.width * 0.3763925, size.height * 0.5465507)
    ..cubicTo(
        size.width * 0.3728050,
        size.height * 0.5562199,
        size.width * 0.3729375,
        size.height * 0.5561405,
        size.width * 0.3696850,
        size.height * 0.5506727)
    ..cubicTo(
        size.width * 0.3655625,
        size.height * 0.5437498,
        size.width * 0.3646275,
        size.height * 0.5455773,
        size.width * 0.3673150,
        size.height * 0.5553061)
    ..lineTo(size.width * 0.3690725, size.height * 0.5616629)
    ..lineTo(size.width * 0.3664425, size.height * 0.5650349)
    ..lineTo(size.width * 0.3638125, size.height * 0.5684120)
    ..lineTo(size.width * 0.3710850, size.height * 0.5684120)
    ..lineTo(size.width * 0.3700975, size.height * 0.5774852)
    ..cubicTo(
        size.width * 0.3695550,
        size.height * 0.5824713,
        size.width * 0.3691525,
        size.height * 0.5866379,
        size.width * 0.3692025,
        size.height * 0.5867373)
    ..cubicTo(
        size.width * 0.3694200,
        size.height * 0.5871693,
        size.width * 0.3750625,
        size.height * 0.5698671,
        size.width * 0.3768650,
        size.height * 0.5632471)
    ..cubicTo(
        size.width * 0.3787775,
        size.height * 0.5562100,
        size.width * 0.3788275,
        size.height * 0.5558326,
        size.width * 0.3784225,
        size.height * 0.5518447)
    ..arcToPoint(Offset(size.width * 0.3778825, size.height * 0.5452843),
        radius: Radius.elliptical(
            size.width * 0.04344250, size.height * 0.08629774),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..lineTo(size.width * 0.3777625, size.height * 0.5428559)
    ..lineTo(size.width * 0.3763925, size.height * 0.5465507)
    ..moveTo(size.width * 0.02306500, size.height * 0.6033293)
    ..lineTo(0, size.height * 0.6506026)
    ..lineTo(0, size.height * 0.7087470)
    ..cubicTo(
        0,
        size.height * 0.7547092,
        size.width * 0.0001125000,
        size.height * 0.7668863,
        size.width * 0.0005325000,
        size.height * 0.7668714)
    ..cubicTo(
        size.width * 0.01232000,
        size.height * 0.7664443,
        size.width * 0.02019000,
        size.height * 0.7611752,
        size.width * 0.02618000,
        size.height * 0.7496983)
    ..cubicTo(
        size.width * 0.03205000,
        size.height * 0.7384449,
        size.width * 0.09784000,
        size.height * 0.5840754,
        size.width * 0.09704750,
        size.height * 0.5834099)
    ..cubicTo(
        size.width * 0.09646500,
        size.height * 0.5829232,
        size.width * 0.05307750,
        size.height * 0.5595820,
        size.width * 0.04740750,
        size.height * 0.5567016)
    ..cubicTo(
        size.width * 0.04616250,
        size.height * 0.5560709,
        size.width * 0.04556500,
        size.height * 0.5572181,
        size.width * 0.02306500,
        size.height * 0.6033293)
    ..moveTo(size.width * 0.5960875, size.height * 0.5566867)
    ..cubicTo(
        size.width * 0.5861650,
        size.height * 0.5581965,
        size.width * 0.5676450,
        size.height * 0.5635947,
        size.width * 0.5559600,
        size.height * 0.5683871)
    ..cubicTo(
        size.width * 0.5524900,
        size.height * 0.5698075,
        size.width * 0.5514875,
        size.height * 0.5705872,
        size.width * 0.5502150,
        size.height * 0.5728468)
    ..lineTo(size.width * 0.5184275, size.height * 0.6294714)
    ..lineTo(size.width * 0.4881825, size.height * 0.6833548)
    ..lineTo(size.width * 0.4884000, size.height * 0.6869056)
    ..cubicTo(
        size.width * 0.4889375,
        size.height * 0.6956511,
        size.width * 0.4918450,
        size.height * 0.7090698,
        size.width * 0.4959175,
        size.height * 0.7216045)
    ..cubicTo(
        size.width * 0.4991600,
        size.height * 0.7315766,
        size.width * 0.5050700,
        size.height * 0.7459041,
        size.width * 0.5056675,
        size.height * 0.7452287)
    ..cubicTo(
        size.width * 0.5260200,
        size.height * 0.7222302,
        size.width * 0.5674475,
        size.height * 0.6584195,
        size.width * 0.5858850,
        size.height * 0.6216695)
    ..cubicTo(
        size.width * 0.6008650,
        size.height * 0.5918127,
        size.width * 0.6038775,
        size.height * 0.5821087,
        size.width * 0.6042575,
        size.height * 0.5624972)
    ..lineTo(size.width * 0.6043800, size.height * 0.5561653)
    ..lineTo(size.width * 0.6026775, size.height * 0.5560511)
    ..cubicTo(
        size.width * 0.6017425,
        size.height * 0.5559915,
        size.width * 0.5987775,
        size.height * 0.5562746,
        size.width * 0.5960875,
        size.height * 0.5566867)
    ..moveTo(size.width * 0.3105825, size.height * 0.5736910)
    ..cubicTo(
        size.width * 0.3106825,
        size.height * 0.5742721,
        size.width * 0.3119325,
        size.height * 0.5853864,
        size.width * 0.3133625,
        size.height * 0.5983929)
    ..cubicTo(
        size.width * 0.3181025,
        size.height * 0.6415592,
        size.width * 0.3213575,
        size.height * 0.6478663,
        size.width * 0.3388425,
        size.height * 0.6477570)
    ..lineTo(size.width * 0.3463000, size.height * 0.6477074)
    ..lineTo(size.width * 0.3476700, size.height * 0.6420558)
    ..cubicTo(
        size.width * 0.3493000,
        size.height * 0.6353167,
        size.width * 0.3520675,
        size.height * 0.6279071,
        size.width * 0.3594500,
        size.height * 0.6105055)
    ..lineTo(size.width * 0.3655425, size.height * 0.5961432)
    ..cubicTo(
        size.width * 0.3658700,
        size.height * 0.5953685,
        size.width * 0.3666000,
        size.height * 0.5906208,
        size.width * 0.3671650,
        size.height * 0.5855851)
    ..cubicTo(
        size.width * 0.3677275,
        size.height * 0.5805543,
        size.width * 0.3682900,
        size.height * 0.5755782,
        size.width * 0.3684150,
        size.height * 0.5745353)
    ..lineTo(size.width * 0.3686375, size.height * 0.5726332)
    ..lineTo(size.width * 0.3395200, size.height * 0.5726332)
    ..cubicTo(
        size.width * 0.3164100,
        size.height * 0.5726332,
        size.width * 0.3104375,
        size.height * 0.5728517,
        size.width * 0.3105825,
        size.height * 0.5736910)
    ..moveTo(size.width * 0.4151775, size.height * 0.5731398)
    ..cubicTo(
        size.width * 0.3965900,
        size.height * 0.5767353,
        size.width * 0.3834650,
        size.height * 0.5992968,
        size.width * 0.3844925,
        size.height * 0.6258809)
    ..cubicTo(
        size.width * 0.3855250,
        size.height * 0.6526388,
        size.width * 0.3932800,
        size.height * 0.6655013,
        size.width * 0.4153675,
        size.height * 0.6771023)
    ..lineTo(size.width * 0.4211300, size.height * 0.6801267)
    ..lineTo(size.width * 0.4315475, size.height * 0.6796202)
    ..cubicTo(
        size.width * 0.4372775,
        size.height * 0.6793371,
        size.width * 0.4453125,
        size.height * 0.6786716,
        size.width * 0.4494050,
        size.height * 0.6781353)
    ..cubicTo(
        size.width * 0.4600475,
        size.height * 0.6767398,
        size.width * 0.4693750,
        size.height * 0.6774748,
        size.width * 0.4774325,
        size.height * 0.6803304)
    ..cubicTo(
        size.width * 0.4828400,
        size.height * 0.6822523,
        size.width * 0.4841375,
        size.height * 0.6824758,
        size.width * 0.4848400,
        size.height * 0.6816017)
    ..cubicTo(
        size.width * 0.4868500,
        size.height * 0.6791087,
        size.width * 0.5127325,
        size.height * 0.6322178,
        size.width * 0.5124325,
        size.height * 0.6316168)
    ..cubicTo(
        size.width * 0.5115400,
        size.height * 0.6298489,
        size.width * 0.4745325,
        size.height * 0.6031009,
        size.width * 0.4579075,
        size.height * 0.5922150)
    ..cubicTo(
        size.width * 0.4345950,
        size.height * 0.5769489,
        size.width * 0.4227725,
        size.height * 0.5716698,
        size.width * 0.4151775,
        size.height * 0.5731398)
    ..moveTo(size.width * 0.6315300, size.height * 0.5741132)
    ..cubicTo(
        size.width * 0.6316450,
        size.height * 0.5749227,
        size.width * 0.6318725,
        size.height * 0.5769191,
        size.width * 0.6320350,
        size.height * 0.5785480)
    ..cubicTo(
        size.width * 0.6322675,
        size.height * 0.5808920,
        size.width * 0.6336150,
        size.height * 0.5840356,
        size.width * 0.6385500,
        size.height * 0.5937545)
    ..cubicTo(
        size.width * 0.6470125,
        size.height * 0.6104211,
        size.width * 0.6513900,
        size.height * 0.6228614,
        size.width * 0.6546225,
        size.height * 0.6394436)
    ..lineTo(size.width * 0.6562500, size.height * 0.6477918)
    ..lineTo(size.width * 0.6624325, size.height * 0.6477967)
    ..cubicTo(
        size.width * 0.6710950,
        size.height * 0.6478067,
        size.width * 0.6748325,
        size.height * 0.6456563,
        size.width * 0.6788025,
        size.height * 0.6383858)
    ..cubicTo(
        size.width * 0.6828825,
        size.height * 0.6309216,
        size.width * 0.6833100,
        size.height * 0.6286669,
        size.width * 0.6869975,
        size.height * 0.5952245)
    ..lineTo(size.width * 0.6894875, size.height * 0.5726332)
    ..lineTo(size.width * 0.6604050, size.height * 0.5726332)
    ..cubicTo(
        size.width * 0.6329100,
        size.height * 0.5726332,
        size.width * 0.6313325,
        size.height * 0.5727127,
        size.width * 0.6315300,
        size.height * 0.5741132)
    ..moveTo(size.width * 0.3812175, size.height * 0.5747190)
    ..cubicTo(
        size.width * 0.3744950,
        size.height * 0.5794816,
        size.width * 0.3739150,
        size.height * 0.5979609,
        size.width * 0.3802925,
        size.height * 0.6043226)
    ..cubicTo(
        size.width * 0.3833075,
        size.height * 0.6073271,
        size.width * 0.3836075,
        size.height * 0.6071285,
        size.width * 0.3854375,
        size.height * 0.6009108)
    ..cubicTo(
        size.width * 0.3863300,
        size.height * 0.5978814,
        size.width * 0.3881925,
        size.height * 0.5932529,
        size.width * 0.3895775,
        size.height * 0.5906308)
    ..lineTo(size.width * 0.3920950, size.height * 0.5858582)
    ..lineTo(size.width * 0.3914200, size.height * 0.5830474)
    ..cubicTo(
        size.width * 0.3909500,
        size.height * 0.5810907,
        size.width * 0.3904525,
        size.height * 0.5802315,
        size.width * 0.3897825,
        size.height * 0.5802266)
    ..cubicTo(
        size.width * 0.3887875,
        size.height * 0.5802166,
        size.width * 0.3846225,
        size.height * 0.5758662,
        size.width * 0.3841475,
        size.height * 0.5743416)
    ..cubicTo(
        size.width * 0.3837975,
        size.height * 0.5732242,
        size.width * 0.3832275,
        size.height * 0.5732937,
        size.width * 0.3812175,
        size.height * 0.5747190)
    ..moveTo(size.width * 0.6123225, size.height * 0.5756725)
    ..cubicTo(
        size.width * 0.6093525,
        size.height * 0.5786125,
        size.width * 0.6079925,
        size.height * 0.5830970,
        size.width * 0.6079925,
        size.height * 0.5899504)
    ..cubicTo(
        size.width * 0.6079925,
        size.height * 0.6107191,
        size.width * 0.6233100,
        size.height * 0.6119904,
        size.width * 0.6240650,
        size.height * 0.5912813)
    ..cubicTo(
        size.width * 0.6245425,
        size.height * 0.5781954,
        size.width * 0.6182375,
        size.height * 0.5698174,
        size.width * 0.6123225,
        size.height * 0.5756725)
    ..moveTo(size.width * 0.5852650, size.height * 0.6617716)
    ..cubicTo(
        size.width * 0.5805500,
        size.height * 0.6653225,
        size.width * 0.5785400,
        size.height * 0.6762928,
        size.width * 0.5810225,
        size.height * 0.6849142)
    ..cubicTo(
        size.width * 0.5847750,
        size.height * 0.6979604,
        size.width * 0.5941225,
        size.height * 0.6954524,
        size.width * 0.5960750,
        size.height * 0.6808816)
    ..cubicTo(
        size.width * 0.5977075,
        size.height * 0.6686896,
        size.width * 0.5912450,
        size.height * 0.6572623,
        size.width * 0.5852650,
        size.height * 0.6617716)
    ..moveTo(size.width * 0.4044000, size.height * 0.6796847)
    ..cubicTo(
        size.width * 0.3982925,
        size.height * 0.6912361,
        size.width * 0.4041225,
        size.height * 0.7113592,
        size.width * 0.4123075,
        size.height * 0.7069840)
    ..cubicTo(
        size.width * 0.4170000,
        size.height * 0.7044711,
        size.width * 0.4195500,
        size.height * 0.6942010,
        size.width * 0.4177200,
        size.height * 0.6851923)
    ..cubicTo(
        size.width * 0.4170800,
        size.height * 0.6820437,
        size.width * 0.4170175,
        size.height * 0.6819791,
        size.width * 0.4120875,
        size.height * 0.6792527)
    ..cubicTo(
        size.width * 0.4059775,
        size.height * 0.6758806,
        size.width * 0.4064250,
        size.height * 0.6758558,
        size.width * 0.4044000,
        size.height * 0.6796847)
    ..moveTo(size.width * 0.6358425, size.height * 0.7231092)
    ..cubicTo(
        size.width * 0.6306450,
        size.height * 0.7269729,
        size.width * 0.6285350,
        size.height * 0.7427208,
        size.width * 0.6323675,
        size.height * 0.7490378)
    ..cubicTo(
        size.width * 0.6338425,
        size.height * 0.7514613,
        size.width * 0.6459575,
        size.height * 0.7316561,
        size.width * 0.6450525,
        size.height * 0.7282989)
    ..cubicTo(
        size.width * 0.6438350,
        size.height * 0.7237797,
        size.width * 0.6387725,
        size.height * 0.7209291,
        size.width * 0.6358425,
        size.height * 0.7231092)
    ..moveTo(size.width * 0.3603325, size.height * 0.7300222)
    ..cubicTo(
        size.width * 0.3559300,
        size.height * 0.7340995,
        size.width * 0.3565925,
        size.height * 0.7364683,
        size.width * 0.3657625,
        size.height * 0.7493904)
    ..cubicTo(
        size.width * 0.3704200,
        size.height * 0.7559557,
        size.width * 0.3701125,
        size.height * 0.7558167,
        size.width * 0.3711500,
        size.height * 0.7517742)
    ..cubicTo(
        size.width * 0.3746800,
        size.height * 0.7380327,
        size.width * 0.3674300,
        size.height * 0.7234519,
        size.width * 0.3603325,
        size.height * 0.7300222)
    ..moveTo(size.width * 0.5401775, size.height * 0.7386088)
    ..cubicTo(
        size.width * 0.5322025,
        size.height * 0.7471457,
        size.width * 0.5347175,
        size.height * 0.7696674,
        size.width * 0.5436475,
        size.height * 0.7696674)
    ..cubicTo(
        size.width * 0.5515325,
        size.height * 0.7696674,
        size.width * 0.5546375,
        size.height * 0.7503141,
        size.width * 0.5483475,
        size.height * 0.7403718)
    ..cubicTo(
        size.width * 0.5463525,
        size.height * 0.7372182,
        size.width * 0.5422975,
        size.height * 0.7363442,
        size.width * 0.5401775,
        size.height * 0.7386088)
    ..moveTo(size.width * 0.1352050, size.height * 0.7394878)
    ..cubicTo(
        size.width * 0.1348525,
        size.height * 0.7396368,
        size.width * 0.1332775,
        size.height * 0.7400043,
        size.width * 0.1317025,
        size.height * 0.7403022)
    ..cubicTo(
        size.width * 0.1263800,
        size.height * 0.7413005,
        size.width * 0.1262775,
        size.height * 0.7430585,
        size.width * 0.1313000,
        size.height * 0.7472748)
    ..cubicTo(
        size.width * 0.1365925,
        size.height * 0.7517245,
        size.width * 0.1363200,
        size.height * 0.7517841,
        size.width * 0.1369250,
        size.height * 0.7460084)
    ..cubicTo(
        size.width * 0.1372025,
        size.height * 0.7433515,
        size.width * 0.1375225,
        size.height * 0.7406896,
        size.width * 0.1376375,
        size.height * 0.7400937)
    ..cubicTo(
        size.width * 0.1378375,
        size.height * 0.7390607,
        size.width * 0.1368025,
        size.height * 0.7388024,
        size.width * 0.1352050,
        size.height * 0.7394878)
    ..moveTo(size.width * 0.7557700, size.height * 0.7513471)
    ..cubicTo(
        size.width * 0.7405025,
        size.height * 0.7540884,
        size.width * 0.7266475,
        size.height * 0.7585630,
        size.width * 0.7195675,
        size.height * 0.7630475)
    ..cubicTo(
        size.width * 0.7093025,
        size.height * 0.7695433,
        size.width * 0.7047700,
        size.height * 0.7869051,
        size.width * 0.7083125,
        size.height * 0.8061641)
    ..cubicTo(
        size.width * 0.7123975,
        size.height * 0.8283630,
        size.width * 0.7153400,
        size.height * 0.8412950,
        size.width * 0.7162000,
        size.height * 0.8408133)
    ..cubicTo(
        size.width * 0.7185700,
        size.height * 0.8394823,
        size.width * 0.7323025,
        size.height * 0.8343274,
        size.width * 0.7380425,
        size.height * 0.8326091)
    ..cubicTo(
        size.width * 0.7455225,
        size.height * 0.8303743,
        size.width * 0.7454900,
        size.height * 0.8304240,
        size.width * 0.7437800,
        size.height * 0.8235905)
    ..cubicTo(
        size.width * 0.7426050,
        size.height * 0.8188875,
        size.width * 0.7425800,
        size.height * 0.8185498,
        size.width * 0.7431325,
        size.height * 0.8144874)
    ..lineTo(size.width * 0.7437050, size.height * 0.8102711)
    ..lineTo(size.width * 0.7553875, size.height * 0.8106535)
    ..lineTo(size.width * 0.7670700, size.height * 0.8110309)
    ..lineTo(size.width * 0.7667975, size.height * 0.8047983)
    ..cubicTo(
        size.width * 0.7653750,
        size.height * 0.7721555,
        size.width * 0.7637100,
        size.height * 0.7508505,
        size.width * 0.7625425,
        size.height * 0.7503787)
    ..cubicTo(
        size.width * 0.7623075,
        size.height * 0.7502843,
        size.width * 0.7592600,
        size.height * 0.7507213,
        size.width * 0.7557700,
        size.height * 0.7513471)
    ..moveTo(size.width * 0.4500225, size.height * 0.7577038)
    ..cubicTo(
        size.width * 0.4425475,
        size.height * 0.7610510,
        size.width * 0.4415950,
        size.height * 0.7825299,
        size.width * 0.4486625,
        size.height * 0.7883950)
    ..cubicTo(
        size.width * 0.4556850,
        size.height * 0.7942253,
        size.width * 0.4627875,
        size.height * 0.7793863,
        size.width * 0.4592600,
        size.height * 0.7662507)
    ..cubicTo(
        size.width * 0.4586950,
        size.height * 0.7641450,
        size.width * 0.4518625,
        size.height * 0.7555882,
        size.width * 0.4517500,
        size.height * 0.7568447)
    ..cubicTo(
        size.width * 0.4517475,
        size.height * 0.7568943,
        size.width * 0.4509700,
        size.height * 0.7572817,
        size.width * 0.4500225,
        size.height * 0.7577038)
    ..moveTo(size.width * 0.8206525, size.height * 0.7852017)
    ..cubicTo(
        size.width * 0.8158175,
        size.height * 0.7899395,
        size.width * 0.8152925,
        size.height * 0.7985211,
        size.width * 0.8139925,
        size.height * 0.8944234)
    ..cubicTo(
        size.width * 0.8132700,
        size.height * 0.9478052,
        size.width * 0.8126125,
        size.height * 0.9849325,
        size.width * 0.8122325,
        size.height * 0.9938916)
    ..lineTo(size.width * 0.8119725, size.height * 1.000030)
    ..lineTo(size.width * 0.8212175, size.height * 0.9998014)
    ..lineTo(size.width * 0.8304625, size.height * 0.9995779)
    ..lineTo(size.width * 0.8334125, size.height * 0.9847935)
    ..cubicTo(
        size.width * 0.8496100,
        size.height * 0.9036556,
        size.width * 0.8513100,
        size.height * 0.8536211,
        size.width * 0.8394800,
        size.height * 0.8062882)
    ..cubicTo(
        size.width * 0.8347975,
        size.height * 0.7875557,
        size.width * 0.8270800,
        size.height * 0.7789095,
        size.width * 0.8206525,
        size.height * 0.7852017)
    ..moveTo(size.width * 0.9462150, size.height * 0.8053546)
    ..cubicTo(
        size.width * 0.9045950,
        size.height * 0.8094715,
        size.width * 0.8810125,
        size.height * 0.8120987,
        size.width * 0.8807325,
        size.height * 0.8126549)
    ..cubicTo(
        size.width * 0.8801425,
        size.height * 0.8138219,
        size.width * 0.8781750,
        size.height * 0.8336123,
        size.width * 0.8786475,
        size.height * 0.8336123)
    ..cubicTo(
        size.width * 0.8792675,
        size.height * 0.8336123,
        size.width * 0.9862625,
        size.height * 0.8075794,
        size.width * 0.9872450,
        size.height * 0.8071921)
    ..cubicTo(
        size.width * 0.9921150,
        size.height * 0.8052602,
        size.width * 0.9611750,
        size.height * 0.8038746,
        size.width * 0.9462150,
        size.height * 0.8053546)
    ..moveTo(size.width * 0.9383500, size.height * 0.8243553)
    ..cubicTo(
        size.width * 0.9047950,
        size.height * 0.8326240,
        size.width * 0.8773000,
        size.height * 0.8394227,
        size.width * 0.8772525,
        size.height * 0.8394575)
    ..cubicTo(
        size.width * 0.8772050,
        size.height * 0.8394972,
        size.width * 0.8767975,
        size.height * 0.8411410,
        size.width * 0.8763475,
        size.height * 0.8431126)
    ..cubicTo(
        size.width * 0.8734325,
        size.height * 0.8559254,
        size.width * 0.8684675,
        size.height * 0.8616813,
        size.width * 0.8554425,
        size.height * 0.8673527)
    ..lineTo(size.width * 0.8501275, size.height * 0.8696669)
    ..lineTo(size.width * 0.8498400, size.height * 0.8805578)
    ..cubicTo(
        size.width * 0.8496825,
        size.height * 0.8865470,
        size.width * 0.8495850,
        size.height * 0.8914835,
        size.width * 0.8496275,
        size.height * 0.8915282)
    ..cubicTo(
        size.width * 0.8496850,
        size.height * 0.8915877,
        size.width * 0.9994125,
        size.height * 0.8541177,
        size.width * 0.9998925,
        size.height * 0.8539240)
    ..cubicTo(
        size.width * 1.000173,
        size.height * 0.8538148,
        size.width * 0.9999600,
        size.height * 0.8091338,
        size.width * 0.9996800,
        size.height * 0.8092183)
    ..cubicTo(
        size.width * 0.9995050,
        size.height * 0.8092729,
        size.width * 0.9719075,
        size.height * 0.8160865,
        size.width * 0.9383500,
        size.height * 0.8243553)
    ..moveTo(size.width * 0.7455425, size.height * 0.8188328)
    ..cubicTo(
        size.width * 0.7460825,
        size.height * 0.8206902,
        size.width * 0.7467625,
        size.height * 0.8238239,
        size.width * 0.7470550,
        size.height * 0.8258004)
    ..cubicTo(
        size.width * 0.7476850,
        size.height * 0.8300763,
        size.width * 0.7467125,
        size.height * 0.8299422,
        size.width * 0.7577125,
        size.height * 0.8272208)
    ..cubicTo(
        size.width * 0.7625375,
        size.height * 0.8260289,
        size.width * 0.7726800,
        size.height * 0.8237841,
        size.width * 0.7802450,
        size.height * 0.8222347)
    ..lineTo(size.width * 0.7940050, size.height * 0.8194189)
    ..lineTo(size.width * 0.7806775, size.height * 0.8193344)
    ..cubicTo(
        size.width * 0.7677975,
        size.height * 0.8192599,
        size.width * 0.7673450,
        size.height * 0.8192003,
        size.width * 0.7671325,
        size.height * 0.8175913)
    ..cubicTo(
        size.width * 0.7669250,
        size.height * 0.8159971,
        size.width * 0.7664450,
        size.height * 0.8159177,
        size.width * 0.7557375,
        size.height * 0.8156942)
    ..lineTo(size.width * 0.7445600, size.height * 0.8154558)
    ..lineTo(size.width * 0.7455425, size.height * 0.8188328)
    ..moveTo(size.width * 0.8005950, size.height * 0.8236302)
    ..cubicTo(
        size.width * 0.7616650,
        size.height * 0.8309702,
        size.width * 0.7413375,
        size.height * 0.8359861,
        size.width * 0.7255525,
        size.height * 0.8421392)
    ..cubicTo(
        size.width * 0.7175725,
        size.height * 0.8452481,
        size.width * 0.7088725,
        size.height * 0.8500057,
        size.width * 0.7106725,
        size.height * 0.8502739)
    ..cubicTo(
        size.width * 0.7113725,
        size.height * 0.8503732,
        size.width * 0.7274450,
        size.height * 0.8520518,
        size.width * 0.7463850,
        size.height * 0.8539985)
    ..lineTo(size.width * 0.7808250, size.height * 0.8575345)
    ..lineTo(size.width * 0.7942175, size.height * 0.8543809)
    ..cubicTo(
        size.width * 0.8015850,
        size.height * 0.8526477,
        size.width * 0.8078275,
        size.height * 0.8510933,
        size.width * 0.8080925,
        size.height * 0.8509294)
    ..cubicTo(
        size.width * 0.8084325,
        size.height * 0.8507208,
        size.width * 0.8083525,
        size.height * 0.8487989,
        size.width * 0.8078125,
        size.height * 0.8442946)
    ..arcToPoint(Offset(size.width * 0.8065050, size.height * 0.8302948),
        radius:
            Radius.elliptical(size.width * 0.09247500, size.height * 0.1836999),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..cubicTo(
        size.width * 0.8060775,
        size.height * 0.8242808,
        size.width * 0.8058175,
        size.height * 0.8226568,
        size.width * 0.8052975,
        size.height * 0.8227512)
    ..cubicTo(
        size.width * 0.8049325,
        size.height * 0.8228157,
        size.width * 0.8028175,
        size.height * 0.8232081,
        size.width * 0.8005950,
        size.height * 0.8236302)
    ..moveTo(size.width * 0.07315750, size.height * 0.8548428)
    ..cubicTo(
        size.width * 0.07669250,
        size.height * 0.8567697,
        size.width * 0.07753750,
        size.height * 0.8581553,
        size.width * 0.07647000,
        size.height * 0.8602758)
    ..cubicTo(
        size.width * 0.07589750,
        size.height * 0.8614131,
        size.width * 0.06973000,
        size.height * 0.8589300,
        size.width * 0.06807250,
        size.height * 0.8568938)
    ..cubicTo(
        size.width * 0.06691250,
        size.height * 0.8554735,
        size.width * 0.06786250,
        size.height * 0.8518333,
        size.width * 0.06918000,
        size.height * 0.8526428)
    ..cubicTo(
        size.width * 0.06946250,
        size.height * 0.8528166,
        size.width * 0.07125250,
        size.height * 0.8538098,
        size.width * 0.07315750,
        size.height * 0.8548428)
    ..moveTo(size.width * 0.7023800, size.height * 0.8733071)
    ..cubicTo(
        size.width * 0.7023800,
        size.height * 0.8835276,
        size.width * 0.7025250,
        size.height * 0.8918857,
        size.width * 0.7027000,
        size.height * 0.8918758)
    ..cubicTo(
        size.width * 0.7028750,
        size.height * 0.8918708,
        size.width * 0.7193775,
        size.height * 0.8959282,
        size.width * 0.7393700,
        size.height * 0.9008994)
    ..lineTo(size.width * 0.7757225, size.height * 0.9099379)
    ..lineTo(size.width * 0.7933200, size.height * 0.9055825)
    ..cubicTo(
        size.width * 0.8030000,
        size.height * 0.9031838,
        size.width * 0.8109750,
        size.height * 0.9011129,
        size.width * 0.8110425,
        size.height * 0.9009788)
    ..cubicTo(
        size.width * 0.8111075,
        size.height * 0.9008497,
        size.width * 0.8112975,
        size.height * 0.8929535,
        size.width * 0.8114600,
        size.height * 0.8834332)
    ..lineTo(size.width * 0.8117550, size.height * 0.8661310)
    ..lineTo(size.width * 0.8106400, size.height * 0.8619941)
    ..cubicTo(
        size.width * 0.8100250,
        size.height * 0.8597146,
        size.width * 0.8095250,
        size.height * 0.8573656,
        size.width * 0.8095250,
        size.height * 0.8567647)
    ..cubicTo(
        size.width * 0.8095250,
        size.height * 0.8560148,
        size.width * 0.8090950,
        size.height * 0.8558112,
        size.width * 0.8081425,
        size.height * 0.8561042)
    ..cubicTo(
        size.width * 0.8073825,
        size.height * 0.8563376,
        size.width * 0.8009125,
        size.height * 0.8579367,
        size.width * 0.7937675,
        size.height * 0.8596650)
    ..lineTo(size.width * 0.7807725, size.height * 0.8627987)
    ..lineTo(size.width * 0.7429575, size.height * 0.8588108)
    ..cubicTo(
        size.width * 0.7221600,
        size.height * 0.8566108,
        size.width * 0.7045225,
        size.height * 0.8547981,
        size.width * 0.7037625,
        size.height * 0.8547733)
    ..lineTo(size.width * 0.7023800, size.height * 0.8547286)
    ..lineTo(size.width * 0.7023800, size.height * 0.8733071)
    ..moveTo(size.width * 0.1352050, size.height * 0.8661310)
    ..lineTo(size.width * 0.1352050, size.height * 0.8741514)
    ..lineTo(size.width * 0.1607150, size.height * 0.8741514)
    ..lineTo(size.width * 0.1607150, size.height * 0.8581056)
    ..lineTo(size.width * 0.1352050, size.height * 0.8581056)
    ..lineTo(size.width * 0.1352050, size.height * 0.8661310)
    ..moveTo(size.width * 0.1709175, size.height * 0.8662402)
    ..lineTo(size.width * 0.1709175, size.height * 0.8743749)
    ..lineTo(size.width * 0.1753775, size.height * 0.8739825)
    ..cubicTo(
        size.width * 0.1819450,
        size.height * 0.8734015,
        size.width * 0.1860425,
        size.height * 0.8685942,
        size.width * 0.1872825,
        size.height * 0.8600076)
    ..lineTo(size.width * 0.1875575, size.height * 0.8581056)
    ..lineTo(size.width * 0.1709175, size.height * 0.8581056)
    ..lineTo(size.width * 0.1709175, size.height * 0.8662402)
    ..moveTo(size.width * 0.9872450, size.height * 0.8624461)
    ..cubicTo(
        size.width * 0.9803475,
        size.height * 0.8641594,
        size.width * 0.9728375,
        size.height * 0.8660168,
        size.width * 0.9705575,
        size.height * 0.8665730)
    ..lineTo(size.width * 0.9664125, size.height * 0.8675861)
    ..lineTo(size.width * 0.9664125, size.height * 0.9941995)
    ..lineTo(size.width * 0.9673675, size.height * 0.9936284)
    ..cubicTo(
        size.width * 0.9747075,
        size.height * 0.9892581,
        size.width * 0.9862050,
        size.height * 0.9712904,
        size.width * 0.9911250,
        size.height * 0.9565010)
    ..cubicTo(
        size.width * 0.9984575,
        size.height * 0.9344709,
        size.width * 1.000067,
        size.height * 0.9212161,
        size.width * 0.9999000,
        size.height * 0.8842427)
    ..lineTo(size.width * 0.9997875, size.height * 0.8593372)
    ..lineTo(size.width * 0.9872450, size.height * 0.8624461)
    ..moveTo(size.width * 0.06142250, size.height * 0.8678145)
    ..cubicTo(
        size.width * 0.06861000,
        size.height * 0.8691653,
        size.width * 0.07018500,
        size.height * 0.8702331,
        size.width * 0.06886250,
        size.height * 0.8728552)
    ..cubicTo(
        size.width * 0.06823750,
        size.height * 0.8740918,
        size.width * 0.05312750,
        size.height * 0.8718967,
        size.width * 0.05189750,
        size.height * 0.8703870)
    ..cubicTo(
        size.width * 0.05065750,
        size.height * 0.8688624,
        size.width * 0.05190750,
        size.height * 0.8656294,
        size.width * 0.05352750,
        size.height * 0.8661707)
    ..cubicTo(
        size.width * 0.05413500,
        size.height * 0.8663743,
        size.width * 0.05769000,
        size.height * 0.8671143,
        size.width * 0.06142250,
        size.height * 0.8678145)
    ..moveTo(size.width * 0.9609900, size.height * 0.8688525)
    ..lineTo(size.width * 0.9596100, size.height * 0.8691951)
    ..lineTo(size.width * 0.9596225, size.height * 0.9056669)
    ..cubicTo(
        size.width * 0.9596350,
        size.height * 0.9364028,
        size.width * 0.9597350,
        size.height * 0.9424020,
        size.width * 0.9602600,
        size.height * 0.9437826)
    ..cubicTo(
        size.width * 0.9606025,
        size.height * 0.9446864,
        size.width * 0.9608850,
        size.height * 0.9466083,
        size.width * 0.9608850,
        size.height * 0.9480535)
    ..cubicTo(
        size.width * 0.9608850,
        size.height * 0.9495036,
        size.width * 0.9606025,
        size.height * 0.9514255,
        size.width * 0.9602600,
        size.height * 0.9523294)
    ..cubicTo(
        size.width * 0.9597500,
        size.height * 0.9536703,
        size.width * 0.9596325,
        size.height * 0.9579859,
        size.width * 0.9596225,
        size.height * 0.9761523)
    ..lineTo(size.width * 0.9596100, size.height * 0.9983413)
    ..lineTo(size.width * 0.9607775, size.height * 0.9977801)
    ..cubicTo(
        size.width * 0.9614200,
        size.height * 0.9974672,
        size.width * 0.9623775,
        size.height * 0.9969011,
        size.width * 0.9629050,
        size.height * 0.9965187)
    ..lineTo(size.width * 0.9638600, size.height * 0.9958185)
    ..lineTo(size.width * 0.9638600, size.height * 0.9320276)
    ..cubicTo(
        size.width * 0.9638600,
        size.height * 0.8608867,
        size.width * 0.9641875,
        size.height * 0.8680628,
        size.width * 0.9609900,
        size.height * 0.8688525)
    ..moveTo(size.width * 0.9026975, size.height * 0.8834779)
    ..cubicTo(
        size.width * 0.8733825,
        size.height * 0.8908378,
        size.width * 0.8493400,
        size.height * 0.8969761,
        size.width * 0.8492675,
        size.height * 0.8971201)
    ..cubicTo(
        size.width * 0.8491975,
        size.height * 0.8972641,
        size.width * 0.8485500,
        size.height * 0.9030944,
        size.width * 0.8478300,
        size.height * 0.9100769)
    ..cubicTo(
        size.width * 0.8462600,
        size.height * 0.9252983,
        size.width * 0.8436025,
        size.height * 0.9440458,
        size.width * 0.8404100,
        size.height * 0.9623959)
    ..cubicTo(
        size.width * 0.8391200,
        size.height * 0.9698154,
        size.width * 0.8381300,
        size.height * 0.9760232,
        size.width * 0.8382100,
        size.height * 0.9761821)
    ..cubicTo(
        size.width * 0.8382925,
        size.height * 0.9763460,
        size.width * 0.8650650,
        size.height * 0.9681815,
        size.width * 0.8977050,
        size.height * 0.9580455)
    ..lineTo(size.width * 0.9570525, size.height * 0.9396109)
    ..lineTo(size.width * 0.9570550, size.height * 0.9047681)
    ..cubicTo(
        size.width * 0.9570575,
        size.height * 0.8774142,
        size.width * 0.9569425,
        size.height * 0.8699500,
        size.width * 0.9565275,
        size.height * 0.8700146)
    ..cubicTo(
        size.width * 0.9562350,
        size.height * 0.8700592,
        size.width * 0.9320100,
        size.height * 0.8761180,
        size.width * 0.9026975,
        size.height * 0.8834779)
    ..moveTo(size.width * 0.7266150, size.height * 0.9033626)
    ..cubicTo(
        size.width * 0.7266150,
        size.height * 0.9036209,
        size.width * 0.7277450,
        size.height * 0.9109261,
        size.width * 0.7291250,
        size.height * 0.9196071)
    ..cubicTo(
        size.width * 0.7351125,
        size.height * 0.9572509,
        size.width * 0.7378100,
        size.height * 0.9804927,
        size.width * 0.7375925,
        size.height * 0.9925904)
    ..lineTo(size.width * 0.7374575, size.height * 1.000050)
    ..lineTo(size.width * 0.7423425, size.height * 0.9998113)
    ..lineTo(size.width * 0.7472275, size.height * 0.9995779)
    ..lineTo(size.width * 0.7478700, size.height * 0.9970401)
    ..cubicTo(
        size.width * 0.7484175,
        size.height * 0.9948798,
        size.width * 0.7485125,
        size.height * 0.9881904,
        size.width * 0.7485125,
        size.height * 0.9514504)
    ..lineTo(size.width * 0.7485125, size.height * 0.9083934)
    ..lineTo(size.width * 0.7393700, size.height * 0.9061338)
    ..cubicTo(
        size.width * 0.7343425,
        size.height * 0.9048922,
        size.width * 0.7294175,
        size.height * 0.9036606,
        size.width * 0.7284225,
        size.height * 0.9033924)
    ..cubicTo(
        size.width * 0.7274300,
        size.height * 0.9031193,
        size.width * 0.7266150,
        size.height * 0.9031093,
        size.width * 0.7266150,
        size.height * 0.9033626)
    ..moveTo(size.width * 0.7929425, size.height * 0.9108616)
    ..lineTo(size.width * 0.7757225, size.height * 0.9152964)
    ..lineTo(size.width * 0.7678575, size.height * 0.9133099)
    ..cubicTo(
        size.width * 0.7635300,
        size.height * 0.9122174,
        size.width * 0.7596575,
        size.height * 0.9113234,
        size.width * 0.7592475,
        size.height * 0.9113185)
    ..cubicTo(
        size.width * 0.7585475,
        size.height * 0.9113135,
        size.width * 0.7585025,
        size.height * 0.9139903,
        size.width * 0.7585025,
        size.height * 0.9556568)
    ..lineTo(size.width * 0.7585025, size.height * 1.000000)
    ..lineTo(size.width * 0.7606575, size.height * 1.000000)
    ..cubicTo(
        size.width * 0.7622575,
        size.height * 1.000000,
        size.width * 0.8094300,
        size.height * 0.9859109,
        size.width * 0.8098025,
        size.height * 0.9853199)
    ..cubicTo(
        size.width * 0.8098375,
        size.height * 0.9852653,
        size.width * 0.8100750,
        size.height * 0.9732471,
        size.width * 0.8103300,
        size.height * 0.9586116)
    ..cubicTo(
        size.width * 0.8105825,
        size.height * 0.9439812,
        size.width * 0.8109050,
        size.height * 0.9262121,
        size.width * 0.8110450,
        size.height * 0.9191303)
    ..cubicTo(
        size.width * 0.8112500,
        size.height * 0.9086665,
        size.width * 0.8111925,
        size.height * 0.9062629,
        size.width * 0.8107300,
        size.height * 0.9063374)
    ..cubicTo(
        size.width * 0.8104175,
        size.height * 0.9063821,
        size.width * 0.8024125,
        size.height * 0.9084182,
        size.width * 0.7929425,
        size.height * 0.9108616)
    ..moveTo(size.width * 0.7517000, size.height * 0.9548125)
    ..lineTo(size.width * 0.7517000, size.height * 1.000000)
    ..lineTo(size.width * 0.7559525, size.height * 1.000000)
    ..lineTo(size.width * 0.7559525, size.height * 0.9553240)
    ..cubicTo(
        size.width * 0.7559525,
        size.height * 0.9058507,
        size.width * 0.7561825,
        size.height * 0.9096250,
        size.width * 0.7531550,
        size.height * 0.9096250)
    ..lineTo(size.width * 0.7517000, size.height * 0.9096250)
    ..lineTo(size.width * 0.7517000, size.height * 0.9548125)
    ..moveTo(size.width * 0.8960875, size.height * 0.9636970)
    ..cubicTo(
        size.width * 0.8636075,
        size.height * 0.9738629,
        size.width * 0.8368900,
        size.height * 0.9824842,
        size.width * 0.8367175,
        size.height * 0.9828517)
    ..cubicTo(
        size.width * 0.8360450,
        size.height * 0.9843018,
        size.width * 0.8358200,
        size.height * 0.9873312,
        size.width * 0.8363875,
        size.height * 0.9873163)
    ..cubicTo(
        size.width * 0.8396250,
        size.height * 0.9872468,
        size.width * 0.9572975,
        size.height * 0.9508991,
        size.width * 0.9578525,
        size.height * 0.9497917)
    ..cubicTo(
        size.width * 0.9587875,
        size.height * 0.9479393,
        size.width * 0.9579975,
        size.height * 0.9455704,
        size.width * 0.9563775,
        size.height * 0.9453668)
    ..cubicTo(
        size.width * 0.9557000,
        size.height * 0.9452824,
        size.width * 0.9285700,
        size.height * 0.9535312,
        size.width * 0.8960875,
        size.height * 0.9636970)
    ..moveTo(size.width * 0.8947700, size.height * 0.9751243)
    ..arcToPoint(Offset(size.width * 0.8349050, size.height * 0.9931963),
        radius:
            Radius.elliptical(size.width * 33.27000, size.height * 66.09026),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..cubicTo(
        size.width * 0.8346050,
        size.height * 0.9932360,
        size.width * 0.8333325,
        size.height * 0.9983761,
        size.width * 0.8333325,
        size.height * 0.9995481)
    ..cubicTo(
        size.width * 0.8333325,
        size.height * 1.000189,
        size.width * 0.9541200,
        size.height * 1.000109,
        size.width * 0.9557300,
        size.height * 0.9994686)
    ..lineTo(size.width * 0.9570575, size.height * 0.9989422)
    ..lineTo(size.width * 0.9570575, size.height * 0.9569231)
    ..lineTo(size.width * 0.9556750, size.height * 0.9570125)
    ..cubicTo(
        size.width * 0.9549150,
        size.height * 0.9570622,
        size.width * 0.9275075,
        size.height * 0.9652117,
        size.width * 0.8947700,
        size.height * 0.9751243)
    ..moveTo(size.width * 0.7946425, size.height * 0.9953566)
    ..lineTo(size.width * 0.7808250, size.height * 0.9998262)
    ..lineTo(size.width * 0.7878400, size.height * 0.9998957)
    ..cubicTo(
        size.width * 0.7933050,
        size.height * 0.9999454,
        size.width * 0.7963575,
        size.height * 0.9995034,
        size.width * 0.8016575,
        size.height * 0.9978794)
    ..cubicTo(
        size.width * 0.8054000,
        size.height * 0.9967322,
        size.width * 0.8087000,
        size.height * 0.9957936,
        size.width * 0.8089925,
        size.height * 0.9957887)
    ..cubicTo(
        size.width * 0.8092975,
        size.height * 0.9957787,
        size.width * 0.8095250,
        size.height * 0.9947011,
        size.width * 0.8095250,
        size.height * 0.9932410)
    ..cubicTo(
        size.width * 0.8095250,
        size.height * 0.9917263,
        size.width * 0.8093100,
        size.height * 0.9907430,
        size.width * 0.8089925,
        size.height * 0.9907976)
    ..cubicTo(
        size.width * 0.8087000,
        size.height * 0.9908523,
        size.width * 0.8022425,
        size.height * 0.9929033,
        size.width * 0.7946425,
        size.height * 0.9953566);
}

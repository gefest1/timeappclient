// import 'dart:convert';

// import 'package:equatable/equatable.dart';
// import 'package:li/data/models/service_model.dart';

// class Service extends Equatable {
//   final String? description;
//   final String? id;
//   final String? institutionId;
//   final String? name;
//   final num? price;
//   final String? specialistId;
//   final num? bufferTimeMinutes;

//   const Service({
//     this.description,
//     this.id,
//     this.institutionId,
//     this.name,
//     this.price,
//     this.specialistId,
//     this.bufferTimeMinutes,
//   });

//   Service copyWith(
//       {String? description,
//       String? id,
//       String? institutionId,
//       String? name,
//       num? price,
//       String? specialistId,
//       num? bufferTimeMinutes}) {
//     return Service(
//       description: description ?? this.description,
//       id: id ?? this.id,
//       institutionId: institutionId ?? this.institutionId,
//       name: name ?? this.name,
//       price: price ?? this.price,
//       specialistId: specialistId ?? this.specialistId,
//       bufferTimeMinutes: bufferTimeMinutes ?? this.bufferTimeMinutes,
//     );
//   }

//   Map<String, dynamic> toMap() {
//     ServiceMinute
//     return {
//       'description': description,
//       'id': id,
//       'institutionId': institutionId,
//       'name': name,
//       'price': price,
//       'specialistId': specialistId,
//       'bufferTimeMinutes': bufferTimeMinutes?.toInt(),
//     };
//   }

//   static Service? fromMap(Map<String, dynamic>? map) {
//     if (map == null) return null;
//     return Service(
//       description: map['description'],
//       id: map['id'],
//       institutionId: map['institutionId'],
//       name: map['name'],
//       price: map['price'],
//       specialistId: map['specialistId'],
//       bufferTimeMinutes: map['bufferTimeMinutes'],
//     );
//   }

//   String toJson() => json.encode(toMap());

//   factory Service.fromJson(String source) =>
//       Service.fromMap(json.decode(source))!;

//   @override
//   bool get stringify => true;

//   @override
//   List<Object?> get props {
//     return [
//       description,
//       id,
//       institutionId,
//       name,
//       price,
//       specialistId,
//       bufferTimeMinutes,
//     ];
//   }
// }

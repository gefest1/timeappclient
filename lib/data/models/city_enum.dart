class CityEnum {
  final String _value;

  const CityEnum._internal(this._value);

  String toString() => _value;
  String toMap() => _value;

  static const Almaty = const CityEnum._internal('Almaty');
  static const Astana = const CityEnum._internal('Astana');
  static CityEnum? fromMap(String? value) {
    if ('Almaty' == value) return Almaty;
    if ('Astana' == value) return Astana;
    return null;
  }
}

import 'package:flutter/material.dart';
import 'package:li/utils/color.dart';

class StarWidget extends StatelessWidget {
  final bool choosen;

  const StarWidget({
    required this.choosen,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: StarCustomPainter(
        color: choosen ? ColorData.clientsIcons : ColorData.allInnerInactive,
      ),
      size: Size(48, 48),
    );
  }
}

class StarCustomPainter extends CustomPainter {
  final Color color;
  const StarCustomPainter({
    required this.color,
    Listenable? repaint,
  }) : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.4570744, size.height * 0.1568795);
    path_0.cubicTo(
        size.width * 0.4729907,
        size.height * 0.1196207,
        size.width * 0.5270023,
        size.height * 0.1196207,
        size.width * 0.5429186,
        size.height * 0.1568795);
    path_0.lineTo(size.width * 0.6171977, size.height * 0.3307727);
    path_0.cubicTo(
        size.width * 0.6235581,
        size.height * 0.3456614,
        size.width * 0.6375186,
        size.height * 0.3561409,
        size.width * 0.6538814,
        size.height * 0.3583045);
    path_0.lineTo(size.width * 0.8521326, size.height * 0.3845273);
    path_0.cubicTo(
        size.width * 0.8924535,
        size.height * 0.3898614,
        size.width * 0.9068256,
        size.height * 0.4396477,
        size.width * 0.8753000,
        size.height * 0.4647886);
    path_0.lineTo(size.width * 0.7232302, size.height * 0.5860636);
    path_0.cubicTo(
        size.width * 0.7094953,
        size.height * 0.5970159,
        size.width * 0.7033256,
        size.height * 0.6146500,
        size.width * 0.7073209,
        size.height * 0.6315273);
    path_0.lineTo(size.width * 0.7558791, size.height * 0.8365841);
    path_0.cubicTo(
        size.width * 0.7654953,
        size.height * 0.8772023,
        size.width * 0.7187605,
        size.height * 0.9079886,
        size.width * 0.6838442,
        size.height * 0.8840386);
    path_0.lineTo(size.width * 0.5267163, size.height * 0.7762636);
    path_0.cubicTo(
        size.width * 0.5106814,
        size.height * 0.7652636,
        size.width * 0.4893116,
        size.height * 0.7652636,
        size.width * 0.4732767,
        size.height * 0.7762636);
    path_0.lineTo(size.width * 0.3161465, size.height * 0.8840409);
    path_0.cubicTo(
        size.width * 0.2812302,
        size.height * 0.9079886,
        size.width * 0.2344953,
        size.height * 0.8772045,
        size.width * 0.2441116,
        size.height * 0.8365886);
    path_0.lineTo(size.width * 0.2926651, size.height * 0.6315273);
    path_0.cubicTo(
        size.width * 0.2966605,
        size.height * 0.6146477,
        size.width * 0.2904930,
        size.height * 0.5970182,
        size.width * 0.2767581,
        size.height * 0.5860636);
    path_0.lineTo(size.width * 0.1246893, size.height * 0.4647886);
    path_0.cubicTo(
        size.width * 0.09316535,
        size.height * 0.4396477,
        size.width * 0.1075372,
        size.height * 0.3898614,
        size.width * 0.1478579,
        size.height * 0.3845273);
    path_0.lineTo(size.width * 0.3461116, size.height * 0.3583045);
    path_0.cubicTo(
        size.width * 0.3624744,
        size.height * 0.3561409,
        size.width * 0.3764349,
        size.height * 0.3456614,
        size.width * 0.3827953,
        size.height * 0.3307727);
    path_0.lineTo(size.width * 0.4570744, size.height * 0.1568795);
    path_0.close();

    final Paint paintStroke = Paint()
      // ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.04651163;
    paintStroke.color = this.color;
    paintStroke.strokeCap = StrokeCap.round;
    paintStroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_0, paintStroke);
  }

  @override
  bool shouldRepaint(covariant StarCustomPainter oldDelegate) {
    return oldDelegate.color != color;
  }
}

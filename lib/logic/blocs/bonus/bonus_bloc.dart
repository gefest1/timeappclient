import 'dart:async';
import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/logic/blocs/get_debt/getdebt_bloc.dart';
import 'package:li/network/graphql_provider.dart';
import 'package:li/template/bloc_template.dart';

part 'bonus_event.dart';
part 'bonus_state.dart';

part './reactions/enterPromocode_reaction.dart';

class BonusBloc extends Bloc<BonusEvent, BonusState> {
  final GraphqlProvider graphqlProvider;

  BonusBloc(
    this.graphqlProvider,
  ) : super(BonusInitial()) {
    on<EnterPromoCode>(usePromocode);
  }
}

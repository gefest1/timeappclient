import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

part 'bottom_nav_update.dart';

const grayScaleColorPlaceHolder = Color(0xffA0A3BD);

const primaryColor = Color(0xFF32D25F);

const List<String> svgAssets = [
  'assets/svg/icon/bottom/compass.svg',
  // 'assets/svg/icon/bottom/heart.svg',
  // 'assets/svg/icon/bottom/calendar.svg',
  // 'assets/svg/icon/bottom/comment.svg',
  'assets/svg/icon/bottom/profile.svg',
];
const List<String?> svgAssetsActive = [
  null,
  null,
  null,
  null,
  null,
];

class BottomNavBar extends StatefulWidget {
  final int currentIndex;
  final void Function(int) onTap;
  final int length;

  const BottomNavBar({
    required this.currentIndex,
    required this.onTap,
    this.length = 5,
    Key? key,
  }) : super(key: key);

  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  late MediaQueryData mediaQuery;
  late double firsLeftPadding = (elementGap + (sizeWidth - circleSize) / 2);
  late double paddingSpread =
      (mediaQuery.size.width - 2 * elementGap - sizeWidth);

  late AnimationController animationController = AnimationController(
    vsync: this,
    duration: Duration(milliseconds: 300),
    lowerBound: 0.0,
    upperBound: widget.length.toDouble() - 1,
    value: widget.currentIndex.toDouble(),
  );

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
    mediaQuery = MediaQueryData.fromWindow(WidgetsBinding.instance!.window);
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  didChangeMetrics() {
    super.didChangeMetrics();
    _didChangeMetrics();
  }

  @override
  void didUpdateWidget(covariant BottomNavBar oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.length != widget.length) {
      animationController.dispose();
      animationController = AnimationController(
        vsync: this,
        duration: Duration(milliseconds: 300),
        lowerBound: 0.0,
        upperBound: widget.length.toDouble() - 1,
        value: widget.currentIndex.toDouble(),
      );
    }
    if (oldWidget.currentIndex != widget.currentIndex) {
      animationController.animateTo(
        widget.currentIndex.toDouble(),
        duration: Duration(milliseconds: 300),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        DecoratedBox(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Color(0x1a000000),
                offset: Offset(0, -4),
                blurRadius: 30,
                spreadRadius: 0,
              )
            ],
          ),
          child: Material(
            color: Colors.white,
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTapUp: (details) {
                final result = math.min(
                  widget.length - 1,
                  math.max(
                    0.0,
                    (details.localPosition.dx -
                            (sizeWidth - circleSize) / 2 -
                            elementGap) /
                        (elementGap + sizeWidth),
                  ),
                );

                widget.onTap(result.round());
              },
              child: Padding(
                padding: EdgeInsets.only(
                  bottom: mediaQuery.padding.bottom == 0
                      ? 16
                      : mediaQuery.padding.bottom,
                ),
                child: SizedBox(
                  height: 64,
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: svgAssets.asMap().entries.map((e) {
                      final Widget newChild;
                      if (widget.currentIndex == e.key) {
                        newChild = RepaintBoundary(
                          child: SvgPicture.asset(
                            svgAssetsActive[e.key] ?? svgAssets[e.key],
                            color: svgAssetsActive[e.key] != null
                                ? null
                                : Color(0xff4CCA6F),
                            height: 24,
                            width: 24,
                          ),
                        );
                      } else {
                        newChild = RepaintBoundary(
                          child: SvgPicture.asset(
                            svgAssets[e.key],
                            height: 24,
                            width: 24,
                          ),
                        );
                      }
                      return SizedBox(
                        width: sizeWidth,
                        height: sizeHeight,
                        child: IconButton(
                          onPressed: () => widget.onTap(e.key),
                          padding: EdgeInsets.zero,
                          icon: newChild,
                        ),
                      );
                    }).toList(),
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          top: 64 - circleSize,
          left: 0,
          right: 0,
          child: Align(
            alignment: Alignment(-1, 0),
            child: AnimatedBuilder(
                animation: animationController,
                builder: (ctx, child) {
                  final _left = firsLeftPadding +
                      paddingSpread *
                          (animationController.value /
                              animationController.upperBound);

                  return Container(
                    margin: EdgeInsets.only(left: _left),
                    height: circleSize,
                    width: circleSize,
                    decoration: BoxDecoration(
                      color: Color(0xff4CCA6F),
                      shape: BoxShape.circle,
                    ),
                  );
                }),
          ),
        ),
      ],
    );
  }
}

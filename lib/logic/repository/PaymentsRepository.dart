import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:li/logic/payment_api.dart';

class PaymentRepository {
  final paymentApi = PaymentsApi();

  Future<dynamic> authPayment({
    required int amount,
    required String currency,
    required String ipAddress,
    required String cardHolderName,
    required String cryptogram,
    required String accountId,
    String? email,
  }) =>
      paymentApi.auth(
        amount: amount,
        currency: currency,
        ipAddress: ipAddress,
        cardHolderName: cardHolderName,
        cryptogram: cryptogram,
        email: email,
        accountId: accountId,
      );

  Future<dynamic> confirmPayment({String? transactionId, int? amount}) =>
      paymentApi.confirmPayment(transactionId: transactionId!, amount: amount!);

  Future<dynamic> post3ds({String? transactionId, String? paRes}) =>
      paymentApi.post3ds(transactionId: transactionId!, paRes: paRes!);

  Future<dynamic> cancelPayment({String? transactionId, String? paRes}) =>
      paymentApi.cancelPayment(transactionId: transactionId!);
}

class PaymentsApi {
  late Dio _client;

  PaymentsApi() {
    final String publicId = cloud_payments_public_id;
    final String secretKey = cloud_payments_secret_key;
    final authCode = base64Encode(
      utf8.encode('$publicId:$secretKey'),
    );
    // log(authCode, name: 'authCode');
    this._client = Dio(
      BaseOptions(
        baseUrl: 'https://api.cloudpayments.ru/',
        headers: {
          'Authorization': 'Basic $authCode',
        },
      ),
    );
  }

  Future<dynamic> testPaymentsApi() {
    return this._client.get('test');
  }

  Future<dynamic> charge({
    int? amount,
    String? currency,
    String? ipAddress,
    String? cardHolderName,
    String? cryptogram,
  }) =>
      this._client.post(
        'payments/cards/charge',
        data: {
          'Amount': amount,
          'Currency': currency,
          'ipAddress': ipAddress,
          'cardHolderName': cardHolderName,
          'cryptogram': cryptogram,
        },
      );

  Future<dynamic> auth({
    int? amount,
    String? currency,
    String? ipAddress,
    String? cardHolderName,
    String? cryptogram,
    String? email,
    required String accountId,
  }) =>
      this._client.post(
        'payments/cards/auth',
        data: {
          'Amount': amount,
          'Currency': currency,
          'ipAddress': ipAddress,
          'Name': cardHolderName,
          'CardCryptogramPacket': cryptogram,
          'Email': email,
          'AccountId': accountId,
        },
      );

  Future<dynamic> post3ds({String? transactionId, String? paRes}) =>
      this._client.post(
        'payments/cards/post3ds',
        data: {
          'TransactionId': transactionId,
          'PaRes': paRes,
        },
      );

  Future<dynamic> confirmPayment({
    String? transactionId,
    int? amount,
  }) =>
      this._client.post(
        'payments/confirm',
        data: {
          'TransactionId': transactionId,
          'Amount': amount,
        },
      );

  Future<dynamic> getTransactonStatus({
    String? transactionId,
  }) =>
      this._client.post(
        'payments/get',
        data: {
          'TransactionId': transactionId,
        },
      );

  Future<dynamic> cancelPayment({
    String? transactionId,
  }) =>
      this._client.post(
        'payments/void',
        data: {
          'TransactionId': transactionId,
        },
      );

  Future<Response<dynamic>> makePreOrder(
      String amount, String token, String id) {
    return this._client.post(
      'payments/tokens/auth',
      data: {
        "Amount": amount,
        'Token': token,
        "AccountId": id,
        "Currency": "KZT"
      },
    );
  }

  Future<Response<dynamic>> payFuck({
    int? amount,
    String? token,
    String? id,
  }) {
    return this._client.post(
      'payments/tokens/charge',
      data: {
        "Amount": amount,
        'Token': token,
        "AccountId": id,
        "Currency": "KZT"
      },
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:li/utils/color.dart';
import 'package:li/utils/text_styles.dart';
import 'package:time_app_components/utils/testStyles.dart';

class LittleButton extends StatelessWidget {
  final String title;
  final VoidCallback onTap;
  final bool right;

  const LittleButton({
    this.right = false,
    required this.title,
    required this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            offset: Offset(1, 1),
            blurRadius: 5,
            color: Color(0x1A000000),
          ),
        ],
      ),
      child: SizedBox(
        height: 40,
        child: ElevatedButton(
          onPressed: this.onTap,
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Colors.white),
            elevation: MaterialStateProperty.all(0),
            shape: MaterialStateProperty.all(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16),
              ),
            ),
            overlayColor: MaterialStateProperty.all(
              Color(0x1f00ab28),
            ),
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (!this.right)
                RepaintBoundary(
                  child: SvgPicture.asset(
                    'assets/svg/icon/arrowChevronDown.svg',
                    height: 18,
                    width: 18,
                  ),
                ),
              if (!this.right) const SizedBox(width: 4),
              Expanded(
                child: FittedBox(
                  child: Text(
                    title,
                    style: TextStyle(
                      color: ColorData.clientsButtonColorPressed,
                      fontWeight: P3TextStyle.fontWeight,
                      height: P3TextStyle.height,
                      fontSize: P3TextStyle.fontSize,
                    ),
                  ),
                ),
              ),
              if (this.right) const SizedBox(width: 4),
              if (this.right)
                RepaintBoundary(
                  child: SvgPicture.asset(
                    'assets/svg/icon/arrowChevronDown.svg',
                    height: 18,
                    width: 18,
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class AlignPositioned extends SingleChildRenderObjectWidget {
  const AlignPositioned({
    Key? key,
    this.alignment = Alignment.center,
    required this.centerPoint,
    this.widthFactor,
    this.heightFactor,
    Widget? child,
  })  : assert(widthFactor == null || widthFactor >= 0.0),
        assert(heightFactor == null || heightFactor >= 0.0),
        super(key: key, child: child);

  final AlignmentGeometry alignment;
  final Offset centerPoint;

  final double? widthFactor;

  final double? heightFactor;

  @override
  _RenderPositionedBox createRenderObject(BuildContext context) {
    return _RenderPositionedBox(
      alignment: alignment,
      widthFactor: widthFactor,
      heightFactor: heightFactor,
      textDirection: Directionality.maybeOf(context),
      centerPoint: this.centerPoint,
    );
  }

  @override
  void updateRenderObject(
      BuildContext context, _RenderPositionedBox renderObject) {
    renderObject
      ..alignment = alignment
      ..widthFactor = widthFactor
      ..heightFactor = heightFactor
      ..textDirection = Directionality.maybeOf(context)
      ..centerPoint = centerPoint;
  }
}

class _RenderPositionedBox extends RenderAligningShiftedBox {
  Offset _centerPoint;

  _RenderPositionedBox({
    RenderBox? child,
    double? widthFactor,
    double? heightFactor,
    AlignmentGeometry alignment = Alignment.center,
    TextDirection? textDirection,
    required Offset centerPoint,
  })  : assert(widthFactor == null || widthFactor >= 0.0),
        assert(heightFactor == null || heightFactor >= 0.0),
        _widthFactor = widthFactor,
        _heightFactor = heightFactor,
        _centerPoint = centerPoint,
        super(child: child, alignment: alignment, textDirection: textDirection);

  double? get widthFactor => _widthFactor;
  double? _widthFactor;
  Size? childSize;

  set widthFactor(double? value) {
    assert(value == null || value >= 0.0);
    if (_widthFactor == value) return;
    _widthFactor = value;
    markNeedsLayout();
  }

  set centerPoint(Offset value) {
    _centerPoint = value;
    markNeedsLayout();
  }

  set alignment(AlignmentGeometry value) {
    super.alignment = value;
    _resolvedAlignment = alignment.resolve(textDirection);
  }

  double? get heightFactor => _heightFactor;
  double? _heightFactor;
  late Alignment _resolvedAlignment = alignment.resolve(textDirection);

  set heightFactor(double? value) {
    assert(value == null || value >= 0.0);
    if (_heightFactor == value) return;
    _heightFactor = value;
    markNeedsLayout();
  }

  @override
  Size computeDryLayout(BoxConstraints constraints) {
    final bool shrinkWrapWidth =
        _widthFactor != null || constraints.maxWidth == double.infinity;
    final bool shrinkWrapHeight =
        _heightFactor != null || constraints.maxHeight == double.infinity;
    if (child != null) {
      final Size childSize = child!.getDryLayout(constraints.loosen());
      return constraints.constrain(Size(
        shrinkWrapWidth
            ? childSize.width * (_widthFactor ?? 1.0)
            : double.infinity,
        shrinkWrapHeight
            ? childSize.height * (_heightFactor ?? 1.0)
            : double.infinity,
      ));
    }
    return constraints.constrain(Size(
      shrinkWrapWidth ? 0.0 : double.infinity,
      shrinkWrapHeight ? 0.0 : double.infinity,
    ));
  }

  @override
  void performLayout() {
    final BoxConstraints constraints = this.constraints;
    final bool shrinkWrapWidth =
        _widthFactor != null || constraints.maxWidth == double.infinity;
    final bool shrinkWrapHeight =
        _heightFactor != null || constraints.maxHeight == double.infinity;

    if (child != null) {
      child!.layout(constraints.loosen(), parentUsesSize: true);
      size = constraints.constrain(
        Size(
          double.infinity,
          double.infinity,
        ),
      );

      final BoxParentData childParentData = child!.parentData! as BoxParentData;
      final moveX = _resolvedAlignment.x - 1;
      final moveY = _resolvedAlignment.y - 1;

      childParentData.offset = _centerPoint +
          Offset(
            child!.size.width / 2 * moveX,
            child!.size.height / 2 * moveY,
          );
    } else {
      size = constraints.constrain(Size(
        shrinkWrapWidth ? 0.0 : double.infinity,
        shrinkWrapHeight ? 0.0 : double.infinity,
      ));
    }
  }
}

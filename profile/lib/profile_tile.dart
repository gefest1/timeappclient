import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:li/components/molecules/size_tap_animation.dart';

import 'package:li/const.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class ProfileTile extends StatelessWidget {
  final String title;
  final VoidCallback onTap;
  final String svgAsset;
  final bool isSmall;
  final String? subTitle;

  const ProfileTile({
    Key? key,
    required this.onTap,
    required this.title,
    required this.svgAsset,
    this.subTitle,
    this.isSmall = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _style = ButtonStyle(
      padding: MaterialStateProperty.all(const EdgeInsets.all(0)),
      shape: MaterialStateProperty.all(
        const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15)),
        ),
      ),
      backgroundColor: MaterialStateProperty.all(Colors.white),
      elevation: MaterialStateProperty.all(0.0),
      overlayColor: MaterialStateProperty.all(Colors.transparent),
    );

    return SizeTapAnimation(
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 20),
        decoration: const BoxDecoration(boxShadow: bigBlur),
        child: ElevatedButton(
          style: _style,
          onPressed: onTap,
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 15,
              vertical: 20,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ConstrainedBox(
                  constraints: const BoxConstraints(
                    minHeight: 50.0,
                    minWidth: 0.0,
                    maxWidth: 250.0,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        title,
                        style: const TextStyle(
                          fontSize: H2TextStyle.fontSize,
                          fontWeight: H2TextStyle.fontWeight,
                          height: H2TextStyle.height,
                          color: ColorData.allMainBlack,
                        ),
                      ),
                      if (!isSmall) const SizedBox(height: 5),
                      if (subTitle != null)
                        Text(
                          subTitle!,
                          // (setting[index].versionType == true)
                          //     ? 'Версия ${packageInfo.version}'
                          //     : setting[index].subtitle,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: Colors.grey[500],
                          ),
                        )
                    ],
                  ),
                ),
                RepaintBoundary(
                  child: ConstrainedBox(
                    constraints: const BoxConstraints(
                      minHeight: 0.0,
                      minWidth: 0.0,
                      maxHeight: 40.0,
                      maxWidth: 40.0,
                    ),
                    child: SvgPicture.asset(svgAsset),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

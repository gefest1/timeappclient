import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:li/components/atoms/leading_icon.dart';
import 'package:li/components/icons/back_arrow.dart';
import 'package:li/components/icons/time_icon.dart';

class RegisterAppBar extends StatelessWidget {
  final bool show;

  const RegisterAppBar({
    this.show = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(16),
        ),
        boxShadow: const [
          BoxShadow(
            offset: Offset(0, -1),
            color: Color(0x0d000000),
            blurRadius: 40,
            spreadRadius: 0,
          ),
          BoxShadow(
            offset: Offset(0, 20),
            color: Color(0x0d000000),
            blurRadius: 40,
            spreadRadius: 0,
          ),
        ],
      ),
      child: Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).padding.top,
          ),
          SizedBox(
            width: double.infinity,
            height: 56,
            child: Stack(
              children: [
                Center(
                  child: CustomPaint(
                    painter: TimeIconPainter(),
                    size: Size(80, 32),
                  ),
                ),
                if (show)
                  Align(
                    alignment: Alignment.centerLeft,
                    child: GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 20,
                          vertical: 16,
                        ),
                        child: CustomPaint(
                          size: Size(24, 24),
                          painter: BackArrowCustomPainter(),
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

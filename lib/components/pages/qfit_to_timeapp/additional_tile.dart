import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class AdditionalTile extends StatelessWidget {
  final double? width;
  final String? title, svgPath;
  const AdditionalTile(
      {this.width = 162, required this.title, required this.svgPath, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      height: 80,
      width: this.width ?? 162,
      decoration: BoxDecoration(
        color: Color(0xFFF9F9F9),
        borderRadius: BorderRadius.circular(15),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (svgPath != null)
            RepaintBoundary(
              child: SvgPicture.network(
                svgPath!,
                color: Colors.black,
              ),
            ),
          const SizedBox(height: 4),
          if (title != null)
            Text(
              title!,
              style: TextStyle(
                fontSize: 18,
                color: Color(0xFF434343),
                fontWeight: FontWeight.w400,
                height: 1,
              ),
              maxLines: 1,
            )
        ],
      ),
    );
  }
}

// import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:li/main.dart' as app;
import 'package:time_app_components/components/molecules/CustomTextField.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();
  group('Pay per minute test', () {
    testWidgets("Main", (tester) async {
      app.main();
      final Finder number = (find.byType(CustomTextField));
      final Finder button = (find.byType(ElevatedButton));
      await tester.pumpAndSettle();
      await tester.enterText(number, '+77475332965');
      await tester.tap(button);
      await Future.delayed(Duration(seconds: 5));
      expect(1, 1);
    });
  });
}

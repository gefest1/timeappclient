import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:take_away/data/models/institution_take_away.dart';

import 'package:li/data/models/photo_url.dart';

class Dish extends Equatable {
  final String? id;
  final num? canteenPrice;
  final String? description;
  final String? dishContainerId;
  final String? institutionId;
  final String? name;
  final PhotoUrl? photoURL;
  final num? price;
  final String? priceCurrency;
  final InstitutionTakeAway? institution;
  // dishContainer
  // type

  const Dish({
    this.id,
    this.canteenPrice,
    this.description,
    this.dishContainerId,
    this.institutionId,
    this.name,
    this.photoURL,
    this.price,
    this.priceCurrency,
    this.institution,
  });

  Dish copyWith({
    String? id,
    num? canteenPrice,
    String? description,
    String? dishContainerId,
    String? institutionId,
    String? name,
    PhotoUrl? photoURL,
    num? price,
    String? priceCurrency,
    InstitutionTakeAway? institution,
  }) {
    return Dish(
      id: id ?? this.id,
      canteenPrice: canteenPrice ?? this.canteenPrice,
      description: description ?? this.description,
      dishContainerId: dishContainerId ?? this.dishContainerId,
      institutionId: institutionId ?? this.institutionId,
      name: name ?? this.name,
      photoURL: photoURL ?? this.photoURL,
      price: price ?? this.price,
      priceCurrency: priceCurrency ?? this.priceCurrency,
      institution: institution ?? this.institution,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'canteenPrice': canteenPrice,
      'description': description,
      'dishContainerId': dishContainerId,
      'institutionId': institutionId,
      'name': name,
      'photoURL': photoURL?.toMap(),
      'price': price,
      'priceCurrency': priceCurrency,
      'institution': institution?.toMap(),
    };
  }

  factory Dish.fromMap(Map<String, dynamic> map) {
    return Dish(
      id: map['id'],
      canteenPrice: map['canteenPrice'],
      description: map['description'],
      dishContainerId: map['dishContainerId'],
      institutionId: map['institutionId'],
      name: map['name'],
      photoURL:
          map['photoURL'] != null ? PhotoUrl.fromMap(map['photoURL']) : null,
      price: map['price'],
      priceCurrency: map['priceCurrency'],
      institution: map['institution'] != null
          ? InstitutionTakeAway.fromMap(map['institution'])
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Dish.fromJson(String source) => Dish.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Dish(id: $id, canteenPrice: $canteenPrice, description: $description, dishContainerId: $dishContainerId, institutionId: $institutionId, name: $name, photoURL: $photoURL, price: $price, priceCurrency: $priceCurrency, institution: $institution)';
  }

  @override
  List<Object?> get props {
    return [
      id,
      canteenPrice,
      description,
      dishContainerId,
      institutionId,
      name,
      photoURL,
      price,
      priceCurrency,
      institution,
    ];
  }
}

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

mixin ActionSheet {
  static Future<XFile?> showActionSheet(BuildContext context) {
    if (Platform.isIOS) {
      return showCupertinoModalPopup<XFile?>(
        context: context,
        builder: (context) => CupertinoActionSheet(
          actions: [
            CupertinoActionSheetAction(
              onPressed: () async => Navigator.pop(
                context,
                await ImagePicker().pickImage(source: ImageSource.camera),
              ),
              child: const Text('Камера'),
            ),
            CupertinoActionSheetAction(
              onPressed: () async => Navigator.pop(
                context,
                await ImagePicker().pickImage(source: ImageSource.gallery),
              ),
              child: const Text('Галерея'),
            ),
          ],
        ),
      );
    }

    return showModalBottomSheet<XFile?>(
      context: context,
      builder: (context) => Wrap(
        children: [
          ListTile(
            leading: const Icon(Icons.camera),
            title: const Text('Камера'),
            onTap: () async => Navigator.pop(
              context,
              await ImagePicker().pickImage(source: ImageSource.camera),
            ),
          ),
          ListTile(
            leading: const Icon(Icons.photo_album),
            title: const Text('Галерея'),
            onTap: () async => Navigator.pop(
              context,
              await ImagePicker().pickImage(source: ImageSource.gallery),
            ),
          )
        ],
      ),
    );

    // return selectImageSource;
  }
}

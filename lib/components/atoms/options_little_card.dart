import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/utils/color.dart';

class OptionsLittleCard extends StatelessWidget {
  final String svg;
  final String title;
  const OptionsLittleCard({
    required this.title,
    required this.svg,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 4.0,
        vertical: 10,
      ),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: ColorData.allMessagesbackground,
        ),
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              RepaintBoundary(child: SvgPicture.asset(svg)),
              SizedBox(
                height: 5,
              ),
              Text(
                title,
                style: TextStyle(
                  fontSize: 18,
                  color: ColorData.allMainBlack,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

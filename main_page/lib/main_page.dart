import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/data/models/credit_card_model.dart';
import 'package:main_page/component/atoms/sliver_up_slow.dart';
import 'package:main_page/component/molecules/main_page/active_head.dart';
import 'package:main_page/component/molecules/main_page/little_button.dart';
import 'package:main_page/component/molecules/main_page/text_up.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:take_away/components/main_page.dart';
import 'package:rent_home/main.dart';
import 'package:li/components/pages/minute_pay_page.dart';
import 'package:li/components/pages/main_service_page.dart';

export './fast_main_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

late QueryResult responseResult;

class _MainPageState extends State<MainPage> {
  final ValueNotifier<CreditCardModel?> creditCard =
      ValueNotifier<CreditCardModel?>(null);
  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
      ),
    );
  }

  int _active = 0;
  final List<Widget> _slivers = [
    const MinutePayPage(),
    const RentHomePage(),
    const TakeAwayMainPage(),
    const MainServicePage(),
  ];
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.dark,
      child: Material(
        color: ColorData.allMainLightgray,
        child: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
            ),
            SliverPadding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              sliver: SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Row(
                    children: const [
                      Expanded(
                        child: TextUp(),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: LittleButton(),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            if (DateTime(2022, 3, 18).isBefore(DateTime.now()))
              SliverToBoxAdapter(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: ActiveHead(
                      index: _active,
                      onTap: (index) {
                        setState(() {
                          _active = index;
                        });
                      }),
                ),
              ),
            const SliverToBoxAdapter(
              child: SizedBox(height: 20),
            ),
            SliverContainer(
              builder: (ctx, constrain) {
                return _slivers[_active];
              },
            ),
          ],
        ),
      ),
    );
  }
}

part of 'GlobalSessionBloc.dart';

abstract class SessionEvent extends Equatable {
  const SessionEvent();
  @override
  List<Object?> get props => [];
}

class GetSessionEvent extends SessionEvent {}

class RemoveMinuteSessionEvent extends SessionEvent {
  final String institutionId;

  const RemoveMinuteSessionEvent(this.institutionId);
  @override
  List<Object?> get props => [institutionId];
}

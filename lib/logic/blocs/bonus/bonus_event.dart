part of 'bonus_bloc.dart';

abstract class BonusEvent extends Equatable {
  const BonusEvent();

  @override
  List<Object?> get props => [];
}

class PayByBonus extends BonusEvent {
  final bool paybybonus;

  const PayByBonus({
    required this.paybybonus,
  });
}

class EnterPromoCode extends BonusEvent {
  final String promoCode;
  final Completer<num> completer;

  const EnterPromoCode({
    required this.completer,
    required this.promoCode,
  });
}

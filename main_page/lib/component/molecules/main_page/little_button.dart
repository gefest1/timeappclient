import 'package:flutter/material.dart';
import 'package:li/components/icons/profile.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/components/pages/auth/your_phone_number_page.dart';
import 'package:li/data/models/credit_card_model.dart';
import 'package:li/main.dart';
import 'package:profile/profile_list_page.dart';

class LittleButton extends StatelessWidget {
  const LittleButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: GestureDetector(
        onTap: () {
          routerDelegate.push(
            isRegistered() ? const ProfileListPage() : const YourPhoneNumber(),
          );
        },
        child: Container(
          height: 47,
          width: 47,
          alignment: Alignment.center,
          child: const CustomPaint(
            painter: ProfileCustomPainter(),
            size: Size(21, 21),
          ),
          decoration: const BoxDecoration(
            color: Colors.white,
            shape: BoxShape.circle,
            boxShadow: [
              BoxShadow(
                color: Color(0x1a000000),
                blurRadius: 5,
                offset: Offset(0, 2),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

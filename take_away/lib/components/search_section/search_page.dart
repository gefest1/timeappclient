import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/main.dart';
import 'package:take_away/components/molecules/search_card.dart';
import 'package:take_away/data/models/dish.dart';
import 'package:take_away/institution/institution_page.dart';
import 'package:take_away/logic/blocs/take_away/take_away_bloc.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/utils/color.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  late final TakeAwayBloc _takeAway = BlocProvider.of<TakeAwayBloc>(context);
  List<Dish> _dishes = [];

  void _initS() async {
    final _newVal = await _takeAway.takeData();
    setState(() {
      _dishes = _newVal;
    });
  }

  @override
  void initState() {
    _initS();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(
        leading: IconButton(
          icon: SvgPicture.asset('assets/svg/icon/backArrow.svg'),
          onPressed: () => Navigator.pop(context),
        ),
        title: const Text(
          'Паста',
          style: TextStyle(
            color: ColorData.allMainBlack,
          ),
        ),
      ),
      body: CustomScrollView(
        slivers: [
          const SliverToBoxAdapter(
            child: SizedBox(height: 20),
          ),
          SliverPadding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            sliver: SliverList(
              delegate: SliverChildBuilderDelegate(
                (context, ind) {
                  if (ind % 2 == 1) {
                    return const SizedBox(height: 20);
                  }
                  final index = ind ~/ 2;
                  return SearchCard(
                    onTap: () {
                      routerDelegate.push(InstitutionPage(
                        institutionId: _dishes[index].institutionId!,
                      ));
                    },
                    nameTitle: _dishes[index].institution?.specialist?.fullName,
                    nameLabel: _dishes[index].institution?.location?.address,
                    title: _dishes[index].name,
                    costTitle: _dishes[index].price.toString(),
                    // typelabel: 'typelabel',
                    description: _dishes[index].description,
                    descriptionTitle: _dishes[index].description == null
                        ? null
                        : 'Описание блюда, состав',
                    namePhoto:
                        _dishes[index].institution?.specialist?.photoURL?.xl,
                  );
                },
                childCount: _dishes.length * 2 - 1,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:li/const.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class FavoritesCard extends StatefulWidget {
  FavoritesCard({Key? key}) : super(key: key);

  @override
  _FavoritesCardState createState() => _FavoritesCardState();
}

class _FavoritesCardState extends State<FavoritesCard> {
  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        boxShadow: bigBlur,
      ),
      child: Material(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
        child: InkWell(
          borderRadius: BorderRadius.circular(16),
          onTap: () {},
          child: Padding(
            padding: const EdgeInsets.only(top: 20, bottom: 24),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(width: 16),
                CircleAvatar(
                  minRadius: 35,
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: 'Иван Иванов',
                              style: TextStyle(
                                fontSize: H2TextStyle.fontSize,
                                fontWeight: H2TextStyle.fontWeight,
                                color: Color(0xFF434343),
                              ),
                            ),
                            WidgetSpan(
                              alignment: PlaceholderAlignment.top,
                              child: Container(
                                width: 11,
                                height: 11,
                                margin: EdgeInsets.only(
                                  left: 2,
                                ),
                                decoration: BoxDecoration(
                                  color: Color(0xFFBEFFC5),
                                  shape: BoxShape.circle,
                                ),
                                alignment: Alignment.center,
                                child: Container(
                                  height: 5,
                                  width: 5,
                                  decoration: BoxDecoration(
                                    color: Color(0xFF0FF426),
                                    shape: BoxShape.circle,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Text(
                        'Тату-студия Zabey',
                        style: TextStyle(
                          fontSize: P3TextStyle.fontSize,
                          fontWeight: P3TextStyle.fontWeight,
                          height: P3TextStyle.height,
                          color: ColorData.allMainActivegray,
                          letterSpacing: 0,
                        ),
                      ),
                      Text(
                        'ул. Сейфуллина 120',
                        style: TextStyle(
                          fontSize: P3TextStyle.fontSize,
                          fontWeight: P3TextStyle.fontWeight,
                          height: P3TextStyle.height,
                          color: ColorData.allMainActivegray,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(width: 10),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

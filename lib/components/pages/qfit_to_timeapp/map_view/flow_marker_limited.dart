import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

import 'package:li/components/atoms/map_pointer.dart';
import 'package:li/components/pages/qfit_to_timeapp/qfit_detail_page.dart';
import 'package:li/data/models/institution_minute.dart';
import 'package:li/logic/providers/location/location_provider.dart';
import 'package:li/main.dart';
import 'package:li/utils/flow_menu_delegate.dart';
import 'package:li/utils/locationsBetween.dart';
import 'package:li/utils/offset_flow_mixin.dart';

extension ListenFlow on _FlowMarkerLimitedState {
  void listenChanges([Null _]) async {
    if (notFinished) return;

    notFinished = true;
    try {
      final _insts = widget.institutions;
      final List<Offset?> _newOffsets = await Future.wait(
        _insts.map(
          (e) => getOffset(
            latitude: e.location!.latitude!.toDouble(),
            longitude: e.location!.longitude!.toDouble(),
          ),
        ),
      );
      offsetListen.value = _newOffsets.map((e) => e!).toList();
    } catch (e) {}
    notFinished = false;
  }
}

class FlowMarkerLimited extends StatefulWidget {
  final YandexMapController yandexMapController;
  final StreamController<Null> streamController;
  final List<InstitutionsMinute> institutions;

  const FlowMarkerLimited({
    required this.yandexMapController,
    required this.streamController,
    required this.institutions,
    Key? key,
  }) : super(key: key);

  @override
  _FlowMarkerLimitedState createState() => _FlowMarkerLimitedState();
}

class _FlowMarkerLimitedState extends State<FlowMarkerLimited>
    with OffsetExtension {
  final ValueNotifier<List<Offset>> offsetListen =
      ValueNotifier<List<Offset>>([]);
  late StreamSubscription<Null> streamSubscription;
  bool notFinished = false;

  @override
  void initState() {
    listenChanges();
    yandexMapController = widget.yandexMapController;
    streamSubscription = widget.streamController.stream.listen(listenChanges);

    super.initState();
  }

  @override
  void setState(VoidCallback fn) {
    super.setState(fn);
  }

  @override
  void dispose() {
    streamSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<LocationProvider>(
      builder: (_, LocationProvider locationProvider, Widget? child) {
        final myPosition = locationProvider.location;

        final _institutionsList =
            widget.institutions.sublist(0, offsetListen.value.length);
        return Flow(
          delegate: FlowMenuDelegate(
            offsetListen: offsetListen,
          ),
          children: _institutionsList
              .map(
                (club) => MapPointer(
                  key: ValueKey(club.id),
                  title: '${club.averagePrice} ₸/мин',
                  label: club.name,
                  tile: myPosition == null
                      ? null
                      : locationBetweenText(
                          club.location!.latitude!.toDouble(),
                          club.location!.longitude!.toDouble(),
                          myPosition.latitude,
                          myPosition.longitude,
                        ),
                  onTap: () {
                    routerDelegate.push(
                      QfitDetailPage(
                        institution: club,
                      ),
                    );
                  },
                ),
              )
              .toList(),
        );
      },
    );
  }
}

import 'package:flutter/material.dart';
import 'package:li/utils/color.dart';

class BottomNavBar extends StatelessWidget {
  final TabController tabController;

  const BottomNavBar({
    required this.tabController,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: DecoratedBox(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              offset: Offset(0, -10),
              blurRadius: 30,
              color: Color(0xff323232).withOpacity(0.1),
            )
          ],
        ),
        child: Material(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(16),
          ),
          child: Padding(
            padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).padding.bottom + 20,
              top: 20,
              left: 20,
              right: 20,
            ),
            child: Row(
              children: [
                Expanded(
                  child: SizedBox(
                    height: 50,
                    child: DecoratedBox(
                      decoration: BoxDecoration(
                        color: Color(0xffF8F8F8),
                        borderRadius: BorderRadius.circular(6),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: TabBar(
                          controller: tabController,
                          labelColor: ColorData.clientsButtonColorPressed,
                          labelStyle: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                          ),
                          unselectedLabelColor: Colors.black,
                          unselectedLabelStyle: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                          ),
                          indicator: BoxDecoration(
                            color: Colors.white,
                            borderRadius: const BorderRadius.all(
                              Radius.circular(6),
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black.withOpacity(0.1),
                                spreadRadius: 0,
                                blurRadius: 5,
                                offset: const Offset(1, 1),
                              ),
                            ],
                          ),
                          tabs: const [
                            Text(
                              'Списком',
                            ),
                            Text(
                              'На карте',
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                // const SizedBox(width: 10),
                // GestureDetector(
                //   onTap: () {},
                //   behavior: HitTestBehavior.opaque,
                //   child: IgnorePointer(
                //     child: SizedBox(
                //       height: 50,
                //       child: Center(
                //         child: Text(
                //           'Фильтры',
                //           style: TextStyle(
                //             fontSize: P3TextStyle.fontSize,
                //             fontWeight: P3TextStyle.fontWeight,
                //             height: P3TextStyle.height,
                //             color: ColorData.clientsButtonColorPressed,
                //           ),
                //         ),
                //       ),
                //     ),
                //   ),
                // )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

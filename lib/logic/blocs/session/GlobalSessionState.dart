part of 'GlobalSessionBloc.dart';

abstract class SessionState extends Equatable {
  @override
  List<Object?> get props => [];
}

class LoadingSessionState extends SessionState {
  @override
  List<Object?> get props => [];
}

class SavedSessionState extends SessionState {
  @override
  List<Object?> get props => [];
}

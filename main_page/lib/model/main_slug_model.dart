import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:li/utils/translate.dart';

class MainSlugModel extends Equatable {
  final String? photoSvg;
  final String? slug;
  final Translate? subTitle;
  final Translate? title;

  const MainSlugModel({
    this.photoSvg,
    this.slug,
    this.subTitle,
    this.title,
  });

  MainSlugModel copyWith({
    String? photoSvg,
    String? slug,
    Translate? subTitle,
    Translate? title,
  }) {
    return MainSlugModel(
      photoSvg: photoSvg ?? this.photoSvg,
      slug: slug ?? this.slug,
      subTitle: subTitle ?? this.subTitle,
      title: title ?? this.title,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'photoSvg': photoSvg,
      'slug': slug,
      'subTitle': subTitle?.toMap(),
      'title': title?.toMap(),
    };
  }

  static MainSlugModel? fromMap(Map<String, dynamic>? map) {
    if (map == null) return null;
    return MainSlugModel(
      photoSvg: map['photoSvg'],
      slug: map['slug'],
      subTitle: Translate.fromMap(map['subTitle']),
      title: Translate.fromMap(map['title']),
    );
  }

  String toJson() => json.encode(toMap());

  factory MainSlugModel.fromJson(String source) =>
      MainSlugModel.fromMap(json.decode(source))!;

  @override
  bool get stringify => true;

  @override
  List<Object?> get props => [photoSvg, slug, subTitle, title];
}

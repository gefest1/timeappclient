import 'package:blobs/blobs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/const.dart';

class LoadWavesFinish extends StatefulWidget {
  final String title;
  final VoidCallback onTap;
  final List<Widget> stackWidget;

  const LoadWavesFinish({
    required this.onTap,
    required this.title,
    this.stackWidget = const [],
    Key? key,
  }) : super(key: key);

  @override
  _LoadWavesFinishState createState() => _LoadWavesFinishState();
}

class _LoadWavesFinishState extends State<LoadWavesFinish> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Center(
          child: SizedBox(
            height: 320,
            width: 320,
            child: Stack(
              children: [
                Center(
                  child: Blob.animatedRandom(
                    size: 320,
                    edgesCount: 5,
                    minGrowth: 9,
                    loop: true,
                    styles:
                        BlobStyles(color: Color(0xffC0FFCD).withOpacity(0.25)),
                  ),
                ),
                Center(
                  child: Blob.animatedRandom(
                    size: 280,
                    edgesCount: 5,
                    minGrowth: 9,
                    loop: true,
                    styles:
                        BlobStyles(color: Color(0xffC0FFCD).withOpacity(0.5)),
                  ),
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 8, bottom: 8),
                    child: Blob.animatedRandom(
                      size: 248,
                      edgesCount: 5,
                      minGrowth: 8,
                      loop: true,
                      styles: BlobStyles(color: Color(0xffC0FFCD)),
                    ),
                  ),
                ),
                Center(
                  child: SizeTapAnimation(
                    child: Center(
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                          boxShadow: bigBlur,
                          color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                        child: SizedBox(
                          width: 160,
                          height: 160,
                          child: GestureDetector(
                            onTap: widget.onTap,
                            behavior: HitTestBehavior.opaque,
                            child: Column(children: [
                              const SizedBox(height: 40),
                              SvgPicture.asset('assets/svg/icon/icon_qr.svg'),
                              const SizedBox(height: 4),
                              Text(
                                widget.title,
                                style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xff00D42B),
                                  fontSize: 16,
                                ),
                              )
                            ]),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        ...widget.stackWidget,
      ],
    );
  }
}

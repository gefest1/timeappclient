import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/molecules/favorites_card.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class FavoritesPage extends StatefulWidget {
  const FavoritesPage({Key? key}) : super(key: key);

  @override
  _FavoritesPageState createState() => _FavoritesPageState();
}

class _FavoritesPageState extends State<FavoritesPage> {
  final bool isON = true;
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Scaffold(
        appBar: CustomAppBar(
          actions: [
            IconButton(
              onPressed: () {},
              icon: SvgPicture.asset('assets/svg/icon/searchMain.svg'),
            ),
            const SizedBox(width: 12),
          ],
          centerTitle: true,
          title: const Text(
            'Избранное',
            style: TextStyle(
              fontSize: 18,
              height: 21 / 18,
              fontWeight: FontWeight.w500,
              color: ColorData.allMainBlack,
            ),
          ),
        ),
        body: CustomScrollView(
          clipBehavior: Clip.none,
          slivers: [
            SliverPadding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              sliver: SliverToBoxAdapter(
                child: Column(
                  children: [
                    const SizedBox(height: 10),
                    Container(
                      width: double.infinity,
                      height: 40,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(16),
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 5,
                            color: Color(0x1a000000),
                            offset: Offset(1, 1),
                          )
                        ],
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Все',
                            style: TextStyle(
                              fontSize: P3TextStyle.fontSize,
                              height: P3TextStyle.height,
                              fontWeight: P3TextStyle.fontWeight,
                              color: ColorData.clientsButtonColorPressed,
                            ),
                          ),
                          SvgPicture.asset(
                            'assets/svg/icon/arrowChevronDown.svg',
                          )
                        ],
                      ),
                    ),
                    const SizedBox(height: 20),
                  ],
                ),
              ),
            ),
            SliverPadding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              sliver: SliverList(
                delegate: SliverChildBuilderDelegate(
                  (context, index) => index % 2 == 1
                      ? SizedBox(
                          height: 10,
                        )
                      : SizeTapAnimation(
                          child: FavoritesCard(),
                        ),
                  childCount: 20,
                ),
              ),
            ),
            const SliverToBoxAdapter(
              child: SizedBox(height: 100),
            ),
          ],
        ),
      ),
    );
  }
}

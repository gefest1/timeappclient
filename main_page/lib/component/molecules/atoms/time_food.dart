// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:time_app_components/utils/color.dart';

class TimeFoodCustomPainter extends CustomPainter {
  final bool active;

  const TimeFoodCustomPainter({this.active = false, Listenable? repaint})
      : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    final _activeColor = active ? const Color(0xff25BA00) : ColorData.allMainGray;

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.2286023, size.height * 0.9918137);
    path_1.lineTo(size.width * 0.2097374, size.height * 0.9918137);
    path_1.lineTo(size.width * 0.2463481, size.height * 0.8634824);
    path_1.lineTo(size.width * 0.2797618, size.height * 0.8634824);
    path_1.lineTo(size.width * 0.2286023, size.height * 0.9918137);
    path_1.close();
    path_1.moveTo(size.width * 0.2154924, size.height * 0.9866765);
    path_1.lineTo(size.width * 0.2262046, size.height * 0.9866765);
    path_1.lineTo(size.width * 0.2732870, size.height * 0.8686157);
    path_1.lineTo(size.width * 0.2491458, size.height * 0.8686157);
    path_1.lineTo(size.width * 0.2154924, size.height * 0.9866765);
    path_1.close();

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = Colors.black;
    canvas.drawPath(path_1, paint_1_fill);

    Path path_2 = Path();
    path_2.moveTo(size.width * 0.7390122, size.height * 0.9918137);
    path_2.lineTo(size.width * 0.7201473, size.height * 0.9918137);
    path_2.lineTo(size.width * 0.7195878, size.height * 0.9903725);
    path_2.lineTo(size.width * 0.6689084, size.height * 0.8634824);
    path_2.lineTo(size.width * 0.7023214, size.height * 0.8634824);
    path_2.lineTo(size.width * 0.7390122, size.height * 0.9918137);
    path_2.close();
    path_2.moveTo(size.width * 0.7225458, size.height * 0.9866765);
    path_2.lineTo(size.width * 0.7332573, size.height * 0.9866765);
    path_2.lineTo(size.width * 0.6995237, size.height * 0.8686157);
    path_2.lineTo(size.width * 0.6753832, size.height * 0.8686157);
    path_2.lineTo(size.width * 0.7225458, size.height * 0.9866765);
    path_2.close();

    Paint paint_2_fill = Paint()..style = PaintingStyle.fill;
    paint_2_fill.color = Colors.black;
    canvas.drawPath(path_2, paint_2_fill);

    Path path_3 = Path();
    path_3.moveTo(size.width * 0.1601863, size.height * 0.7402127);
    path_3.lineTo(size.width * 0.1601863, size.height * 0.4767775);
    path_3.cubicTo(
        size.width * 0.1601863,
        size.height * 0.4504961,
        size.width * 0.1768130,
        size.height * 0.4291422,
        size.width * 0.1972763,
        size.height * 0.4291422);
    path_3.lineTo(size.width * 0.7593908, size.height * 0.4291422);
    path_3.cubicTo(
        size.width * 0.7798550,
        size.height * 0.4291422,
        size.width * 0.7964809,
        size.height * 0.4504961,
        size.width * 0.7964809,
        size.height * 0.4767775);
    path_3.lineTo(size.width * 0.7964809, size.height * 0.7402127);
    path_3.lineTo(size.width * 0.1601863, size.height * 0.7402127);
    path_3.close();

    Paint paint_3_fill = Paint()..style = PaintingStyle.fill;
    paint_3_fill.color = Colors.white;
    canvas.drawPath(path_3, paint_3_fill);

    Path path_4 = Path();
    path_4.moveTo(size.width * 0.7984733, size.height * 0.7427039);
    path_4.lineTo(size.width * 0.1581809, size.height * 0.7427039);
    path_4.lineTo(size.width * 0.1581809, size.height * 0.4767020);
    path_4.cubicTo(
        size.width * 0.1581809,
        size.height * 0.4489824,
        size.width * 0.1756870,
        size.height * 0.4264990,
        size.width * 0.1972695,
        size.height * 0.4264990);
    path_4.lineTo(size.width * 0.7593840, size.height * 0.4264990);
    path_4.cubicTo(
        size.width * 0.7809695,
        size.height * 0.4264990,
        size.width * 0.7984733,
        size.height * 0.4489824,
        size.width * 0.7984733,
        size.height * 0.4767020);
    path_4.lineTo(size.width * 0.7984733, size.height * 0.7427039);
    path_4.close();
    path_4.moveTo(size.width * 0.1621779, size.height * 0.7375706);
    path_4.lineTo(size.width * 0.7944733, size.height * 0.7375706);
    path_4.lineTo(size.width * 0.7944733, size.height * 0.4767020);
    path_4.cubicTo(
        size.width * 0.7944733,
        size.height * 0.4518569,
        size.width * 0.7787252,
        size.height * 0.4316324,
        size.width * 0.7593840,
        size.height * 0.4316324);
    path_4.lineTo(size.width * 0.1972695, size.height * 0.4316324);
    path_4.cubicTo(
        size.width * 0.1779252,
        size.height * 0.4316324,
        size.width * 0.1621779,
        size.height * 0.4518569,
        size.width * 0.1621779,
        size.height * 0.4767020);
    path_4.lineTo(size.width * 0.1621779, size.height * 0.7375706);
    path_4.close();

    Paint paint_4_fill = Paint()..style = PaintingStyle.fill;
    paint_4_fill.color = Colors.black;
    canvas.drawPath(path_4, paint_4_fill);

    Path path_5 = Path();
    path_5.moveTo(size.width * 0.1988015, size.height * 0.4278667);
    path_5.lineTo(size.width * 0.1959985, size.height * 0.4315255);
    path_5.lineTo(size.width * 0.4422382, size.height * 0.7426294);
    path_5.lineTo(size.width * 0.4450412, size.height * 0.7389696);
    path_5.lineTo(size.width * 0.1988015, size.height * 0.4278667);
    path_5.close();

    Paint paint_5_fill = Paint()..style = PaintingStyle.fill;
    paint_5_fill.color = Colors.black;
    canvas.drawPath(path_5, paint_5_fill);

    Path path_6 = Path();
    path_6.moveTo(size.width * 0.1620275, size.height * 0.5636078);
    path_6.lineTo(size.width * 0.1592092, size.height * 0.5672490);
    path_6.lineTo(size.width * 0.2966855, size.height * 0.7427657);
    path_6.lineTo(size.width * 0.2995031, size.height * 0.7391245);
    path_6.lineTo(size.width * 0.1620275, size.height * 0.5636078);
    path_6.close();

    Paint paint_6_fill = Paint()..style = PaintingStyle.fill;
    paint_6_fill.color = Colors.black;
    canvas.drawPath(path_6, paint_6_fill);

    Path path_7 = Path();
    path_7.moveTo(size.width * 0.3410863, size.height * 0.4267333);
    path_7.lineTo(size.width * 0.3382786, size.height * 0.4303863);
    path_7.lineTo(size.width * 0.5845023, size.height * 0.7425275);
    path_7.lineTo(size.width * 0.5873099, size.height * 0.7388735);
    path_7.lineTo(size.width * 0.3410863, size.height * 0.4267333);
    path_7.close();

    Paint paint_7_fill = Paint()..style = PaintingStyle.fill;
    paint_7_fill.color = Colors.black;
    canvas.drawPath(path_7, paint_7_fill);

    Path path_8 = Path();
    path_8.moveTo(size.width * 0.4889122, size.height * 0.4279588);
    path_8.lineTo(size.width * 0.4861046, size.height * 0.4316118);
    path_8.lineTo(size.width * 0.7314748, size.height * 0.7426706);
    path_8.lineTo(size.width * 0.7342824, size.height * 0.7390176);
    path_8.lineTo(size.width * 0.4889122, size.height * 0.4279588);
    path_8.close();

    Paint paint_8_fill = Paint()..style = PaintingStyle.fill;
    paint_8_fill.color = Colors.black;
    canvas.drawPath(path_8, paint_8_fill);

    Path path_9 = Path();
    path_9.moveTo(size.width * 0.6327702, size.height * 0.4270941);
    path_9.lineTo(size.width * 0.6299649, size.height * 0.4307500);
    path_9.lineTo(size.width * 0.7954046, size.height * 0.6401559);
    path_9.lineTo(size.width * 0.7982137, size.height * 0.6365000);
    path_9.lineTo(size.width * 0.6327702, size.height * 0.4270941);
    path_9.close();

    Paint paint_9_fill = Paint()..style = PaintingStyle.fill;
    paint_9_fill.color = Colors.black;
    canvas.drawPath(path_9, paint_9_fill);

    Path path_10 = Path();
    path_10.moveTo(size.width * 0.7499015, size.height * 0.4280029);
    path_10.lineTo(size.width * 0.5085061, size.height * 0.7390853);
    path_10.lineTo(size.width * 0.5113366, size.height * 0.7427088);
    path_10.lineTo(size.width * 0.7527328, size.height * 0.4316265);
    path_10.lineTo(size.width * 0.7499015, size.height * 0.4280029);
    path_10.close();

    Paint paint_10_fill = Paint()..style = PaintingStyle.fill;
    paint_10_fill.color = Colors.black;
    canvas.drawPath(path_10, paint_10_fill);

    Path path_11 = Path();
    path_11.moveTo(size.width * 0.6075916, size.height * 0.4267265);
    path_11.lineTo(size.width * 0.3654061, size.height * 0.7388275);
    path_11.lineTo(size.width * 0.3682366, size.height * 0.7424510);
    path_11.lineTo(size.width * 0.6104229, size.height * 0.4303500);
    path_11.lineTo(size.width * 0.6075916, size.height * 0.4267265);
    path_11.close();

    Paint paint_11_fill = Paint()..style = PaintingStyle.fill;
    paint_11_fill.color = Colors.black;
    canvas.drawPath(path_11, paint_11_fill);

    Path path_12 = Path();
    path_12.moveTo(size.width * 0.4597656, size.height * 0.4279265);
    path_12.lineTo(size.width * 0.2183695, size.height * 0.7390088);
    path_12.lineTo(size.width * 0.2212008, size.height * 0.7426324);
    path_12.lineTo(size.width * 0.4625969, size.height * 0.4315500);
    path_12.lineTo(size.width * 0.4597656, size.height * 0.4279265);
    path_12.close();

    Paint paint_12_fill = Paint()..style = PaintingStyle.fill;
    paint_12_fill.color = Colors.black;
    canvas.drawPath(path_12, paint_12_fill);

    Path path_13 = Path();
    path_13.moveTo(size.width * 0.3159252, size.height * 0.4275559);
    path_13.lineTo(size.width * 0.1592695, size.height * 0.6298353);
    path_13.lineTo(size.width * 0.1621031, size.height * 0.6334549);
    path_13.lineTo(size.width * 0.3187588, size.height * 0.4311755);
    path_13.lineTo(size.width * 0.3159252, size.height * 0.4275559);
    path_13.close();

    Paint paint_13_fill = Paint()..style = PaintingStyle.fill;
    paint_13_fill.color = Colors.black;
    canvas.drawPath(path_13, paint_13_fill);

    Path path_14 = Path();
    path_14.moveTo(size.width * 0.7954427, size.height * 0.5540902);
    path_14.lineTo(size.width * 0.6515313, size.height * 0.7389127);
    path_14.lineTo(size.width * 0.6543573, size.height * 0.7425431);
    path_14.lineTo(size.width * 0.7982672, size.height * 0.5577196);
    path_14.lineTo(size.width * 0.7954427, size.height * 0.5540902);
    path_14.close();

    Paint paint_14_fill = Paint()..style = PaintingStyle.fill;
    paint_14_fill.color = Colors.black;
    canvas.drawPath(path_14, paint_14_fill);

    Path path_15 = Path();
    path_15.moveTo(size.width * 0.8265267, size.height * 0.5534951);
    path_15.cubicTo(
        size.width * 0.7921527,
        size.height * 0.5462059,
        size.width * 0.7597008,
        size.height * 0.5763892,
        size.width * 0.7543450,
        size.height * 0.6206373);
    path_15.lineTo(size.width * 0.7505878, size.height * 0.6519490);
    path_15.cubicTo(
        size.width * 0.7454718,
        size.height * 0.6940412,
        size.width * 0.7172542,
        size.height * 0.7250461,
        size.width * 0.6840809,
        size.height * 0.7250461);
    path_15.lineTo(size.width * 0.5374771, size.height * 0.7250461);
    path_15.lineTo(size.width * 0.4147740, size.height * 0.7250461);
    path_15.lineTo(size.width * 0.2681702, size.height * 0.7250461);
    path_15.cubicTo(
        size.width * 0.2349969,
        size.height * 0.7250461,
        size.width * 0.2067794,
        size.height * 0.6940412,
        size.width * 0.2016634,
        size.height * 0.6519490);
    path_15.lineTo(size.width * 0.1979061, size.height * 0.6206373);
    path_15.cubicTo(
        size.width * 0.1925504,
        size.height * 0.5763892,
        size.width * 0.1600962,
        size.height * 0.5462059,
        size.width * 0.1257237,
        size.height * 0.5534951);
    path_15.cubicTo(
        size.width * 0.08903282,
        size.height * 0.5612971,
        size.width * 0.06561099,
        size.height * 0.6081118,
        size.width * 0.07536328,
        size.height * 0.6542078);
    path_15.lineTo(size.width * 0.09071145, size.height * 0.7264833);
    path_15.cubicTo(
        size.width * 0.1095763,
        size.height * 0.8153902,
        size.width * 0.1724061,
        size.height * 0.8769882,
        size.width * 0.2441092,
        size.height * 0.8769882);
    path_15.lineTo(size.width * 0.4147740, size.height * 0.8769882);
    path_15.lineTo(size.width * 0.5374771, size.height * 0.8769882);
    path_15.lineTo(size.width * 0.7081420, size.height * 0.8769882);
    path_15.cubicTo(
        size.width * 0.7798473,
        size.height * 0.8769882,
        size.width * 0.8426718,
        size.height * 0.8152873,
        size.width * 0.8615420,
        size.height * 0.7264833);
    path_15.lineTo(size.width * 0.8768855, size.height * 0.6542078);
    path_15.cubicTo(
        size.width * 0.8867176,
        size.height * 0.6081118,
        size.width * 0.8632977,
        size.height * 0.5612971,
        size.width * 0.8265267,
        size.height * 0.5534951);
    path_15.close();

    Paint paint_15_fill = Paint()..style = PaintingStyle.fill;
    paint_15_fill.color = Colors.black;
    canvas.drawPath(path_15, paint_15_fill);

    Path path_16 = Path();
    path_16.moveTo(size.width * 0.7081847, size.height * 0.8798314);
    path_16.lineTo(size.width * 0.2441527, size.height * 0.8798314);
    path_16.cubicTo(
        size.width * 0.2085008,
        size.height * 0.8798314,
        size.width * 0.1746878,
        size.height * 0.8651510,
        size.width * 0.1463901,
        size.height * 0.8373294);
    path_16.cubicTo(
        size.width * 0.1180924,
        size.height * 0.8095069,
        size.width * 0.09818855,
        size.height * 0.7715216,
        size.width * 0.08875573,
        size.height * 0.7273765);
    path_16.lineTo(size.width * 0.07340809, size.height * 0.6551010);
    path_16.cubicTo(
        size.width * 0.06861191,
        size.height * 0.6324127,
        size.width * 0.07148962,
        size.height * 0.6080814,
        size.width * 0.08140153,
        size.height * 0.5882667);
    path_16.cubicTo(
        size.width * 0.09131374,
        size.height * 0.5684529,
        size.width * 0.1073015,
        size.height * 0.5550039,
        size.width * 0.1253672,
        size.height * 0.5511029);
    path_16.cubicTo(
        size.width * 0.1425534,
        size.height * 0.5475098,
        size.width * 0.1597397,
        size.height * 0.5526422,
        size.width * 0.1738885,
        size.height * 0.5657833);
    path_16.cubicTo(
        size.width * 0.1879573,
        size.height * 0.5789245,
        size.width * 0.1972298,
        size.height * 0.5982255,
        size.width * 0.1998679,
        size.height * 0.6204010);
    path_16.lineTo(size.width * 0.2036252, size.height * 0.6517127);
    path_16.cubicTo(
        size.width * 0.2085809,
        size.height * 0.6927784,
        size.width * 0.2357595,
        size.height * 0.7226539,
        size.width * 0.2681336,
        size.height * 0.7226539);
    path_16.lineTo(size.width * 0.6841237, size.height * 0.7226539);
    path_16.cubicTo(
        size.width * 0.7164985,
        size.height * 0.7226539,
        size.width * 0.7436771,
        size.height * 0.6927784,
        size.width * 0.7486328,
        size.height * 0.6517127);
    path_16.lineTo(size.width * 0.7523901, size.height * 0.6204010);
    path_16.cubicTo(
        size.width * 0.7550275,
        size.height * 0.5983275,
        size.width * 0.7642977,
        size.height * 0.5789245,
        size.width * 0.7783664,
        size.height * 0.5657833);
    path_16.cubicTo(
        size.width * 0.7924351,
        size.height * 0.5526422,
        size.width * 0.8097023,
        size.height * 0.5475098,
        size.width * 0.8268931,
        size.height * 0.5511029);
    path_16.cubicTo(
        size.width * 0.8449542,
        size.height * 0.5549010,
        size.width * 0.8609466,
        size.height * 0.5684529,
        size.width * 0.8708550,
        size.height * 0.5882667);
    path_16.cubicTo(
        size.width * 0.8807710,
        size.height * 0.6080814,
        size.width * 0.8836489,
        size.height * 0.6324127,
        size.width * 0.8788473,
        size.height * 0.6551010);
    path_16.lineTo(size.width * 0.8635038, size.height * 0.7273765);
    path_16.cubicTo(
        size.width * 0.8541527,
        size.height * 0.7715216,
        size.width * 0.8342443,
        size.height * 0.8095069,
        size.width * 0.8058702,
        size.height * 0.8373294);
    path_16.cubicTo(
        size.width * 0.7776489,
        size.height * 0.8650480,
        size.width * 0.7438366,
        size.height * 0.8798314,
        size.width * 0.7081847,
        size.height * 0.8798314);
    path_16.close();
    path_16.moveTo(size.width * 0.1360786, size.height * 0.5552088);
    path_16.cubicTo(
        size.width * 0.1327214,
        size.height * 0.5552088,
        size.width * 0.1294435,
        size.height * 0.5555167,
        size.width * 0.1260863,
        size.height * 0.5562363);
    path_16.cubicTo(
        size.width * 0.1091397,
        size.height * 0.5598294,
        size.width * 0.09411145,
        size.height * 0.5725598,
        size.width * 0.08483893,
        size.height * 0.5910392);
    path_16.cubicTo(
        size.width * 0.07556641,
        size.height * 0.6095186,
        size.width * 0.07284855,
        size.height * 0.6324127,
        size.width * 0.07732519,
        size.height * 0.6536637);
    path_16.lineTo(size.width * 0.09267252, size.height * 0.7259392);
    path_16.cubicTo(
        size.width * 0.1112183,
        size.height * 0.8135108,
        size.width * 0.1735687,
        size.height * 0.8745961,
        size.width * 0.2441527,
        size.height * 0.8745961);
    path_16.lineTo(size.width * 0.7081847, size.height * 0.8745961);
    path_16.cubicTo(
        size.width * 0.7788473,
        size.height * 0.8745961,
        size.width * 0.8411221,
        size.height * 0.8135108,
        size.width * 0.8596641,
        size.height * 0.7259392);
    path_16.lineTo(size.width * 0.8750153, size.height * 0.6536637);
    path_16.cubicTo(
        size.width * 0.8794885,
        size.height * 0.6324127,
        size.width * 0.8767710,
        size.height * 0.6095186,
        size.width * 0.8674962,
        size.height * 0.5910392);
    path_16.cubicTo(
        size.width * 0.8582290,
        size.height * 0.5724569,
        size.width * 0.8431985,
        size.height * 0.5598294,
        size.width * 0.8262519,
        size.height * 0.5562363);
    path_16.cubicTo(
        size.width * 0.8101069,
        size.height * 0.5528480,
        size.width * 0.7939542,
        size.height * 0.5577755,
        size.width * 0.7807710,
        size.height * 0.5699931);
    path_16.cubicTo(
        size.width * 0.7675802,
        size.height * 0.5823127,
        size.width * 0.7588649,
        size.height * 0.6004843,
        size.width * 0.7563870,
        size.height * 0.6212216);
    path_16.lineTo(size.width * 0.7526298, size.height * 0.6525343);
    path_16.cubicTo(
        size.width * 0.7500718,
        size.height * 0.6734775,
        size.width * 0.7417580,
        size.height * 0.6926755,
        size.width * 0.7292084,
        size.height * 0.7064333);
    path_16.cubicTo(
        size.width * 0.7166580,
        size.height * 0.7201902,
        size.width * 0.7006710,
        size.height * 0.7277873,
        size.width * 0.6841237,
        size.height * 0.7277873);
    path_16.lineTo(size.width * 0.2682137, size.height * 0.7277873);
    path_16.cubicTo(
        size.width * 0.2516664,
        size.height * 0.7277873,
        size.width * 0.2356794,
        size.height * 0.7201902,
        size.width * 0.2231298,
        size.height * 0.7064333);
    path_16.cubicTo(
        size.width * 0.2105794,
        size.height * 0.6926755,
        size.width * 0.2022656,
        size.height * 0.6734775,
        size.width * 0.1997076,
        size.height * 0.6525343);
    path_16.lineTo(size.width * 0.1959511, size.height * 0.6212216);
    path_16.cubicTo(
        size.width * 0.1934733,
        size.height * 0.6004843,
        size.width * 0.1847595,
        size.height * 0.5823127,
        size.width * 0.1715702,
        size.height * 0.5699931);
    path_16.cubicTo(
        size.width * 0.1610985,
        size.height * 0.5602402,
        size.width * 0.1487084,
        size.height * 0.5552088,
        size.width * 0.1360786,
        size.height * 0.5552088);
    path_16.close();

    Paint paint_16_fill = Paint()..style = PaintingStyle.fill;
    paint_16_fill.color = Colors.black;
    canvas.drawPath(path_16, paint_16_fill);

    Path path_17 = Path();
    path_17.moveTo(size.width * 0.1330107, size.height * 0.6425902);
    path_17.cubicTo(
        size.width * 0.1389710,
        size.height * 0.6425902,
        size.width * 0.1438023,
        size.height * 0.6363843,
        size.width * 0.1438023,
        size.height * 0.6287304);
    path_17.cubicTo(
        size.width * 0.1438023,
        size.height * 0.6210755,
        size.width * 0.1389710,
        size.height * 0.6148706,
        size.width * 0.1330107,
        size.height * 0.6148706);
    path_17.cubicTo(
        size.width * 0.1270511,
        size.height * 0.6148706,
        size.width * 0.1222191,
        size.height * 0.6210755,
        size.width * 0.1222191,
        size.height * 0.6287304);
    path_17.cubicTo(
        size.width * 0.1222191,
        size.height * 0.6363843,
        size.width * 0.1270511,
        size.height * 0.6425902,
        size.width * 0.1330107,
        size.height * 0.6425902);
    path_17.close();

    Paint paint_17_fill = Paint()..style = PaintingStyle.fill;
    paint_17_fill.color = Colors.white;
    canvas.drawPath(path_17, paint_17_fill);

    Path path_18 = Path();
    path_18.moveTo(size.width * 0.8200534, size.height * 0.6426569);
    path_18.cubicTo(
        size.width * 0.8260153,
        size.height * 0.6426569,
        size.width * 0.8308473,
        size.height * 0.6364520,
        size.width * 0.8308473,
        size.height * 0.6287971);
    path_18.cubicTo(
        size.width * 0.8308473,
        size.height * 0.6211431,
        size.width * 0.8260153,
        size.height * 0.6149373,
        size.width * 0.8200534,
        size.height * 0.6149373);
    path_18.cubicTo(
        size.width * 0.8140992,
        size.height * 0.6149373,
        size.width * 0.8092672,
        size.height * 0.6211431,
        size.width * 0.8092672,
        size.height * 0.6287971);
    path_18.cubicTo(
        size.width * 0.8092672,
        size.height * 0.6364520,
        size.width * 0.8140992,
        size.height * 0.6426569,
        size.width * 0.8200534,
        size.height * 0.6426569);
    path_18.close();

    Paint paint_18_fill = Paint()..style = PaintingStyle.fill;
    paint_18_fill.color = Colors.white;
    canvas.drawPath(path_18, paint_18_fill);

    Path path_19 = Path();
    path_19.moveTo(size.width * 0.5060794, size.height * 0.9444324);
    path_19.cubicTo(
        size.width * 0.5100756,
        size.height * 0.9478206,
        size.width * 0.5199084,
        size.height * 0.9570598,
        size.width * 0.5474863,
        size.height * 0.9546990);
    path_19.cubicTo(
        size.width * 0.5474863,
        size.height * 0.9546990,
        size.width * 0.5750641,
        size.height * 0.9573686,
        size.width * 0.5840176,
        size.height * 0.9401206);
    path_19.lineTo(size.width * 0.5838573, size.height * 0.9108618);
    path_19.lineTo(size.width * 0.5128740, size.height * 0.9096294);
    path_19.lineTo(size.width * 0.5046405, size.height * 0.9380676);
    path_19.cubicTo(
        size.width * 0.5039206,
        size.height * 0.9404284,
        size.width * 0.5044802,
        size.height * 0.9430980,
        size.width * 0.5060794,
        size.height * 0.9444324);
    path_19.close();

    Paint paint_19_fill = Paint()..style = PaintingStyle.fill;
    paint_19_fill.color = Colors.black;
    canvas.drawPath(path_19, paint_19_fill);

    Path path_20 = Path();
    path_20.moveTo(size.width * 0.6579031, size.height * 0.8584559);
    path_20.cubicTo(
        size.width * 0.6579031,
        size.height * 0.8584559,
        size.width * 0.6706130,
        size.height * 0.8716990,
        size.width * 0.6947542,
        size.height * 0.8510637);
    path_20.cubicTo(
        size.width * 0.7036267,
        size.height * 0.8435696,
        size.width * 0.7156176,
        size.height * 0.8209833,
        size.width * 0.7144985,
        size.height * 0.8037363);
    path_20.lineTo(size.width * 0.6969924, size.height * 0.7880284);
    path_20.lineTo(size.width * 0.6535069, size.height * 0.8238578);
    path_20.lineTo(size.width * 0.6579031, size.height * 0.8584559);
    path_20.close();

    Paint paint_20_fill = Paint()..style = PaintingStyle.fill;
    paint_20_fill.color = Colors.white;
    canvas.drawPath(path_20, paint_20_fill);

    Path path_21 = Path();
    path_21.moveTo(size.width * 0.6697672, size.height * 0.8648745);
    path_21.cubicTo(
        size.width * 0.6614542,
        size.height * 0.8648745,
        size.width * 0.6570573,
        size.height * 0.8604598,
        size.width * 0.6567382,
        size.height * 0.8601520);
    path_21.lineTo(size.width * 0.6561786, size.height * 0.8595363);
    path_21.lineTo(size.width * 0.6513824, size.height * 0.8222686);
    path_21.lineTo(size.width * 0.6971061, size.height * 0.7845912);
    path_21.lineTo(size.width * 0.7164504, size.height * 0.8020441);
    path_21.lineTo(size.width * 0.7165305, size.height * 0.8032765);
    path_21.cubicTo(
        size.width * 0.7177298,
        size.height * 0.8218578,
        size.width * 0.7050992,
        size.height * 0.8450608,
        size.width * 0.6959069,
        size.height * 0.8529657);
    path_21.cubicTo(
        size.width * 0.6849557,
        size.height * 0.8622049,
        size.width * 0.6761626,
        size.height * 0.8648745,
        size.width * 0.6697672,
        size.height * 0.8648745);
    path_21.close();
    path_21.moveTo(size.width * 0.6597756, size.height * 0.8566618);
    path_21.cubicTo(
        size.width * 0.6624130,
        size.height * 0.8587147,
        size.width * 0.6737641,
        size.height * 0.8655931,
        size.width * 0.6936687,
        size.height * 0.8486539);
    path_21.cubicTo(
        size.width * 0.7016626,
        size.height * 0.8417755,
        size.width * 0.7130130,
        size.height * 0.8211392,
        size.width * 0.7126137,
        size.height * 0.8049186);
    path_21.lineTo(size.width * 0.6970260, size.height * 0.7908539);
    path_21.lineTo(size.width * 0.6557786, size.height * 0.8248353);
    path_21.lineTo(size.width * 0.6597756, size.height * 0.8566618);
    path_21.close();

    Paint paint_21_fill = Paint()..style = PaintingStyle.fill;
    paint_21_fill.color = Colors.black;
    canvas.drawPath(path_21, paint_21_fill);

    Path path_22 = Path();
    path_22.moveTo(size.width * 0.4230588, size.height * 0.4287412);
    path_22.lineTo(size.width * 0.4230588, size.height * 0.3741235);
    path_22.cubicTo(
        size.width * 0.4230588,
        size.height * 0.3741235,
        size.width * 0.4591099,
        size.height * 0.3786412,
        size.width * 0.4320916,
        size.height * 0.2997951);
    path_22.lineTo(size.width * 0.3817313, size.height * 0.3203275);
    path_22.lineTo(size.width * 0.3889260, size.height * 0.3676559);
    path_22.lineTo(size.width * 0.3830908, size.height * 0.4256608);
    path_22.lineTo(size.width * 0.4230588, size.height * 0.4287412);
    path_22.close();

    Paint paint_22_fill = Paint()..style = PaintingStyle.fill;
    paint_22_fill.color = Colors.white;
    canvas.drawPath(path_22, paint_22_fill);

    Path path_23 = Path();
    path_23.moveTo(size.width * 0.4250122, size.height * 0.4314431);
    path_23.lineTo(size.width * 0.3808076, size.height * 0.4281578);
    path_23.lineTo(size.width * 0.3868824, size.height * 0.3677912);
    path_23.lineTo(size.width * 0.3793687, size.height * 0.3186157);
    path_23.lineTo(size.width * 0.4331656, size.height * 0.2966451);
    path_23.lineTo(size.width * 0.4338855, size.height * 0.2988010);
    path_23.cubicTo(
        size.width * 0.4449962,
        size.height * 0.3314480,
        size.width * 0.4470748,
        size.height * 0.3543422,
        size.width * 0.4398802,
        size.height * 0.3670725);
    path_23.cubicTo(
        size.width * 0.4353244,
        size.height * 0.3750804,
        size.width * 0.4284496,
        size.height * 0.3766206,
        size.width * 0.4250122,
        size.height * 0.3768255);
    path_23.lineTo(size.width * 0.4250122, size.height * 0.4314431);
    path_23.close();
    path_23.moveTo(size.width * 0.3853634, size.height * 0.4233324);
    path_23.lineTo(size.width * 0.4210153, size.height * 0.4260020);
    path_23.lineTo(size.width * 0.4210153, size.height * 0.3712814);
    path_23.lineTo(size.width * 0.4231740, size.height * 0.3715902);
    path_23.cubicTo(
        size.width * 0.4234931,
        size.height * 0.3715902,
        size.width * 0.4318870,
        size.height * 0.3725137,
        size.width * 0.4366832,
        size.height * 0.3639922);
    path_23.cubicTo(
        size.width * 0.4409198,
        size.height * 0.3564980,
        size.width * 0.4432382,
        size.height * 0.3398667,
        size.width * 0.4309275,
        size.height * 0.3030108);
    path_23.lineTo(size.width * 0.3840046, size.height * 0.3221059);
    path_23.lineTo(size.width * 0.3909595, size.height * 0.3676882);
    path_23.lineTo(size.width * 0.3853634, size.height * 0.4233324);
    path_23.close();

    Paint paint_23_fill = Paint()..style = PaintingStyle.fill;
    paint_23_fill.color = Colors.black;
    canvas.drawPath(path_23, paint_23_fill);

    Path path_24 = Path();
    path_24.moveTo(size.width * 0.4066145, size.height * 0.3298157);
    path_24.cubicTo(
        size.width * 0.4066145,
        size.height * 0.3298157,
        size.width * 0.4018183,
        size.height * 0.3178039,
        size.width * 0.3958229,
        size.height * 0.3257098);
    path_24.cubicTo(
        size.width * 0.3933450,
        size.height * 0.3290971,
        size.width * 0.3931053,
        size.height * 0.3384402,
        size.width * 0.4022977,
        size.height * 0.3424431);
    path_24.cubicTo(
        size.width * 0.4022977,
        size.height * 0.3424431,
        size.width * 0.4038168,
        size.height * 0.3634892,
        size.width * 0.3872695,
        size.height * 0.3656461);
    path_24.cubicTo(
        size.width * 0.3872695,
        size.height * 0.3656461,
        size.width * 0.3664863,
        size.height * 0.3388500,
        size.width * 0.3736008,
        size.height * 0.3084618);
    path_24.cubicTo(
        size.width * 0.3736008,
        size.height * 0.3084618,
        size.width * 0.3451427,
        size.height * 0.3177020,
        size.width * 0.3404267,
        size.height * 0.2844382);
    path_24.cubicTo(
        size.width * 0.3389878,
        size.height * 0.2744804,
        size.width * 0.3440237,
        size.height * 0.2554873,
        size.width * 0.3611305,
        size.height * 0.2536392);
    path_24.cubicTo(
        size.width * 0.3696038,
        size.height * 0.2527157,
        size.width * 0.3850313,
        size.height * 0.2580539,
        size.width * 0.3853511,
        size.height * 0.2841304);
    path_24.cubicTo(
        size.width * 0.3853511,
        size.height * 0.2841304,
        size.width * 0.4210031,
        size.height * 0.2582598,
        size.width * 0.4371504,
        size.height * 0.3058951);
    path_24.cubicTo(
        size.width * 0.4370702,
        size.height * 0.3057931,
        size.width * 0.4242802,
        size.height * 0.3288922,
        size.width * 0.4066145,
        size.height * 0.3298157);
    path_24.close();

    Paint paint_24_fill = Paint()..style = PaintingStyle.fill;
    paint_24_fill.color = Colors.black;
    canvas.drawPath(path_24, paint_24_fill);

    Path path_25 = Path();
    path_25.moveTo(size.width * 0.3170473, size.height * 0.6480833);
    path_25.cubicTo(
        size.width * 0.3170473,
        size.height * 0.6480833,
        size.width * 0.3170473,
        size.height * 0.6828863,
        size.width * 0.4141702,
        size.height * 0.6571176);
    path_25.cubicTo(
        size.width * 0.4141702,
        size.height * 0.6571176,
        size.width * 0.4427878,
        size.height * 0.6477755,
        size.width * 0.4491031,
        size.height * 0.6080441);
    path_25.lineTo(size.width * 0.3196855, size.height * 0.6236490);
    path_25.lineTo(size.width * 0.3170473, size.height * 0.6480833);
    path_25.close();

    Paint paint_25_fill = Paint()..style = PaintingStyle.fill;
    paint_25_fill.color = Colors.white;
    canvas.drawPath(path_25, paint_25_fill);

    Path path_26 = Path();
    path_26.moveTo(size.width * 0.3547634, size.height * 0.6697725);
    path_26.cubicTo(
        size.width * 0.3353382,
        size.height * 0.6697725,
        size.width * 0.3252664,
        size.height * 0.6649480,
        size.width * 0.3200702,
        size.height * 0.6595069);
    path_26.cubicTo(
        size.width * 0.3151947,
        size.height * 0.6543735,
        size.width * 0.3150344,
        size.height * 0.6492402,
        size.width * 0.3150344,
        size.height * 0.6487265);
    path_26.lineTo(size.width * 0.3150344, size.height * 0.6484186);
    path_26.lineTo(size.width * 0.3179122, size.height * 0.6219314);
    path_26.lineTo(size.width * 0.4515664, size.height * 0.6058137);
    path_26.lineTo(size.width * 0.4510069, size.height * 0.6092010);
    path_26.cubicTo(
        size.width * 0.4444519,
        size.height * 0.6500618,
        size.width * 0.4148756,
        size.height * 0.6602255,
        size.width * 0.4145557,
        size.height * 0.6603275);
    path_26.lineTo(size.width * 0.4144756, size.height * 0.6603275);
    path_26.cubicTo(
        size.width * 0.3883366,
        size.height * 0.6671039,
        size.width * 0.3689916,
        size.height * 0.6697725,
        size.width * 0.3547634,
        size.height * 0.6697725);
    path_26.close();
    path_26.moveTo(size.width * 0.3190313, size.height * 0.6487265);
    path_26.cubicTo(
        size.width * 0.3191115,
        size.height * 0.6494451,
        size.width * 0.3195908,
        size.height * 0.6525255,
        size.width * 0.3228687,
        size.height * 0.6558108);
    path_26.cubicTo(
        size.width * 0.3298229,
        size.height * 0.6627922,
        size.width * 0.3508458,
        size.height * 0.6719284,
        size.width * 0.4136763,
        size.height * 0.6551951);
    path_26.cubicTo(
        size.width * 0.4151954,
        size.height * 0.6546814,
        size.width * 0.4395756,
        size.height * 0.6458520,
        size.width * 0.4465305,
        size.height * 0.6114598);
    path_26.lineTo(size.width * 0.3214298, size.height * 0.6265520);
    path_26.lineTo(size.width * 0.3190313, size.height * 0.6487265);
    path_26.close();

    Paint paint_26_fill = Paint()..style = PaintingStyle.fill;
    paint_26_fill.color = Colors.black;
    canvas.drawPath(path_26, paint_26_fill);

    Path path_27 = Path();
    path_27.moveTo(size.width * 0.5516389, size.height * 0.9156137);
    path_27.cubicTo(
        size.width * 0.5516389,
        size.height * 0.9156137,
        size.width * 0.5603527,
        size.height * 0.9570902,
        size.width * 0.5808962,
        size.height * 0.9616069);
    path_27.cubicTo(
        size.width * 0.6066359,
        size.height * 0.9672529,
        size.width * 0.6139901,
        size.height * 0.9848137,
        size.width * 0.6145496,
        size.height * 0.9889118);
    path_27.lineTo(size.width * 0.5141489, size.height * 0.9889118);
    path_27.lineTo(size.width * 0.5211038, size.height * 0.9170510);
    path_27.lineTo(size.width * 0.5516389, size.height * 0.9156137);
    path_27.close();

    Paint paint_27_fill = Paint()..style = PaintingStyle.fill;
    paint_27_fill.color = _activeColor;
    canvas.drawPath(path_27, paint_27_fill);

    Path path_28 = Path();
    path_28.moveTo(size.width * 0.6169550, size.height * 0.9914118);
    path_28.lineTo(size.width * 0.5118382, size.height * 0.9914118);
    path_28.lineTo(size.width * 0.5193519, size.height * 0.9145206);
    path_28.lineTo(size.width * 0.5531656, size.height * 0.9129804);
    path_28.lineTo(size.width * 0.5535649, size.height * 0.9149314);
    path_28.cubicTo(
        size.width * 0.5536450,
        size.height * 0.9153422,
        size.width * 0.5621985,
        size.height * 0.9548676,
        size.width * 0.5812229,
        size.height * 0.9590765);
    path_28.cubicTo(
        size.width * 0.6072023,
        size.height * 0.9648255,
        size.width * 0.6155954,
        size.height * 0.9825882,
        size.width * 0.6165550,
        size.height * 0.9884412);
    path_28.lineTo(size.width * 0.6169550, size.height * 0.9914118);
    path_28.close();
    path_28.moveTo(size.width * 0.5163145, size.height * 0.9862843);
    path_28.lineTo(size.width * 0.6115191, size.height * 0.9862843);
    path_28.cubicTo(
        size.width * 0.6088015,
        size.height * 0.9801225,
        size.width * 0.6004076,
        size.height * 0.9685216,
        size.width * 0.5804237,
        size.height * 0.9641069);
    path_28.cubicTo(
        size.width * 0.5613191,
        size.height * 0.9598980,
        size.width * 0.5521260,
        size.height * 0.9268402,
        size.width * 0.5500481,
        size.height * 0.9182167);
    path_28.lineTo(size.width * 0.5227893, size.height * 0.9194480);
    path_28.lineTo(size.width * 0.5163145, size.height * 0.9862843);
    path_28.close();

    Paint paint_28_fill = Paint()..style = PaintingStyle.fill;
    paint_28_fill.color = Colors.black;
    canvas.drawPath(path_28, paint_28_fill);

    Path path_29 = Path();
    path_29.moveTo(size.width * 0.6978298, size.height * 0.8017000);
    path_29.cubicTo(
        size.width * 0.6978298,
        size.height * 0.8017000,
        size.width * 0.7215710,
        size.height * 0.8305490,
        size.width * 0.7438733,
        size.height * 0.8192559);
    path_29.cubicTo(
        size.width * 0.7681756,
        size.height * 0.8070392,
        size.width * 0.7814427,
        size.height * 0.8155598,
        size.width * 0.7836794,
        size.height * 0.8185373);
    path_29.lineTo(size.width * 0.6995084, size.height * 0.8889647);
    path_29.lineTo(size.width * 0.6694519, size.height * 0.8186402);
    path_29.lineTo(size.width * 0.6978298, size.height * 0.8017000);
    path_29.close();

    Paint paint_29_fill = Paint()..style = PaintingStyle.fill;
    paint_29_fill.color = _activeColor;
    canvas.drawPath(path_29, paint_29_fill);

    Path path_30 = Path();
    path_30.moveTo(size.width * 0.6989252, size.height * 0.8927559);
    path_30.lineTo(size.width * 0.6667908, size.height * 0.8176059);
    path_30.lineTo(size.width * 0.6982061, size.height * 0.7988186);
    path_30.lineTo(size.width * 0.6991649, size.height * 0.8000510);
    path_30.cubicTo(
        size.width * 0.6993252,
        size.height * 0.8002559,
        size.width * 0.7154725,
        size.height * 0.8194539,
        size.width * 0.7329786,
        size.height * 0.8194539);
    path_30.cubicTo(
        size.width * 0.7363359,
        size.height * 0.8194539,
        size.width * 0.7397733,
        size.height * 0.8187353,
        size.width * 0.7431305,
        size.height * 0.8169902);
    path_30.cubicTo(
        size.width * 0.7679084,
        size.height * 0.8044647,
        size.width * 0.7821374,
        size.height * 0.8128833,
        size.width * 0.7850992,
        size.height * 0.8168873);
    path_30.lineTo(size.width * 0.7867786, size.height * 0.8191461);
    path_30.lineTo(size.width * 0.6989252, size.height * 0.8927559);
    path_30.close();
    path_30.moveTo(size.width * 0.6723069, size.height * 0.8198647);
    path_30.lineTo(size.width * 0.7002840, size.height * 0.8853637);
    path_30.lineTo(size.width * 0.7800611, size.height * 0.8186324);
    path_30.cubicTo(
        size.width * 0.7751832,
        size.height * 0.8156549,
        size.width * 0.7632740,
        size.height * 0.8123706,
        size.width * 0.7446489,
        size.height * 0.8218157);
    path_30.cubicTo(
        size.width * 0.7237855,
        size.height * 0.8322873,
        size.width * 0.7024427,
        size.height * 0.8104196,
        size.width * 0.6974863,
        size.height * 0.8048755);
    path_30.lineTo(size.width * 0.6723069, size.height * 0.8198647);
    path_30.close();

    Paint paint_30_fill = Paint()..style = PaintingStyle.fill;
    paint_30_fill.color = Colors.black;
    canvas.drawPath(path_30, paint_30_fill);

    Path path_31 = Path();
    path_31.moveTo(size.width * 0.3378008, size.height * 0.6174078);
    path_31.cubicTo(
        size.width * 0.3378008,
        size.height * 0.6174078,
        size.width * 0.3222931,
        size.height * 0.7225353,
        size.width * 0.3744916,
        size.height * 0.7351627);
    path_31.cubicTo(
        size.width * 0.4266908,
        size.height * 0.7477902,
        size.width * 0.5152603,
        size.height * 0.7131931,
        size.width * 0.5152603,
        size.height * 0.7131931);
    path_31.lineTo(size.width * 0.5063076, size.height * 0.9352549);
    path_31.cubicTo(
        size.width * 0.5063076,
        size.height * 0.9352549,
        size.width * 0.5250924,
        size.height * 0.9212922,
        size.width * 0.5452366,
        size.height * 0.9227294);
    path_31.cubicTo(
        size.width * 0.5664198,
        size.height * 0.9242696,
        size.width * 0.5820878,
        size.height * 0.9362814,
        size.width * 0.5820878,
        size.height * 0.9362814);
    path_31.lineTo(size.width * 0.6033504, size.height * 0.6178186);
    path_31.lineTo(size.width * 0.3378008, size.height * 0.6174078);
    path_31.close();

    Paint paint_31_fill = Paint()..style = PaintingStyle.fill;
    paint_31_fill.color = Colors.white;
    canvas.drawPath(path_31, paint_31_fill);

    Path path_32 = Path();
    path_32.moveTo(size.width * 0.5837557, size.height * 0.9401922);
    path_32.lineTo(size.width * 0.5810382, size.height * 0.9380353);
    path_32.cubicTo(
        size.width * 0.5808779,
        size.height * 0.9379333,
        size.width * 0.5655305,
        size.height * 0.9262294,
        size.width * 0.5450664,
        size.height * 0.9247922);
    path_32.cubicTo(
        size.width * 0.5258015,
        size.height * 0.9234578,
        size.width * 0.5074962,
        size.height * 0.9369069,
        size.width * 0.5072565,
        size.height * 0.9370088);
    path_32.lineTo(size.width * 0.5040588, size.height * 0.9393706);
    path_32.lineTo(size.width * 0.5042191, size.height * 0.9346480);
    path_32.lineTo(size.width * 0.5130122, size.height * 0.7161794);
    path_32.cubicTo(
        size.width * 0.4974244,
        size.height * 0.7219284,
        size.width * 0.4215641,
        size.height * 0.7486216,
        size.width * 0.3740023,
        size.height * 0.7372255);
    path_32.cubicTo(
        size.width * 0.3619321,
        size.height * 0.7343510,
        size.width * 0.3523397,
        size.height * 0.7263431,
        size.width * 0.3455450,
        size.height * 0.7136127);
    path_32.cubicTo(
        size.width * 0.3268397,
        size.height * 0.6783990,
        size.width * 0.3353130,
        size.height * 0.6190598,
        size.width * 0.3357122,
        size.height * 0.6164931);
    path_32.lineTo(size.width * 0.3360321, size.height * 0.6144402);
    path_32.lineTo(size.width * 0.6054191, size.height * 0.6148500);
    path_32.lineTo(size.width * 0.6052588, size.height * 0.6176225);
    path_32.lineTo(size.width * 0.5837557, size.height * 0.9401922);
    path_32.close();
    path_32.moveTo(size.width * 0.5421885, size.height * 0.9196588);
    path_32.cubicTo(
        size.width * 0.5432282,
        size.height * 0.9196588,
        size.width * 0.5442672,
        size.height * 0.9196588,
        size.width * 0.5452267,
        size.height * 0.9197618);
    path_32.cubicTo(
        size.width * 0.5616931,
        size.height * 0.9208912,
        size.width * 0.5748031,
        size.height * 0.9282824,
        size.width * 0.5802389,
        size.height * 0.9317735);
    path_32.lineTo(size.width * 0.6010221, size.height * 0.6199833);
    path_32.lineTo(size.width * 0.3393099, size.height * 0.6195725);
    path_32.cubicTo(
        size.width * 0.3379511,
        size.height * 0.6304549,
        size.width * 0.3329145,
        size.height * 0.6808627,
        size.width * 0.3488221,
        size.height * 0.7107382);
    path_32.cubicTo(
        size.width * 0.3549771,
        size.height * 0.7223392,
        size.width * 0.3636901,
        size.height * 0.7295255,
        size.width * 0.3747214,
        size.height * 0.7321951);
    path_32.cubicTo(
        size.width * 0.4258809,
        size.height * 0.7445147,
        size.width * 0.5136511,
        size.height * 0.7106353,
        size.width * 0.5145305,
        size.height * 0.7102245);
    path_32.lineTo(size.width * 0.5172489, size.height * 0.7091980);
    path_32.lineTo(size.width * 0.5083756, size.height * 0.9304382);
    path_32.cubicTo(
        size.width * 0.5143710,
        size.height * 0.9268451,
        size.width * 0.5276405,
        size.height * 0.9196588,
        size.width * 0.5421885,
        size.height * 0.9196588);
    path_32.close();

    Paint paint_32_fill = Paint()..style = PaintingStyle.fill;
    paint_32_fill.color = Colors.black;
    canvas.drawPath(path_32, paint_32_fill);

    Path path_33 = Path();
    path_33.moveTo(size.width * 0.3858427, size.height * 0.3911039);
    path_33.cubicTo(
        size.width * 0.3858427,
        size.height * 0.3911039,
        size.width * 0.3704954,
        size.height * 0.3975716,
        size.width * 0.3539481,
        size.height * 0.4048608);
    path_33.cubicTo(
        size.width * 0.3331649,
        size.height * 0.4141000,
        size.width * 0.3190962,
        size.height * 0.4391500,
        size.width * 0.3187763,
        size.height * 0.4673833);
    path_33.lineTo(size.width * 0.3170176, size.height * 0.6474549);
    path_33.cubicTo(
        size.width * 0.3170176,
        size.height * 0.6474549,
        size.width * 0.3449153,
        size.height * 0.6274353,
        size.width * 0.3817664,
        size.height * 0.6245608);
    path_33.cubicTo(
        size.width * 0.3987130,
        size.height * 0.6232265,
        size.width * 0.4505115,
        size.height * 0.6197363,
        size.width * 0.4505115,
        size.height * 0.6197363);
    path_33.lineTo(size.width * 0.4489931, size.height * 0.5145059);
    path_33.lineTo(size.width * 0.4759313, size.height * 0.5516696);
    path_33.lineTo(size.width * 0.5071069, size.height * 0.5013647);
    path_33.lineTo(size.width * 0.4587450, size.height * 0.4313480);
    path_33.cubicTo(
        size.width * 0.4550679,
        size.height * 0.4236480,
        size.width * 0.4501122,
        size.height * 0.4171804,
        size.width * 0.4441969,
        size.height * 0.4123549);
    path_33.lineTo(size.width * 0.4243725, size.height * 0.3948000);
    path_33.cubicTo(
        size.width * 0.4243725,
        size.height * 0.3946971,
        size.width * 0.4179779,
        size.height * 0.4269333,
        size.width * 0.3858427,
        size.height * 0.3911039);
    path_33.close();

    Paint paint_33_fill = Paint()..style = PaintingStyle.fill;
    paint_33_fill.color = _activeColor;
    canvas.drawPath(path_33, paint_33_fill);

    Path path_34 = Path();
    path_34.moveTo(size.width * 0.3150344, size.height * 0.6517353);
    path_34.lineTo(size.width * 0.3168733, size.height * 0.4671461);
    path_34.cubicTo(
        size.width * 0.3171931,
        size.height * 0.4378863,
        size.width * 0.3318214,
        size.height * 0.4118098,
        size.width * 0.3533244,
        size.height * 0.4022627);
    path_34.cubicTo(
        size.width * 0.3697115,
        size.height * 0.3949735,
        size.width * 0.3851389,
        size.height * 0.3885049,
        size.width * 0.3852985,
        size.height * 0.3884029);
    path_34.lineTo(size.width * 0.3863382, size.height * 0.3879922);
    path_34.lineTo(size.width * 0.3872176, size.height * 0.3889157);
    path_34.cubicTo(
        size.width * 0.3987282,
        size.height * 0.4017490,
        size.width * 0.4082405,
        size.height * 0.4071902,
        size.width * 0.4147954,
        size.height * 0.4046235);
    path_34.cubicTo(
        size.width * 0.4207107,
        size.height * 0.4022627,
        size.width * 0.4224695,
        size.height * 0.3939471,
        size.width * 0.4224695,
        size.height * 0.3939471);
    path_34.lineTo(size.width * 0.4231885, size.height * 0.3904559);
    path_34.lineTo(size.width * 0.4255069, size.height * 0.3925088);
    path_34.lineTo(size.width * 0.4453313, size.height * 0.4100647);
    path_34.cubicTo(
        size.width * 0.4513267,
        size.height * 0.4149931,
        size.width * 0.4566023,
        size.height * 0.4217686,
        size.width * 0.4603595,
        size.height * 0.4296735);
    path_34.lineTo(size.width * 0.5097603, size.height * 0.5011275);
    path_34.lineTo(size.width * 0.4761069, size.height * 0.5554363);
    path_34.lineTo(size.width * 0.4510870, size.height * 0.5209412);
    path_34.lineTo(size.width * 0.4526053, size.height * 0.6219627);
    path_34.lineTo(size.width * 0.4506870, size.height * 0.6220657);
    path_34.cubicTo(
        size.width * 0.4502076,
        size.height * 0.6220657,
        size.width * 0.3987282,
        size.height * 0.6256588,
        size.width * 0.3819412,
        size.height * 0.6268902);
    path_34.cubicTo(
        size.width * 0.3460504,
        size.height * 0.6296627,
        size.width * 0.3183122,
        size.height * 0.6492716,
        size.width * 0.3180725,
        size.height * 0.6494765);
    path_34.lineTo(size.width * 0.3150344, size.height * 0.6517353);
    path_34.close();
    path_34.moveTo(size.width * 0.3854588, size.height * 0.3938441);
    path_34.cubicTo(
        size.width * 0.3817015,
        size.height * 0.3954863,
        size.width * 0.3685122,
        size.height * 0.4010304,
        size.width * 0.3546832,
        size.height * 0.4071902);
    path_34.cubicTo(
        size.width * 0.3347786,
        size.height * 0.4160196,
        size.width * 0.3211901,
        size.height * 0.4402480,
        size.width * 0.3208702,
        size.height * 0.4673510);
    path_34.lineTo(size.width * 0.3191115, size.height * 0.6432137);
    path_34.cubicTo(
        size.width * 0.3273450,
        size.height * 0.6379784,
        size.width * 0.3517252,
        size.height * 0.6243235,
        size.width * 0.3817015,
        size.height * 0.6219627);
    path_34.cubicTo(
        size.width * 0.3966496,
        size.height * 0.6208333,
        size.width * 0.4389366,
        size.height * 0.6178559,
        size.width * 0.4485290,
        size.height * 0.6172402);
    path_34.lineTo(size.width * 0.4469298, size.height * 0.5078010);
    path_34.lineTo(size.width * 0.4759473, size.height * 0.5477373);
    path_34.lineTo(size.width * 0.5046443, size.height * 0.5014353);
    path_34.lineTo(size.width * 0.4572420, size.height * 0.4328559);
    path_34.lineTo(size.width * 0.4571618, size.height * 0.4326510);
    path_34.cubicTo(
        size.width * 0.4536450,
        size.height * 0.4253618,
        size.width * 0.4488481,
        size.height * 0.4189961,
        size.width * 0.4432527,
        size.height * 0.4144794);
    path_34.lineTo(size.width * 0.4253473, size.height * 0.3985667);
    path_34.cubicTo(
        size.width * 0.4240679,
        size.height * 0.4020569,
        size.width * 0.4212702,
        size.height * 0.4074980,
        size.width * 0.4159947,
        size.height * 0.4095520);
    path_34.cubicTo(
        size.width * 0.4081611,
        size.height * 0.4127343,
        size.width * 0.3978489,
        size.height * 0.4073951,
        size.width * 0.3854588,
        size.height * 0.3938441);
    path_34.close();

    Paint paint_34_fill = Paint()..style = PaintingStyle.fill;
    paint_34_fill.color = Colors.black;
    canvas.drawPath(path_34, paint_34_fill);

    Path path_35 = Path();
    path_35.moveTo(size.width * 0.6087954, size.height * 0.5794843);
    path_35.lineTo(size.width * 0.7151908, size.height * 0.8029833);
    path_35.cubicTo(
        size.width * 0.7151908,
        size.height * 0.8029833,
        size.width * 0.7042397,
        size.height * 0.8003147,
        size.width * 0.6847351,
        size.height * 0.8164324);
    path_35.cubicTo(
        size.width * 0.6691473,
        size.height * 0.8293686,
        size.width * 0.6579565,
        size.height * 0.8565745,
        size.width * 0.6579565,
        size.height * 0.8565745);
    path_35.cubicTo(
        size.width * 0.6579565,
        size.height * 0.8565745,
        size.width * 0.5661092,
        size.height * 0.6923118,
        size.width * 0.5325359,
        size.height * 0.6172647);
    path_35.lineTo(size.width * 0.4406084, size.height * 0.6174706);
    path_35.lineTo(size.width * 0.5726641, size.height * 0.5624422);
    path_35.cubicTo(
        size.width * 0.5864130,
        size.height * 0.5566931,
        size.width * 0.6013611,
        size.height * 0.5637775,
        size.width * 0.6087954,
        size.height * 0.5794843);
    path_35.close();

    Paint paint_35_fill = Paint()..style = PaintingStyle.fill;
    paint_35_fill.color = Colors.white;
    canvas.drawPath(path_35, paint_35_fill);

    Path path_36 = Path();
    path_36.moveTo(size.width * 0.6582855, size.height * 0.8615294);
    path_36.lineTo(size.width * 0.6563664, size.height * 0.8581422);
    path_36.cubicTo(
        size.width * 0.6554878,
        size.height * 0.8564990,
        size.width * 0.5651588,
        size.height * 0.6948039,
        size.width * 0.5314260,
        size.height * 0.6198598);
    path_36.lineTo(size.width * 0.4279878, size.height * 0.6201676);
    path_36.lineTo(size.width * 0.5721939, size.height * 0.5601088);
    path_36.cubicTo(
        size.width * 0.5868221,
        size.height * 0.5540520,
        size.width * 0.6025695,
        size.height * 0.5615461,
        size.width * 0.6105634,
        size.height * 0.5783833);
    path_36.lineTo(size.width * 0.7192771, size.height * 0.8067069);
    path_36.lineTo(size.width * 0.7148802, size.height * 0.8056804);
    path_36.cubicTo(
        size.width * 0.7148008,
        size.height * 0.8056804,
        size.width * 0.7043290,
        size.height * 0.8035245,
        size.width * 0.6858634,
        size.height * 0.8188216);
    path_36.cubicTo(
        size.width * 0.6709153,
        size.height * 0.8312441,
        size.width * 0.6598840,
        size.height * 0.8577314,
        size.width * 0.6598038,
        size.height * 0.8580392);
    path_36.lineTo(size.width * 0.6582855, size.height * 0.8615294);
    path_36.close();
    path_36.moveTo(size.width * 0.5337443, size.height * 0.6148284);
    path_36.lineTo(size.width * 0.5343038, size.height * 0.6161637);
    path_36.cubicTo(
        size.width * 0.5648397,
        size.height * 0.6843324,
        size.width * 0.6446962,
        size.height * 0.8284716,
        size.width * 0.6578053,
        size.height * 0.8519824);
    path_36.cubicTo(
        size.width * 0.6614824,
        size.height * 0.8440765,
        size.width * 0.6710748,
        size.height * 0.8248784,
        size.width * 0.6837053,
        size.height * 0.8145098);
    path_36.cubicTo(
        size.width * 0.6970542,
        size.height * 0.8034216,
        size.width * 0.7066466,
        size.height * 0.8009578,
        size.width * 0.7116031,
        size.height * 0.8005471);
    path_36.lineTo(size.width * 0.6071260, size.height * 0.5809500);
    path_36.cubicTo(
        size.width * 0.6000916,
        size.height * 0.5661657,
        size.width * 0.5861824,
        size.height * 0.5595961,
        size.width * 0.5733924,
        size.height * 0.5649343);
    path_36.lineTo(size.width * 0.4534076, size.height * 0.6149314);
    path_36.lineTo(size.width * 0.5337443, size.height * 0.6148284);
    path_36.close();

    Paint paint_36_fill = Paint()..style = PaintingStyle.fill;
    paint_36_fill.color = Colors.black;
    canvas.drawPath(path_36, paint_36_fill);

    Path path_37 = Path();
    path_37.moveTo(size.width * 0.4474023, size.height * 0.4653108);
    path_37.lineTo(size.width * 0.4434191, size.height * 0.4657245);
    path_37.lineTo(size.width * 0.4471198, size.height * 0.5244588);
    path_37.lineTo(size.width * 0.4511038, size.height * 0.5240451);
    path_37.lineTo(size.width * 0.4474023, size.height * 0.4653108);
    path_37.close();

    Paint paint_37_fill = Paint()..style = PaintingStyle.fill;
    paint_37_fill.color = Colors.black;
    canvas.drawPath(path_37, paint_37_fill);

    Path path_38 = Path();
    path_38.moveTo(size.width * 0.5279282, size.height * 0.3059706);
    path_38.lineTo(size.width * 0.4953137, size.height * 0.3066892);
    path_38.cubicTo(
        size.width * 0.4941947,
        size.height * 0.3066892,
        size.width * 0.4932351,
        size.height * 0.3078186,
        size.width * 0.4930756,
        size.height * 0.3093588);
    path_38.lineTo(size.width * 0.4834031, size.height * 0.3889225);
    path_38.cubicTo(
        size.width * 0.4831634,
        size.height * 0.3908735,
        size.width * 0.4842023,
        size.height * 0.3925157,
        size.width * 0.4855618,
        size.height * 0.3925157);
    path_38.lineTo(size.width * 0.5181756, size.height * 0.3917971);
    path_38.cubicTo(
        size.width * 0.5192947,
        size.height * 0.3917971,
        size.width * 0.5202542,
        size.height * 0.3906676,
        size.width * 0.5204137,
        size.height * 0.3891284);
    path_38.lineTo(size.width * 0.5300863, size.height * 0.3095637);
    path_38.cubicTo(
        size.width * 0.5303260,
        size.height * 0.3077157,
        size.width * 0.5292870,
        size.height * 0.3059706,
        size.width * 0.5279282,
        size.height * 0.3059706);
    path_38.close();

    Paint paint_38_fill = Paint()..style = PaintingStyle.fill;
    paint_38_fill.color = Colors.black;
    canvas.drawPath(path_38, paint_38_fill);

    Path path_39 = Path();
    path_39.moveTo(size.width * 0.4795748, size.height * 0.4802627);
    path_39.lineTo(size.width * 0.5088321, size.height * 0.3913559);
    path_39.lineTo(size.width * 0.4994794, size.height * 0.3719520);
    path_39.lineTo(size.width * 0.4895672, size.height * 0.3689755);
    path_39.cubicTo(
        size.width * 0.4870092,
        size.height * 0.3683588,
        size.width * 0.4851710,
        size.height * 0.3655873,
        size.width * 0.4850908,
        size.height * 0.3623020);
    path_39.cubicTo(
        size.width * 0.4848511,
        size.height * 0.3534725,
        size.width * 0.4850107,
        size.height * 0.3376627,
        size.width * 0.4941237,
        size.height * 0.3338637);
    path_39.lineTo(size.width * 0.5106702, size.height * 0.3338637);
    path_39.cubicTo(
        size.width * 0.5127489,
        size.height * 0.3338637,
        size.width * 0.5147473,
        size.height * 0.3351990,
        size.width * 0.5159466,
        size.height * 0.3374569);
    path_39.lineTo(size.width * 0.5217015, size.height * 0.3459784);
    path_39.cubicTo(
        size.width * 0.5217015,
        size.height * 0.3459784,
        size.width * 0.5217015,
        size.height * 0.3302706,
        size.width * 0.5336924,
        size.height * 0.3336588);
    path_39.lineTo(size.width * 0.5322534, size.height * 0.3754431);
    path_39.cubicTo(
        size.width * 0.5322534,
        size.height * 0.3754431,
        size.width * 0.5237000,
        size.height * 0.4908373,
        size.width * 0.5127489,
        size.height * 0.5264608);
    path_39.cubicTo(
        size.width * 0.5085924,
        size.height * 0.5399098,
        size.width * 0.4865298,
        size.height * 0.5769716,
        size.width * 0.4697427,
        size.height * 0.5430931);
    path_39.cubicTo(
        size.width * 0.4633481,
        size.height * 0.5297461,
        size.width * 0.4795748,
        size.height * 0.4802627,
        size.width * 0.4795748,
        size.height * 0.4802627);
    path_39.close();

    Paint paint_39_fill = Paint()..style = PaintingStyle.fill;
    paint_39_fill.color = Colors.white;
    canvas.drawPath(path_39, paint_39_fill);

    Path path_40 = Path();
    path_40.moveTo(size.width * 0.4856840, size.height * 0.5591873);
    path_40.cubicTo(
        size.width * 0.4790489,
        size.height * 0.5591873,
        size.width * 0.4732137,
        size.height * 0.5541559,
        size.width * 0.4682580,
        size.height * 0.5440951);
    path_40.cubicTo(
        size.width * 0.4613031,
        size.height * 0.5301333,
        size.width * 0.4760916,
        size.height * 0.4841392,
        size.width * 0.4777702,
        size.height * 0.4790069);
    path_40.lineTo(size.width * 0.5066275, size.height * 0.3913314);
    path_40.lineTo(size.width * 0.4983137, size.height * 0.3739814);
    path_40.lineTo(size.width * 0.4892008, size.height * 0.3712098);
    path_40.cubicTo(
        size.width * 0.4857641,
        size.height * 0.3703882,
        size.width * 0.4832855,
        size.height * 0.3665902,
        size.width * 0.4832061,
        size.height * 0.3620725);
    path_40.cubicTo(
        size.width * 0.4830458,
        size.height * 0.3548863,
        size.width * 0.4825664,
        size.height * 0.3357902,
        size.width * 0.4935977,
        size.height * 0.3310676);
    path_40.lineTo(size.width * 0.4939176, size.height * 0.3309657);
    path_40.lineTo(size.width * 0.5107840, size.height * 0.3309657);
    path_40.cubicTo(
        size.width * 0.5135015,
        size.height * 0.3309657,
        size.width * 0.5160595,
        size.height * 0.3327108,
        size.width * 0.5176588,
        size.height * 0.3355853);
    path_40.lineTo(size.width * 0.5206160, size.height * 0.3398971);
    path_40.cubicTo(
        size.width * 0.5211756,
        size.height * 0.3375363,
        size.width * 0.5222947,
        size.height * 0.3348667,
        size.width * 0.5241336,
        size.height * 0.3330186);
    path_40.cubicTo(
        size.width * 0.5266916,
        size.height * 0.3303490,
        size.width * 0.5301290,
        size.height * 0.3296304,
        size.width * 0.5342855,
        size.height * 0.3308627);
    path_40.lineTo(size.width * 0.5358840, size.height * 0.3313765);
    path_40.lineTo(size.width * 0.5343656, size.height * 0.3755216);
    path_40.cubicTo(
        size.width * 0.5340458,
        size.height * 0.3802441,
        size.width * 0.5256519,
        size.height * 0.4917373,
        size.width * 0.5147008,
        size.height * 0.5272588);
    path_40.cubicTo(
        size.width * 0.5115832,
        size.height * 0.5373196,
        size.width * 0.4999924,
        size.height * 0.5577500,
        size.width * 0.4872824,
        size.height * 0.5592892);
    path_40.cubicTo(
        size.width * 0.4868031,
        size.height * 0.5591873,
        size.width * 0.4862435,
        size.height * 0.5591873,
        size.width * 0.4856840,
        size.height * 0.5591873);
    path_40.close();
    path_40.moveTo(size.width * 0.4814473, size.height * 0.4809569);
    path_40.cubicTo(
        size.width * 0.4770504,
        size.height * 0.4944059,
        size.width * 0.4666588,
        size.height * 0.5313647,
        size.width * 0.4716153,
        size.height * 0.5413235);
    path_40.cubicTo(
        size.width * 0.4760916,
        size.height * 0.5503578,
        size.width * 0.4812870,
        size.height * 0.5546696,
        size.width * 0.4869626,
        size.height * 0.5539510);
    path_40.cubicTo(
        size.width * 0.4982336,
        size.height * 0.5525137,
        size.width * 0.5086260,
        size.height * 0.5330078,
        size.width * 0.5111038,
        size.height * 0.5252049);
    path_40.cubicTo(
        size.width * 0.5217351,
        size.height * 0.4907098,
        size.width * 0.5302084,
        size.height * 0.3781912,
        size.width * 0.5305282,
        size.height * 0.3750078);
    path_40.lineTo(size.width * 0.5318870, size.height * 0.3354824);
    path_40.cubicTo(
        size.width * 0.5297290,
        size.height * 0.3351745,
        size.width * 0.5280504,
        size.height * 0.3356882,
        size.width * 0.5267710,
        size.height * 0.3370225);
    path_40.cubicTo(
        size.width * 0.5240534,
        size.height * 0.3397941,
        size.width * 0.5239733,
        size.height * 0.3456461,
        size.width * 0.5239733,
        size.height * 0.3456461);
    path_40.lineTo(size.width * 0.5238939, size.height * 0.3524225);
    path_40.lineTo(size.width * 0.5145412, size.height * 0.3385627);
    path_40.cubicTo(
        size.width * 0.5137420,
        size.height * 0.3370225,
        size.width * 0.5123824,
        size.height * 0.3360990,
        size.width * 0.5109435,
        size.height * 0.3360990);
    path_40.lineTo(size.width * 0.4947168, size.height * 0.3360990);
    path_40.cubicTo(
        size.width * 0.4876824,
        size.height * 0.3393843,
        size.width * 0.4871229,
        size.height * 0.3516010,
        size.width * 0.4873626,
        size.height * 0.3619696);
    path_40.cubicTo(
        size.width * 0.4874427,
        size.height * 0.3641255,
        size.width * 0.4885618,
        size.height * 0.3658706,
        size.width * 0.4901603,
        size.height * 0.3661794);
    path_40.lineTo(size.width * 0.4902405, size.height * 0.3661794);
    path_40.lineTo(size.width * 0.5009519, size.height * 0.3694647);
    path_40.lineTo(size.width * 0.5112634, size.height * 0.3909206);
    path_40.lineTo(size.width * 0.4814473, size.height * 0.4809569);
    path_40.close();

    Paint paint_40_fill = Paint()..style = PaintingStyle.fill;
    paint_40_fill.color = Colors.black;
    canvas.drawPath(path_40, paint_40_fill);

    Path path_41 = Path();
    path_41.moveTo(size.width * 0.5243687, size.height * 0.3169098);
    path_41.cubicTo(
        size.width * 0.5248603,
        size.height * 0.3146137,
        size.width * 0.5240168,
        size.height * 0.3123127,
        size.width * 0.5224847,
        size.height * 0.3117716);
    path_41.cubicTo(
        size.width * 0.5209519,
        size.height * 0.3112304,
        size.width * 0.5193115,
        size.height * 0.3126529,
        size.width * 0.5188191,
        size.height * 0.3149490);
    path_41.cubicTo(
        size.width * 0.5183275,
        size.height * 0.3172451,
        size.width * 0.5191710,
        size.height * 0.3195451,
        size.width * 0.5207031,
        size.height * 0.3200863);
    path_41.cubicTo(
        size.width * 0.5222359,
        size.height * 0.3206284,
        size.width * 0.5238763,
        size.height * 0.3192059,
        size.width * 0.5243687,
        size.height * 0.3169098);
    path_41.close();

    Paint paint_41_fill = Paint()..style = PaintingStyle.fill;
    paint_41_fill.color = Colors.white;
    canvas.drawPath(path_41, paint_41_fill);

    Path path_42 = Path();
    path_42.moveTo(size.width * 0.3232870, size.height * 0.5040863);
    path_42.lineTo(size.width * 0.3676519, size.height * 0.4943333);
    path_42.lineTo(size.width * 0.3688511, size.height * 0.5102461);
    path_42.lineTo(size.width * 0.4339191, size.height * 0.4411529);
    path_42.lineTo(size.width * 0.4423924, size.height * 0.4186696);
    path_42.cubicTo(
        size.width * 0.4435916,
        size.height * 0.4152814,
        size.width * 0.4456702,
        size.height * 0.4117912,
        size.width * 0.4472687,
        size.height * 0.4102510);
    path_42.lineTo(size.width * 0.4684519, size.height * 0.3816078);
    path_42.cubicTo(
        size.width * 0.4704504,
        size.height * 0.3789392,
        size.width * 0.4740473,
        size.height * 0.3810951,
        size.width * 0.4735679,
        size.height * 0.3847912);
    path_42.cubicTo(
        size.width * 0.4726084,
        size.height * 0.3933118,
        size.width * 0.4680519,
        size.height * 0.4064529,
        size.width * 0.4607779,
        size.height * 0.4178480);
    path_42.cubicTo(
        size.width * 0.4607779,
        size.height * 0.4178480,
        size.width * 0.4703702,
        size.height * 0.4332480,
        size.width * 0.4671725,
        size.height * 0.4445412);
    path_42.lineTo(size.width * 0.4503863, size.height * 0.4671275);
    path_42.lineTo(size.width * 0.3772443, size.height * 0.5807755);
    path_42.cubicTo(
        size.width * 0.3739664,
        size.height * 0.5858059,
        size.width * 0.3699702,
        size.height * 0.5900157,
        size.width * 0.3654137,
        size.height * 0.5927873);
    path_42.cubicTo(
        size.width * 0.3582191,
        size.height * 0.5972020,
        size.width * 0.3482275,
        size.height * 0.5992549,
        size.width * 0.3408733,
        size.height * 0.5850873);
    path_42.cubicTo(
        size.width * 0.3280832,
        size.height * 0.5605510,
        size.width * 0.3232870,
        size.height * 0.5040863,
        size.width * 0.3232870,
        size.height * 0.5040863);
    path_42.close();

    Paint paint_42_fill = Paint()..style = PaintingStyle.fill;
    paint_42_fill.color = Colors.white;
    canvas.drawPath(path_42, paint_42_fill);

    Path path_43 = Path();
    path_43.moveTo(size.width * 0.3553382, size.height * 0.5992196);
    path_43.cubicTo(
        size.width * 0.3489427,
        size.height * 0.5992196,
        size.width * 0.3434275,
        size.height * 0.5951127,
        size.width * 0.3392710,
        size.height * 0.5871049);
    path_43.cubicTo(
        size.width * 0.3263206,
        size.height * 0.5622608,
        size.width * 0.3215244,
        size.height * 0.5072324,
        size.width * 0.3213649,
        size.height * 0.5048716);
    path_43.lineTo(size.width * 0.3211252, size.height * 0.5025098);
    path_43.lineTo(size.width * 0.3694870, size.height * 0.4918333);
    path_43.lineTo(size.width * 0.3705260, size.height * 0.5056922);
    path_43.lineTo(size.width * 0.4323168, size.height * 0.4400902);
    path_43.lineTo(size.width * 0.4405504, size.height * 0.4181206);
    path_43.cubicTo(
        size.width * 0.4418298,
        size.height * 0.4146294,
        size.width * 0.4439878,
        size.height * 0.4108314,
        size.width * 0.4459061,
        size.height * 0.4088804);
    path_43.lineTo(size.width * 0.4669298, size.height * 0.3804431);
    path_43.cubicTo(
        size.width * 0.4684481,
        size.height * 0.3783892,
        size.width * 0.4707664,
        size.height * 0.3778765,
        size.width * 0.4727649,
        size.height * 0.3791078);
    path_43.cubicTo(
        size.width * 0.4747634,
        size.height * 0.3803402,
        size.width * 0.4758023,
        size.height * 0.3830098,
        size.width * 0.4755626,
        size.height * 0.3857814);
    path_43.cubicTo(
        size.width * 0.4745237,
        size.height * 0.3953294,
        size.width * 0.4697275,
        size.height * 0.4079569,
        size.width * 0.4633321,
        size.height * 0.4185314);
    path_43.cubicTo(
        size.width * 0.4658107,
        size.height * 0.4230480,
        size.width * 0.4719656,
        size.height * 0.4357784,
        size.width * 0.4690878,
        size.height * 0.4460451);
    path_43.lineTo(size.width * 0.4689282, size.height * 0.4465578);
    path_43.lineTo(size.width * 0.4519015, size.height * 0.4694520);
    path_43.lineTo(size.width * 0.3788397, size.height * 0.5831010);
    path_43.cubicTo(
        size.width * 0.3752420,
        size.height * 0.5886451,
        size.width * 0.3710855,
        size.height * 0.5929569,
        size.width * 0.3662893,
        size.height * 0.5958314);
    path_43.cubicTo(
        size.width * 0.3623725,
        size.height * 0.5979873,
        size.width * 0.3586954,
        size.height * 0.5992196,
        size.width * 0.3553382,
        size.height * 0.5992196);
    path_43.close();
    path_43.moveTo(size.width * 0.3255214, size.height * 0.5067196);
    path_43.cubicTo(
        size.width * 0.3266405,
        size.height * 0.5177039,
        size.width * 0.3316763,
        size.height * 0.5632873,
        size.width * 0.3425481,
        size.height * 0.5843333);
    path_43.cubicTo(
        size.width * 0.3478237,
        size.height * 0.5944971,
        size.width * 0.3552580,
        size.height * 0.5967549,
        size.width * 0.3645305,
        size.height * 0.5911088);
    path_43.cubicTo(
        size.width * 0.3687672,
        size.height * 0.5885422,
        size.width * 0.3725244,
        size.height * 0.5847441,
        size.width * 0.3757221,
        size.height * 0.5797127);
    path_43.lineTo(size.width * 0.4489435, size.height * 0.4658588);
    path_43.lineTo(size.width * 0.4654107, size.height * 0.4436833);
    path_43.cubicTo(
        size.width * 0.4673290,
        size.height * 0.4355735,
        size.width * 0.4616534,
        size.height * 0.4238696,
        size.width * 0.4591756,
        size.height * 0.4199686);
    path_43.lineTo(size.width * 0.4581366, size.height * 0.4183255);
    path_43.lineTo(size.width * 0.4591756, size.height * 0.4166833);
    path_43.cubicTo(
        size.width * 0.4665298,
        size.height * 0.4050824,
        size.width * 0.4706863,
        size.height * 0.3923520,
        size.width * 0.4715656,
        size.height * 0.3848578);
    path_43.cubicTo(
        size.width * 0.4716458,
        size.height * 0.3840363,
        size.width * 0.4712458,
        size.height * 0.3837284,
        size.width * 0.4710061,
        size.height * 0.3835225);
    path_43.cubicTo(
        size.width * 0.4707664,
        size.height * 0.3834206,
        size.width * 0.4702870,
        size.height * 0.3832147,
        size.width * 0.4698069,
        size.height * 0.3838304);
    path_43.lineTo(size.width * 0.4485443, size.height * 0.4125765);
    path_43.lineTo(size.width * 0.4484641, size.height * 0.4126794);
    path_43.cubicTo(
        size.width * 0.4472649,
        size.height * 0.4139108,
        size.width * 0.4452664,
        size.height * 0.4169912,
        size.width * 0.4441473,
        size.height * 0.4201735);
    path_43.lineTo(size.width * 0.4355145, size.height * 0.4431706);
    path_43.lineTo(size.width * 0.3672489, size.height * 0.5157539);
    path_43.lineTo(size.width * 0.3658893, size.height * 0.4977873);
    path_43.lineTo(size.width * 0.3255214, size.height * 0.5067196);
    path_43.close();

    Paint paint_43_fill = Paint()..style = PaintingStyle.fill;
    paint_43_fill.color = Colors.black;
    canvas.drawPath(path_43, paint_43_fill);

    Path path_44 = Path();
    path_44.moveTo(size.width * 0.3672412, size.height * 0.4601794);
    path_44.lineTo(size.width * 0.3632588, size.height * 0.4606216);
    path_44.lineTo(size.width * 0.3658702, size.height * 0.4993873);
    path_44.lineTo(size.width * 0.3698527, size.height * 0.4989451);
    path_44.lineTo(size.width * 0.3672412, size.height * 0.4601794);
    path_44.close();

    Paint paint_44_fill = Paint()..style = PaintingStyle.fill;
    paint_44_fill.color = Colors.black;
    canvas.drawPath(path_44, paint_44_fill);

    Path path_45 = Path();
    path_45.moveTo(size.width * 0.08418931, size.height * 0.8846353);
    path_45.cubicTo(
        size.width * 0.08418931,
        size.height * 0.8840196,
        size.width * 0.08235115,
        size.height * 0.8219078,
        size.width * 0.06500473,
        size.height * 0.7883373);
    path_45.lineTo(size.width * 0.06836214, size.height * 0.7854627);
    path_45.cubicTo(
        size.width * 0.08634809,
        size.height * 0.8202657,
        size.width * 0.08810611,
        size.height * 0.8818637,
        size.width * 0.08818626,
        size.height * 0.8844304);
    path_45.lineTo(size.width * 0.08418931, size.height * 0.8846353);
    path_45.close();

    Paint paint_45_fill = Paint()..style = PaintingStyle.fill;
    paint_45_fill.color = Colors.black;
    canvas.drawPath(path_45, paint_45_fill);

    Path path_46 = Path();
    path_46.moveTo(size.width * 0.1539962, size.height * 0.6322118);
    path_46.cubicTo(
        size.width * 0.1574336,
        size.height * 0.6308775,
        size.width * 0.1617504,
        size.height * 0.6330333,
        size.width * 0.1635885,
        size.height * 0.6370373);
    path_46.cubicTo(
        size.width * 0.1686252,
        size.height * 0.6478167,
        size.width * 0.1736611,
        size.height * 0.6762549,
        size.width * 0.1502397,
        size.height * 0.7453471);
    path_46.cubicTo(
        size.width * 0.1502397,
        size.height * 0.7453471,
        size.width * 0.1304954,
        size.height * 0.8003755,
        size.width * 0.1101115,
        size.height * 0.8063294);
    path_46.cubicTo(
        size.width * 0.1065939,
        size.height * 0.8073559,
        size.width * 0.1023573,
        size.height * 0.8049951,
        size.width * 0.1006786,
        size.height * 0.8009912);
    path_46.cubicTo(
        size.width * 0.09652214,
        size.height * 0.7911353,
        size.width * 0.09276489,
        size.height * 0.7653667,
        size.width * 0.1122695,
        size.height * 0.7029471);
    path_46.cubicTo(
        size.width * 0.1122695,
        size.height * 0.7029471,
        size.width * 0.1310550,
        size.height * 0.6415539,
        size.width * 0.1539962,
        size.height * 0.6322118);
    path_46.close();

    Paint paint_46_fill = Paint()..style = PaintingStyle.fill;
    paint_46_fill.color = Colors.black;
    canvas.drawPath(path_46, paint_46_fill);

    Path path_47 = Path();
    path_47.moveTo(size.width * 0.1083641, size.height * 0.8090020);
    path_47.cubicTo(
        size.width * 0.1045275,
        size.height * 0.8090020,
        size.width * 0.1006901,
        size.height * 0.8062304,
        size.width * 0.09893206,
        size.height * 0.8020216);
    path_47.cubicTo(
        size.width * 0.09469542,
        size.height * 0.7919598,
        size.width * 0.09053817,
        size.height * 0.7654725,
        size.width * 0.1104427,
        size.height * 0.7017186);
    path_47.cubicTo(
        size.width * 0.1112420,
        size.height * 0.6991520,
        size.width * 0.1297870,
        size.height * 0.6390941,
        size.width * 0.1533687,
        size.height * 0.6295461);
    path_47.lineTo(size.width * 0.1540084, size.height * 0.6320098);
    path_47.lineTo(size.width * 0.1533687, size.height * 0.6295461);
    path_47.cubicTo(
        size.width * 0.1576053,
        size.height * 0.6278010,
        size.width * 0.1629611,
        size.height * 0.6304696,
        size.width * 0.1652794,
        size.height * 0.6355010);
    path_47.cubicTo(
        size.width * 0.1705550,
        size.height * 0.6467931,
        size.width * 0.1758305,
        size.height * 0.6760529,
        size.width * 0.1520901,
        size.height * 0.7461725);
    path_47.cubicTo(
        size.width * 0.1512107,
        size.height * 0.7485333,
        size.width * 0.1316260,
        size.height * 0.8024314,
        size.width * 0.1106023,
        size.height * 0.8085912);
    path_47.cubicTo(
        size.width * 0.1098031,
        size.height * 0.8089000,
        size.width * 0.1090840,
        size.height * 0.8090020,
        size.width * 0.1083641,
        size.height * 0.8090020);
    path_47.close();
    path_47.moveTo(size.width * 0.1544878, size.height * 0.6344735);
    path_47.cubicTo(
        size.width * 0.1327450,
        size.height * 0.6433029,
        size.width * 0.1142794,
        size.height * 0.7030529,
        size.width * 0.1140397,
        size.height * 0.7036696);
    path_47.cubicTo(
        size.width * 0.09477481,
        size.height * 0.7654725,
        size.width * 0.09845191,
        size.height * 0.7903176,
        size.width * 0.1023695,
        size.height * 0.7995569);
    path_47.cubicTo(
        size.width * 0.1036481,
        size.height * 0.8025343,
        size.width * 0.1069252,
        size.height * 0.8043824,
        size.width * 0.1095634,
        size.height * 0.8036637);
    path_47.cubicTo(
        size.width * 0.1287481,
        size.height * 0.7980176,
        size.width * 0.1481725,
        size.height * 0.7446324,
        size.width * 0.1483328,
        size.height * 0.7441186);
    path_47.cubicTo(
        size.width * 0.1713542,
        size.height * 0.6761559,
        size.width * 0.1666382,
        size.height * 0.6485392,
        size.width * 0.1617618,
        size.height * 0.6381696);
    path_47.cubicTo(
        size.width * 0.1604031,
        size.height * 0.6351922,
        size.width * 0.1570458,
        size.height * 0.6334471,
        size.width * 0.1544878,
        size.height * 0.6344735);
    path_47.close();

    Paint paint_47_fill = Paint()..style = PaintingStyle.fill;
    paint_47_fill.color = Colors.black;
    canvas.drawPath(path_47, paint_47_fill);

    Path path_48 = Path();
    path_48.moveTo(size.width * 0.07823588, size.height * 0.8840775);
    path_48.lineTo(size.width * 0.07423893, size.height * 0.8838716);
    path_48.lineTo(size.width * 0.07623733, size.height * 0.8839745);
    path_48.lineTo(size.width * 0.07423893, size.height * 0.8838716);
    path_48.cubicTo(
        size.width * 0.07423893,
        size.height * 0.8835637,
        size.width * 0.07487840,
        size.height * 0.8529696,
        size.width * 0.05337550,
        size.height * 0.8121098);
    path_48.lineTo(size.width * 0.05665290, size.height * 0.8092353);
    path_48.cubicTo(
        size.width * 0.07903511,
        size.height * 0.8515324,
        size.width * 0.07831603,
        size.height * 0.8827422,
        size.width * 0.07823588,
        size.height * 0.8840775);
    path_48.close();

    Paint paint_48_fill = Paint()..style = PaintingStyle.fill;
    paint_48_fill.color = Colors.black;
    canvas.drawPath(path_48, paint_48_fill);

    Path path_49 = Path();
    path_49.moveTo(size.width * 0.09349237, size.height * 0.8853510);
    path_49.cubicTo(
        size.width * 0.09341221,
        size.height * 0.8850431,
        size.width * 0.08885573,
        size.height * 0.8586588,
        size.width * 0.08437939,
        size.height * 0.8085588);
    path_49.lineTo(size.width * 0.08837634, size.height * 0.8079431);
    path_49.cubicTo(
        size.width * 0.09285267,
        size.height * 0.8578373,
        size.width * 0.09740916,
        size.height * 0.8839137,
        size.width * 0.09740916,
        size.height * 0.8842216);
    path_49.lineTo(size.width * 0.09349237, size.height * 0.8853510);
    path_49.close();

    Paint paint_49_fill = Paint()..style = PaintingStyle.fill;
    paint_49_fill.color = Colors.black;
    canvas.drawPath(path_49, paint_49_fill);

    Path path_50 = Path();
    path_50.moveTo(size.width * 0.07269863, size.height * 0.6476157);
    path_50.cubicTo(
        size.width * 0.07269863,
        size.height * 0.6476157,
        size.width * 0.06510466,
        size.height * 0.6243108,
        size.width * 0.05503260,
        size.height * 0.6352961);
    path_50.cubicTo(
        size.width * 0.04496061,
        size.height * 0.6462814,
        size.width * 0.03584779,
        size.height * 0.7215333,
        size.width * 0.04999664,
        size.height * 0.7669108);
    path_50.cubicTo(
        size.width * 0.06414542,
        size.height * 0.8122882,
        size.width * 0.07421740,
        size.height * 0.7967863,
        size.width * 0.07421740,
        size.height * 0.7967863);
    path_50.cubicTo(
        size.width * 0.07421740,
        size.height * 0.7967863,
        size.width * 0.08532824,
        size.height * 0.7429902,
        size.width * 0.07269863,
        size.height * 0.6476157);
    path_50.close();

    Paint paint_50_fill = Paint()..style = PaintingStyle.fill;
    paint_50_fill.color = Colors.black;
    canvas.drawPath(path_50, paint_50_fill);

    Path path_51 = Path();
    path_51.moveTo(size.width * 0.06969664, size.height * 0.8017892);
    path_51.cubicTo(
        size.width * 0.06929695,
        size.height * 0.8017892,
        size.width * 0.06881733,
        size.height * 0.8017892,
        size.width * 0.06833771,
        size.height * 0.8016873);
    path_51.cubicTo(
        size.width * 0.06154313,
        size.height * 0.8003520,
        size.width * 0.05482840,
        size.height * 0.7889569,
        size.width * 0.04819366,
        size.height * 0.7678078);
    path_51.cubicTo(
        size.width * 0.03364519,
        size.height * 0.7210961,
        size.width * 0.04291786,
        size.height * 0.6451245,
        size.width * 0.05378924,
        size.height * 0.6333186);
    path_51.cubicTo(
        size.width * 0.05658702,
        size.height * 0.6302382,
        size.width * 0.05962466,
        size.height * 0.6291088,
        size.width * 0.06258229,
        size.height * 0.6300333);
    path_51.cubicTo(
        size.width * 0.06985649,
        size.height * 0.6322922,
        size.width * 0.07441290,
        size.height * 0.6459461,
        size.width * 0.07457275,
        size.height * 0.6465618);
    path_51.lineTo(size.width * 0.07465275, size.height * 0.6468696);
    path_51.lineTo(size.width * 0.07465275, size.height * 0.6471775);
    path_51.cubicTo(
        size.width * 0.08720305,
        size.height * 0.7419363,
        size.width * 0.07625145,
        size.height * 0.7968618,
        size.width * 0.07609160,
        size.height * 0.7973745);
    path_51.lineTo(size.width * 0.07601168, size.height * 0.7978882);
    path_51.lineTo(size.width * 0.07569191, size.height * 0.7982990);
    path_51.cubicTo(
        size.width * 0.07569191,
        size.height * 0.7984020,
        size.width * 0.07345366,
        size.height * 0.8017892,
        size.width * 0.06969664,
        size.height * 0.8017892);
    path_51.close();
    path_51.moveTo(size.width * 0.06058389, size.height * 0.6348578);
    path_51.cubicTo(
        size.width * 0.05922496,
        size.height * 0.6348578,
        size.width * 0.05786603,
        size.height * 0.6356794,
        size.width * 0.05642718,
        size.height * 0.6372196);
    path_51.cubicTo(
        size.width * 0.05243031,
        size.height * 0.6415314,
        size.width * 0.04739435,
        size.height * 0.6605245,
        size.width * 0.04547588,
        size.height * 0.6884490);
    path_51.cubicTo(
        size.width * 0.04347740,
        size.height * 0.7181186,
        size.width * 0.04579557,
        size.height * 0.7463510,
        size.width * 0.05187076,
        size.height * 0.7659598);
    path_51.cubicTo(
        size.width * 0.05970458,
        size.height * 0.7911127,
        size.width * 0.06585969,
        size.height * 0.7960402,
        size.width * 0.06881733,
        size.height * 0.7966559);
    path_51.cubicTo(
        size.width * 0.07065588,
        size.height * 0.7970667,
        size.width * 0.07185496,
        size.height * 0.7960402,
        size.width * 0.07241450,
        size.height * 0.7954245);
    path_51.cubicTo(
        size.width * 0.07361359,
        size.height * 0.7889569,
        size.width * 0.08240687,
        size.height * 0.7361873,
        size.width * 0.07081580,
        size.height * 0.6483069);
    path_51.cubicTo(
        size.width * 0.06961672,
        size.height * 0.6448167,
        size.width * 0.06593962,
        size.height * 0.6363980,
        size.width * 0.06170298,
        size.height * 0.6350637);
    path_51.cubicTo(
        size.width * 0.06130328,
        size.height * 0.6348578,
        size.width * 0.06090359,
        size.height * 0.6348578,
        size.width * 0.06058389,
        size.height * 0.6348578);
    path_51.close();

    Paint paint_51_fill = Paint()..style = PaintingStyle.fill;
    paint_51_fill.color = Colors.black;
    canvas.drawPath(path_51, paint_51_fill);

    Path path_52 = Path();
    path_52.moveTo(size.width * 0.002504679, size.height * 0.7133255);
    path_52.cubicTo(
        size.width * 0.003304046,
        size.height * 0.7088088,
        size.width * 0.006581458,
        size.height * 0.7057284,
        size.width * 0.01017863,
        size.height * 0.7061392);
    path_52.cubicTo(
        size.width * 0.01721305,
        size.height * 0.7070627,
        size.width * 0.03160160,
        size.height * 0.7163029,
        size.width * 0.05662176,
        size.height * 0.7601402);
    path_52.cubicTo(
        size.width * 0.05662176,
        size.height * 0.7601402,
        size.width * 0.07492733,
        size.height * 0.7917608,
        size.width * 0.06597443,
        size.height * 0.8170157);
    path_52.cubicTo(
        size.width * 0.06365626,
        size.height * 0.8234843,
        size.width * 0.05854031,
        size.height * 0.8275902,
        size.width * 0.05302466,
        size.height * 0.8275902);
    path_52.cubicTo(
        size.width * 0.04287267,
        size.height * 0.8274873,
        size.width * 0.02544649,
        size.height * 0.8214304,
        size.width * 0.01425534,
        size.height * 0.7847794);
    path_52.cubicTo(
        size.width * 0.01425534,
        size.height * 0.7847794,
        size.width * -0.001172405,
        size.height * 0.7337559,
        size.width * 0.002504679,
        size.height * 0.7133255);
    path_52.close();

    Paint paint_52_fill = Paint()..style = PaintingStyle.fill;
    paint_52_fill.color = _activeColor;
    canvas.drawPath(path_52, paint_52_fill);

    Path path_53 = Path();
    path_53.moveTo(size.width * 0.05306733, size.height * 0.8304912);
    path_53.lineTo(size.width * 0.05298740, size.height * 0.8304912);
    path_53.cubicTo(
        size.width * 0.04283542,
        size.height * 0.8303882,
        size.width * 0.02413031,
        size.height * 0.8245373,
        size.width * 0.01237962,
        size.height * 0.7860382);
    path_53.cubicTo(
        size.width * 0.01174008,
        size.height * 0.7838824,
        size.width * -0.003208069,
        size.height * 0.7339873,
        size.width * 0.0006288885,
        size.height * 0.7130441);
    path_53.cubicTo(
        size.width * 0.001668061,
        size.height * 0.7071922,
        size.width * 0.005904710,
        size.height * 0.7032912,
        size.width * 0.01046107,
        size.height * 0.7039069);
    path_53.cubicTo(
        size.width * 0.01861466,
        size.height * 0.7050363,
        size.width * 0.03348282,
        size.height * 0.7155078,
        size.width * 0.05826321,
        size.height * 0.7589343);
    path_53.cubicTo(
        size.width * 0.05906260,
        size.height * 0.7602696,
        size.width * 0.07712824,
        size.height * 0.7920951,
        size.width * 0.06785557,
        size.height * 0.8184794);
    path_53.cubicTo(
        size.width * 0.06513779,
        size.height * 0.8257686,
        size.width * 0.05938229,
        size.height * 0.8304912,
        size.width * 0.05306733,
        size.height * 0.8304912);
    path_53.close();
    path_53.moveTo(size.width * 0.009421908, size.height * 0.7089373);
    path_53.cubicTo(
        size.width * 0.007103756,
        size.height * 0.7089373,
        size.width * 0.005025405,
        size.height * 0.7110931,
        size.width * 0.004465847,
        size.height * 0.7141735);
    path_53.cubicTo(
        size.width * 0.0009486336,
        size.height * 0.7335765,
        size.width * 0.01589679,
        size.height * 0.7835735,
        size.width * 0.01605664,
        size.height * 0.7840873);
    path_53.cubicTo(
        size.width * 0.02700802,
        size.height * 0.8198137,
        size.width * 0.04387458,
        size.height * 0.8252559,
        size.width * 0.05298740,
        size.height * 0.8252559);
    path_53.lineTo(size.width * 0.05306733, size.height * 0.8252559);
    path_53.cubicTo(
        size.width * 0.05786351,
        size.height * 0.8252559,
        size.width * 0.06218008,
        size.height * 0.8216618,
        size.width * 0.06417855,
        size.height * 0.8162206);
    path_53.cubicTo(
        size.width * 0.07257183,
        size.height * 0.7926088,
        size.width * 0.05522565,
        size.height * 0.7622196,
        size.width * 0.05506573,
        size.height * 0.7619118);
    path_53.cubicTo(
        size.width * 0.02988573,
        size.height * 0.7177667,
        size.width * 0.01605664,
        size.height * 0.7097588,
        size.width * 0.01006145,
        size.height * 0.7089373);
    path_53.cubicTo(
        size.width * 0.009741679,
        size.height * 0.7089373,
        size.width * 0.009581756,
        size.height * 0.7089373,
        size.width * 0.009421908,
        size.height * 0.7089373);
    path_53.close();

    Paint paint_53_fill = Paint()..style = PaintingStyle.fill;
    paint_53_fill.color = Colors.black;
    canvas.drawPath(path_53, paint_53_fill);

    Path path_54 = Path();
    path_54.moveTo(size.width * 0.08188168, size.height * 0.5726147);
    path_54.cubicTo(
        size.width * 0.08483969,
        size.height * 0.5686108,
        size.width * 0.08979542,
        size.height * 0.5685078,
        size.width * 0.09283359,
        size.height * 0.5724098);
    path_54.cubicTo(
        size.width * 0.1011466,
        size.height * 0.5828814,
        size.width * 0.1148160,
        size.height * 0.6158363,
        size.width * 0.1130573,
        size.height * 0.7193216);
    path_54.cubicTo(
        size.width * 0.1130573,
        size.height * 0.7193216,
        size.width * 0.1104992,
        size.height * 0.8024784,
        size.width * 0.09227405,
        size.height * 0.8234225);
    path_54.cubicTo(
        size.width * 0.08907634,
        size.height * 0.8271176,
        size.width * 0.08428015,
        size.height * 0.8268098,
        size.width * 0.08132214,
        size.height * 0.8228059);
    path_54.cubicTo(
        size.width * 0.07420802,
        size.height * 0.8129500,
        size.width * 0.06253725,
        size.height * 0.7826647,
        size.width * 0.06253725,
        size.height * 0.6900618);
    path_54.cubicTo(
        size.width * 0.06261718,
        size.height * 0.6900618,
        size.width * 0.06213763,
        size.height * 0.5995127,
        size.width * 0.08188168,
        size.height * 0.5726147);
    path_54.close();

    Paint paint_54_fill = Paint()..style = PaintingStyle.fill;
    paint_54_fill.color = _activeColor;
    canvas.drawPath(path_54, paint_54_fill);

    Path path_55 = Path();
    path_55.moveTo(size.width * 0.08707863, size.height * 0.8288196);
    path_55.cubicTo(
        size.width * 0.08444046,
        size.height * 0.8288196,
        size.width * 0.08180305,
        size.height * 0.8274853,
        size.width * 0.07988397,
        size.height * 0.8248157);
    path_55.cubicTo(
        size.width * 0.07268992,
        size.height * 0.8148569,
        size.width * 0.06053954,
        size.height * 0.7841608,
        size.width * 0.06053954,
        size.height * 0.6903265);
    path_55.cubicTo(
        size.width * 0.06053954,
        size.height * 0.6866304,
        size.width * 0.06029977,
        size.height * 0.5984422,
        size.width * 0.08036412,
        size.height * 0.5710314);
    path_55.cubicTo(
        size.width * 0.08212214,
        size.height * 0.5685667,
        size.width * 0.08452061,
        size.height * 0.5672324,
        size.width * 0.08715878,
        size.height * 0.5671294);
    path_55.cubicTo(
        size.width * 0.08979618,
        size.height * 0.5670275,
        size.width * 0.09227405,
        size.height * 0.5683618,
        size.width * 0.09419313,
        size.height * 0.5707235);
    path_55.cubicTo(
        size.width * 0.1027458,
        size.height * 0.5815029,
        size.width * 0.1168153,
        size.height * 0.6151765,
        size.width * 0.1149763,
        size.height * 0.7194824);
    path_55.cubicTo(
        size.width * 0.1148962,
        size.height * 0.7229735,
        size.width * 0.1121786,
        size.height * 0.8039755,
        size.width * 0.09355344,
        size.height * 0.8254314);
    path_55.cubicTo(
        size.width * 0.09179466,
        size.height * 0.8276902,
        size.width * 0.08947634,
        size.height * 0.8288196,
        size.width * 0.08707863,
        size.height * 0.8288196);
    path_55.close();
    path_55.moveTo(size.width * 0.08747786, size.height * 0.5723657);
    path_55.lineTo(size.width * 0.08739847, size.height * 0.5723657);
    path_55.cubicTo(
        size.width * 0.08587939,
        size.height * 0.5723657,
        size.width * 0.08444046,
        size.height * 0.5731873,
        size.width * 0.08340153,
        size.height * 0.5746245);
    path_55.cubicTo(
        size.width * 0.06445641,
        size.height * 0.6004951,
        size.width * 0.06461634,
        size.height * 0.6895049,
        size.width * 0.06461634,
        size.height * 0.6903265);
    path_55.cubicTo(
        size.width * 0.06461634,
        size.height * 0.7823127,
        size.width * 0.07604725,
        size.height * 0.8118804,
        size.width * 0.08284198,
        size.height * 0.8213255);
    path_55.cubicTo(
        size.width * 0.08500000,
        size.height * 0.8243020,
        size.width * 0.08859695,
        size.height * 0.8245078,
        size.width * 0.09099542,
        size.height * 0.8217363);
    path_55.cubicTo(
        size.width * 0.1084214,
        size.height * 0.8016137,
        size.width * 0.1110595,
        size.height * 0.7202010,
        size.width * 0.1111397,
        size.height * 0.7193804);
    path_55.cubicTo(
        size.width * 0.1128977,
        size.height * 0.6169216,
        size.width * 0.09962824,
        size.height * 0.5845824,
        size.width * 0.09155496,
        size.height * 0.5744186);
    path_55.cubicTo(
        size.width * 0.09035573,
        size.height * 0.5730843,
        size.width * 0.08899695,
        size.height * 0.5723657,
        size.width * 0.08747786,
        size.height * 0.5723657);
    path_55.close();

    Paint paint_55_fill = Paint()..style = PaintingStyle.fill;
    paint_55_fill.color = Colors.black;
    canvas.drawPath(path_55, paint_55_fill);

    Path path_56 = Path();
    path_56.moveTo(size.width * 0.05964031, size.height * 0.8254667);
    path_56.cubicTo(
        size.width * 0.04085519,
        size.height * 0.7670510,
        size.width * 0.004164298,
        size.height * 0.7101745,
        size.width * 0.003764611,
        size.height * 0.7096618);
    path_56.lineTo(size.width * 0.005603153, size.height * 0.7077108);
    path_56.cubicTo(
        size.width * 0.006002840,
        size.height * 0.7083275,
        size.width * 0.04293359,
        size.height * 0.7655108,
        size.width * 0.06179863,
        size.height * 0.8243373);
    path_56.lineTo(size.width * 0.05964031, size.height * 0.8254667);
    path_56.close();

    Paint paint_56_fill = Paint()..style = PaintingStyle.fill;
    paint_56_fill.color = Colors.black;
    canvas.drawPath(path_56, paint_56_fill);

    Path path_57 = Path();
    path_57.moveTo(size.width * 0.05654137, size.height * 0.8148784);
    path_57.cubicTo(
        size.width * 0.04351168,
        size.height * 0.8092314,
        size.width * 0.02176893,
        size.height * 0.8051245,
        size.width * 0.02152908,
        size.height * 0.8051245);
    path_57.lineTo(size.width * 0.02184885, size.height * 0.8020451);
    path_57.cubicTo(
        size.width * 0.02208863,
        size.height * 0.8020451,
        size.width * 0.04399130,
        size.height * 0.8061520,
        size.width * 0.05726076,
        size.height * 0.8119010);
    path_57.lineTo(size.width * 0.05654137, size.height * 0.8148784);
    path_57.close();

    Paint paint_57_fill = Paint()..style = PaintingStyle.fill;
    paint_57_fill.color = Colors.black;
    canvas.drawPath(path_57, paint_57_fill);

    Path path_58 = Path();
    path_58.moveTo(size.width * 0.05186153, size.height * 0.8030667);
    path_58.cubicTo(
        size.width * 0.04434748,
        size.height * 0.7984471,
        size.width * 0.01628969,
        size.height * 0.7928000,
        size.width * 0.01604992,
        size.height * 0.7926980);
    path_58.lineTo(size.width * 0.01644962, size.height * 0.7896176);
    path_58.cubicTo(
        size.width * 0.01764863,
        size.height * 0.7898235,
        size.width * 0.04514687,
        size.height * 0.7953667,
        size.width * 0.05298061,
        size.height * 0.8002951);
    path_58.lineTo(size.width * 0.05186153, size.height * 0.8030667);
    path_58.close();

    Paint paint_58_fill = Paint()..style = PaintingStyle.fill;
    paint_58_fill.color = Colors.black;
    canvas.drawPath(path_58, paint_58_fill);

    Path path_59 = Path();
    path_59.moveTo(size.width * 0.02953450, size.height * 0.7548059);
    path_59.cubicTo(
        size.width * 0.02353924,
        size.height * 0.7498784,
        size.width * 0.003475137,
        size.height * 0.7415618,
        size.width * 0.003235328,
        size.height * 0.7414598);
    path_59.lineTo(size.width * 0.003954763, size.height * 0.7384824);
    path_59.cubicTo(
        size.width * 0.004754130,
        size.height * 0.7387902,
        size.width * 0.02449847,
        size.height * 0.7470029,
        size.width * 0.03081344,
        size.height * 0.7521363);
    path_59.lineTo(size.width * 0.02953450, size.height * 0.7548059);
    path_59.close();

    Paint paint_59_fill = Paint()..style = PaintingStyle.fill;
    paint_59_fill.color = Colors.black;
    canvas.drawPath(path_59, paint_59_fill);

    Path path_60 = Path();
    path_60.moveTo(size.width * 0.02167924, size.height * 0.7397078);
    path_60.cubicTo(
        size.width * 0.01528427,
        size.height * 0.7353961,
        size.width * 0.002254634,
        size.height * 0.7312892,
        size.width * 0.002094763,
        size.height * 0.7311863);
    path_60.lineTo(size.width * 0.002654321, size.height * 0.7282088);
    path_60.cubicTo(
        size.width * 0.003213878,
        size.height * 0.7284147,
        size.width * 0.01616359,
        size.height * 0.7324186,
        size.width * 0.02279840,
        size.height * 0.7369353);
    path_60.lineTo(size.width * 0.02167924, size.height * 0.7397078);
    path_60.close();

    Paint paint_60_fill = Paint()..style = PaintingStyle.fill;
    paint_60_fill.color = Colors.black;
    canvas.drawPath(path_60, paint_60_fill);

    Path path_61 = Path();
    path_61.moveTo(size.width * 0.03921878, size.height * 0.7695549);
    path_61.lineTo(size.width * 0.03690061, size.height * 0.7687333);
    path_61.cubicTo(
        size.width * 0.03905893,
        size.height * 0.7583647,
        size.width * 0.03889908,
        size.height * 0.7343412,
        size.width * 0.03889908,
        size.height * 0.7341353);
    path_61.lineTo(size.width * 0.04129718, size.height * 0.7341353);
    path_61.cubicTo(
        size.width * 0.04129718,
        size.height * 0.7351627,
        size.width * 0.04145702,
        size.height * 0.7587745,
        size.width * 0.03921878,
        size.height * 0.7695549);
    path_61.close();

    Paint paint_61_fill = Paint()..style = PaintingStyle.fill;
    paint_61_fill.color = Colors.black;
    canvas.drawPath(path_61, paint_61_fill);

    Path path_62 = Path();
    path_62.moveTo(size.width * 0.04446687, size.height * 0.7804255);
    path_62.lineTo(size.width * 0.04214870, size.height * 0.7796039);
    path_62.cubicTo(
        size.width * 0.04486656,
        size.height * 0.7666686,
        size.width * 0.04502641,
        size.height * 0.7431588,
        size.width * 0.04502641,
        size.height * 0.7429529);
    path_62.lineTo(size.width * 0.04742458, size.height * 0.7429529);
    path_62.cubicTo(
        size.width * 0.04742458,
        size.height * 0.7439804,
        size.width * 0.04726466,
        size.height * 0.7671824,
        size.width * 0.04446687,
        size.height * 0.7804255);
    path_62.close();

    Paint paint_62_fill = Paint()..style = PaintingStyle.fill;
    paint_62_fill.color = Colors.black;
    canvas.drawPath(path_62, paint_62_fill);

    Path path_63 = Path();
    path_63.moveTo(size.width * 0.1056565, size.height * 0.8856118);
    path_63.cubicTo(
        size.width * 0.1054969,
        size.height * 0.8847902,
        size.width * 0.1010198,
        size.height * 0.8647706,
        size.width * 0.1138901,
        size.height * 0.8308922);
    path_63.lineTo(size.width * 0.1174870, size.height * 0.8331510);
    path_63.cubicTo(
        size.width * 0.1053366,
        size.height * 0.8650794,
        size.width * 0.1094137,
        size.height * 0.8839686,
        size.width * 0.1094931,
        size.height * 0.8841745);
    path_63.lineTo(size.width * 0.1056565, size.height * 0.8856118);
    path_63.close();

    Paint paint_63_fill = Paint()..style = PaintingStyle.fill;
    paint_63_fill.color = Colors.black;
    canvas.drawPath(path_63, paint_63_fill);

    Path path_64 = Path();
    path_64.moveTo(size.width * 0.1510053, size.height * 0.7465804);
    path_64.cubicTo(
        size.width * 0.1499664,
        size.height * 0.7429873,
        size.width * 0.1471687,
        size.height * 0.7409333,
        size.width * 0.1442908,
        size.height * 0.7417549);
    path_64.cubicTo(
        size.width * 0.1386153,
        size.height * 0.7433971,
        size.width * 0.1277443,
        size.height * 0.7526373,
        size.width * 0.1107176,
        size.height * 0.7911363);
    path_64.cubicTo(
        size.width * 0.1107176,
        size.height * 0.7911363,
        size.width * 0.09824733,
        size.height * 0.8190608,
        size.width * 0.1074405,
        size.height * 0.8383618);
    path_64.cubicTo(
        size.width * 0.1097580,
        size.height * 0.8432892,
        size.width * 0.1142344,
        size.height * 0.8460608,
        size.width * 0.1187115,
        size.height * 0.8453422);
    path_64.cubicTo(
        size.width * 0.1269443,
        size.height * 0.8440078,
        size.width * 0.1405336,
        size.height * 0.8369245,
        size.width * 0.1469290,
        size.height * 0.8058167);
    path_64.cubicTo(
        size.width * 0.1469290,
        size.height * 0.8059196,
        size.width * 0.1555618,
        size.height * 0.7626980,
        size.width * 0.1510053,
        size.height * 0.7465804);
    path_64.close();

    Paint paint_64_fill = Paint()..style = PaintingStyle.fill;
    paint_64_fill.color = _activeColor;
    canvas.drawPath(path_64, paint_64_fill);

    Path path_65 = Path();
    path_65.moveTo(size.width * 0.1172305, size.height * 0.8479137);
    path_65.cubicTo(
        size.width * 0.1125145,
        size.height * 0.8479137,
        size.width * 0.1081176,
        size.height * 0.8448343,
        size.width * 0.1056397,
        size.height * 0.8395980);
    path_65.cubicTo(
        size.width * 0.09604733,
        size.height * 0.8191676,
        size.width * 0.1083580,
        size.height * 0.7909353,
        size.width * 0.1089176,
        size.height * 0.7898059);
    path_65.cubicTo(
        size.width * 0.1257840,
        size.height * 0.7514098,
        size.width * 0.1372145,
        size.height * 0.7410412,
        size.width * 0.1437695,
        size.height * 0.7391931);
    path_65.cubicTo(
        size.width * 0.1476069,
        size.height * 0.7380637,
        size.width * 0.1514435,
        size.height * 0.7408353,
        size.width * 0.1528023,
        size.height * 0.7456608);
    path_65.cubicTo(
        size.width * 0.1575183,
        size.height * 0.7622922,
        size.width * 0.1491252,
        size.height * 0.8046922,
        size.width * 0.1487260,
        size.height * 0.8064373);
    path_65.cubicTo(
        size.width * 0.1420107,
        size.height * 0.8393931,
        size.width * 0.1271427,
        size.height * 0.8464765,
        size.width * 0.1188290,
        size.height * 0.8478108);
    path_65.cubicTo(
        size.width * 0.1183496,
        size.height * 0.8478108,
        size.width * 0.1177901,
        size.height * 0.8479137,
        size.width * 0.1172305,
        size.height * 0.8479137);
    path_65.close();
    path_65.moveTo(size.width * 0.1454481, size.height * 0.7440186);
    path_65.cubicTo(
        size.width * 0.1452084,
        size.height * 0.7440186,
        size.width * 0.1448885,
        size.height * 0.7440186,
        size.width * 0.1446489,
        size.height * 0.7441206);
    path_65.cubicTo(
        size.width * 0.1399328,
        size.height * 0.7455578,
        size.width * 0.1293809,
        size.height * 0.7535657,
        size.width * 0.1123542,
        size.height * 0.7922706);
    path_65.cubicTo(
        size.width * 0.1121947,
        size.height * 0.7925784,
        size.width * 0.1006038,
        size.height * 0.8190657,
        size.width * 0.1089969,
        size.height * 0.8369284);
    path_65.cubicTo(
        size.width * 0.1109153,
        size.height * 0.8410353,
        size.width * 0.1145924,
        size.height * 0.8432941,
        size.width * 0.1183496,
        size.height * 0.8426784);
    path_65.cubicTo(
        size.width * 0.1256237,
        size.height * 0.8415490,
        size.width * 0.1387336,
        size.height * 0.8350804,
        size.width * 0.1448084,
        size.height * 0.8051029);
    path_65.cubicTo(
        size.width * 0.1448885,
        size.height * 0.8046922,
        size.width * 0.1532817,
        size.height * 0.7623951,
        size.width * 0.1489656,
        size.height * 0.7473029);
    path_65.cubicTo(
        size.width * 0.1484855,
        size.height * 0.7452500,
        size.width * 0.1470473,
        size.height * 0.7440186,
        size.width * 0.1454481,
        size.height * 0.7440186);
    path_65.close();

    Paint paint_65_fill = Paint()..style = PaintingStyle.fill;
    paint_65_fill.color = Colors.black;
    canvas.drawPath(path_65, paint_65_fill);

    Path path_66 = Path();
    path_66.moveTo(size.width * 0.1133634, size.height * 0.8443451);
    path_66.lineTo(size.width * 0.1110450, size.height * 0.8435235);
    path_66.cubicTo(
        size.width * 0.1219160,
        size.height * 0.7935265,
        size.width * 0.1475756,
        size.height * 0.7426049,
        size.width * 0.1478160,
        size.height * 0.7420922);
    path_66.lineTo(size.width * 0.1498137, size.height * 0.7437343);
    path_66.cubicTo(
        size.width * 0.1495740,
        size.height * 0.7442480,
        size.width * 0.1241542,
        size.height * 0.7948608,
        size.width * 0.1133634,
        size.height * 0.8443451);
    path_66.close();

    Paint paint_66_fill = Paint()..style = PaintingStyle.fill;
    paint_66_fill.color = Colors.black;
    canvas.drawPath(path_66, paint_66_fill);

    Path path_67 = Path();
    path_67.moveTo(size.width * 0.1149687, size.height * 0.8358804);
    path_67.lineTo(size.width * 0.1139298, size.height * 0.8331078);
    path_67.cubicTo(
        size.width * 0.1242420,
        size.height * 0.8268461,
        size.width * 0.1411885,
        size.height * 0.8209941,
        size.width * 0.1419076,
        size.height * 0.8206863);
    path_67.lineTo(size.width * 0.1425473, size.height * 0.8236627);
    path_67.cubicTo(
        size.width * 0.1423870,
        size.height * 0.8236627,
        size.width * 0.1251206,
        size.height * 0.8296176,
        size.width * 0.1149687,
        size.height * 0.8358804);
    path_67.close();

    Paint paint_67_fill = Paint()..style = PaintingStyle.fill;
    paint_67_fill.color = Colors.black;
    canvas.drawPath(path_67, paint_67_fill);

    Path path_68 = Path();
    path_68.moveTo(size.width * 0.1177359, size.height * 0.8255843);
    path_68.lineTo(size.width * 0.1164573, size.height * 0.8230176);
    path_68.cubicTo(
        size.width * 0.1224519,
        size.height * 0.8180902,
        size.width * 0.1443550,
        size.height * 0.8101843,
        size.width * 0.1452344,
        size.height * 0.8098765);
    path_68.lineTo(size.width * 0.1458733, size.height * 0.8128539);
    path_68.cubicTo(
        size.width * 0.1457137,
        size.height * 0.8128539,
        size.width * 0.1234916,
        size.height * 0.8208618,
        size.width * 0.1177359,
        size.height * 0.8255843);
    path_68.close();

    Paint paint_68_fill = Paint()..style = PaintingStyle.fill;
    paint_68_fill.color = Colors.black;
    canvas.drawPath(path_68, paint_68_fill);

    Path path_69 = Path();
    path_69.moveTo(size.width * 0.1323183, size.height * 0.7834598);
    path_69.lineTo(size.width * 0.1308000, size.height * 0.7810990);
    path_69.cubicTo(
        size.width * 0.1355160,
        size.height * 0.7761716,
        size.width * 0.1508641,
        size.height * 0.7670343,
        size.width * 0.1515832,
        size.height * 0.7667265);
    path_69.lineTo(size.width * 0.1525427, size.height * 0.7694980);
    path_69.cubicTo(
        size.width * 0.1523824,
        size.height * 0.7696010,
        size.width * 0.1367947,
        size.height * 0.7787373,
        size.width * 0.1323183,
        size.height * 0.7834598);
    path_69.close();

    Paint paint_69_fill = Paint()..style = PaintingStyle.fill;
    paint_69_fill.color = Colors.black;
    canvas.drawPath(path_69, paint_69_fill);

    Path path_70 = Path();
    path_70.moveTo(size.width * 0.1375710, size.height * 0.7701343);
    path_70.lineTo(size.width * 0.1362122, size.height * 0.7675686);
    path_70.cubicTo(
        size.width * 0.1412481,
        size.height * 0.7630510,
        size.width * 0.1514794,
        size.height * 0.7581235,
        size.width * 0.1518794,
        size.height * 0.7579176);
    path_70.lineTo(size.width * 0.1526786, size.height * 0.7607922);
    path_70.cubicTo(
        size.width * 0.1525985,
        size.height * 0.7609980,
        size.width * 0.1423672,
        size.height * 0.7659255,
        size.width * 0.1375710,
        size.height * 0.7701343);
    path_70.close();

    Paint paint_70_fill = Paint()..style = PaintingStyle.fill;
    paint_70_fill.color = Colors.black;
    canvas.drawPath(path_70, paint_70_fill);

    Path path_71 = Path();
    path_71.moveTo(size.width * 0.1252389, size.height * 0.7967804);
    path_71.cubicTo(
        size.width * 0.1226008,
        size.height * 0.7882598,
        size.width * 0.1210023,
        size.height * 0.7690618,
        size.width * 0.1209221,
        size.height * 0.7682402);
    path_71.lineTo(size.width * 0.1233206, size.height * 0.7679324);
    path_71.cubicTo(
        size.width * 0.1233206,
        size.height * 0.7681373,
        size.width * 0.1249992,
        size.height * 0.7875412,
        size.width * 0.1274771,
        size.height * 0.7956520);
    path_71.lineTo(size.width * 0.1252389, size.height * 0.7967804);
    path_71.close();

    Paint paint_71_fill = Paint()..style = PaintingStyle.fill;
    paint_71_fill.color = Colors.black;
    canvas.drawPath(path_71, paint_71_fill);

    Path path_72 = Path();
    path_72.moveTo(size.width * 0.1218618, size.height * 0.8060735);
    path_72.cubicTo(
        size.width * 0.1185840,
        size.height * 0.7956020,
        size.width * 0.1167458,
        size.height * 0.7768137,
        size.width * 0.1166656,
        size.height * 0.7759931);
    path_72.lineTo(size.width * 0.1190641, size.height * 0.7755824);
    path_72.cubicTo(
        size.width * 0.1190641,
        size.height * 0.7757873,
        size.width * 0.1209824,
        size.height * 0.7947804,
        size.width * 0.1241000,
        size.height * 0.8048412);
    path_72.lineTo(size.width * 0.1218618, size.height * 0.8060735);
    path_72.close();

    Paint paint_72_fill = Paint()..style = PaintingStyle.fill;
    paint_72_fill.color = Colors.black;
    canvas.drawPath(path_72, paint_72_fill);

    Path path_73 = Path();
    path_73.moveTo(size.width * 0.08703282, size.height * 0.8280333);
    path_73.cubicTo(
        size.width * 0.08535420,
        size.height * 0.7966176,
        size.width * 0.08671298,
        size.height * 0.5720922,
        size.width * 0.08679313,
        size.height * 0.5698333);
    path_73.lineTo(size.width * 0.08919160, size.height * 0.5698333);
    path_73.cubicTo(
        size.width * 0.08919160,
        size.height * 0.5720922,
        size.width * 0.08783282,
        size.height * 0.7965147,
        size.width * 0.08943130,
        size.height * 0.8277245);
    path_73.lineTo(size.width * 0.08703282, size.height * 0.8280333);
    path_73.close();

    Paint paint_73_fill = Paint()..style = PaintingStyle.fill;
    paint_73_fill.color = Colors.black;
    canvas.drawPath(path_73, paint_73_fill);

    Path path_74 = Path();
    path_74.moveTo(size.width * 0.08693664, size.height * 0.6557961);
    path_74.cubicTo(
        size.width * 0.08070153,
        size.height * 0.6423471,
        size.width * 0.06575359,
        size.height * 0.6301294,
        size.width * 0.06559366,
        size.height * 0.6300275);
    path_74.lineTo(size.width * 0.06687267, size.height * 0.6274608);
    path_74.cubicTo(
        size.width * 0.06751214,
        size.height * 0.6279735,
        size.width * 0.08238015,
        size.height * 0.6401912,
        size.width * 0.08893511,
        size.height * 0.6542559);
    path_74.lineTo(size.width * 0.08693664, size.height * 0.6557961);
    path_74.close();

    Paint paint_74_fill = Paint()..style = PaintingStyle.fill;
    paint_74_fill.color = Colors.black;
    canvas.drawPath(path_74, paint_74_fill);

    Path path_75 = Path();
    path_75.moveTo(size.width * 0.08661145, size.height * 0.6685951);
    path_75.cubicTo(
        size.width * 0.08037634,
        size.height * 0.6572000,
        size.width * 0.06438939,
        size.height * 0.6445716,
        size.width * 0.06422947,
        size.height * 0.6443667);
    path_75.lineTo(size.width * 0.06550847, size.height * 0.6416971);
    path_75.cubicTo(
        size.width * 0.06614794,
        size.height * 0.6422108,
        size.width * 0.08213511,
        size.height * 0.6548382,
        size.width * 0.08860992,
        size.height * 0.6667471);
    path_75.lineTo(size.width * 0.08661145, size.height * 0.6685951);
    path_75.close();

    Paint paint_75_fill = Paint()..style = PaintingStyle.fill;
    paint_75_fill.color = Colors.black;
    canvas.drawPath(path_75, paint_75_fill);

    Path path_76 = Path();
    path_76.moveTo(size.width * 0.08831374, size.height * 0.6816676);
    path_76.lineTo(size.width * 0.08679466, size.height * 0.6793069);
    path_76.cubicTo(
        size.width * 0.08727405,
        size.height * 0.6787931,
        size.width * 0.09862519,
        size.height * 0.6665765,
        size.width * 0.1121344,
        size.height * 0.6619559);
    path_76.lineTo(size.width * 0.1127740, size.height * 0.6649333);
    path_76.cubicTo(
        size.width * 0.09974427,
        size.height * 0.6693480,
        size.width * 0.08839389,
        size.height * 0.6815647,
        size.width * 0.08831374,
        size.height * 0.6816676);
    path_76.close();

    Paint paint_76_fill = Paint()..style = PaintingStyle.fill;
    paint_76_fill.color = Colors.black;
    canvas.drawPath(path_76, paint_76_fill);

    Path path_77 = Path();
    path_77.moveTo(size.width * 0.08836947, size.height * 0.6991265);
    path_77.lineTo(size.width * 0.08669008, size.height * 0.6968676);
    path_77.cubicTo(
        size.width * 0.08717023,
        size.height * 0.6962520,
        size.width * 0.09828092,
        size.height * 0.6825971,
        size.width * 0.1126695,
        size.height * 0.6766431);
    path_77.lineTo(size.width * 0.1133893, size.height * 0.6796206);
    path_77.cubicTo(
        size.width * 0.09956031,
        size.height * 0.6853696,
        size.width * 0.08844885,
        size.height * 0.6989206,
        size.width * 0.08836947,
        size.height * 0.6991265);
    path_77.close();

    Paint paint_77_fill = Paint()..style = PaintingStyle.fill;
    paint_77_fill.color = Colors.black;
    canvas.drawPath(path_77, paint_77_fill);

    Path path_78 = Path();
    path_78.moveTo(size.width * 0.08642290, size.height * 0.7303333);
    path_78.cubicTo(
        size.width * 0.07866870,
        size.height * 0.7114431,
        size.width * 0.06236168,
        size.height * 0.7012794,
        size.width * 0.06220183,
        size.height * 0.7011765);
    path_78.lineTo(size.width * 0.06324099, size.height * 0.6984049);
    path_78.cubicTo(
        size.width * 0.06396046,
        size.height * 0.6988157,
        size.width * 0.08050763,
        size.height * 0.7090824,
        size.width * 0.08858092,
        size.height * 0.7288961);
    path_78.lineTo(size.width * 0.08642290, size.height * 0.7303333);
    path_78.close();

    Paint paint_78_fill = Paint()..style = PaintingStyle.fill;
    paint_78_fill.color = Colors.black;
    canvas.drawPath(path_78, paint_78_fill);

    Path path_79 = Path();
    path_79.moveTo(size.width * 0.08827023, size.height * 0.7680147);
    path_79.lineTo(size.width * 0.08651145, size.height * 0.7659618);
    path_79.cubicTo(
        size.width * 0.08691145,
        size.height * 0.7653461,
        size.width * 0.09650382,
        size.height * 0.7520000,
        size.width * 0.1110519,
        size.height * 0.7419382);
    path_79.lineTo(size.width * 0.1121710, size.height * 0.7446078);
    path_79.cubicTo(
        size.width * 0.09802214,
        size.height * 0.7544637,
        size.width * 0.08842977,
        size.height * 0.7679127,
        size.width * 0.08827023,
        size.height * 0.7680147);
    path_79.close();

    Paint paint_79_fill = Paint()..style = PaintingStyle.fill;
    paint_79_fill.color = Colors.black;
    canvas.drawPath(path_79, paint_79_fill);

    Path path_80 = Path();
    path_80.moveTo(size.width * 0.08837634, size.height * 0.7797990);
    path_80.lineTo(size.width * 0.08669771, size.height * 0.7775402);
    path_80.cubicTo(
        size.width * 0.08717786,
        size.height * 0.7770275,
        size.width * 0.09804885,
        size.height * 0.7637833,
        size.width * 0.1097198,
        size.height * 0.7577265);
    path_80.lineTo(size.width * 0.1105992, size.height * 0.7606010);
    path_80.cubicTo(
        size.width * 0.09932748,
        size.height * 0.7664529,
        size.width * 0.08845649,
        size.height * 0.7796961,
        size.width * 0.08837634,
        size.height * 0.7797990);
    path_80.close();

    Paint paint_80_fill = Paint()..style = PaintingStyle.fill;
    paint_80_fill.color = Colors.black;
    canvas.drawPath(path_80, paint_80_fill);

    Path path_81 = Path();
    path_81.moveTo(size.width * 0.09901756, size.height * 0.8855637);
    path_81.cubicTo(
        size.width * 0.09869771,
        size.height * 0.8831000,
        size.width * 0.09102366,
        size.height * 0.8239657,
        size.width * 0.1082901,
        size.height * 0.7834137);
    path_81.lineTo(size.width * 0.1118076, size.height * 0.7858775);
    path_81.cubicTo(
        size.width * 0.09518015,
        size.height * 0.8247873,
        size.width * 0.1028542,
        size.height * 0.8841265,
        size.width * 0.1029344,
        size.height * 0.8847431);
    path_81.lineTo(size.width * 0.09901756, size.height * 0.8855637);
    path_81.close();

    Paint paint_81_fill = Paint()..style = PaintingStyle.fill;
    paint_81_fill.color = Colors.black;
    canvas.drawPath(path_81, paint_81_fill);

    Path path_82 = Path();
    path_82.moveTo(size.width * 0.1420214, size.height * 0.8860382);
    path_82.lineTo(size.width * 0.1409824, size.height * 0.8943539);
    path_82.lineTo(size.width * 0.1298710, size.height * 0.9819216);
    path_82.cubicTo(
        size.width * 0.1293916,
        size.height * 0.9856176,
        size.width * 0.1269137,
        size.height * 0.9883922,
        size.width * 0.1239557,
        size.height * 0.9883922);
    path_82.lineTo(size.width * 0.05784817, size.height * 0.9883922);
    path_82.cubicTo(
        size.width * 0.05489053,
        size.height * 0.9883922,
        size.width * 0.05241252,
        size.height * 0.9856176,
        size.width * 0.05193290,
        size.height * 0.9819216);
    path_82.lineTo(size.width * 0.04082168, size.height * 0.8943539);
    path_82.lineTo(size.width * 0.03978252, size.height * 0.8860382);
    path_82.cubicTo(
        size.width * 0.03922298,
        size.height * 0.8814186,
        size.width * 0.04202076,
        size.height * 0.8771059,
        size.width * 0.04569779,
        size.height * 0.8771059);
    path_82.lineTo(size.width * 0.1361863, size.height * 0.8771059);
    path_82.cubicTo(
        size.width * 0.1397832,
        size.height * 0.8771059,
        size.width * 0.1425809,
        size.height * 0.8813157,
        size.width * 0.1420214,
        size.height * 0.8860382);
    path_82.close();

    Paint paint_82_fill = Paint()..style = PaintingStyle.fill;
    paint_82_fill.color = Colors.white;
    canvas.drawPath(path_82, paint_82_fill);

    Path path_83 = Path();
    path_83.moveTo(size.width * 0.1239962, size.height * 0.9910588);
    path_83.lineTo(size.width * 0.05788840, size.height * 0.9910588);
    path_83.cubicTo(
        size.width * 0.05397145,
        size.height * 0.9910588,
        size.width * 0.05069405,
        size.height * 0.9874608,
        size.width * 0.05005458,
        size.height * 0.9824314);
    path_83.lineTo(size.width * 0.03790420, size.height * 0.8865451);
    path_83.cubicTo(
        size.width * 0.03750450,
        size.height * 0.8835676,
        size.width * 0.03814405,
        size.height * 0.8805902,
        size.width * 0.03966282,
        size.height * 0.8782294);
    path_83.cubicTo(
        size.width * 0.04118160,
        size.height * 0.8759706,
        size.width * 0.04341985,
        size.height * 0.8746363,
        size.width * 0.04573802,
        size.height * 0.8746363);
    path_83.lineTo(size.width * 0.1362260, size.height * 0.8746363);
    path_83.cubicTo(
        size.width * 0.1385443,
        size.height * 0.8746363,
        size.width * 0.1407824,
        size.height * 0.8759706,
        size.width * 0.1423015,
        size.height * 0.8782294);
    path_83.cubicTo(
        size.width * 0.1438206,
        size.height * 0.8804882,
        size.width * 0.1444595,
        size.height * 0.8835676,
        size.width * 0.1440603,
        size.height * 0.8865451);
    path_83.lineTo(size.width * 0.1319099, size.height * 0.9824314);
    path_83.cubicTo(
        size.width * 0.1311901,
        size.height * 0.9874608,
        size.width * 0.1279130,
        size.height * 0.9910588,
        size.width * 0.1239962,
        size.height * 0.9910588);
    path_83.close();
    path_83.moveTo(size.width * 0.04573802, size.height * 0.8798716);
    path_83.cubicTo(
        size.width * 0.04453893,
        size.height * 0.8798716,
        size.width * 0.04349977,
        size.height * 0.8804882,
        size.width * 0.04270038,
        size.height * 0.8816176);
    path_83.cubicTo(
        size.width * 0.04198099,
        size.height * 0.8827471,
        size.width * 0.04158130,
        size.height * 0.8842863,
        size.width * 0.04182107,
        size.height * 0.8857235);
    path_83.lineTo(size.width * 0.05397145, size.height * 0.9816078);
    path_83.cubicTo(
        size.width * 0.05429122,
        size.height * 0.9840784,
        size.width * 0.05596985,
        size.height * 0.9859216,
        size.width * 0.05788840,
        size.height * 0.9859216);
    path_83.lineTo(size.width * 0.1239962, size.height * 0.9859216);
    path_83.cubicTo(
        size.width * 0.1259145,
        size.height * 0.9859216,
        size.width * 0.1275931,
        size.height * 0.9840784,
        size.width * 0.1279130,
        size.height * 0.9816078);
    path_83.lineTo(size.width * 0.1400634, size.height * 0.8857235);
    path_83.cubicTo(
        size.width * 0.1402229,
        size.height * 0.8842863,
        size.width * 0.1399031,
        size.height * 0.8827471,
        size.width * 0.1391840,
        size.height * 0.8816176);
    path_83.cubicTo(
        size.width * 0.1384641,
        size.height * 0.8804882,
        size.width * 0.1373450,
        size.height * 0.8798716,
        size.width * 0.1361466,
        size.height * 0.8798716);
    path_83.lineTo(size.width * 0.04573802, size.height * 0.8798716);
    path_83.lineTo(size.width * 0.04573802, size.height * 0.8798716);
    path_83.close();

    Paint paint_83_fill = Paint()..style = PaintingStyle.fill;
    paint_83_fill.color = Colors.black;
    canvas.drawPath(path_83, paint_83_fill);

    Path path_84 = Path();
    path_84.moveTo(size.width * 0.1420809, size.height * 0.8860382);
    path_84.lineTo(size.width * 0.1410420, size.height * 0.8943539);
    path_84.lineTo(size.width * 0.04088130, size.height * 0.8943539);
    path_84.lineTo(size.width * 0.03984214, size.height * 0.8860382);
    path_84.cubicTo(
        size.width * 0.03928260,
        size.height * 0.8814186,
        size.width * 0.04208038,
        size.height * 0.8771059,
        size.width * 0.04575748,
        size.height * 0.8771059);
    path_84.lineTo(size.width * 0.1362458, size.height * 0.8771059);
    path_84.cubicTo(
        size.width * 0.1398427,
        size.height * 0.8771059,
        size.width * 0.1426405,
        size.height * 0.8813157,
        size.width * 0.1420809,
        size.height * 0.8860382);
    path_84.close();

    Paint paint_84_fill = Paint()..style = PaintingStyle.fill;
    paint_84_fill.color = Colors.black;
    canvas.drawPath(path_84, paint_84_fill);

    Path path_85 = Path();
    path_85.moveTo(size.width * 0.1426840, size.height * 0.8973716);
    path_85.lineTo(size.width * 0.03916626, size.height * 0.8973716);
    path_85.lineTo(size.width * 0.03780733, size.height * 0.8868990);
    path_85.cubicTo(
        size.width * 0.03740763,
        size.height * 0.8839225,
        size.width * 0.03804710,
        size.height * 0.8809451,
        size.width * 0.03956588,
        size.height * 0.8785833);
    path_85.cubicTo(
        size.width * 0.04108473,
        size.height * 0.8763255,
        size.width * 0.04332290,
        size.height * 0.8749902,
        size.width * 0.04564107,
        size.height * 0.8749902);
    path_85.lineTo(size.width * 0.1361290, size.height * 0.8749902);
    path_85.cubicTo(
        size.width * 0.1384473,
        size.height * 0.8749902,
        size.width * 0.1406855,
        size.height * 0.8763255,
        size.width * 0.1422046,
        size.height * 0.8785833);
    path_85.cubicTo(
        size.width * 0.1437237,
        size.height * 0.8808422,
        size.width * 0.1443626,
        size.height * 0.8839225,
        size.width * 0.1439634,
        size.height * 0.8868990);
    path_85.lineTo(size.width * 0.1426840, size.height * 0.8973716);
    path_85.close();
    path_85.moveTo(size.width * 0.04252359, size.height * 0.8922382);
    path_85.lineTo(size.width * 0.1392466, size.height * 0.8922382);
    path_85.lineTo(size.width * 0.1400466, size.height * 0.8860784);
    path_85.cubicTo(
        size.width * 0.1402061,
        size.height * 0.8846412,
        size.width * 0.1398863,
        size.height * 0.8831010,
        size.width * 0.1391672,
        size.height * 0.8819716);
    path_85.cubicTo(
        size.width * 0.1384473,
        size.height * 0.8808422,
        size.width * 0.1373282,
        size.height * 0.8802265,
        size.width * 0.1361290,
        size.height * 0.8802265);
    path_85.lineTo(size.width * 0.04572099, size.height * 0.8802265);
    path_85.cubicTo(
        size.width * 0.04452198,
        size.height * 0.8802265,
        size.width * 0.04348282,
        size.height * 0.8808422,
        size.width * 0.04268344,
        size.height * 0.8819716);
    path_85.cubicTo(
        size.width * 0.04196397,
        size.height * 0.8831010,
        size.width * 0.04156435,
        size.height * 0.8846412,
        size.width * 0.04180412,
        size.height * 0.8860784);
    path_85.lineTo(size.width * 0.04252359, size.height * 0.8922382);
    path_85.close();

    Paint paint_85_fill = Paint()..style = PaintingStyle.fill;
    paint_85_fill.color = Colors.black;
    canvas.drawPath(path_85, paint_85_fill);

    Path path_86 = Path();
    path_86.moveTo(size.width * 0.1392015, size.height * 0.2848304);
    path_86.cubicTo(
        size.width * 0.1392015,
        size.height * 0.2848304,
        size.width * 0.1406397,
        size.height * 0.2944814,
        size.width * 0.1440771,
        size.height * 0.3023863);
    path_86.cubicTo(
        size.width * 0.1455160,
        size.height * 0.3057745,
        size.width * 0.1483137,
        size.height * 0.3078275,
        size.width * 0.1512718,
        size.height * 0.3078275);
    path_86.lineTo(size.width * 0.2288901, size.height * 0.3078275);
    path_86.cubicTo(
        size.width * 0.2320076,
        size.height * 0.3078275,
        size.width * 0.2348855,
        size.height * 0.3055686,
        size.width * 0.2363244,
        size.height * 0.3019755);
    path_86.cubicTo(
        size.width * 0.2379229,
        size.height * 0.2978686,
        size.width * 0.2398412,
        size.height * 0.2920176,
        size.width * 0.2409603,
        size.height * 0.2849333);
    path_86.lineTo(size.width * 0.1392015, size.height * 0.2849333);
    path_86.lineTo(size.width * 0.1392015, size.height * 0.2848304);
    path_86.close();

    Paint paint_86_fill = Paint()..style = PaintingStyle.fill;
    paint_86_fill.color = Colors.white;
    canvas.drawPath(path_86, paint_86_fill);

    Path path_87 = Path();
    path_87.moveTo(size.width * 0.2288504, size.height * 0.3101745);
    path_87.lineTo(size.width * 0.1512321, size.height * 0.3101745);
    path_87.cubicTo(
        size.width * 0.1474748,
        size.height * 0.3101745,
        size.width * 0.1440374,
        size.height * 0.3076078,
        size.width * 0.1422794,
        size.height * 0.3033990);
    path_87.cubicTo(
        size.width * 0.1387618,
        size.height * 0.2951853,
        size.width * 0.1372427,
        size.height * 0.2855353,
        size.width * 0.1372427,
        size.height * 0.2851245);
    path_87.lineTo(size.width * 0.1367634, size.height * 0.2820451);
    path_87.lineTo(size.width * 0.2433992, size.height * 0.2820451);
    path_87.lineTo(size.width * 0.2429191, size.height * 0.2851245);
    path_87.cubicTo(
        size.width * 0.2417206,
        size.height * 0.2926186,
        size.width * 0.2396420,
        size.height * 0.2988814,
        size.width * 0.2381229,
        size.height * 0.3027824);
    path_87.cubicTo(
        size.width * 0.2362847,
        size.height * 0.3074029,
        size.width * 0.2327672,
        size.height * 0.3101745,
        size.width * 0.2288504,
        size.height * 0.3101745);
    path_87.close();
    path_87.moveTo(size.width * 0.1417198, size.height * 0.2872804);
    path_87.cubicTo(
        size.width * 0.1423588,
        size.height * 0.2906686,
        size.width * 0.1437183,
        size.height * 0.2962118,
        size.width * 0.1457962,
        size.height * 0.3009343);
    path_87.cubicTo(
        size.width * 0.1469153,
        size.height * 0.3035010,
        size.width * 0.1489939,
        size.height * 0.3050412,
        size.width * 0.1513122,
        size.height * 0.3050412);
    path_87.lineTo(size.width * 0.2289305, size.height * 0.3050412);
    path_87.cubicTo(
        size.width * 0.2313282,
        size.height * 0.3050412,
        size.width * 0.2334870,
        size.height * 0.3032961,
        size.width * 0.2345260,
        size.height * 0.3005245);
    path_87.cubicTo(
        size.width * 0.2357252,
        size.height * 0.2974441,
        size.width * 0.2373237,
        size.height * 0.2927216,
        size.width * 0.2384427,
        size.height * 0.2871775);
    path_87.lineTo(size.width * 0.1417198, size.height * 0.2871775);
    path_87.lineTo(size.width * 0.1417198, size.height * 0.2872804);
    path_87.close();

    Paint paint_87_fill = Paint()..style = PaintingStyle.fill;
    paint_87_fill.color = Colors.black;
    canvas.drawPath(path_87, paint_87_fill);

    Path path_88 = Path();
    path_88.moveTo(size.width * 0.1413748, size.height * 0.2738588);
    path_88.cubicTo(
        size.width * 0.1413748,
        size.height * 0.2738588,
        size.width * 0.1327412,
        size.height * 0.2817637,
        size.width * 0.1329817,
        size.height * 0.2882314);
    path_88.cubicTo(
        size.width * 0.1332214,
        size.height * 0.2942892,
        size.width * 0.1424939,
        size.height * 0.2896686,
        size.width * 0.1520061,
        size.height * 0.2867941);
    path_88.cubicTo(
        size.width * 0.1520061,
        size.height * 0.2867941,
        size.width * 0.1452916,
        size.height * 0.3018863,
        size.width * 0.1631977,
        size.height * 0.2903873);
    path_88.cubicTo(
        size.width * 0.1631977,
        size.height * 0.2903873,
        size.width * 0.1699122,
        size.height * 0.3054794,
        size.width * 0.1766267,
        size.height * 0.2911059);
    path_88.cubicTo(
        size.width * 0.1766267,
        size.height * 0.2911059,
        size.width * 0.1984496,
        size.height * 0.3119471,
        size.width * 0.2012473,
        size.height * 0.2918245);
    path_88.cubicTo(
        size.width * 0.2012473,
        size.height * 0.2918245,
        size.width * 0.2157954,
        size.height * 0.3040422,
        size.width * 0.2163550,
        size.height * 0.2918245);
    path_88.cubicTo(
        size.width * 0.2163550,
        size.height * 0.2918245,
        size.width * 0.2366595,
        size.height * 0.2997304,
        size.width * 0.2434542,
        size.height * 0.2926461);
    path_88.cubicTo(
        size.width * 0.2445733,
        size.height * 0.2915167,
        size.width * 0.2451328,
        size.height * 0.2896686,
        size.width * 0.2448924,
        size.height * 0.2879235);
    path_88.cubicTo(
        size.width * 0.2445733,
        size.height * 0.2850490,
        size.width * 0.2430542,
        size.height * 0.2806343,
        size.width * 0.2364992,
        size.height * 0.2782735);
    path_88.lineTo(size.width * 0.1413748, size.height * 0.2738588);
    path_88.close();

    Paint paint_88_fill = Paint()..style = PaintingStyle.fill;
    paint_88_fill.color = _activeColor;
    canvas.drawPath(path_88, paint_88_fill);

    Path path_89 = Path();
    path_89.moveTo(size.width * 0.1435962, size.height * 0.2594402);
    path_89.cubicTo(
        size.width * 0.1435962,
        size.height * 0.2594402,
        size.width * 0.1372015,
        size.height * 0.2640598,
        size.width * 0.1374412,
        size.height * 0.2702196);
    path_89.cubicTo(
        size.width * 0.1376008,
        size.height * 0.2740186,
        size.width * 0.1393595,
        size.height * 0.2777147,
        size.width * 0.1469534,
        size.height * 0.2795618);
    path_89.cubicTo(
        size.width * 0.1469534,
        size.height * 0.2795618,
        size.width * 0.1811664,
        size.height * 0.2895206,
        size.width * 0.2229733,
        size.height * 0.2838745);
    path_89.cubicTo(
        size.width * 0.2284092,
        size.height * 0.2831559,
        size.width * 0.2408786,
        size.height * 0.2818206,
        size.width * 0.2419977,
        size.height * 0.2709382);
    path_89.cubicTo(
        size.width * 0.2423977,
        size.height * 0.2670373,
        size.width * 0.2411985,
        size.height * 0.2605696,
        size.width * 0.2324855,
        size.height * 0.2608775);
    path_89.lineTo(size.width * 0.1435962, size.height * 0.2594402);
    path_89.close();

    Paint paint_89_fill = Paint()..style = PaintingStyle.fill;
    paint_89_fill.color = Colors.white;
    canvas.drawPath(path_89, paint_89_fill);

    Path path_90 = Path();
    path_90.moveTo(size.width * 0.1969656, size.height * 0.2881225);
    path_90.cubicTo(
        size.width * 0.1679489,
        size.height * 0.2881225,
        size.width * 0.1468450,
        size.height * 0.2821676,
        size.width * 0.1466053,
        size.height * 0.2820647);
    path_90.cubicTo(
        size.width * 0.1394908,
        size.height * 0.2803196,
        size.width * 0.1358145,
        size.height * 0.2765216,
        size.width * 0.1355740,
        size.height * 0.2703618);
    path_90.cubicTo(
        size.width * 0.1352550,
        size.height * 0.2628667,
        size.width * 0.1419695,
        size.height * 0.2577343,
        size.width * 0.1427687,
        size.height * 0.2572206);
    path_90.lineTo(size.width * 0.1432481, size.height * 0.2569127);
    path_90.lineTo(size.width * 0.2326176, size.height * 0.2583500);
    path_90.cubicTo(
        size.width * 0.2369344,
        size.height * 0.2581441,
        size.width * 0.2401313,
        size.height * 0.2595814,
        size.width * 0.2421298,
        size.height * 0.2623539);
    path_90.cubicTo(
        size.width * 0.2437290,
        size.height * 0.2646127,
        size.width * 0.2444481,
        size.height * 0.2678971,
        size.width * 0.2441282,
        size.height * 0.2712853);
    path_90.cubicTo(
        size.width * 0.2428496,
        size.height * 0.2838108,
        size.width * 0.2299794,
        size.height * 0.2855559,
        size.width * 0.2238244,
        size.height * 0.2863775);
    path_90.lineTo(size.width * 0.2233450, size.height * 0.2864794);
    path_90.cubicTo(
        size.width * 0.2140718,
        size.height * 0.2876088,
        size.width * 0.2051992,
        size.height * 0.2881225,
        size.width * 0.1969656,
        size.height * 0.2881225);
    path_90.close();
    path_90.moveTo(size.width * 0.1442076, size.height * 0.2620461);
    path_90.cubicTo(
        size.width * 0.1425290,
        size.height * 0.2633804,
        size.width * 0.1393313,
        size.height * 0.2667686,
        size.width * 0.1394916,
        size.height * 0.2700539);
    path_90.cubicTo(
        size.width * 0.1395710,
        size.height * 0.2715931,
        size.width * 0.1397313,
        size.height * 0.2751863,
        size.width * 0.1474046,
        size.height * 0.2770343);
    path_90.cubicTo(
        size.width * 0.1478046,
        size.height * 0.2771373,
        size.width * 0.1818580,
        size.height * 0.2867873,
        size.width * 0.2228649,
        size.height * 0.2813461);
    path_90.lineTo(size.width * 0.2233450, size.height * 0.2812441);
    path_90.cubicTo(
        size.width * 0.2284611,
        size.height * 0.2805255,
        size.width * 0.2391725,
        size.height * 0.2791902,
        size.width * 0.2400519,
        size.height * 0.2705667);
    path_90.cubicTo(
        size.width * 0.2401313,
        size.height * 0.2696431,
        size.width * 0.2402115,
        size.height * 0.2674873,
        size.width * 0.2390122,
        size.height * 0.2657412);
    path_90.cubicTo(
        size.width * 0.2378130,
        size.height * 0.2640990,
        size.width * 0.2355748,
        size.height * 0.2632775,
        size.width * 0.2325374,
        size.height * 0.2633804);
    path_90.lineTo(size.width * 0.2324573, size.height * 0.2633804);
    path_90.lineTo(size.width * 0.2323779, size.height * 0.2633804);
    path_90.lineTo(size.width * 0.1442076, size.height * 0.2620461);
    path_90.close();

    Paint paint_90_fill = Paint()..style = PaintingStyle.fill;
    paint_90_fill.color = Colors.black;
    canvas.drawPath(path_90, paint_90_fill);

    Path path_91 = Path();
    path_91.moveTo(size.width * 0.1576809, size.height * 0.2622833);
    path_91.lineTo(size.width * 0.1748679, size.height * 0.2779912);
    path_91.cubicTo(
        size.width * 0.1772656,
        size.height * 0.2802500,
        size.width * 0.1805435,
        size.height * 0.2802500,
        size.width * 0.1830214,
        size.height * 0.2780941);
    path_91.lineTo(size.width * 0.1995679, size.height * 0.2637206);
    path_91.lineTo(size.width * 0.1576809, size.height * 0.2622833);
    path_91.close();

    Paint paint_91_fill = Paint()..style = PaintingStyle.fill;
    paint_91_fill.color = _activeColor;
    canvas.drawPath(path_91, paint_91_fill);

    Path path_92 = Path();
    path_92.moveTo(size.width * 0.2082237, size.height * 0.2632461);
    path_92.lineTo(size.width * 0.2186954, size.height * 0.2746412);
    path_92.cubicTo(
        size.width * 0.2210137,
        size.height * 0.2771049,
        size.width * 0.2243710,
        size.height * 0.2767971,
        size.width * 0.2263695,
        size.height * 0.2739225);
    path_92.lineTo(size.width * 0.2378802, size.height * 0.2574961);
    path_92.lineTo(size.width * 0.2082237, size.height * 0.2632461);
    path_92.close();

    Paint paint_92_fill = Paint()..style = PaintingStyle.fill;
    paint_92_fill.color = _activeColor;
    canvas.drawPath(path_92, paint_92_fill);

    Path path_93 = Path();
    path_93.moveTo(size.width * 0.1424924, size.height * 0.2626353);
    path_93.cubicTo(
        size.width * 0.1600786,
        size.height * 0.2655098,
        size.width * 0.1978084,
        size.height * 0.2700275,
        size.width * 0.2319420,
        size.height * 0.2633539);
    path_93.cubicTo(
        size.width * 0.2385763,
        size.height * 0.2620196,
        size.width * 0.2425733,
        size.height * 0.2532931,
        size.width * 0.2402550,
        size.height * 0.2451824);
    path_93.cubicTo(
        size.width * 0.2355389,
        size.height * 0.2284480,
        size.width * 0.2229092,
        size.height * 0.2039118,
        size.width * 0.1896550,
        size.height * 0.2015500);
    path_93.cubicTo(
        size.width * 0.1731885,
        size.height * 0.2004206,
        size.width * 0.1435321,
        size.height * 0.2094549,
        size.width * 0.1345786,
        size.height * 0.2468245);
    path_93.cubicTo(
        size.width * 0.1329000,
        size.height * 0.2541137,
        size.width * 0.1366573,
        size.height * 0.2617108,
        size.width * 0.1424924,
        size.height * 0.2626353);
    path_93.close();

    Paint paint_93_fill = Paint()..style = PaintingStyle.fill;
    paint_93_fill.color = Colors.white;
    canvas.drawPath(path_93, paint_93_fill);

    Path path_94 = Path();
    path_94.moveTo(size.width * 0.1928427, size.height * 0.2695549);
    path_94.cubicTo(
        size.width * 0.1725389,
        size.height * 0.2695549,
        size.width * 0.1540740,
        size.height * 0.2671931,
        size.width * 0.1424031,
        size.height * 0.2653451);
    path_94.cubicTo(
        size.width * 0.1390458,
        size.height * 0.2648324,
        size.width * 0.1360084,
        size.height * 0.2623686,
        size.width * 0.1341695,
        size.height * 0.2586725);
    path_94.cubicTo(
        size.width * 0.1323313,
        size.height * 0.2549765,
        size.width * 0.1318511,
        size.height * 0.2504588,
        size.width * 0.1328107,
        size.height * 0.2461471);
    path_94.cubicTo(
        size.width * 0.1368870,
        size.height * 0.2290029,
        size.width * 0.1458405,
        size.height * 0.2156559,
        size.width * 0.1585504,
        size.height * 0.2075461);
    path_94.cubicTo(
        size.width * 0.1679031,
        size.height * 0.2015912,
        size.width * 0.1796534,
        size.height * 0.1984088,
        size.width * 0.1899656,
        size.height * 0.1991275);
    path_94.cubicTo(
        size.width * 0.2250573,
        size.height * 0.2015912,
        size.width * 0.2378473,
        size.height * 0.2285922,
        size.width * 0.2423237,
        size.height * 0.2445049);
    path_94.cubicTo(
        size.width * 0.2436031,
        size.height * 0.2491245,
        size.width * 0.2432832,
        size.height * 0.2541549,
        size.width * 0.2412847,
        size.height * 0.2584667);
    path_94.cubicTo(
        size.width * 0.2393664,
        size.height * 0.2626765,
        size.width * 0.2360885,
        size.height * 0.2654480,
        size.width * 0.2324115,
        size.height * 0.2661667);
    path_94.cubicTo(
        size.width * 0.2192221,
        size.height * 0.2686304,
        size.width * 0.2056328,
        size.height * 0.2695549,
        size.width * 0.1928427,
        size.height * 0.2695549);
    path_94.close();
    path_94.moveTo(size.width * 0.1428824, size.height * 0.2602127);
    path_94.cubicTo(
        size.width * 0.1617473,
        size.height * 0.2632922,
        size.width * 0.1983588,
        size.height * 0.2675020,
        size.width * 0.2317718,
        size.height * 0.2610333);
    path_94.cubicTo(
        size.width * 0.2343298,
        size.height * 0.2605206,
        size.width * 0.2364885,
        size.height * 0.2586725,
        size.width * 0.2378473,
        size.height * 0.2557980);
    path_94.cubicTo(
        size.width * 0.2392061,
        size.height * 0.2529235,
        size.width * 0.2394458,
        size.height * 0.2494324,
        size.width * 0.2385664,
        size.height * 0.2462500);
    path_94.cubicTo(
        size.width * 0.2335305,
        size.height * 0.2282843,
        size.width * 0.2209008,
        size.height * 0.2065196,
        size.width * 0.1897252,
        size.height * 0.2042608);
    path_94.cubicTo(
        size.width * 0.1751771,
        size.height * 0.2032343,
        size.width * 0.1454405,
        size.height * 0.2110363,
        size.width * 0.1366473,
        size.height * 0.2476873);
    path_94.cubicTo(
        size.width * 0.1360084,
        size.height * 0.2504588,
        size.width * 0.1363275,
        size.height * 0.2534363,
        size.width * 0.1375267,
        size.height * 0.2559000);
    path_94.cubicTo(
        size.width * 0.1387260,
        size.height * 0.2582618,
        size.width * 0.1406443,
        size.height * 0.2599039,
        size.width * 0.1428824,
        size.height * 0.2602127);
    path_94.close();

    Paint paint_94_fill = Paint()..style = PaintingStyle.fill;
    paint_94_fill.color = Colors.black;
    canvas.drawPath(path_94, paint_94_fill);

    Path path_95 = Path();
    path_95.moveTo(size.width * 0.1817695, size.height * 0.2518922);
    path_95.cubicTo(
        size.width * 0.1832084,
        size.height * 0.2512765,
        size.width * 0.1868053,
        size.height * 0.2504549,
        size.width * 0.1892031,
        size.height * 0.2546637);
    path_95.cubicTo(
        size.width * 0.1894427,
        size.height * 0.2550745,
        size.width * 0.1892832,
        size.height * 0.2555882,
        size.width * 0.1889634,
        size.height * 0.2557931);
    path_95.cubicTo(
        size.width * 0.1875244,
        size.height * 0.2565118,
        size.width * 0.1838473,
        size.height * 0.2577441,
        size.width * 0.1814496,
        size.height * 0.2530216);
    path_95.cubicTo(
        size.width * 0.1812893,
        size.height * 0.2526108,
        size.width * 0.1813695,
        size.height * 0.2520971,
        size.width * 0.1817695,
        size.height * 0.2518922);
    path_95.close();

    Paint paint_95_fill = Paint()..style = PaintingStyle.fill;
    paint_95_fill.color = Colors.black;
    canvas.drawPath(path_95, paint_95_fill);

    Path path_96 = Path();
    path_96.moveTo(size.width * 0.1836908, size.height * 0.2160422);
    path_96.cubicTo(
        size.width * 0.1846504,
        size.height * 0.2175814,
        size.width * 0.1867282,
        size.height * 0.2214833,
        size.width * 0.1846504,
        size.height * 0.2258971);
    path_96.cubicTo(
        size.width * 0.1844901,
        size.height * 0.2263078,
        size.width * 0.1840107,
        size.height * 0.2264108,
        size.width * 0.1837710,
        size.height * 0.2260000);
    path_96.cubicTo(
        size.width * 0.1826519,
        size.height * 0.2245627,
        size.width * 0.1804137,
        size.height * 0.2207647,
        size.width * 0.1828115,
        size.height * 0.2160422);
    path_96.cubicTo(
        size.width * 0.1830511,
        size.height * 0.2157333,
        size.width * 0.1834511,
        size.height * 0.2157333,
        size.width * 0.1836908,
        size.height * 0.2160422);
    path_96.close();

    Paint paint_96_fill = Paint()..style = PaintingStyle.fill;
    paint_96_fill.color = Colors.black;
    canvas.drawPath(path_96, paint_96_fill);

    Path path_97 = Path();
    path_97.moveTo(size.width * 0.1735176, size.height * 0.2340657);
    path_97.cubicTo(
        size.width * 0.1730382,
        size.height * 0.2359137,
        size.width * 0.1715992,
        size.height * 0.2402255,
        size.width * 0.1675221,
        size.height * 0.2404304);
    path_97.cubicTo(
        size.width * 0.1671229,
        size.height * 0.2404304,
        size.width * 0.1668832,
        size.height * 0.2400206,
        size.width * 0.1669626,
        size.height * 0.2395069);
    path_97.cubicTo(
        size.width * 0.1672824,
        size.height * 0.2375559,
        size.width * 0.1685618,
        size.height * 0.2330392,
        size.width * 0.1729580,
        size.height * 0.2331412);
    path_97.cubicTo(
        size.width * 0.1733580,
        size.height * 0.2331412,
        size.width * 0.1736771,
        size.height * 0.2336549,
        size.width * 0.1735176,
        size.height * 0.2340657);
    path_97.close();

    Paint paint_97_fill = Paint()..style = PaintingStyle.fill;
    paint_97_fill.color = Colors.black;
    canvas.drawPath(path_97, paint_97_fill);

    Path path_98 = Path();
    path_98.moveTo(size.width * 0.1499931, size.height * 0.2346784);
    path_98.cubicTo(
        size.width * 0.1510321,
        size.height * 0.2361157,
        size.width * 0.1531908,
        size.height * 0.2399147,
        size.width * 0.1512718,
        size.height * 0.2445343);
    path_98.cubicTo(
        size.width * 0.1511122,
        size.height * 0.2449451,
        size.width * 0.1506328,
        size.height * 0.2450480,
        size.width * 0.1503931,
        size.height * 0.2447392);
    path_98.cubicTo(
        size.width * 0.1492740,
        size.height * 0.2434049,
        size.width * 0.1468756,
        size.height * 0.2397088,
        size.width * 0.1491137,
        size.height * 0.2348843);
    path_98.cubicTo(
        size.width * 0.1493534,
        size.height * 0.2344735,
        size.width * 0.1497534,
        size.height * 0.2343706,
        size.width * 0.1499931,
        size.height * 0.2346784);
    path_98.close();

    Paint paint_98_fill = Paint()..style = PaintingStyle.fill;
    paint_98_fill.color = Colors.black;
    canvas.drawPath(path_98, paint_98_fill);

    Path path_99 = Path();
    path_99.moveTo(size.width * 0.1612855, size.height * 0.2152520);
    path_99.cubicTo(
        size.width * 0.1628038,
        size.height * 0.2157657,
        size.width * 0.1662412,
        size.height * 0.2174078,
        size.width * 0.1665611,
        size.height * 0.2225412);
    path_99.cubicTo(
        size.width * 0.1665611,
        size.height * 0.2230549,
        size.width * 0.1662412,
        size.height * 0.2233627,
        size.width * 0.1659214,
        size.height * 0.2233627);
    path_99.cubicTo(
        size.width * 0.1644031,
        size.height * 0.2230549,
        size.width * 0.1608053,
        size.height * 0.2216176,
        size.width * 0.1606458,
        size.height * 0.2159706);
    path_99.cubicTo(
        size.width * 0.1605656,
        size.height * 0.2155598,
        size.width * 0.1608855,
        size.height * 0.2151500,
        size.width * 0.1612855,
        size.height * 0.2152520);
    path_99.close();

    Paint paint_99_fill = Paint()..style = PaintingStyle.fill;
    paint_99_fill.color = Colors.black;
    canvas.drawPath(path_99, paint_99_fill);

    Path path_100 = Path();
    path_100.moveTo(size.width * 0.1964954, size.height * 0.2151216);
    path_100.cubicTo(
        size.width * 0.1967351,
        size.height * 0.2131706,
        size.width * 0.1977740,
        size.height * 0.2086539,
        size.width * 0.2018511,
        size.height * 0.2078324);
    path_100.cubicTo(
        size.width * 0.2022504,
        size.height * 0.2077294,
        size.width * 0.2025702,
        size.height * 0.2081402,
        size.width * 0.2024908,
        size.height * 0.2086539);
    path_100.cubicTo(
        size.width * 0.2023305,
        size.height * 0.2106039,
        size.width * 0.2014511,
        size.height * 0.2153265,
        size.width * 0.1971344,
        size.height * 0.2159422);
    path_100.cubicTo(
        size.width * 0.1967351,
        size.height * 0.2160451,
        size.width * 0.1964153,
        size.height * 0.2156343,
        size.width * 0.1964954,
        size.height * 0.2151216);
    path_100.close();

    Paint paint_100_fill = Paint()..style = PaintingStyle.fill;
    paint_100_fill.color = Colors.black;
    canvas.drawPath(path_100, paint_100_fill);

    Path path_101 = Path();
    path_101.moveTo(size.width * 0.2217252, size.height * 0.2410618);
    path_101.cubicTo(
        size.width * 0.2212458,
        size.height * 0.2392137,
        size.width * 0.2204466,
        size.height * 0.2345941,
        size.width * 0.2237237,
        size.height * 0.2314118);
    path_101.cubicTo(
        size.width * 0.2240435,
        size.height * 0.2311039,
        size.width * 0.2244435,
        size.height * 0.2313088,
        size.width * 0.2246031,
        size.height * 0.2317196);
    path_101.cubicTo(
        size.width * 0.2251626,
        size.height * 0.2335676,
        size.width * 0.2262015,
        size.height * 0.2381873,
        size.width * 0.2226046,
        size.height * 0.2413696);
    path_101.cubicTo(
        size.width * 0.2222847,
        size.height * 0.2416784,
        size.width * 0.2218855,
        size.height * 0.2415755,
        size.width * 0.2217252,
        size.height * 0.2410618);
    path_101.close();

    Paint paint_101_fill = Paint()..style = PaintingStyle.fill;
    paint_101_fill.color = Colors.black;
    canvas.drawPath(path_101, paint_101_fill);

    Path path_102 = Path();
    path_102.moveTo(size.width * 0.1985458, size.height * 0.2301245);
    path_102.cubicTo(
        size.width * 0.2000649,
        size.height * 0.2301245,
        size.width * 0.2037420,
        size.height * 0.2307402,
        size.width * 0.2049405,
        size.height * 0.2357706);
    path_102.cubicTo(
        size.width * 0.2050206,
        size.height * 0.2361814,
        size.width * 0.2047809,
        size.height * 0.2366941,
        size.width * 0.2044611,
        size.height * 0.2366941);
    path_102.cubicTo(
        size.width * 0.2029420,
        size.height * 0.2367971,
        size.width * 0.1991855,
        size.height * 0.2364892,
        size.width * 0.1979863,
        size.height * 0.2310480);
    path_102.cubicTo(
        size.width * 0.1979061,
        size.height * 0.2306373,
        size.width * 0.1982260,
        size.height * 0.2301245,
        size.width * 0.1985458,
        size.height * 0.2301245);
    path_102.close();

    Paint paint_102_fill = Paint()..style = PaintingStyle.fill;
    paint_102_fill.color = Colors.black;
    canvas.drawPath(path_102, paint_102_fill);

    Path path_103 = Path();
    path_103.moveTo(size.width * 0.4089710, size.height * 0.01795843);
    path_103.lineTo(size.width * 0.4089710, size.height * 0.04927088);
    path_103.cubicTo(
        size.width * 0.4089710,
        size.height * 0.05173480,
        size.width * 0.4099305,
        size.height * 0.05389078,
        size.width * 0.4115290,
        size.height * 0.05522539);
    path_103.cubicTo(
        size.width * 0.4170443,
        size.height * 0.05974255,
        size.width * 0.4309534,
        size.height * 0.06816098,
        size.width * 0.4487794,
        size.height * 0.05686794);
    path_103.cubicTo(
        size.width * 0.4506176,
        size.height * 0.05573873,
        size.width * 0.4518168,
        size.height * 0.05317206,
        size.width * 0.4518168,
        size.height * 0.05050284);
    path_103.lineTo(size.width * 0.4518168, size.height * 0.01785578);
    path_103.lineTo(size.width * 0.4089710, size.height * 0.01785578);
    path_103.lineTo(size.width * 0.4089710, size.height * 0.01795843);
    path_103.close();

    Paint paint_103_fill = Paint()..style = PaintingStyle.fill;
    paint_103_fill.color = Colors.black;
    canvas.drawPath(path_103, paint_103_fill);

    Path path_104 = Path();
    path_104.moveTo(size.width * 0.4309466, size.height * 0.06494088);
    path_104.cubicTo(
        size.width * 0.4211947,
        size.height * 0.06494088,
        size.width * 0.4139198,
        size.height * 0.06011569,
        size.width * 0.4104031,
        size.height * 0.05724108);
    path_104.cubicTo(
        size.width * 0.4082443,
        size.height * 0.05549578,
        size.width * 0.4069656,
        size.height * 0.05241588,
        size.width * 0.4069656,
        size.height * 0.04913059);
    path_104.lineTo(size.width * 0.4069656, size.height * 0.01525157);
    path_104.lineTo(size.width * 0.4538084, size.height * 0.01525157);
    path_104.lineTo(size.width * 0.4538084, size.height * 0.05046529);
    path_104.cubicTo(
        size.width * 0.4538084,
        size.height * 0.05416118,
        size.width * 0.4522099,
        size.height * 0.05744637,
        size.width * 0.4496519,
        size.height * 0.05908902);
    path_104.cubicTo(
        size.width * 0.4428573,
        size.height * 0.06340088,
        size.width * 0.4365420,
        size.height * 0.06494088,
        size.width * 0.4309466,
        size.height * 0.06494088);
    path_104.close();
    path_104.moveTo(size.width * 0.4109626, size.height * 0.02038480);
    path_104.lineTo(size.width * 0.4109626, size.height * 0.04913059);
    path_104.cubicTo(
        size.width * 0.4109626,
        size.height * 0.05067059,
        size.width * 0.4116023,
        size.height * 0.05210784,
        size.width * 0.4125611,
        size.height * 0.05292922);
    path_104.cubicTo(
        size.width * 0.4175176,
        size.height * 0.05703578,
        size.width * 0.4307870,
        size.height * 0.06524882,
        size.width * 0.4478130,
        size.height * 0.05446912);
    path_104.cubicTo(
        size.width * 0.4490122,
        size.height * 0.05375049,
        size.width * 0.4497321,
        size.height * 0.05210784,
        size.width * 0.4497321,
        size.height * 0.05046529);
    path_104.lineTo(size.width * 0.4497321, size.height * 0.02038480);
    path_104.lineTo(size.width * 0.4109626, size.height * 0.02038480);
    path_104.lineTo(size.width * 0.4109626, size.height * 0.02038480);
    path_104.close();

    Paint paint_104_fill = Paint()..style = PaintingStyle.fill;
    paint_104_fill.color = Colors.black;
    canvas.drawPath(path_104, paint_104_fill);

    Path path_105 = Path();
    path_105.moveTo(size.width * 0.4303420, size.height * 0.03149255);
    path_105.cubicTo(
        size.width * 0.4421733,
        size.height * 0.03149255,
        size.width * 0.4517649,
        size.height * 0.02496569,
        size.width * 0.4517649,
        size.height * 0.01691431);
    path_105.cubicTo(
        size.width * 0.4517649,
        size.height * 0.008862990,
        size.width * 0.4421733,
        size.height * 0.002336088,
        size.width * 0.4303420,
        size.height * 0.002336088);
    path_105.cubicTo(
        size.width * 0.4185099,
        size.height * 0.002336088,
        size.width * 0.4089191,
        size.height * 0.008862990,
        size.width * 0.4089191,
        size.height * 0.01691431);
    path_105.cubicTo(
        size.width * 0.4089191,
        size.height * 0.02496569,
        size.width * 0.4185099,
        size.height * 0.03149255,
        size.width * 0.4303420,
        size.height * 0.03149255);
    path_105.close();

    Paint paint_105_fill = Paint()..style = PaintingStyle.fill;
    paint_105_fill.color = Colors.white;
    canvas.drawPath(path_105, paint_105_fill);

    Path path_106 = Path();
    path_106.moveTo(size.width * 0.4303794, size.height * 0.03428971);
    path_106.cubicTo(
        size.width * 0.4172702,
        size.height * 0.03428971,
        size.width * 0.4069580,
        size.height * 0.02679520,
        size.width * 0.4069580,
        size.height * 0.01714480);
    path_106.cubicTo(size.width * 0.4069580, size.height * 0.007494451,
        size.width * 0.4172702, 0, size.width * 0.4303794, 0);
    path_106.cubicTo(
        size.width * 0.4434893,
        0,
        size.width * 0.4538008,
        size.height * 0.007494451,
        size.width * 0.4538008,
        size.height * 0.01714480);
    path_106.cubicTo(
        size.width * 0.4538008,
        size.height * 0.02669255,
        size.width * 0.4434893,
        size.height * 0.03428971,
        size.width * 0.4303794,
        size.height * 0.03428971);
    path_106.close();
    path_106.moveTo(size.width * 0.4303794, size.height * 0.005133186);
    path_106.cubicTo(
        size.width * 0.4189489,
        size.height * 0.005133186,
        size.width * 0.4109550,
        size.height * 0.01149833,
        size.width * 0.4109550,
        size.height * 0.01714480);
    path_106.cubicTo(
        size.width * 0.4109550,
        size.height * 0.02279137,
        size.width * 0.4189489,
        size.height * 0.02915647,
        size.width * 0.4303794,
        size.height * 0.02915647);
    path_106.cubicTo(
        size.width * 0.4418107,
        size.height * 0.02915647,
        size.width * 0.4498046,
        size.height * 0.02279137,
        size.width * 0.4498046,
        size.height * 0.01714480);
    path_106.cubicTo(
        size.width * 0.4498046,
        size.height * 0.01149833,
        size.width * 0.4418107,
        size.height * 0.005133186,
        size.width * 0.4303794,
        size.height * 0.005133186);
    path_106.close();

    Paint paint_106_fill = Paint()..style = PaintingStyle.fill;
    paint_106_fill.color = Colors.black;
    canvas.drawPath(path_106, paint_106_fill);

    Path path_107 = Path();
    path_107.moveTo(size.width * 0.4305260, size.height * 0.02331618);
    path_107.cubicTo(
        size.width * 0.4363534,
        size.height * 0.02331618,
        size.width * 0.4410771,
        size.height * 0.02019069,
        size.width * 0.4410771,
        size.height * 0.01633510);
    path_107.cubicTo(
        size.width * 0.4410771,
        size.height * 0.01247951,
        size.width * 0.4363534,
        size.height * 0.009353941,
        size.width * 0.4305260,
        size.height * 0.009353941);
    path_107.cubicTo(
        size.width * 0.4246985,
        size.height * 0.009353941,
        size.width * 0.4199740,
        size.height * 0.01247951,
        size.width * 0.4199740,
        size.height * 0.01633510);
    path_107.cubicTo(
        size.width * 0.4199740,
        size.height * 0.02019069,
        size.width * 0.4246985,
        size.height * 0.02331618,
        size.width * 0.4305260,
        size.height * 0.02331618);
    path_107.close();

    Paint paint_107_fill = Paint()..style = PaintingStyle.fill;
    paint_107_fill.color = Colors.black;
    canvas.drawPath(path_107, paint_107_fill);

    Path path_108 = Path();
    path_108.moveTo(size.width * 0.3643924, size.height * 0.02718794);
    path_108.lineTo(size.width * 0.3643924, size.height * 0.05850039);
    path_108.cubicTo(
        size.width * 0.3643924,
        size.height * 0.06096431,
        size.width * 0.3653511,
        size.height * 0.06312020,
        size.width * 0.3669496,
        size.height * 0.06445480);
    path_108.cubicTo(
        size.width * 0.3724656,
        size.height * 0.06897206,
        size.width * 0.3863748,
        size.height * 0.07739049,
        size.width * 0.4042008,
        size.height * 0.06609745);
    path_108.cubicTo(
        size.width * 0.4060389,
        size.height * 0.06496814,
        size.width * 0.4072382,
        size.height * 0.06240157,
        size.width * 0.4072382,
        size.height * 0.05973235);
    path_108.lineTo(size.width * 0.4072382, size.height * 0.02708529);
    path_108.lineTo(size.width * 0.3643924, size.height * 0.02708529);
    path_108.lineTo(size.width * 0.3643924, size.height * 0.02718794);
    path_108.close();

    Paint paint_108_fill = Paint()..style = PaintingStyle.fill;
    paint_108_fill.color = Colors.black;
    canvas.drawPath(path_108, paint_108_fill);

    Path path_109 = Path();
    path_109.moveTo(size.width * 0.3864122, size.height * 0.07418941);
    path_109.cubicTo(
        size.width * 0.3766603,
        size.height * 0.07418941,
        size.width * 0.3693863,
        size.height * 0.06936431,
        size.width * 0.3658687,
        size.height * 0.06648971);
    path_109.cubicTo(
        size.width * 0.3637107,
        size.height * 0.06474441,
        size.width * 0.3624313,
        size.height * 0.06166451,
        size.width * 0.3624313,
        size.height * 0.05837922);
    path_109.lineTo(size.width * 0.3624313, size.height * 0.02450020);
    path_109.lineTo(size.width * 0.4092740, size.height * 0.02450020);
    path_109.lineTo(size.width * 0.4092740, size.height * 0.05971392);
    path_109.cubicTo(
        size.width * 0.4092740,
        size.height * 0.06330716,
        size.width * 0.4076756,
        size.height * 0.06669500,
        size.width * 0.4051176,
        size.height * 0.06833765);
    path_109.cubicTo(
        size.width * 0.3982427,
        size.height * 0.07264951,
        size.width * 0.3919282,
        size.height * 0.07418941,
        size.width * 0.3864122,
        size.height * 0.07418941);
    path_109.close();
    path_109.moveTo(size.width * 0.3664282, size.height * 0.02963343);
    path_109.lineTo(size.width * 0.3664282, size.height * 0.05837922);
    path_109.cubicTo(
        size.width * 0.3664282,
        size.height * 0.05991922,
        size.width * 0.3670679,
        size.height * 0.06135647,
        size.width * 0.3680267,
        size.height * 0.06217784);
    path_109.cubicTo(
        size.width * 0.3729832,
        size.height * 0.06628431,
        size.width * 0.3862527,
        size.height * 0.07449745,
        size.width * 0.4032794,
        size.height * 0.06371775);
    path_109.cubicTo(
        size.width * 0.4044779,
        size.height * 0.06299912,
        size.width * 0.4051977,
        size.height * 0.06135647,
        size.width * 0.4051977,
        size.height * 0.05971392);
    path_109.lineTo(size.width * 0.4051977, size.height * 0.02963343);
    path_109.lineTo(size.width * 0.3664282, size.height * 0.02963343);
    path_109.lineTo(size.width * 0.3664282, size.height * 0.02963343);
    path_109.close();

    Paint paint_109_fill = Paint()..style = PaintingStyle.fill;
    paint_109_fill.color = Colors.black;
    canvas.drawPath(path_109, paint_109_fill);

    Path path_110 = Path();
    path_110.moveTo(size.width * 0.3857779, size.height * 0.04121990);
    path_110.cubicTo(
        size.width * 0.3976092,
        size.height * 0.04121990,
        size.width * 0.4072008,
        size.height * 0.03469304,
        size.width * 0.4072008,
        size.height * 0.02664167);
    path_110.cubicTo(
        size.width * 0.4072008,
        size.height * 0.01859029,
        size.width * 0.3976092,
        size.height * 0.01206343,
        size.width * 0.3857779,
        size.height * 0.01206343);
    path_110.cubicTo(
        size.width * 0.3739458,
        size.height * 0.01206343,
        size.width * 0.3643550,
        size.height * 0.01859029,
        size.width * 0.3643550,
        size.height * 0.02664167);
    path_110.cubicTo(
        size.width * 0.3643550,
        size.height * 0.03469304,
        size.width * 0.3739458,
        size.height * 0.04121990,
        size.width * 0.3857779,
        size.height * 0.04121990);
    path_110.close();

    Paint paint_110_fill = Paint()..style = PaintingStyle.fill;
    paint_110_fill.color = Colors.white;
    canvas.drawPath(path_110, paint_110_fill);

    Path path_111 = Path();
    path_111.moveTo(size.width * 0.3858679, size.height * 0.04347127);
    path_111.cubicTo(
        size.width * 0.3727580,
        size.height * 0.04347127,
        size.width * 0.3624466,
        size.height * 0.03597686,
        size.width * 0.3624466,
        size.height * 0.02632647);
    path_111.cubicTo(
        size.width * 0.3624466,
        size.height * 0.01667608,
        size.width * 0.3727580,
        size.height * 0.009181598,
        size.width * 0.3858679,
        size.height * 0.009181598);
    path_111.cubicTo(
        size.width * 0.3989771,
        size.height * 0.009181598,
        size.width * 0.4092893,
        size.height * 0.01667608,
        size.width * 0.4092893,
        size.height * 0.02632647);
    path_111.cubicTo(
        size.width * 0.4092893,
        size.height * 0.03597686,
        size.width * 0.3989771,
        size.height * 0.04347127,
        size.width * 0.3858679,
        size.height * 0.04347127);
    path_111.close();
    path_111.moveTo(size.width * 0.3858679, size.height * 0.01441745);
    path_111.cubicTo(
        size.width * 0.3744366,
        size.height * 0.01441745,
        size.width * 0.3664435,
        size.height * 0.02078265,
        size.width * 0.3664435,
        size.height * 0.02642912);
    path_111.cubicTo(
        size.width * 0.3664435,
        size.height * 0.03207559,
        size.width * 0.3744366,
        size.height * 0.03844078,
        size.width * 0.3858679,
        size.height * 0.03844078);
    path_111.cubicTo(
        size.width * 0.3972985,
        size.height * 0.03844078,
        size.width * 0.4052924,
        size.height * 0.03207559,
        size.width * 0.4052924,
        size.height * 0.02642912);
    path_111.cubicTo(
        size.width * 0.4052924,
        size.height * 0.02078265,
        size.width * 0.3972985,
        size.height * 0.01441745,
        size.width * 0.3858679,
        size.height * 0.01441745);
    path_111.close();

    Paint paint_111_fill = Paint()..style = PaintingStyle.fill;
    paint_111_fill.color = Colors.black;
    canvas.drawPath(path_111, paint_111_fill);

    Path path_112 = Path();
    path_112.moveTo(size.width * 0.3853580, size.height * 0.03237333);
    path_112.cubicTo(
        size.width * 0.3911855,
        size.height * 0.03237333,
        size.width * 0.3959092,
        size.height * 0.02924775,
        size.width * 0.3959092,
        size.height * 0.02539225);
    path_112.cubicTo(
        size.width * 0.3959092,
        size.height * 0.02153667,
        size.width * 0.3911855,
        size.height * 0.01841108,
        size.width * 0.3853580,
        size.height * 0.01841108);
    path_112.cubicTo(
        size.width * 0.3795305,
        size.height * 0.01841108,
        size.width * 0.3748061,
        size.height * 0.02153667,
        size.width * 0.3748061,
        size.height * 0.02539225);
    path_112.cubicTo(
        size.width * 0.3748061,
        size.height * 0.02924775,
        size.width * 0.3795305,
        size.height * 0.03237333,
        size.width * 0.3853580,
        size.height * 0.03237333);
    path_112.close();

    Paint paint_112_fill = Paint()..style = PaintingStyle.fill;
    paint_112_fill.color = Colors.black;
    canvas.drawPath(path_112, paint_112_fill);

    Path path_113 = Path();
    path_113.moveTo(size.width * 0.3996824, size.height * 0.05325833);
    path_113.lineTo(size.width * 0.3996824, size.height * 0.08457078);
    path_113.cubicTo(
        size.width * 0.3996824,
        size.height * 0.08703471,
        size.width * 0.4006420,
        size.height * 0.08919059,
        size.width * 0.4022405,
        size.height * 0.09052520);
    path_113.cubicTo(
        size.width * 0.4077557,
        size.height * 0.09504245,
        size.width * 0.4216649,
        size.height * 0.1034608,
        size.width * 0.4394908,
        size.height * 0.09216784);
    path_113.cubicTo(
        size.width * 0.4413298,
        size.height * 0.09103853,
        size.width * 0.4425282,
        size.height * 0.08847196,
        size.width * 0.4425282,
        size.height * 0.08580275);
    path_113.lineTo(size.width * 0.4425282, size.height * 0.05315569);
    path_113.lineTo(size.width * 0.3996824, size.height * 0.05315569);
    path_113.lineTo(size.width * 0.3996824, size.height * 0.05325833);
    path_113.close();

    Paint paint_113_fill = Paint()..style = PaintingStyle.fill;
    paint_113_fill.color = Colors.black;
    canvas.drawPath(path_113, paint_113_fill);

    Path path_114 = Path();
    path_114.moveTo(size.width * 0.4216580, size.height * 0.1000206);
    path_114.cubicTo(
        size.width * 0.4119061,
        size.height * 0.1000206,
        size.width * 0.4046313,
        size.height * 0.09519529,
        size.width * 0.4011145,
        size.height * 0.09232069);
    path_114.cubicTo(
        size.width * 0.3989565,
        size.height * 0.09057539,
        size.width * 0.3976771,
        size.height * 0.08749549,
        size.width * 0.3976771,
        size.height * 0.08421029);
    path_114.lineTo(size.width * 0.3976771, size.height * 0.05033127);
    path_114.lineTo(size.width * 0.4445198, size.height * 0.05033127);
    path_114.lineTo(size.width * 0.4445198, size.height * 0.08554490);
    path_114.cubicTo(
        size.width * 0.4445198,
        size.height * 0.08913814,
        size.width * 0.4429214,
        size.height * 0.09252608,
        size.width * 0.4403634,
        size.height * 0.09416863);
    path_114.cubicTo(
        size.width * 0.4334885,
        size.height * 0.09848039,
        size.width * 0.4271740,
        size.height * 0.1000206,
        size.width * 0.4216580,
        size.height * 0.1000206);
    path_114.close();
    path_114.moveTo(size.width * 0.4016740, size.height * 0.05546441);
    path_114.lineTo(size.width * 0.4016740, size.height * 0.08421029);
    path_114.cubicTo(
        size.width * 0.4016740,
        size.height * 0.08575029,
        size.width * 0.4023137,
        size.height * 0.08718755,
        size.width * 0.4032725,
        size.height * 0.08800882);
    path_114.cubicTo(
        size.width * 0.4082290,
        size.height * 0.09211539,
        size.width * 0.4214985,
        size.height * 0.1003284,
        size.width * 0.4385244,
        size.height * 0.08954882);
    path_114.cubicTo(
        size.width * 0.4397237,
        size.height * 0.08883020,
        size.width * 0.4404435,
        size.height * 0.08718755,
        size.width * 0.4404435,
        size.height * 0.08554490);
    path_114.lineTo(size.width * 0.4404435, size.height * 0.05546441);
    path_114.lineTo(size.width * 0.4016740, size.height * 0.05546441);
    path_114.close();

    Paint paint_114_fill = Paint()..style = PaintingStyle.fill;
    paint_114_fill.color = Colors.black;
    canvas.drawPath(path_114, paint_114_fill);

    Path path_115 = Path();
    path_115.moveTo(size.width * 0.4210832, size.height * 0.06713716);
    path_115.cubicTo(
        size.width * 0.4329145,
        size.height * 0.06713716,
        size.width * 0.4425061,
        size.height * 0.06061020,
        size.width * 0.4425061,
        size.height * 0.05255882);
    path_115.cubicTo(
        size.width * 0.4425061,
        size.height * 0.04450755,
        size.width * 0.4329145,
        size.height * 0.03798059,
        size.width * 0.4210832,
        size.height * 0.03798059);
    path_115.cubicTo(
        size.width * 0.4092511,
        size.height * 0.03798059,
        size.width * 0.3996603,
        size.height * 0.04450755,
        size.width * 0.3996603,
        size.height * 0.05255882);
    path_115.cubicTo(
        size.width * 0.3996603,
        size.height * 0.06061020,
        size.width * 0.4092511,
        size.height * 0.06713716,
        size.width * 0.4210832,
        size.height * 0.06713716);
    path_115.close();

    Paint paint_115_fill = Paint()..style = PaintingStyle.fill;
    paint_115_fill.color = Colors.white;
    canvas.drawPath(path_115, paint_115_fill);

    Path path_116 = Path();
    path_116.moveTo(size.width * 0.4210840, size.height * 0.06951294);
    path_116.cubicTo(
        size.width * 0.4079740,
        size.height * 0.06951294,
        size.width * 0.3976626,
        size.height * 0.06201853,
        size.width * 0.3976626,
        size.height * 0.05236814);
    path_116.cubicTo(
        size.width * 0.3976626,
        size.height * 0.04271775,
        size.width * 0.4079740,
        size.height * 0.03522324,
        size.width * 0.4210840,
        size.height * 0.03522324);
    path_116.cubicTo(
        size.width * 0.4341931,
        size.height * 0.03522324,
        size.width * 0.4445053,
        size.height * 0.04271775,
        size.width * 0.4445053,
        size.height * 0.05236814);
    path_116.cubicTo(
        size.width * 0.4445053,
        size.height * 0.06201853,
        size.width * 0.4341931,
        size.height * 0.06951294,
        size.width * 0.4210840,
        size.height * 0.06951294);
    path_116.close();
    path_116.moveTo(size.width * 0.4210840, size.height * 0.04045912);
    path_116.cubicTo(
        size.width * 0.4096527,
        size.height * 0.04045912,
        size.width * 0.4016588,
        size.height * 0.04682422,
        size.width * 0.4016588,
        size.height * 0.05247078);
    path_116.cubicTo(
        size.width * 0.4016588,
        size.height * 0.05811725,
        size.width * 0.4096527,
        size.height * 0.06448245,
        size.width * 0.4210840,
        size.height * 0.06448245);
    path_116.cubicTo(
        size.width * 0.4325145,
        size.height * 0.06448245,
        size.width * 0.4405084,
        size.height * 0.05811725,
        size.width * 0.4405084,
        size.height * 0.05247078);
    path_116.cubicTo(
        size.width * 0.4405084,
        size.height * 0.04682422,
        size.width * 0.4325145,
        size.height * 0.04045912,
        size.width * 0.4210840,
        size.height * 0.04045912);
    path_116.close();

    Paint paint_116_fill = Paint()..style = PaintingStyle.fill;
    paint_116_fill.color = Colors.black;
    canvas.drawPath(path_116, paint_116_fill);

    Path path_117 = Path();
    path_117.moveTo(size.width * 0.4205809, size.height * 0.05915216);
    path_117.cubicTo(
        size.width * 0.4264084,
        size.height * 0.05915216,
        size.width * 0.4311328,
        size.height * 0.05602667,
        size.width * 0.4311328,
        size.height * 0.05217108);
    path_117.cubicTo(
        size.width * 0.4311328,
        size.height * 0.04831549,
        size.width * 0.4264084,
        size.height * 0.04519000,
        size.width * 0.4205809,
        size.height * 0.04519000);
    path_117.cubicTo(
        size.width * 0.4147534,
        size.height * 0.04519000,
        size.width * 0.4100298,
        size.height * 0.04831549,
        size.width * 0.4100298,
        size.height * 0.05217108);
    path_117.cubicTo(
        size.width * 0.4100298,
        size.height * 0.05602667,
        size.width * 0.4147534,
        size.height * 0.05915216,
        size.width * 0.4205809,
        size.height * 0.05915216);
    path_117.close();

    Paint paint_117_fill = Paint()..style = PaintingStyle.fill;
    paint_117_fill.color = Colors.black;
    canvas.drawPath(path_117, paint_117_fill);

    Path path_118 = Path();
    path_118.moveTo(size.width * 0.4480107, size.height * 0.04914765);
    path_118.lineTo(size.width * 0.4476908, size.height * 0.04914765);
    path_118.cubicTo(
        size.width * 0.4468916,
        size.height * 0.04914765,
        size.width * 0.4462519,
        size.height * 0.04832637,
        size.width * 0.4462519,
        size.height * 0.04729971);
    path_118.lineTo(size.width * 0.4462519, size.height * 0.03816265);
    path_118.cubicTo(
        size.width * 0.4462519,
        size.height * 0.03713598,
        size.width * 0.4468916,
        size.height * 0.03631471,
        size.width * 0.4476908,
        size.height * 0.03631471);
    path_118.lineTo(size.width * 0.4480107, size.height * 0.03631471);
    path_118.cubicTo(
        size.width * 0.4488099,
        size.height * 0.03631471,
        size.width * 0.4494496,
        size.height * 0.03713598,
        size.width * 0.4494496,
        size.height * 0.03816265);
    path_118.lineTo(size.width * 0.4494496, size.height * 0.04729971);
    path_118.cubicTo(
        size.width * 0.4493695,
        size.height * 0.04832637,
        size.width * 0.4488099,
        size.height * 0.04914765,
        size.width * 0.4480107,
        size.height * 0.04914765);
    path_118.close();

    Paint paint_118_fill = Paint()..style = PaintingStyle.fill;
    paint_118_fill.color = Colors.white;
    canvas.drawPath(path_118, paint_118_fill);

    Path path_119 = Path();
    path_119.moveTo(size.width * 0.4479580, size.height * 0.03350980);
    path_119.lineTo(size.width * 0.4476382, size.height * 0.03350980);
    path_119.cubicTo(
        size.width * 0.4468389,
        size.height * 0.03350980,
        size.width * 0.4461992,
        size.height * 0.03268843,
        size.width * 0.4461992,
        size.height * 0.03166176);
    path_119.cubicTo(
        size.width * 0.4461992,
        size.height * 0.03063520,
        size.width * 0.4468389,
        size.height * 0.02981392,
        size.width * 0.4476382,
        size.height * 0.02981392);
    path_119.lineTo(size.width * 0.4479580, size.height * 0.02981392);
    path_119.cubicTo(
        size.width * 0.4487573,
        size.height * 0.02981392,
        size.width * 0.4493969,
        size.height * 0.03063520,
        size.width * 0.4493969,
        size.height * 0.03166176);
    path_119.cubicTo(
        size.width * 0.4493168,
        size.height * 0.03279108,
        size.width * 0.4487573,
        size.height * 0.03350980,
        size.width * 0.4479580,
        size.height * 0.03350980);
    path_119.close();

    Paint paint_119_fill = Paint()..style = PaintingStyle.fill;
    paint_119_fill.color = Colors.white;
    canvas.drawPath(path_119, paint_119_fill);

    Path path_120 = Path();
    path_120.moveTo(size.width * 0.4378573, size.height * 0.08811441);
    path_120.lineTo(size.width * 0.4375374, size.height * 0.08811441);
    path_120.cubicTo(
        size.width * 0.4367382,
        size.height * 0.08811441,
        size.width * 0.4360985,
        size.height * 0.08729314,
        size.width * 0.4360985,
        size.height * 0.08626647);
    path_120.lineTo(size.width * 0.4360985, size.height * 0.07712941);
    path_120.cubicTo(
        size.width * 0.4360985,
        size.height * 0.07610275,
        size.width * 0.4367382,
        size.height * 0.07528147,
        size.width * 0.4375374,
        size.height * 0.07528147);
    path_120.lineTo(size.width * 0.4378573, size.height * 0.07528147);
    path_120.cubicTo(
        size.width * 0.4386565,
        size.height * 0.07528147,
        size.width * 0.4392962,
        size.height * 0.07610275,
        size.width * 0.4392962,
        size.height * 0.07712941);
    path_120.lineTo(size.width * 0.4392962, size.height * 0.08626647);
    path_120.cubicTo(
        size.width * 0.4392962,
        size.height * 0.08729314,
        size.width * 0.4386565,
        size.height * 0.08811441,
        size.width * 0.4378573,
        size.height * 0.08811441);
    path_120.close();

    Paint paint_120_fill = Paint()..style = PaintingStyle.fill;
    paint_120_fill.color = Colors.white;
    canvas.drawPath(path_120, paint_120_fill);

    Path path_121 = Path();
    path_121.moveTo(size.width * 0.4378573, size.height * 0.07226588);
    path_121.lineTo(size.width * 0.4375374, size.height * 0.07226588);
    path_121.cubicTo(
        size.width * 0.4367382,
        size.height * 0.07226588,
        size.width * 0.4360985,
        size.height * 0.07144461,
        size.width * 0.4360985,
        size.height * 0.07041794);
    path_121.cubicTo(
        size.width * 0.4360985,
        size.height * 0.06939127,
        size.width * 0.4367382,
        size.height * 0.06857000,
        size.width * 0.4375374,
        size.height * 0.06857000);
    path_121.lineTo(size.width * 0.4378573, size.height * 0.06857000);
    path_121.cubicTo(
        size.width * 0.4386565,
        size.height * 0.06857000,
        size.width * 0.4392962,
        size.height * 0.06939127,
        size.width * 0.4392962,
        size.height * 0.07041794);
    path_121.cubicTo(
        size.width * 0.4392962,
        size.height * 0.07154725,
        size.width * 0.4386565,
        size.height * 0.07226588,
        size.width * 0.4378573,
        size.height * 0.07226588);
    path_121.close();

    Paint paint_121_fill = Paint()..style = PaintingStyle.fill;
    paint_121_fill.color = Colors.white;
    canvas.drawPath(path_121, paint_121_fill);

    Path path_122 = Path();
    path_122.moveTo(size.width * 0.3678733, size.height * 0.03631471);
    path_122.lineTo(size.width * 0.3681931, size.height * 0.03631471);
    path_122.cubicTo(
        size.width * 0.3689924,
        size.height * 0.03631471,
        size.width * 0.3696321,
        size.height * 0.03713608,
        size.width * 0.3696321,
        size.height * 0.03816265);
    path_122.lineTo(size.width * 0.3696321, size.height * 0.04729971);
    path_122.cubicTo(
        size.width * 0.3696321,
        size.height * 0.04832637,
        size.width * 0.3689924,
        size.height * 0.04914765,
        size.width * 0.3681931,
        size.height * 0.04914765);
    path_122.lineTo(size.width * 0.3678733, size.height * 0.04914765);
    path_122.cubicTo(
        size.width * 0.3670740,
        size.height * 0.04914765,
        size.width * 0.3664344,
        size.height * 0.04832637,
        size.width * 0.3664344,
        size.height * 0.04729971);
    path_122.lineTo(size.width * 0.3664344, size.height * 0.03816265);
    path_122.cubicTo(
        size.width * 0.3665145,
        size.height * 0.03713608,
        size.width * 0.3670740,
        size.height * 0.03631471,
        size.width * 0.3678733,
        size.height * 0.03631471);
    path_122.close();

    Paint paint_122_fill = Paint()..style = PaintingStyle.fill;
    paint_122_fill.color = Colors.white;
    canvas.drawPath(path_122, paint_122_fill);

    Path path_123 = Path();
    path_123.moveTo(size.width * 0.3678359, size.height * 0.05186314);
    path_123.lineTo(size.width * 0.3681557, size.height * 0.05186314);
    path_123.cubicTo(
        size.width * 0.3689550,
        size.height * 0.05186314,
        size.width * 0.3695947,
        size.height * 0.05268441,
        size.width * 0.3695947,
        size.height * 0.05371108);
    path_123.cubicTo(
        size.width * 0.3695947,
        size.height * 0.05473775,
        size.width * 0.3689550,
        size.height * 0.05555902,
        size.width * 0.3681557,
        size.height * 0.05555902);
    path_123.lineTo(size.width * 0.3678359, size.height * 0.05555902);
    path_123.cubicTo(
        size.width * 0.3670366,
        size.height * 0.05555902,
        size.width * 0.3663969,
        size.height * 0.05473775,
        size.width * 0.3663969,
        size.height * 0.05371108);
    path_123.cubicTo(
        size.width * 0.3664771,
        size.height * 0.05268441,
        size.width * 0.3670366,
        size.height * 0.05186314,
        size.width * 0.3678359,
        size.height * 0.05186314);
    path_123.close();

    Paint paint_123_fill = Paint()..style = PaintingStyle.fill;
    paint_123_fill.color = Colors.white;
    canvas.drawPath(path_123, paint_123_fill);

    Path path_124 = Path();
    path_124.moveTo(size.width * 0.3667656, size.height * 0.1305382);
    path_124.lineTo(size.width * 0.4873107, size.height * 0.1013814);
    path_124.cubicTo(
        size.width * 0.4905878,
        size.height * 0.1005608,
        size.width * 0.4926664,
        size.height * 0.09655667,
        size.width * 0.4919466,
        size.height * 0.09234745);
    path_124.cubicTo(
        size.width * 0.4912275,
        size.height * 0.08824088,
        size.width * 0.4880298,
        size.height * 0.08557167,
        size.width * 0.4848321,
        size.height * 0.08659824);
    path_124.lineTo(size.width * 0.3657267, size.height * 0.1241735);
    path_124.cubicTo(
        size.width * 0.3643679,
        size.height * 0.1245843,
        size.width * 0.3635687,
        size.height * 0.1263294,
        size.width * 0.3638885,
        size.height * 0.1279716);
    path_124.cubicTo(
        size.width * 0.3640481,
        size.height * 0.1298196,
        size.width * 0.3654069,
        size.height * 0.1309490,
        size.width * 0.3667656,
        size.height * 0.1305382);
    path_124.close();

    Paint paint_124_fill = Paint()..style = PaintingStyle.fill;
    paint_124_fill.color = Colors.white;
    canvas.drawPath(path_124, paint_124_fill);

    Path path_125 = Path();
    path_125.moveTo(size.width * 0.3663473, size.height * 0.1332392);
    path_125.cubicTo(
        size.width * 0.3642695,
        size.height * 0.1332392,
        size.width * 0.3624305,
        size.height * 0.1313912,
        size.width * 0.3619511,
        size.height * 0.1286196);
    path_125.cubicTo(
        size.width * 0.3613916,
        size.height * 0.1255392,
        size.width * 0.3629107,
        size.height * 0.1224598,
        size.width * 0.3653084,
        size.height * 0.1217412);
    path_125.lineTo(size.width * 0.4844137, size.height * 0.08416578);
    path_125.cubicTo(
        size.width * 0.4864924,
        size.height * 0.08354980,
        size.width * 0.4886504,
        size.height * 0.08396049,
        size.width * 0.4904893,
        size.height * 0.08539775);
    path_125.cubicTo(
        size.width * 0.4923275,
        size.height * 0.08683510,
        size.width * 0.4935267,
        size.height * 0.08909373,
        size.width * 0.4940061,
        size.height * 0.09186559);
    path_125.cubicTo(
        size.width * 0.4944863,
        size.height * 0.09453490,
        size.width * 0.4940863,
        size.height * 0.09730676,
        size.width * 0.4928870,
        size.height * 0.09956569);
    path_125.cubicTo(
        size.width * 0.4916885,
        size.height * 0.1018235,
        size.width * 0.4898496,
        size.height * 0.1033637,
        size.width * 0.4877718,
        size.height * 0.1038775);
    path_125.lineTo(size.width * 0.3672267, size.height * 0.1330333);
    path_125.cubicTo(
        size.width * 0.3669069,
        size.height * 0.1332392,
        size.width * 0.3665878,
        size.height * 0.1332392,
        size.width * 0.3663473,
        size.height * 0.1332392);
    path_125.close();
    path_125.moveTo(size.width * 0.4862527, size.height * 0.08909373);
    path_125.cubicTo(
        size.width * 0.4859328,
        size.height * 0.08909373,
        size.width * 0.4856130,
        size.height * 0.08909373,
        size.width * 0.4852931,
        size.height * 0.08919637);
    path_125.lineTo(size.width * 0.3661878, size.height * 0.1267716);
    path_125.cubicTo(
        size.width * 0.3658679,
        size.height * 0.1268735,
        size.width * 0.3657084,
        size.height * 0.1271824,
        size.width * 0.3657878,
        size.height * 0.1275922);
    path_125.cubicTo(
        size.width * 0.3658679,
        size.height * 0.1280029,
        size.width * 0.3661076,
        size.height * 0.1282088,
        size.width * 0.3664275,
        size.height * 0.1281059);
    path_125.lineTo(size.width * 0.4869718, size.height * 0.09894902);
    path_125.cubicTo(
        size.width * 0.4880115,
        size.height * 0.09874412,
        size.width * 0.4888908,
        size.height * 0.09792275,
        size.width * 0.4894504,
        size.height * 0.09679343);
    path_125.cubicTo(
        size.width * 0.4900099,
        size.height * 0.09566412,
        size.width * 0.4902496,
        size.height * 0.09432951,
        size.width * 0.4900099,
        size.height * 0.09299490);
    path_125.cubicTo(
        size.width * 0.4897695,
        size.height * 0.09166029,
        size.width * 0.4891305,
        size.height * 0.09053098,
        size.width * 0.4882511,
        size.height * 0.08981235);
    path_125.cubicTo(
        size.width * 0.4876115,
        size.height * 0.08929902,
        size.width * 0.4869718,
        size.height * 0.08909373,
        size.width * 0.4862527,
        size.height * 0.08909373);
    path_125.close();

    Paint paint_125_fill = Paint()..style = PaintingStyle.fill;
    paint_125_fill.color = Colors.black;
    canvas.drawPath(path_125, paint_125_fill);

    Path path_126 = Path();
    path_126.moveTo(size.width * 0.3846611, size.height * 0.1154843);
    path_126.lineTo(size.width * 0.5024084, size.height * 0.07154412);
    path_126.cubicTo(
        size.width * 0.5056053,
        size.height * 0.07031216,
        size.width * 0.5073641,
        size.height * 0.06600020,
        size.width * 0.5064046,
        size.height * 0.06199637);
    path_126.cubicTo(
        size.width * 0.5053656,
        size.height * 0.05799245,
        size.width * 0.5020084,
        size.height * 0.05573382,
        size.width * 0.4988908,
        size.height * 0.05717118);
    path_126.lineTo(size.width * 0.3831427, size.height * 0.1092216);
    path_126.cubicTo(
        size.width * 0.3818634,
        size.height * 0.1098373,
        size.width * 0.3811443,
        size.height * 0.1115833,
        size.width * 0.3815435,
        size.height * 0.1132255);
    path_126.cubicTo(
        size.width * 0.3819435,
        size.height * 0.1150735,
        size.width * 0.3833023,
        size.height * 0.1159971,
        size.width * 0.3846611,
        size.height * 0.1154843);
    path_126.close();

    Paint paint_126_fill = Paint()..style = PaintingStyle.fill;
    paint_126_fill.color = Colors.white;
    canvas.drawPath(path_126, paint_126_fill);

    Path path_127 = Path();
    path_127.moveTo(size.width * 0.3840046, size.height * 0.1182676);
    path_127.cubicTo(
        size.width * 0.3820863,
        size.height * 0.1182676,
        size.width * 0.3803275,
        size.height * 0.1167275,
        size.width * 0.3796885,
        size.height * 0.1142637);
    path_127.cubicTo(
        size.width * 0.3789687,
        size.height * 0.1112863,
        size.width * 0.3801679,
        size.height * 0.1081039,
        size.width * 0.3824863,
        size.height * 0.1069745);
    path_127.lineTo(size.width * 0.4982344, size.height * 0.05482127);
    path_127.cubicTo(
        size.width * 0.5002328,
        size.height * 0.05389735,
        size.width * 0.5024710,
        size.height * 0.05410265,
        size.width * 0.5043092,
        size.height * 0.05533461);
    path_127.cubicTo(
        size.width * 0.5062282,
        size.height * 0.05656657,
        size.width * 0.5076664,
        size.height * 0.05872255,
        size.width * 0.5083061,
        size.height * 0.06128912);
    path_127.cubicTo(
        size.width * 0.5089458,
        size.height * 0.06395833,
        size.width * 0.5087855,
        size.height * 0.06673029,
        size.width * 0.5077466,
        size.height * 0.06909157);
    path_127.cubicTo(
        size.width * 0.5067076,
        size.height * 0.07155549,
        size.width * 0.5050290,
        size.height * 0.07330078,
        size.width * 0.5030305,
        size.height * 0.07401941);
    path_127.lineTo(size.width * 0.3852840, size.height * 0.1179598);
    path_127.cubicTo(
        size.width * 0.3848038,
        size.height * 0.1182676,
        size.width * 0.3844046,
        size.height * 0.1182676,
        size.width * 0.3840046,
        size.height * 0.1182676);
    path_127.close();
    path_127.moveTo(size.width * 0.5007924, size.height * 0.05944118);
    path_127.cubicTo(
        size.width * 0.5003924,
        size.height * 0.05944118,
        size.width * 0.4999130,
        size.height * 0.05954382,
        size.width * 0.4995130,
        size.height * 0.05974922);
    path_127.lineTo(size.width * 0.3837649, size.height * 0.1119020);
    path_127.cubicTo(
        size.width * 0.3835252,
        size.height * 0.1120049,
        size.width * 0.3833656,
        size.height * 0.1124157,
        size.width * 0.3834450,
        size.height * 0.1127235);
    path_127.cubicTo(
        size.width * 0.3835252,
        size.height * 0.1130314,
        size.width * 0.3838450,
        size.height * 0.1132373,
        size.width * 0.3840847,
        size.height * 0.1131343);
    path_127.lineTo(size.width * 0.5018313, size.height * 0.06919422);
    path_127.cubicTo(
        size.width * 0.5028702,
        size.height * 0.06878353,
        size.width * 0.5036702,
        size.height * 0.06796225,
        size.width * 0.5041496,
        size.height * 0.06673029);
    path_127.cubicTo(
        size.width * 0.5046290,
        size.height * 0.06549833,
        size.width * 0.5047092,
        size.height * 0.06416373,
        size.width * 0.5043893,
        size.height * 0.06282912);
    path_127.cubicTo(
        size.width * 0.5040695,
        size.height * 0.06149441,
        size.width * 0.5033504,
        size.height * 0.06046784,
        size.width * 0.5023908,
        size.height * 0.05985186);
    path_127.cubicTo(
        size.width * 0.5019916,
        size.height * 0.05964647,
        size.width * 0.5014321,
        size.height * 0.05944118,
        size.width * 0.5007924,
        size.height * 0.05944118);
    path_127.close();

    Paint paint_127_fill = Paint()..style = PaintingStyle.fill;
    paint_127_fill.color = Colors.black;
    canvas.drawPath(path_127, paint_127_fill);

    Path path_128 = Path();
    path_128.moveTo(size.width * 0.7500099, size.height * 0.2530853);
    path_128.cubicTo(
        size.width * 0.7309847,
        size.height * 0.1993922,
        size.width * 0.6939740,
        size.height * 0.1710569,
        size.width * 0.6768672,
        size.height * 0.1602775);
    path_128.cubicTo(
        size.width * 0.6719916,
        size.height * 0.1571971,
        size.width * 0.6660763,
        size.height * 0.1600716,
        size.width * 0.6639977,
        size.height * 0.1664373);
    path_128.cubicTo(
        size.width * 0.6622389,
        size.height * 0.1717755,
        size.width * 0.6635977,
        size.height * 0.1778333,
        size.width * 0.6671153,
        size.height * 0.1811176);
    path_128.lineTo(size.width * 0.6271466, size.height * 0.3006186);
    path_128.lineTo(size.width * 0.6317031, size.height * 0.3095500);
    path_128.cubicTo(
        size.width * 0.6333023,
        size.height * 0.3126304,
        size.width * 0.6363397,
        size.height * 0.3139647,
        size.width * 0.6390573,
        size.height * 0.3126304);
    path_128.lineTo(size.width * 0.7342618, size.height * 0.2677667);
    path_128.cubicTo(
        size.width * 0.7372198,
        size.height * 0.2713598,
        size.width * 0.7417763,
        size.height * 0.2722833,
        size.width * 0.7456130,
        size.height * 0.2697167);
    path_128.cubicTo(
        size.width * 0.7502496,
        size.height * 0.2667392,
        size.width * 0.7521679,
        size.height * 0.2593480,
        size.width * 0.7500099,
        size.height * 0.2530853);
    path_128.close();

    Paint paint_128_fill = Paint()..style = PaintingStyle.fill;
    paint_128_fill.color = Colors.black;
    canvas.drawPath(path_128, paint_128_fill);

    Path path_129 = Path();
    path_129.moveTo(size.width * 0.6368710, size.height * 0.3158333);
    path_129.cubicTo(
        size.width * 0.6341534,
        size.height * 0.3158333,
        size.width * 0.6315954,
        size.height * 0.3140882,
        size.width * 0.6299969,
        size.height * 0.3110078);
    path_129.lineTo(size.width * 0.6248809, size.height * 0.3009471);
    path_129.lineTo(size.width * 0.6646893, size.height * 0.1818569);
    path_129.cubicTo(
        size.width * 0.6613321,
        size.height * 0.1776480,
        size.width * 0.6602924,
        size.height * 0.1710775,
        size.width * 0.6621313,
        size.height * 0.1654304);
    path_129.cubicTo(
        size.width * 0.6634099,
        size.height * 0.1616324,
        size.width * 0.6657282,
        size.height * 0.1588598,
        size.width * 0.6686863,
        size.height * 0.1574225);
    path_129.cubicTo(
        size.width * 0.6716435,
        size.height * 0.1559853,
        size.width * 0.6749214,
        size.height * 0.1561912,
        size.width * 0.6777191,
        size.height * 0.1580392);
    path_129.cubicTo(
        size.width * 0.6934664,
        size.height * 0.1678951,
        size.width * 0.7322359,
        size.height * 0.1968461,
        size.width * 0.7518198,
        size.height * 0.2520794);
    path_129.cubicTo(
        size.width * 0.7544580,
        size.height * 0.2595735,
        size.width * 0.7521397,
        size.height * 0.2684029,
        size.width * 0.7464641,
        size.height * 0.2720980);
    path_129.cubicTo(
        size.width * 0.7423878,
        size.height * 0.2747676,
        size.width * 0.7374313,
        size.height * 0.2742539,
        size.width * 0.7338344,
        size.height * 0.2707637);
    path_129.lineTo(size.width * 0.6397489, size.height * 0.3151147);
    path_129.cubicTo(
        size.width * 0.6387901,
        size.height * 0.3156275,
        size.width * 0.6378305,
        size.height * 0.3158333,
        size.width * 0.6368710,
        size.height * 0.3158333);
    path_129.close();
    path_129.moveTo(size.width * 0.6293573, size.height * 0.3004333);
    path_129.lineTo(size.width * 0.6332740, size.height * 0.3081333);
    path_129.cubicTo(
        size.width * 0.6343931,
        size.height * 0.3102892,
        size.width * 0.6363916,
        size.height * 0.3111108,
        size.width * 0.6382305,
        size.height * 0.3102892);
    path_129.lineTo(size.width * 0.7345534, size.height * 0.2649118);
    path_129.lineTo(size.width * 0.7354328, size.height * 0.2659382);
    path_129.cubicTo(
        size.width * 0.7379115,
        size.height * 0.2688127,
        size.width * 0.7415084,
        size.height * 0.2694294,
        size.width * 0.7444656,
        size.height * 0.2674784);
    path_129.cubicTo(
        size.width * 0.7482229,
        size.height * 0.2650147,
        size.width * 0.7498214,
        size.height * 0.2590598,
        size.width * 0.7480634,
        size.height * 0.2541324);
    path_129.cubicTo(
        size.width * 0.7290382,
        size.height * 0.2004392,
        size.width * 0.6912282,
        size.height * 0.1722069,
        size.width * 0.6758802,
        size.height * 0.1626588);
    path_129.cubicTo(
        size.width * 0.6740420,
        size.height * 0.1615294,
        size.width * 0.6718832,
        size.height * 0.1613245,
        size.width * 0.6699649,
        size.height * 0.1622480);
    path_129.cubicTo(
        size.width * 0.6680466,
        size.height * 0.1631725,
        size.width * 0.6665275,
        size.height * 0.1650206,
        size.width * 0.6657282,
        size.height * 0.1674843);
    path_129.cubicTo(
        size.width * 0.6643695,
        size.height * 0.1715902,
        size.width * 0.6654084,
        size.height * 0.1764157,
        size.width * 0.6682061,
        size.height * 0.1790853);
    path_129.lineTo(size.width * 0.6694855, size.height * 0.1803167);
    path_129.lineTo(size.width * 0.6293573, size.height * 0.3004333);
    path_129.close();

    Paint paint_129_fill = Paint()..style = PaintingStyle.fill;
    paint_129_fill.color = Colors.black;
    canvas.drawPath(path_129, paint_129_fill);

    Path path_130 = Path();
    path_130.moveTo(size.width * 0.7407992, size.height * 0.2531804);
    path_130.lineTo(size.width * 0.6350435, size.height * 0.3037931);
    path_130.cubicTo(
        size.width * 0.6296076,
        size.height * 0.3063598,
        size.width * 0.6243313,
        size.height * 0.2994814,
        size.width * 0.6264099,
        size.height * 0.2926029);
    path_130.lineTo(size.width * 0.6299275, size.height * 0.2809000);
    path_130.lineTo(size.width * 0.6407985, size.height * 0.2449676);
    path_130.lineTo(size.width * 0.6503107, size.height * 0.2135520);
    path_130.lineTo(size.width * 0.6603031, size.height * 0.1807000);
    path_130.lineTo(size.width * 0.6669382, size.height * 0.1587294);
    path_130.cubicTo(
        size.width * 0.7207351,
        size.height * 0.2009245,
        size.width * 0.7407992,
        size.height * 0.2531804,
        size.width * 0.7407992,
        size.height * 0.2531804);
    path_130.close();

    Paint paint_130_fill = Paint()..style = PaintingStyle.fill;
    paint_130_fill.color = Colors.white;
    canvas.drawPath(path_130, paint_130_fill);

    Path path_131 = Path();
    path_131.moveTo(size.width * 0.6326374, size.height * 0.3066608);
    path_131.cubicTo(
        size.width * 0.6303191,
        size.height * 0.3066608,
        size.width * 0.6281603,
        size.height * 0.3055314,
        size.width * 0.6264817,
        size.height * 0.3032725);
    path_131.cubicTo(
        size.width * 0.6240038,
        size.height * 0.3000902,
        size.width * 0.6232847,
        size.height * 0.2954706,
        size.width * 0.6245634,
        size.height * 0.2913637);
    path_131.lineTo(size.width * 0.6660504, size.height * 0.1547176);
    path_131.lineTo(size.width * 0.6680489, size.height * 0.1563608);
    path_131.cubicTo(
        size.width * 0.7216069,
        size.height * 0.1983500,
        size.width * 0.7423901,
        size.height * 0.2512216,
        size.width * 0.7425496,
        size.height * 0.2518382);
    path_131.lineTo(size.width * 0.7435092, size.height * 0.2544049);
    path_131.lineTo(size.width * 0.6356748, size.height * 0.3060441);
    path_131.cubicTo(
        size.width * 0.6347153,
        size.height * 0.3064549,
        size.width * 0.6336763,
        size.height * 0.3066608,
        size.width * 0.6326374,
        size.height * 0.3066608);
    path_131.close();
    path_131.moveTo(size.width * 0.6680489, size.height * 0.1624176);
    path_131.lineTo(size.width * 0.6283206, size.height * 0.2933137);
    path_131.cubicTo(
        size.width * 0.6276008,
        size.height * 0.2955725,
        size.width * 0.6280008,
        size.height * 0.2980363,
        size.width * 0.6293595,
        size.height * 0.2997824);
    path_131.cubicTo(
        size.width * 0.6307183,
        size.height * 0.3015275,
        size.width * 0.6325573,
        size.height * 0.3020402,
        size.width * 0.6343954,
        size.height * 0.3012196);
    path_131.lineTo(size.width * 0.7379939, size.height * 0.2516324);
    path_131.cubicTo(
        size.width * 0.7332771,
        size.height * 0.2408529,
        size.width * 0.7124939,
        size.height * 0.1980422,
        size.width * 0.6680489,
        size.height * 0.1624176);
    path_131.close();

    Paint paint_131_fill = Paint()..style = PaintingStyle.fill;
    paint_131_fill.color = Colors.black;
    canvas.drawPath(path_131, paint_131_fill);

    Path path_132 = Path();
    path_132.moveTo(size.width * 0.6729359, size.height * 0.1507941);
    path_132.cubicTo(
        size.width * 0.6901221,
        size.height * 0.1615745,
        size.width * 0.7270534,
        size.height * 0.1899098,
        size.width * 0.7460779,
        size.height * 0.2436029);
    path_132.cubicTo(
        size.width * 0.7483160,
        size.height * 0.2498647,
        size.width * 0.7463183,
        size.height * 0.2571539,
        size.width * 0.7416015,
        size.height * 0.2603373);
    path_132.cubicTo(
        size.width * 0.7366458,
        size.height * 0.2636216,
        size.width * 0.7305702,
        size.height * 0.2610559,
        size.width * 0.7280924,
        size.height * 0.2546902);
    path_132.cubicTo(
        size.width * 0.7205786,
        size.height * 0.2353892,
        size.width * 0.7017931,
        size.height * 0.1969931,
        size.width * 0.6643832,
        size.height * 0.1726618);
    path_132.cubicTo(
        size.width * 0.6599061,
        size.height * 0.1697873,
        size.width * 0.6580679,
        size.height * 0.1629088,
        size.width * 0.6599863,
        size.height * 0.1570569);
    path_132.cubicTo(
        size.width * 0.6621450,
        size.height * 0.1504863,
        size.width * 0.6680603,
        size.height * 0.1477147,
        size.width * 0.6729359,
        size.height * 0.1507941);
    path_132.close();

    Paint paint_132_fill = Paint()..style = PaintingStyle.fill;
    paint_132_fill.color = Colors.white;
    canvas.drawPath(path_132, paint_132_fill);

    Path path_133 = Path();
    path_133.moveTo(size.width * 0.7369603, size.height * 0.2642559);
    path_133.cubicTo(
        size.width * 0.7356817,
        size.height * 0.2642559,
        size.width * 0.7344824,
        size.height * 0.2640500,
        size.width * 0.7332031,
        size.height * 0.2635373);
    path_133.cubicTo(
        size.width * 0.7301656,
        size.height * 0.2622020,
        size.width * 0.7276878,
        size.height * 0.2595333,
        size.width * 0.7262489,
        size.height * 0.2558373);
    path_133.cubicTo(
        size.width * 0.7184947,
        size.height * 0.2358176,
        size.width * 0.6999496,
        size.height * 0.1987559,
        size.width * 0.6634191,
        size.height * 0.1749382);
    path_133.cubicTo(
        size.width * 0.6580626,
        size.height * 0.1714480,
        size.width * 0.6557450,
        size.height * 0.1631324,
        size.width * 0.6580634,
        size.height * 0.1560480);
    path_133.cubicTo(
        size.width * 0.6593420,
        size.height * 0.1522500,
        size.width * 0.6616603,
        size.height * 0.1494775,
        size.width * 0.6646176,
        size.height * 0.1480402);
    path_133.cubicTo(
        size.width * 0.6675756,
        size.height * 0.1466029,
        size.width * 0.6708527,
        size.height * 0.1468088,
        size.width * 0.6736504,
        size.height * 0.1486559);
    path_133.cubicTo(
        size.width * 0.6893985,
        size.height * 0.1585118,
        size.width * 0.7281672,
        size.height * 0.1874627,
        size.width * 0.7477519,
        size.height * 0.2426961);
    path_133.cubicTo(
        size.width * 0.7503901,
        size.height * 0.2501912,
        size.width * 0.7480718,
        size.height * 0.2590196,
        size.width * 0.7423962,
        size.height * 0.2627157);
    path_133.cubicTo(
        size.width * 0.7407176,
        size.height * 0.2637422,
        size.width * 0.7388786,
        size.height * 0.2642559,
        size.width * 0.7369603,
        size.height * 0.2642559);
    path_133.close();
    path_133.moveTo(size.width * 0.6686947, size.height * 0.1521471);
    path_133.cubicTo(
        size.width * 0.6678153,
        size.height * 0.1521471,
        size.width * 0.6669359,
        size.height * 0.1523520,
        size.width * 0.6660565,
        size.height * 0.1527627);
    path_133.cubicTo(
        size.width * 0.6641382,
        size.height * 0.1536873,
        size.width * 0.6626191,
        size.height * 0.1555343,
        size.width * 0.6618198,
        size.height * 0.1579990);
    path_133.cubicTo(
        size.width * 0.6603015,
        size.height * 0.1626186,
        size.width * 0.6618198,
        size.height * 0.1680598,
        size.width * 0.6652573,
        size.height * 0.1703186);
    path_133.cubicTo(
        size.width * 0.7028275,
        size.height * 0.1947520,
        size.width * 0.7218527,
        size.height * 0.2329431,
        size.width * 0.7298458,
        size.height * 0.2534755);
    path_133.cubicTo(
        size.width * 0.7308053,
        size.height * 0.2559402,
        size.width * 0.7324840,
        size.height * 0.2577882,
        size.width * 0.7344824,
        size.height * 0.2586088);
    path_133.cubicTo(
        size.width * 0.7365611,
        size.height * 0.2594304,
        size.width * 0.7387191,
        size.height * 0.2592255,
        size.width * 0.7406374,
        size.height * 0.2579931);
    path_133.cubicTo(
        size.width * 0.7443947,
        size.height * 0.2555294,
        size.width * 0.7459931,
        size.height * 0.2495745,
        size.width * 0.7442344,
        size.height * 0.2446471);
    path_133.cubicTo(
        size.width * 0.7252099,
        size.height * 0.1909539,
        size.width * 0.6874000,
        size.height * 0.1627216,
        size.width * 0.6720519,
        size.height * 0.1531735);
    path_133.cubicTo(
        size.width * 0.6709328,
        size.height * 0.1524549,
        size.width * 0.6698137,
        size.height * 0.1521471,
        size.width * 0.6686947,
        size.height * 0.1521471);
    path_133.close();

    Paint paint_133_fill = Paint()..style = PaintingStyle.fill;
    paint_133_fill.color = Colors.black;
    canvas.drawPath(path_133, paint_133_fill);

    Path path_134 = Path();
    path_134.moveTo(size.width * 0.6570115, size.height * 0.2656765);
    path_134.cubicTo(
        size.width * 0.6570115,
        size.height * 0.2770725,
        size.width * 0.6498168,
        size.height * 0.2864147,
        size.width * 0.6408641,
        size.height * 0.2864147);
    path_134.cubicTo(
        size.width * 0.6366275,
        size.height * 0.2864147,
        size.width * 0.6327901,
        size.height * 0.2843618,
        size.width * 0.6299130,
        size.height * 0.2809735);
    path_134.lineTo(size.width * 0.6407840, size.height * 0.2450412);
    path_134.cubicTo(
        size.width * 0.6498168,
        size.height * 0.2449392,
        size.width * 0.6570115,
        size.height * 0.2542814,
        size.width * 0.6570115,
        size.height * 0.2656765);
    path_134.close();

    Paint paint_134_fill = Paint()..style = PaintingStyle.fill;
    paint_134_fill.color = _activeColor;
    canvas.drawPath(path_134, paint_134_fill);

    Path path_135 = Path();
    path_135.moveTo(size.width * 0.6407947, size.height * 0.2887137);
    path_135.cubicTo(
        size.width * 0.6362382,
        size.height * 0.2887137,
        size.width * 0.6318420,
        size.height * 0.2865578,
        size.width * 0.6284840,
        size.height * 0.2825539);
    path_135.lineTo(size.width * 0.6274450, size.height * 0.2813216);
    path_135.lineTo(size.width * 0.6393557, size.height * 0.2422069);
    path_135.lineTo(size.width * 0.6407145, size.height * 0.2422069);
    path_135.cubicTo(
        size.width * 0.6507069,
        size.height * 0.2422069,
        size.width * 0.6588603,
        size.height * 0.2526784,
        size.width * 0.6588603,
        size.height * 0.2655118);
    path_135.cubicTo(
        size.width * 0.6589405,
        size.height * 0.2783441,
        size.width * 0.6507863,
        size.height * 0.2887137,
        size.width * 0.6407947,
        size.height * 0.2887137);
    path_135.close();
    path_135.moveTo(size.width * 0.6322412, size.height * 0.2798843);
    path_135.cubicTo(
        size.width * 0.6347191,
        size.height * 0.2822451,
        size.width * 0.6376771,
        size.height * 0.2835804,
        size.width * 0.6407947,
        size.height * 0.2835804);
    path_135.cubicTo(
        size.width * 0.6485481,
        size.height * 0.2835804,
        size.width * 0.6549435,
        size.height * 0.2754696,
        size.width * 0.6549435,
        size.height * 0.2654088);
    path_135.cubicTo(
        size.width * 0.6549435,
        size.height * 0.2559637,
        size.width * 0.6493481,
        size.height * 0.2482637,
        size.width * 0.6421534,
        size.height * 0.2473402);
    path_135.lineTo(size.width * 0.6322412, size.height * 0.2798843);
    path_135.close();

    Paint paint_135_fill = Paint()..style = PaintingStyle.fill;
    paint_135_fill.color = Colors.black;
    canvas.drawPath(path_135, paint_135_fill);

    Path path_136 = Path();
    path_136.moveTo(size.width * 0.6990160, size.height * 0.2402735);
    path_136.lineTo(size.width * 0.7054115, size.height * 0.2431480);
    path_136.cubicTo(
        size.width * 0.7070901,
        size.height * 0.2438667,
        size.width * 0.7081290,
        size.height * 0.2460225,
        size.width * 0.7079695,
        size.height * 0.2482814);
    path_136.cubicTo(
        size.width * 0.7078893,
        size.height * 0.2500265,
        size.width * 0.7074893,
        size.height * 0.2522853,
        size.width * 0.7066099,
        size.height * 0.2544412);
    path_136.cubicTo(
        size.width * 0.7058107,
        size.height * 0.2562882,
        size.width * 0.7042122,
        size.height * 0.2573157,
        size.width * 0.7025336,
        size.height * 0.2569049);
    path_136.lineTo(size.width * 0.6950992, size.height * 0.2551598);
    path_136.cubicTo(
        size.width * 0.6944595,
        size.height * 0.2550569,
        size.width * 0.6939000,
        size.height * 0.2555696,
        size.width * 0.6938206,
        size.height * 0.2563912);
    path_136.lineTo(size.width * 0.6934206, size.height * 0.2602922);
    path_136.cubicTo(
        size.width * 0.6932611,
        size.height * 0.2622431,
        size.width * 0.6918221,
        size.height * 0.2635775,
        size.width * 0.6903031,
        size.height * 0.2631676);
    path_136.cubicTo(
        size.width * 0.6852672,
        size.height * 0.2619353,
        size.width * 0.6748756,
        size.height * 0.2560833,
        size.width * 0.6781527,
        size.height * 0.2400676);
    path_136.cubicTo(
        size.width * 0.6797511,
        size.height * 0.2323676,
        size.width * 0.6879847,
        size.height * 0.2240520,
        size.width * 0.6980573,
        size.height * 0.2303147);
    path_136.cubicTo(
        size.width * 0.6992557,
        size.height * 0.2310333,
        size.width * 0.6998153,
        size.height * 0.2329843,
        size.width * 0.6993359,
        size.height * 0.2345235);
    path_136.lineTo(size.width * 0.6983771, size.height * 0.2378088);
    path_136.cubicTo(
        size.width * 0.6981366,
        size.height * 0.2392461,
        size.width * 0.6984565,
        size.height * 0.2400676,
        size.width * 0.6990160,
        size.height * 0.2402735);
    path_136.close();

    Paint paint_136_fill = Paint()..style = PaintingStyle.fill;
    paint_136_fill.color = Colors.white;
    canvas.drawPath(path_136, paint_136_fill);

    Path path_137 = Path();
    path_137.moveTo(size.width * 0.6907664, size.height * 0.2659461);
    path_137.cubicTo(
        size.width * 0.6904466,
        size.height * 0.2659461,
        size.width * 0.6902069,
        size.height * 0.2659461,
        size.width * 0.6898870,
        size.height * 0.2658441);
    path_137.cubicTo(
        size.width * 0.6858901,
        size.height * 0.2648167,
        size.width * 0.6805344,
        size.height * 0.2617373,
        size.width * 0.6776565,
        size.height * 0.2558853);
    path_137.cubicTo(
        size.width * 0.6754183,
        size.height * 0.2513686,
        size.width * 0.6749389,
        size.height * 0.2458245,
        size.width * 0.6762183,
        size.height * 0.2395618);
    path_137.cubicTo(
        size.width * 0.6772573,
        size.height * 0.2345314,
        size.width * 0.6803748,
        size.height * 0.2299118,
        size.width * 0.6844511,
        size.height * 0.2275500);
    path_137.cubicTo(
        size.width * 0.6889282,
        size.height * 0.2248814,
        size.width * 0.6940435,
        size.height * 0.2250863,
        size.width * 0.6989198,
        size.height * 0.2281657);
    path_137.cubicTo(
        size.width * 0.7009985,
        size.height * 0.2295010,
        size.width * 0.7020374,
        size.height * 0.2327863,
        size.width * 0.7012382,
        size.height * 0.2355578);
    path_137.lineTo(size.width * 0.7004389, size.height * 0.2382275);
    path_137.lineTo(size.width * 0.7060344, size.height * 0.2406912);
    path_137.cubicTo(
        size.width * 0.7085924,
        size.height * 0.2418206,
        size.width * 0.7101908,
        size.height * 0.2450029,
        size.width * 0.7099511,
        size.height * 0.2483912);
    path_137.cubicTo(
        size.width * 0.7097916,
        size.height * 0.2509578,
        size.width * 0.7092321,
        size.height * 0.2534216,
        size.width * 0.7083527,
        size.height * 0.2555775);
    path_137.cubicTo(
        size.width * 0.7071534,
        size.height * 0.2584520,
        size.width * 0.7046756,
        size.height * 0.2599922,
        size.width * 0.7021977,
        size.height * 0.2593755);
    path_137.lineTo(size.width * 0.6956427, size.height * 0.2578363);
    path_137.lineTo(size.width * 0.6954031, size.height * 0.2606078);
    path_137.cubicTo(
        size.width * 0.6952427,
        size.height * 0.2622510,
        size.width * 0.6945237,
        size.height * 0.2636882,
        size.width * 0.6934840,
        size.height * 0.2647147);
    path_137.cubicTo(
        size.width * 0.6926847,
        size.height * 0.2655353,
        size.width * 0.6917260,
        size.height * 0.2659461,
        size.width * 0.6907664,
        size.height * 0.2659461);
    path_137.close();
    path_137.moveTo(size.width * 0.6910061, size.height * 0.2308353);
    path_137.cubicTo(
        size.width * 0.6890076,
        size.height * 0.2308353,
        size.width * 0.6873290,
        size.height * 0.2314510,
        size.width * 0.6861298,
        size.height * 0.2321696);
    path_137.cubicTo(
        size.width * 0.6831725,
        size.height * 0.2339157,
        size.width * 0.6807740,
        size.height * 0.2373029,
        size.width * 0.6800550,
        size.height * 0.2407941);
    path_137.cubicTo(
        size.width * 0.6790954,
        size.height * 0.2456186,
        size.width * 0.6794153,
        size.height * 0.2497255,
        size.width * 0.6810145,
        size.height * 0.2530108);
    path_137.cubicTo(
        size.width * 0.6838122,
        size.height * 0.2587598,
        size.width * 0.6894878,
        size.height * 0.2604029,
        size.width * 0.6906069,
        size.height * 0.2606078);
    path_137.cubicTo(
        size.width * 0.6908466,
        size.height * 0.2607108,
        size.width * 0.6910061,
        size.height * 0.2606078,
        size.width * 0.6910863,
        size.height * 0.2605049);
    path_137.cubicTo(
        size.width * 0.6911664,
        size.height * 0.2604029,
        size.width * 0.6913260,
        size.height * 0.2601971,
        size.width * 0.6913260,
        size.height * 0.2598892);
    path_137.lineTo(size.width * 0.6917260, size.height * 0.2559882);
    path_137.cubicTo(
        size.width * 0.6918053,
        size.height * 0.2548588,
        size.width * 0.6922855,
        size.height * 0.2539343,
        size.width * 0.6930046,
        size.height * 0.2533186);
    path_137.cubicTo(
        size.width * 0.6937244,
        size.height * 0.2527029,
        size.width * 0.6946031,
        size.height * 0.2523951,
        size.width * 0.6954031,
        size.height * 0.2526000);
    path_137.lineTo(size.width * 0.7028366, size.height * 0.2543451);
    path_137.cubicTo(
        size.width * 0.7036359,
        size.height * 0.2545510,
        size.width * 0.7044359,
        size.height * 0.2540373,
        size.width * 0.7047557,
        size.height * 0.2531137);
    path_137.cubicTo(
        size.width * 0.7053947,
        size.height * 0.2515735,
        size.width * 0.7057947,
        size.height * 0.2498284,
        size.width * 0.7059542,
        size.height * 0.2478775);
    path_137.cubicTo(
        size.width * 0.7060344,
        size.height * 0.2467480,
        size.width * 0.7055550,
        size.height * 0.2458245,
        size.width * 0.7047557,
        size.height * 0.2454137);
    path_137.lineTo(size.width * 0.6983603, size.height * 0.2425392);
    path_137.cubicTo(
        size.width * 0.6975611,
        size.height * 0.2422314,
        size.width * 0.6969214,
        size.height * 0.2415127,
        size.width * 0.6966015,
        size.height * 0.2405882);
    path_137.cubicTo(
        size.width * 0.6963618,
        size.height * 0.2398696,
        size.width * 0.6961221,
        size.height * 0.2386382,
        size.width * 0.6966015,
        size.height * 0.2369951);
    path_137.lineTo(size.width * 0.6975611, size.height * 0.2337098);
    path_137.cubicTo(
        size.width * 0.6976412,
        size.height * 0.2334020,
        size.width * 0.6974809,
        size.height * 0.2328882,
        size.width * 0.6972412,
        size.height * 0.2326833);
    path_137.cubicTo(
        size.width * 0.6949229,
        size.height * 0.2313490,
        size.width * 0.6928450,
        size.height * 0.2308353,
        size.width * 0.6910061,
        size.height * 0.2308353);
    path_137.close();

    Paint paint_137_fill = Paint()..style = PaintingStyle.fill;
    paint_137_fill.color = Colors.black;
    canvas.drawPath(path_137, paint_137_fill);

    Path path_138 = Path();
    path_138.moveTo(size.width * 0.7032794, size.height * 0.2488549);
    path_138.lineTo(size.width * 0.7029595, size.height * 0.2504971);
    path_138.cubicTo(
        size.width * 0.7027198,
        size.height * 0.2515235,
        size.width * 0.7019206,
        size.height * 0.2522422,
        size.width * 0.7010412,
        size.height * 0.2519343);
    path_138.lineTo(size.width * 0.6907290, size.height * 0.2487520);
    path_138.cubicTo(
        size.width * 0.6904092,
        size.height * 0.2486490,
        size.width * 0.6900893,
        size.height * 0.2489569,
        size.width * 0.6900099,
        size.height * 0.2493676);
    path_138.lineTo(size.width * 0.6896901, size.height * 0.2516265);
    path_138.cubicTo(
        size.width * 0.6896099,
        size.height * 0.2523451,
        size.width * 0.6890504,
        size.height * 0.2527559,
        size.width * 0.6884908,
        size.height * 0.2525500);
    path_138.cubicTo(
        size.width * 0.6864924,
        size.height * 0.2516265,
        size.width * 0.6820962,
        size.height * 0.2483412,
        size.width * 0.6835351,
        size.height * 0.2422843);
    path_138.cubicTo(
        size.width * 0.6842542,
        size.height * 0.2393069,
        size.width * 0.6872916,
        size.height * 0.2358167,
        size.width * 0.6916885,
        size.height * 0.2375618);
    path_138.cubicTo(
        size.width * 0.6921679,
        size.height * 0.2377667,
        size.width * 0.6924076,
        size.height * 0.2384853,
        size.width * 0.6922481,
        size.height * 0.2391010);
    path_138.lineTo(size.width * 0.6915282, size.height * 0.2412578);
    path_138.cubicTo(
        size.width * 0.6913687,
        size.height * 0.2416676,
        size.width * 0.6915282,
        size.height * 0.2421814,
        size.width * 0.6919282,
        size.height * 0.2422843);
    path_138.lineTo(size.width * 0.7022397, size.height * 0.2464931);
    path_138.cubicTo(
        size.width * 0.7030397,
        size.height * 0.2466990,
        size.width * 0.7034389,
        size.height * 0.2478275,
        size.width * 0.7032794,
        size.height * 0.2488549);
    path_138.close();

    Paint paint_138_fill = Paint()..style = PaintingStyle.fill;
    paint_138_fill.color = Colors.black;
    canvas.drawPath(path_138, paint_138_fill);

    Path path_139 = Path();
    path_139.moveTo(size.width * 0.6636237, size.height * 0.2316755);
    path_139.lineTo(size.width * 0.6596267, size.height * 0.2316755);
    path_139.lineTo(size.width * 0.6596267, size.height * 0.2471775);
    path_139.lineTo(size.width * 0.6636237, size.height * 0.2471775);
    path_139.lineTo(size.width * 0.6636237, size.height * 0.2316755);
    path_139.close();

    Paint paint_139_fill = Paint()..style = PaintingStyle.fill;
    paint_139_fill.color = Colors.black;
    canvas.drawPath(path_139, paint_139_fill);

    Path path_140 = Path();
    path_140.moveTo(size.width * 0.6776473, size.height * 0.2250510);
    path_140.cubicTo(
        size.width * 0.6843450,
        size.height * 0.2204578,
        size.width * 0.6868756,
        size.height * 0.2097598,
        size.width * 0.6832992,
        size.height * 0.2011578);
    path_140.cubicTo(
        size.width * 0.6797229,
        size.height * 0.1925549,
        size.width * 0.6713939,
        size.height * 0.1893049,
        size.width * 0.6646954,
        size.height * 0.1938980);
    path_140.cubicTo(
        size.width * 0.6579969,
        size.height * 0.1984922,
        size.width * 0.6554664,
        size.height * 0.2091892,
        size.width * 0.6590435,
        size.height * 0.2177922);
    path_140.cubicTo(
        size.width * 0.6626198,
        size.height * 0.2263941,
        size.width * 0.6709489,
        size.height * 0.2296441,
        size.width * 0.6776473,
        size.height * 0.2250510);
    path_140.close();

    Paint paint_140_fill = Paint()..style = PaintingStyle.fill;
    paint_140_fill.color = Colors.black;
    canvas.drawPath(path_140, paint_140_fill);

    Path path_141 = Path();
    path_141.moveTo(size.width * 0.6782908, size.height * 0.2051382);
    path_141.cubicTo(
        size.width * 0.6793817,
        size.height * 0.2043902,
        size.width * 0.6797931,
        size.height * 0.2026490,
        size.width * 0.6792115,
        size.height * 0.2012490);
    path_141.cubicTo(
        size.width * 0.6786290,
        size.height * 0.1998480,
        size.width * 0.6772733,
        size.height * 0.1993186,
        size.width * 0.6761824,
        size.height * 0.2000667);
    path_141.cubicTo(
        size.width * 0.6750924,
        size.height * 0.2008147,
        size.width * 0.6746802,
        size.height * 0.2025559,
        size.width * 0.6752626,
        size.height * 0.2039569);
    path_141.cubicTo(
        size.width * 0.6758450,
        size.height * 0.2053569,
        size.width * 0.6772008,
        size.height * 0.2058863,
        size.width * 0.6782908,
        size.height * 0.2051382);
    path_141.close();

    Paint paint_141_fill = Paint()..style = PaintingStyle.fill;
    paint_141_fill.color = Colors.white;
    canvas.drawPath(path_141, paint_141_fill);

    Path path_142 = Path();
    path_142.moveTo(size.width * 0.6688214, size.height * 0.2071049);
    path_142.cubicTo(
        size.width * 0.6703794,
        size.height * 0.2060363,
        size.width * 0.6709679,
        size.height * 0.2035490,
        size.width * 0.6701359,
        size.height * 0.2015480);
    path_142.cubicTo(
        size.width * 0.6693046,
        size.height * 0.1995480,
        size.width * 0.6673672,
        size.height * 0.1987922,
        size.width * 0.6658092,
        size.height * 0.1998598);
    path_142.cubicTo(
        size.width * 0.6642519,
        size.height * 0.2009284,
        size.width * 0.6636634,
        size.height * 0.2034157,
        size.width * 0.6644947,
        size.height * 0.2054167);
    path_142.cubicTo(
        size.width * 0.6653267,
        size.height * 0.2074176,
        size.width * 0.6672641,
        size.height * 0.2081725,
        size.width * 0.6688214,
        size.height * 0.2071049);
    path_142.close();

    Paint paint_142_fill = Paint()..style = PaintingStyle.fill;
    paint_142_fill.color = Colors.white;
    canvas.drawPath(path_142, paint_142_fill);

    Path path_143 = Path();
    path_143.moveTo(size.width * 0.6760443, size.height * 0.2192588);
    path_143.cubicTo(
        size.width * 0.6771733,
        size.height * 0.2184843,
        size.width * 0.6776000,
        size.height * 0.2166804,
        size.width * 0.6769969,
        size.height * 0.2152304);
    path_143.cubicTo(
        size.width * 0.6763939,
        size.height * 0.2137794,
        size.width * 0.6749893,
        size.height * 0.2132324,
        size.width * 0.6738603,
        size.height * 0.2140059);
    path_143.cubicTo(
        size.width * 0.6727305,
        size.height * 0.2147804,
        size.width * 0.6723046,
        size.height * 0.2165843,
        size.width * 0.6729069,
        size.height * 0.2180353);
    path_143.cubicTo(
        size.width * 0.6735099,
        size.height * 0.2194853,
        size.width * 0.6749145,
        size.height * 0.2200333,
        size.width * 0.6760443,
        size.height * 0.2192588);
    path_143.close();

    Paint paint_143_fill = Paint()..style = PaintingStyle.fill;
    paint_143_fill.color = Colors.white;
    canvas.drawPath(path_143, paint_143_fill);

    Path path_144 = Path();
    path_144.moveTo(size.width * 0.6667069, size.height * 0.2174412);
    path_144.cubicTo(
        size.width * 0.6677969,
        size.height * 0.2166931,
        size.width * 0.6682092,
        size.height * 0.2149520,
        size.width * 0.6676267,
        size.height * 0.2135510);
    path_144.cubicTo(
        size.width * 0.6670443,
        size.height * 0.2121510,
        size.width * 0.6656885,
        size.height * 0.2116216,
        size.width * 0.6645985,
        size.height * 0.2123696);
    path_144.cubicTo(
        size.width * 0.6635076,
        size.height * 0.2131176,
        size.width * 0.6630954,
        size.height * 0.2148588,
        size.width * 0.6636779,
        size.height * 0.2162588);
    path_144.cubicTo(
        size.width * 0.6642603,
        size.height * 0.2176598,
        size.width * 0.6656160,
        size.height * 0.2181882,
        size.width * 0.6667069,
        size.height * 0.2174412);
    path_144.close();

    Paint paint_144_fill = Paint()..style = PaintingStyle.fill;
    paint_144_fill.color = Colors.white;
    canvas.drawPath(path_144, paint_144_fill);

    Path path_145 = Path();
    path_145.moveTo(size.width * 0.6412137, size.height * 0.2610373);
    path_145.cubicTo(
        size.width * 0.6412137,
        size.height * 0.2610373,
        size.width * 0.6425725,
        size.height * 0.2552882,
        size.width * 0.6476092,
        size.height * 0.2574441);
    path_145.cubicTo(
        size.width * 0.6476092,
        size.height * 0.2575471,
        size.width * 0.6460099,
        size.height * 0.2642196,
        size.width * 0.6412137,
        size.height * 0.2610373);
    path_145.close();

    Paint paint_145_fill = Paint()..style = PaintingStyle.fill;
    paint_145_fill.color = Colors.black;
    canvas.drawPath(path_145, paint_145_fill);

    Path path_146 = Path();
    path_146.moveTo(size.width * 0.6449710, size.height * 0.2681363);
    path_146.cubicTo(
        size.width * 0.6449710,
        size.height * 0.2681363,
        size.width * 0.6498473,
        size.height * 0.2647490,
        size.width * 0.6514458,
        size.height * 0.2730647);
    path_146.cubicTo(
        size.width * 0.6513656,
        size.height * 0.2729618,
        size.width * 0.6458504,
        size.height * 0.2755284,
        size.width * 0.6449710,
        size.height * 0.2681363);
    path_146.close();

    Paint paint_146_fill = Paint()..style = PaintingStyle.fill;
    paint_146_fill.color = Colors.black;
    canvas.drawPath(path_146, paint_146_fill);

    Path path_147 = Path();
    path_147.moveTo(size.width * 0.6383809, size.height * 0.2655392);
    path_147.cubicTo(
        size.width * 0.6383809,
        size.height * 0.2655392,
        size.width * 0.6333443,
        size.height * 0.2701588,
        size.width * 0.6387000,
        size.height * 0.2753941);
    path_147.cubicTo(
        size.width * 0.6387000,
        size.height * 0.2753941,
        size.width * 0.6430969,
        size.height * 0.2708775,
        size.width * 0.6383809,
        size.height * 0.2655392);
    path_147.close();

    Paint paint_147_fill = Paint()..style = PaintingStyle.fill;
    paint_147_fill.color = Colors.black;
    canvas.drawPath(path_147, paint_147_fill);

    Path path_148 = Path();
    path_148.moveTo(size.width * 0.6796282, size.height * 0.2637824);
    path_148.lineTo(size.width * 0.6654939, size.height * 0.2666402);
    path_148.lineTo(size.width * 0.6661153, size.height * 0.2717108);
    path_148.lineTo(size.width * 0.6802496, size.height * 0.2688529);
    path_148.lineTo(size.width * 0.6796282, size.height * 0.2637824);
    path_148.close();

    Paint paint_148_fill = Paint()..style = PaintingStyle.fill;
    paint_148_fill.color = Colors.black;
    canvas.drawPath(path_148, paint_148_fill);

    Path path_149 = Path();
    path_149.moveTo(size.width * 0.6918718, size.height * 0.2099059);
    path_149.lineTo(size.width * 0.6896840, size.height * 0.2142020);
    path_149.lineTo(size.width * 0.7042031, size.height * 0.2263951);
    path_149.lineTo(size.width * 0.7063908, size.height * 0.2220990);
    path_149.lineTo(size.width * 0.6918718, size.height * 0.2099059);
    path_149.close();

    Paint paint_149_fill = Paint()..style = PaintingStyle.fill;
    paint_149_fill.color = Colors.black;
    canvas.drawPath(path_149, paint_149_fill);

    Path path_150 = Path();
    path_150.moveTo(size.width * 0.6426130, size.height * 0.1329098);
    path_150.cubicTo(
        size.width * 0.6189519,
        size.height * 0.09153647,
        size.width * 0.5841794,
        size.height * 0.06946382,
        size.width * 0.5591588,
        size.height * 0.05827343);
    path_150.cubicTo(
        size.width * 0.5319008,
        size.height * 0.04605647,
        size.width * 0.5099977,
        size.height * 0.04359255,
        size.width * 0.5097580,
        size.height * 0.04359255);
    path_150.lineTo(size.width * 0.5100779, size.height * 0.03845931);
    path_150.cubicTo(
        size.width * 0.5103176,
        size.height * 0.03845931,
        size.width * 0.5326198,
        size.height * 0.04092324,
        size.width * 0.5603580,
        size.height * 0.05334559);
    path_150.cubicTo(
        size.width * 0.5859374,
        size.height * 0.06484392,
        size.width * 0.6215893,
        size.height * 0.08742990,
        size.width * 0.6458107,
        size.height * 0.1298304);
    path_150.lineTo(size.width * 0.6426130, size.height * 0.1329098);
    path_150.close();

    Paint paint_150_fill = Paint()..style = PaintingStyle.fill;
    paint_150_fill.color = Colors.black;
    canvas.drawPath(path_150, paint_150_fill);

    Path path_151 = Path();
    path_151.moveTo(size.width * 0.6386145, size.height * 0.1603520);
    path_151.cubicTo(
        size.width * 0.6057611,
        size.height * 0.1070696,
        size.width * 0.5615557,
        size.height * 0.08438098,
        size.width * 0.5610763,
        size.height * 0.08417569);
    path_151.lineTo(size.width * 0.5625153, size.height * 0.07935049);
    path_151.cubicTo(
        size.width * 0.5629947,
        size.height * 0.07955578,
        size.width * 0.6081588,
        size.height * 0.1028608,
        size.width * 0.6416519,
        size.height * 0.1571696);
    path_151.lineTo(size.width * 0.6386145, size.height * 0.1603520);
    path_151.close();

    Paint paint_151_fill = Paint()..style = PaintingStyle.fill;
    paint_151_fill.color = Colors.black;
    canvas.drawPath(path_151, paint_151_fill);

    Path path_152 = Path();
    path_152.moveTo(size.width * 0.2104580, size.height * 0.1842206);
    path_152.lineTo(size.width * 0.2066214, size.height * 0.1826804);
    path_152.cubicTo(
        size.width * 0.2068611,
        size.height * 0.1815520,
        size.width * 0.2135756,
        size.height * 0.1549618,
        size.width * 0.2322008,
        size.height * 0.1241627);
    path_152.cubicTo(
        size.width * 0.2494672,
        size.height * 0.09572461,
        size.width * 0.2805626,
        size.height * 0.05845775,
        size.width * 0.3310824,
        size.height * 0.04244216);
    path_152.lineTo(size.width * 0.3320420, size.height * 0.04747275);
    path_152.cubicTo(
        size.width * 0.2827206,
        size.height * 0.06318029,
        size.width * 0.2522649,
        size.height * 0.09952353,
        size.width * 0.2353985,
        size.height * 0.1272422);
    path_152.cubicTo(
        size.width * 0.2171725,
        size.height * 0.1571176,
        size.width * 0.2105382,
        size.height * 0.1839127,
        size.width * 0.2104580,
        size.height * 0.1842206);
    path_152.close();

    Paint paint_152_fill = Paint()..style = PaintingStyle.fill;
    paint_152_fill.color = Colors.black;
    canvas.drawPath(path_152, paint_152_fill);

    Path path_153 = Path();
    path_153.moveTo(size.width * 0.2508740, size.height * 0.1325353);
    path_153.lineTo(size.width * 0.2477565, size.height * 0.1293529);
    path_153.cubicTo(
        size.width * 0.2669412,
        size.height * 0.09845098,
        size.width * 0.2951588,
        size.height * 0.07915000,
        size.width * 0.2963573,
        size.height * 0.07843137);
    path_153.lineTo(size.width * 0.2981962, size.height * 0.08294863);
    path_153.cubicTo(
        size.width * 0.2978763,
        size.height * 0.08305127,
        size.width * 0.2694992,
        size.height * 0.1024549,
        size.width * 0.2508740,
        size.height * 0.1325353);
    path_153.close();

    Paint paint_153_fill = Paint()..style = PaintingStyle.fill;
    paint_153_fill.color = Colors.black;
    canvas.drawPath(path_153, paint_153_fill);

    Path path_154 = Path();
    path_154.moveTo(size.width * 0.6793985, size.height * 0.4211304);
    path_154.lineTo(size.width * 0.6762809, size.height * 0.4179480);
    path_154.cubicTo(
        size.width * 0.6988229,
        size.height * 0.3809892,
        size.width * 0.7013809,
        size.height * 0.3264745,
        size.width * 0.7014611,
        size.height * 0.3259608);
    path_154.lineTo(size.width * 0.7054573, size.height * 0.3262696);
    path_154.cubicTo(
        size.width * 0.7052977,
        size.height * 0.3286304,
        size.width * 0.7026595,
        size.height * 0.3830422,
        size.width * 0.6793985,
        size.height * 0.4211304);
    path_154.close();

    Paint paint_154_fill = Paint()..style = PaintingStyle.fill;
    paint_154_fill.color = Colors.black;
    canvas.drawPath(path_154, paint_154_fill);

    Path path_155 = Path();
    path_155.moveTo(size.width * 0.2069191, size.height * 0.3923843);
    path_155.cubicTo(
        size.width * 0.1907718,
        size.height * 0.3643578,
        size.width * 0.1882137,
        size.height * 0.3279118,
        size.width * 0.1881336,
        size.height * 0.3263716);
    path_155.lineTo(size.width * 0.1921305, size.height * 0.3259608);
    path_155.cubicTo(
        size.width * 0.1921305,
        size.height * 0.3263716,
        size.width * 0.1946885,
        size.height * 0.3625098,
        size.width * 0.2101160,
        size.height * 0.3893049);
    path_155.lineTo(size.width * 0.2069191, size.height * 0.3923843);
    path_155.close();

    Paint paint_155_fill = Paint()..style = PaintingStyle.fill;
    paint_155_fill.color = Colors.black;
    canvas.drawPath(path_155, paint_155_fill);

    Path path_156 = Path();
    path_156.moveTo(size.width * 0.8465954, size.height * 0.9599225);
    path_156.cubicTo(
        size.width * 0.8465954,
        size.height * 0.9599225,
        size.width * 0.7944733,
        size.height * 0.9641314,
        size.width * 0.7870458,
        size.height * 0.9862059);
    path_156.lineTo(size.width * 0.8556260, size.height * 0.9853824);
    path_156.lineTo(size.width * 0.8465954, size.height * 0.9599225);
    path_156.close();

    Paint paint_156_fill = Paint()..style = PaintingStyle.fill;
    paint_156_fill.color = Colors.white;
    canvas.drawPath(path_156, paint_156_fill);

    Path path_157 = Path();
    path_157.moveTo(size.width * 0.7840229, size.height * 0.9888137);
    path_157.lineTo(size.width * 0.7852214, size.height * 0.9852255);
    path_157.cubicTo(
        size.width * 0.7930534,
        size.height * 0.9620216,
        size.width * 0.8442977,
        size.height * 0.9576069,
        size.width * 0.8465344,
        size.height * 0.9574020);
    path_157.lineTo(size.width * 0.8478931, size.height * 0.9572990);
    path_157.lineTo(size.width * 0.8587634, size.height * 0.9880000);
    path_157.lineTo(size.width * 0.7840229, size.height * 0.9888137);
    path_157.close();
    path_157.moveTo(size.width * 0.8453359, size.height * 0.9626382);
    path_157.cubicTo(
        size.width * 0.8314275,
        size.height * 0.9639725,
        size.width * 0.7997710,
        size.height * 0.9698245,
        size.width * 0.7906565,
        size.height * 0.9835784);
    path_157.lineTo(size.width * 0.8525267, size.height * 0.9828627);
    path_157.lineTo(size.width * 0.8453359, size.height * 0.9626382);
    path_157.close();

    Paint paint_157_fill = Paint()..style = PaintingStyle.fill;
    paint_157_fill.color = Colors.black;
    canvas.drawPath(path_157, paint_157_fill);

    Path path_158 = Path();
    path_158.moveTo(size.width * 0.9651069, size.height * 0.8827725);
    path_158.cubicTo(
        size.width * 0.9643053,
        size.height * 0.8874951,
        size.width * 0.9629466,
        size.height * 0.8924225,
        size.width * 0.9609466,
        size.height * 0.8972480);
    path_158.cubicTo(
        size.width * 0.9573511,
        size.height * 0.9060775,
        size.width * 0.9514351,
        size.height * 0.9150088,
        size.width * 0.9422443,
        size.height * 0.9233245);
    path_158.lineTo(size.width * 0.9394427, size.height * 0.9139824);
    path_158.cubicTo(
        size.width * 0.9394427,
        size.height * 0.9139824,
        size.width * 0.9441603,
        size.height * 0.9050510,
        size.width * 0.9448015,
        size.height * 0.8916020);
    path_158.cubicTo(
        size.width * 0.9449618,
        size.height * 0.8873922,
        size.width * 0.9448015,
        size.height * 0.8826696,
        size.width * 0.9439237,
        size.height * 0.8776392);
    path_158.cubicTo(
        size.width * 0.9429618,
        size.height * 0.8723010,
        size.width * 0.9412061,
        size.height * 0.8666539,
        size.width * 0.9383282,
        size.height * 0.8607000);
    path_158.cubicTo(
        size.width * 0.9380840,
        size.height * 0.8602892,
        size.width * 0.9378473,
        size.height * 0.8597755,
        size.width * 0.9376031,
        size.height * 0.8592627);
    path_158.cubicTo(
        size.width * 0.9360076,
        size.height * 0.8562853,
        size.width * 0.9336870,
        size.height * 0.8521784,
        size.width * 0.9319313,
        size.height * 0.8477637);
    path_158.cubicTo(
        size.width * 0.9288168,
        size.height * 0.8399618,
        size.width * 0.9275344,
        size.height * 0.8310304,
        size.width * 0.9355267,
        size.height * 0.8246647);
    path_158.cubicTo(
        size.width * 0.9420840,
        size.height * 0.8194294,
        size.width * 0.9553511,
        size.height * 0.8214824,
        size.width * 0.9619084,
        size.height * 0.8415020);
    path_158.cubicTo(
        size.width * 0.9627099,
        size.height * 0.8439657,
        size.width * 0.9634275,
        size.height * 0.8467373,
        size.width * 0.9640687,
        size.height * 0.8498176);
    path_158.cubicTo(
        size.width * 0.9647023,
        size.height * 0.8531029,
        size.width * 0.9652672,
        size.height * 0.8567990,
        size.width * 0.9656641,
        size.height * 0.8607000);
    path_158.cubicTo(
        size.width * 0.9663817,
        size.height * 0.8676814,
        size.width * 0.9664656,
        size.height * 0.8750725,
        size.width * 0.9651069,
        size.height * 0.8827725);
    path_158.close();

    Paint paint_158_fill = Paint()..style = PaintingStyle.fill;
    paint_158_fill.color = Colors.white;
    canvas.drawPath(path_158, paint_158_fill);

    Path path_159 = Path();
    path_159.moveTo(size.width * 0.9413359, size.height * 0.9271843);
    path_159.lineTo(size.width * 0.9372595, size.height * 0.9135304);
    path_159.lineTo(size.width * 0.9378168, size.height * 0.9124010);
    path_159.cubicTo(
        size.width * 0.9379008,
        size.height * 0.9122980,
        size.width * 0.9422977,
        size.height * 0.9038804,
        size.width * 0.9428550,
        size.height * 0.8913549);
    path_159.cubicTo(
        size.width * 0.9430916,
        size.height * 0.8869402,
        size.width * 0.9427710,
        size.height * 0.8825255,
        size.width * 0.9419771,
        size.height * 0.8782137);
    path_159.cubicTo(
        size.width * 0.9410153,
        size.height * 0.8727725,
        size.width * 0.9391756,
        size.height * 0.8673314,
        size.width * 0.9366183,
        size.height * 0.8620961);
    path_159.cubicTo(
        size.width * 0.9363817,
        size.height * 0.8616853,
        size.width * 0.9362214,
        size.height * 0.8612745,
        size.width * 0.9359771,
        size.height * 0.8607608);
    path_159.cubicTo(
        size.width * 0.9356565,
        size.height * 0.8601451,
        size.width * 0.9352595,
        size.height * 0.8594265,
        size.width * 0.9349389,
        size.height * 0.8588108);
    path_159.cubicTo(
        size.width * 0.9335038,
        size.height * 0.8560382,
        size.width * 0.9316641,
        size.height * 0.8526510,
        size.width * 0.9302214,
        size.height * 0.8490578);
    path_159.cubicTo(
        size.width * 0.9255878,
        size.height * 0.8374569,
        size.width * 0.9270229,
        size.height * 0.8286275,
        size.width * 0.9345420,
        size.height * 0.8226725);
    path_159.cubicTo(
        size.width * 0.9383740,
        size.height * 0.8195931,
        size.width * 0.9436565,
        size.height * 0.8189775,
        size.width * 0.9485267,
        size.height * 0.8210304);
    path_159.cubicTo(
        size.width * 0.9550840,
        size.height * 0.8237000,
        size.width * 0.9605191,
        size.height * 0.8306804,
        size.width * 0.9637939,
        size.height * 0.8406392);
    path_159.cubicTo(
        size.width * 0.9646794,
        size.height * 0.8433088,
        size.width * 0.9653969,
        size.height * 0.8462853,
        size.width * 0.9660382,
        size.height * 0.8493657);
    path_159.cubicTo(
        size.width * 0.9668321,
        size.height * 0.8533696,
        size.width * 0.9673130,
        size.height * 0.8569627,
        size.width * 0.9676336,
        size.height * 0.8605559);
    path_159.cubicTo(
        size.width * 0.9684351,
        size.height * 0.8686667,
        size.width * 0.9681908,
        size.height * 0.8760578,
        size.width * 0.9669924,
        size.height * 0.8832441);
    path_159.cubicTo(
        size.width * 0.9661145,
        size.height * 0.8884804,
        size.width * 0.9646794,
        size.height * 0.8936137,
        size.width * 0.9626794,
        size.height * 0.8983363);
    path_159.cubicTo(
        size.width * 0.9586031,
        size.height * 0.9082941,
        size.width * 0.9521298,
        size.height * 0.9174314,
        size.width * 0.9434122,
        size.height * 0.9253363);
    path_159.lineTo(size.width * 0.9413359, size.height * 0.9271843);
    path_159.close();
    path_159.moveTo(size.width * 0.9417328, size.height * 0.9140441);
    path_159.lineTo(size.width * 0.9432519, size.height * 0.9190745);
    path_159.cubicTo(
        size.width * 0.9503664,
        size.height * 0.9120931,
        size.width * 0.9557252,
        size.height * 0.9042902,
        size.width * 0.9592443,
        size.height * 0.8957696);
    path_159.cubicTo(
        size.width * 0.9610000,
        size.height * 0.8913549,
        size.width * 0.9623588,
        size.height * 0.8867353,
        size.width * 0.9631603,
        size.height * 0.8819098);
    path_159.cubicTo(
        size.width * 0.9642748,
        size.height * 0.8753392,
        size.width * 0.9644351,
        size.height * 0.8684608,
        size.width * 0.9637176,
        size.height * 0.8609667);
    path_159.cubicTo(
        size.width * 0.9633969,
        size.height * 0.8576814,
        size.width * 0.9629160,
        size.height * 0.8541912,
        size.width * 0.9621985,
        size.height * 0.8503922);
    path_159.cubicTo(
        size.width * 0.9616412,
        size.height * 0.8475176,
        size.width * 0.9609160,
        size.height * 0.8448480,
        size.width * 0.9601221,
        size.height * 0.8424873);
    path_159.cubicTo(
        size.width * 0.9564427,
        size.height * 0.8315020,
        size.width * 0.9511679,
        size.height * 0.8272931,
        size.width * 0.9473282,
        size.height * 0.8257529);
    path_159.cubicTo(
        size.width * 0.9435725,
        size.height * 0.8242127,
        size.width * 0.9394198,
        size.height * 0.8246235,
        size.width * 0.9366183,
        size.height * 0.8268824);
    path_159.cubicTo(
        size.width * 0.9310229,
        size.height * 0.8312971,
        size.width * 0.9301450,
        size.height * 0.8374569,
        size.width * 0.9337405,
        size.height * 0.8465931);
    path_159.cubicTo(
        size.width * 0.9350992,
        size.height * 0.8499814,
        size.width * 0.9367786,
        size.height * 0.8531637,
        size.width * 0.9382214,
        size.height * 0.8558333);
    path_159.cubicTo(
        size.width * 0.9386183,
        size.height * 0.8565520,
        size.width * 0.9389389,
        size.height * 0.8571676,
        size.width * 0.9392595,
        size.height * 0.8577843);
    path_159.cubicTo(
        size.width * 0.9394962,
        size.height * 0.8582971,
        size.width * 0.9398168,
        size.height * 0.8588108,
        size.width * 0.9400534,
        size.height * 0.8592216);
    path_159.cubicTo(
        size.width * 0.9428550,
        size.height * 0.8649706,
        size.width * 0.9447710,
        size.height * 0.8709245,
        size.width * 0.9458931,
        size.height * 0.8768794);
    path_159.cubicTo(
        size.width * 0.9467710,
        size.height * 0.8817049,
        size.width * 0.9470916,
        size.height * 0.8866324,
        size.width * 0.9468473,
        size.height * 0.8915598);
    path_159.cubicTo(
        size.width * 0.9463740,
        size.height * 0.9029559,
        size.width * 0.9430916,
        size.height * 0.9111686,
        size.width * 0.9417328,
        size.height * 0.9140441);
    path_159.close();

    Paint paint_159_fill = Paint()..style = PaintingStyle.fill;
    paint_159_fill.color = Colors.black;
    canvas.drawPath(path_159, paint_159_fill);

    Path path_160 = Path();
    path_160.moveTo(size.width * 0.9657023, size.height * 0.8605882);
    path_160.cubicTo(
        size.width * 0.9596336,
        size.height * 0.8582275,
        size.width * 0.9498015,
        size.height * 0.8559686,
        size.width * 0.9376489,
        size.height * 0.8591510);
    path_160.cubicTo(
        size.width * 0.9360458,
        size.height * 0.8561745,
        size.width * 0.9337328,
        size.height * 0.8520676,
        size.width * 0.9319695,
        size.height * 0.8476529);
    path_160.cubicTo(
        size.width * 0.9378855,
        size.height * 0.8441627,
        size.width * 0.9493969,
        size.height * 0.8389265,
        size.width * 0.9619466,
        size.height * 0.8413902);
    path_160.cubicTo(
        size.width * 0.9627481,
        size.height * 0.8438549,
        size.width * 0.9634656,
        size.height * 0.8466265,
        size.width * 0.9641069,
        size.height * 0.8497059);
    path_160.cubicTo(
        size.width * 0.9647481,
        size.height * 0.8530941,
        size.width * 0.9653053,
        size.height * 0.8567902,
        size.width * 0.9657023,
        size.height * 0.8605882);
    path_160.close();

    Paint paint_160_fill = Paint()..style = PaintingStyle.fill;
    paint_160_fill.color = Colors.black;
    canvas.drawPath(path_160, paint_160_fill);

    Path path_161 = Path();
    path_161.moveTo(size.width * 0.9650992, size.height * 0.8825392);
    path_161.cubicTo(
        size.width * 0.9642977,
        size.height * 0.8872618,
        size.width * 0.9629389,
        size.height * 0.8921902,
        size.width * 0.9609466,
        size.height * 0.8970147);
    path_161.cubicTo(
        size.width * 0.9571069,
        size.height * 0.8945510,
        size.width * 0.9519084,
        size.height * 0.8922922,
        size.width * 0.9448779,
        size.height * 0.8914716);
    path_161.cubicTo(
        size.width * 0.9450382,
        size.height * 0.8872618,
        size.width * 0.9448779,
        size.height * 0.8825392,
        size.width * 0.9440000,
        size.height * 0.8775088);
    path_161.cubicTo(
        size.width * 0.9487939,
        size.height * 0.8774059,
        size.width * 0.9564656,
        size.height * 0.8780225,
        size.width * 0.9650992,
        size.height * 0.8825392);
    path_161.close();

    Paint paint_161_fill = Paint()..style = PaintingStyle.fill;
    paint_161_fill.color = Colors.black;
    canvas.drawPath(path_161, paint_161_fill);

    Path path_162 = Path();
    path_162.moveTo(size.width * 0.9700076, size.height * 0.9863824);
    path_162.lineTo(size.width * 0.8413130, size.height * 0.9863824);
    path_162.lineTo(size.width * 0.8418702, size.height * 0.9820686);
    path_162.cubicTo(
        size.width * 0.8333206,
        size.height * 0.9849412,
        size.width * 0.8194122,
        size.height * 0.9834020,
        size.width * 0.8110153,
        size.height * 0.9819608);
    path_162.cubicTo(
        size.width * 0.8064580,
        size.height * 0.9812451,
        size.width * 0.8027023,
        size.height * 0.9768324,
        size.width * 0.8018244,
        size.height * 0.9709804);
    path_162.cubicTo(
        size.width * 0.7951908,
        size.height * 0.9299147,
        size.width * 0.8178931,
        size.height * 0.9000402,
        size.width * 0.8178931,
        size.height * 0.9000402);
    path_162.lineTo(size.width * 0.8214885, size.height * 0.9046598);
    path_162.cubicTo(
        size.width * 0.8317176,
        size.height * 0.8938804,
        size.width * 0.8428321,
        size.height * 0.8872069,
        size.width * 0.8530611,
        size.height * 0.8831010);
    path_162.cubicTo(
        size.width * 0.8568244,
        size.height * 0.8815608,
        size.width * 0.8604962,
        size.height * 0.8804314,
        size.width * 0.8639389,
        size.height * 0.8795069);
    path_162.cubicTo(
        size.width * 0.8712901,
        size.height * 0.8775569,
        size.width * 0.8776031,
        size.height * 0.8768382,
        size.width * 0.8820840,
        size.height * 0.8766324);
    path_162.cubicTo(
        size.width * 0.8865573,
        size.height * 0.8764275,
        size.width * 0.8891985,
        size.height * 0.8766324,
        size.width * 0.8891985,
        size.height * 0.8766324);
    path_162.cubicTo(
        size.width * 0.8915115,
        size.height * 0.8768382,
        size.width * 0.8937557,
        size.height * 0.8772490,
        size.width * 0.8959084,
        size.height * 0.8776598);
    path_162.cubicTo(
        size.width * 0.9063817,
        size.height * 0.8797127,
        size.width * 0.9150153,
        size.height * 0.8836137,
        size.width * 0.9221298,
        size.height * 0.8888500);
    path_162.cubicTo(
        size.width * 0.9275649,
        size.height * 0.8928539,
        size.width * 0.9321221,
        size.height * 0.8975765,
        size.width * 0.9358779,
        size.height * 0.9027088);
    path_162.cubicTo(
        size.width * 0.9561832,
        size.height * 0.9302235,
        size.width * 0.9546641,
        size.height * 0.9694412,
        size.width * 0.9546641,
        size.height * 0.9694412);
    path_162.cubicTo(
        size.width * 0.9705725,
        size.height * 0.9727255,
        size.width * 0.9700076,
        size.height * 0.9863824,
        size.width * 0.9700076,
        size.height * 0.9863824);
    path_162.close();

    Paint paint_162_fill = Paint()..style = PaintingStyle.fill;
    paint_162_fill.color = Colors.white;
    canvas.drawPath(path_162, paint_162_fill);

    Path path_163 = Path();
    path_163.moveTo(size.width * 0.9718779, size.height * 0.9888824);
    path_163.lineTo(size.width * 0.8389466, size.height * 0.9888824);
    path_163.lineTo(size.width * 0.8394198, size.height * 0.9852843);
    path_163.cubicTo(
        size.width * 0.8309466,
        size.height * 0.9870294,
        size.width * 0.8194427,
        size.height * 0.9859020,
        size.width * 0.8107252,
        size.height * 0.9844608);
    path_163.cubicTo(
        size.width * 0.8052901,
        size.height * 0.9835392,
        size.width * 0.8008931,
        size.height * 0.9784069,
        size.width * 0.7997710,
        size.height * 0.9715284);
    path_163.cubicTo(
        size.width * 0.7929771,
        size.height * 0.9295392,
        size.width * 0.8153588,
        size.height * 0.8995618,
        size.width * 0.8163206,
        size.height * 0.8983294);
    path_163.lineTo(size.width * 0.8177634, size.height * 0.8964814);
    path_163.lineTo(size.width * 0.8215954, size.height * 0.9014098);
    path_163.cubicTo(
        size.width * 0.8305496,
        size.height * 0.8924775,
        size.width * 0.8408626,
        size.height * 0.8854961,
        size.width * 0.8523740,
        size.height * 0.8808765);
    path_163.cubicTo(
        size.width * 0.8558931,
        size.height * 0.8794392,
        size.width * 0.8596489,
        size.height * 0.8782078,
        size.width * 0.8634046,
        size.height * 0.8771804);
    path_163.cubicTo(
        size.width * 0.8694809,
        size.height * 0.8755382,
        size.width * 0.8758779,
        size.height * 0.8745118,
        size.width * 0.8819466,
        size.height * 0.8743059);
    path_163.cubicTo(
        size.width * 0.8864275,
        size.height * 0.8741010,
        size.width * 0.8891450,
        size.height * 0.8743059,
        size.width * 0.8893053,
        size.height * 0.8743059);
    path_163.cubicTo(
        size.width * 0.8917023,
        size.height * 0.8745118,
        size.width * 0.8939389,
        size.height * 0.8749225,
        size.width * 0.8960153,
        size.height * 0.8753324);
    path_163.lineTo(size.width * 0.8960992, size.height * 0.8753324);
    path_163.cubicTo(
        size.width * 0.9061679,
        size.height * 0.8772833,
        size.width * 0.9152824,
        size.height * 0.8810824,
        size.width * 0.9230382,
        size.height * 0.8868314);
    path_163.cubicTo(
        size.width * 0.9283130,
        size.height * 0.8907324,
        size.width * 0.9331069,
        size.height * 0.8954549,
        size.width * 0.9372672,
        size.height * 0.9011010);
    path_163.cubicTo(
        size.width * 0.9554122,
        size.height * 0.9256382,
        size.width * 0.9566870,
        size.height * 0.9584902,
        size.width * 0.9566870,
        size.height * 0.9674225);
    path_163.cubicTo(
        size.width * 0.9724351,
        size.height * 0.9716314,
        size.width * 0.9720382,
        size.height * 0.9864118,
        size.width * 0.9720382,
        size.height * 0.9866176);
    path_163.lineTo(size.width * 0.9718779, size.height * 0.9888824);
    path_163.close();
    path_163.moveTo(size.width * 0.8436565, size.height * 0.9837451);
    path_163.lineTo(size.width * 0.9677252, size.height * 0.9837451);
    path_163.cubicTo(
        size.width * 0.9670000,
        size.height * 0.9802549,
        size.width * 0.9642824,
        size.height * 0.9739922,
        size.width * 0.9542901,
        size.height * 0.9719392);
    path_163.lineTo(size.width * 0.9525344, size.height * 0.9715284);
    path_163.lineTo(size.width * 0.9526107, size.height * 0.9692706);
    path_163.cubicTo(
        size.width * 0.9526107,
        size.height * 0.9688598,
        size.width * 0.9538092,
        size.height * 0.9308735,
        size.width * 0.9343893,
        size.height * 0.9044892);
    path_163.cubicTo(
        size.width * 0.9305496,
        size.height * 0.8992529,
        size.width * 0.9260763,
        size.height * 0.8947363,
        size.width * 0.9211221,
        size.height * 0.8911431);
    path_163.cubicTo(
        size.width * 0.9138473,
        size.height * 0.8858049,
        size.width * 0.9052901,
        size.height * 0.8821088,
        size.width * 0.8957786,
        size.height * 0.8802608);
    path_163.lineTo(size.width * 0.8956183, size.height * 0.8802608);
    path_163.cubicTo(
        size.width * 0.8935420,
        size.height * 0.8798500,
        size.width * 0.8913817,
        size.height * 0.8795422,
        size.width * 0.8890611,
        size.height * 0.8793363);
    path_163.cubicTo(
        size.width * 0.8890611,
        size.height * 0.8793363,
        size.width * 0.8864275,
        size.height * 0.8791314,
        size.width * 0.8821908,
        size.height * 0.8793363);
    path_163.cubicTo(
        size.width * 0.8781145,
        size.height * 0.8795422,
        size.width * 0.8717176,
        size.height * 0.8801578,
        size.width * 0.8643664,
        size.height * 0.8821088);
    path_163.cubicTo(
        size.width * 0.8606870,
        size.height * 0.8830324,
        size.width * 0.8571679,
        size.height * 0.8842647,
        size.width * 0.8537328,
        size.height * 0.8857020);
    path_163.cubicTo(
        size.width * 0.8420611,
        size.height * 0.8904245,
        size.width * 0.8316718,
        size.height * 0.8975078,
        size.width * 0.8227939,
        size.height * 0.9068510);
    path_163.lineTo(size.width * 0.8213588, size.height * 0.9082882);
    path_163.lineTo(size.width * 0.8179237, size.height * 0.9038735);
    path_163.cubicTo(
        size.width * 0.8132824,
        size.height * 0.9110598,
        size.width * 0.7983359,
        size.height * 0.9370333,
        size.width * 0.8037710,
        size.height * 0.9705020);
    path_163.cubicTo(
        size.width * 0.8045725,
        size.height * 0.9752245,
        size.width * 0.8075267,
        size.height * 0.9788176,
        size.width * 0.8112824,
        size.height * 0.9794343);
    path_163.cubicTo(
        size.width * 0.8189618,
        size.height * 0.9806667,
        size.width * 0.8330305,
        size.height * 0.9824118,
        size.width * 0.8414198,
        size.height * 0.9795363);
    path_163.lineTo(size.width * 0.8443817, size.height * 0.9785098);
    path_163.lineTo(size.width * 0.8436565, size.height * 0.9837451);
    path_163.close();

    Paint paint_163_fill = Paint()..style = PaintingStyle.fill;
    paint_163_fill.color = Colors.black;
    canvas.drawPath(path_163, paint_163_fill);

    Path path_164 = Path();
    path_164.moveTo(size.width * 0.8764275, size.height * 0.9243735);
    path_164.cubicTo(
        size.width * 0.8743511,
        size.height * 0.9244765,
        size.width * 0.8725954,
        size.height * 0.9225255,
        size.width * 0.8723511,
        size.height * 0.9199588);
    path_164.cubicTo(
        size.width * 0.8701985,
        size.height * 0.8956275,
        size.width * 0.8594809,
        size.height * 0.8863882,
        size.width * 0.8530076,
        size.height * 0.8831029);
    path_164.cubicTo(
        size.width * 0.8567634,
        size.height * 0.8815627,
        size.width * 0.8604427,
        size.height * 0.8804333,
        size.width * 0.8638779,
        size.height * 0.8795098);
    path_164.cubicTo(
        size.width * 0.8640382,
        size.height * 0.8796118,
        size.width * 0.8641985,
        size.height * 0.8798176,
        size.width * 0.8642824,
        size.height * 0.8799206);
    path_164.cubicTo(
        size.width * 0.8705954,
        size.height * 0.8860804,
        size.width * 0.8782672,
        size.height * 0.8976814,
        size.width * 0.8801069,
        size.height * 0.9189324);
    path_164.cubicTo(
        size.width * 0.8804275,
        size.height * 0.9217039,
        size.width * 0.8786718,
        size.height * 0.9242706,
        size.width * 0.8764275,
        size.height * 0.9243735);
    path_164.close();

    Paint paint_164_fill = Paint()..style = PaintingStyle.fill;
    paint_164_fill.color = Colors.black;
    canvas.drawPath(path_164, paint_164_fill);

    Path path_165 = Path();
    path_165.moveTo(size.width * 0.9068931, size.height * 0.9241108);
    path_165.cubicTo(
        size.width * 0.9048092,
        size.height * 0.9241108,
        size.width * 0.9031298,
        size.height * 0.9220578,
        size.width * 0.9029695,
        size.height * 0.9193882);
    path_165.cubicTo(
        size.width * 0.9015344,
        size.height * 0.8958784,
        size.width * 0.8898626,
        size.height * 0.8828402,
        size.width * 0.8820305,
        size.height * 0.8766804);
    path_165.cubicTo(
        size.width * 0.8865038,
        size.height * 0.8764755,
        size.width * 0.8891450,
        size.height * 0.8766804,
        size.width * 0.8891450,
        size.height * 0.8766804);
    path_165.cubicTo(
        size.width * 0.8914656,
        size.height * 0.8768863,
        size.width * 0.8937023,
        size.height * 0.8772961,
        size.width * 0.8958626,
        size.height * 0.8777069);
    path_165.cubicTo(
        size.width * 0.9024962,
        size.height * 0.8859206,
        size.width * 0.9096870,
        size.height * 0.8991637,
        size.width * 0.9108092,
        size.height * 0.9186696);
    path_165.cubicTo(
        size.width * 0.9109695,
        size.height * 0.9215441,
        size.width * 0.9092061,
        size.height * 0.9241108,
        size.width * 0.9068931,
        size.height * 0.9241108);
    path_165.close();

    Paint paint_165_fill = Paint()..style = PaintingStyle.fill;
    paint_165_fill.color = Colors.black;
    canvas.drawPath(path_165, paint_165_fill);

    Path path_166 = Path();
    path_166.moveTo(size.width * 0.9348855, size.height * 0.9238157);
    path_166.cubicTo(
        size.width * 0.9328092,
        size.height * 0.9240216,
        size.width * 0.9309695,
        size.height * 0.9220706,
        size.width * 0.9307328,
        size.height * 0.9194010);
    path_166.cubicTo(
        size.width * 0.9292901,
        size.height * 0.9053363,
        size.width * 0.9252977,
        size.height * 0.8949676,
        size.width * 0.9220153,
        size.height * 0.8885000);
    path_166.cubicTo(
        size.width * 0.9274504,
        size.height * 0.8925029,
        size.width * 0.9320076,
        size.height * 0.8972255,
        size.width * 0.9357634,
        size.height * 0.9023588);
    path_166.cubicTo(
        size.width * 0.9369618,
        size.height * 0.9070814,
        size.width * 0.9379237,
        size.height * 0.9123176,
        size.width * 0.9384809,
        size.height * 0.9181696);
    path_166.cubicTo(
        size.width * 0.9387252,
        size.height * 0.9210441,
        size.width * 0.9371221,
        size.height * 0.9236108,
        size.width * 0.9348855,
        size.height * 0.9238157);
    path_166.close();

    Paint paint_166_fill = Paint()..style = PaintingStyle.fill;
    paint_166_fill.color = Colors.black;
    canvas.drawPath(path_166, paint_166_fill);

    Path path_167 = Path();
    path_167.moveTo(size.width * 0.8491985, size.height * 0.9171941);
    path_167.cubicTo(
        size.width * 0.8491221,
        size.height * 0.9167833,
        size.width * 0.8474427,
        size.height * 0.9077490,
        size.width * 0.8406489,
        size.height * 0.9042588);
    path_167.lineTo(size.width * 0.8400840, size.height * 0.9138059);
    path_167.lineTo(size.width * 0.8380076, size.height * 0.9133961);
    path_167.cubicTo(
        size.width * 0.8379313,
        size.height * 0.9133961,
        size.width * 0.8298550,
        size.height * 0.9120608,
        size.width * 0.8240992,
        size.height * 0.9133961);
    path_167.lineTo(size.width * 0.8222595, size.height * 0.9138059);
    path_167.lineTo(size.width * 0.8217786, size.height * 0.9114451);
    path_167.cubicTo(
        size.width * 0.8217786,
        size.height * 0.9114451,
        size.width * 0.8212214,
        size.height * 0.9088784,
        size.width * 0.8197863,
        size.height * 0.9056961);
    path_167.lineTo(size.width * 0.8232214, size.height * 0.9030265);
    path_167.cubicTo(
        size.width * 0.8240992,
        size.height * 0.9049775,
        size.width * 0.8247405,
        size.height * 0.9067225,
        size.width * 0.8251374,
        size.height * 0.9079539);
    path_167.cubicTo(
        size.width * 0.8292977,
        size.height * 0.9073382,
        size.width * 0.8338550,
        size.height * 0.9076461,
        size.width * 0.8364885,
        size.height * 0.9079539);
    path_167.lineTo(size.width * 0.8370458, size.height * 0.8975853);
    path_167.lineTo(size.width * 0.8392901, size.height * 0.8982010);
    path_167.cubicTo(
        size.width * 0.8504809,
        size.height * 0.9013843,
        size.width * 0.8530382,
        size.height * 0.9153461,
        size.width * 0.8531145,
        size.height * 0.9159618);
    path_167.lineTo(size.width * 0.8491985, size.height * 0.9171941);
    path_167.lineTo(size.width * 0.8511221, size.height * 0.9165784);
    path_167.lineTo(size.width * 0.8491985, size.height * 0.9171941);
    path_167.close();

    Paint paint_167_fill = Paint()..style = PaintingStyle.fill;
    paint_167_fill.color = Colors.black;
    canvas.drawPath(path_167, paint_167_fill);

    Path path_168 = Path();
    path_168.moveTo(size.width * 0.8436260, size.height * 0.9828824);
    path_168.lineTo(size.width * 0.8400305, size.height * 0.9807255);
    path_168.cubicTo(
        size.width * 0.8447481,
        size.height * 0.9679941,
        size.width * 0.8549771,
        size.height * 0.9669676,
        size.width * 0.8604962,
        size.height * 0.9683020);
    path_168.lineTo(size.width * 0.8676107, size.height * 0.9624500);
    path_168.lineTo(size.width * 0.8697634, size.height * 0.9667618);
    path_168.lineTo(size.width * 0.8612137, size.height * 0.9738461);
    path_168.lineTo(size.width * 0.8604122, size.height * 0.9735382);
    path_168.cubicTo(
        size.width * 0.8599313,
        size.height * 0.9733324,
        size.width * 0.8485038,
        size.height * 0.9698422,
        size.width * 0.8436260,
        size.height * 0.9828824);
    path_168.close();

    Paint paint_168_fill = Paint()..style = PaintingStyle.fill;
    paint_168_fill.color = Colors.black;
    canvas.drawPath(path_168, paint_168_fill);

    Path path_169 = Path();
    path_169.moveTo(size.width * 0.8114046, size.height * 0.9537049);
    path_169.cubicTo(
        size.width * 0.8066870,
        size.height * 0.9482637,
        size.width * 0.7942137,
        size.height * 0.9454912,
        size.width * 0.7940534,
        size.height * 0.9454912);
    path_169.lineTo(size.width * 0.7944580, size.height * 0.9424118);
    path_169.cubicTo(
        size.width * 0.7950153,
        size.height * 0.9425137,
        size.width * 0.8077252,
        size.height * 0.9453892,
        size.width * 0.8129237,
        size.height * 0.9514461);
    path_169.lineTo(size.width * 0.8114046, size.height * 0.9537049);
    path_169.close();

    Paint paint_169_fill = Paint()..style = PaintingStyle.fill;
    paint_169_fill.color = Colors.black;
    canvas.drawPath(path_169, paint_169_fill);

    Path path_170 = Path();
    path_170.moveTo(size.width * 0.7948168, size.height * 0.9617980);
    path_170.lineTo(size.width * 0.7937710, size.height * 0.9590265);
    path_170.cubicTo(
        size.width * 0.7942519,
        size.height * 0.9587186,
        size.width * 0.8049618,
        size.height * 0.9522500,
        size.width * 0.8127176,
        size.height * 0.9568706);
    path_170.lineTo(size.width * 0.8116794, size.height * 0.9596422);
    path_170.cubicTo(
        size.width * 0.8050458,
        size.height * 0.9556382,
        size.width * 0.7949695,
        size.height * 0.9616951,
        size.width * 0.7948168,
        size.height * 0.9617980);
    path_170.close();

    Paint paint_170_fill = Paint()..style = PaintingStyle.fill;
    paint_170_fill.color = Colors.black;
    canvas.drawPath(path_170, paint_170_fill);

    Path path_171 = Path();
    path_171.moveTo(size.width * 0.8357786, size.height * 0.9532088);
    path_171.lineTo(size.width * 0.8345802, size.height * 0.9505392);
    path_171.cubicTo(
        size.width * 0.8350611,
        size.height * 0.9502314,
        size.width * 0.8456107,
        size.height * 0.9424294,
        size.width * 0.8532061,
        size.height * 0.9431480);
    path_171.lineTo(size.width * 0.8530458, size.height * 0.9462275);
    path_171.cubicTo(
        size.width * 0.8461679,
        size.height * 0.9455088,
        size.width * 0.8359389,
        size.height * 0.9531059,
        size.width * 0.8357786,
        size.height * 0.9532088);
    path_171.close();

    Paint paint_171_fill = Paint()..style = PaintingStyle.fill;
    paint_171_fill.color = Colors.black;
    canvas.drawPath(path_171, paint_171_fill);

    Path path_172 = Path();
    path_172.moveTo(size.width * 0.8522443, size.height * 0.9616892);
    path_172.cubicTo(
        size.width * 0.8472061,
        size.height * 0.9558373,
        size.width * 0.8355420,
        size.height * 0.9591225,
        size.width * 0.8353817,
        size.height * 0.9591225);
    path_172.lineTo(size.width * 0.8348168, size.height * 0.9561451);
    path_172.cubicTo(
        size.width * 0.8353817,
        size.height * 0.9560422,
        size.width * 0.8477710,
        size.height * 0.9524490,
        size.width * 0.8537634,
        size.height * 0.9593275);
    path_172.lineTo(size.width * 0.8522443, size.height * 0.9616892);
    path_172.close();

    Paint paint_172_fill = Paint()..style = PaintingStyle.fill;
    paint_172_fill.color = Colors.black;
    canvas.drawPath(path_172, paint_172_fill);

    Path path_173 = Path();
    path_173.moveTo(size.width * 0.8274656, size.height * 0.9618265);
    path_173.cubicTo(
        size.width * 0.8273053,
        size.height * 0.9618265,
        size.width * 0.8271450,
        size.height * 0.9618265,
        size.width * 0.8269924,
        size.height * 0.9618265);
    path_173.cubicTo(
        size.width * 0.8257099,
        size.height * 0.9616216,
        size.width * 0.8246718,
        size.height * 0.9606971,
        size.width * 0.8239542,
        size.height * 0.9598765);
    path_173.cubicTo(
        size.width * 0.8229924,
        size.height * 0.9610059,
        size.width * 0.8219542,
        size.height * 0.9615186,
        size.width * 0.8208321,
        size.height * 0.9614157);
    path_173.cubicTo(
        size.width * 0.8181985,
        size.height * 0.9613137,
        size.width * 0.8164351,
        size.height * 0.9580284,
        size.width * 0.8161985,
        size.height * 0.9576176);
    path_173.lineTo(size.width * 0.8181985, size.height * 0.9558725);
    path_173.cubicTo(
        size.width * 0.8185191,
        size.height * 0.9564882,
        size.width * 0.8197176,
        size.height * 0.9582333,
        size.width * 0.8209160,
        size.height * 0.9583363);
    path_173.cubicTo(
        size.width * 0.8215496,
        size.height * 0.9583363,
        size.width * 0.8222748,
        size.height * 0.9577206,
        size.width * 0.8229924,
        size.height * 0.9565912);
    path_173.lineTo(size.width * 0.8240305, size.height * 0.9547431);
    path_173.lineTo(size.width * 0.8249924, size.height * 0.9566931);
    path_173.cubicTo(
        size.width * 0.8249924,
        size.height * 0.9566931,
        size.width * 0.8259466,
        size.height * 0.9585412,
        size.width * 0.8272290,
        size.height * 0.9587471);
    path_173.cubicTo(
        size.width * 0.8280305,
        size.height * 0.9588490,
        size.width * 0.8289084,
        size.height * 0.9583363,
        size.width * 0.8297863,
        size.height * 0.9571039);
    path_173.lineTo(size.width * 0.8314656, size.height * 0.9592598);
    path_173.cubicTo(
        size.width * 0.8301832,
        size.height * 0.9610059,
        size.width * 0.8288244,
        size.height * 0.9618265,
        size.width * 0.8274656,
        size.height * 0.9618265);
    path_173.close();

    Paint paint_173_fill = Paint()..style = PaintingStyle.fill;
    paint_173_fill.color = Colors.black;
    canvas.drawPath(path_173, paint_173_fill);

    Path path_174 = Path();
    path_174.moveTo(size.width * 0.8203740, size.height * 0.9396569);
    path_174.cubicTo(
        size.width * 0.8162137,
        size.height * 0.9348314,
        size.width * 0.8120611,
        size.height * 0.9394510,
        size.width * 0.8119008,
        size.height * 0.9396569);
    path_174.lineTo(size.width * 0.8102977, size.height * 0.9373980);
    path_174.cubicTo(
        size.width * 0.8122977,
        size.height * 0.9351392,
        size.width * 0.8173359,
        size.height * 0.9321618,
        size.width * 0.8219695,
        size.height * 0.9373980);
    path_174.lineTo(size.width * 0.8203740, size.height * 0.9396569);
    path_174.close();

    Paint paint_174_fill = Paint()..style = PaintingStyle.fill;
    paint_174_fill.color = Colors.black;
    canvas.drawPath(path_174, paint_174_fill);

    Path path_175 = Path();
    path_175.moveTo(size.width * 0.8388321, size.height * 0.9402314);
    path_175.cubicTo(
        size.width * 0.8342748,
        size.height * 0.9355088,
        size.width * 0.8307557,
        size.height * 0.9396147,
        size.width * 0.8303588,
        size.height * 0.9401284);
    path_175.lineTo(size.width * 0.8286794, size.height * 0.9379725);
    path_175.cubicTo(
        size.width * 0.8305115,
        size.height * 0.9356108,
        size.width * 0.8352290,
        size.height * 0.9326333,
        size.width * 0.8403511,
        size.height * 0.9378696);
    path_175.lineTo(size.width * 0.8388321, size.height * 0.9402314);
    path_175.close();

    Paint paint_175_fill = Paint()..style = PaintingStyle.fill;
    paint_175_fill.color = Colors.black;
    canvas.drawPath(path_175, paint_175_fill);

    Path path_176 = Path();
    path_176.moveTo(size.width * 0.8212061, size.height * 0.9467196);
    path_176.lineTo(size.width * 0.8273588, size.height * 0.9467196);
    path_176.cubicTo(
        size.width * 0.8284809,
        size.height * 0.9467196,
        size.width * 0.8289618,
        size.height * 0.9485676,
        size.width * 0.8281603,
        size.height * 0.9493892);
    path_176.lineTo(size.width * 0.8258397, size.height * 0.9518529);
    path_176.cubicTo(
        size.width * 0.8248015,
        size.height * 0.9529824,
        size.width * 0.8232824,
        size.height * 0.9528794,
        size.width * 0.8223282,
        size.height * 0.9516480);
    path_176.lineTo(size.width * 0.8204885, size.height * 0.9492863);
    path_176.cubicTo(
        size.width * 0.8196107,
        size.height * 0.9483627,
        size.width * 0.8201679,
        size.height * 0.9467196,
        size.width * 0.8212061,
        size.height * 0.9467196);
    path_176.close();

    Paint paint_176_fill = Paint()..style = PaintingStyle.fill;
    paint_176_fill.color = Colors.black;
    canvas.drawPath(path_176, paint_176_fill);

    Path path_177 = Path();
    path_177.moveTo(size.width * 0.9545802, size.height * 0.9721882);
    path_177.cubicTo(
        size.width * 0.9494656,
        size.height * 0.9721882,
        size.width * 0.9456260,
        size.height * 0.9681843,
        size.width * 0.9454656,
        size.height * 0.9679794);
    path_177.lineTo(size.width * 0.9480305, size.height * 0.9640775);
    path_177.cubicTo(
        size.width * 0.9480305,
        size.height * 0.9640775,
        size.width * 0.9509084,
        size.height * 0.9670549,
        size.width * 0.9545802,
        size.height * 0.9670549);
    path_177.lineTo(size.width * 0.9545802, size.height * 0.9721882);
    path_177.close();

    Paint paint_177_fill = Paint()..style = PaintingStyle.fill;
    paint_177_fill.color = Colors.black;
    canvas.drawPath(path_177, paint_177_fill);

    Path path_178 = Path();
    path_178.moveTo(size.width * 0.8222672, size.height * 0.8652314);
    path_178.lineTo(size.width * 0.8091603, size.height * 0.8652314);
    path_178.lineTo(size.width * 0.8164351, size.height * 0.8530147);
    path_178.lineTo(size.width * 0.8088397, size.height * 0.8530147);
    path_178.lineTo(size.width * 0.8088397, size.height * 0.8499353);
    path_178.lineTo(size.width * 0.8213130, size.height * 0.8499353);
    path_178.lineTo(size.width * 0.8140382, size.height * 0.8621520);
    path_178.lineTo(size.width * 0.8222672, size.height * 0.8621520);
    path_178.lineTo(size.width * 0.8222672, size.height * 0.8652314);
    path_178.close();

    Paint paint_178_fill = Paint()..style = PaintingStyle.fill;
    paint_178_fill.color = Colors.black;
    canvas.drawPath(path_178, paint_178_fill);

    Path path_179 = Path();
    path_179.moveTo(size.width * 0.8368702, size.height * 0.8720833);
    path_179.lineTo(size.width * 0.8261603, size.height * 0.8720833);
    path_179.lineTo(size.width * 0.8323130, size.height * 0.8641784);
    path_179.lineTo(size.width * 0.8279160, size.height * 0.8641784);
    path_179.lineTo(size.width * 0.8279160, size.height * 0.8610980);
    path_179.lineTo(size.width * 0.8380687, size.height * 0.8610980);
    path_179.lineTo(size.width * 0.8319160, size.height * 0.8690039);
    path_179.lineTo(size.width * 0.8368702, size.height * 0.8690039);
    path_179.lineTo(size.width * 0.8368702, size.height * 0.8720833);
    path_179.close();

    Paint paint_179_fill = Paint()..style = PaintingStyle.fill;
    paint_179_fill.color = Colors.black;
    canvas.drawPath(path_179, paint_179_fill);

    Path path_180 = Path();
    path_180.moveTo(size.width * 0.8245496, size.height * 0.8863216);
    path_180.lineTo(size.width * 0.8135191, size.height * 0.8863216);
    path_180.lineTo(size.width * 0.8196794, size.height * 0.8769784);
    path_180.lineTo(size.width * 0.8144809, size.height * 0.8769784);
    path_180.lineTo(size.width * 0.8144809, size.height * 0.8738990);
    path_180.lineTo(size.width * 0.8248702, size.height * 0.8738990);
    path_180.lineTo(size.width * 0.8187176, size.height * 0.8832412);
    path_180.lineTo(size.width * 0.8245496, size.height * 0.8832412);
    path_180.lineTo(size.width * 0.8245496, size.height * 0.8863216);
    path_180.close();

    Paint paint_180_fill = Paint()..style = PaintingStyle.fill;
    paint_180_fill.color = Colors.black;
    canvas.drawPath(path_180, paint_180_fill);

    Path path_181 = Path();
    path_181.moveTo(size.width * 0.7250832, size.height * 0.9516696);
    path_181.lineTo(size.width * 0.7855191, size.height * 0.9516696);
    path_181.cubicTo(
        size.width * 0.7877557,
        size.height * 0.9516696,
        size.width * 0.7897557,
        size.height * 0.9537225,
        size.width * 0.7905496,
        size.height * 0.9568029);
    path_181.lineTo(size.width * 0.7960687, size.height * 0.9774382);
    path_181.cubicTo(
        size.width * 0.7975038,
        size.height * 0.9827745,
        size.width * 0.7948702,
        size.height * 0.9885294,
        size.width * 0.7910305,
        size.height * 0.9885294);
    path_181.lineTo(size.width * 0.7194878, size.height * 0.9885294);
    path_181.cubicTo(
        size.width * 0.7156511,
        size.height * 0.9885294,
        size.width * 0.7130130,
        size.height * 0.9827745,
        size.width * 0.7144519,
        size.height * 0.9774382);
    path_181.lineTo(size.width * 0.7199679, size.height * 0.9568029);
    path_181.cubicTo(
        size.width * 0.7208466,
        size.height * 0.9537225,
        size.width * 0.7228450,
        size.height * 0.9516696,
        size.width * 0.7250832,
        size.height * 0.9516696);
    path_181.close();

    Paint paint_181_fill = Paint()..style = PaintingStyle.fill;
    paint_181_fill.color = Colors.white;
    canvas.drawPath(path_181, paint_181_fill);

    Path path_182 = Path();
    path_182.moveTo(size.width * 0.7910458, size.height * 0.9911961);
    path_182.lineTo(size.width * 0.7194992, size.height * 0.9911961);
    path_182.cubicTo(
        size.width * 0.7172611,
        size.height * 0.9911961,
        size.width * 0.7151031,
        size.height * 0.9897549,
        size.width * 0.7136641,
        size.height * 0.9871863);
    path_182.cubicTo(
        size.width * 0.7119855,
        size.height * 0.9842059,
        size.width * 0.7115855,
        size.height * 0.9803088,
        size.width * 0.7125450,
        size.height * 0.9767157);
    path_182.lineTo(size.width * 0.7180603, size.height * 0.9560804);
    path_182.cubicTo(
        size.width * 0.7191794,
        size.height * 0.9519745,
        size.width * 0.7218969,
        size.height * 0.9493049,
        size.width * 0.7250145,
        size.height * 0.9493049);
    path_182.lineTo(size.width * 0.7854504, size.height * 0.9493049);
    path_182.cubicTo(
        size.width * 0.7885649,
        size.height * 0.9493049,
        size.width * 0.7912824,
        size.height * 0.9519745,
        size.width * 0.7924046,
        size.height * 0.9560804);
    path_182.lineTo(size.width * 0.7979160, size.height * 0.9767157);
    path_182.cubicTo(
        size.width * 0.7988779,
        size.height * 0.9803088,
        size.width * 0.7984733,
        size.height * 0.9842059,
        size.width * 0.7968015,
        size.height * 0.9871863);
    path_182.cubicTo(
        size.width * 0.7954427,
        size.height * 0.9897549,
        size.width * 0.7933588,
        size.height * 0.9911961,
        size.width * 0.7910458,
        size.height * 0.9911961);
    path_182.close();
    path_182.moveTo(size.width * 0.7250947, size.height * 0.9544382);
    path_182.cubicTo(
        size.width * 0.7237359,
        size.height * 0.9544382,
        size.width * 0.7224573,
        size.height * 0.9557725,
        size.width * 0.7219771,
        size.height * 0.9578265);
    path_182.lineTo(size.width * 0.7164618, size.height * 0.9784618);
    path_182.cubicTo(
        size.width * 0.7159023,
        size.height * 0.9804118,
        size.width * 0.7161420,
        size.height * 0.9826667,
        size.width * 0.7171008,
        size.height * 0.9843137);
    path_182.cubicTo(
        size.width * 0.7175008,
        size.height * 0.9850294,
        size.width * 0.7183000,
        size.height * 0.9861569,
        size.width * 0.7196588,
        size.height * 0.9861569);
    path_182.lineTo(size.width * 0.7912061, size.height * 0.9861569);
    path_182.cubicTo(
        size.width * 0.7925649,
        size.height * 0.9861569,
        size.width * 0.7933588,
        size.height * 0.9850294,
        size.width * 0.7937634,
        size.height * 0.9843137);
    path_182.cubicTo(
        size.width * 0.7946412,
        size.height * 0.9826667,
        size.width * 0.7948779,
        size.height * 0.9804118,
        size.width * 0.7943969,
        size.height * 0.9784618);
    path_182.lineTo(size.width * 0.7888855, size.height * 0.9578265);
    path_182.cubicTo(
        size.width * 0.7883282,
        size.height * 0.9557725,
        size.width * 0.7871221,
        size.height * 0.9544382,
        size.width * 0.7857634,
        size.height * 0.9544382);
    path_182.lineTo(size.width * 0.7250947, size.height * 0.9544382);
    path_182.close();

    Paint paint_182_fill = Paint()..style = PaintingStyle.fill;
    paint_182_fill.color = Colors.black;
    canvas.drawPath(path_182, paint_182_fill);

    Path path_183 = Path();
    path_183.moveTo(size.width * 0.9967634, size.height * 0.9864216);
    path_183.lineTo(size.width * 0.02153656, size.height * 0.9864216);
    path_183.lineTo(size.width * 0.02153656, size.height * 0.9915588);
    path_183.lineTo(size.width * 0.9967634, size.height * 0.9915588);
    path_183.lineTo(size.width * 0.9967634, size.height * 0.9864216);
    path_183.close();

    Paint paint_183_fill = Paint()..style = PaintingStyle.fill;
    paint_183_fill.color = Colors.black;
    canvas.drawPath(path_183, paint_183_fill);
  }

  @override
  bool shouldRepaint(covariant TimeFoodCustomPainter oldDelegate) {
    return oldDelegate.active != active;
  }
}

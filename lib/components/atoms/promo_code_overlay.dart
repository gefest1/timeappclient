import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/utils/color.dart';

class PromoCodeOverlayLoader extends OverlayEntry {
  final String text;

  PromoCodeOverlayLoader({
    required this.text,
  }) : super(
          builder: (_) {
            return SizedBox();
          },
        );

  Widget _buider(BuildContext context) {
    return PromoCodeWidget(overlayEntry: this, text: text);
  }

  WidgetBuilder get builder => _buider;
}

class PromoCodeWidget extends StatefulWidget {
  final PromoCodeOverlayLoader overlayEntry;
  final String text;

  const PromoCodeWidget({
    required this.overlayEntry,
    required this.text,
    Key? key,
  }) : super(key: key);

  @override
  _PromoCodeWidgetState createState() => _PromoCodeWidgetState();
}

class _PromoCodeWidgetState extends State<PromoCodeWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController animationController = AnimationController(
    vsync: this,
    duration: Duration(
      // seconds: 5,
      milliseconds: 850,
    ),
    value: 0,
  );
  late Animation<Offset> offset =
      Tween<Offset>(begin: Offset.zero, end: Offset(0.0, -0.5))
          .animate(animationController);

  void _init() async {
    await animationController.forward();
    await Future.delayed(Duration(seconds: 2));
    await animationController.reverse();
    widget.overlayEntry.remove();
  }

  @override
  void initState() {
    super.initState();
    _init();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bottomPadding = MediaQuery.of(context).padding.bottom;
    return Positioned(
      bottom: bottomPadding + 180,
      left: 20,
      right: 20,
      child: FadeTransition(
        opacity: animationController,
        child: SlideTransition(
          position: offset,
          child: Material(
            color: Color(0xe6424242),
            borderRadius: BorderRadius.circular(16),
            child: Padding(
              padding: EdgeInsets.all(16),
              child: Row(
                children: [
                  Icon(
                    Icons.done,
                    size: 24,
                    color: Colors.white,
                  ),
                  const SizedBox(width: 10),
                  Expanded(
                    child: Text(
                      widget.text,
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        height: 19 / 16,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

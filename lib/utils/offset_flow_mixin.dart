import 'package:flutter/material.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

mixin OffsetExtension {
  YandexMapController? yandexMapController;
  Future<Offset?> getOffset({
    required double latitude,
    required double longitude,
  }) async {
    if (this.yandexMapController == null) return null;
    final _dirtOffset = await this.yandexMapController!.getScreenPoint(
          Point(
            latitude: latitude,
            longitude: longitude,
          ),
        );
    if (_dirtOffset == null) return Offset.zero;
    return Offset(
      _dirtOffset.x,
      _dirtOffset.y,
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/const.dart';
import 'package:li/utils/color.dart';

class HeadTrain extends StatelessWidget {
  const HeadTrain({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 0,
      right: 0,
      top: 0,
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          boxShadow: bigBlur,
          color: Colors.white,
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(16),
          ),
        ),
        padding: EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: 20,
            ),
            Text(
              'Подтверждение',
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 18,
                height: 21 / 18,
                color: ColorData.allMainBlack,
              ),
            ),
            InkWell(
              onTap: () => Navigator.pop(context),
              child: SvgPicture.asset(
                'assets/svg/icon/close.svg',
                color: ColorData.allMainBlack,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/utils/auto_size_group_text.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/text_styles.dart';
import 'package:time_app_components/utils/testStyles.dart';

class BottomDetailData extends StatelessWidget {
  final num price;

  final String? rating, ratingCount, locationLabel;

  const BottomDetailData({
    required this.price,
    this.rating,
    this.ratingCount,
    this.locationLabel,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            height: 30,
            decoration: BoxDecoration(
              color: ColorData.allMainBlack,
              borderRadius: BorderRadius.circular(6),
            ),
            padding: EdgeInsets.symmetric(
              vertical: 6,
              horizontal: 10,
            ),
            alignment: Alignment.center,
            child: FittedBox(
              child: AutoSizeText.rich(
                TextSpan(
                  children: [
                    TextSpan(
                      text: price.toString(),
                      style: TextStyle(
                        fontFamily: Platform.isIOS ? 'SfProDisplay' : 'Roboto',
                      ),
                    ),
                    TextSpan(
                      text: ' ₸',
                    ),
                    TextSpan(
                      text: '/мин',
                      style: TextStyle(
                        fontFamily: Platform.isIOS ? 'SfProDisplay' : 'Roboto',
                      ),
                    ),
                  ],
                ),

                maxFontSize: P3TextStyle.fontSize,
                minFontSize: 1,
                style: TextStyle(
                  fontSize: P3TextStyle.fontSize,
                  fontWeight: P3TextStyle.fontWeight,
                  height: P3TextStyle.height,
                  color: Colors.white,
                ),
                // maxFontSize: P3TextStyle.fontSize,
              ),
            ),
          ),
        ),
        const SizedBox(width: 10),
        Expanded(
          child: rating == null
              ? const SizedBox()
              : Container(
                  height: 30,
                  decoration: BoxDecoration(
                    color: ColorData.allMessagesbackground,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  alignment: Alignment.center,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      RepaintBoundary(
                        child: SvgPicture.asset(
                          'assets/svg/icon/star.svg',
                          color: Color(0xff00DF31),
                          height: 16,
                          width: 16,
                        ),
                      ),
                      const SizedBox(width: 2),
                      Flexible(
                        child: Text(
                          rating!,
                          style: TextStyle(
                            color: const Color(0xff00DF31),
                            fontSize: P3TextStyle.fontSize,
                            fontWeight: P3TextStyle.fontWeight,
                            height: P3TextStyle.height,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
        ),
        const SizedBox(width: 10),
        Expanded(
          child: ratingCount == null
              ? const SizedBox()
              : Container(
                  height: 30,
                  decoration: BoxDecoration(
                    color: ColorData.allMessagesbackground,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: Center(
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        RepaintBoundary(
                          child: SvgPicture.asset(
                            'assets/svg/icon/bullHorn.svg',
                            color: ColorData.allMainBlack,
                            height: 16,
                            width: 16,
                          ),
                        ),
                        const SizedBox(width: 2),
                        Flexible(
                          child: Text(
                            ratingCount!,
                            style: TextStyle(
                              color: ColorData.allMainBlack,
                              fontSize: P3TextStyle.fontSize,
                              fontWeight: P3TextStyle.fontWeight,
                              height: P3TextStyle.height,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
        ),
        const SizedBox(width: 10),
        Expanded(
          child: locationLabel == null
              ? const SizedBox()
              : Container(
                  height: 30,
                  decoration: BoxDecoration(
                    color: ColorData.allMessagesbackground,
                    borderRadius: BorderRadius.circular(6),
                  ),
                  child: Center(
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SvgPicture.asset(
                          'assets/svg/icon/sendFilled.svg',
                          color: ColorData.allMainBlack,
                          height: 16,
                          width: 16,
                        ),
                        const SizedBox(width: 2),
                        Flexible(
                          child: Text(
                            locationLabel!,
                            style: TextStyle(
                              color: ColorData.allMainBlack,
                              fontSize: P3TextStyle.fontSize,
                              fontWeight: P3TextStyle.fontWeight,
                              height: P3TextStyle.height,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
        ),
      ],
    );
  }
}

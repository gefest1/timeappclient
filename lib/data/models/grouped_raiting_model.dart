import 'dart:convert';

import 'package:equatable/equatable.dart';

class GroupedRatingModel extends Equatable {
  final int? fiveStars;
  final int? fourStars;
  final int? oneStars;
  final int? threeStars;
  final int? twoStars;

  GroupedRatingModel({
    required this.fiveStars,
    required this.fourStars,
    required this.oneStars,
    required this.threeStars,
    required this.twoStars,
  });

  GroupedRatingModel copyWith({
    int? fiveStars,
    int? fourStars,
    int? oneStars,
    int? threeStars,
    int? twoStars,
  }) {
    return GroupedRatingModel(
      fiveStars: fiveStars ?? this.fiveStars,
      fourStars: fourStars ?? this.fourStars,
      oneStars: oneStars ?? this.oneStars,
      threeStars: threeStars ?? this.threeStars,
      twoStars: twoStars ?? this.twoStars,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'fiveStars': fiveStars,
      'fourStars': fourStars,
      'oneStars': oneStars,
      'threeStars': threeStars,
      'twoStars': twoStars,
    };
  }

  factory GroupedRatingModel.fromMap(Map<String, dynamic> map) {
    return GroupedRatingModel(
      fiveStars: map['fiveStars'] != null ? map['fiveStars'] : null,
      fourStars: map['fourStars'] != null ? map['fourStars'] : null,
      oneStars: map['oneStars'] != null ? map['oneStars'] : null,
      threeStars: map['threeStars'] != null ? map['threeStars'] : null,
      twoStars: map['twoStars'] != null ? map['twoStars'] : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory GroupedRatingModel.fromJson(String source) =>
      GroupedRatingModel.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props {
    return [
      fiveStars!,
      fourStars!,
      oneStars!,
      threeStars!,
      twoStars!,
    ];
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/const.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/text_styles.dart';
import 'package:time_app_components/utils/testStyles.dart';

class SearchCard extends StatelessWidget {
  final String? nameTitle, nameLabel, namePhoto;

  final String? title;
  final String? costTitle;
  final String? typelabel;

  final String? descriptionTitle;
  final String? description;

  final VoidCallback? onTap;

  const SearchCard({
    this.nameTitle,
    this.nameLabel,
    this.namePhoto,
    this.title,
    this.costTitle,
    this.typelabel,
    this.descriptionTitle,
    this.description,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            color: const Color(0xffF8F8F8),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                IntrinsicHeight(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (namePhoto != null)
                        CircleAvatar(
                          radius: 34.5,
                          backgroundImage: NetworkImage(
                            namePhoto!,
                          ),
                        ),
                      const SizedBox(width: 10),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            if (nameTitle != null)
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: nameTitle,
                                      style: const TextStyle(
                                        fontSize: H2TextStyle.fontSize,
                                        fontWeight: H2TextStyle.fontWeight,
                                        height: H2TextStyle.height,
                                        color: Colors.black,
                                      ),
                                    ),
                                    WidgetSpan(
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                          left: 2,
                                          bottom: 2,
                                        ),
                                        child: SvgPicture.asset(
                                          'assets/svg/icon/ExecutorInfoIcons/ExecutorOnline.svg',
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            const SizedBox(height: 2),
                            if (nameLabel != null)
                              Text(
                                nameLabel!,
                                style: const TextStyle(
                                  fontSize: P3TextStyle.fontSize,
                                  fontWeight: P3TextStyle.fontWeight,
                                  height: P3TextStyle.height,
                                  color: ColorData.allMainActivegray,
                                ),
                              ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                const SizedBox(height: 20),
                DecoratedBox(
                  decoration: BoxDecoration(
                    boxShadow: bigBlur,
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  if (title != null) ...[
                                    Text(
                                      title!,
                                      style: const TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                        height: 21 / 18,
                                      ),
                                    ),
                                    const SizedBox(height: 5),
                                  ],
                                  if (costTitle != null)
                                    Text(
                                      costTitle!,
                                      style: const TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600,
                                        height: 19 / 16,
                                        color: ColorData.allMainBlack,
                                      ),
                                    ),
                                  if (typelabel != null)
                                    Text(
                                      typelabel!,
                                      style: const TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        height: 17 / 14,
                                        color: ColorData.allMainActivegray,
                                      ),
                                    )
                                ],
                              ),
                            ),
                            SvgPicture.asset(
                              'assets/svg/icon/ExecutorInfoIcons/tattooMachine.svg',
                            )
                          ],
                        ),
                        const SizedBox(height: 16),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if (descriptionTitle != null) ...[
                              Text(
                                descriptionTitle!,
                                style: const TextStyle(
                                  fontWeight: FontWeight.w600,
                                  height: 17 / 14,
                                  color: ColorData.allMainBlack,
                                  fontSize: 14,
                                ),
                              ),
                              const SizedBox(height: 2),
                            ],
                            if (description != null)
                              Text(
                                description!,
                                style: const TextStyle(
                                  fontWeight: FontWeight.w400,
                                  height: 17 / 14,
                                  color: ColorData.allMainActivegray,
                                  fontSize: 14,
                                ),
                                textAlign: TextAlign.left,
                              ),
                          ],
                        ),
                        const SizedBox(height: 16),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const SizedBox(height: 4),
                            SizedBox(
                              height: 105,
                              child: ListView.separated(
                                separatorBuilder: (_, __) =>
                                    const SizedBox(width: 5),
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (context, index) => Container(
                                  width: 105,
                                  height: 105,
                                  decoration: BoxDecoration(
                                    color: Colors.orangeAccent,
                                    borderRadius: BorderRadius.circular(6),
                                  ),
                                ),
                                itemCount: 20,
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          RepaintBoundary(
                            child: SvgPicture.asset(
                                'assets/svg/icon/ExecutorInfoIcons/starrating.svg'),
                          ),
                          const SizedBox(width: 2),
                          const Text(
                            '5,0',
                            style: TextStyle(
                              fontSize: 16,
                              color: Color(0xff00DF31),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          RepaintBoundary(
                            child: SvgPicture.asset(
                                'assets/svg/icon/ExecutorInfoIcons/smilerating.svg'),
                          ),
                          const SizedBox(width: 2),
                          const Text(
                            '100',
                            style: TextStyle(
                              fontSize: 16,
                              color: Color(0xff00DF31),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          RepaintBoundary(
                            child: SvgPicture.asset(
                                'assets/svg/icon/ExecutorInfoIcons/repost.svg'),
                          ),
                          const SizedBox(width: 2),
                          const Text(
                            '0',
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          RepaintBoundary(
                            child: SvgPicture.asset(
                                'assets/svg/icon/ExecutorInfoIcons/location.svg'),
                          ),
                          const SizedBox(width: 2),
                          const Text(
                            '500м',
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

class LocationCustomPainter extends CustomPainter {
  const LocationCustomPainter({Listenable? repaint}) : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawPath(
      Path.combine(
        PathOperation.difference,
        Path()
          ..moveTo(size.width * 0.9166700, size.height * 0.4760229)
          ..cubicTo(
              size.width * 0.9166700,
              size.height * 0.6436714,
              size.width * 0.6728450,
              size.height * 0.8567048,
              size.width * 0.5582350,
              size.height * 0.9475810)
          ..cubicTo(
              size.width * 0.5241600,
              size.height * 0.9746000,
              size.width * 0.4758415,
              size.height * 0.9746000,
              size.width * 0.4417685,
              size.height * 0.9475810)
          ..cubicTo(
              size.width * 0.3271550,
              size.height * 0.8567048,
              size.width * 0.08333450,
              size.height * 0.6436714,
              size.width * 0.08333450,
              size.height * 0.4760229)
          ..cubicTo(
              size.width * 0.08333450,
              size.height * 0.2568624,
              size.width * 0.2698825,
              size.height * 0.07919762,
              size.width * 0.5000000,
              size.height * 0.07919762)
          ..cubicTo(
              size.width * 0.7301200,
              size.height * 0.07919762,
              size.width * 0.9166700,
              size.height * 0.2568624,
              size.width * 0.9166700,
              size.height * 0.4760229)
          ..close(),
        Path()
          ..addOval(
            Rect.fromCenter(
              center: Offset(size.width * 0.5000000, size.height * 0.4760248),
              width: size.width * 0.2500000,
              height: size.height * 0.2380952,
            ),
          ),
      ),
      Paint()
        ..style = PaintingStyle.fill
        ..color = Colors.white,
    );
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}

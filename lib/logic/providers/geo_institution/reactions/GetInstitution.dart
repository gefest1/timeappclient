part of '../GeoInstitutionProvider.dart';

extension GetInstitution on GeoInstitutionProvider {
  Future<List<InstitutionsMinute>> getInstituion({
    required Point point,
    double? maxRadius = 5000,
    double? minRadius = 0,
  }) async {
    final QueryResult queryResult = await client.query(
      QueryOptions(
        document: getRadiusQuery,
        variables: {
          "latitude": point.latitude,
          "longitude": point.longitude,
          "maxMetres": maxRadius,
          "minMetres": minRadius,
        },
        fetchPolicy: FetchPolicy.cacheFirst,
      ),
    );
    if (queryResult.hasException) throw queryResult.exception!;

    final List<InstitutionsMinute> institutions =
        (queryResult.data!['getInstitutionCord'] as List<dynamic>).map(
      (e) {
        final _i = InstitutionsMinute.fromMap(e);

        institutionsMinuteMap[_i.id!] = _i;
        return _i;
      },
    ).toList();

    institutionsMinuteList = institutionsMinuteMap.values.toList();
    // ignore: invalid_use_of_protected_member, invalid_use_of_visible_for_testing_member
    notifyListeners();
    return institutions;
  }
}

import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:li/data/models/location.dart';
import 'package:li/data/models/photo_url.dart';
import 'package:li/data/models/specialist.dart';

class InstitutionTakeAway extends Equatable {
  final String? id;
  final Location? location;
  final String? name;
  final PhotoUrl? photoURL;
  final String? specialistId;
  final Specialist? specialist;

  const InstitutionTakeAway({
    this.id,
    this.location,
    this.name,
    this.photoURL,
    this.specialistId,
    this.specialist,
  });

  InstitutionTakeAway copyWith({
    String? id,
    Location? location,
    String? name,
    PhotoUrl? photoURL,
    String? specialistId,
    Specialist? specialist,
  }) {
    return InstitutionTakeAway(
      id: id ?? this.id,
      location: location ?? this.location,
      name: name ?? this.name,
      photoURL: photoURL ?? this.photoURL,
      specialistId: specialistId ?? this.specialistId,
      specialist: specialist ?? this.specialist,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'location': location?.toMap(),
      'name': name,
      'photoURL': photoURL?.toMap(),
      'specialistId': specialistId,
      'specialist': specialist?.toMap(),
    };
  }

  factory InstitutionTakeAway.fromMap(Map<String, dynamic> map) {
    return InstitutionTakeAway(
      id: map['id'],
      location:
          map['location'] != null ? Location.fromMap(map['location']) : null,
      name: map['name'],
      photoURL:
          map['photoURL'] != null ? PhotoUrl.fromMap(map['photoURL']) : null,
      specialistId: map['specialistId'],
      specialist: map['specialist'] != null
          ? Specialist.fromMap(map['specialist'])
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory InstitutionTakeAway.fromJson(String source) =>
      InstitutionTakeAway.fromMap(json.decode(source));

  @override
  String toString() {
    return 'InstitutionTakeAway(id: $id, location: $location, name: $name, photoURL: $photoURL, specialistId: $specialistId, specialist: $specialist)';
  }

  @override
  List<Object?> get props {
    return [
      id,
      location,
      name,
      photoURL,
      specialistId,
      specialist,
    ];
  }
}

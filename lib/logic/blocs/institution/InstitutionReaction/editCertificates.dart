part of '../InstitutionBloc.dart';

extension EditCertificates on InstitutionBloc {
  void editCertificate(
    EditDataCertificatgeInstitutionEvent event,
    Emitter<InstitutionState> emit,
  ) async {
    final _mutationResult = await this.graphqlProvider.client.mutate(
          MutationOptions(
            document: gql(''' 
            mutation(
            	\$photo: EditPhoto!
              \$institutionId: String!
            ) {
              editCertificiate(
                institutionId: \$institutionId
                photo: \$photo
              ) {
                _id
              }
            }
            '''),
            variables: {
              'institutionId': "6163b455b55cadcc91229b3b",
              "photo": {
                "addPhoto": [
                  {
                    "photo": await MultipartFile.fromPath('', event.file.path),
                  }
                ],
                "removePhoto": []
              }
            },
          ),
        );
    if (_mutationResult.hasException) throw _mutationResult.exception!;
    // log(_mutationResult.data.toString());
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class TileCategory extends StatelessWidget {
  final String title, svgPath;

  const TileCategory({required this.title, required this.svgPath, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: const Color(0xffF9F9F9),
        borderRadius: BorderRadius.circular(16),
      ),
      height: 76,
      child: Row(
        children: [
          const SizedBox(width: 16),
          SvgPicture.network(
            svgPath,
            fit: BoxFit.cover,
            height: 24,
            width: 24,
          ),
          const SizedBox(width: 10),
          Expanded(
            child: Text(
              title,
              style: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w400,
                height: 21 / 18,
                color: Colors.black,
              ),
            ),
          ),
          const SizedBox(width: 10),
          SvgPicture.asset(
            'assets/svg/icon/arrowChevronForward.svg',
            height: 18,
            width: 18,
            color: const Color(0xffD0D0D0),
          ),
          const SizedBox(width: 16),
        ],
      ),
    );
  }
}

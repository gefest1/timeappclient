import 'package:flutter/material.dart';
import 'package:li/components/atoms/leading_icon.dart';
import 'package:li/components/pages/qfit_to_timeapp/map_view/map_view.dart';
import 'package:li/data/models/institution_minute.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';

class MinutePagePage extends StatelessWidget {
  final InstitutionsMinute institutionsMinute;

  const MinutePagePage({
    required this.institutionsMinute,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Stack(
        children: [
          MapView(
            institutions: [institutionsMinute],
          ),
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            child: CustomAppBar(
              title: Text(
                '${institutionsMinute.name}',
                style: TextStyle(
                  fontSize: 18,
                  height: 21 / 18,
                  fontWeight: FontWeight.w400,
                  color: ColorData.allMainBlack,
                ),
              ),
              leading: const LeadingIcon(),
            ),
          )
        ],
      ),
    );
  }
}

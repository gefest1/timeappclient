import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:li/data/models/allowed_session_types.dart';
import 'package:li/data/models/client_model.dart';

class GlobalSessionModel extends Equatable {
  final String? id; 
  final String? clientId;
  final Client? client; 
  final bool? isActive; 
  final dynamic session;
  final String? sessionId;
  final AllowedSessionTypes? type; 

  const GlobalSessionModel({
    this.id,
    this.client,
    this.clientId,
    this.isActive,
    this.session,
    this.sessionId,
    this.type,
  });

  GlobalSessionModel copyWith({
    String? id,
    Client? client,
    String? clientId,
    bool? isActive,
    dynamic session,
    String? sessionId,
    AllowedSessionTypes? type,
  }) {
    return GlobalSessionModel(
      id: id ?? this.id,
      client: client ?? this.client,
      clientId: clientId ?? this.clientId,
      isActive: isActive ?? this.isActive,
      session: session ?? this.session,
      sessionId: sessionId ?? this.sessionId,
      type: type ?? this.type,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'client': client?.toMap(),
      'clientId': clientId,
      'isActive': isActive,
      'session': session,
      'sessionId': sessionId,
      'type': type?.toMap(),
    };
  }

  factory GlobalSessionModel.fromMap(Map<String, dynamic> map) {
    return GlobalSessionModel(
      id: map['_id'] != null ? map['_id'] : null,
      client: map['client'] != null ? Client.fromMap(map['client']) : null,
      clientId: map['clientId'] != null ? map['clientId'] : null,
      isActive: map['isActive'] != null ? map['isActive'] : null,
      session: map['session'] != null ? map['session'] : null,
      sessionId: map['sessionId'] != null ? map['sessionId'] : null,
      type:
          map['type'] != null ? AllowedSessionTypes.fromMap(map['type']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory GlobalSessionModel.fromJson(String source) =>
      GlobalSessionModel.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object?> get props {
    return [
      id,
      client,
      clientId,
      isActive,
      session,
      sessionId,
      type,
    ];
  }
}

import 'package:flutter/foundation.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/data/models/credit_card_model.dart';
import 'package:li/network/graphql_provider.dart';

class CreditCardProvider extends ChangeNotifier {
  final GraphqlProvider graphqlProvider;
  List<CreditCardModel> creditCards = [];
  CreditCardProvider(this.graphqlProvider) {
    initS();
  }
  void initS() async {
    while (true) {
      try {
        getData();
        break;
      } catch (e) {
        await Future.delayed(Duration(seconds: 2));
      }
    }
  }

  void getData() async {
    final _query = await graphqlProvider.client.query(
      QueryOptions(
        document: gql('''{
            getCreditCards {            
              _id
            	cardType
              last4Digits
              cardHolderName          	
          }
        }'''),
      ),
    );
    final List<CreditCardModel> _newValue = _query.hasException
        ? []
        : (_query.data!['getCreditCards'] as List<dynamic>?)
                ?.map((e) => CreditCardModel.fromMap(e)!)
                .toList() ??
            [];

    if (listEquals(_newValue, creditCards)) return;
    creditCards = _newValue;
    notifyListeners();
  }

  void saveCard({
    required String cardHolderName,
    required String cardToken,
    required String cardType,
    required String last4Digits,
  }) async {
    final mutationResult = await graphqlProvider.client.mutate(
      MutationOptions(
        document: gql(''' mutation(
            \$cardHolderName: String!
            \$cardToken: String!
            \$cardType: String!
            \$last4Digits: String!	
          ) {
            saveCreditCard(
              cardHolderName: \$cardHolderName
              cardToken: \$cardToken
              cardType: \$cardType
              last4Digits: \$last4Digits
            ) {
            _id
            cardType
            last4Digits
            cardHolderName
          }
        }'''),
        variables: {
          'cardHolderName': cardHolderName,
          'cardToken': cardToken,
          'cardType': cardType,
          'last4Digits': last4Digits,
        },
      ),
    );
    if (mutationResult.hasException) throw mutationResult.exception!;

    final _newValue = (mutationResult.data!['saveCreditCard'] as List<dynamic>?)
            ?.map((e) => CreditCardModel.fromMap(e)!)
            .toList() ??
        [];

    if (listEquals(_newValue, creditCards)) return;
    creditCards = _newValue;
    notifyListeners();
  }

  void removeCard({required String cardId}) async {
    creditCards.removeWhere((element) {
      return element.id == cardId;
    });
    notifyListeners();
    final mutationResult = await graphqlProvider.client.mutate(
      MutationOptions(
        document: gql('''
          mutation(
            \$cardId: String!
          ) {
            removeCreditCard(cardId: \$cardId) {
              _id
              cardType
              last4Digits
              cardHolderName
            }
          }'''),
        variables: {
          "cardId": cardId,
        },
      ),
    );
    if (mutationResult.hasException) throw mutationResult.exception!;
    final _newValue =
        (mutationResult.data!['removeCreditCard'] as List<dynamic>?)
                ?.map((e) => CreditCardModel.fromMap(e)!)
                .toList() ??
            [];
    if (listEquals(_newValue, creditCards)) return;
    creditCards = _newValue;
    notifyListeners();
  }
}

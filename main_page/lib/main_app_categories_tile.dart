import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/const.dart';
import 'package:li/components/atoms/update_blur.dart';

class MainAppCategoriesTile extends StatelessWidget
    with DiagnosticableTreeMixin {
  final String title, subtitle, svgPath;
  final double? height;
  final VoidCallback? onTap;

  final bool notReady;

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    properties.add(
      StringProperty('svgPath', svgPath),
    );
    super.debugFillProperties(properties);
  }

  const MainAppCategoriesTile({
    required this.title,
    required this.subtitle,
    required this.svgPath,
    this.notReady = false,
    this.onTap,
    this.height,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final child = SizeTapAnimation(
      child: GestureDetector(
        onTap: onTap,
        behavior: HitTestBehavior.opaque,
        child: DecoratedBox(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            color: Colors.white,
            boxShadow: bigBlur,
          ),
          child: SizedBox(
            width: double.infinity,
            child: Stack(
              children: [
                Positioned(
                  top: 82,
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(16),
                    child: SvgPicture.network(
                      svgPath,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 16,
                    horizontal: 10,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        title,
                        style: const TextStyle(
                          fontSize: 18,
                          color: Color(0xFF434343),
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Text(
                        subtitle,
                        style: const TextStyle(
                          fontSize: 14,
                          color: Color(0xFF979797),
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      const SizedBox(
                        height: 14,
                      ),
                      SizedBox(
                        height: height ?? 140,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );

    if (notReady) {
      return AbsorbPointer(
        child: Stack(
          children: [
            child,
            const UpdateBlur(),
          ],
        ),
      );
    }
    return child;
  }
}

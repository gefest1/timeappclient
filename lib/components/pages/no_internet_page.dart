import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/atoms/leading_icon.dart';
import 'package:li/utils/restart_widget.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import '../../../utils/color.dart';

class NoInternetPage extends StatefulWidget {
  NoInternetPage({Key? key}) : super(key: key);

  @override
  _NoInternetPageState createState() => _NoInternetPageState();
}

class _NoInternetPageState extends State<NoInternetPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        centerTitle: true,
        title: const Text(
          'Что-то пошло не так',
          style: TextStyle(
            fontSize: 18,
            height: 21 / 18,
            fontWeight: FontWeight.w400,
            color: ColorData.allMainBlack,
          ),
        ),
        leading: const LeadingIcon(),
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  const SizedBox(height: 64),
                  SvgPicture.asset('assets/svg/NoNetworkArt.svg'),
                  const SizedBox(height: 40),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Пожалуйста, проверьте соединение с интернетом',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w600,
                          height: 28 / 24,
                          color: ColorData.allMainBlack,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 10),
                      Text(
                        'Не можем открыть страницу без интернета. Пожалуйста, попробуйте еще раз — если не получится, обратитесь в нашу службу поддержки',
                        style: TextStyle(
                          fontSize: 18,
                          color: ColorData.allMainBlack,
                          height: 21 / 18,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          SliverLayoutBuilder(
            builder: (_, SliverConstraints constraints) {
              final _dd = constraints.viewportMainAxisExtent -
                  constraints.precedingScrollExtent -
                  76;
              return SliverToBoxAdapter(
                child: Padding(
                  padding: EdgeInsets.only(
                    top: _dd > 0 ? _dd : 0,
                    left: 20,
                    right: 20,
                  ),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      fixedSize:
                          MaterialStateProperty.all(Size(double.infinity, 60)),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(16),
                        ),
                      ),
                      backgroundColor: MaterialStateProperty.all(
                        ColorData.clientsButtonColorDefault,
                      ),
                    ),
                    onPressed: () {
                      RestartWidget.restartApp(context);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                      ),
                      width: double.infinity,
                      child: Text(
                        'Повторить попытку',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 18,
                          height: 21 / 18,
                          color: ColorData.grayScaleOffWhite,
                        ),
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ],
      ),
      // SliverLayoutBuilder(
      //   builder: (ctx, SliverConstraints constraints) {
      //     final _dd = constraints.viewportMainAxisExtent -
      //         constraints.precedingScrollExtent -
      //         140;
      //     return SliverToBoxAdapter(
      //       child: Padding(
      //         padding: const EdgeInsets.symmetric(horizontal: 20),
      //         child: ListView(
      //           children: [
      //             SizedBox(height: 110),
      //             Padding(
      //               padding: const EdgeInsets.symmetric(horizontal: 20.0),
      // ElevatedButton(
      //   style: ButtonStyle(
      //     fixedSize: MaterialStateProperty.all(
      //         Size(double.infinity, 60)),
      //     shape: MaterialStateProperty.all(
      //       RoundedRectangleBorder(
      //         borderRadius: BorderRadius.circular(16),
      //       ),
      //     ),
      //     backgroundColor: MaterialStateProperty.all(
      //       ColorData.clientsButtonColorDefault,
      //     ),
      //   ),
      //   onPressed: () {
      //     Navigator.pop(context);
      //   },
      //   child: Container(
      //     decoration: BoxDecoration(
      //       borderRadius: BorderRadius.circular(16),
      //     ),
      //     width: double.infinity,
      //     child: Text(
      //       'Повторить Попытку',
      //       textAlign: TextAlign.center,
      //       style: TextStyle(
      //         fontWeight: FontWeight.w500,
      //         fontSize: 18,
      //         height: 21 / 18,
      //         color: ColorData.grayScaleOffWhite,
      //       ),
      //     ),
      //   ),
      // ),
      //             ),
      //           ],
      //         ),
      //       ),
      //     );
      //   },
      // ),
    );
  }
}

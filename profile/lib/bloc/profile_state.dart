part of 'profile_bloc.dart';

abstract class ProfileState extends Equatable {
  const ProfileState();

  @override
  List<Object?> get props => [];
}

class NotRegistered extends ProfileState {}

class ProfileInitial extends ProfileState {}

class ProfileInformation extends ProfileState {
  final Client client;
  const ProfileInformation({required this.client});

  @override
  List<Object?> get props => [client];
}

class DeleteAccountState extends ProfileState {}

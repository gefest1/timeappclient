import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:li/data/models/review/comment_model.dart';
import 'package:li/data/models/meta_rating_model.dart';
import 'package:li/logic/blocs/comments_of_club/comment_api/get_comments_api.dart';

import 'package:li/network/graphql_provider.dart';

part 'commentsofclub_event.dart';
part 'commentsofclub_state.dart';
part './reactions/getCommentsReaction.dart';

class CommentsofclubBloc
    extends Bloc<CommentsofclubEvent, CommentsofclubState> {
  final GraphqlMinuteProvider graphqlMinuteProvider;
  CommentsofclubBloc(this.graphqlMinuteProvider)
      : super(CommentsofclubInitial()) {
    on<GetCommentsOfClubEvent>(getCommentsOfInstitution);
  }
}

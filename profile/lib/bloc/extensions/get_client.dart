part of '../profile_bloc.dart';

extension GetData on ProfileBloc {
  void getClient(
    ProfileEvent event,
    Emitter<ProfileState> emit,
  ) async {
    try {
      final _mutationResult = await graphqlProvider.client.query(
        QueryOptions(
          document: gql('''
              query{
                getClient{
                  debt
                  email,
                  city,
                  bonus,
                  creditCards{
                    cardHolderName,
                    cardType,
                    last4Digits,
                  }
                  _id,
                  dateOfBirth,
                  fullName,
                  phoneNumber,
                  photoURL{
                    M,
                    XL,
                    thumbnail
                  }
                  sex
                }
              }'''),
        ),
      );
      if (_mutationResult.hasException) throw _mutationResult.exception!;

      final Client _data = Client.fromMap(_mutationResult.data!['getClient']);
      emit(ProfileInformation(client: _data));
    } catch (e) {
      // log(e.toString());
      add(GetClient());
      rethrow;
    }
  }
}

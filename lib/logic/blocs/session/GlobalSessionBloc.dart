import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:li/data/models/allowed_session_types.dart';
import 'package:li/logic/blocs/qfit_bloc/session/session_minute_bloc.dart';
import 'package:li/logic/blocs/session/session_api/get_active_session.dart';
import 'package:li/logic/blocs/session/models/global_session_model.dart';
import 'package:li/network/graphql_provider.dart';

part 'GlobalSessionState.dart';
part 'GlobalSessionEvent.dart';

part 'session_react/get_session.dart';
part 'session_react/remove_minute_session.dart';

class GlobalSessionBloc extends Bloc<SessionEvent, SessionState> {
  final GraphqlProvider graphqlProvider;
  late final SessionMinuteBloc sessionMinuteBloc;

  GlobalSessionBloc(this.graphqlProvider) : super(LoadingSessionState()) {
    on<GetSessionEvent>(getSession);
    on<RemoveMinuteSessionEvent>(removeMinuteSession);
  }
}

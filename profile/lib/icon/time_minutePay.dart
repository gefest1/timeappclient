import 'package:flutter/material.dart';

//Copy this CustomPainter code to the Bottom of the File
class TimeAppMinutePayWide extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(117.68, 16.0898);
    path_0.lineTo(117.615, 22.3578);

    Paint paint_0_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_0_stroke.color = Colors.black.withOpacity(1.0);
    paint_0_stroke.strokeCap = StrokeCap.round;
    canvas.drawPath(path_0, paint_0_stroke);

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Color(0xff000000).withOpacity(1.0);
    canvas.drawPath(path_0, paint_0_fill);

    Path path_1 = Path();
    path_1.moveTo(122.695, 18.0781);
    path_1.lineTo(121.164, 22.3591);

    Paint paint_1_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_1_stroke.color = Colors.black.withOpacity(1.0);
    paint_1_stroke.strokeCap = StrokeCap.round;
    canvas.drawPath(path_1, paint_1_stroke);

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = Color(0xff000000).withOpacity(1.0);
    canvas.drawPath(path_1, paint_1_fill);

    Path path_2 = Path();
    path_2.moveTo(187.873, 101.161);
    path_2.cubicTo(189.691, 107.13, 188.677, 112.257, 185.542, 116.595);
    path_2.cubicTo(182.4, 120.941, 177.121, 124.508, 170.394, 127.319);
    path_2.cubicTo(156.939, 132.942, 137.756, 135.519, 118.54, 135.327);
    path_2.cubicTo(98.0325, 135.122, 79.6994, 132.652, 67.3513, 127.184);
    path_2.cubicTo(61.1783, 124.45, 56.517, 120.975, 53.8206, 116.674);
    path_2.cubicTo(51.128, 112.379, 50.3828, 107.243, 52.076, 101.157);
    path_2.cubicTo(52.519, 99.5647, 53.4686, 97.9881, 54.7242, 96.3207);
    path_2.cubicTo(55.6673, 95.0682, 56.7754, 93.7738, 57.9616, 92.3882);
    path_2.cubicTo(58.3553, 91.9284, 58.7575, 91.4586, 59.1652, 90.9769);
    path_2.cubicTo(62.4321, 87.1168, 66.0213, 82.5253, 68.1662, 76.3885);
    path_2.cubicTo(69.1484, 73.5781, 69.2909, 70.7158, 69.1084, 67.8488);
    path_2.cubicTo(68.9906, 65.9995, 68.7361, 64.1362, 68.483, 62.2831);
    path_2.cubicTo(68.3449, 61.2713, 68.2071, 60.2626, 68.0922, 59.2608);
    path_2.cubicTo(67.4398, 53.571, 67.5095, 48.0137, 72.272, 42.8366);
    path_2.cubicTo(82.9176, 31.2641, 100.666, 28.3936, 117.673, 28.3936);
    path_2.cubicTo(118.947, 28.3936, 120.244, 28.3863, 121.557, 28.379);
    path_2.cubicTo(128.348, 28.3413, 135.572, 28.3011, 142.283, 29.2432);
    path_2.cubicTo(150.29, 30.3674, 157.532, 32.8864, 162.413, 38.447);
    path_2.cubicTo(165.658, 42.1442, 167.36, 45.4447, 168.173, 48.5084);
    path_2.cubicTo(168.986, 51.5727, 168.913, 54.4123, 168.59, 57.1954);
    path_2.cubicTo(168.465, 58.2666, 168.303, 59.3311, 168.141, 60.3973);
    path_2.cubicTo(167.462, 64.858, 166.778, 69.3487, 168.837, 74.4819);
    path_2.cubicTo(171.224, 80.4324, 174.631, 83.5706, 178, 86.6739);
    path_2.cubicTo(178.226, 86.8813, 178.451, 87.0885, 178.675, 87.2965);
    path_2.cubicTo(182.254, 90.6101, 185.722, 94.1043, 187.873, 101.161);
    path_2.close();

    Paint paint_2_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_2_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_2, paint_2_stroke);

    Paint paint_2_fill = Paint()..style = PaintingStyle.fill;
    paint_2_fill.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_2, paint_2_fill);

    Path path_3 = Path();
    path_3.moveTo(92.4168, 40.7214);
    path_3.cubicTo(92.4168, 43.0611, 90.5201, 44.9578, 88.1804, 44.9578);
    path_3.cubicTo(85.8407, 44.9578, 83.944, 43.0611, 83.944, 40.7214);
    path_3.cubicTo(83.944, 38.3817, 85.8407, 36.485, 88.1804, 36.485);
    path_3.cubicTo(90.5201, 36.485, 92.4168, 38.3817, 92.4168, 40.7214);
    path_3.close();

    Paint paint_3_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_3_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_3, paint_3_stroke);

    Paint paint_3_fill = Paint()..style = PaintingStyle.fill;
    paint_3_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_3, paint_3_fill);

    Path path_4 = Path();
    path_4.moveTo(124.218, 40.7214);
    path_4.cubicTo(124.218, 43.0611, 122.321, 44.9578, 119.981, 44.9578);
    path_4.cubicTo(117.641, 44.9578, 115.745, 43.0611, 115.745, 40.7214);
    path_4.cubicTo(115.745, 38.3817, 117.641, 36.485, 119.981, 36.485);
    path_4.cubicTo(122.321, 36.485, 124.218, 38.3817, 124.218, 40.7214);
    path_4.close();

    Paint paint_4_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_4_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_4, paint_4_stroke);

    Paint paint_4_fill = Paint()..style = PaintingStyle.fill;
    paint_4_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_4, paint_4_fill);

    Path path_5 = Path();
    path_5.moveTo(153.462, 40.7214);
    path_5.cubicTo(153.462, 43.0611, 151.565, 44.9578, 149.225, 44.9578);
    path_5.cubicTo(146.886, 44.9578, 144.989, 43.0611, 144.989, 40.7214);
    path_5.cubicTo(144.989, 38.3817, 146.886, 36.485, 149.225, 36.485);
    path_5.cubicTo(151.565, 36.485, 153.462, 38.3817, 153.462, 40.7214);
    path_5.close();

    Paint paint_5_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_5_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_5, paint_5_stroke);

    Paint paint_5_fill = Paint()..style = PaintingStyle.fill;
    paint_5_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_5, paint_5_fill);

    Path path_6 = Path();
    path_6.moveTo(104.511, 56.073);
    path_6.cubicTo(104.511, 58.4127, 102.614, 60.3094, 100.274, 60.3094);
    path_6.cubicTo(97.9344, 60.3094, 96.0377, 58.4127, 96.0377, 56.073);
    path_6.cubicTo(96.0377, 53.7333, 97.9344, 51.8366, 100.274, 51.8366);
    path_6.cubicTo(102.614, 51.8366, 104.511, 53.7333, 104.511, 56.073);
    path_6.close();

    Paint paint_6_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_6_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_6, paint_6_stroke);

    Paint paint_6_fill = Paint()..style = PaintingStyle.fill;
    paint_6_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_6, paint_6_fill);

    Path path_7 = Path();
    path_7.moveTo(139.512, 54.8034);
    path_7.cubicTo(139.512, 57.1431, 137.616, 59.0398, 135.276, 59.0398);
    path_7.cubicTo(132.936, 59.0398, 131.04, 57.1431, 131.04, 54.8034);
    path_7.cubicTo(131.04, 52.4637, 132.936, 50.567, 135.276, 50.567);
    path_7.cubicTo(137.616, 50.567, 139.512, 52.4637, 139.512, 54.8034);
    path_7.close();

    Paint paint_7_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_7_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_7, paint_7_stroke);

    Paint paint_7_fill = Paint()..style = PaintingStyle.fill;
    paint_7_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_7, paint_7_fill);

    Path path_8 = Path();
    path_8.moveTo(68.6488, 50.567);
    path_8.cubicTo(70.9885, 50.567, 72.8852, 52.4637, 72.8852, 54.8034);
    path_8.cubicTo(72.8852, 57.1432, 70.9885, 59.0398, 68.6488, 59.0398);
    path_8.cubicTo(68.4525, 59.0398, 68.2593, 59.0258, 68.0696, 58.9996);
    path_8.cubicTo(67.7543, 56.1497, 67.6374, 53.3373, 68.2142, 50.5903);
    path_8.cubicTo(68.3576, 50.5753, 68.5024, 50.567, 68.6488, 50.567);
    path_8.close();

    Paint paint_8_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_8_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_8, paint_8_stroke);

    Paint paint_8_fill = Paint()..style = PaintingStyle.fill;
    paint_8_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_8, paint_8_fill);

    Path path_9 = Path();
    path_9.moveTo(82.8883, 38.7596);
    path_9.lineTo(89.009, 56.5263);
    path_9.lineTo(81.9203, 70.8154);
    path_9.lineTo(69.3833, 84.3923);
    path_9.cubicTo(67.1392, 82.5359, 66.2351, 79.879, 66.212, 76.8746);
    path_9.cubicTo(66.1886, 73.8118, 67.082, 70.4032, 68.3918, 67.1592);
    path_9.cubicTo(69.7021, 63.9142, 73.3702, 56.7322, 76.7161, 50.3523);
    path_9.cubicTo(78.388, 47.1644, 79.9778, 44.1799, 81.1496, 41.9922);
    path_9.cubicTo(81.7355, 40.8984, 82.2169, 40.0039, 82.5518, 39.3828);
    path_9.cubicTo(82.6898, 39.1269, 82.803, 38.9174, 82.8883, 38.7596);
    path_9.close();

    Paint paint_9_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_9_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_9, paint_9_stroke);

    Paint paint_9_fill = Paint()..style = PaintingStyle.fill;
    paint_9_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_9, paint_9_fill);

    Path path_10 = Path();
    path_10.moveTo(82.7737, 38.9651);
    path_10.lineTo(82.8847, 38.7596);
    path_10.lineTo(89.0052, 56.5265);
    path_10.lineTo(81.9006, 70.9124);
    path_10.cubicTo(81.3905, 70.2233, 80.7441, 69.4204, 79.5928, 68.3379);
    path_10.cubicTo(79.3178, 68.0793, 79.0421, 67.8218, 78.7666, 67.5646);
    path_10.cubicTo(77.1567, 66.0615, 75.554, 64.5652, 74.1497, 62.883);
    path_10.cubicTo(73.3414, 61.9149, 72.6007, 60.8945, 71.9351, 59.8236);
    path_10.cubicTo(71.9171, 59.7946, 71.9007, 59.7668, 71.8838, 59.738);
    path_10.cubicTo(74.1266, 55.2591, 76.913, 49.9475, 79.1442, 45.7456);
    path_10.cubicTo(80.2667, 43.6316, 81.2486, 41.7987, 81.9498, 40.4943);
    path_10.cubicTo(82.3004, 39.8421, 82.5809, 39.3221, 82.7737, 38.9651);
    path_10.close();

    Paint paint_10_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_10_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_10, paint_10_stroke);

    Paint paint_10_fill = Paint()..style = PaintingStyle.fill;
    paint_10_fill.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_10, paint_10_fill);

    Path path_11 = Path();
    path_11.moveTo(160.866, 47.521);
    path_11.lineTo(161.02, 47.4428);
    path_11.lineTo(160.866, 47.521);
    path_11.cubicTo(162.093, 49.9343, 162.584, 52.1023, 162.278, 54.0019);
    path_11.cubicTo(161.976, 55.8691, 160.899, 57.503, 158.934, 58.8724);
    path_11.lineTo(129.186, 44.1539);
    path_11.lineTo(125.967, 26.6569);
    path_11.cubicTo(125.985, 26.665, 126.004, 26.6735, 126.024, 26.6823);
    path_11.cubicTo(126.256, 26.7855, 126.597, 26.9378, 127.033, 27.1342);
    path_11.cubicTo(127.904, 27.5271, 129.152, 28.0964, 130.662, 28.8022);
    path_11.cubicTo(133.682, 30.2139, 137.746, 32.171, 141.924, 34.3531);
    path_11.cubicTo(146.102, 36.5356, 150.392, 38.9414, 153.864, 41.2505);
    path_11.cubicTo(155.6, 42.4051, 157.129, 43.5335, 158.337, 44.5959);
    path_11.cubicTo(159.547, 45.6608, 160.422, 46.6489, 160.866, 47.521);
    path_11.close();

    Paint paint_11_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_11_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_11, paint_11_stroke);

    Paint paint_11_fill = Paint()..style = PaintingStyle.fill;
    paint_11_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_11, paint_11_fill);

    Path path_12 = Path();
    path_12.moveTo(132.962, 29.8883);
    path_12.cubicTo(137.223, 31.9216, 142.734, 34.6667, 147.758, 37.5256);
    path_12.cubicTo(148.5, 39.7746, 148.39, 42.2741, 147.874, 44.6053);
    path_12.cubicTo(147.301, 47.1917, 146.278, 49.6721, 144.87, 51.9137);
    path_12.lineTo(129.186, 44.1539);
    path_12.lineTo(125.967, 26.6569);
    path_12.cubicTo(126.036, 26.6873, 126.116, 26.7229, 126.206, 26.7635);
    path_12.cubicTo(126.556, 26.9193, 127.067, 27.1484, 127.711, 27.4412);
    path_12.cubicTo(129, 28.027, 130.823, 28.8679, 132.962, 29.8883);
    path_12.close();

    Paint paint_12_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_12_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_12, paint_12_stroke);

    Paint paint_12_fill = Paint()..style = PaintingStyle.fill;
    paint_12_fill.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_12, paint_12_fill);

    Path path_13 = Path();
    path_13.moveTo(70.5582, 56.4465);
    path_13.lineTo(70.5915, 56.3866);
    path_13.cubicTo(70.6501, 56.2817, 70.7366, 56.1272, 70.8484, 55.9292);
    path_13.cubicTo(71.0719, 55.5331, 71.3967, 54.9628, 71.8011, 54.2661);
    path_13.cubicTo(72.6101, 52.8727, 73.7376, 50.9744, 75.0121, 48.9542);
    path_13.cubicTo(76.2868, 46.9338, 77.7074, 44.7933, 79.1025, 42.9149);
    path_13.cubicTo(80.4997, 41.0338, 81.8639, 39.4259, 83.0253, 38.4626);
    path_13.cubicTo(85.3652, 36.522, 89.9395, 33.7779, 95.1707, 31.3576);
    path_13.cubicTo(100.401, 28.9376, 106.269, 26.8506, 111.193, 26.2122);
    path_13.cubicTo(116.018, 25.5869, 121.653, 25.4207, 125.701, 26.5304);
    path_13.cubicTo(127.733, 27.0875, 132.744, 28.8645, 137.256, 30.5056);
    path_13.cubicTo(139.509, 31.3255, 141.636, 32.1104, 143.199, 32.6904);
    path_13.cubicTo(143.98, 32.9804, 144.621, 33.2191, 145.066, 33.3854);
    path_13.lineTo(145.58, 33.5777);
    path_13.lineTo(145.642, 33.601);
    path_13.cubicTo(145.279, 40.2548, 143.206, 46.0379, 138.935, 50.7145);
    path_13.lineTo(129.439, 45.9684);
    path_13.lineTo(129.133, 45.8158);
    path_13.lineTo(129.192, 46.1522);
    path_13.lineTo(133.477, 70.8886);
    path_13.cubicTo(122.933, 81.3334, 110.531, 82.3547, 97.1919, 78.7978);
    path_13.lineTo(89.9888, 58.2974);
    path_13.lineTo(89.8569, 57.922);
    path_13.lineTo(89.6731, 58.2749);
    path_13.lineTo(85.6662, 65.9666);
    path_13.cubicTo(78.7941, 63.8856, 73.919, 60.6778, 70.5582, 56.4465);
    path_13.close();

    Paint paint_13_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_13_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_13, paint_13_stroke);

    Paint paint_13_fill = Paint()..style = PaintingStyle.fill;
    paint_13_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_13, paint_13_fill);

    Path path_14 = Path();
    path_14.moveTo(125.652, 26.5535);
    path_14.cubicTo(125.767, 26.585, 125.892, 26.6206, 126.026, 26.6598);
    path_14.lineTo(125.793, 26.9068);
    path_14.lineTo(125.747, 26.9201);
    path_14.lineTo(125.746, 26.9201);
    path_14.lineTo(83.0787, 39.2423);
    path_14.lineTo(82.5216, 38.8864);
    path_14.cubicTo(82.6135, 38.8012, 82.7044, 38.7196, 82.7941, 38.6416);
    path_14.lineTo(83.0685, 38.8168);
    path_14.lineTo(83.1342, 38.8589);
    path_14.lineTo(83.2092, 38.8372);
    path_14.lineTo(125.557, 26.6073);
    path_14.lineTo(125.602, 26.5942);
    path_14.lineTo(125.635, 26.5599);
    path_14.lineTo(125.643, 26.5511);
    path_14.cubicTo(125.645, 26.5518, 125.648, 26.5524, 125.65, 26.5531);
    path_14.lineTo(125.65, 26.5531);
    path_14.lineTo(125.652, 26.5535);
    path_14.close();

    Paint paint_14_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_14_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_14, paint_14_stroke);

    Paint paint_14_fill = Paint()..style = PaintingStyle.fill;
    paint_14_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_14, paint_14_fill);

    Path path_15 = Path();
    path_15.moveTo(130.362, 28.0822);
    path_15.lineTo(126.693, 31.3213);
    path_15.lineTo(126.656, 31.3325);
    path_15.lineTo(126.656, 31.3325);
    path_15.lineTo(84.7145, 43.9115);
    path_15.lineTo(79.8645, 41.8803);
    path_15.cubicTo(79.9404, 41.7834, 80.016, 41.6874, 80.0914, 41.5925);
    path_15.lineTo(84.6773, 43.5131);
    path_15.lineTo(84.7343, 43.537);
    path_15.lineTo(84.7935, 43.5193);
    path_15.lineTo(126.481, 31.0164);
    path_15.lineTo(126.517, 31.0055);
    path_15.lineTo(126.546, 30.9805);
    path_15.lineTo(129.979, 27.9498);
    path_15.cubicTo(130.106, 27.9934, 130.233, 28.0376, 130.362, 28.0822);
    path_15.close();

    Paint paint_15_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_15_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_15, paint_15_stroke);

    Paint paint_15_fill = Paint()..style = PaintingStyle.fill;
    paint_15_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_15, paint_15_fill);

    Path path_16 = Path();
    path_16.moveTo(127.239, 35.3989);
    path_16.lineTo(127.271, 35.3891);
    path_16.lineTo(127.298, 35.3677);
    path_16.lineTo(134.487, 29.5428);
    path_16.cubicTo(134.616, 29.5892, 134.746, 29.6357, 134.875, 29.6824);
    path_16.lineTo(127.439, 35.7071);
    path_16.lineTo(86.1571, 48.0885);
    path_16.lineTo(77.6755, 44.8738);
    path_16.cubicTo(77.7448, 44.7739, 77.8141, 44.6743, 77.8835, 44.5752);
    path_16.lineTo(86.1157, 47.6952);
    path_16.lineTo(86.1704, 47.7159);
    path_16.lineTo(86.2264, 47.6991);
    path_16.lineTo(127.239, 35.3989);
    path_16.close();

    Paint paint_16_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_16_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_16, paint_16_stroke);

    Paint paint_16_fill = Paint()..style = PaintingStyle.fill;
    paint_16_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_16, paint_16_fill);

    Path path_17 = Path();
    path_17.moveTo(128.086, 39.7203);
    path_17.lineTo(128.114, 39.7119);
    path_17.lineTo(128.137, 39.6948);
    path_17.lineTo(139.585, 31.3928);
    path_17.cubicTo(139.72, 31.4421, 139.853, 31.491, 139.985, 31.5395);
    path_17.lineTo(128.272, 40.0331);
    path_17.lineTo(128.244, 40.0416);
    path_17.lineTo(128.243, 40.0417);
    path_17.lineTo(87.598, 52.2319);
    path_17.lineTo(75.2735, 48.5109);
    path_17.cubicTo(75.339, 48.408, 75.4047, 48.305, 75.4708, 48.2018);
    path_17.lineTo(87.5487, 51.8482);
    path_17.lineTo(87.5984, 51.8632);
    path_17.lineTo(87.6481, 51.8483);
    path_17.lineTo(128.086, 39.7203);
    path_17.close();

    Paint paint_17_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_17_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_17, paint_17_stroke);

    Paint paint_17_fill = Paint()..style = PaintingStyle.fill;
    paint_17_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_17, paint_17_fill);

    Path path_18 = Path();
    path_18.moveTo(128.88, 44.1222);
    path_18.lineTo(128.908, 44.1139);
    path_18.lineTo(128.931, 44.0969);
    path_18.lineTo(144.169, 33.0923);
    path_18.cubicTo(144.31, 33.1447, 144.443, 33.1941, 144.566, 33.2403);
    path_18.lineTo(129.066, 44.4347);
    path_18.lineTo(129.037, 44.4432);
    path_18.lineTo(129.037, 44.4432);
    path_18.lineTo(89.0484, 56.4367);
    path_18.lineTo(88.9992, 56.4231);
    path_18.lineTo(88.9991, 56.4231);
    path_18.lineTo(73.0985, 52.0096);
    path_18.cubicTo(73.1607, 51.906, 73.2237, 51.8013, 73.2875, 51.6957);
    path_18.lineTo(88.9967, 56.0561);
    path_18.lineTo(89.0447, 56.0694);
    path_18.lineTo(89.0924, 56.0551);
    path_18.lineTo(128.88, 44.1222);
    path_18.close();

    Paint paint_18_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_18_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_18, paint_18_stroke);

    Paint paint_18_fill = Paint()..style = PaintingStyle.fill;
    paint_18_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_18, paint_18_fill);

    Path path_19 = Path();
    path_19.moveTo(88.7281, 60.0263);
    path_19.lineTo(88.5602, 60.3486);
    path_19.lineTo(70.791, 55.9747);
    path_19.cubicTo(70.8451, 55.8788, 70.9054, 55.7723, 70.9716, 55.6557);
    path_19.lineTo(88.7281, 60.0263);
    path_19.close();

    Paint paint_19_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_19_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_19, paint_19_stroke);

    Paint paint_19_fill = Paint()..style = PaintingStyle.fill;
    paint_19_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_19, paint_19_fill);

    Path path_20 = Path();
    path_20.moveTo(90.8076, 60.7864);
    path_20.lineTo(90.6904, 60.4529);
    path_20.lineTo(129.596, 48.7842);
    path_20.lineTo(129.657, 49.1345);
    path_20.lineTo(90.8076, 60.7864);
    path_20.close();

    Paint paint_20_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_20_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_20, paint_20_stroke);

    Paint paint_20_fill = Paint()..style = PaintingStyle.fill;
    paint_20_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_20, paint_20_fill);

    Path path_21 = Path();
    path_21.moveTo(132.211, 47.3917);
    path_21.lineTo(131.854, 47.2133);
    path_21.lineTo(145.148, 37.6735);
    path_21.cubicTo(145.119, 37.841, 145.088, 38.0078, 145.056, 38.1742);
    path_21.lineTo(132.211, 47.3917);
    path_21.close();

    Paint paint_21_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_21_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_21, paint_21_stroke);

    Paint paint_21_fill = Paint()..style = PaintingStyle.fill;
    paint_21_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_21, paint_21_fill);

    Path path_22 = Path();
    path_22.moveTo(92.3388, 65.1441);
    path_22.lineTo(92.2216, 64.8106);
    path_22.lineTo(130.381, 53.317);
    path_22.lineTo(130.442, 53.6674);
    path_22.lineTo(92.3388, 65.1441);
    path_22.close();

    Paint paint_22_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_22_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_22, paint_22_stroke);

    Paint paint_22_fill = Paint()..style = PaintingStyle.fill;
    paint_22_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_22, paint_22_fill);

    Path path_23 = Path();
    path_23.moveTo(136.5, 49.5412);
    path_23.lineTo(136.14, 49.3614);
    path_23.lineTo(143.011, 44.5779);
    path_23.cubicTo(142.909, 44.7962, 142.803, 45.0131, 142.694, 45.2286);
    path_23.lineTo(136.5, 49.5412);
    path_23.close();

    Paint paint_23_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_23_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_23, paint_23_stroke);

    Paint paint_23_fill = Paint()..style = PaintingStyle.fill;
    paint_23_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_23, paint_23_fill);

    Path path_24 = Path();
    path_24.moveTo(86.891, 63.5582);
    path_24.lineTo(86.7231, 63.8805);
    path_24.lineTo(75.3226, 61.0655);
    path_24.cubicTo(75.0798, 60.8884, 74.8418, 60.7083, 74.6082, 60.5253);
    path_24.lineTo(86.891, 63.5582);
    path_24.close();

    Paint paint_24_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_24_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_24, paint_24_stroke);

    Paint paint_24_fill = Paint()..style = PaintingStyle.fill;
    paint_24_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_24, paint_24_fill);

    Path path_25 = Path();
    path_25.moveTo(131.198, 58.0481);
    path_25.lineTo(93.7852, 69.2688);
    path_25.lineTo(93.668, 68.9353);
    path_25.lineTo(131.137, 57.6977);
    path_25.lineTo(131.198, 58.0481);
    path_25.close();

    Paint paint_25_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_25_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_25, paint_25_stroke);

    Paint paint_25_fill = Paint()..style = PaintingStyle.fill;
    paint_25_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_25, paint_25_fill);

    Path path_26 = Path();
    path_26.moveTo(125.377, 62.0248);
    path_26.lineTo(125.377, 62.0248);
    path_26.cubicTo(125.372, 62.0848, 125.342, 62.181, 125.276, 62.3124);
    path_26.cubicTo(125.212, 62.4402, 125.12, 62.5883, 125.007, 62.7485);
    path_26.cubicTo(124.78, 63.0687, 124.473, 63.4266, 124.143, 63.7513);
    path_26.cubicTo(123.811, 64.0769, 123.463, 64.3627, 123.154, 64.5437);
    path_26.cubicTo(123, 64.6343, 122.862, 64.6948, 122.746, 64.7233);
    path_26.cubicTo(122.629, 64.752, 122.55, 64.7444, 122.5, 64.7212);
    path_26.cubicTo(122.116, 64.5456, 121.907, 64.3896, 121.796, 64.285);
    path_26.cubicTo(121.741, 64.2327, 121.71, 64.1933, 121.695, 64.1705);
    path_26.cubicTo(121.687, 64.1591, 121.683, 64.1517, 121.681, 64.1488);
    path_26.cubicTo(121.681, 64.1483, 121.681, 64.1479, 121.681, 64.1477);
    path_26.cubicTo(121.681, 64.1472, 121.681, 64.1472, 121.681, 64.1476);
    path_26.lineTo(121.679, 64.1437);
    path_26.lineTo(121.677, 64.1401);
    path_26.lineTo(121.677, 64.1401);
    path_26.cubicTo(121.658, 64.0997, 121.651, 64.0149, 121.703, 63.8637);
    path_26.cubicTo(121.753, 63.7184, 121.848, 63.5417, 121.982, 63.3432);
    path_26.cubicTo(122.25, 62.9474, 122.655, 62.4917, 123.101, 62.0687);
    path_26.cubicTo(123.547, 61.6459, 124.026, 61.2629, 124.439, 61.0094);
    path_26.cubicTo(124.645, 60.8823, 124.829, 60.7916, 124.979, 60.7432);
    path_26.cubicTo(125.088, 60.7082, 125.163, 60.7005, 125.212, 60.7049);
    path_26.cubicTo(125.214, 60.71, 125.217, 60.7155, 125.219, 60.7213);
    path_26.cubicTo(125.242, 60.7777, 125.274, 60.8646, 125.303, 60.9809);
    path_26.cubicTo(125.362, 61.2132, 125.414, 61.5639, 125.377, 62.0248);
    path_26.close();
    path_26.moveTo(125.183, 60.6439);
    path_26.lineTo(125.184, 60.6445);
    path_26.lineTo(125.184, 60.6444);
    path_26.lineTo(125.184, 60.6442);
    path_26.lineTo(125.183, 60.6439);
    path_26.close();

    Paint paint_26_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_26_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_26, paint_26_stroke);

    Paint paint_26_fill = Paint()..style = PaintingStyle.fill;
    paint_26_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_26, paint_26_fill);

    Path path_27 = Path();
    path_27.moveTo(123.414, 52.7235);
    path_27.cubicTo(123.535, 52.9557, 123.717, 53.2485, 123.923, 53.5822);
    path_27.cubicTo(124.014, 53.7289, 124.11, 53.8835, 124.208, 54.0444);
    path_27.cubicTo(124.532, 54.5771, 124.885, 55.1915, 125.182, 55.8556);
    path_27.cubicTo(125.777, 57.188, 126.133, 58.6928, 125.594, 60.1227);
    path_27.cubicTo(125.475, 60.4394, 125.158, 60.8641, 124.664, 61.3717);
    path_27.cubicTo(124.175, 61.8746, 123.527, 62.4432, 122.768, 63.0478);
    path_27.cubicTo(121.251, 64.2565, 119.301, 65.6003, 117.309, 66.8369);
    path_27.cubicTo(115.317, 68.0735, 113.288, 69.2002, 111.614, 69.9759);
    path_27.cubicTo(110.777, 70.3639, 110.033, 70.6623, 109.429, 70.843);
    path_27.cubicTo(109.127, 70.9334, 108.864, 70.9932, 108.644, 71.0209);
    path_27.cubicTo(108.422, 71.0488, 108.256, 71.0422, 108.143, 71.0101);
    path_27.cubicTo(105.633, 70.2997, 102.714, 68.4272, 101.392, 64.8451);
    path_27.cubicTo(101.293, 64.5789, 100.986, 63.7309, 100.976, 62.7917);
    path_27.cubicTo(100.965, 61.8572, 101.248, 60.8589, 102.304, 60.2361);
    path_27.cubicTo(102.52, 60.1084, 102.794, 60.0666, 103.127, 60.0766);
    path_27.cubicTo(103.419, 60.0853, 103.737, 60.1325, 104.081, 60.1837);
    path_27.cubicTo(104.132, 60.1912, 104.183, 60.1988, 104.235, 60.2064);
    path_27.cubicTo(105.027, 60.3223, 105.939, 60.4318, 106.762, 60.0281);
    path_27.cubicTo(109.225, 58.8198, 110.807, 57.9786, 112.433, 57.0076);
    path_27.cubicTo(112.986, 56.6769, 113.622, 56.3461, 114.294, 55.9965);
    path_27.cubicTo(114.383, 55.95, 114.473, 55.9033, 114.563, 55.8561);
    path_27.cubicTo(115.332, 55.4552, 116.136, 55.0283, 116.898, 54.5533);
    path_27.lineTo(116.82, 54.4272);
    path_27.lineTo(116.898, 54.5533);
    path_27.cubicTo(117.255, 54.331, 117.457, 54.0122, 117.604, 53.6646);
    path_27.cubicTo(117.676, 53.4918, 117.737, 53.3086, 117.795, 53.1251);
    path_27.cubicTo(117.806, 53.089, 117.818, 53.053, 117.829, 53.0169);
    path_27.cubicTo(117.876, 52.8674, 117.923, 52.7178, 117.977, 52.5666);
    path_27.cubicTo(118.11, 52.1939, 118.28, 51.8272, 118.567, 51.4951);
    path_27.cubicTo(118.854, 51.164, 119.265, 50.8586, 119.893, 50.6209);
    path_27.lineTo(119.893, 50.6209);
    path_27.cubicTo(120.098, 50.5435, 120.387, 50.5641, 120.738, 50.6798);
    path_27.cubicTo(121.084, 50.7938, 121.463, 50.9916, 121.829, 51.2311);
    path_27.cubicTo(122.195, 51.4702, 122.543, 51.7472, 122.825, 52.0151);
    path_27.cubicTo(123.11, 52.2858, 123.317, 52.5371, 123.414, 52.7235);
    path_27.close();
    path_27.moveTo(123.414, 52.7235);
    path_27.lineTo(123.567, 52.6438);
    path_27.moveTo(123.414, 52.7235);
    path_27.cubicTo(123.414, 52.7235, 123.414, 52.7235, 123.414, 52.7235);
    path_27.lineTo(123.567, 52.6438);
    path_27.moveTo(123.567, 52.6438);
    path_27.cubicTo(123.685, 52.87, 123.861, 53.1538, 124.066, 53.484);
    path_27.cubicTo(125.046, 55.0663, 126.686, 57.7126, 125.756, 60.1835);
    path_27.cubicTo(124.695, 63.0021, 110.421, 71.834, 108.096, 71.1761);
    path_27.cubicTo(105.546, 70.4547, 102.575, 68.5509, 101.23, 64.9048);
    path_27.cubicTo(101.031, 64.3674, 99.9613, 61.4178, 102.216, 60.0875);
    path_27.lineTo(119.832, 50.4596);
    path_27.cubicTo(120.871, 50.0664, 123.126, 51.7971, 123.567, 52.6438);
    path_27.close();

    Paint paint_27_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_27_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_27, paint_27_stroke);

    Paint paint_27_fill = Paint()..style = PaintingStyle.fill;
    paint_27_fill.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_27, paint_27_fill);

    Path path_28 = Path();
    path_28.moveTo(119.888, 56.2353);
    path_28.cubicTo(119.888, 56.9711, 119.291, 57.5676, 118.556, 57.5676);
    path_28.cubicTo(117.82, 57.5676, 117.223, 56.9711, 117.223, 56.2353);
    path_28.cubicTo(117.223, 55.4995, 117.82, 54.903, 118.556, 54.903);
    path_28.cubicTo(119.291, 54.903, 119.888, 55.4995, 119.888, 56.2353);
    path_28.close();

    Paint paint_28_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_28_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_28, paint_28_stroke);

    Paint paint_28_fill = Paint()..style = PaintingStyle.fill;
    paint_28_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_28, paint_28_fill);

    Path path_29 = Path();
    path_29.moveTo(108.757, 62.2392);
    path_29.cubicTo(108.757, 62.975, 108.161, 63.5715, 107.425, 63.5715);
    path_29.cubicTo(106.689, 63.5715, 106.092, 62.975, 106.092, 62.2392);
    path_29.cubicTo(106.092, 61.5034, 106.689, 60.9069, 107.425, 60.9069);
    path_29.cubicTo(108.161, 60.9069, 108.757, 61.5034, 108.757, 62.2392);
    path_29.close();

    Paint paint_29_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_29_stroke.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_29, paint_29_stroke);

    Paint paint_29_fill = Paint()..style = PaintingStyle.fill;
    paint_29_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_29, paint_29_fill);

    Path path_30 = Path();
    path_30.moveTo(119.494, 59.4115);
    path_30.cubicTo(119.494, 59.6773, 119.279, 59.8928, 119.013, 59.8928);
    path_30.cubicTo(118.747, 59.8928, 118.532, 59.6773, 118.532, 59.4115);
    path_30.cubicTo(118.532, 59.1458, 118.747, 58.9303, 119.013, 58.9303);
    path_30.cubicTo(119.279, 58.9303, 119.494, 59.1458, 119.494, 59.4115);
    path_30.close();

    Paint paint_30_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_30_stroke.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_30, paint_30_stroke);

    Paint paint_30_fill = Paint()..style = PaintingStyle.fill;
    paint_30_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_30, paint_30_fill);

    Path path_31 = Path();
    path_31.moveTo(122.317, 60.0639);
    path_31.cubicTo(122.317, 60.3297, 122.101, 60.5451, 121.835, 60.5451);
    path_31.cubicTo(121.57, 60.5451, 121.354, 60.3296, 121.354, 60.0639);
    path_31.cubicTo(121.354, 59.7981, 121.57, 59.5827, 121.835, 59.5827);
    path_31.cubicTo(122.101, 59.5827, 122.317, 59.7981, 122.317, 60.0639);
    path_31.close();

    Paint paint_31_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_31_stroke.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_31, paint_31_stroke);

    Paint paint_31_fill = Paint()..style = PaintingStyle.fill;
    paint_31_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_31, paint_31_fill);

    Path path_32 = Path();
    path_32.moveTo(125.724, 57.7933);
    path_32.cubicTo(125.229, 59.3318, 124.439, 59.9633, 123.667, 60.2788);
    path_32.cubicTo(123.346, 60.4098, 123.031, 60.4854, 122.759, 60.5505);
    path_32.cubicTo(122.711, 60.5619, 122.665, 60.573, 122.62, 60.584);
    path_32.cubicTo(122.307, 60.661, 122.088, 60.7309, 121.949, 60.8532);
    path_32.cubicTo(121.133, 61.571, 121.095, 62.3515, 121.424, 62.8479);
    path_32.cubicTo(121.758, 63.3516, 122.518, 63.6412, 123.461, 63.2698);
    path_32.cubicTo(127.018, 61.8685, 128.525, 60.6429, 129.222, 59.592);
    path_32.cubicTo(129.57, 59.0682, 129.723, 58.5796, 129.824, 58.1155);
    path_32.cubicTo(129.857, 57.9639, 129.884, 57.8189, 129.911, 57.6742);
    path_32.cubicTo(129.926, 57.5931, 129.941, 57.512, 129.957, 57.43);
    path_32.cubicTo(130.001, 57.2062, 130.051, 56.9836, 130.128, 56.7669);
    path_32.lineTo(130.187, 56.5998);
    path_32.lineTo(130.352, 56.6632);
    path_32.cubicTo(141.149, 60.7993, 150.763, 62.5795, 158.701, 58.9989);
    path_32.lineTo(151.649, 49.2639);
    path_32.lineTo(129.706, 48.5381);
    path_32.lineTo(129.697, 48.5378);
    path_32.lineTo(129.689, 48.5366);
    path_32.cubicTo(129.191, 48.47, 128.676, 48.3809, 128.155, 48.2907);
    path_32.cubicTo(127.929, 48.2514, 127.701, 48.212, 127.473, 48.1742);
    path_32.cubicTo(126.718, 48.0488, 125.955, 47.9398, 125.198, 47.906);
    path_32.cubicTo(123.684, 47.8384, 122.202, 48.0715, 120.864, 49.0675);
    path_32.cubicTo(120.796, 49.1179, 120.702, 49.2308, 120.585, 49.416);
    path_32.cubicTo(120.47, 49.5963, 120.344, 49.8292, 120.209, 50.1024);
    path_32.cubicTo(119.94, 50.6484, 119.642, 51.3429, 119.344, 52.0729);
    path_32.cubicTo(119.127, 52.6012, 118.91, 53.1494, 118.704, 53.6712);
    path_32.cubicTo(118.343, 54.5831, 118.013, 55.4146, 117.776, 55.9194);
    path_32.cubicTo(117.754, 55.9669, 117.737, 56.0535, 117.742, 56.1762);
    path_32.cubicTo(117.746, 56.2954, 117.769, 56.4353, 117.815, 56.58);
    path_32.cubicTo(117.906, 56.8719, 118.079, 57.1616, 118.328, 57.34);
    path_32.cubicTo(118.569, 57.5125, 118.897, 57.5929, 119.337, 57.4456);
    path_32.cubicTo(119.787, 57.2952, 120.357, 56.9056, 121.053, 56.1214);
    path_32.cubicTo(121.319, 55.8217, 121.573, 55.5297, 121.815, 55.2513);
    path_32.cubicTo(122.446, 54.5273, 122.996, 53.8955, 123.459, 53.4618);
    path_32.cubicTo(123.778, 53.1632, 124.078, 52.9369, 124.353, 52.8464);
    path_32.cubicTo(124.495, 52.7998, 124.639, 52.7859, 124.779, 52.8251);
    path_32.cubicTo(124.92, 52.8645, 125.038, 52.9529, 125.135, 53.078);
    path_32.lineTo(125.135, 53.0781);
    path_32.cubicTo(125.161, 53.1111, 125.179, 53.1561, 125.192, 53.1874);
    path_32.cubicTo(125.207, 53.2258, 125.224, 53.2739, 125.241, 53.3292);
    path_32.cubicTo(125.276, 53.4403, 125.318, 53.5883, 125.363, 53.7625);
    path_32.cubicTo(125.452, 54.1111, 125.554, 54.5705, 125.64, 55.061);
    path_32.cubicTo(125.726, 55.551, 125.796, 56.0754, 125.821, 56.5529);
    path_32.cubicTo(125.846, 57.0254, 125.828, 57.4711, 125.724, 57.7933);
    path_32.close();
    path_32.moveTo(125.724, 57.7933);
    path_32.lineTo(125.56, 57.7405);
    path_32.lineTo(125.724, 57.7933);
    path_32.close();

    Paint paint_32_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_32_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_32, paint_32_stroke);

    Paint paint_32_fill = Paint()..style = PaintingStyle.fill;
    paint_32_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_32, paint_32_fill);

    Path path_33 = Path();
    path_33.moveTo(142.852, 48.2734);
    path_33.lineTo(152.733, 48.9205);

    Paint paint_33_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_33_stroke.color = Colors.black.withOpacity(1.0);
    paint_33_stroke.strokeCap = StrokeCap.round;
    canvas.drawPath(path_33, paint_33_stroke);

    Paint paint_33_fill = Paint()..style = PaintingStyle.fill;
    paint_33_fill.color = Color(0xff000000).withOpacity(1.0);
    canvas.drawPath(path_33, paint_33_fill);

    Path path_34 = Path();
    path_34.moveTo(100.452, 34.3161);
    path_34.cubicTo(100.442, 34.3069, 100.433, 34.2982, 100.424, 34.29);
    path_34.lineTo(107.475, 20.7424);
    path_34.lineTo(111.004, 26.0971);
    path_34.cubicTo(111.004, 26.1059, 111.004, 26.1159, 111.004, 26.1271);
    path_34.cubicTo(111.006, 26.178, 111.007, 26.2536, 111.007, 26.351);
    path_34.cubicTo(111.008, 26.5457, 111.002, 26.8276, 110.98, 27.1737);
    path_34.cubicTo(110.936, 27.8664, 110.824, 28.814, 110.556, 29.8349);
    path_34.cubicTo(110.018, 31.8803, 108.859, 34.1928, 106.38, 35.3666);
    path_34.lineTo(106.454, 35.5225);
    path_34.lineTo(106.38, 35.3666);
    path_34.cubicTo(104.701, 36.1619, 103.195, 35.8741, 102.1, 35.3834);
    path_34.cubicTo(101.552, 35.1377, 101.109, 34.8416, 100.803, 34.6068);
    path_34.cubicTo(100.65, 34.4895, 100.532, 34.3879, 100.452, 34.3161);
    path_34.close();

    Paint paint_34_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_34_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_34, paint_34_stroke);

    Paint paint_34_fill = Paint()..style = PaintingStyle.fill;
    paint_34_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_34, paint_34_fill);

    Path path_35 = Path();
    path_35.moveTo(87.7316, 23.602);
    path_35.lineTo(87.7316, 23.602);
    path_35.cubicTo(86.2856, 23.4063, 84.7, 22.4178, 83.5186, 20.9863);
    path_35.cubicTo(82.3389, 19.5569, 81.5813, 17.7089, 81.7588, 15.8155);
    path_35.lineTo(81.6114, 15.8017);
    path_35.lineTo(81.7588, 15.8155);
    path_35.cubicTo(81.9612, 13.6568, 82.7905, 12.6149, 83.6011, 11.9895);
    path_35.cubicTo(83.934, 11.7326, 84.264, 11.5453, 84.5624, 11.376);
    path_35.cubicTo(84.6339, 11.3354, 84.7035, 11.2959, 84.771, 11.2567);
    path_35.cubicTo(84.9425, 11.157, 85.1031, 11.0572, 85.234, 10.9482);
    path_35.cubicTo(85.3656, 10.8388, 85.4772, 10.7121, 85.5391, 10.5545);
    path_35.lineTo(85.5391, 10.5545);
    path_35.cubicTo(85.6146, 10.3624, 85.6856, 10.0863, 85.759, 9.75825);
    path_35.cubicTo(85.8163, 9.50162, 85.8766, 9.20605, 85.9423, 8.88428);
    path_35.cubicTo(85.9611, 8.79215, 85.9803, 8.69787, 86, 8.60173);
    path_35.cubicTo(86.1779, 7.73528, 86.3978, 6.70913, 86.709, 5.69625);
    path_35.cubicTo(87.0205, 4.68235, 87.4208, 3.6906, 87.9553, 2.89042);
    path_35.cubicTo(88.4894, 2.09072, 89.1493, 1.49432, 89.9776, 1.24697);
    path_35.cubicTo(92.3939, 0.525426, 94.0481, 1.28737, 95.1047, 2.22326);
    path_35.cubicTo(95.6361, 2.69405, 96.018, 3.21093, 96.2672, 3.6109);
    path_35.cubicTo(96.3916, 3.81061, 96.4824, 3.98041, 96.5418, 4.09962);
    path_35.cubicTo(96.5715, 4.15919, 96.5934, 4.20606, 96.6076, 4.23763);
    path_35.cubicTo(96.6147, 4.25341, 96.6199, 4.26535, 96.6233, 4.27314);
    path_35.lineTo(96.6269, 4.28166);
    path_35.lineTo(96.6277, 4.28351);
    path_35.lineTo(96.6278, 4.2837);
    path_35.lineTo(96.6278, 4.28381);
    path_35.cubicTo(96.6278, 4.28381, 96.6278, 4.2838, 96.6282, 4.28363);
    path_35.lineTo(96.9596, 4.22786);
    path_35.cubicTo(97.1015, 1.69915, 98.6591, 0.0333451, 100.59, 0.181812);
    path_35.lineTo(100.603, 0.00980463);
    path_35.lineTo(100.59, 0.181812);
    path_35.cubicTo(104.693, 0.497345, 107.012, 1.8503, 108.2, 4.2662);
    path_35.cubicTo(109.392, 6.68914, 109.469, 10.2182, 108.963, 14.9626);
    path_35.lineTo(108.856, 14.9977);
    path_35.cubicTo(108.717, 15.0432, 108.513, 15.1101, 108.253, 15.1951);
    path_35.cubicTo(107.733, 15.3653, 106.987, 15.6086, 106.086, 15.9009);
    path_35.cubicTo(104.285, 16.4855, 101.863, 17.2663, 99.3814, 18.0511);
    path_35.cubicTo(96.8995, 18.836, 94.3585, 19.6245, 92.3184, 20.2248);
    path_35.cubicTo(91.2982, 20.5249, 90.4044, 20.7777, 89.7063, 20.9592);
    path_35.cubicTo(89.0015, 21.1425, 88.5128, 21.2487, 88.2921, 21.2634);
    path_35.lineTo(88.1722, 21.2714);
    path_35.lineTo(88.1381, 21.3866);
    path_35.cubicTo(88.0114, 21.8151, 88.0184, 22.189, 88.0518, 22.5075);
    path_35.cubicTo(88.0628, 22.6118, 88.0772, 22.7143, 88.0904, 22.8083);
    path_35.cubicTo(88.0968, 22.8541, 88.103, 22.8979, 88.1083, 22.9389);
    path_35.cubicTo(88.1252, 23.0693, 88.135, 23.1784, 88.1299, 23.2748);
    path_35.lineTo(88.2683, 23.2822);
    path_35.lineTo(88.1299, 23.2748);
    path_35.cubicTo(88.1191, 23.4778, 87.9353, 23.6296, 87.7316, 23.602);
    path_35.close();

    Paint paint_35_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_35_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_35, paint_35_stroke);

    Paint paint_35_fill = Paint()..style = PaintingStyle.fill;
    paint_35_fill.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_35, paint_35_fill);

    Path path_36 = Path();
    path_36.moveTo(107.34, 20.259);
    path_36.lineTo(107.34, 20.259);
    path_36.lineTo(107.34, 20.259);
    path_36.lineTo(107.253, 20.3186);
    path_36.lineTo(107.266, 20.4227);
    path_36.cubicTo(107.671, 23.6186, 107.25, 27.0675, 106.1, 29.7104);
    path_36.cubicTo(104.949, 32.3573, 103.092, 34.1514, 100.643, 34.1514);
    path_36.cubicTo(98.1663, 34.1514, 96.003, 32.7437, 94.0087, 30.4247);
    path_36.cubicTo(92.0207, 28.1132, 90.2221, 24.9205, 88.4696, 21.3917);
    path_36.cubicTo(88.4695, 21.3854, 88.4694, 21.3782, 88.4694, 21.3701);
    path_36.cubicTo(88.4693, 21.3317, 88.4705, 21.2739, 88.4754, 21.1993);
    path_36.cubicTo(88.4852, 21.0503, 88.51, 20.8347, 88.5704, 20.5739);
    path_36.cubicTo(88.6911, 20.0525, 88.9537, 19.351, 89.5225, 18.6376);
    path_36.cubicTo(89.7944, 18.2966, 90.264, 17.9632, 90.8619, 17.6264);
    path_36.cubicTo(91.4566, 17.2914, 92.1589, 16.9632, 92.8826, 16.6254);
    path_36.lineTo(92.8984, 16.6181);
    path_36.cubicTo(93.6144, 16.2839, 94.35, 15.9407, 95.0073, 15.577);
    path_36.cubicTo(95.6679, 15.2115, 96.2618, 14.8188, 96.6824, 14.3838);
    path_36.cubicTo(97.0887, 13.9634, 97.4675, 13.3233, 97.8061, 12.6138);
    path_36.cubicTo(98.1463, 11.901, 98.4521, 11.1048, 98.7083, 10.3617);
    path_36.cubicTo(98.9647, 9.6182, 99.1724, 8.92533, 99.316, 8.41846);
    path_36.cubicTo(99.3668, 8.23894, 99.4097, 8.08265, 99.4438, 7.9556);
    path_36.cubicTo(99.5233, 8.0721, 99.6233, 8.2195, 99.741, 8.39448);
    path_36.cubicTo(100.062, 8.87203, 100.515, 9.55494, 101.04, 10.3759);
    path_36.cubicTo(102.091, 12.0184, 103.432, 14.2114, 104.595, 16.4174);
    path_36.lineTo(104.767, 16.7444);
    path_36.lineTo(104.907, 16.4023);
    path_36.lineTo(104.907, 16.4023);
    path_36.lineTo(104.907, 16.4019);
    path_36.lineTo(104.908, 16.4002);
    path_36.lineTo(104.911, 16.3928);
    path_36.lineTo(104.924, 16.3629);
    path_36.cubicTo(104.935, 16.3366, 104.952, 16.2977, 104.974, 16.2485);
    path_36.cubicTo(105.019, 16.1501, 105.085, 16.0106, 105.169, 15.8472);
    path_36.cubicTo(105.338, 15.5196, 105.579, 15.1005, 105.871, 14.7256);
    path_36.cubicTo(106.165, 14.3474, 106.497, 14.032, 106.842, 13.8882);
    path_36.cubicTo(107.012, 13.8174, 107.182, 13.7893, 107.353, 13.8127);
    path_36.cubicTo(107.525, 13.836, 107.708, 13.9123, 107.902, 14.0676);
    path_36.cubicTo(108.898, 14.8644, 109.343, 15.6425, 109.471, 16.3507);
    path_36.cubicTo(109.6, 17.0603, 109.415, 17.7268, 109.101, 18.3085);
    path_36.cubicTo(108.787, 18.8911, 108.347, 19.3793, 107.982, 19.7237);
    path_36.cubicTo(107.8, 19.8954, 107.638, 20.03, 107.523, 20.1215);
    path_36.cubicTo(107.465, 20.1671, 107.418, 20.202, 107.387, 20.2252);
    path_36.cubicTo(107.371, 20.2367, 107.359, 20.2454, 107.351, 20.2511);
    path_36.lineTo(107.342, 20.2573);
    path_36.lineTo(107.34, 20.2587);
    path_36.lineTo(107.34, 20.259);
    path_36.close();

    Paint paint_36_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002116748;
    paint_36_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_36, paint_36_stroke);

    Paint paint_36_fill = Paint()..style = PaintingStyle.fill;
    paint_36_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_36, paint_36_fill);

    Path path_37 = Path();
    path_37.moveTo(44.1543, 8.91668);
    path_37.cubicTo(44.1543, 13.7743, 47.1921, 17.6088, 50.8272, 17.6088);
    path_37.cubicTo(54.4623, 17.6088, 57.5001, 13.7743, 57.5001, 8.91668);
    path_37.cubicTo(57.5001, 4.05911, 54.4623, 0.224541, 50.8272, 0.224541);
    path_37.cubicTo(47.1921, 0.224541, 44.1543, 4.05911, 44.1543, 8.91668);
    path_37.close();

    Paint paint_37_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002755098;
    paint_37_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_37, paint_37_stroke);

    Paint paint_37_fill = Paint()..style = PaintingStyle.fill;
    paint_37_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_37, paint_37_fill);

    Path path_38 = Path();
    path_38.moveTo(41.7461, 8.91668);
    path_38.cubicTo(41.7461, 13.7743, 44.7839, 17.6088, 48.419, 17.6088);
    path_38.cubicTo(52.0541, 17.6088, 55.0919, 13.7743, 55.0919, 8.91668);
    path_38.cubicTo(55.0919, 4.05911, 52.0541, 0.224541, 48.419, 0.224541);
    path_38.cubicTo(44.7839, 0.224541, 41.7461, 4.05911, 41.7461, 8.91668);
    path_38.close();

    Paint paint_38_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002755098;
    paint_38_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_38, paint_38_stroke);

    Paint paint_38_fill = Paint()..style = PaintingStyle.fill;
    paint_38_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_38, paint_38_fill);

    Path path_39 = Path();
    path_39.moveTo(41.5103, 15.2545);
    path_39.lineTo(41.7289, 15.3061);
    path_39.lineTo(41.763, 15.1617);
    path_39.lineTo(41.5103, 15.2545);
    path_39.close();
    path_39.moveTo(41.5103, 15.2545);
    path_39.cubicTo(41.7289, 15.3061, 41.7289, 15.3062, 41.7288, 15.3064);
    path_39.lineTo(41.7286, 15.3073);
    path_39.lineTo(41.7277, 15.3111);
    path_39.lineTo(41.7242, 15.3261);
    path_39.lineTo(41.7101, 15.3857);
    path_39.lineTo(41.6554, 15.6186);
    path_39.cubicTo(41.6074, 15.8231, 41.537, 16.1239, 41.4473, 16.5089);
    path_39.cubicTo(41.2679, 17.279, 41.0111, 18.3862, 40.7019, 19.7347);
    path_39.cubicTo(40.0835, 22.4317, 39.2555, 26.0936, 38.417, 29.9532);
    path_39.cubicTo(37.5783, 33.813, 36.7293, 37.8693, 36.0689, 41.3557);
    path_39.cubicTo(35.4074, 44.8474, 34.938, 47.7526, 34.854, 49.3184);
    path_39.cubicTo(34.6843, 52.4782, 34.7375, 56.1932, 35.3347, 59.0546);
    path_39.cubicTo(35.6338, 60.4875, 36.0649, 61.6834, 36.6551, 62.4886);
    path_39.cubicTo(37.2377, 63.2833, 37.9626, 63.6844, 38.8816, 63.5823);
    path_39.cubicTo(39.7985, 63.4804, 40.643, 62.7801, 41.4154, 61.6253);
    path_39.cubicTo(42.1833, 60.4771, 42.8499, 58.9238, 43.4241, 57.2082);
    path_39.cubicTo(44.5718, 53.7788, 45.3334, 49.7542, 45.8099, 47.1551);
    path_39.cubicTo(46.0444, 45.8762, 46.1959, 43.2652, 46.2894, 40.0487);
    path_39.cubicTo(46.3826, 36.839, 46.4178, 33.045, 46.4231, 29.4118);
    path_39.cubicTo(46.4285, 25.7789, 46.4041, 22.3081, 46.3784, 19.7455);
    path_39.cubicTo(46.3655, 18.4642, 46.3523, 17.41, 46.3423, 16.6762);
    path_39.cubicTo(46.3373, 16.3092, 46.3331, 16.0224, 46.3302, 15.8274);
    path_39.lineTo(46.3268, 15.605);
    path_39.lineTo(46.3259, 15.5482);
    path_39.lineTo(46.3256, 15.5338);
    path_39.lineTo(46.3256, 15.5317);
    path_39.lineTo(45.9717, 6.79233);
    path_39.cubicTo(45.8482, 6.78856, 45.6738, 6.78474, 45.4628, 6.78415);
    path_39.cubicTo(45.0264, 6.78294, 44.4354, 6.79556, 43.8148, 6.85058);
    path_39.cubicTo(43.1927, 6.90574, 42.549, 7.00282, 42.004, 7.16763);
    path_39.cubicTo(41.4503, 7.33503, 41.0392, 7.56042, 40.8338, 7.8436);
    path_39.lineTo(40.5497, 8.23508);
    path_39.lineTo(40.434, 7.76544);
    path_39.cubicTo(40.2567, 7.04592, 39.9073, 6.38028, 39.4157, 5.82581);
    path_39.cubicTo(39.3701, 5.77603, 39.3106, 5.74112, 39.2448, 5.72566);
    path_39.cubicTo(39.1787, 5.7101, 39.1095, 5.71495, 39.0462, 5.73956);
    path_39.cubicTo(38.9829, 5.76418, 38.9286, 5.8074, 38.8903, 5.86352);
    path_39.cubicTo(38.8521, 5.91963, 38.8318, 5.98602, 38.8321, 6.05394);
    path_39.lineTo(38.8321, 6.05536);
    path_39.cubicTo(38.8293, 7.29372, 38.8302, 9.2886, 38.8573, 10.8001);
    path_39.lineTo(38.8573, 10.8002);
    path_39.cubicTo(38.8764, 11.8986, 39.5508, 12.9599, 40.256, 13.767);
    path_39.cubicTo(40.6049, 14.1662, 40.9531, 14.494, 41.2142, 14.722);
    path_39.cubicTo(41.3446, 14.8359, 41.4529, 14.9245, 41.5281, 14.9844);
    path_39.cubicTo(41.5658, 15.0143, 41.5951, 15.037, 41.6148, 15.0521);
    path_39.lineTo(41.6369, 15.0688);
    path_39.lineTo(41.6422, 15.0728);
    path_39.lineTo(41.6434, 15.0736);
    path_39.lineTo(41.6435, 15.0737);
    path_39.lineTo(41.6436, 15.0738);
    path_39.lineTo(41.5103, 15.2545);
    path_39.close();

    Paint paint_39_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002755098;
    paint_39_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_39, paint_39_stroke);

    Paint paint_39_fill = Paint()..style = PaintingStyle.fill;
    paint_39_fill.color = Color(0xff000000).withOpacity(1.0);
    canvas.drawPath(path_39, paint_39_fill);

    Path path_40 = Path();
    path_40.moveTo(42.4757, 6.81097);
    path_40.cubicTo(42.4757, 7.03821, 42.6776, 7.31121, 43.1605, 7.54067);
    path_40.cubicTo(43.6266, 7.76209, 44.2862, 7.9052, 45.027, 7.9052);
    path_40.cubicTo(45.7677, 7.9052, 46.4273, 7.76209, 46.8934, 7.54067);
    path_40.cubicTo(47.3763, 7.31122, 47.5782, 7.03821, 47.5782, 6.81097);
    path_40.cubicTo(47.5782, 6.58372, 47.3763, 6.31072, 46.8934, 6.08126);
    path_40.cubicTo(46.4273, 5.85984, 45.7677, 5.71673, 45.027, 5.71673);
    path_40.cubicTo(44.2862, 5.71673, 43.6266, 5.85984, 43.1605, 6.08127);
    path_40.cubicTo(42.6776, 6.31072, 42.4757, 6.58373, 42.4757, 6.81097);
    path_40.close();

    Paint paint_40_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002755098;
    paint_40_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_40, paint_40_stroke);

    Paint paint_40_fill = Paint()..style = PaintingStyle.fill;
    paint_40_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_40, paint_40_fill);

    Path path_41 = Path();
    path_41.moveTo(30.4199, 8.91668);
    path_41.cubicTo(30.4199, 13.7743, 33.4577, 17.6088, 37.0928, 17.6088);
    path_41.cubicTo(40.7279, 17.6088, 43.7657, 13.7743, 43.7657, 8.91668);
    path_41.cubicTo(43.7657, 4.05911, 40.7279, 0.224541, 37.0928, 0.224541);
    path_41.cubicTo(33.4577, 0.224541, 30.4199, 4.05911, 30.4199, 8.91668);
    path_41.close();

    Paint paint_41_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002755098;
    paint_41_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_41, paint_41_stroke);

    Paint paint_41_fill = Paint()..style = PaintingStyle.fill;
    paint_41_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_41, paint_41_fill);

    Path path_42 = Path();
    path_42.moveTo(28.0078, 8.91668);
    path_42.cubicTo(28.0078, 13.7743, 31.0456, 17.6088, 34.6807, 17.6088);
    path_42.cubicTo(38.3158, 17.6088, 41.3536, 13.7743, 41.3536, 8.91668);
    path_42.cubicTo(41.3536, 4.05911, 38.3158, 0.224541, 34.6807, 0.224541);
    path_42.cubicTo(31.0456, 0.224541, 28.0078, 4.05911, 28.0078, 8.91668);
    path_42.close();

    Paint paint_42_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002755098;
    paint_42_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_42, paint_42_stroke);

    Paint paint_42_fill = Paint()..style = PaintingStyle.fill;
    paint_42_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_42, paint_42_fill);

    Path path_43 = Path();
    path_43.moveTo(33.2463, 6.33594);
    path_43.cubicTo(34.3484, 6.33594, 35.2422, 7.49127, 35.2422, 8.91559);
    path_43.cubicTo(35.2422, 10.3399, 34.3484, 11.4952, 33.2463, 11.4952);
    path_43.cubicTo(32.1442, 11.4952, 31.2504, 10.3408, 31.2504, 8.91559);
    path_43.cubicTo(31.2504, 7.49034, 32.1442, 6.33594, 33.2463, 6.33594);
    path_43.close();

    Paint paint_43_fill = Paint()..style = PaintingStyle.fill;
    paint_43_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_43, paint_43_fill);

    Path path_44 = Path();
    path_44.moveTo(33.245, 6.15234);
    path_44.cubicTo(34.448, 6.15234, 35.4277, 7.3936, 35.4277, 8.91879);
    path_44.cubicTo(35.4277, 10.444, 34.448, 11.6852, 33.245, 11.6852);
    path_44.cubicTo(32.0421, 11.6852, 31.0623, 10.4449, 31.0623, 8.91879);
    path_44.cubicTo(31.0623, 7.39267, 32.0411, 6.15234, 33.245, 6.15234);
    path_44.close();
    path_44.moveTo(33.245, 11.3116);
    path_44.cubicTo(34.2425, 11.3116, 35.0541, 10.2385, 35.0541, 8.91879);
    path_44.cubicTo(35.0541, 7.59908, 34.2425, 6.52593, 33.245, 6.52593);
    path_44.cubicTo(32.2475, 6.52593, 31.4359, 7.60001, 31.4359, 8.91879);
    path_44.cubicTo(31.4359, 10.2376, 32.2475, 11.3116, 33.245, 11.3116);
    path_44.lineTo(33.245, 11.3116);
    path_44.close();

    Paint paint_44_fill = Paint()..style = PaintingStyle.fill;
    paint_44_fill.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_44, paint_44_fill);

    Path path_45 = Path();
    path_45.moveTo(32.1779, 6.33594);
    path_45.cubicTo(33.28, 6.33594, 34.1738, 7.49127, 34.1738, 8.91559);
    path_45.cubicTo(34.1738, 10.3399, 33.28, 11.4952, 32.1779, 11.4952);
    path_45.cubicTo(31.0758, 11.4952, 30.182, 10.3408, 30.182, 8.91559);
    path_45.cubicTo(30.182, 7.49034, 31.0758, 6.33594, 32.1779, 6.33594);
    path_45.close();

    Paint paint_45_fill = Paint()..style = PaintingStyle.fill;
    paint_45_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_45, paint_45_fill);

    Path path_46 = Path();
    path_46.moveTo(32.1767, 6.15234);
    path_46.cubicTo(33.3806, 6.15234, 34.3594, 7.3936, 34.3594, 8.91879);
    path_46.cubicTo(34.3594, 10.444, 33.3806, 11.6852, 32.1767, 11.6852);
    path_46.cubicTo(30.9728, 11.6852, 29.994, 10.4449, 29.994, 8.91879);
    path_46.cubicTo(29.994, 7.39267, 30.9737, 6.15234, 32.1767, 6.15234);
    path_46.close();
    path_46.moveTo(32.1767, 11.3116);
    path_46.cubicTo(33.1742, 11.3116, 33.9858, 10.2385, 33.9858, 8.91879);
    path_46.cubicTo(33.9858, 7.59908, 33.1742, 6.52593, 32.1767, 6.52593);
    path_46.cubicTo(31.1792, 6.52593, 30.3676, 7.60001, 30.3676, 8.91879);
    path_46.cubicTo(30.3676, 10.2376, 31.1792, 11.3116, 32.1767, 11.3116);
    path_46.lineTo(32.1767, 11.3116);
    path_46.close();

    Paint paint_46_fill = Paint()..style = PaintingStyle.fill;
    paint_46_fill.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_46, paint_46_fill);

    Path path_47 = Path();
    path_47.moveTo(21.4434, 8.91668);
    path_47.cubicTo(21.4434, 13.7743, 24.4812, 17.6088, 28.1163, 17.6088);
    path_47.cubicTo(31.7514, 17.6088, 34.7891, 13.7743, 34.7891, 8.91668);
    path_47.cubicTo(34.7891, 4.05911, 31.7514, 0.224541, 28.1163, 0.224541);
    path_47.cubicTo(24.4812, 0.224541, 21.4434, 4.05911, 21.4434, 8.91668);
    path_47.close();

    Paint paint_47_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002755098;
    paint_47_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_47, paint_47_stroke);

    Paint paint_47_fill = Paint()..style = PaintingStyle.fill;
    paint_47_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_47, paint_47_fill);

    Path path_48 = Path();
    path_48.moveTo(19.0351, 8.91668);
    path_48.cubicTo(19.0351, 13.7743, 22.0729, 17.6088, 25.708, 17.6088);
    path_48.cubicTo(29.3432, 17.6088, 32.3809, 13.7743, 32.3809, 8.91668);
    path_48.cubicTo(32.3809, 4.05911, 29.3432, 0.224541, 25.708, 0.224541);
    path_48.cubicTo(22.0729, 0.224541, 19.0351, 4.05911, 19.0351, 8.91668);
    path_48.close();

    Paint paint_48_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002755098;
    paint_48_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_48, paint_48_stroke);

    Paint paint_48_fill = Paint()..style = PaintingStyle.fill;
    paint_48_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_48, paint_48_fill);

    Path path_49 = Path();
    path_49.moveTo(44.6558, 62.1813);
    path_49.lineTo(44.6558, 62.1812);
    path_49.cubicTo(43.71, 57.9449, 40.9409, 53.9067, 38.3903, 50.9161);
    path_49.cubicTo(37.1173, 49.4235, 35.9036, 48.1975, 35.0084, 47.3448);
    path_49.cubicTo(34.5608, 46.9186, 34.1931, 46.5858, 33.9376, 46.3599);
    path_49.cubicTo(33.8259, 46.2612, 33.7357, 46.1829, 33.6697, 46.1262);
    path_49.lineTo(19.7742, 55.6877);
    path_49.lineTo(22.2832, 81.6327);
    path_49.lineTo(22.2843, 81.6435);
    path_49.lineTo(22.2843, 81.6543);
    path_49.lineTo(22.2843, 81.6544);
    path_49.lineTo(22.2843, 81.6547);
    path_49.lineTo(22.2843, 81.6562);
    path_49.lineTo(22.2843, 81.6622);
    path_49.lineTo(22.2844, 81.6868);
    path_49.lineTo(22.285, 81.785);
    path_49.cubicTo(22.2856, 81.8721, 22.287, 82.0023, 22.2897, 82.1738);
    path_49.cubicTo(22.2951, 82.517, 22.3059, 83.0257, 22.3276, 83.6868);
    path_49.cubicTo(22.3704, 84.993, 22.4556, 86.8944, 22.6249, 89.2895);
    path_49.cubicTo(25.6806, 92.1247, 30.2802, 92.9525, 34.1678, 93.0565);
    path_49.cubicTo(36.1191, 93.1086, 37.8779, 92.9782, 39.1489, 92.8345);
    path_49.cubicTo(39.7842, 92.7627, 40.2971, 92.6877, 40.6507, 92.6307);
    path_49.cubicTo(40.8275, 92.6023, 40.9644, 92.5783, 41.0568, 92.5615);
    path_49.cubicTo(41.0568, 92.5615, 41.0568, 92.5615, 41.0568, 92.5615);
    path_49.cubicTo(41.0646, 92.5302, 41.0735, 92.4938, 41.0837, 92.4523);
    path_49.cubicTo(41.1238, 92.2892, 41.1824, 92.0487, 41.2563, 91.7395);
    path_49.cubicTo(41.4041, 91.121, 41.6132, 90.2276, 41.8582, 89.1286);
    path_49.cubicTo(42.3482, 86.9304, 42.9815, 83.9105, 43.5549, 80.6234);
    path_49.cubicTo(44.1284, 77.3356, 44.6414, 73.7836, 44.8916, 70.5203);
    path_49.cubicTo(45.1422, 67.2519, 45.1274, 64.2927, 44.6558, 62.1813);
    path_49.close();

    Paint paint_49_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002755098;
    paint_49_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_49, paint_49_stroke);

    Paint paint_49_fill = Paint()..style = PaintingStyle.fill;
    paint_49_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_49, paint_49_fill);

    Path path_50 = Path();
    path_50.moveTo(26.5223, 26.7233);
    path_50.cubicTo(26.5223, 26.7233, 29.1235, 23.4431, 34.4266, 24.789);
    path_50.cubicTo(38.5772, 25.8425, 42.2617, 27.5657, 43.6664, 28.2653);
    path_50.cubicTo(43.9475, 28.4, 44.1963, 28.5935, 44.3959, 28.8328);
    path_50.cubicTo(44.5956, 29.0721, 44.7415, 29.3516, 44.8236, 29.6522);
    path_50.cubicTo(45.016, 30.3994, 44.7919, 31.3063, 42.7567, 31.5034);
    path_50.cubicTo(39.1404, 31.8545, 31.648, 33.2004, 31.648, 33.2004);
    path_50.lineTo(27.192, 43.0585);
    path_50.lineTo(22.0635, 34.9647);
    path_50.cubicTo(22.0635, 34.9647, 23.3272, 26.387, 26.5223, 26.7233);
    path_50.close();

    Paint paint_50_fill = Paint()..style = PaintingStyle.fill;
    paint_50_fill.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_50, paint_50_fill);

    Path path_51 = Path();
    path_51.moveTo(26.3356, 26.5229);
    path_51.lineTo(26.4384, 26.5229);
    path_51.cubicTo(26.9958, 25.9223, 27.6611, 25.4319, 28.3997, 25.0771);
    path_51.cubicTo(29.626, 24.4682, 31.6752, 23.891, 34.4706, 24.6008);
    path_51.cubicTo(38.6417, 25.659, 42.3477, 27.3943, 43.7477, 28.0901);
    path_51.cubicTo(44.0529, 28.2369, 44.3229, 28.4475, 44.5395, 28.7077);
    path_51.cubicTo(44.7561, 28.968, 44.9142, 29.2718, 45.003, 29.5985);
    path_51.cubicTo(45.1235, 30.0711, 45.0665, 30.4793, 44.834, 30.8127);
    path_51.cubicTo(44.4987, 31.2909, 43.8066, 31.5832, 42.7736, 31.6832);
    path_51.cubicTo(39.4272, 32.0082, 32.6652, 33.2027, 31.776, 33.3643);
    path_51.lineTo(27.2182, 43.4513);
    path_51.lineTo(21.8675, 35.0072);
    path_51.lineTo(21.8768, 34.9381);
    path_51.cubicTo(21.9226, 34.5832, 23.1498, 26.5229, 26.3356, 26.5229);
    path_51.close();
    path_51.moveTo(27.1613, 42.6611);
    path_51.lineTo(31.5145, 33.03);
    path_51.lineTo(31.6079, 33.0122);
    path_51.cubicTo(31.6826, 32.9991, 39.1489, 31.6617, 42.7316, 31.3133);
    path_51.cubicTo(43.6506, 31.2199, 44.253, 30.9845, 44.522, 30.6007);
    path_51.cubicTo(44.6901, 30.3606, 44.7284, 30.0562, 44.635, 29.6947);
    path_51.cubicTo(44.5598, 29.42, 44.4262, 29.1646, 44.2435, 28.9461);
    path_51.cubicTo(44.0607, 28.7275, 43.833, 28.5509, 43.5759, 28.4282);
    path_51.cubicTo(42.1871, 27.7362, 38.5091, 26.0149, 34.3725, 24.966);
    path_51.cubicTo(33.5837, 24.7605, 32.7723, 24.6544, 31.9572, 24.6503);
    path_51.cubicTo(30.788, 24.6385, 29.6318, 24.8971, 28.579, 25.4059);
    path_51.cubicTo(27.3154, 26.0289, 26.6709, 26.8227, 26.6606, 26.8358);
    path_51.lineTo(26.5971, 26.9161);
    path_51.lineTo(26.4953, 26.9049);
    path_51.cubicTo(24.9683, 26.7424, 23.8951, 28.9177, 23.2647, 30.7697);
    path_51.cubicTo(22.8199, 32.1249, 22.4811, 33.5126, 22.2513, 34.9203);
    path_51.lineTo(27.1613, 42.6611);
    path_51.close();

    Paint paint_51_fill = Paint()..style = PaintingStyle.fill;
    paint_51_fill.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_51, paint_51_fill);

    Path path_52 = Path();
    path_52.moveTo(26.7566, 39.3925);
    path_52.cubicTo(26.7821, 39.3596, 26.8148, 39.3171, 26.8536, 39.2658);
    path_52.cubicTo(26.954, 39.1333, 27.0957, 38.9419, 27.2619, 38.7056);
    path_52.cubicTo(27.594, 38.2334, 28.0253, 37.5794, 28.4202, 36.8548);
    path_52.cubicTo(28.8142, 36.1319, 29.1769, 35.3296, 29.3651, 34.5619);
    path_52.cubicTo(29.5235, 33.9162, 29.5648, 33.2686, 29.3716, 32.7097);
    path_52.cubicTo(30.9573, 32.4946, 31.8753, 31.8806, 32.3998, 31.2821);
    path_52.cubicTo(32.6763, 30.9665, 32.8385, 30.6606, 32.9319, 30.4311);
    path_52.cubicTo(32.9589, 30.3647, 32.9803, 30.3046, 32.9969, 30.2524);
    path_52.lineTo(36.7503, 30.8225);
    path_52.lineTo(36.7506, 30.8226);
    path_52.lineTo(42.5102, 31.6891);
    path_52.lineTo(42.4606, 32.1621);
    path_52.lineTo(42.4606, 32.1622);
    path_52.cubicTo(42.2971, 33.7321, 41.906, 37.7272, 41.775, 41.1318);
    path_52.lineTo(26.7566, 39.3925);
    path_52.close();
    path_52.moveTo(26.7566, 39.3925);
    path_52.lineTo(27.6634, 44.4247);
    path_52.lineTo(27.6693, 44.4575);
    path_52.lineTo(27.6654, 44.4906);
    path_52.lineTo(27.4424, 44.4645);
    path_52.lineTo(27.6654, 44.4906);
    path_52.lineTo(27.6654, 44.4908);
    path_52.lineTo(27.6653, 44.4918);
    path_52.lineTo(27.6648, 44.4959);
    path_52.lineTo(27.6629, 44.5125);
    path_52.lineTo(27.6556, 44.5779);
    path_52.cubicTo(27.6493, 44.6355, 27.6403, 44.7202, 27.6293, 44.8285);
    path_52.cubicTo(27.6075, 45.0451, 27.5781, 45.3557, 27.548, 45.7312);
    path_52.cubicTo(27.4879, 46.4826, 27.4252, 47.4918, 27.4148, 48.5258);
    path_52.cubicTo(27.4044, 49.5621, 27.4467, 50.6134, 27.593, 51.4529);
    path_52.cubicTo(27.6663, 51.8731, 27.764, 52.2301, 27.8885, 52.5024);
    path_52.cubicTo(28.0146, 52.7781, 28.1559, 52.9401, 28.3002, 53.0122);
    path_52.cubicTo(28.444, 53.0841, 28.5796, 53.2081, 28.7073, 53.3562);
    path_52.cubicTo(28.8372, 53.5068, 28.9705, 53.6961, 29.1068, 53.9147);
    path_52.cubicTo(29.3792, 54.352, 29.6743, 54.9245, 29.9861, 55.5806);
    path_52.cubicTo(30.381, 56.4113, 30.8094, 57.3902, 31.2581, 58.4156);
    path_52.cubicTo(31.5171, 59.0075, 31.7829, 59.6148, 32.0529, 60.218);
    path_52.cubicTo(32.7939, 61.8736, 33.5693, 63.5043, 34.34, 64.7454);
    path_52.cubicTo(34.7255, 65.3662, 35.1047, 65.881, 35.4718, 66.251);
    path_52.cubicTo(35.8425, 66.6246, 36.1783, 66.8273, 36.4749, 66.8697);
    path_52.cubicTo(37.095, 66.9584, 37.6478, 66.6364, 38.1267, 65.9663);
    path_52.cubicTo(38.6065, 65.2948, 38.9814, 64.31, 39.2303, 63.1836);
    path_52.cubicTo(39.7288, 60.9279, 39.7029, 58.1974, 39.0924, 56.4845);
    path_52.cubicTo(38.4806, 54.7682, 37.2488, 53.3264, 36.1572, 52.3077);
    path_52.cubicTo(35.613, 51.7999, 35.1075, 51.4008, 34.7384, 51.1289);
    path_52.cubicTo(34.5539, 50.993, 34.4038, 50.8891, 34.3002, 50.8195);
    path_52.cubicTo(34.2484, 50.7846, 34.2083, 50.7584, 34.1814, 50.741);
    path_52.lineTo(34.1511, 50.7216);
    path_52.lineTo(34.1438, 50.717);
    path_52.lineTo(34.1421, 50.7159);
    path_52.lineTo(34.1418, 50.7157);
    path_52.lineTo(34.1418, 50.7157);
    path_52.lineTo(34.1418, 50.7157);
    path_52.lineTo(34.0599, 50.6648);
    path_52.lineTo(34.0405, 50.5703);
    path_52.lineTo(33.9564, 50.1613);
    path_52.lineTo(33.9564, 50.1612);
    path_52.lineTo(33.4511, 47.7096);
    path_52.lineTo(33.8775, 47.6052);
    path_52.cubicTo(33.8869, 47.6024, 33.8869, 47.6023, 33.8868, 47.6021);
    path_52.lineTo(33.8866, 47.6014);
    path_52.lineTo(33.8862, 47.6);
    path_52.lineTo(33.8854, 47.5973);
    path_52.lineTo(33.8837, 47.5922);
    path_52.lineTo(33.8803, 47.5831);
    path_52.cubicTo(33.8781, 47.5776, 33.876, 47.5732, 33.8742, 47.5698);
    path_52.cubicTo(33.8706, 47.563, 33.8686, 47.5609, 33.8702, 47.5629);
    path_52.cubicTo(33.8735, 47.567, 33.8923, 47.5883, 33.9434, 47.6205);
    path_52.cubicTo(34.0473, 47.6862, 34.2716, 47.7889, 34.7285, 47.8844);
    path_52.cubicTo(35.2358, 47.99, 36.0152, 48.0836, 37.2099, 48.1132);
    path_52.lineTo(37.2102, 48.1132);
    path_52.cubicTo(38.0173, 48.1343, 38.6862, 47.9734, 39.2401, 47.6641);
    path_52.cubicTo(39.7942, 47.3547, 40.246, 46.8897, 40.6079, 46.2834);
    path_52.cubicTo(41.3359, 45.064, 41.6914, 43.2884, 41.775, 41.1318);
    path_52.lineTo(26.7566, 39.3925);
    path_52.close();

    Paint paint_52_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002755098;
    paint_52_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_52, paint_52_stroke);

    Paint paint_52_fill = Paint()..style = PaintingStyle.fill;
    paint_52_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_52, paint_52_fill);

    Path path_53 = Path();
    path_53.moveTo(12.3009, 8.91559);
    path_53.cubicTo(12.3009, 7.55829, 11.4574, 6.56048, 10.5295, 6.56048);
    path_53.cubicTo(9.60427, 6.56048, 8.75903, 7.55749, 8.75903, 8.91559);
    path_53.cubicTo(8.75903, 10.2739, 9.60164, 11.2707, 10.5295, 11.2707);
    path_53.cubicTo(11.4574, 11.2707, 12.3009, 10.2729, 12.3009, 8.91559);
    path_53.close();

    Paint paint_53_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002755098;
    paint_53_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_53, paint_53_stroke);

    Paint paint_53_fill = Paint()..style = PaintingStyle.fill;
    paint_53_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_53, paint_53_fill);

    Path path_54 = Path();
    path_54.moveTo(11.2384, 8.91559);
    path_54.cubicTo(11.2384, 7.55829, 10.3949, 6.56048, 9.46699, 6.56048);
    path_54.cubicTo(8.53916, 6.56048, 7.69655, 7.55727, 7.69655, 8.91559);
    path_54.cubicTo(7.69655, 10.2739, 8.53916, 11.2707, 9.46699, 11.2707);
    path_54.cubicTo(10.3949, 11.2707, 11.2384, 10.2729, 11.2384, 8.91559);
    path_54.close();

    Paint paint_54_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002755098;
    paint_54_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_54, paint_54_stroke);

    Paint paint_54_fill = Paint()..style = PaintingStyle.fill;
    paint_54_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_54, paint_54_fill);

    Path path_55 = Path();
    path_55.moveTo(24.4086, 7.22484);
    path_55.lineTo(24.5493, 6.76226);
    path_55.cubicTo(24.7649, 6.05351, 25.1493, 5.40766, 25.6694, 4.8801);
    path_55.cubicTo(25.7175, 4.83256, 25.7788, 4.80064, 25.8454, 4.78854);
    path_55.cubicTo(25.9123, 4.77639, 25.9813, 4.78482, 26.0433, 4.81273);
    path_55.cubicTo(26.1053, 4.84063, 26.1573, 4.88668, 26.1926, 4.94482);
    path_55.cubicTo(26.2279, 5.00297, 26.2446, 5.07042, 26.2407, 5.13827);
    path_55.lineTo(26.2407, 5.13965);
    path_55.lineTo(26.2407, 5.13965);
    path_55.cubicTo(26.1772, 6.37609, 26.0689, 8.36911, 25.9607, 9.8769);
    path_55.lineTo(24.4086, 7.22484);
    path_55.close();
    path_55.moveTo(24.4086, 7.22484);
    path_55.lineTo(24.146, 6.81886);
    path_55.cubicTo(23.9558, 6.52488, 23.5573, 6.2777, 23.0133, 6.08083);
    path_55.cubicTo(22.4778, 5.88701, 21.8402, 5.75558, 21.2219, 5.66719);
    path_55.cubicTo(20.6051, 5.57902, 20.0156, 5.5348, 19.5797, 5.51266);
    path_55.cubicTo(19.369, 5.50196, 19.1946, 5.49644, 19.071, 5.4936);
    path_55.lineTo(18.2483, 14.2002);
    path_55.lineTo(18.2482, 14.2023);
    path_55.lineTo(18.2472, 14.2166);
    path_55.lineTo(18.2433, 14.2733);
    path_55.lineTo(18.2279, 14.4951);
    path_55.cubicTo(18.2145, 14.6898, 18.195, 14.9759, 18.1703, 15.3421);
    path_55.cubicTo(18.1211, 16.0743, 18.0514, 17.1263, 17.9699, 18.4051);
    path_55.cubicTo(17.8069, 20.9627, 17.5966, 24.4273, 17.4074, 28.0553);
    path_55.cubicTo(17.2181, 31.6837, 17.05, 35.4743, 16.9712, 38.6844);
    path_55.cubicTo(16.8923, 41.9014, 16.9038, 44.5168, 17.0695, 45.8066);
    path_55.cubicTo(17.4064, 48.4276, 17.9525, 52.4862, 18.9155, 55.9715);
    path_55.cubicTo(19.3973, 57.7152, 19.9799, 59.3018, 20.6851, 60.4897);
    path_55.cubicTo(21.3944, 61.6844, 22.1996, 62.4295, 23.1087, 62.5813);
    path_55.cubicTo(24.0197, 62.7334, 24.764, 62.3722, 25.3878, 61.61);
    path_55.cubicTo(26.0197, 60.8377, 26.5139, 59.6663, 26.8891, 58.2511);
    path_55.cubicTo(27.6384, 55.4251, 27.8907, 51.717, 27.8907, 48.5531);
    path_55.cubicTo(27.8907, 46.9852, 27.5778, 44.0592, 27.1046, 40.5372);
    path_55.cubicTo(26.6322, 37.0206, 26.0021, 32.9247, 25.3718, 29.0256);
    path_55.cubicTo(24.7416, 25.1268, 24.1113, 21.4259, 23.6386, 18.6997);
    path_55.cubicTo(23.4022, 17.3366, 23.2052, 16.2172, 23.0674, 15.4386);
    path_55.cubicTo(22.9984, 15.0493, 22.9443, 14.7453, 22.9074, 14.5385);
    path_55.lineTo(22.8652, 14.303);
    path_55.lineTo(22.8544, 14.2428);
    path_55.lineTo(22.8517, 14.2276);
    path_55.lineTo(22.851, 14.2238);
    path_55.lineTo(22.8508, 14.2228);
    path_55.cubicTo(22.8508, 14.2226, 22.8508, 14.2225, 23.0718, 14.1827);
    path_55.lineTo(22.8508, 14.2225);
    path_55.lineTo(22.8245, 14.0765);
    path_55.lineTo(22.9484, 13.9951);
    path_55.lineTo(24.4086, 7.22484);
    path_55.close();
    path_55.moveTo(22.9486, 13.995);
    path_55.lineTo(22.9485, 13.9951);
    path_55.lineTo(22.9486, 13.995);
    path_55.close();
    path_55.moveTo(22.9486, 13.995);
    path_55.lineTo(22.9498, 13.9942);
    path_55.moveTo(22.9486, 13.995);
    path_55.lineTo(22.9498, 13.9942);
    path_55.moveTo(22.9498, 13.9942);
    path_55.lineTo(22.9553, 13.9905);
    path_55.moveTo(22.9498, 13.9942);
    path_55.lineTo(22.9553, 13.9905);
    path_55.moveTo(22.9553, 13.9905);
    path_55.lineTo(22.9783, 13.975);
    path_55.moveTo(22.9553, 13.9905);
    path_55.lineTo(22.9783, 13.975);
    path_55.moveTo(22.9783, 13.975);
    path_55.cubicTo(22.9988, 13.961, 23.0293, 13.9399, 23.0684, 13.912);
    path_55.moveTo(22.9783, 13.975);
    path_55.lineTo(23.0684, 13.912);
    path_55.moveTo(23.0684, 13.912);
    path_55.cubicTo(23.1468, 13.8563, 23.2597, 13.7736, 23.396, 13.6669);
    path_55.moveTo(23.0684, 13.912);
    path_55.lineTo(23.396, 13.6669);
    path_55.moveTo(23.396, 13.6669);
    path_55.cubicTo(23.6689, 13.4532, 24.0341, 13.1445, 24.4039, 12.7646);
    path_55.moveTo(23.396, 13.6669);
    path_55.lineTo(24.4039, 12.7646);
    path_55.moveTo(24.4039, 12.7646);
    path_55.cubicTo(25.1515, 11.9964, 25.8821, 10.9728, 25.9607, 9.87691);
    path_55.lineTo(24.4039, 12.7646);
    path_55.close();

    Paint paint_55_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002755098;
    paint_55_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_55, paint_55_stroke);

    Paint paint_55_fill = Paint()..style = PaintingStyle.fill;
    paint_55_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_55, paint_55_fill);

    Path path_56 = Path();
    path_56.moveTo(7.70509, 8.91668);
    path_56.cubicTo(7.70509, 13.7743, 10.7428, 17.6088, 14.378, 17.6088);
    path_56.cubicTo(18.0131, 17.6088, 21.0509, 13.7743, 21.0509, 8.91668);
    path_56.cubicTo(21.0509, 4.05911, 18.0131, 0.224541, 14.378, 0.224541);
    path_56.cubicTo(10.7428, 0.224541, 7.70509, 4.05911, 7.70509, 8.91668);
    path_56.close();

    Paint paint_56_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002755098;
    paint_56_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_56, paint_56_stroke);

    Paint paint_56_fill = Paint()..style = PaintingStyle.fill;
    paint_56_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_56, paint_56_fill);

    Path path_57 = Path();
    path_57.moveTo(5.29689, 8.91668);
    path_57.cubicTo(5.29689, 13.7743, 8.33465, 17.6088, 11.9698, 17.6088);
    path_57.cubicTo(15.6049, 17.6088, 18.6426, 13.7743, 18.6426, 8.91668);
    path_57.cubicTo(18.6426, 4.05911, 15.6049, 0.224541, 11.9698, 0.224541);
    path_57.cubicTo(8.33465, 0.224541, 5.29689, 4.05911, 5.29689, 8.91668);
    path_57.close();

    Paint paint_57_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002755098;
    paint_57_stroke.color = Colors.black.withOpacity(1.0);
    canvas.drawPath(path_57, paint_57_stroke);

    Paint paint_57_fill = Paint()..style = PaintingStyle.fill;
    paint_57_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_57, paint_57_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

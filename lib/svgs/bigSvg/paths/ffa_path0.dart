import 'package:flutter/material.dart';

Path getPath0(Size size) {
  return Path()
    ..moveTo(size.width * 0.1949750, size.height * 0.1215230)
    ..cubicTo(
        size.width * 0.1936075,
        size.height * 0.1254463,
        size.width * 0.1913150,
        size.height * 0.1292008,
        size.width * 0.1877700,
        size.height * 0.1333277)
    ..cubicTo(
        size.width * 0.1852175,
        size.height * 0.1362975,
        size.width * 0.1851500,
        size.height * 0.1364713,
        size.width * 0.1856725,
        size.height * 0.1385124)
    ..cubicTo(
        size.width * 0.1862050,
        size.height * 0.1406032,
        size.width * 0.1864400,
        size.height * 0.1487825,
        size.width * 0.1859925,
        size.height * 0.1496715)
    ..cubicTo(
        size.width * 0.1858650,
        size.height * 0.1499248,
        size.width * 0.1845375,
        size.height * 0.1485044,
        size.width * 0.1830425,
        size.height * 0.1465179)
    ..cubicTo(
        size.width * 0.1728550,
        size.height * 0.1329453,
        size.width * 0.1593400,
        size.height * 0.1370722,
        size.width * 0.1476850,
        size.height * 0.1573194)
    ..cubicTo(
        size.width * 0.1406800,
        size.height * 0.1694866,
        size.width * 0.1378675,
        size.height * 0.1717413,
        size.width * 0.1294650,
        size.height * 0.1719449)
    ..cubicTo(
        size.width * 0.1188675,
        size.height * 0.1721982,
        size.width * 0.1095275,
        size.height * 0.1808543,
        size.width * 0.1031450,
        size.height * 0.1963240)
    ..cubicTo(
        size.width * 0.1015975,
        size.height * 0.2000735,
        size.width * 0.09893500,
        size.height * 0.2035449,
        size.width * 0.09760500,
        size.height * 0.2035449)
    ..cubicTo(
        size.width * 0.09500000,
        size.height * 0.2035449,
        size.width * 0.09715500,
        size.height * 0.2158760,
        size.width * 0.09993000,
        size.height * 0.2168593)
    ..cubicTo(
        size.width * 0.1002725,
        size.height * 0.2169785,
        size.width * 0.1023550,
        size.height * 0.2188805,
        size.width * 0.1045550,
        size.height * 0.2210805)
    ..lineTo(size.width * 0.1085600, size.height * 0.2250833)
    ..lineTo(size.width * 0.1047675, size.height * 0.2256296)
    ..cubicTo(
        size.width * 0.1026825,
        size.height * 0.2259276,
        size.width * 0.09456750,
        size.height * 0.2262007,
        size.width * 0.08673500,
        size.height * 0.2262355)
    ..lineTo(size.width * 0.07249250, size.height * 0.2263000)
    ..lineTo(size.width * 0.07419250, size.height * 0.2241497)
    ..cubicTo(
        size.width * 0.07839500,
        size.height * 0.2188358,
        size.width * 0.08774250,
        size.height * 0.2119924,
        size.width * 0.09079500,
        size.height * 0.2119924)
    ..cubicTo(
        size.width * 0.09653250,
        size.height * 0.2119924,
        size.width * 0.09331500,
        size.height * 0.2030383,
        size.width * 0.08587000,
        size.height * 0.1982857)
    ..cubicTo(
        size.width * 0.06704250,
        size.height * 0.1862625,
        size.width * 0.04789250,
        size.height * 0.1851252,
        size.width * 0.03178250,
        size.height * 0.1950825)
    ..cubicTo(
        size.width * 0.02829750,
        size.height * 0.1972378,
        size.width * 0.02818250,
        size.height * 0.1955989,
        size.width * 0.03273000,
        size.height * 0.2085011)
    ..lineTo(size.width * 0.03655000, size.height * 0.2193374)
    ..lineTo(size.width * 0.04070250, size.height * 0.2198837)
    ..cubicTo(
        size.width * 0.04298750,
        size.height * 0.2201817,
        size.width * 0.04771500,
        size.height * 0.2204300,
        size.width * 0.05121250,
        size.height * 0.2204349)
    ..cubicTo(
        size.width * 0.05595000,
        size.height * 0.2204399,
        size.width * 0.05744500,
        size.height * 0.2206833,
        size.width * 0.05708500,
        size.height * 0.2213984)
    ..cubicTo(
        size.width * 0.05641000,
        size.height * 0.2227393,
        size.width * 0.05072750,
        size.height * 0.2259425,
        size.width * 0.04580500,
        size.height * 0.2277551)
    ..cubicTo(
        size.width * 0.04287250,
        size.height * 0.2288328,
        size.width * 0.04171250,
        size.height * 0.2295926,
        size.width * 0.04185750,
        size.height * 0.2303425)
    ..cubicTo(
        size.width * 0.04197250,
        size.height * 0.2309385,
        size.width * 0.04165250,
        size.height * 0.2315791,
        size.width * 0.04112000,
        size.height * 0.2318075)
    ..cubicTo(
        size.width * 0.04060250,
        size.height * 0.2320310,
        size.width * 0.03214250,
        size.height * 0.2352740,
        size.width * 0.02232250,
        size.height * 0.2390135)
    ..cubicTo(
        size.width * 0.01250000,
        size.height * 0.2427580,
        size.width * 0.003460000,
        size.height * 0.2462145,
        size.width * 0.002232500,
        size.height * 0.2467012)
    ..cubicTo(
        size.width * 0.0002750000,
        size.height * 0.2474759,
        size.width * -4.274359e-17,
        size.height * 0.2478385,
        size.width * -4.274359e-17,
        size.height * 0.2496362)
    ..cubicTo(
        size.width * -4.274359e-17,
        size.height * 0.2522931,
        size.width * -0.001782500,
        size.height * 0.2527749,
        size.width * 0.02062000,
        size.height * 0.2440492)
    ..cubicTo(
        size.width * 0.03989000,
        size.height * 0.2365453,
        size.width * 0.05714500,
        size.height * 0.2322793,
        size.width * 0.05986250,
        size.height * 0.2343552)
    ..cubicTo(
        size.width * 0.06213750,
        size.height * 0.2360884,
        size.width * 0.05935750,
        size.height * 0.2670726,
        size.width * 0.05588000,
        size.height * 0.2787134)
    ..cubicTo(
        size.width * 0.05274500,
        size.height * 0.2892169,
        size.width * 0.05381250,
        size.height * 0.2883776,
        size.width * 0.04625500,
        size.height * 0.2862471)
    ..lineTo(size.width * 0.03970250, size.height * 0.2843947)
    ..lineTo(size.width * 0.02187000, size.height * 0.2875035)
    ..cubicTo(size.width * -0.001547500, size.height * 0.2915907, 0,
        size.height * 0.2911587, 0, size.height * 0.2936070)
    ..cubicTo(
        0,
        size.height * 0.2960653,
        size.width * -0.002282500,
        size.height * 0.2962242,
        size.width * 0.02003250,
        size.height * 0.2922314)
    ..cubicTo(
        size.width * 0.03041250,
        size.height * 0.2903690,
        size.width * 0.03923750,
        size.height * 0.2888494,
        size.width * 0.03964750,
        size.height * 0.2888494)
    ..cubicTo(
        size.width * 0.04029000,
        size.height * 0.2888494,
        size.width * 0.04039000,
        size.height * 0.2897234,
        size.width * 0.04039000,
        size.height * 0.2953253)
    ..cubicTo(
        size.width * 0.04039000,
        size.height * 0.3017714,
        size.width * 0.04103750,
        size.height * 0.3113364,
        size.width * 0.04159000,
        size.height * 0.3130745)
    ..cubicTo(
        size.width * 0.04179750,
        size.height * 0.3137301,
        size.width * 0.04796000,
        size.height * 0.3135612,
        size.width * 0.06313750,
        size.height * 0.3124885)
    ..cubicTo(
        size.width * 0.07483000,
        size.height * 0.3116592,
        size.width * 0.08445500,
        size.height * 0.3110930,
        size.width * 0.08452500,
        size.height * 0.3112221)
    ..cubicTo(
        size.width * 0.08469250,
        size.height * 0.3115350,
        size.width * 0.05635500,
        size.height * 0.3975944,
        size.width * 0.05518000,
        size.height * 0.4003357)
    ..arcToPoint(Offset(size.width * 0.05109250, size.height * 0.4092054),
        radius:
            Radius.elliptical(size.width * 0.2067600, size.height * 0.4107250),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..cubicTo(
        size.width * 0.02500000,
        size.height * 0.4645835,
        size.width * 0.02059000,
        size.height * 0.5277934,
        size.width * 0.04130750,
        size.height * 0.5494162)
    ..cubicTo(
        size.width * 0.04290750,
        size.height * 0.5510898,
        size.width * 0.04421750,
        size.height * 0.5526443,
        size.width * 0.04421750,
        size.height * 0.5528777)
    ..cubicTo(
        size.width * 0.04421750,
        size.height * 0.5531061,
        size.width * 0.03426750,
        size.height * 0.5736612,
        size.width * 0.02211000,
        size.height * 0.5985469)
    ..cubicTo(size.width * 0.001405000, size.height * 0.6409235, 0,
        size.height * 0.6439877, 0, size.height * 0.6467787)
    ..cubicTo(
        0,
        size.height * 0.6505729,
        size.width * -0.002540000,
        size.height * 0.6554149,
        size.width * 0.02396000,
        size.height * 0.6010946)
    ..cubicTo(
        size.width * 0.04163250,
        size.height * 0.5648661,
        size.width * 0.04630000,
        size.height * 0.5557382,
        size.width * 0.04698000,
        size.height * 0.5560709)
    ..cubicTo(
        size.width * 0.05560750,
        size.height * 0.5603071,
        size.width * 0.09778500,
        size.height * 0.5833801,
        size.width * 0.09778750,
        size.height * 0.5838668)
    ..cubicTo(
        size.width * 0.09779250,
        size.height * 0.5857639,
        size.width * 0.02629500,
        size.height * 0.7506816,
        size.width * 0.02397000,
        size.height * 0.7541282)
    ..cubicTo(
        size.width * 0.01859000,
        size.height * 0.7621138,
        size.width * 0.01320750,
        size.height * 0.7656448,
        size.width * 0.004995000,
        size.height * 0.7665784)
    ..lineTo(0, size.height * 0.7671446)
    ..lineTo(0, size.height * 0.7715546)
    ..lineTo(size.width * 0.005050000, size.height * 0.7709537)
    ..cubicTo(
        size.width * 0.01493750,
        size.height * 0.7697767,
        size.width * 0.02138000,
        size.height * 0.7649992,
        size.width * 0.02723500,
        size.height * 0.7545006)
    ..cubicTo(
        size.width * 0.02822750,
        size.height * 0.7527227,
        size.width * 0.04508250,
        size.height * 0.7139466,
        size.width * 0.06469000,
        size.height * 0.6683370)
    ..cubicTo(
        size.width * 0.08429500,
        size.height * 0.6227224,
        size.width * 0.1005175,
        size.height * 0.5851828,
        size.width * 0.1007375,
        size.height * 0.5849147)
    ..cubicTo(
        size.width * 0.1010425,
        size.height * 0.5845372,
        size.width * 0.1461550,
        size.height * 0.6093087,
        size.width * 0.1465300,
        size.height * 0.6100536)
    ..cubicTo(
        size.width * 0.1465750,
        size.height * 0.6101430,
        size.width * 0.1410975,
        size.height * 0.6402431,
        size.width * 0.1343575,
        size.height * 0.6769434)
    ..lineTo(size.width * 0.1221025, size.height * 0.7436644)
    ..lineTo(size.width * 0.1256750, size.height * 0.7472599)
    ..cubicTo(
        size.width * 0.1286575,
        size.height * 0.7502645,
        size.width * 0.1352700,
        size.height * 0.7559110,
        size.width * 0.1358025,
        size.height * 0.7559110)
    ..cubicTo(
        size.width * 0.1358875,
        size.height * 0.7559110,
        size.width * 0.1360450,
        size.height * 0.7549923,
        size.width * 0.1361525,
        size.height * 0.7538649)
    ..cubicTo(
        size.width * 0.1363350,
        size.height * 0.7519679,
        size.width * 0.1360050,
        size.height * 0.7515358,
        size.width * 0.1315175,
        size.height * 0.7478459)
    ..cubicTo(
        size.width * 0.1262300,
        size.height * 0.7434955,
        size.width * 0.1259650,
        size.height * 0.7425768,
        size.width * 0.1294075,
        size.height * 0.7405108)
    ..cubicTo(
        size.width * 0.1323150,
        size.height * 0.7387627,
        size.width * 0.1481950,
        size.height * 0.7390855,
        size.width * 0.1525375,
        size.height * 0.7409826)
    ..cubicTo(
        size.width * 0.1626075,
        size.height * 0.7453727,
        size.width * 0.1787750,
        size.height * 0.7609766,
        size.width * 0.1732575,
        size.height * 0.7609766)
    ..cubicTo(
        size.width * 0.1726725,
        size.height * 0.7609766,
        size.width * 0.1721950,
        size.height * 0.7613292,
        size.width * 0.1721950,
        size.height * 0.7617513)
    ..cubicTo(
        size.width * 0.1721950,
        size.height * 0.7621784,
        size.width * 0.1720650,
        size.height * 0.7631915,
        size.width * 0.1719100,
        size.height * 0.7640010)
    ..cubicTo(
        size.width * 0.1716575,
        size.height * 0.7653021,
        size.width * 0.1764850,
        size.height * 0.7654014,
        size.width * 0.1771375,
        size.height * 0.7641102)
    ..cubicTo(
        size.width * 0.1772050,
        size.height * 0.7639761,
        size.width * 0.1854225,
        size.height * 0.7226921,
        size.width * 0.1954000,
        size.height * 0.6723646)
    ..lineTo(size.width * 0.2135425, size.height * 0.5808622)
    ..lineTo(size.width * 0.2133300, size.height * 0.5755782)
    ..cubicTo(
        size.width * 0.2126650,
        size.height * 0.5591550,
        size.width * 0.2013375,
        size.height * 0.5403380,
        size.width * 0.1828225,
        size.height * 0.5249130)
    ..cubicTo(
        size.width * 0.1741700,
        size.height * 0.5177020,
        size.width * 0.1220625,
        size.height * 0.4676278,
        size.width * 0.1220425,
        size.height * 0.4665005)
    ..cubicTo(
        size.width * 0.1219850,
        size.height * 0.4634413,
        size.width * 0.1315125,
        size.height * 0.4251568,
        size.width * 0.1361025,
        size.height * 0.4099950)
    ..cubicTo(
        size.width * 0.1412525,
        size.height * 0.3929907,
        size.width * 0.1478275,
        size.height * 0.3767711,
        size.width * 0.1487350,
        size.height * 0.3788420)
    ..cubicTo(
        size.width * 0.1489275,
        size.height * 0.3792840,
        size.width * 0.1510650,
        size.height * 0.3880046,
        size.width * 0.1534825,
        size.height * 0.3982251)
    ..cubicTo(
        size.width * 0.1559000,
        size.height * 0.4084455,
        size.width * 0.1589950,
        size.height * 0.4215066,
        size.width * 0.1603600,
        size.height * 0.4272525)
    ..lineTo(size.width * 0.1628425, size.height * 0.4376965)
    ..lineTo(size.width * 0.1643650, size.height * 0.4353872)
    ..cubicTo(
        size.width * 0.1662475,
        size.height * 0.4325366,
        size.width * 0.1656675,
        size.height * 0.4310964,
        size.width * 0.1713500,
        size.height * 0.4526994)
    ..cubicTo(
        size.width * 0.1789475,
        size.height * 0.4815828,
        size.width * 0.1814300,
        size.height * 0.4861021,
        size.width * 0.1896250,
        size.height * 0.4859680)
    ..cubicTo(
        size.width * 0.1948825,
        size.height * 0.4858836,
        size.width * 0.1937050,
        size.height * 0.4872393,
        size.width * 0.2309075,
        size.height * 0.4383024)
    ..lineTo(size.width * 0.2699825, size.height * 0.3869071)
    ..cubicTo(
        size.width * 0.2795350,
        size.height * 0.3743674,
        size.width * 0.2874100,
        size.height * 0.3546019,
        size.width * 0.2857450,
        size.height * 0.3473413)
    ..cubicTo(
        size.width * 0.2853800,
        size.height * 0.3457621,
        size.width * 0.2854400,
        size.height * 0.3444063,
        size.width * 0.2860025,
        size.height * 0.3412528)
    ..cubicTo(
        size.width * 0.2867850,
        size.height * 0.3368825,
        size.width * 0.2861400,
        size.height * 0.3327705,
        size.width * 0.2846750,
        size.height * 0.3327705)
    ..cubicTo(
        size.width * 0.2842275,
        size.height * 0.3327705,
        size.width * 0.2840925,
        size.height * 0.3315836,
        size.width * 0.2841350,
        size.height * 0.3280228)
    ..cubicTo(
        size.width * 0.2841925,
        size.height * 0.3231559,
        size.width * 0.2832275,
        size.height * 0.3201017,
        size.width * 0.2816350,
        size.height * 0.3201017)
    ..cubicTo(
        size.width * 0.2813150,
        size.height * 0.3201017,
        size.width * 0.2809325,
        size.height * 0.3191383,
        size.width * 0.2807850,
        size.height * 0.3179662)
    ..cubicTo(
        size.width * 0.2797975,
        size.height * 0.3101544,
        size.width * 0.2768525,
        size.height * 0.3124488,
        size.width * 0.2689200,
        size.height * 0.3272183)
    ..cubicTo(
        size.width * 0.2610075,
        size.height * 0.3419480,
        size.width * 0.2616475,
        size.height * 0.3411634,
        size.width * 0.2616100,
        size.height * 0.3361475)
    ..cubicTo(
        size.width * 0.2615800,
        size.height * 0.3321497,
        size.width * 0.2597275,
        size.height * 0.3237072,
        size.width * 0.2587800,
        size.height * 0.3232354)
    ..cubicTo(
        size.width * 0.2585825,
        size.height * 0.3231361,
        size.width * 0.2561900,
        size.height * 0.3309876,
        size.width * 0.2534650,
        size.height * 0.3406866)
    ..lineTo(size.width * 0.2485125, size.height * 0.3583117)
    ..lineTo(size.width * 0.2255525, size.height * 0.3847915)
    ..cubicTo(
        size.width * 0.2129250,
        size.height * 0.3993524,
        size.width * 0.2024725,
        size.height * 0.4112763,
        size.width * 0.2023250,
        size.height * 0.4112912)
    ..cubicTo(
        size.width * 0.2015100,
        size.height * 0.4113657,
        size.width * 0.1964800,
        size.height * 0.3917045,
        size.width * 0.1969400,
        size.height * 0.3902444)
    ..cubicTo(
        size.width * 0.1973450,
        size.height * 0.3889532,
        size.width * 0.1951275,
        size.height * 0.3804312,
        size.width * 0.1826825,
        size.height * 0.3354870)
    ..cubicTo(
        size.width * 0.1745750,
        size.height * 0.3062112,
        size.width * 0.1679550,
        size.height * 0.2817477,
        size.width * 0.1679700,
        size.height * 0.2811220)
    ..cubicTo(
        size.width * 0.1679850,
        size.height * 0.2804962,
        size.width * 0.1698575,
        size.height * 0.2735237,
        size.width * 0.1721300,
        size.height * 0.2656224)
    ..lineTo(size.width * 0.1762650, size.height * 0.2512651)
    ..lineTo(size.width * 0.1800750, size.height * 0.2507288)
    ..cubicTo(
        size.width * 0.1955025,
        size.height * 0.2485437,
        size.width * 0.1984700,
        size.height * 0.2210805,
        size.width * 0.1881475,
        size.height * 0.1760768)
    ..cubicTo(
        size.width * 0.1873225,
        size.height * 0.1724862,
        size.width * 0.1866500,
        size.height * 0.1694469,
        size.width * 0.1866500,
        size.height * 0.1693178)
    ..cubicTo(
        size.width * 0.1866500,
        size.height * 0.1691887,
        size.width * 0.1877850,
        size.height * 0.1688212,
        size.width * 0.1891750,
        size.height * 0.1685033)
    ..cubicTo(
        size.width * 0.1961450,
        size.height * 0.1668943,
        size.width * 0.2043775,
        size.height * 0.1508236,
        size.width * 0.2046575,
        size.height * 0.1382691)
    ..cubicTo(
        size.width * 0.2047625,
        size.height * 0.1335909,
        size.width * 0.2036650,
        size.height * 0.1351851,
        size.width * 0.2024250,
        size.height * 0.1415170)
    ..cubicTo(
        size.width * 0.2012200,
        size.height * 0.1476800,
        size.width * 0.1993275,
        size.height * 0.1537785,
        size.width * 0.1977450,
        size.height * 0.1566093)
    ..cubicTo(
        size.width * 0.1968425,
        size.height * 0.1582233,
        size.width * 0.1968425,
        size.height * 0.1582084,
        size.width * 0.1978025,
        size.height * 0.1540715)
    ..cubicTo(
        size.width * 0.1992050,
        size.height * 0.1480227,
        size.width * 0.1991750,
        size.height * 0.1339485,
        size.width * 0.1977350,
        size.height * 0.1251831)
    ..cubicTo(
        size.width * 0.1964950,
        size.height * 0.1176246,
        size.width * 0.1963850,
        size.height * 0.1174805,
        size.width * 0.1949750,
        size.height * 0.1215230)
    ..moveTo(size.width * 0.7712575, size.height * 0.1278897)
    ..cubicTo(
        size.width * 0.7476175,
        size.height * 0.1377079,
        size.width * 0.7466400,
        size.height * 0.2263398,
        size.width * 0.7700150,
        size.height * 0.2407914)
    ..cubicTo(
        size.width * 0.7726650,
        size.height * 0.2424303,
        size.width * 0.7857300,
        size.height * 0.2426686,
        size.width * 0.7881600,
        size.height * 0.2411192)
    ..lineTo(size.width * 0.7891150, size.height * 0.2405133)
    ..lineTo(size.width * 0.7891200, size.height * 0.3075422)
    ..cubicTo(
        size.width * 0.7891275,
        size.height * 0.4210349,
        size.width * 0.7906475,
        size.height * 0.4515919,
        size.width * 0.7985850,
        size.height * 0.4978174)
    ..lineTo(size.width * 0.7995875, size.height * 0.5036626)
    ..lineTo(size.width * 0.7979575, size.height * 0.5111715)
    ..cubicTo(
        size.width * 0.7907100,
        size.height * 0.5445692,
        size.width * 0.7924325,
        size.height * 0.6033244,
        size.width * 0.8035950,
        size.height * 0.7034679)
    ..cubicTo(
        size.width * 0.8047550,
        size.height * 0.7138771,
        size.width * 0.8056425,
        size.height * 0.7225183,
        size.width * 0.8055650,
        size.height * 0.7226722)
    ..cubicTo(
        size.width * 0.8052575,
        size.height * 0.7232781,
        size.width * 0.7957675,
        size.height * 0.7300321,
        size.width * 0.7901775,
        size.height * 0.7336128)
    ..cubicTo(
        size.width * 0.7772900,
        size.height * 0.7418815,
        size.width * 0.7724275,
        size.height * 0.7437935,
        size.width * 0.7580775,
        size.height * 0.7462518)
    ..cubicTo(
        size.width * 0.7163200,
        size.height * 0.7533932,
        size.width * 0.7046050,
        size.height * 0.7638669,
        size.width * 0.7047600,
        size.height * 0.7939075)
    ..cubicTo(
        size.width * 0.7048075,
        size.height * 0.8027622,
        size.width * 0.7047250,
        size.height * 0.8022308,
        size.width * 0.7102050,
        size.height * 0.8289043)
    ..cubicTo(
        size.width * 0.7129900,
        size.height * 0.8424571,
        size.width * 0.7131275,
        size.height * 0.8437384,
        size.width * 0.7118150,
        size.height * 0.8437632)
    ..cubicTo(
        size.width * 0.7114200,
        size.height * 0.8437682,
        size.width * 0.7094700,
        size.height * 0.8450941,
        size.width * 0.7074825,
        size.height * 0.8467032)
    ..cubicTo(
        size.width * 0.7051350,
        size.height * 0.8486052,
        size.width * 0.7031625,
        size.height * 0.8496382,
        size.width * 0.7018500,
        size.height * 0.8496481)
    ..lineTo(size.width * 0.6998300, size.height * 0.8496581)
    ..lineTo(size.width * 0.6998300, size.height * 0.8728850)
    ..cubicTo(
        size.width * 0.6998300,
        size.height * 0.8944284,
        size.width * 0.6998850,
        size.height * 0.8961169,
        size.width * 0.7005750,
        size.height * 0.8961517)
    ..cubicTo(
        size.width * 0.7030550,
        size.height * 0.8962709,
        size.width * 0.7232950,
        size.height * 0.9018579,
        size.width * 0.7236025,
        size.height * 0.9025035)
    ..cubicTo(
        size.width * 0.7244450,
        size.height * 0.9042764,
        size.width * 0.7309775,
        size.height * 0.9471993,
        size.width * 0.7331700,
        size.height * 0.9653707)
    ..cubicTo(
        size.width * 0.7354350,
        size.height * 0.9841330,
        size.width * 0.7359100,
        size.height * 0.9932907,
        size.width * 0.7348900,
        size.height * 0.9985201)
    ..cubicTo(
        size.width * 0.7346550,
        size.height * 0.9997269,
        size.width * 0.7348375,
        size.height * 1.000000,
        size.width * 0.7358975,
        size.height * 1.000000)
    ..cubicTo(
        size.width * 0.7396850,
        size.height * 1.000000,
        size.width * 0.7369700,
        size.height * 0.9691450,
        size.width * 0.7287550,
        size.height * 0.9187926)
    ..cubicTo(
        size.width * 0.7273700,
        size.height * 0.9102954,
        size.width * 0.7263225,
        size.height * 0.9031689,
        size.width * 0.7264300,
        size.height * 0.9029604)
    ..cubicTo(
        size.width * 0.7265350,
        size.height * 0.9027468,
        size.width * 0.7315475,
        size.height * 0.9037847,
        size.width * 0.7375675,
        size.height * 0.9052696)
    ..cubicTo(
        size.width * 0.7435875,
        size.height * 0.9067496,
        size.width * 0.7486650,
        size.height * 0.9079564,
        size.width * 0.7488525,
        size.height * 0.9079514)
    ..cubicTo(
        size.width * 0.7496100,
        size.height * 0.9079216,
        size.width * 0.7489950,
        size.height * 0.9934049,
        size.width * 0.7482150,
        size.height * 0.9968316)
    ..lineTo(size.width * 0.7474925, size.height * 1.000000)
    ..lineTo(size.width * 0.7512750, size.height * 1.000000)
    ..lineTo(size.width * 0.7512750, size.height * 0.9085622)
    ..lineTo(size.width * 0.7528700, size.height * 0.9090837)
    ..cubicTo(
        size.width * 0.7537475,
        size.height * 0.9093717,
        size.width * 0.7548000,
        size.height * 0.9096101,
        size.width * 0.7552075,
        size.height * 0.9096151)
    ..cubicTo(
        size.width * 0.7559075,
        size.height * 0.9096250,
        size.width * 0.7559525,
        size.height * 0.9123465,
        size.width * 0.7559525,
        size.height * 0.9548125)
    ..lineTo(size.width * 0.7559525, size.height)
    ..lineTo(size.width * 0.7585025, size.height)
    ..lineTo(size.width * 0.7585025, size.height * 0.9102805)
    ..lineTo(size.width * 0.7598850, size.height * 0.9107474)
    ..cubicTo(
        size.width * 0.7606450,
        size.height * 0.9110006,
        size.width * 0.7645200,
        size.height * 0.9120187,
        size.width * 0.7684950,
        size.height * 0.9130070)
    ..lineTo(size.width * 0.7757225, size.height * 0.9148048)
    ..lineTo(size.width * 0.7928000, size.height * 0.9105239)
    ..cubicTo(
        size.width * 0.8021925,
        size.height * 0.9081749,
        size.width * 0.8102075,
        size.height * 0.9062480,
        size.width * 0.8106125,
        size.height * 0.9062480)
    ..cubicTo(
        size.width * 0.8116750,
        size.height * 0.9062480,
        size.width * 0.8106875,
        size.height * 0.9842124,
        size.width * 0.8096100,
        size.height * 0.9855334)
    ..cubicTo(
        size.width * 0.8093550,
        size.height * 0.9858463,
        size.width * 0.7993300,
        size.height * 0.9891339,
        size.width * 0.7873325,
        size.height * 0.9928387)
    ..lineTo(size.width * 0.7655175, size.height * 0.9995779)
    ..lineTo(size.width * 0.7719200, size.height * 0.9998063)
    ..cubicTo(
        size.width * 0.7779050,
        size.height * 1.000025,
        size.width * 0.7792900,
        size.height * 0.9997368,
        size.width * 0.7932625,
        size.height * 0.9953715)
    ..cubicTo(
        size.width * 0.8109875,
        size.height * 0.9898391,
        size.width * 0.8099500,
        size.height * 0.9899732,
        size.width * 0.8099500,
        size.height * 0.9932013)
    ..cubicTo(
        size.width * 0.8099500,
        size.height * 0.9960072,
        size.width * 0.8106775,
        size.height * 0.9955254,
        size.width * 0.8022950,
        size.height * 0.9982271)
    ..lineTo(size.width * 0.7974075, size.height * 0.9998014)
    ..lineTo(size.width * 0.8047400, size.height * 0.9999007)
    ..lineTo(size.width * 0.8120750, size.height * 1.000000)
    ..lineTo(size.width * 0.8120750, size.height * 0.9959873)
    ..cubicTo(
        size.width * 0.8120750,
        size.height * 0.9937773,
        size.width * 0.8122625,
        size.height * 0.9841826,
        size.width * 0.8124925,
        size.height * 0.9746624)
    ..cubicTo(
        size.width * 0.8127200,
        size.height * 0.9651372,
        size.width * 0.8132000,
        size.height * 0.9354940,
        size.width * 0.8135600,
        size.height * 0.9087807)
    ..cubicTo(
        size.width * 0.8151050,
        size.height * 0.7932718,
        size.width * 0.8155050,
        size.height * 0.7874315,
        size.width * 0.8220875,
        size.height * 0.7837714)
    ..cubicTo(
        size.width * 0.8287875,
        size.height * 0.7800418,
        size.width * 0.8353800,
        size.height * 0.7886532,
        size.width * 0.8398875,
        size.height * 0.8070083)
    ..cubicTo(
        size.width * 0.8515925,
        size.height * 0.8546739,
        size.width * 0.8498425,
        size.height * 0.9036358,
        size.width * 0.8335150,
        size.height * 0.9853944)
    ..cubicTo(
        size.width * 0.8303450,
        size.height * 1.001271,
        size.width * 0.8304900,
        size.height * 1.000000,
        size.width * 0.8318600,
        size.height * 1.000000)
    ..cubicTo(
        size.width * 0.8327250,
        size.height * 1.000000,
        size.width * 0.8330925,
        size.height * 0.9992849,
        size.width * 0.8337150,
        size.height * 0.9963995)
    ..lineTo(size.width * 0.8344925, size.height * 0.9927990)
    ..lineTo(size.width * 0.8946050, size.height * 0.9747667)
    ..cubicTo(
        size.width * 0.9276675,
        size.height * 0.9648492,
        size.width * 0.9552450,
        size.height * 0.9565407,
        size.width * 0.9558875,
        size.height * 0.9563024)
    ..lineTo(size.width * 0.9570575, size.height * 0.9558653)
    ..lineTo(size.width * 0.9570575, size.height * 0.9775081)
    ..cubicTo(
        size.width * 0.9570575,
        size.height * 0.9900924,
        size.width * 0.9572225,
        size.height * 0.9991508,
        size.width * 0.9574475,
        size.height * 0.9991508)
    ..cubicTo(
        size.width * 0.9591725,
        size.height * 0.9991508,
        size.width * 0.9591825,
        size.height * 0.9990217,
        size.width * 0.9591825,
        size.height * 0.9764453)
    ..cubicTo(
        size.width * 0.9591825,
        size.height * 0.9579412,
        size.width * 0.9592825,
        size.height * 0.9544847,
        size.width * 0.9598650,
        size.height * 0.9528360)
    ..cubicTo(
        size.width * 0.9607375,
        size.height * 0.9503578,
        size.width * 0.9607375,
        size.height * 0.9454214,
        size.width * 0.9598650,
        size.height * 0.9435045)
    ..cubicTo(
        size.width * 0.9592625,
        size.height * 0.9421834,
        size.width * 0.9591825,
        size.height * 0.9377933,
        size.width * 0.9591825,
        size.height * 0.9056471)
    ..lineTo(size.width * 0.9591825, size.height * 0.8692845)
    ..lineTo(size.width * 0.9601400, size.height * 0.8687730)
    ..cubicTo(
        size.width * 0.9606675,
        size.height * 0.8684949,
        size.width * 0.9617200,
        size.height * 0.8682615,
        size.width * 0.9624775,
        size.height * 0.8682515)
    ..lineTo(size.width * 0.9638600, size.height * 0.8682416)
    ..lineTo(size.width * 0.9638600, size.height * 0.9315359)
    ..cubicTo(
        size.width * 0.9638600,
        size.height * 0.9663490,
        size.width * 0.9640175,
        size.height * 0.9950239,
        size.width * 0.9642125,
        size.height * 0.9952622)
    ..cubicTo(
        size.width * 0.9644050,
        size.height * 0.9955006,
        size.width * 0.9649800,
        size.height * 0.9954758,
        size.width * 0.9654875,
        size.height * 0.9952126)
    ..lineTo(size.width * 0.9664125, size.height * 0.9947309)
    ..lineTo(size.width * 0.9664125, size.height * 0.8674272)
    ..lineTo(size.width * 0.9673675, size.height * 0.8670150)
    ..cubicTo(
        size.width * 0.9682150,
        size.height * 0.8666524,
        size.width * 0.9991325,
        size.height * 0.8589498,
        size.width * 0.9997525,
        size.height * 0.8589498)
    ..cubicTo(
        size.width * 0.9998900,
        size.height * 0.8589498,
        size.width * 1.000000,
        size.height * 0.8580013,
        size.width * 1.000000,
        size.height * 0.8568392)
    ..cubicTo(
        size.width * 1.000000,
        size.height * 0.8541326,
        size.width * 1.009210,
        size.height * 0.8521263,
        size.width * 0.9243875,
        size.height * 0.8733071)
    ..cubicTo(
        size.width * 0.8394225,
        size.height * 0.8945277,
        size.width * 0.8489050,
        size.height * 0.8924320,
        size.width * 0.8492175,
        size.height * 0.8899439)
    ..cubicTo(
        size.width * 0.8493550,
        size.height * 0.8888712,
        size.width * 0.8495925,
        size.height * 0.8837908,
        size.width * 0.8497500,
        size.height * 0.8786557)
    ..lineTo(size.width * 0.8500350, size.height * 0.8693143)
    ..lineTo(size.width * 0.8545450, size.height * 0.8674272)
    ..cubicTo(
        size.width * 0.8677200,
        size.height * 0.8619047,
        size.width * 0.8733525,
        size.height * 0.8555778,
        size.width * 0.8761050,
        size.height * 0.8432219)
    ..cubicTo(
        size.width * 0.8766600,
        size.height * 0.8407239,
        size.width * 0.8773125,
        size.height * 0.8386828,
        size.width * 0.8775525,
        size.height * 0.8386828)
    ..cubicTo(
        size.width * 0.8777950,
        size.height * 0.8386828,
        size.width * 0.9052725,
        size.height * 0.8320280,
        size.width * 0.9386175,
        size.height * 0.8238984)
    ..cubicTo(
        size.width * 0.9719625,
        size.height * 0.8157687,
        size.width * 0.9994125,
        size.height * 0.8091189,
        size.width * 0.9996225,
        size.height * 0.8091189)
    ..cubicTo(
        size.width * 0.9998300,
        size.height * 0.8091189,
        size.width * 1.000000,
        size.height * 0.8082300,
        size.width * 1.000000,
        size.height * 0.8071424)
    ..cubicTo(
        size.width * 1.000000,
        size.height * 0.8040782,
        size.width * 0.9982925,
        size.height * 0.8035071,
        size.width * 0.9817175,
        size.height * 0.8010489)
    ..lineTo(size.width * 0.9666250, size.height * 0.7988091)
    ..lineTo(size.width * 0.9470675, size.height * 0.8006069)
    ..cubicTo(
        size.width * 0.9243500,
        size.height * 0.8026877,
        size.width * 0.8895725,
        size.height * 0.8062932,
        size.width * 0.8843950,
        size.height * 0.8070977)
    ..lineTo(size.width * 0.8808250, size.height * 0.8076539)
    ..lineTo(size.width * 0.8811150, size.height * 0.8047983)
    ..cubicTo(
        size.width * 0.8851350,
        size.height * 0.7652674,
        size.width * 0.8812950,
        size.height * 0.7262280,
        size.width * 0.8713250,
        size.height * 0.7052309)
    ..lineTo(size.width * 0.8685750, size.height * 0.6994403)
    ..lineTo(size.width * 0.8688550, size.height * 0.6801665)
    ..cubicTo(
        size.width * 0.8690625,
        size.height * 0.6657992,
        size.width * 0.8700425,
        size.height * 0.6421899,
        size.width * 0.8727075,
        size.height * 0.5874127)
    ..lineTo(size.width * 0.8762825, size.height * 0.5139327)
    ..lineTo(size.width * 0.8777500, size.height * 0.5062599)
    ..cubicTo(
        size.width * 0.8818125,
        size.height * 0.4850095,
        size.width * 0.8861475,
        size.height * 0.4390274,
        size.width * 0.8867425,
        size.height * 0.4108939)
    ..cubicTo(
        size.width * 0.8871875,
        size.height * 0.3898570,
        size.width * 0.8853750,
        size.height * 0.3027200,
        size.width * 0.8832400,
        size.height * 0.2426438)
    ..lineTo(size.width * 0.8829650, size.height * 0.2348618)
    ..lineTo(size.width * 0.8864500, size.height * 0.2384027)
    ..lineTo(size.width * 0.8899375, size.height * 0.2419386)
    ..lineTo(size.width * 0.8970300, size.height * 0.2421720)
    ..cubicTo(
        size.width * 0.9076575,
        size.height * 0.2425246,
        size.width * 0.9107825,
        size.height * 0.2405431,
        size.width * 0.9160775,
        size.height * 0.2300942)
    ..cubicTo(
        size.width * 0.9304675,
        size.height * 0.2017074,
        size.width * 0.9261200,
        size.height * 0.1423712,
        size.width * 0.9086850,
        size.height * 0.1291760)
    ..cubicTo(
        size.width * 0.9059375,
        size.height * 0.1270951,
        size.width * 0.8950100,
        size.height * 0.1261317,
        size.width * 0.8909450,
        size.height * 0.1276066)
    ..cubicTo(
        size.width * 0.8861650,
        size.height * 0.1293448,
        size.width * 0.8799825,
        size.height * 0.1383386,
        size.width * 0.8772875,
        size.height * 0.1474814)
    ..cubicTo(
        size.width * 0.8759475,
        size.height * 0.1520205,
        size.width * 0.8762850,
        size.height * 0.1521894,
        size.width * 0.8740125,
        size.height * 0.1458624)
    ..cubicTo(
        size.width * 0.8689525,
        size.height * 0.1317733,
        size.width * 0.8645425,
        size.height * 0.1274577,
        size.width * 0.8548050,
        size.height * 0.1270554)
    ..cubicTo(
        size.width * 0.8474800,
        size.height * 0.1267524,
        size.width * 0.8447825,
        size.height * 0.1275371,
        size.width * 0.8411775,
        size.height * 0.1310234)
    ..lineTo(size.width * 0.8386025, size.height * 0.1335115)
    ..lineTo(size.width * 0.8367100, size.height * 0.1314107)
    ..cubicTo(
        size.width * 0.8302825,
        size.height * 0.1242743,
        size.width * 0.8157050,
        size.height * 0.1252775,
        size.width * 0.8098775,
        size.height * 0.1332582)
    ..cubicTo(
        size.width * 0.8076625,
        size.height * 0.1362925,
        size.width * 0.8041200,
        size.height * 0.1436524,
        size.width * 0.8027775,
        size.height * 0.1480128)
    ..cubicTo(
        size.width * 0.8015825,
        size.height * 0.1518914,
        size.width * 0.8018900,
        size.height * 0.1520602,
        size.width * 0.7999675,
        size.height * 0.1465130)
    ..cubicTo(
        size.width * 0.7970475,
        size.height * 0.1380853,
        size.width * 0.7931975,
        size.height * 0.1320713,
        size.width * 0.7885200,
        size.height * 0.1286148)
    ..cubicTo(
        size.width * 0.7863025,
        size.height * 0.1269759,
        size.width * 0.7746350,
        size.height * 0.1264892,
        size.width * 0.7712575,
        size.height * 0.1278897)
    ..moveTo(size.width * 0.7884775, size.height * 0.1332830)
    ..cubicTo(
        size.width * 0.7948925,
        size.height * 0.1382244,
        size.width * 0.8008275,
        size.height * 0.1523681,
        size.width * 0.7997350,
        size.height * 0.1601104)
    ..lineTo(size.width * 0.7992125, size.height * 0.1638351)
    ..lineTo(size.width * 0.7974575, size.height * 0.1633534)
    ..arcToPoint(Offset(size.width * 0.7952100, size.height * 0.1625687),
        radius: Radius.elliptical(
            size.width * 0.02433000, size.height * 0.04833111),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..cubicTo(
        size.width * 0.7941925,
        size.height * 0.1619529,
        size.width * 0.7890200,
        size.height * 0.1639146,
        size.width * 0.7874475,
        size.height * 0.1655037)
    ..cubicTo(
        size.width * 0.7840825,
        size.height * 0.1689155,
        size.width * 0.7846800,
        size.height * 0.1742939,
        size.width * 0.7888025,
        size.height * 0.1776660)
    ..lineTo(size.width * 0.7904000, size.height * 0.1789771)
    ..lineTo(size.width * 0.7901800, size.height * 0.1847180)
    ..cubicTo(
        size.width * 0.7900600,
        size.height * 0.1878715,
        size.width * 0.7897625,
        size.height * 0.2005155,
        size.width * 0.7895175,
        size.height * 0.2128118)
    ..cubicTo(
        size.width * 0.7892075,
        size.height * 0.2284454,
        size.width * 0.7889200,
        size.height * 0.2353634,
        size.width * 0.7885625,
        size.height * 0.2358103)
    ..cubicTo(
        size.width * 0.7821550,
        size.height * 0.2438009,
        size.width * 0.7706950,
        size.height * 0.2324234,
        size.width * 0.7661725,
        size.height * 0.2135816)
    ..cubicTo(
        size.width * 0.7562400,
        size.height * 0.1721982,
        size.width * 0.7708300,
        size.height * 0.1196855,
        size.width * 0.7884775,
        size.height * 0.1332830)
    ..moveTo(size.width * 0.8305850, size.height * 0.1318329)
    ..cubicTo(
        size.width * 0.8326225,
        size.height * 0.1325828,
        size.width * 0.8358975,
        size.height * 0.1350758,
        size.width * 0.8366850,
        size.height * 0.1364763)
    ..cubicTo(
        size.width * 0.8367750,
        size.height * 0.1366352,
        size.width * 0.8358575,
        size.height * 0.1389147,
        size.width * 0.8346500,
        size.height * 0.1415418)
    ..cubicTo(
        size.width * 0.8321100,
        size.height * 0.1470642,
        size.width * 0.8290750,
        size.height * 0.1589186,
        size.width * 0.8280500,
        size.height * 0.1673363)
    ..cubicTo(
        size.width * 0.8276750,
        size.height * 0.1704203,
        size.width * 0.8270350,
        size.height * 0.1742194,
        size.width * 0.8266300,
        size.height * 0.1757838)
    ..cubicTo(
        size.width * 0.8256125,
        size.height * 0.1797170,
        size.width * 0.8256375,
        size.height * 0.1902603,
        size.width * 0.8266725,
        size.height * 0.1940942)
    ..cubicTo(
        size.width * 0.8270875,
        size.height * 0.1956287,
        size.width * 0.8278025,
        size.height * 0.1997904,
        size.width * 0.8282575,
        size.height * 0.2033363)
    ..arcToPoint(Offset(size.width * 0.8344200, size.height * 0.2274919),
        radius: Radius.elliptical(
            size.width * 0.03231250, size.height * 0.06418820),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..lineTo(size.width * 0.8367350, size.height * 0.2330640)
    ..lineTo(size.width * 0.8355650, size.height * 0.2342609)
    ..cubicTo(
        size.width * 0.8278125,
        size.height * 0.2421969,
        size.width * 0.8189625,
        size.height * 0.2369923,
        size.width * 0.8133850,
        size.height * 0.2212196)
    ..cubicTo(
        size.width * 0.8014100,
        size.height * 0.1873451,
        size.width * 0.8097225,
        size.height * 0.1346586,
        size.width * 0.8276225,
        size.height * 0.1310035)
    ..cubicTo(
        size.width * 0.8278700,
        size.height * 0.1309539,
        size.width * 0.8292050,
        size.height * 0.1313263,
        size.width * 0.8305850,
        size.height * 0.1318329)
    ..moveTo(size.width * 0.8599350, size.height * 0.1318279)
    ..cubicTo(
        size.width * 0.8649850,
        size.height * 0.1336704,
        size.width * 0.8701150,
        size.height * 0.1412687,
        size.width * 0.8732525,
        size.height * 0.1515636)
    ..cubicTo(
        size.width * 0.8754925,
        size.height * 0.1589186,
        size.width * 0.8749600,
        size.height * 0.1613470,
        size.width * 0.8708125,
        size.height * 0.1627276)
    ..cubicTo(
        size.width * 0.8668725,
        size.height * 0.1640387,
        size.width * 0.8652150,
        size.height * 0.1651362,
        size.width * 0.8633175,
        size.height * 0.1676938)
    ..lineTo(size.width * 0.8617950, size.height * 0.1697449)
    ..lineTo(size.width * 0.8613800, size.height * 0.1675796)
    ..cubicTo(
        size.width * 0.8584925,
        size.height * 0.1524824,
        size.width * 0.8547825,
        size.height * 0.1529541,
        size.width * 0.8552150,
        size.height * 0.1683643)
    ..cubicTo(
        size.width * 0.8560875,
        size.height * 0.1993683,
        size.width * 0.8564300,
        size.height * 0.2014392,
        size.width * 0.8625500,
        size.height * 0.2128168)
    ..lineTo(size.width * 0.8661050, size.height * 0.2194218)
    ..lineTo(size.width * 0.8653950, size.height * 0.2271095)
    ..lineTo(size.width * 0.8646875, size.height * 0.2347972)
    ..lineTo(size.width * 0.8619325, size.height * 0.2364857)
    ..cubicTo(
        size.width * 0.8443100,
        size.height * 0.2472971,
        size.width * 0.8306100,
        size.height * 0.1957032,
        size.width * 0.8404500,
        size.height * 0.1555862)
    ..cubicTo(
        size.width * 0.8433950,
        size.height * 0.1435780,
        size.width * 0.8491225,
        size.height * 0.1339634,
        size.width * 0.8545925,
        size.height * 0.1318378)
    ..cubicTo(
        size.width * 0.8572500,
        size.height * 0.1308098,
        size.width * 0.8571275,
        size.height * 0.1308098,
        size.width * 0.8599350,
        size.height * 0.1318279)
    ..moveTo(size.width * 0.9048100, size.height * 0.1317634)
    ..cubicTo(
        size.width * 0.9226200,
        size.height * 0.1383634,
        size.width * 0.9289225,
        size.height * 0.1972527,
        size.width * 0.9149325,
        size.height * 0.2263249)
    ..cubicTo(
        size.width * 0.9037025,
        size.height * 0.2496660,
        size.width * 0.8876550,
        size.height * 0.2365105,
        size.width * 0.8830275,
        size.height * 0.2001728)
    ..cubicTo(
        size.width * 0.8785275,
        size.height * 0.1648085,
        size.width * 0.8907125,
        size.height * 0.1265339,
        size.width * 0.9048100,
        size.height * 0.1317634)
    ..moveTo(size.width * 0.7721100, size.height * 0.1340329)
    ..cubicTo(
        size.width * 0.7565675,
        size.height * 0.1576224,
        size.width * 0.7569175,
        size.height * 0.2133531,
        size.width * 0.7727475,
        size.height * 0.2362573)
    ..lineTo(size.width * 0.7740225, size.height * 0.2381047)
    ..lineTo(size.width * 0.7725350, size.height * 0.2376279)
    ..cubicTo(
        size.width * 0.7518200,
        size.height * 0.2310080,
        size.width * 0.7487050,
        size.height * 0.1534061,
        size.width * 0.7684025,
        size.height * 0.1346537)
    ..cubicTo(
        size.width * 0.7723000,
        size.height * 0.1309439,
        size.width * 0.7743725,
        size.height * 0.1305963,
        size.width * 0.7721100,
        size.height * 0.1340329)
    ..moveTo(size.width * 0.8173900, size.height * 0.1336108)
    ..cubicTo(
        size.width * 0.8015900,
        size.height * 0.1559239,
        size.width * 0.8018250,
        size.height * 0.2127324,
        size.width * 0.8178150,
        size.height * 0.2362672)
    ..lineTo(size.width * 0.8190900, size.height * 0.2381444)
    ..lineTo(size.width * 0.8176025, size.height * 0.2376677)
    ..cubicTo(
        size.width * 0.8024350,
        size.height * 0.2328058,
        size.width * 0.7947225,
        size.height * 0.1846485,
        size.width * 0.8039700,
        size.height * 0.1525370)
    ..cubicTo(
        size.width * 0.8071725,
        size.height * 0.1414276,
        size.width * 0.8134575,
        size.height * 0.1318726,
        size.width * 0.8176025,
        size.height * 0.1318229)
    ..lineTo(size.width * 0.8186650, size.height * 0.1318080)
    ..lineTo(size.width * 0.8173900, size.height * 0.1336108)
    ..moveTo(size.width * 0.8467250, size.height * 0.1336157)
    ..cubicTo(
        size.width * 0.8309125,
        size.height * 0.1560133,
        size.width * 0.8311475,
        size.height * 0.2127125,
        size.width * 0.8471525,
        size.height * 0.2362672)
    ..lineTo(size.width * 0.8484275, size.height * 0.2381444)
    ..lineTo(size.width * 0.8469400, size.height * 0.2376677)
    ..cubicTo(
        size.width * 0.8336375,
        size.height * 0.2334067,
        size.width * 0.8256325,
        size.height * 0.1955394,
        size.width * 0.8310950,
        size.height * 0.1627127)
    ..cubicTo(
        size.width * 0.8337650,
        size.height * 0.1466719,
        size.width * 0.8413325,
        size.height * 0.1319173,
        size.width * 0.8469400,
        size.height * 0.1318279)
    ..lineTo(size.width * 0.8480025, size.height * 0.1318080)
    ..lineTo(size.width * 0.8467250, size.height * 0.1336157)
    ..moveTo(size.width * 0.8907650, size.height * 0.1351602)
    ..cubicTo(
        size.width * 0.8757450,
        size.height * 0.1587447,
        size.width * 0.8766900,
        size.height * 0.2162882,
        size.width * 0.8924325,
        size.height * 0.2367291)
    ..lineTo(size.width * 0.8934950, size.height * 0.2381097)
    ..lineTo(size.width * 0.8920075, size.height * 0.2376478)
    ..cubicTo(
        size.width * 0.8726300,
        size.height * 0.2316784,
        size.width * 0.8677775,
        size.height * 0.1611931,
        size.width * 0.8851125,
        size.height * 0.1374993)
    ..cubicTo(
        size.width * 0.8894675,
        size.height * 0.1315498,
        size.width * 0.8943525,
        size.height * 0.1295286,
        size.width * 0.8907650,
        size.height * 0.1351602)
    ..moveTo(size.width * 0.8583750, size.height * 0.1657173)
    ..cubicTo(
        size.width * 0.8586975,
        size.height * 0.1667453,
        size.width * 0.8594700,
        size.height * 0.1705792,
        size.width * 0.8600900,
        size.height * 0.1742443)
    ..lineTo(size.width * 0.8612150, size.height * 0.1809089)
    ..lineTo(size.width * 0.8625575, size.height * 0.1766181)
    ..cubicTo(
        size.width * 0.8641950,
        size.height * 0.1713887,
        size.width * 0.8657750,
        size.height * 0.1693774,
        size.width * 0.8696175,
        size.height * 0.1676442)
    ..cubicTo(
        size.width * 0.8731775,
        size.height * 0.1660351,
        size.width * 0.8730800,
        size.height * 0.1658712,
        size.width * 0.8724975,
        size.height * 0.1725309)
    ..cubicTo(
        size.width * 0.8708800,
        size.height * 0.1909804,
        size.width * 0.8734600,
        size.height * 0.2127622,
        size.width * 0.8788625,
        size.height * 0.2262355)
    ..lineTo(size.width * 0.8806725, size.height * 0.2307547)
    ..lineTo(size.width * 0.8814250, size.height * 0.2545230)
    ..cubicTo(
        size.width * 0.8860050,
        size.height * 0.3992928,
        size.width * 0.8853975,
        size.height * 0.4405371,
        size.width * 0.8779375,
        size.height * 0.4909094)
    ..cubicTo(
        size.width * 0.8694875,
        size.height * 0.5479959,
        size.width * 0.8569675,
        size.height * 0.5432532,
        size.width * 0.8528700,
        size.height * 0.4814189)
    ..cubicTo(
        size.width * 0.8502925,
        size.height * 0.4425137,
        size.width * 0.8538575,
        size.height * 0.3803169,
        size.width * 0.8671025,
        size.height * 0.2331186)
    ..cubicTo(
        size.width * 0.8683200,
        size.height * 0.2195857,
        size.width * 0.8684800,
        size.height * 0.2165464,
        size.width * 0.8680050,
        size.height * 0.2160200)
    ..cubicTo(
        size.width * 0.8618425,
        size.height * 0.2091716,
        size.width * 0.8584775,
        size.height * 0.1984942,
        size.width * 0.8579625,
        size.height * 0.1841618)
    ..cubicTo(
        size.width * 0.8571800,
        size.height * 0.1623502,
        size.width * 0.8571875,
        size.height * 0.1619430,
        size.width * 0.8583750,
        size.height * 0.1657173)
    ..moveTo(size.width * 0.7979825, size.height * 0.1679819)
    ..cubicTo(
        size.width * 0.7989525,
        size.height * 0.1691986,
        size.width * 0.7981850,
        size.height * 0.1738817,
        size.width * 0.7969125,
        size.height * 0.1745174)
    ..cubicTo(
        size.width * 0.7937975,
        size.height * 0.1760718,
        size.width * 0.7865950,
        size.height * 0.1727544,
        size.width * 0.7878100,
        size.height * 0.1703259)
    ..cubicTo(
        size.width * 0.7891225,
        size.height * 0.1677038,
        size.width * 0.7964250,
        size.height * 0.1660202,
        size.width * 0.7979825,
        size.height * 0.1679819)
    ..moveTo(size.width * 0.1852000, size.height * 0.1741400)
    ..cubicTo(
        size.width * 0.1960700,
        size.height * 0.2167500,
        size.width * 0.1937025,
        size.height * 0.2455341,
        size.width * 0.1792675,
        size.height * 0.2462691)
    ..lineTo(size.width * 0.1750750, size.height * 0.2464827)
    ..lineTo(size.width * 0.1707450, size.height * 0.2617240)
    ..cubicTo(
        size.width * 0.1683650,
        size.height * 0.2701020,
        size.width * 0.1663050,
        size.height * 0.2771838,
        size.width * 0.1661675,
        size.height * 0.2774569)
    ..cubicTo(
        size.width * 0.1641650,
        size.height * 0.2814299,
        size.width * 0.1522100,
        size.height * 0.2536291,
        size.width * 0.1522100,
        size.height * 0.2450028)
    ..cubicTo(
        size.width * 0.1522100,
        size.height * 0.2427431,
        size.width * 0.1551000,
        size.height * 0.2366645,
        size.width * 0.1574700,
        size.height * 0.2339281)
    ..cubicTo(
        size.width * 0.1588700,
        size.height * 0.2323141,
        size.width * 0.1616250,
        size.height * 0.2279786,
        size.width * 0.1635925,
        size.height * 0.2242837)
    ..lineTo(size.width * 0.1671700, size.height * 0.2175744)
    ..lineTo(size.width * 0.1660550, size.height * 0.2159654)
    ..cubicTo(
        size.width * 0.1638550,
        size.height * 0.2127771,
        size.width * 0.1629350,
        size.height * 0.2061273,
        size.width * 0.1636950,
        size.height * 0.1988617)
    ..cubicTo(
        size.width * 0.1644750,
        size.height * 0.1913975,
        size.width * 0.1672800,
        size.height * 0.1888946,
        size.width * 0.1712100,
        size.height * 0.1921574)
    ..cubicTo(
        size.width * 0.1720575,
        size.height * 0.1928576,
        size.width * 0.1727825,
        size.height * 0.1933344,
        size.width * 0.1728225,
        size.height * 0.1932102)
    ..cubicTo(
        size.width * 0.1728625,
        size.height * 0.1930910,
        size.width * 0.1733375,
        size.height * 0.1905185,
        size.width * 0.1738775,
        size.height * 0.1874991)
    ..cubicTo(
        size.width * 0.1759675,
        size.height * 0.1758136,
        size.width * 0.1787275,
        size.height * 0.1707878,
        size.width * 0.1835600,
        size.height * 0.1698641)
    ..cubicTo(
        size.width * 0.1838475,
        size.height * 0.1698094,
        size.width * 0.1845875,
        size.height * 0.1717314,
        size.width * 0.1852000,
        size.height * 0.1741400)
    ..moveTo(size.width * 0.7977475, size.height * 0.1877126)
    ..cubicTo(
        size.width * 0.7978875,
        size.height * 0.1924702,
        size.width * 0.7984150,
        size.height * 0.1994080,
        size.width * 0.7989200,
        size.height * 0.2031227)
    ..cubicTo(
        size.width * 0.7994225,
        size.height * 0.2068424,
        size.width * 0.7999325,
        size.height * 0.2107359,
        size.width * 0.8000525,
        size.height * 0.2117838)
    ..cubicTo(
        size.width * 0.8004650,
        size.height * 0.2153893,
        size.width * 0.7938150,
        size.height * 0.2322644,
        size.width * 0.7919825,
        size.height * 0.2322644)
    ..cubicTo(
        size.width * 0.7915350,
        size.height * 0.2322644,
        size.width * 0.7918100,
        size.height * 0.1998649,
        size.width * 0.7923775,
        size.height * 0.1855970)
    ..lineTo(size.width * 0.7926400, size.height * 0.1790516)
    ..lineTo(size.width * 0.7974900, size.height * 0.1790516)
    ..lineTo(size.width * 0.7977475, size.height * 0.1877126)
    ..moveTo(size.width * 0.4544700, size.height * 0.1933592)
    ..cubicTo(
        size.width * 0.4515600,
        size.height * 0.1951520,
        size.width * 0.4492325,
        size.height * 0.2009178,
        size.width * 0.4486425,
        size.height * 0.2077959)
    ..lineTo(size.width * 0.4483350, size.height * 0.2113766)
    ..lineTo(size.width * 0.4466375, size.height * 0.2068623)
    ..cubicTo(
        size.width * 0.4441500,
        size.height * 0.2002424,
        size.width * 0.4415200,
        size.height * 0.1973073,
        size.width * 0.4377425,
        size.height * 0.1969299)
    ..cubicTo(
        size.width * 0.4301175,
        size.height * 0.1961651,
        size.width * 0.4267200,
        size.height * 0.2032717,
        size.width * 0.4240975,
        size.height * 0.2255054)
    ..cubicTo(
        size.width * 0.4222200,
        size.height * 0.2414072,
        size.width * 0.4223175,
        size.height * 0.2410050,
        size.width * 0.4192225,
        size.height * 0.2450921)
    ..cubicTo(
        size.width * 0.4094825,
        size.height * 0.2579497,
        size.width * 0.4135750,
        size.height * 0.2922413,
        size.width * 0.4257025,
        size.height * 0.2993926)
    ..cubicTo(
        size.width * 0.4284750,
        size.height * 0.3010265,
        size.width * 0.4289350,
        size.height * 0.3003064,
        size.width * 0.4286675,
        size.height * 0.2947641)
    ..cubicTo(
        size.width * 0.4283825,
        size.height * 0.2888593,
        size.width * 0.4289725,
        size.height * 0.2894851,
        size.width * 0.4308600,
        size.height * 0.2970833)
    ..cubicTo(
        size.width * 0.4332750,
        size.height * 0.3068121,
        size.width * 0.4367500,
        size.height * 0.3182890,
        size.width * 0.4394025,
        size.height * 0.3252864)
    ..cubicTo(
        size.width * 0.4422625,
        size.height * 0.3328450,
        size.width * 0.4422700,
        size.height * 0.3322689,
        size.width * 0.4393175,
        size.height * 0.3335850)
    ..cubicTo(
        size.width * 0.4266600,
        size.height * 0.3392117,
        size.width * 0.4099800,
        size.height * 0.3553171,
        size.width * 0.4000850,
        size.height * 0.3714672)
    ..cubicTo(
        size.width * 0.3831700,
        size.height * 0.3990743,
        size.width * 0.3795700,
        size.height * 0.4212136,
        size.width * 0.3832825,
        size.height * 0.4747742)
    ..cubicTo(
        size.width * 0.3843250,
        size.height * 0.4898168,
        size.width * 0.3843725,
        size.height * 0.4915848,
        size.width * 0.3837975,
        size.height * 0.4937699)
    ..lineTo(size.width * 0.3831600, size.height * 0.4961785)
    ..lineTo(size.width * 0.3824325, size.height * 0.4911974)
    ..cubicTo(
        size.width * 0.3811175,
        size.height * 0.4822185,
        size.width * 0.3806175,
        size.height * 0.4804158,
        size.width * 0.3797025,
        size.height * 0.4813891)
    ..cubicTo(
        size.width * 0.3777475,
        size.height * 0.4834700,
        size.width * 0.3700725,
        size.height * 0.4954087,
        size.width * 0.3671650,
        size.height * 0.5008964)
    ..lineTo(size.width * 0.3639750, size.height * 0.5069154)
    ..lineTo(size.width * 0.3652300, size.height * 0.5118618)
    ..cubicTo(
        size.width * 0.3672350,
        size.height * 0.5197531,
        size.width * 0.3660925,
        size.height * 0.5216452,
        size.width * 0.3623800,
        size.height * 0.5165747)
    ..lineTo(size.width * 0.3602925, size.height * 0.5137191)
    ..lineTo(size.width * 0.3586775, size.height * 0.5153679)
    ..cubicTo(
        size.width * 0.3565400,
        size.height * 0.5175431,
        size.width * 0.3565425,
        size.height * 0.5175779,
        size.width * 0.3582150,
        size.height * 0.5088423)
    ..cubicTo(
        size.width * 0.3601000,
        size.height * 0.4989993,
        size.width * 0.3601275,
        size.height * 0.4992873,
        size.width * 0.3568475,
        size.height * 0.4953541)
    ..cubicTo(
        size.width * 0.3522875,
        size.height * 0.4898913,
        size.width * 0.3520225,
        size.height * 0.4859432,
        size.width * 0.3563225,
        size.height * 0.4874926)
    ..cubicTo(
        size.width * 0.3575050,
        size.height * 0.4879197,
        size.width * 0.3591700,
        size.height * 0.4881531,
        size.width * 0.3600250,
        size.height * 0.4880091)
    ..lineTo(size.width * 0.3615800, size.height * 0.4877509)
    ..lineTo(size.width * 0.3623100, size.height * 0.4814189)
    ..cubicTo(
        size.width * 0.3633275,
        size.height * 0.4725791,
        size.width * 0.3645400,
        size.height * 0.4552917,
        size.width * 0.3641900,
        size.height * 0.4545965)
    ..cubicTo(
        size.width * 0.3640300,
        size.height * 0.4542786,
        size.width * 0.3625225,
        size.height * 0.4530272,
        size.width * 0.3608400,
        size.height * 0.4518204)
    ..cubicTo(
        size.width * 0.3564825,
        size.height * 0.4486867,
        size.width * 0.3564400,
        size.height * 0.4461887,
        size.width * 0.3607075,
        size.height * 0.4440681)
    ..cubicTo(
        size.width * 0.3623200,
        size.height * 0.4432735,
        size.width * 0.3637300,
        size.height * 0.4424293,
        size.width * 0.3638475,
        size.height * 0.4422008)
    ..cubicTo(
        size.width * 0.3645800,
        size.height * 0.4407457,
        size.width * 0.3603975,
        size.height * 0.4157905,
        size.width * 0.3574950,
        size.height * 0.4042888)
    ..lineTo(size.width * 0.3558275, size.height * 0.3976838)
    ..lineTo(size.width * 0.3542525, size.height * 0.3995163)
    ..cubicTo(
        size.width * 0.3519575,
        size.height * 0.4021881,
        size.width * 0.3465275,
        size.height * 0.4099602,
        size.width * 0.3465200,
        size.height * 0.4105860)
    ..cubicTo(
        size.width * 0.3465175,
        size.height * 0.4108790,
        size.width * 0.3473900,
        size.height * 0.4132379,
        size.width * 0.3484625,
        size.height * 0.4158253)
    ..cubicTo(
        size.width * 0.3518050,
        size.height * 0.4238855,
        size.width * 0.3496900,
        size.height * 0.4264480,
        size.width * 0.3449050,
        size.height * 0.4201360)
    ..lineTo(size.width * 0.3425050, size.height * 0.4169725)
    ..lineTo(size.width * 0.3400800, size.height * 0.4222069)
    ..cubicTo(
        size.width * 0.3369675,
        size.height * 0.4289262,
        size.width * 0.3324400,
        size.height * 0.4470975,
        size.width * 0.3324975,
        size.height * 0.4526249)
    ..cubicTo(
        size.width * 0.3325050,
        size.height * 0.4533649,
        size.width * 0.3340425,
        size.height * 0.4566276,
        size.width * 0.3359125,
        size.height * 0.4598805)
    ..cubicTo(
        size.width * 0.3408975,
        size.height * 0.4685416,
        size.width * 0.3397550,
        size.height * 0.4711737,
        size.width * 0.3324150,
        size.height * 0.4679556)
    ..cubicTo(
        size.width * 0.3309075,
        size.height * 0.4672951,
        size.width * 0.3308500,
        size.height * 0.4673596,
        size.width * 0.3306275,
        size.height * 0.4699122)
    ..cubicTo(
        size.width * 0.3302725,
        size.height * 0.4739746,
        size.width * 0.3294250,
        size.height * 0.4925184,
        size.width * 0.3295800,
        size.height * 0.4927965)
    ..cubicTo(
        size.width * 0.3296575,
        size.height * 0.4929306,
        size.width * 0.3309150,
        size.height * 0.4942318,
        size.width * 0.3323775,
        size.height * 0.4956869)
    ..cubicTo(
        size.width * 0.3363100,
        size.height * 0.4996052,
        size.width * 0.3358550,
        size.height * 0.5033795,
        size.width * 0.3314475,
        size.height * 0.5033795)
    ..lineTo(size.width * 0.3295075, size.height * 0.5033795)
    ..lineTo(size.width * 0.3294950, size.height * 0.5073872)
    ..lineTo(size.width * 0.3294850, size.height * 0.5113999)
    ..lineTo(size.width * 0.3284175, size.height * 0.5051773)
    ..cubicTo(
        size.width * 0.3272525,
        size.height * 0.4983736,
        size.width * 0.3265200,
        size.height * 0.4952995,
        size.width * 0.3249300,
        size.height * 0.4905270)
    ..lineTo(size.width * 0.3238850, size.height * 0.4873883)
    ..lineTo(size.width * 0.3223050, size.height * 0.4891960)
    ..cubicTo(
        size.width * 0.3199375,
        size.height * 0.4919026,
        size.width * 0.3191050,
        size.height * 0.4909044,
        size.width * 0.3195050,
        size.height * 0.4858339)
    ..cubicTo(
        size.width * 0.3196775,
        size.height * 0.4836388,
        size.width * 0.3198450,
        size.height * 0.4812104,
        size.width * 0.3198800,
        size.height * 0.4804356)
    ..cubicTo(
        size.width * 0.3199625,
        size.height * 0.4785435,
        size.width * 0.3151975,
        size.height * 0.4704337,
        size.width * 0.3120325,
        size.height * 0.4670815)
    ..lineTo(size.width * 0.3094700, size.height * 0.4643700)
    ..lineTo(size.width * 0.3078050, size.height * 0.4665601)
    ..cubicTo(
        size.width * 0.3050125,
        size.height * 0.4702351,
        size.width * 0.3036700,
        size.height * 0.4686955,
        size.width * 0.3049650,
        size.height * 0.4632973)
    ..lineTo(size.width * 0.3056275, size.height * 0.4605361)
    ..lineTo(size.width * 0.3011850, size.height * 0.4569256)
    ..cubicTo(
        size.width * 0.2987425,
        size.height * 0.4549391,
        size.width * 0.2966550,
        size.height * 0.4534890,
        size.width * 0.2965475,
        size.height * 0.4537026)
    ..cubicTo(
        size.width * 0.2961650,
        size.height * 0.4544624,
        size.width * 0.2958000,
        size.height * 0.4836239,
        size.width * 0.2960950,
        size.height * 0.4898615)
    ..lineTo(size.width * 0.2963950, size.height * 0.4961984)
    ..lineTo(size.width * 0.2992675, size.height * 0.4959252)
    ..cubicTo(
        size.width * 0.3042250,
        size.height * 0.4954485,
        size.width * 0.3041975,
        size.height * 0.4996995,
        size.width * 0.2992125,
        size.height * 0.5047402)
    ..cubicTo(
        size.width * 0.2981025,
        size.height * 0.5058676,
        size.width * 0.2971950,
        size.height * 0.5072780,
        size.width * 0.2971950,
        size.height * 0.5078789)
    ..cubicTo(
        size.width * 0.2971950,
        size.height * 0.5094979,
        size.width * 0.2994275,
        size.height * 0.5222660,
        size.width * 0.3010450,
        size.height * 0.5298692)
    ..lineTo(size.width * 0.3024575, size.height * 0.5365190)
    ..lineTo(size.width * 0.3055450, size.height * 0.5361267)
    ..cubicTo(
        size.width * 0.3101500,
        size.height * 0.5355357,
        size.width * 0.3107500,
        size.height * 0.5377307,
        size.width * 0.3075800,
        size.height * 0.5435909)
    ..cubicTo(
        size.width * 0.3058100,
        size.height * 0.5468686,
        size.width * 0.3056825,
        size.height * 0.5473602,
        size.width * 0.3061500,
        size.height * 0.5491431)
    ..cubicTo(
        size.width * 0.3070300,
        size.height * 0.5525102,
        size.width * 0.3106675,
        size.height * 0.5619460,
        size.width * 0.3122550,
        size.height * 0.5649853)
    ..cubicTo(
        size.width * 0.3142825,
        size.height * 0.5688688,
        size.width * 0.3142125,
        size.height * 0.5691370,
        size.width * 0.3110850,
        size.height * 0.5694300)
    ..lineTo(size.width * 0.3083950, size.height * 0.5696783)
    ..lineTo(size.width * 0.3114825, size.height * 0.5974295)
    ..cubicTo(
        size.width * 0.3166875,
        size.height * 0.6442111,
        size.width * 0.3201750,
        size.height * 0.6511787,
        size.width * 0.3383625,
        size.height * 0.6511787)
    ..lineTo(size.width * 0.3457625, size.height * 0.6511787)
    ..lineTo(size.width * 0.3455100, size.height * 0.6535029)
    ..cubicTo(
        size.width * 0.3393200,
        size.height * 0.7105398,
        size.width * 0.3525800,
        size.height * 0.7488590,
        size.width * 0.3877550,
        size.height * 0.7755722)
    ..cubicTo(
        size.width * 0.4294525,
        size.height * 0.8072368,
        size.width * 0.5091275,
        size.height * 0.8166229,
        size.width * 0.5682400,
        size.height * 0.7968276)
    ..cubicTo(
        size.width * 0.6327600,
        size.height * 0.7752246,
        size.width * 0.6655575,
        size.height * 0.7240479,
        size.width * 0.6573850,
        size.height * 0.6577242)
    ..lineTo(size.width * 0.6565800, size.height * 0.6511787)
    ..lineTo(size.width * 0.6624150, size.height * 0.6511787)
    ..cubicTo(
        size.width * 0.6797075,
        size.height * 0.6511787,
        size.width * 0.6833900,
        size.height * 0.6436003,
        size.width * 0.6885275,
        size.height * 0.5973997)
    ..lineTo(size.width * 0.6916100, size.height * 0.5696783)
    ..lineTo(size.width * 0.6889175, size.height * 0.5694300)
    ..cubicTo(
        size.width * 0.6857425,
        size.height * 0.5691321,
        size.width * 0.6857000,
        size.height * 0.5689235,
        size.width * 0.6879525,
        size.height * 0.5645681)
    ..cubicTo(
        size.width * 0.6889025,
        size.height * 0.5627356,
        size.width * 0.6907475,
        size.height * 0.5581170,
        size.width * 0.6920525,
        size.height * 0.5543079)
    ..lineTo(size.width * 0.6944250, size.height * 0.5473850)
    ..lineTo(size.width * 0.6924500, size.height * 0.5435710)
    ..cubicTo(
        size.width * 0.6893050,
        size.height * 0.5374973,
        size.width * 0.6898350,
        size.height * 0.5355357,
        size.width * 0.6944550,
        size.height * 0.5361267)
    ..lineTo(size.width * 0.6975425, size.height * 0.5365190)
    ..lineTo(size.width * 0.6989550, size.height * 0.5298692)
    ..cubicTo(
        size.width * 0.7005725,
        size.height * 0.5222660,
        size.width * 0.7028050,
        size.height * 0.5094979,
        size.width * 0.7028050,
        size.height * 0.5078789)
    ..cubicTo(
        size.width * 0.7028050,
        size.height * 0.5072780,
        size.width * 0.7018975,
        size.height * 0.5058676,
        size.width * 0.7007875,
        size.height * 0.5047402)
    ..cubicTo(
        size.width * 0.6958000,
        size.height * 0.4996995,
        size.width * 0.6957750,
        size.height * 0.4954485,
        size.width * 0.7007375,
        size.height * 0.4959252)
    ..lineTo(size.width * 0.7036125, size.height * 0.4961984)
    ..lineTo(size.width * 0.7039100, size.height * 0.4907107)
    ..cubicTo(
        size.width * 0.7042100,
        size.height * 0.4851535,
        size.width * 0.7038375,
        size.height * 0.4544673,
        size.width * 0.7034600,
        size.height * 0.4537175)
    ..cubicTo(
        size.width * 0.7033475,
        size.height * 0.4534940,
        size.width * 0.7012575,
        size.height * 0.4549391,
        size.width * 0.6988150,
        size.height * 0.4569256)
    ..lineTo(size.width * 0.6943725, size.height * 0.4605361)
    ..lineTo(size.width * 0.6950350, size.height * 0.4632973)
    ..cubicTo(
        size.width * 0.6963300,
        size.height * 0.4686955,
        size.width * 0.6949875,
        size.height * 0.4702351,
        size.width * 0.6921950,
        size.height * 0.4665601)
    ..lineTo(size.width * 0.6905300, size.height * 0.4643700)
    ..lineTo(size.width * 0.6879675, size.height * 0.4670815)
    ..cubicTo(
        size.width * 0.6848025,
        size.height * 0.4704337,
        size.width * 0.6800375,
        size.height * 0.4785435,
        size.width * 0.6801200,
        size.height * 0.4804356)
    ..cubicTo(
        size.width * 0.6801550,
        size.height * 0.4812104,
        size.width * 0.6803225,
        size.height * 0.4836388,
        size.width * 0.6804950,
        size.height * 0.4858339)
    ..cubicTo(
        size.width * 0.6808950,
        size.height * 0.4909143,
        size.width * 0.6800625,
        size.height * 0.4919026,
        size.width * 0.6776775,
        size.height * 0.4891712)
    ..lineTo(size.width * 0.6760775, size.height * 0.4873436)
    ..lineTo(size.width * 0.6744875, size.height * 0.4925432)
    ..cubicTo(
        size.width * 0.6736150,
        size.height * 0.4953988,
        size.width * 0.6723600,
        size.height * 0.5008120,
        size.width * 0.6717025,
        size.height * 0.5045714)
    ..lineTo(size.width * 0.6705050, size.height * 0.5113999)
    ..lineTo(size.width * 0.6705000, size.height * 0.5073872)
    ..lineTo(size.width * 0.6704925, size.height * 0.5033795)
    ..lineTo(size.width * 0.6685525, size.height * 0.5033795)
    ..cubicTo(
        size.width * 0.6640775,
        size.height * 0.5033795,
        size.width * 0.6636650,
        size.height * 0.4995059,
        size.width * 0.6677300,
        size.height * 0.4956521)
    ..cubicTo(
        size.width * 0.6694675,
        size.height * 0.4940033,
        size.width * 0.6704900,
        size.height * 0.4925184,
        size.width * 0.6704850,
        size.height * 0.4916593)
    ..cubicTo(
        size.width * 0.6704750,
        size.height * 0.4897969,
        size.width * 0.6697050,
        size.height * 0.4750622,
        size.width * 0.6693975,
        size.height * 0.4708310)
    ..lineTo(size.width * 0.6691475, size.height * 0.4674192)
    ..lineTo(size.width * 0.6655675, size.height * 0.4685962)
    ..cubicTo(
        size.width * 0.6595225,
        size.height * 0.4705827,
        size.width * 0.6591775,
        size.height * 0.4685167,
        size.width * 0.6641075,
        size.height * 0.4598805)
    ..cubicTo(
        size.width * 0.6679325,
        size.height * 0.4531811,
        size.width * 0.6679125,
        size.height * 0.4533102,
        size.width * 0.6664075,
        size.height * 0.4449372)
    ..cubicTo(
        size.width * 0.6646950,
        size.height * 0.4354071,
        size.width * 0.6625425,
        size.height * 0.4278684,
        size.width * 0.6599025,
        size.height * 0.4221721)
    ..lineTo(size.width * 0.6574950, size.height * 0.4169725)
    ..lineTo(size.width * 0.6550950, size.height * 0.4201360)
    ..cubicTo(
        size.width * 0.6503100,
        size.height * 0.4264480,
        size.width * 0.6481950,
        size.height * 0.4238855,
        size.width * 0.6515375,
        size.height * 0.4158253)
    ..cubicTo(
        size.width * 0.6526100,
        size.height * 0.4132379,
        size.width * 0.6534825,
        size.height * 0.4108790,
        size.width * 0.6534800,
        size.height * 0.4105860)
    ..cubicTo(
        size.width * 0.6534725,
        size.height * 0.4099553,
        size.width * 0.6480375,
        size.height * 0.4021831,
        size.width * 0.6457200,
        size.height * 0.3994815)
    ..lineTo(size.width * 0.6441175, size.height * 0.3976192)
    ..lineTo(size.width * 0.6420450, size.height * 0.4063697)
    ..cubicTo(
        size.width * 0.6390100,
        size.height * 0.4191775,
        size.width * 0.6354550,
        size.height * 0.4408153,
        size.width * 0.6361575,
        size.height * 0.4422108)
    ..cubicTo(
        size.width * 0.6362700,
        size.height * 0.4424342,
        size.width * 0.6376800,
        size.height * 0.4432735,
        size.width * 0.6392925,
        size.height * 0.4440681)
    ..cubicTo(
        size.width * 0.6435850,
        size.height * 0.4461986,
        size.width * 0.6435050,
        size.height * 0.4485874,
        size.width * 0.6390300,
        size.height * 0.4518750)
    ..lineTo(size.width * 0.6358425, size.height * 0.4542190)
    ..lineTo(size.width * 0.6359475, size.height * 0.4591604)
    ..cubicTo(
        size.width * 0.6361250,
        size.height * 0.4673100,
        size.width * 0.6381975,
        size.height * 0.4873983,
        size.width * 0.6389200,
        size.height * 0.4879445)
    ..cubicTo(
        size.width * 0.6392650,
        size.height * 0.4882127,
        size.width * 0.6409725,
        size.height * 0.4880687,
        size.width * 0.6427125,
        size.height * 0.4876317)
    ..cubicTo(
        size.width * 0.6479725,
        size.height * 0.4863057,
        size.width * 0.6481475,
        size.height * 0.4893698,
        size.width * 0.6431525,
        size.height * 0.4953541)
    ..cubicTo(
        size.width * 0.6398725,
        size.height * 0.4992873,
        size.width * 0.6399000,
        size.height * 0.4989993,
        size.width * 0.6417850,
        size.height * 0.5088423)
    ..cubicTo(
        size.width * 0.6434600,
        size.height * 0.5175878,
        size.width * 0.6434625,
        size.height * 0.5175382,
        size.width * 0.6413050,
        size.height * 0.5153630)
    ..lineTo(size.width * 0.6396675, size.height * 0.5137142)
    ..lineTo(size.width * 0.6375500, size.height * 0.5165697)
    ..cubicTo(
        size.width * 0.6339875,
        size.height * 0.5213721,
        size.width * 0.6327575,
        size.height * 0.5197531,
        size.width * 0.6345700,
        size.height * 0.5126514)
    ..cubicTo(
        size.width * 0.6351525,
        size.height * 0.5103670,
        size.width * 0.6356300,
        size.height * 0.5079534,
        size.width * 0.6356300,
        size.height * 0.5072829)
    ..cubicTo(
        size.width * 0.6356300,
        size.height * 0.5050233,
        size.width * 0.6239425,
        size.height * 0.4852628,
        size.width * 0.6203375,
        size.height * 0.4814338)
    ..cubicTo(
        size.width * 0.6190175,
        size.height * 0.4800284,
        size.width * 0.6167975,
        size.height * 0.4928313,
        size.width * 0.6162200,
        size.height * 0.5052021)
    ..lineTo(size.width * 0.6159575, size.height * 0.5108288)
    ..lineTo(size.width * 0.6183725, size.height * 0.5114297)
    ..cubicTo(
        size.width * 0.6206325,
        size.height * 0.5119909,
        size.width * 0.6220250,
        size.height * 0.5134460,
        size.width * 0.6220250,
        size.height * 0.5152487)
    ..cubicTo(
        size.width * 0.6220250,
        size.height * 0.5156758,
        size.width * 0.6208650,
        size.height * 0.5171856,
        size.width * 0.6194500,
        size.height * 0.5186009)
    ..cubicTo(
        size.width * 0.6171975,
        size.height * 0.5208605,
        size.width * 0.6169125,
        size.height * 0.5214168,
        size.width * 0.6171675,
        size.height * 0.5230506)
    ..cubicTo(
        size.width * 0.6181150,
        size.height * 0.5291243,
        size.width * 0.6230750,
        size.height * 0.5454383,
        size.width * 0.6259400,
        size.height * 0.5519043)
    ..lineTo(size.width * 0.6274200, size.height * 0.5552416)
    ..lineTo(size.width * 0.6299625, size.height * 0.5508465)
    ..cubicTo(
        size.width * 0.6342525,
        size.height * 0.5434369,
        size.width * 0.6356575,
        size.height * 0.5452496,
        size.width * 0.6330400,
        size.height * 0.5548244)
    ..cubicTo(
        size.width * 0.6309725,
        size.height * 0.5624028,
        size.width * 0.6309050,
        size.height * 0.5616232,
        size.width * 0.6339250,
        size.height * 0.5650747)
    ..cubicTo(
        size.width * 0.6372575,
        size.height * 0.5688887,
        size.width * 0.6371600,
        size.height * 0.5692562,
        size.width * 0.6328375,
        size.height * 0.5692562)
    ..cubicTo(
        size.width * 0.6295725,
        size.height * 0.5692562,
        size.width * 0.6292150,
        size.height * 0.5694102,
        size.width * 0.6294050,
        size.height * 0.5707312)
    ..cubicTo(
        size.width * 0.6305200,
        size.height * 0.5786125,
        size.width * 0.6303675,
        size.height * 0.5785778,
        size.width * 0.6261425,
        size.height * 0.5699912)
    ..cubicTo(
        size.width * 0.6121375,
        size.height * 0.5415448,
        size.width * 0.6081000,
        size.height * 0.5138781,
        size.width * 0.6113650,
        size.height * 0.4687502)
    ..cubicTo(
        size.width * 0.6141400,
        size.height * 0.4303912,
        size.width * 0.6133575,
        size.height * 0.4148321,
        size.width * 0.6075450,
        size.height * 0.3928417)
    ..cubicTo(
        size.width * 0.5951225,
        size.height * 0.3458564,
        size.width * 0.5736875,
        size.height * 0.3253162,
        size.width * 0.5329500,
        size.height * 0.3213631)
    ..cubicTo(
        size.width * 0.5277800,
        size.height * 0.3208615,
        size.width * 0.5269700,
        size.height * 0.3205288,
        size.width * 0.5203825,
        size.height * 0.3162678)
    ..cubicTo(
        size.width * 0.5097025,
        size.height * 0.3093548,
        size.width * 0.4935750,
        size.height * 0.3068817,
        size.width * 0.4825300,
        size.height * 0.3104673)
    ..lineTo(size.width * 0.4810050, size.height * 0.3109639)
    ..lineTo(size.width * 0.4771825, size.height * 0.2992734)
    ..arcToPoint(Offset(size.width * 0.4730375, size.height * 0.2864904),
        radius:
            Radius.elliptical(size.width * 0.5942875, size.height * 1.180541),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..cubicTo(
        size.width * 0.4728325,
        size.height * 0.2857952,
        size.width * 0.4733375,
        size.height * 0.2840570,
        size.width * 0.4744175,
        size.height * 0.2817328)
    ..cubicTo(
        size.width * 0.4773825,
        size.height * 0.2753363,
        size.width * 0.4783450,
        size.height * 0.2680360,
        size.width * 0.4769025,
        size.height * 0.2628761)
    ..cubicTo(
        size.width * 0.4765250,
        size.height * 0.2615253,
        size.width * 0.4764600,
        size.height * 0.2591912,
        size.width * 0.4766800,
        size.height * 0.2548507)
    ..cubicTo(
        size.width * 0.4782425,
        size.height * 0.2238914,
        size.width * 0.4752050,
        size.height * 0.2049751,
        size.width * 0.4675575,
        size.height * 0.1980274)
    ..cubicTo(
        size.width * 0.4635275,
        size.height * 0.1943624,
        size.width * 0.4567550,
        size.height * 0.1919438,
        size.width * 0.4544700,
        size.height * 0.1933592)
    ..moveTo(size.width * 0.8070275, size.height * 0.2579050)
    ..lineTo(size.width * 0.8114250, size.height * 0.2960802)
    ..cubicTo(
        size.width * 0.8116000,
        size.height * 0.2975899,
        size.width * 0.8110575,
        size.height * 0.2982852,
        size.width * 0.8072800,
        size.height * 0.3013940)
    ..cubicTo(
        size.width * 0.7997125,
        size.height * 0.3076167,
        size.width * 0.7962650,
        size.height * 0.3116244,
        size.width * 0.7951850,
        size.height * 0.3154533)
    ..cubicTo(
        size.width * 0.7929975,
        size.height * 0.3232106,
        size.width * 0.7947200,
        size.height * 0.3299398,
        size.width * 0.7993875,
        size.height * 0.3318666)
    ..lineTo(size.width * 0.8017650, size.height * 0.3328450)
    ..lineTo(size.width * 0.8020350, size.height * 0.3376622)
    ..cubicTo(
        size.width * 0.8026325,
        size.height * 0.3483644,
        size.width * 0.8035700,
        size.height * 0.3721128,
        size.width * 0.8039925,
        size.height * 0.3872448)
    ..cubicTo(
        size.width * 0.8051050,
        size.height * 0.4270539,
        size.width * 0.8086650,
        size.height * 0.4395737,
        size.width * 0.8192000,
        size.height * 0.4407110)
    ..lineTo(size.width * 0.8227100, size.height * 0.4410884)
    ..lineTo(size.width * 0.8198350, size.height * 0.4471074)
    ..cubicTo(
        size.width * 0.8138625,
        size.height * 0.4596024,
        size.width * 0.8078425,
        size.height * 0.4751814,
        size.width * 0.8036875,
        size.height * 0.4889030)
    ..cubicTo(
        size.width * 0.8006975,
        size.height * 0.4987659,
        size.width * 0.8010400,
        size.height * 0.4988603,
        size.width * 0.7993875,
        size.height * 0.4877211)
    ..cubicTo(
        size.width * 0.7922175,
        size.height * 0.4393453,
        size.width * 0.7912600,
        size.height * 0.4175635,
        size.width * 0.7912475,
        size.height * 0.3022879)
    ..lineTo(size.width * 0.7912425, size.height * 0.2384474)
    ..lineTo(size.width * 0.7922550, size.height * 0.2375286)
    ..cubicTo(
        size.width * 0.7938825,
        size.height * 0.2360537,
        size.width * 0.7987100,
        size.height * 0.2266477,
        size.width * 0.8001925,
        size.height * 0.2220589)
    ..cubicTo(
        size.width * 0.8021675,
        size.height * 0.2159405,
        size.width * 0.8022425,
        size.height * 0.2163329,
        size.width * 0.8070275,
        size.height * 0.2579050)
    ..moveTo(size.width * 0.1306050, size.height * 0.2373300)
    ..cubicTo(
        size.width * 0.1406200,
        size.height * 0.2410447,
        size.width * 0.1489000,
        size.height * 0.2440890,
        size.width * 0.1490075,
        size.height * 0.2440890)
    ..cubicTo(
        size.width * 0.1491150,
        size.height * 0.2440890,
        size.width * 0.1503750,
        size.height * 0.2486529,
        size.width * 0.1518075,
        size.height * 0.2542349)
    ..cubicTo(
        size.width * 0.1556575,
        size.height * 0.2692378,
        size.width * 0.1592175,
        size.height * 0.2775612,
        size.width * 0.1639525,
        size.height * 0.2826267)
    ..cubicTo(
        size.width * 0.1656300,
        size.height * 0.2844195,
        size.width * 0.1657250,
        size.height * 0.2862173,
        size.width * 0.1649550,
        size.height * 0.3016423)
    ..cubicTo(
        size.width * 0.1629800,
        size.height * 0.3412627,
        size.width * 0.1589375,
        size.height * 0.3612616,
        size.width * 0.1514675,
        size.height * 0.3683434)
    ..cubicTo(
        size.width * 0.1433425,
        size.height * 0.3760410,
        size.width * 0.1310850,
        size.height * 0.4121702,
        size.width * 0.1214850,
        size.height * 0.4567121)
    ..cubicTo(
        size.width * 0.1208600,
        size.height * 0.4596173,
        size.width * 0.1201750,
        size.height * 0.4619912,
        size.width * 0.1199650,
        size.height * 0.4619912)
    ..cubicTo(
        size.width * 0.1196450,
        size.height * 0.4619912,
        size.width * 0.05826750,
        size.height * 0.4022825,
        size.width * 0.05791250,
        size.height * 0.4016269)
    ..cubicTo(
        size.width * 0.05784500,
        size.height * 0.4015028,
        size.width * 0.06478250,
        size.height * 0.3801928,
        size.width * 0.07332500,
        size.height * 0.3542692)
    ..cubicTo(
        size.width * 0.08187000,
        size.height * 0.3283456,
        size.width * 0.08886000,
        size.height * 0.3069214,
        size.width * 0.08886000,
        size.height * 0.3066532)
    ..cubicTo(
        size.width * 0.08886000,
        size.height * 0.3061169,
        size.width * 0.08880250,
        size.height * 0.3061218,
        size.width * 0.06463000,
        size.height * 0.3078203)
    ..cubicTo(
        size.width * 0.05539500,
        size.height * 0.3084659,
        size.width * 0.04780250,
        size.height * 0.3089228,
        size.width * 0.04775750,
        size.height * 0.3088334)
    ..cubicTo(
        size.width * 0.04771250,
        size.height * 0.3087440,
        size.width * 0.04925750,
        size.height * 0.3047412,
        size.width * 0.05119250,
        size.height * 0.2999339)
    ..cubicTo(
        size.width * 0.06004000,
        size.height * 0.2779386,
        size.width * 0.06257000,
        size.height * 0.2651854,
        size.width * 0.06255500,
        size.height * 0.2426239)
    ..lineTo(size.width * 0.06255000, size.height * 0.2314450)
    ..lineTo(size.width * 0.07453500, size.height * 0.2312066)
    ..cubicTo(
        size.width * 0.08112750,
        size.height * 0.2310775,
        size.width * 0.09234500,
        size.height * 0.2308789,
        size.width * 0.09946000,
        size.height * 0.2307696)
    ..lineTo(size.width * 0.1123975, size.height * 0.2305759)
    ..lineTo(size.width * 0.1306050, size.height * 0.2373300)
    ..moveTo(size.width * 0.8136800, size.height * 0.2399670)
    ..cubicTo(
        size.width * 0.8190475,
        size.height * 0.2441486,
        size.width * 0.8326050,
        size.height * 0.2428524,
        size.width * 0.8370025,
        size.height * 0.2377322)
    ..lineTo(size.width * 0.8387775, size.height * 0.2356663)
    ..lineTo(size.width * 0.8406250, size.height * 0.2376975)
    ..cubicTo(
        size.width * 0.8453025,
        size.height * 0.2428375,
        size.width * 0.8583175,
        size.height * 0.2442976,
        size.width * 0.8635900,
        size.height * 0.2402799)
    ..cubicTo(
        size.width * 0.8641975,
        size.height * 0.2398131,
        size.width * 0.8594625,
        size.height * 0.3000680,
        size.width * 0.8588425,
        size.height * 0.3006640)
    ..cubicTo(
        size.width * 0.8581400,
        size.height * 0.3013444,
        size.width * 0.8544850,
        size.height * 0.2981064,
        size.width * 0.8526775,
        size.height * 0.2952061)
    ..cubicTo(
        size.width * 0.8446750,
        size.height * 0.2823635,
        size.width * 0.8358375,
        size.height * 0.2816335,
        size.width * 0.8192150,
        size.height * 0.2924449)
    ..cubicTo(
        size.width * 0.8162425,
        size.height * 0.2943768,
        size.width * 0.8137450,
        size.height * 0.2957822,
        size.width * 0.8136625,
        size.height * 0.2955736)
    ..cubicTo(
        size.width * 0.8135825,
        size.height * 0.2953601,
        size.width * 0.8119275,
        size.height * 0.2811468,
        size.width * 0.8099875,
        size.height * 0.2639935)
    ..cubicTo(
        size.width * 0.8080475,
        size.height * 0.2468353,
        size.width * 0.8063575,
        size.height * 0.2322595,
        size.width * 0.8062300,
        size.height * 0.2315990)
    ..cubicTo(
        size.width * 0.8061000,
        size.height * 0.2309335,
        size.width * 0.8071775,
        size.height * 0.2321204,
        size.width * 0.8086225,
        size.height * 0.2342311)
    ..cubicTo(
        size.width * 0.8100650,
        size.height * 0.2363417,
        size.width * 0.8123400,
        size.height * 0.2389241,
        size.width * 0.8136800,
        size.height * 0.2399670)
    ..moveTo(size.width * 0.4585075, size.height * 0.2460109)
    ..arcToPoint(Offset(size.width * 0.4640125, size.height * 0.2647236),
        radius:
            Radius.elliptical(size.width * 0.2479800, size.height * 0.4926078),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..lineTo(size.width * 0.4666725, size.height * 0.2743282)
    ..lineTo(size.width * 0.4676675, size.height * 0.2693521)
    ..cubicTo(
        size.width * 0.4687125,
        size.height * 0.2641177,
        size.width * 0.4711925,
        size.height * 0.2576020,
        size.width * 0.4721350,
        size.height * 0.2576020)
    ..cubicTo(
        size.width * 0.4730900,
        size.height * 0.2576020,
        size.width * 0.4757050,
        size.height * 0.2641177,
        size.width * 0.4759975,
        size.height * 0.2672365)
    ..cubicTo(
        size.width * 0.4763725,
        size.height * 0.2711995,
        size.width * 0.4749800,
        size.height * 0.2769057,
        size.width * 0.4726900,
        size.height * 0.2807992)
    ..lineTo(size.width * 0.4709650, size.height * 0.2837342)
    ..lineTo(size.width * 0.4712075, size.height * 0.2901009)
    ..cubicTo(
        size.width * 0.4721825,
        size.height * 0.3157314,
        size.width * 0.4670675,
        size.height * 0.3400212,
        size.width * 0.4598150,
        size.height * 0.3442126)
    ..cubicTo(
        size.width * 0.4522400,
        size.height * 0.3485879,
        size.width * 0.4443025,
        size.height * 0.3370563,
        size.width * 0.4361775,
        size.height * 0.3098813)
    ..cubicTo(
        size.width * 0.4283300,
        size.height * 0.2836200,
        size.width * 0.4285400,
        size.height * 0.2816186,
        size.width * 0.4404125,
        size.height * 0.2698238)
    ..cubicTo(
        size.width * 0.4492925,
        size.height * 0.2610039,
        size.width * 0.4512325,
        size.height * 0.2566634,
        size.width * 0.4539025,
        size.height * 0.2396343)
    ..cubicTo(
        size.width * 0.4548125,
        size.height * 0.2338338,
        size.width * 0.4546175,
        size.height * 0.2335606,
        size.width * 0.4585075,
        size.height * 0.2460109)
    ..moveTo(size.width * 0.4951075, size.height * 0.2781224)
    ..cubicTo(
        size.width * 0.4947700,
        size.height * 0.2933885,
        size.width * 0.4948375,
        size.height * 0.2951863,
        size.width * 0.4957550,
        size.height * 0.2951863)
    ..cubicTo(
        size.width * 0.4962850,
        size.height * 0.2951863,
        size.width * 0.4964050,
        size.height * 0.2927429,
        size.width * 0.4965025,
        size.height * 0.2797712)
    ..cubicTo(
        size.width * 0.4965975,
        size.height * 0.2669286,
        size.width * 0.4965175,
        size.height * 0.2643561,
        size.width * 0.4960150,
        size.height * 0.2643561)
    ..cubicTo(
        size.width * 0.4955325,
        size.height * 0.2643561,
        size.width * 0.4953525,
        size.height * 0.2670974,
        size.width * 0.4951075,
        size.height * 0.2781224)
    ..moveTo(size.width * 0.5069400, size.height * 0.2738216)
    ..cubicTo(
        size.width * 0.5063550,
        size.height * 0.2753413,
        size.width * 0.5033925,
        size.height * 0.2923208,
        size.width * 0.5033975,
        size.height * 0.2941285)
    ..cubicTo(
        size.width * 0.5034050,
        size.height * 0.2967854,
        size.width * 0.5042650,
        size.height * 0.2956829,
        size.width * 0.5050450,
        size.height * 0.2920178)
    ..cubicTo(
        size.width * 0.5076900,
        size.height * 0.2795626,
        size.width * 0.5087850,
        size.height * 0.2690442,
        size.width * 0.5069400,
        size.height * 0.2738216)
    ..moveTo(size.width * 0.04746000, size.height * 0.2910097)
    ..cubicTo(
        size.width * 0.05154500,
        size.height * 0.2919036,
        size.width * 0.05155250,
        size.height * 0.2920327,
        size.width * 0.04810250,
        size.height * 0.3001723)
    ..cubicTo(
        size.width * 0.04332500,
        size.height * 0.3114406,
        size.width * 0.04307500,
        size.height * 0.3114357,
        size.width * 0.04266750,
        size.height * 0.2999836)
    ..cubicTo(
        size.width * 0.04234750,
        size.height * 0.2910544,
        size.width * 0.04256000,
        size.height * 0.2893261,
        size.width * 0.04390250,
        size.height * 0.2899767)
    ..cubicTo(
        size.width * 0.04442750,
        size.height * 0.2902300,
        size.width * 0.04602750,
        size.height * 0.2906918,
        size.width * 0.04746000,
        size.height * 0.2910097)
    ..moveTo(size.width * 0.1811225, size.height * 0.3391769)
    ..cubicTo(
        size.width * 0.1882550,
        size.height * 0.3649813,
        size.width * 0.1941450,
        size.height * 0.3865694,
        size.width * 0.1942150,
        size.height * 0.3871604)
    ..cubicTo(
        size.width * 0.1943375,
        size.height * 0.3882281,
        size.width * 0.1644800,
        size.height * 0.4301975,
        size.width * 0.1638225,
        size.height * 0.4298797)
    ..cubicTo(
        size.width * 0.1635200,
        size.height * 0.4297307,
        size.width * 0.1505550,
        size.height * 0.3761751,
        size.width * 0.1505225,
        size.height * 0.3749435)
    ..cubicTo(
        size.width * 0.1505150,
        size.height * 0.3746803,
        size.width * 0.1516825,
        size.height * 0.3733543,
        size.width * 0.1531150,
        size.height * 0.3719936)
    ..cubicTo(
        size.width * 0.1610100,
        size.height * 0.3644946,
        size.width * 0.1675175,
        size.height * 0.3303321,
        size.width * 0.1675175,
        size.height * 0.2963732)
    ..cubicTo(
        size.width * 0.1675175,
        size.height * 0.2940937,
        size.width * 0.1676600,
        size.height * 0.2922363,
        size.width * 0.1678350,
        size.height * 0.2922512)
    ..cubicTo(
        size.width * 0.1680125,
        size.height * 0.2922612,
        size.width * 0.1739900,
        size.height * 0.3133775,
        size.width * 0.1811225,
        size.height * 0.3391769)
    ..moveTo(size.width * 0.4793600, size.height * 0.3171468)
    ..cubicTo(
        size.width * 0.4782425,
        size.height * 0.3385512,
        size.width * 0.4726175,
        size.height * 0.3525509,
        size.width * 0.4651350,
        size.height * 0.3525509)
    ..cubicTo(
        size.width * 0.4616375,
        size.height * 0.3525559,
        size.width * 0.4577250,
        size.height * 0.3491739,
        size.width * 0.4596100,
        size.height * 0.3477784)
    ..cubicTo(
        size.width * 0.4674575,
        size.height * 0.3419629,
        size.width * 0.4720450,
        size.height * 0.3257830,
        size.width * 0.4726700,
        size.height * 0.3017069)
    ..lineTo(size.width * 0.4729175, size.height * 0.2921767)
    ..lineTo(size.width * 0.4762700, size.height * 0.3021290)
    ..lineTo(size.width * 0.4796250, size.height * 0.3120763)
    ..lineTo(size.width * 0.4793600, size.height * 0.3171468)
    ..moveTo(size.width * 0.5045325, size.height * 0.3125034)
    ..cubicTo(
        size.width * 0.5071400,
        size.height * 0.3129603,
        size.width * 0.5096075,
        size.height * 0.3135910,
        size.width * 0.5100100,
        size.height * 0.3138989)
    ..cubicTo(
        size.width * 0.5104575,
        size.height * 0.3142366,
        size.width * 0.5047325,
        size.height * 0.3179017,
        size.width * 0.4953500,
        size.height * 0.3232701)
    ..cubicTo(
        size.width * 0.4868850,
        size.height * 0.3281221,
        size.width * 0.4798900,
        size.height * 0.3319560,
        size.width * 0.4798075,
        size.height * 0.3317872)
    ..cubicTo(
        size.width * 0.4797225,
        size.height * 0.3316233,
        size.width * 0.4799250,
        size.height * 0.3292097,
        size.width * 0.4802550,
        size.height * 0.3264287)
    ..cubicTo(
        size.width * 0.4805850,
        size.height * 0.3236426,
        size.width * 0.4808575,
        size.height * 0.3197739,
        size.width * 0.4808600,
        size.height * 0.3178322)
    ..cubicTo(
        size.width * 0.4808675,
        size.height * 0.3143161,
        size.width * 0.4808800,
        size.height * 0.3142863,
        size.width * 0.4826750,
        size.height * 0.3137648)
    ..cubicTo(
        size.width * 0.4890200,
        size.height * 0.3119075,
        size.width * 0.4981425,
        size.height * 0.3113811,
        size.width * 0.5045325,
        size.height * 0.3125034)
    ..moveTo(size.width * 0.5186375, size.height * 0.3185076)
    ..cubicTo(
        size.width * 0.5208225,
        size.height * 0.3199527,
        size.width * 0.5224475,
        size.height * 0.3214029,
        size.width * 0.5222525,
        size.height * 0.3217306)
    ..cubicTo(
        size.width * 0.5220575,
        size.height * 0.3220584,
        size.width * 0.5205575,
        size.height * 0.3246508,
        size.width * 0.5189200,
        size.height * 0.3274815)
    ..lineTo(size.width * 0.5159450, size.height * 0.3326364)
    ..lineTo(size.width * 0.4683500, size.height * 0.3610083)
    ..lineTo(size.width * 0.4207550, size.height * 0.3893753)
    ..lineTo(size.width * 0.4163750, size.height * 0.3857748)
    ..cubicTo(
        size.width * 0.4113100,
        size.height * 0.3816131,
        size.width * 0.4114250,
        size.height * 0.3820452,
        size.width * 0.4138825,
        size.height * 0.3764632)
    ..lineTo(size.width * 0.4157775, size.height * 0.3721624)
    ..lineTo(size.width * 0.4360200, size.height * 0.3605564)
    ..lineTo(size.width * 0.4562650, size.height * 0.3489504)
    ..lineTo(size.width * 0.4581000, size.height * 0.3514087)
    ..cubicTo(
        size.width * 0.4649400,
        size.height * 0.3605812,
        size.width * 0.4724725,
        size.height * 0.3559329,
        size.width * 0.4778900,
        size.height * 0.3392017)
    ..lineTo(size.width * 0.4789550, size.height * 0.3359191)
    ..lineTo(size.width * 0.4961725, size.height * 0.3259668)
    ..cubicTo(
        size.width * 0.5162350,
        size.height * 0.3143707,
        size.width * 0.5136750,
        size.height * 0.3152199,
        size.width * 0.5186375,
        size.height * 0.3185076)
    ..moveTo(size.width * 0.2784875, size.height * 0.3213184)
    ..cubicTo(
        size.width * 0.2784875,
        size.height * 0.3240647,
        size.width * 0.2785725,
        size.height * 0.3242336,
        size.width * 0.2800800,
        size.height * 0.3244819)
    ..cubicTo(
        size.width * 0.2818225,
        size.height * 0.3247699,
        size.width * 0.2821550,
        size.height * 0.3273822,
        size.width * 0.2813125,
        size.height * 0.3341958)
    ..cubicTo(
        size.width * 0.2809850,
        size.height * 0.3368626,
        size.width * 0.2810000,
        size.height * 0.3368924,
        size.width * 0.2826925,
        size.height * 0.3371507)
    ..lineTo(size.width * 0.2844025, size.height * 0.3374139)
    ..lineTo(size.width * 0.2835650, size.height * 0.3411237)
    ..cubicTo(
        size.width * 0.2828575,
        size.height * 0.3442474,
        size.width * 0.2828050,
        size.height * 0.3452555,
        size.width * 0.2832200,
        size.height * 0.3475400)
    ..cubicTo(
        size.width * 0.2839275,
        size.height * 0.3514285,
        size.width * 0.2834825,
        size.height * 0.3537825,
        size.width * 0.2806025,
        size.height * 0.3613758)
    ..cubicTo(
        size.width * 0.2754550,
        size.height * 0.3749485,
        size.width * 0.2790950,
        size.height * 0.3698383,
        size.width * 0.2097175,
        size.height * 0.4608042)
    ..cubicTo(
        size.width * 0.1970125,
        size.height * 0.4774609,
        size.width * 0.1943675,
        size.height * 0.4806045,
        size.width * 0.1923400,
        size.height * 0.4814388)
    ..cubicTo(
        size.width * 0.1878875,
        size.height * 0.4832763,
        size.width * 0.1828600,
        size.height * 0.4800532,
        size.width * 0.1800100,
        size.height * 0.4735475)
    ..cubicTo(
        size.width * 0.1787925,
        size.height * 0.4707664,
        size.width * 0.1677650,
        size.height * 0.4307289,
        size.width * 0.1680775,
        size.height * 0.4302174)
    ..cubicTo(
        size.width * 0.1693875,
        size.height * 0.4280769,
        size.width * 0.1939975,
        size.height * 0.3944259,
        size.width * 0.1942550,
        size.height * 0.3944259)
    ..cubicTo(
        size.width * 0.1944625,
        size.height * 0.3944259,
        size.width * 0.1961050,
        size.height * 0.3995759,
        size.width * 0.1979075,
        size.height * 0.4058730)
    ..cubicTo(
        size.width * 0.1997650,
        size.height * 0.4123688,
        size.width * 0.2014150,
        size.height * 0.4172109,
        size.width * 0.2017150,
        size.height * 0.4170669)
    ..cubicTo(
        size.width * 0.2020075,
        size.height * 0.4169228,
        size.width * 0.2130425,
        size.height * 0.4043683,
        size.width * 0.2262350,
        size.height * 0.3891618)
    ..lineTo(size.width * 0.2502250, size.height * 0.3615149)
    ..lineTo(size.width * 0.2544400, size.height * 0.3462835)
    ..cubicTo(
        size.width * 0.2595525,
        size.height * 0.3278192,
        size.width * 0.2610650,
        size.height * 0.3281519,
        size.width * 0.2580900,
        size.height * 0.3470881)
    ..cubicTo(
        size.width * 0.2574000,
        size.height * 0.3514832,
        size.width * 0.2568750,
        size.height * 0.3551433,
        size.width * 0.2569250,
        size.height * 0.3552326)
    ..cubicTo(
        size.width * 0.2569750,
        size.height * 0.3553171,
        size.width * 0.2600775,
        size.height * 0.3497053,
        size.width * 0.2638175,
        size.height * 0.3427575)
    ..cubicTo(
        size.width * 0.2778350,
        size.height * 0.3167297,
        size.width * 0.2784875,
        size.height * 0.3157761,
        size.width * 0.2784875,
        size.height * 0.3213184)
    ..moveTo(size.width * 0.5290525, size.height * 0.3254652)
    ..lineTo(size.width * 0.5328075, size.height * 0.3282016)
    ..lineTo(size.width * 0.5252300, size.height * 0.3403837)
    ..lineTo(size.width * 0.5176525, size.height * 0.3525658)
    ..lineTo(size.width * 0.4719425, size.height * 0.3798650)
    ..cubicTo(
        size.width * 0.4468025,
        size.height * 0.3948779,
        size.width * 0.4256600,
        size.height * 0.4073629,
        size.width * 0.4249575,
        size.height * 0.4076062)
    ..cubicTo(
        size.width * 0.4237425,
        size.height * 0.4080284,
        size.width * 0.4081650,
        size.height * 0.3972169,
        size.width * 0.4071400,
        size.height * 0.3952454)
    ..cubicTo(
        size.width * 0.4069150,
        size.height * 0.3948083,
        size.width * 0.4075100,
        size.height * 0.3924891,
        size.width * 0.4085900,
        size.height * 0.3896087)
    ..lineTo(size.width * 0.4105100, size.height * 0.3844786)
    ..cubicTo(
        size.width * 0.4105550,
        size.height * 0.3843545,
        size.width * 0.4128425,
        size.height * 0.3861622,
        size.width * 0.4155925,
        size.height * 0.3885062)
    ..lineTo(size.width * 0.4205950, size.height * 0.3927573)
    ..lineTo(size.width * 0.4682700, size.height * 0.3643754)
    ..cubicTo(
        size.width * 0.4944900,
        size.height * 0.3487666,
        size.width * 0.5163275,
        size.height * 0.3354969,
        size.width * 0.5167950,
        size.height * 0.3348960)
    ..cubicTo(
        size.width * 0.5172625,
        size.height * 0.3342902,
        size.width * 0.5190800,
        size.height * 0.3312955,
        size.width * 0.5208325,
        size.height * 0.3282364)
    ..cubicTo(
        size.width * 0.5245275,
        size.height * 0.3217952,
        size.width * 0.5241800,
        size.height * 0.3219094,
        size.width * 0.5290525,
        size.height * 0.3254652)
    ..moveTo(size.width * 0.8335900, size.height * 0.3300242)
    ..cubicTo(
        size.width * 0.8356600,
        size.height * 0.3354423,
        size.width * 0.8381050,
        size.height * 0.3387250,
        size.width * 0.8416250,
        size.height * 0.3408108)
    ..lineTo(size.width * 0.8433250, size.height * 0.3418189)
    ..lineTo(size.width * 0.8436025, size.height * 0.3472172)
    ..cubicTo(
        size.width * 0.8440675,
        size.height * 0.3562507,
        size.width * 0.8468475,
        size.height * 0.3685768,
        size.width * 0.8509750,
        size.height * 0.3799196)
    ..lineTo(size.width * 0.8523050, size.height * 0.3835748)
    ..lineTo(size.width * 0.8510625, size.height * 0.3968147)
    ..cubicTo(
        size.width * 0.8498775,
        size.height * 0.4094139,
        size.width * 0.8498125,
        size.height * 0.4113110,
        size.width * 0.8497200,
        size.height * 0.4362314)
    ..cubicTo(
        size.width * 0.8496100,
        size.height * 0.4656413,
        size.width * 0.8496375,
        size.height * 0.4652937,
        size.width * 0.8471400,
        size.height * 0.4702251)
    ..cubicTo(
        size.width * 0.8449600,
        size.height * 0.4745358,
        size.width * 0.8428400,
        size.height * 0.4825463,
        size.width * 0.8373625,
        size.height * 0.5071786)
    ..cubicTo(
        size.width * 0.8259000,
        size.height * 0.5587378,
        size.width * 0.8215625,
        size.height * 0.5678557,
        size.width * 0.8170025,
        size.height * 0.5499824)
    ..cubicTo(
        size.width * 0.8137525,
        size.height * 0.5372391,
        size.width * 0.8126975,
        size.height * 0.5086933,
        size.width * 0.8149900,
        size.height * 0.4955031)
    ..cubicTo(
        size.width * 0.8169000,
        size.height * 0.4845179,
        size.width * 0.8227475,
        size.height * 0.4702152,
        size.width * 0.8288925,
        size.height * 0.4615045)
    ..lineTo(size.width * 0.8315200, size.height * 0.4577798)
    ..lineTo(size.width * 0.8324900, size.height * 0.4479765)
    ..cubicTo(
        size.width * 0.8335650,
        size.height * 0.4371303,
        size.width * 0.8335800,
        size.height * 0.4372495,
        size.width * 0.8308100,
        size.height * 0.4362215)
    ..cubicTo(
        size.width * 0.8299775,
        size.height * 0.4359086,
        size.width * 0.8291925,
        size.height * 0.4353177,
        size.width * 0.8290650,
        size.height * 0.4349055)
    ..cubicTo(
        size.width * 0.8289325,
        size.height * 0.4344833,
        size.width * 0.8281200,
        size.height * 0.4345529,
        size.width * 0.8271875,
        size.height * 0.4350644)
    ..cubicTo(
        size.width * 0.8239375,
        size.height * 0.4368423,
        size.width * 0.8163250,
        size.height * 0.4364549,
        size.width * 0.8144125,
        size.height * 0.4344088)
    ..cubicTo(
        size.width * 0.8089650,
        size.height * 0.4285736,
        size.width * 0.8070075,
        size.height * 0.4170470,
        size.width * 0.8061225,
        size.height * 0.3855563)
    ..arcToPoint(Offset(size.width * 0.8048900, size.height * 0.3521933),
        radius:
            Radius.elliptical(size.width * 0.5882925, size.height * 1.168632),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.8044725,
        size.height * 0.3431350,
        size.width * 0.8041675,
        size.height * 0.3356261,
        size.width * 0.8042125,
        size.height * 0.3355019)
    ..cubicTo(
        size.width * 0.8042950,
        size.height * 0.3352735,
        size.width * 0.8304775,
        size.height * 0.3271239,
        size.width * 0.8316875,
        size.height * 0.3269551)
    ..cubicTo(
        size.width * 0.8320700,
        size.height * 0.3269004,
        size.width * 0.8329250,
        size.height * 0.3282810,
        size.width * 0.8335900,
        size.height * 0.3300242)
    ..moveTo(size.width * 0.5400975, size.height * 0.3333863)
    ..cubicTo(
        size.width * 0.5423925,
        size.height * 0.3350351,
        size.width * 0.5441125,
        size.height * 0.3366640,
        size.width * 0.5439250,
        size.height * 0.3370067)
    ..cubicTo(
        size.width * 0.5437350,
        size.height * 0.3373493,
        size.width * 0.5382225,
        size.height * 0.3453499,
        size.width * 0.5316750,
        size.height * 0.3547857)
    ..lineTo(size.width * 0.5197700, size.height * 0.3719390)
    ..lineTo(size.width * 0.4735150, size.height * 0.3995411)
    ..lineTo(size.width * 0.4272600, size.height * 0.4271383)
    ..lineTo(size.width * 0.4143150, size.height * 0.4194407)
    ..cubicTo(
        size.width * 0.4071975,
        size.height * 0.4152045,
        size.width * 0.4013700,
        size.height * 0.4114352,
        size.width * 0.4013650,
        size.height * 0.4110677)
    ..cubicTo(
        size.width * 0.4013625,
        size.height * 0.4107002,
        size.width * 0.4023200,
        size.height * 0.4075715,
        size.width * 0.4034925,
        size.height * 0.4041150)
    ..lineTo(size.width * 0.4056250, size.height * 0.3978377)
    ..lineTo(size.width * 0.4076375, size.height * 0.3993921)
    ..cubicTo(
        size.width * 0.4087450,
        size.height * 0.4002463,
        size.width * 0.4128750,
        size.height * 0.4033452,
        size.width * 0.4168150,
        size.height * 0.4062703)
    ..lineTo(size.width * 0.4239775, size.height * 0.4115991)
    ..lineTo(size.width * 0.4712275, size.height * 0.3834457)
    ..lineTo(size.width * 0.5184775, size.height * 0.3552922)
    ..lineTo(size.width * 0.5265250, size.height * 0.3423056)
    ..cubicTo(
        size.width * 0.5320750,
        size.height * 0.3333466,
        size.width * 0.5347850,
        size.height * 0.3294829,
        size.width * 0.5352500,
        size.height * 0.3298504)
    ..cubicTo(
        size.width * 0.5356225,
        size.height * 0.3301483,
        size.width * 0.5378025,
        size.height * 0.3317375,
        size.width * 0.5400975,
        size.height * 0.3333863)
    ..moveTo(size.width * 0.4463675, size.height * 0.3402595)
    ..cubicTo(
        size.width * 0.4472200,
        size.height * 0.3415954,
        size.width * 0.4490325,
        size.height * 0.3438004,
        size.width * 0.4503975,
        size.height * 0.3451661)
    ..lineTo(size.width * 0.4528800, size.height * 0.3476492)
    ..lineTo(size.width * 0.4363975, size.height * 0.3570900)
    ..cubicTo(
        size.width * 0.4273325,
        size.height * 0.3622797,
        size.width * 0.4198500,
        size.height * 0.3664016,
        size.width * 0.4197725,
        size.height * 0.3662477)
    ..cubicTo(
        size.width * 0.4191275,
        size.height * 0.3649664,
        size.width * 0.4426050,
        size.height * 0.3379502,
        size.width * 0.4444550,
        size.height * 0.3378559)
    ..cubicTo(
        size.width * 0.4446550,
        size.height * 0.3378460,
        size.width * 0.4455150,
        size.height * 0.3389286,
        size.width * 0.4463675,
        size.height * 0.3402595)
    ..moveTo(size.width * 0.5501550, size.height * 0.3407363)
    ..cubicTo(
        size.width * 0.5545500,
        size.height * 0.3439842,
        size.width * 0.5548975,
        size.height * 0.3444113,
        size.width * 0.5539925,
        size.height * 0.3454542)
    ..cubicTo(
        size.width * 0.5536450,
        size.height * 0.3458564,
        size.width * 0.5461300,
        size.height * 0.3565636,
        size.width * 0.5372975,
        size.height * 0.3692522)
    ..lineTo(size.width * 0.5212350, size.height * 0.3923153)
    ..lineTo(size.width * 0.4759425, size.height * 0.4192669)
    ..lineTo(size.width * 0.4306500, size.height * 0.4462234)
    ..lineTo(size.width * 0.4139850, size.height * 0.4370906)
    ..cubicTo(
        size.width * 0.4048200,
        size.height * 0.4320648,
        size.width * 0.3970875,
        size.height * 0.4277194,
        size.width * 0.3968025,
        size.height * 0.4274264)
    ..cubicTo(
        size.width * 0.3962150,
        size.height * 0.4268354,
        size.width * 0.3993350,
        size.height * 0.4157359,
        size.width * 0.4002925,
        size.height * 0.4150059)
    ..cubicTo(
        size.width * 0.4005925,
        size.height * 0.4147725,
        size.width * 0.4067750,
        size.height * 0.4181545,
        size.width * 0.4140275,
        size.height * 0.4225198)
    ..lineTo(size.width * 0.4272150, size.height * 0.4304508)
    ..lineTo(size.width * 0.4715800, size.height * 0.4040008)
    ..cubicTo(
        size.width * 0.4959800,
        size.height * 0.3894548,
        size.width * 0.5169150,
        size.height * 0.3769896,
        size.width * 0.5181000,
        size.height * 0.3762943)
    ..cubicTo(
        size.width * 0.5197000,
        size.height * 0.3753656,
        size.width * 0.5235925,
        size.height * 0.3702405,
        size.width * 0.5331775,
        size.height * 0.3564394)
    ..cubicTo(
        size.width * 0.5402850,
        size.height * 0.3462090,
        size.width * 0.5461275,
        size.height * 0.3378360,
        size.width * 0.5461650,
        size.height * 0.3378360)
    ..cubicTo(
        size.width * 0.5462000,
        size.height * 0.3378360,
        size.width * 0.5479950,
        size.height * 0.3391421,
        size.width * 0.5501550,
        size.height * 0.3407363)
    ..moveTo(size.width * 0.5580925, size.height * 0.3533802)
    ..cubicTo(
        size.width * 0.5576400,
        size.height * 0.3647578,
        size.width * 0.5586375,
        size.height * 0.3626621,
        size.width * 0.5431675,
        size.height * 0.3847319)
    ..lineTo(size.width * 0.5291475, size.height * 0.4047308)
    ..lineTo(size.width * 0.5249900, size.height * 0.4006685)
    ..cubicTo(
        size.width * 0.5207375,
        size.height * 0.3965117,
        size.width * 0.5202775,
        size.height * 0.3957569,
        size.width * 0.5215775,
        size.height * 0.3950815)
    ..cubicTo(
        size.width * 0.5219875,
        size.height * 0.3948679,
        size.width * 0.5300200,
        size.height * 0.3836443,
        size.width * 0.5394300,
        size.height * 0.3701412)
    ..cubicTo(
        size.width * 0.5591325,
        size.height * 0.3418686,
        size.width * 0.5585275,
        size.height * 0.3424149,
        size.width * 0.5580925,
        size.height * 0.3533802)
    ..moveTo(size.width * 0.5726125, size.height * 0.3633772)
    ..cubicTo(
        size.width * 0.5742400,
        size.height * 0.3655127,
        size.width * 0.5767750,
        size.height * 0.3735281,
        size.width * 0.5763050,
        size.height * 0.3750428)
    ..cubicTo(
        size.width * 0.5761925,
        size.height * 0.3754004,
        size.width * 0.5735050,
        size.height * 0.3726044,
        size.width * 0.5703325,
        size.height * 0.3688351)
    ..cubicTo(
        size.width * 0.5640575,
        size.height * 0.3613709,
        size.width * 0.5638425,
        size.height * 0.3604372,
        size.width * 0.5685200,
        size.height * 0.3609140)
    ..cubicTo(
        size.width * 0.5702600,
        size.height * 0.3610928,
        size.width * 0.5713925,
        size.height * 0.3617731,
        size.width * 0.5726125,
        size.height * 0.3633772)
    ..moveTo(size.width * 0.5732100, size.height * 0.3759914)
    ..cubicTo(
        size.width * 0.5874800,
        size.height * 0.3935618,
        size.width * 0.5926975,
        size.height * 0.4035389,
        size.width * 0.5952600,
        size.height * 0.4181594)
    ..cubicTo(
        size.width * 0.5977000,
        size.height * 0.4320896,
        size.width * 0.5967550,
        size.height * 0.4436063,
        size.width * 0.5924525,
        size.height * 0.4523617)
    ..cubicTo(
        size.width * 0.5893900,
        size.height * 0.4585893,
        size.width * 0.5895275,
        size.height * 0.4587879,
        size.width * 0.5812400,
        size.height * 0.4360676)
    ..cubicTo(
        size.width * 0.5735600,
        size.height * 0.4150059,
        size.width * 0.5740100,
        size.height * 0.4165851,
        size.width * 0.5755750,
        size.height * 0.4162276)
    ..cubicTo(
        size.width * 0.5762700,
        size.height * 0.4160687,
        size.width * 0.5767425,
        size.height * 0.4154479,
        size.width * 0.5767425,
        size.height * 0.4146930)
    ..cubicTo(
        size.width * 0.5767425,
        size.height * 0.4133223,
        size.width * 0.5748075,
        size.height * 0.4127214,
        size.width * 0.5670400,
        size.height * 0.4116984)
    ..cubicTo(
        size.width * 0.5633950,
        size.height * 0.4112216,
        size.width * 0.5629000,
        size.height * 0.4109733,
        size.width * 0.5631375,
        size.height * 0.4097566)
    ..cubicTo(
        size.width * 0.5652225,
        size.height * 0.3990693,
        size.width * 0.5665300,
        size.height * 0.3833513,
        size.width * 0.5661225,
        size.height * 0.3738807)
    ..lineTo(size.width * 0.5658375, size.height * 0.3672707)
    ..lineTo(size.width * 0.5668250, size.height * 0.3683087)
    ..cubicTo(
        size.width * 0.5673700,
        size.height * 0.3688798,
        size.width * 0.5702425,
        size.height * 0.3723363,
        size.width * 0.5732100,
        size.height * 0.3759914)
    ..moveTo(size.width * 0.5564875, size.height * 0.3722518)
    ..arcToPoint(Offset(size.width * 0.5534850, size.height * 0.3899712),
        radius:
            Radius.elliptical(size.width * 0.06920500, size.height * 0.1374745),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..lineTo(size.width * 0.5522250, size.height * 0.3960697)
    ..lineTo(size.width * 0.5461250, size.height * 0.4045371)
    ..cubicTo(
        size.width * 0.5416350,
        size.height * 0.4107697,
        size.width * 0.5396825,
        size.height * 0.4130045,
        size.width * 0.5387275,
        size.height * 0.4130045)
    ..cubicTo(
        size.width * 0.5375325,
        size.height * 0.4130045,
        size.width * 0.5310250,
        size.height * 0.4073033,
        size.width * 0.5310475,
        size.height * 0.4062753)
    ..cubicTo(
        size.width * 0.5310525,
        size.height * 0.4059326,
        size.width * 0.5565075,
        size.height * 0.3692522,
        size.width * 0.5568325,
        size.height * 0.3691182)
    ..cubicTo(
        size.width * 0.5568725,
        size.height * 0.3690983,
        size.width * 0.5567175,
        size.height * 0.3705137,
        size.width * 0.5564875,
        size.height * 0.3722518)
    ..moveTo(size.width * 0.5204075, size.height * 0.3963230)
    ..cubicTo(
        size.width * 0.5204100,
        size.height * 0.3964422,
        size.width * 0.5206925,
        size.height * 0.3997398,
        size.width * 0.5210400,
        size.height * 0.4036581)
    ..cubicTo(
        size.width * 0.5218000,
        size.height * 0.4122298,
        size.width * 0.5220325,
        size.height * 0.4116885,
        size.width * 0.5181425,
        size.height * 0.4104419)
    ..cubicTo(
        size.width * 0.5101150,
        size.height * 0.4078694,
        size.width * 0.5038000,
        size.height * 0.4108343,
        size.width * 0.5017300,
        size.height * 0.4181545)
    ..cubicTo(
        size.width * 0.5012125,
        size.height * 0.4199771,
        size.width * 0.5002475,
        size.height * 0.4220281,
        size.width * 0.4995825,
        size.height * 0.4227085)
    ..cubicTo(
        size.width * 0.4989175,
        size.height * 0.4233938,
        size.width * 0.4979050,
        size.height * 0.4251419,
        size.width * 0.4973300,
        size.height * 0.4265970)
    ..cubicTo(
        size.width * 0.4963400,
        size.height * 0.4291149,
        size.width * 0.4948150,
        size.height * 0.4301230,
        size.width * 0.4665750,
        size.height * 0.4468989)
    ..arcToPoint(Offset(size.width * 0.4361900, size.height * 0.4649709),
        radius:
            Radius.elliptical(size.width * 19.99360, size.height * 39.71693),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.4356650,
        size.height * 0.4652986,
        size.width * 0.4352075,
        size.height * 0.4636101,
        size.width * 0.4341100,
        size.height * 0.4572931)
    ..lineTo(size.width * 0.4327050, size.height * 0.4491883)
    ..lineTo(size.width * 0.4315050, size.height * 0.4532655)
    ..cubicTo(
        size.width * 0.4308450,
        size.height * 0.4555103,
        size.width * 0.4299800,
        size.height * 0.4587780,
        size.width * 0.4295825,
        size.height * 0.4605261)
    ..cubicTo(
        size.width * 0.4290925,
        size.height * 0.4626914,
        size.width * 0.4286450,
        size.height * 0.4635903,
        size.width * 0.4281850,
        size.height * 0.4633320)
    ..cubicTo(
        size.width * 0.4278125,
        size.height * 0.4631284,
        size.width * 0.4194725,
        size.height * 0.4590661,
        size.width * 0.4096525,
        size.height * 0.4543134)
    ..cubicTo(
        size.width * 0.3998300,
        size.height * 0.4495558,
        size.width * 0.3916225,
        size.height * 0.4453693,
        size.width * 0.3914100,
        size.height * 0.4450018)
    ..cubicTo(
        size.width * 0.3909800,
        size.height * 0.4442568,
        size.width * 0.3950625,
        size.height * 0.4302621,
        size.width * 0.3956650,
        size.height * 0.4304160)
    ..cubicTo(
        size.width * 0.3958750,
        size.height * 0.4304706,
        size.width * 0.4038125,
        size.height * 0.4347863,
        size.width * 0.4133050,
        size.height * 0.4400008)
    ..lineTo(size.width * 0.4305675, size.height * 0.4494862)
    ..lineTo(size.width * 0.4751675, size.height * 0.4228475)
    ..cubicTo(
        size.width * 0.5181925,
        size.height * 0.3971573,
        size.width * 0.5204075,
        size.height * 0.3958612,
        size.width * 0.5204075,
        size.height * 0.3963230)
    ..moveTo(size.width * 0.3596900, size.height * 0.4228128)
    ..cubicTo(
        size.width * 0.3604225,
        size.height * 0.4269446,
        size.width * 0.3612800,
        size.height * 0.4324919,
        size.width * 0.3615925,
        size.height * 0.4351438)
    ..cubicTo(
        size.width * 0.3622225,
        size.height * 0.4404825,
        size.width * 0.3623550,
        size.height * 0.4401150,
        size.width * 0.3593950,
        size.height * 0.4412970)
    ..cubicTo(
        size.width * 0.3544600,
        size.height * 0.4432636,
        size.width * 0.3547675,
        size.height * 0.4508669,
        size.width * 0.3599300,
        size.height * 0.4545220)
    ..lineTo(size.width * 0.3627250, size.height * 0.4565035)
    ..lineTo(size.width * 0.3624400, size.height * 0.4614101)
    ..cubicTo(
        size.width * 0.3622825,
        size.height * 0.4641117,
        size.width * 0.3617450,
        size.height * 0.4703840,
        size.width * 0.3612425,
        size.height * 0.4753453)
    ..lineTo(size.width * 0.3603325, size.height * 0.4843738)
    ..lineTo(size.width * 0.3573550, size.height * 0.4841504)
    ..cubicTo(
        size.width * 0.3525225,
        size.height * 0.4837829,
        size.width * 0.3508600,
        size.height * 0.4860872,
        size.width * 0.3521400,
        size.height * 0.4913812)
    ..cubicTo(
        size.width * 0.3526050,
        size.height * 0.4932981,
        size.width * 0.3563975,
        size.height * 0.4993668,
        size.width * 0.3576525,
        size.height * 0.5001962)
    ..cubicTo(
        size.width * 0.3580850,
        size.height * 0.5004792,
        size.width * 0.3558400,
        size.height * 0.5129096,
        size.width * 0.3542075,
        size.height * 0.5192664)
    ..cubicTo(
        size.width * 0.3537175,
        size.height * 0.5211734,
        size.width * 0.3533175,
        size.height * 0.5229265,
        size.width * 0.3533175,
        size.height * 0.5231649)
    ..cubicTo(
        size.width * 0.3533175,
        size.height * 0.5233983,
        size.width * 0.3548825,
        size.height * 0.5222809,
        size.width * 0.3567975,
        size.height * 0.5206768)
    ..lineTo(size.width * 0.3602775, size.height * 0.5177616)
    ..lineTo(size.width * 0.3622550, size.height * 0.5202795)
    ..cubicTo(
        size.width * 0.3665475,
        size.height * 0.5257523,
        size.width * 0.3692350,
        size.height * 0.5211585,
        size.width * 0.3670200,
        size.height * 0.5121349)
    ..lineTo(size.width * 0.3658600, size.height * 0.5074021)
    ..lineTo(size.width * 0.3684100, size.height * 0.5028034)
    ..cubicTo(
        size.width * 0.3713550,
        size.height * 0.4974896,
        size.width * 0.3791150,
        size.height * 0.4856402,
        size.width * 0.3796475,
        size.height * 0.4856402)
    ..cubicTo(
        size.width * 0.3800775,
        size.height * 0.4856402,
        size.width * 0.3818025,
        size.height * 0.4979266,
        size.width * 0.3818025,
        size.height * 0.5009858)
    ..cubicTo(
        size.width * 0.3818025,
        size.height * 0.5053759,
        size.width * 0.3809650,
        size.height * 0.5083060,
        size.width * 0.3795375,
        size.height * 0.5089069)
    ..cubicTo(
        size.width * 0.3764625,
        size.height * 0.5102031,
        size.width * 0.3756050,
        size.height * 0.5160731,
        size.width * 0.3779800,
        size.height * 0.5195644)
    ..cubicTo(
        size.width * 0.3787500,
        size.height * 0.5206967,
        size.width * 0.3791750,
        size.height * 0.5219581,
        size.width * 0.3790500,
        size.height * 0.5227527)
    ..cubicTo(
        size.width * 0.3789375,
        size.height * 0.5234777,
        size.width * 0.3786675,
        size.height * 0.5266015,
        size.width * 0.3784475,
        size.height * 0.5296954)
    ..cubicTo(
        size.width * 0.3780325,
        size.height * 0.5355903,
        size.width * 0.3765575,
        size.height * 0.5411773,
        size.width * 0.3737375,
        size.height * 0.5475589)
    ..lineTo(size.width * 0.3723650, size.height * 0.5506627)
    ..lineTo(size.width * 0.3701300, size.height * 0.5468686)
    ..cubicTo(
        size.width * 0.3654825,
        size.height * 0.5389822,
        size.width * 0.3622525,
        size.height * 0.5443060,
        size.width * 0.3652125,
        size.height * 0.5549734)
    ..cubicTo(
        size.width * 0.3673300,
        size.height * 0.5626015,
        size.width * 0.3657275,
        size.height * 0.5650349,
        size.width * 0.3585725,
        size.height * 0.5650349)
    ..lineTo(size.width * 0.3540350, size.height * 0.5650349)
    ..lineTo(size.width * 0.3582550, size.height * 0.5563739)
    ..cubicTo(
        size.width * 0.3663100,
        size.height * 0.5398563,
        size.width * 0.3733775,
        size.height * 0.5185811,
        size.width * 0.3708100,
        size.height * 0.5185811)
    ..cubicTo(
        size.width * 0.3705950,
        size.height * 0.5185811,
        size.width * 0.3696400,
        size.height * 0.5211883,
        size.width * 0.3686850,
        size.height * 0.5243816)
    ..cubicTo(
        size.width * 0.3648575,
        size.height * 0.5371845,
        size.width * 0.3588550,
        size.height * 0.5518099,
        size.width * 0.3540650,
        size.height * 0.5600240)
    ..lineTo(size.width * 0.3511425, size.height * 0.5650349)
    ..lineTo(size.width * 0.3404275, size.height * 0.5650349)
    ..lineTo(size.width * 0.3406875, size.height * 0.5487756)
    ..cubicTo(
        size.width * 0.3408300,
        size.height * 0.5398314,
        size.width * 0.3411500,
        size.height * 0.5294769,
        size.width * 0.3414000,
        size.height * 0.5257572)
    ..cubicTo(
        size.width * 0.3416475,
        size.height * 0.5220425,
        size.width * 0.3419325,
        size.height * 0.5176722,
        size.width * 0.3420300,
        size.height * 0.5160483)
    ..cubicTo(
        size.width * 0.3426950,
        size.height * 0.5050879,
        size.width * 0.3438450,
        size.height * 0.4950164,
        size.width * 0.3461100,
        size.height * 0.4803413)
    ..cubicTo(
        size.width * 0.3471875,
        size.height * 0.4733638,
        size.width * 0.3473975,
        size.height * 0.4710098,
        size.width * 0.3470150,
        size.height * 0.4702499)
    ..cubicTo(
        size.width * 0.3446650,
        size.height * 0.4655817,
        size.width * 0.3395625,
        size.height * 0.5188741,
        size.width * 0.3391100,
        size.height * 0.5527883)
    ..lineTo(size.width * 0.3389400, size.height * 0.5654571)
    ..lineTo(size.width * 0.3341150, size.height * 0.5656905)
    ..lineTo(size.width * 0.3292875, size.height * 0.5659289)
    ..lineTo(size.width * 0.3269700, size.height * 0.5542881)
    ..cubicTo(
        size.width * 0.3227900,
        size.height * 0.5333009,
        size.width * 0.3135900,
        size.height * 0.4991533,
        size.width * 0.3121150,
        size.height * 0.4991533)
    ..cubicTo(
        size.width * 0.3109775,
        size.height * 0.4991533,
        size.width * 0.3114200,
        size.height * 0.5016115,
        size.width * 0.3140525,
        size.height * 0.5099200)
    ..cubicTo(
        size.width * 0.3182175,
        size.height * 0.5230606,
        size.width * 0.3273800,
        size.height * 0.5612060,
        size.width * 0.3273800,
        size.height * 0.5654074)
    ..cubicTo(
        size.width * 0.3273800,
        size.height * 0.5656656,
        size.width * 0.3245300,
        size.height * 0.5658792,
        size.width * 0.3210425,
        size.height * 0.5658792)
    ..lineTo(size.width * 0.3147050, size.height * 0.5658792)
    ..lineTo(size.width * 0.3128375, size.height * 0.5617523)
    ..arcToPoint(Offset(size.width * 0.3093325, size.height * 0.5529025),
        radius:
            Radius.elliptical(size.width * 0.05310750, size.height * 0.1054971),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..lineTo(size.width * 0.3076950, size.height * 0.5481796)
    ..lineTo(size.width * 0.3094600, size.height * 0.5443805)
    ..cubicTo(
        size.width * 0.3126850,
        size.height * 0.5374328,
        size.width * 0.3116175,
        size.height * 0.5329384,
        size.width * 0.3067450,
        size.height * 0.5329384)
    ..lineTo(size.width * 0.3035875, size.height * 0.5329384)
    ..lineTo(size.width * 0.3027675, size.height * 0.5297401)
    ..cubicTo(
        size.width * 0.3019325,
        size.height * 0.5264823,
        size.width * 0.2987850,
        size.height * 0.5091353,
        size.width * 0.2989925,
        size.height * 0.5089218)
    ..cubicTo(
        size.width * 0.3060825,
        size.height * 0.5016860,
        size.width * 0.3066400,
        size.height * 0.4923992,
        size.width * 0.2999850,
        size.height * 0.4923992)
    ..lineTo(size.width * 0.2976200, size.height * 0.4923992)
    ..lineTo(size.width * 0.2976200, size.height * 0.4750821)
    ..cubicTo(
        size.width * 0.2976200,
        size.height * 0.4655619,
        size.width * 0.2976825,
        size.height * 0.4577699,
        size.width * 0.2977600,
        size.height * 0.4577699)
    ..cubicTo(
        size.width * 0.2978375,
        size.height * 0.4577699,
        size.width * 0.2990325,
        size.height * 0.4586886,
        size.width * 0.3004175,
        size.height * 0.4598110)
    ..lineTo(size.width * 0.3029325, size.height * 0.4618521)
    ..lineTo(size.width * 0.3027950, size.height * 0.4652341)
    ..cubicTo(
        size.width * 0.3025275,
        size.height * 0.4716653,
        size.width * 0.3048275,
        size.height * 0.4737958,
        size.width * 0.3080225,
        size.height * 0.4700761)
    ..lineTo(size.width * 0.3097825, size.height * 0.4680251)
    ..lineTo(size.width * 0.3117875, size.height * 0.4703493)
    ..cubicTo(
        size.width * 0.3128875,
        size.height * 0.4716306,
        size.width * 0.3148100,
        size.height * 0.4744216,
        size.width * 0.3160550,
        size.height * 0.4765570)
    ..lineTo(size.width * 0.3183175, size.height * 0.4804406)
    ..lineTo(size.width * 0.3178825, size.height * 0.4846817)
    ..cubicTo(
        size.width * 0.3171100,
        size.height * 0.4921708,
        size.width * 0.3189925,
        size.height * 0.4959004,
        size.width * 0.3220725,
        size.height * 0.4929852)
    ..lineTo(size.width * 0.3234750, size.height * 0.4916543)
    ..lineTo(size.width * 0.3249575, size.height * 0.4973058)
    ..cubicTo(
        size.width * 0.3257750,
        size.height * 0.5004147,
        size.width * 0.3267825,
        size.height * 0.5051425,
        size.width * 0.3272000,
        size.height * 0.5078094)
    ..cubicTo(
        size.width * 0.3279800,
        size.height * 0.5127805,
        size.width * 0.3282675,
        size.height * 0.5131927,
        size.width * 0.3302500,
        size.height * 0.5121349)
    ..cubicTo(
        size.width * 0.3309500,
        size.height * 0.5117625,
        size.width * 0.3312075,
        size.height * 0.5109977,
        size.width * 0.3312075,
        size.height * 0.5092992)
    ..cubicTo(
        size.width * 0.3312075,
        size.height * 0.5072134,
        size.width * 0.3313950,
        size.height * 0.5069105,
        size.width * 0.3330100,
        size.height * 0.5063990)
    ..cubicTo(
        size.width * 0.3379250,
        size.height * 0.5048396,
        size.width * 0.3378525,
        size.height * 0.4966503,
        size.width * 0.3328850,
        size.height * 0.4927022)
    ..lineTo(size.width * 0.3311100, size.height * 0.4912918)
    ..lineTo(size.width * 0.3313900, size.height * 0.4852976)
    ..cubicTo(
        size.width * 0.3315425,
        size.height * 0.4820050,
        size.width * 0.3317825,
        size.height * 0.4774708,
        size.width * 0.3319225,
        size.height * 0.4752311)
    ..lineTo(size.width * 0.3321750, size.height * 0.4711588)
    ..lineTo(size.width * 0.3343500, size.height * 0.4716852)
    ..cubicTo(
        size.width * 0.3355450,
        size.height * 0.4719782,
        size.width * 0.3370425,
        size.height * 0.4724152,
        size.width * 0.3376800,
        size.height * 0.4726586)
    ..cubicTo(
        size.width * 0.3385400,
        size.height * 0.4729814,
        size.width * 0.3391125,
        size.height * 0.4725493,
        size.width * 0.3399125,
        size.height * 0.4709601)
    ..cubicTo(
        size.width * 0.3417075,
        size.height * 0.4673944,
        size.width * 0.3413625,
        size.height * 0.4656115,
        size.width * 0.3375075,
        size.height * 0.4585496)
    ..lineTo(size.width * 0.3340150, size.height * 0.4521481)
    ..lineTo(size.width * 0.3350000, size.height * 0.4467250)
    ..cubicTo(
        size.width * 0.3364025,
        size.height * 0.4389778,
        size.width * 0.3390450,
        size.height * 0.4292291,
        size.width * 0.3408900,
        size.height * 0.4249929)
    ..lineTo(size.width * 0.3424775, size.height * 0.4213577)
    ..lineTo(size.width * 0.3453775, size.height * 0.4243622)
    ..cubicTo(
        size.width * 0.3512225,
        size.height * 0.4304160,
        size.width * 0.3540750,
        size.height * 0.4253108,
        size.width * 0.3503500,
        size.height * 0.4154628)
    ..cubicTo(
        size.width * 0.3494200,
        size.height * 0.4130045,
        size.width * 0.3487025,
        size.height * 0.4109137,
        size.width * 0.3487550,
        size.height * 0.4108194)
    ..cubicTo(
        size.width * 0.3488100,
        size.height * 0.4107250,
        size.width * 0.3502875,
        size.height * 0.4086839,
        size.width * 0.3520400,
        size.height * 0.4062803)
    ..lineTo(size.width * 0.3552300, size.height * 0.4019199)
    ..lineTo(size.width * 0.3567925, size.height * 0.4086144)
    ..cubicTo(
        size.width * 0.3576500,
        size.height * 0.4122943,
        size.width * 0.3589550,
        size.height * 0.4186858,
        size.width * 0.3596900,
        size.height * 0.4228128)
    ..moveTo(size.width * 0.6481550, size.height * 0.4066080)
    ..lineTo(size.width * 0.6510875, size.height * 0.4105959)
    ..cubicTo(
        size.width * 0.6513050,
        size.height * 0.4108988,
        size.width * 0.6506900,
        size.height * 0.4130144,
        size.width * 0.6497225,
        size.height * 0.4152989)
    ..cubicTo(
        size.width * 0.6456925,
        size.height * 0.4247843,
        size.width * 0.6487525,
        size.height * 0.4304409,
        size.width * 0.6546225,
        size.height * 0.4243622)
    ..lineTo(size.width * 0.6575225, size.height * 0.4213577)
    ..lineTo(size.width * 0.6591100, size.height * 0.4249929)
    ..cubicTo(
        size.width * 0.6609450,
        size.height * 0.4291993,
        size.width * 0.6640925,
        size.height * 0.4408997,
        size.width * 0.6651900,
        size.height * 0.4476041)
    ..lineTo(size.width * 0.6659450, size.height * 0.4522177)
    ..lineTo(size.width * 0.6624725, size.height * 0.4585843)
    ..cubicTo(
        size.width * 0.6587725,
        size.height * 0.4653682,
        size.width * 0.6583975,
        size.height * 0.4670517,
        size.width * 0.6598175,
        size.height * 0.4705330)
    ..cubicTo(
        size.width * 0.6606550,
        size.height * 0.4725890,
        size.width * 0.6624875,
        size.height * 0.4729118,
        size.width * 0.6662900,
        size.height * 0.4716752)
    ..lineTo(size.width * 0.6678250, size.height * 0.4711737)
    ..lineTo(size.width * 0.6680775, size.height * 0.4752410)
    ..cubicTo(
        size.width * 0.6682175,
        size.height * 0.4774758,
        size.width * 0.6684550,
        size.height * 0.4819851,
        size.width * 0.6686075,
        size.height * 0.4852578)
    ..lineTo(size.width * 0.6688850, size.height * 0.4912123)
    ..lineTo(size.width * 0.6670800, size.height * 0.4928313)
    ..cubicTo(
        size.width * 0.6620925,
        size.height * 0.4973009,
        size.width * 0.6619850,
        size.height * 0.5049190,
        size.width * 0.6668900,
        size.height * 0.5063791)
    ..cubicTo(
        size.width * 0.6686175,
        size.height * 0.5068956,
        size.width * 0.6687925,
        size.height * 0.5071638,
        size.width * 0.6687925,
        size.height * 0.5092843)
    ..cubicTo(
        size.width * 0.6687925,
        size.height * 0.5109977,
        size.width * 0.6690500,
        size.height * 0.5117625,
        size.width * 0.6697500,
        size.height * 0.5121349)
    ..cubicTo(
        size.width * 0.6717325,
        size.height * 0.5131927,
        size.width * 0.6720200,
        size.height * 0.5127805,
        size.width * 0.6728000,
        size.height * 0.5078094)
    ..cubicTo(
        size.width * 0.6732175,
        size.height * 0.5051425,
        size.width * 0.6742250,
        size.height * 0.5004147,
        size.width * 0.6750425,
        size.height * 0.4973058)
    ..lineTo(size.width * 0.6765250, size.height * 0.4916543)
    ..lineTo(size.width * 0.6779275, size.height * 0.4929852)
    ..cubicTo(
        size.width * 0.6810000,
        size.height * 0.4958954,
        size.width * 0.6828900,
        size.height * 0.4921708,
        size.width * 0.6821225,
        size.height * 0.4847215)
    ..lineTo(size.width * 0.6816900, size.height * 0.4805151)
    ..lineTo(size.width * 0.6833600, size.height * 0.4775205)
    ..cubicTo(
        size.width * 0.6842775,
        size.height * 0.4758717,
        size.width * 0.6861825,
        size.height * 0.4730410,
        size.width * 0.6875900,
        size.height * 0.4712333)
    ..lineTo(size.width * 0.6901500, size.height * 0.4679456)
    ..lineTo(size.width * 0.6919450, size.height * 0.4700364)
    ..cubicTo(
        size.width * 0.6951800,
        size.height * 0.4738058,
        size.width * 0.6975850,
        size.height * 0.4716107,
        size.width * 0.6971575,
        size.height * 0.4652738)
    ..cubicTo(
        size.width * 0.6969400,
        size.height * 0.4620855,
        size.width * 0.6969850,
        size.height * 0.4619564,
        size.width * 0.6987700,
        size.height * 0.4603871)
    ..cubicTo(
        size.width * 0.7027625,
        size.height * 0.4568809,
        size.width * 0.7023800,
        size.height * 0.4553364,
        size.width * 0.7023800,
        size.height * 0.4750274)
    ..lineTo(size.width * 0.7023800, size.height * 0.4923992)
    ..lineTo(size.width * 0.7000150, size.height * 0.4923992)
    ..cubicTo(
        size.width * 0.6934450,
        size.height * 0.4923992,
        size.width * 0.6940450,
        size.height * 0.5024459,
        size.width * 0.7009975,
        size.height * 0.5089019)
    ..cubicTo(
        size.width * 0.7011900,
        size.height * 0.5090807,
        size.width * 0.6986675,
        size.height * 0.5231897,
        size.width * 0.6975675,
        size.height * 0.5280814)
    ..lineTo(size.width * 0.6964750, size.height * 0.5329384)
    ..lineTo(size.width * 0.6932725, size.height * 0.5329384)
    ..cubicTo(
        size.width * 0.6883375,
        size.height * 0.5329384,
        size.width * 0.6873250,
        size.height * 0.5378450,
        size.width * 0.6907950,
        size.height * 0.5449566)
    ..lineTo(size.width * 0.6923300, size.height * 0.5481051)
    ..lineTo(size.width * 0.6906800, size.height * 0.5528677)
    ..arcToPoint(Offset(size.width * 0.6871625, size.height * 0.5617523),
        radius:
            Radius.elliptical(size.width * 0.05332500, size.height * 0.1059292),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..lineTo(size.width * 0.6852950, size.height * 0.5658792)
    ..lineTo(size.width * 0.6785325, size.height * 0.5658792)
    ..cubicTo(
        size.width * 0.6748125,
        size.height * 0.5658792,
        size.width * 0.6717675,
        size.height * 0.5656656,
        size.width * 0.6717675,
        size.height * 0.5654074)
    ..cubicTo(
        size.width * 0.6717675,
        size.height * 0.5611365,
        size.width * 0.6814050,
        size.height * 0.5211089,
        size.width * 0.6853300,
        size.height * 0.5090708)
    ..cubicTo(
        size.width * 0.6877550,
        size.height * 0.5016364,
        size.width * 0.6881825,
        size.height * 0.4991533,
        size.width * 0.6870425,
        size.height * 0.4991533)
    ..cubicTo(
        size.width * 0.6855900,
        size.height * 0.4991533,
        size.width * 0.6772525,
        size.height * 0.5297203,
        size.width * 0.6726400,
        size.height * 0.5519391)
    ..lineTo(size.width * 0.6698375, size.height * 0.5654571)
    ..lineTo(size.width * 0.6658075, size.height * 0.5657004)
    ..cubicTo(
        size.width * 0.6596050,
        size.height * 0.5660828,
        size.width * 0.6600775,
        size.height * 0.5670959,
        size.width * 0.6600775,
        size.height * 0.5534587)
    ..cubicTo(
        size.width * 0.6600775,
        size.height * 0.5363402,
        size.width * 0.6596175,
        size.height * 0.5264128,
        size.width * 0.6579600,
        size.height * 0.5076008)
    ..cubicTo(
        size.width * 0.6563925,
        size.height * 0.4898267,
        size.width * 0.6534075,
        size.height * 0.4692120,
        size.width * 0.6524925,
        size.height * 0.4698427)
    ..cubicTo(
        size.width * 0.6517525,
        size.height * 0.4703493,
        size.width * 0.6519475,
        size.height * 0.4731154,
        size.width * 0.6536050,
        size.height * 0.4856402)
    ..cubicTo(
        size.width * 0.6563675,
        size.height * 0.5064933,
        size.width * 0.6578650,
        size.height * 0.5271279,
        size.width * 0.6584025,
        size.height * 0.5517305)
    ..lineTo(size.width * 0.6586925, size.height * 0.5650349)
    ..lineTo(size.width * 0.6476025, size.height * 0.5650349)
    ..lineTo(size.width * 0.6431675, size.height * 0.5563739)
    ..cubicTo(
        size.width * 0.6387100,
        size.height * 0.5476781,
        size.width * 0.6339175,
        size.height * 0.5356747,
        size.width * 0.6304700,
        size.height * 0.5245703)
    ..cubicTo(
        size.width * 0.6286900,
        size.height * 0.5188393,
        size.width * 0.6279050,
        size.height * 0.5176871,
        size.width * 0.6276400,
        size.height * 0.5204037)
    ..cubicTo(
        size.width * 0.6275175,
        size.height * 0.5216502,
        size.width * 0.6315700,
        size.height * 0.5345673,
        size.width * 0.6340175,
        size.height * 0.5407403)
    ..lineTo(size.width * 0.6352700, size.height * 0.5438988)
    ..lineTo(size.width * 0.6340125, size.height * 0.5434021)
    ..cubicTo(
        size.width * 0.6325525,
        size.height * 0.5428211,
        size.width * 0.6314900,
        size.height * 0.5437994,
        size.width * 0.6292825,
        size.height * 0.5477625)
    ..lineTo(size.width * 0.6276875, size.height * 0.5506280)
    ..lineTo(size.width * 0.6266100, size.height * 0.5483286)
    ..cubicTo(
        size.width * 0.6242950,
        size.height * 0.5433922,
        size.width * 0.6190550,
        size.height * 0.5259161,
        size.width * 0.6190500,
        size.height * 0.5231053)
    ..cubicTo(
        size.width * 0.6190475,
        size.height * 0.5225739,
        size.width * 0.6194950,
        size.height * 0.5219183,
        size.width * 0.6200425,
        size.height * 0.5216452)
    ..cubicTo(
        size.width * 0.6246325,
        size.height * 0.5193558,
        size.width * 0.6244175,
        size.height * 0.5102031,
        size.width * 0.6197400,
        size.height * 0.5088324)
    ..lineTo(size.width * 0.6176675, size.height * 0.5082216)
    ..lineTo(size.width * 0.6179450, size.height * 0.5034788)
    ..cubicTo(
        size.width * 0.6183175,
        size.height * 0.4970724,
        size.width * 0.6198925,
        size.height * 0.4856402,
        size.width * 0.6204025,
        size.height * 0.4856402)
    ..cubicTo(
        size.width * 0.6209650,
        size.height * 0.4856402,
        size.width * 0.6284775,
        size.height * 0.4971469,
        size.width * 0.6315200,
        size.height * 0.5026743)
    ..lineTo(size.width * 0.6340025, size.height * 0.5071786)
    ..lineTo(size.width * 0.6329100, size.height * 0.5118221)
    ..cubicTo(
        size.width * 0.6307375,
        size.height * 0.5210642,
        size.width * 0.6334375,
        size.height * 0.5257721,
        size.width * 0.6377550,
        size.height * 0.5202696)
    ..lineTo(size.width * 0.6397425, size.height * 0.5177368)
    ..lineTo(size.width * 0.6430975, size.height * 0.5206371)
    ..cubicTo(
        size.width * 0.6471750,
        size.height * 0.5241581,
        size.width * 0.6470175,
        size.height * 0.5244809,
        size.width * 0.6451475,
        size.height * 0.5164357)
    ..cubicTo(
        size.width * 0.6436575,
        size.height * 0.5100342,
        size.width * 0.6418900,
        size.height * 0.5007127,
        size.width * 0.6421125,
        size.height * 0.5004296)
    ..cubicTo(
        size.width * 0.6421700,
        size.height * 0.5003551,
        size.width * 0.6429850,
        size.height * 0.4994065,
        size.width * 0.6439200,
        size.height * 0.4983189)
    ..cubicTo(
        size.width * 0.6503825,
        size.height * 0.4908249,
        size.width * 0.6497325,
        size.height * 0.4836140,
        size.width * 0.6426450,
        size.height * 0.4841504)
    ..lineTo(size.width * 0.6396675, size.height * 0.4843738)
    ..lineTo(size.width * 0.6387575, size.height * 0.4753453)
    ..cubicTo(
        size.width * 0.6382550,
        size.height * 0.4703840,
        size.width * 0.6377175,
        size.height * 0.4641117,
        size.width * 0.6375600,
        size.height * 0.4614101)
    ..lineTo(size.width * 0.6372750, size.height * 0.4565035)
    ..lineTo(size.width * 0.6400700, size.height * 0.4545220)
    ..cubicTo(
        size.width * 0.6429525,
        size.height * 0.4524809,
        size.width * 0.6441325,
        size.height * 0.4503206,
        size.width * 0.6441325,
        size.height * 0.4470876)
    ..cubicTo(
        size.width * 0.6441325,
        size.height * 0.4452550,
        size.width * 0.6423475,
        size.height * 0.4417241,
        size.width * 0.6414175,
        size.height * 0.4417191)
    ..cubicTo(
        size.width * 0.6392775,
        size.height * 0.4416943,
        size.width * 0.6377575,
        size.height * 0.4398816,
        size.width * 0.6380225,
        size.height * 0.4376667)
    ..cubicTo(
        size.width * 0.6394125,
        size.height * 0.4260259,
        size.width * 0.6443450,
        size.height * 0.4016865,
        size.width * 0.6451300,
        size.height * 0.4025953)
    ..cubicTo(
        size.width * 0.6454000,
        size.height * 0.4029082,
        size.width * 0.6467625,
        size.height * 0.4047159,
        size.width * 0.6481550,
        size.height * 0.4066080)
    ..moveTo(size.width * 0.5495100, size.height * 0.4051927)
    ..cubicTo(
        size.width * 0.5465950,
        size.height * 0.4141120,
        size.width * 0.5467300,
        size.height * 0.4138488,
        size.width * 0.5450825,
        size.height * 0.4138488)
    ..cubicTo(
        size.width * 0.5425750,
        size.height * 0.4138488,
        size.width * 0.5427550,
        size.height * 0.4130740,
        size.width * 0.5464625,
        size.height * 0.4078645)
    ..cubicTo(
        size.width * 0.5505350,
        size.height * 0.4021384,
        size.width * 0.5504950,
        size.height * 0.4021732,
        size.width * 0.5495100,
        size.height * 0.4051927)
    ..moveTo(size.width * 0.1438175, size.height * 0.4931541)
    ..cubicTo(
        size.width * 0.1617575,
        size.height * 0.5104017,
        size.width * 0.1775375,
        size.height * 0.5253103,
        size.width * 0.1788850,
        size.height * 0.5262836)
    ..cubicTo(
        size.width * 0.1961975,
        size.height * 0.5388432,
        size.width * 0.2113100,
        size.height * 0.5624525,
        size.width * 0.2113100,
        size.height * 0.5769389)
    ..cubicTo(
        size.width * 0.2113100,
        size.height * 0.5793376,
        size.width * 0.1770700,
        size.height * 0.7549277,
        size.width * 0.1763000,
        size.height * 0.7564821)
    ..cubicTo(
        size.width * 0.1762575,
        size.height * 0.7565616,
        size.width * 0.1750800,
        size.height * 0.7551164,
        size.width * 0.1736775,
        size.height * 0.7532640)
    ..cubicTo(
        size.width * 0.1614050,
        size.height * 0.7370643,
        size.width * 0.1425650,
        size.height * 0.7301712,
        size.width * 0.1280250,
        size.height * 0.7365677)
    ..cubicTo(
        size.width * 0.1270000,
        size.height * 0.7370196,
        size.width * 0.1260725,
        size.height * 0.7372083,
        size.width * 0.1259650,
        size.height * 0.7369948)
    ..cubicTo(
        size.width * 0.1258550,
        size.height * 0.7367762,
        size.width * 0.1310450,
        size.height * 0.7078431,
        size.width * 0.1375000,
        size.height * 0.6726973)
    ..cubicTo(
        size.width * 0.1439550,
        size.height * 0.6375515,
        size.width * 0.1492350,
        size.height * 0.6083551,
        size.width * 0.1492325,
        size.height * 0.6078188)
    ..cubicTo(
        size.width * 0.1492325,
        size.height * 0.6072427,
        size.width * 0.1279400,
        size.height * 0.5949017,
        size.width * 0.09746750,
        size.height * 0.5778130)
    ..cubicTo(
        size.width * 0.04117750,
        size.height * 0.5462528,
        size.width * 0.04162250,
        size.height * 0.5465458,
        size.width * 0.03741000,
        size.height * 0.5380784)
    ..cubicTo(
        size.width * 0.02410000,
        size.height * 0.5113354,
        size.width * 0.02997750,
        size.height * 0.4622395,
        size.width * 0.05240250,
        size.height * 0.4128357)
    ..lineTo(size.width * 0.05535750, size.height * 0.4063299)
    ..lineTo(size.width * 0.08327750, size.height * 0.4340662)
    ..arcToPoint(Offset(size.width * 0.1438175, size.height * 0.4931541),
        radius:
            Radius.elliptical(size.width * 6.279275, size.height * 12.47367),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..moveTo(size.width * 0.5184950, size.height * 0.4137842)
    ..cubicTo(
        size.width * 0.5225250,
        size.height * 0.4150605,
        size.width * 0.5286425,
        size.height * 0.4156962,
        size.width * 0.5476200,
        size.height * 0.4168086)
    ..cubicTo(
        size.width * 0.5608300,
        size.height * 0.4175833,
        size.width * 0.5719775,
        size.height * 0.4184673,
        size.width * 0.5723900,
        size.height * 0.4187802)
    ..cubicTo(
        size.width * 0.5728025,
        size.height * 0.4190881,
        size.width * 0.5763925,
        size.height * 0.4282607,
        size.width * 0.5803675,
        size.height * 0.4391615)
    ..lineTo(size.width * 0.5875925, size.height * 0.4589816)
    ..lineTo(size.width * 0.5864850, size.height * 0.4601338)
    ..cubicTo(
        size.width * 0.5763950,
        size.height * 0.4706373,
        size.width * 0.5544350,
        size.height * 0.4675632,
        size.width * 0.5327375,
        size.height * 0.4526150)
    ..cubicTo(
        size.width * 0.5224550,
        size.height * 0.4455282,
        size.width * 0.5236000,
        size.height * 0.4455033,
        size.width * 0.5227625,
        size.height * 0.4528235)
    ..cubicTo(
        size.width * 0.5215475,
        size.height * 0.4634264,
        size.width * 0.5196725,
        size.height * 0.4672355,
        size.width * 0.5122100,
        size.height * 0.4742577)
    ..cubicTo(
        size.width * 0.5078625,
        size.height * 0.4783498,
        size.width * 0.5067975,
        size.height * 0.4787571,
        size.width * 0.5057700,
        size.height * 0.4767110)
    ..cubicTo(
        size.width * 0.5044375,
        size.height * 0.4740640,
        size.width * 0.5056400,
        size.height * 0.4709849,
        size.width * 0.5086925,
        size.height * 0.4692219)
    ..cubicTo(
        size.width * 0.5145500,
        size.height * 0.4658400,
        size.width * 0.5166500,
        size.height * 0.4564687,
        size.width * 0.5151175,
        size.height * 0.4405620)
    ..cubicTo(
        size.width * 0.5137425,
        size.height * 0.4262841,
        size.width * 0.5114025,
        size.height * 0.4261451,
        size.width * 0.5052175,
        size.height * 0.4399611)
    ..cubicTo(
        size.width * 0.4976550,
        size.height * 0.4568511,
        size.width * 0.4957450,
        size.height * 0.4552272,
        size.width * 0.4997675,
        size.height * 0.4353177)
    ..cubicTo(
        size.width * 0.5044975,
        size.height * 0.4119070,
        size.width * 0.5062475,
        size.height * 0.4098907,
        size.width * 0.5184950,
        size.height * 0.4137842)
    ..moveTo(size.width * 0.3871950, size.height * 0.4257329)
    ..cubicTo(
        size.width * 0.3914000,
        size.height * 0.4288964,
        size.width * 0.3922850,
        size.height * 0.4338079,
        size.width * 0.3900500,
        size.height * 0.4415800)
    ..cubicTo(
        size.width * 0.3879200,
        size.height * 0.4489847,
        size.width * 0.3878800,
        size.height * 0.4493671,
        size.width * 0.3890200,
        size.height * 0.4514181)
    ..cubicTo(
        size.width * 0.3903175,
        size.height * 0.4537472,
        size.width * 0.3873325,
        size.height * 0.4581771,
        size.width * 0.3848525,
        size.height * 0.4576060)
    ..lineTo(size.width * 0.3837150, size.height * 0.4573477)
    ..lineTo(size.width * 0.3835875, size.height * 0.4446790)
    ..cubicTo(
        size.width * 0.3833825,
        size.height * 0.4244814,
        size.width * 0.3836525,
        size.height * 0.4230660,
        size.width * 0.3871950,
        size.height * 0.4257329)
    ..moveTo(size.width * 0.4957475, size.height * 0.4337384)
    ..cubicTo(
        size.width * 0.4957475,
        size.height * 0.4363854,
        size.width * 0.4940600,
        size.height * 0.4395389,
        size.width * 0.4907525,
        size.height * 0.4430699)
    ..cubicTo(
        size.width * 0.4887075,
        size.height * 0.4452550,
        size.width * 0.4848975,
        size.height * 0.4494515,
        size.width * 0.4822850,
        size.height * 0.4523964)
    ..arcToPoint(Offset(size.width * 0.4735700, size.height * 0.4616237),
        radius:
            Radius.elliptical(size.width * 0.1991625, size.height * 0.3956327),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..lineTo(size.width * 0.4696000, size.height * 0.4654923)
    ..lineTo(size.width * 0.4653175, size.height * 0.4649361)
    ..cubicTo(
        size.width * 0.4604725,
        size.height * 0.4643104,
        size.width * 0.4589100,
        size.height * 0.4657059,
        size.width * 0.4579350,
        size.height * 0.4715213)
    ..cubicTo(
        size.width * 0.4574850,
        size.height * 0.4742080,
        size.width * 0.4573800,
        size.height * 0.4742974,
        size.width * 0.4483325,
        size.height * 0.4796460)
    ..cubicTo(
        size.width * 0.4433025,
        size.height * 0.4826158,
        size.width * 0.4391200,
        size.height * 0.4848953,
        size.width * 0.4390400,
        size.height * 0.4847115)
    ..cubicTo(
        size.width * 0.4387075,
        size.height * 0.4839368,
        size.width * 0.4361150,
        size.height * 0.4686856,
        size.width * 0.4362700,
        size.height * 0.4684025)
    ..cubicTo(
        size.width * 0.4370900,
        size.height * 0.4668829,
        size.width * 0.4957475,
        size.height * 0.4327005,
        size.width * 0.4957475,
        size.height * 0.4337384)
    ..moveTo(size.width * 0.5129425, size.height * 0.4356007)
    ..cubicTo(
        size.width * 0.5130950,
        size.height * 0.4368771,
        size.width * 0.5133200,
        size.height * 0.4385854,
        size.width * 0.5134425,
        size.height * 0.4393999)
    ..cubicTo(
        size.width * 0.5138800,
        size.height * 0.4423349,
        size.width * 0.5121425,
        size.height * 0.4409493,
        size.width * 0.5111775,
        size.height * 0.4375972)
    ..cubicTo(
        size.width * 0.5101625,
        size.height * 0.4340612,
        size.width * 0.5102600,
        size.height * 0.4332766,
        size.width * 0.5117175,
        size.height * 0.4332766)
    ..cubicTo(
        size.width * 0.5123975,
        size.height * 0.4332766,
        size.width * 0.5127400,
        size.height * 0.4339271,
        size.width * 0.5129425,
        size.height * 0.4356007)
    ..moveTo(size.width * 0.8310475, size.height * 0.4389778)
    ..cubicTo(
        size.width * 0.8309300,
        size.height * 0.4397922,
        size.width * 0.8305100,
        size.height * 0.4437602,
        size.width * 0.8301150,
        size.height * 0.4478027)
    ..cubicTo(
        size.width * 0.8294275,
        size.height * 0.4548497,
        size.width * 0.8293300,
        size.height * 0.4552470,
        size.width * 0.8277000,
        size.height * 0.4573875)
    ..cubicTo(
        size.width * 0.8227050,
        size.height * 0.4639528,
        size.width * 0.8161000,
        size.height * 0.4790699,
        size.width * 0.8138250,
        size.height * 0.4891513)
    ..cubicTo(
        size.width * 0.8084600,
        size.height * 0.5128948,
        size.width * 0.8121775,
        size.height * 0.5574118,
        size.width * 0.8199500,
        size.height * 0.5625071)
    ..cubicTo(
        size.width * 0.8251125,
        size.height * 0.5658891,
        size.width * 0.8295350,
        size.height * 0.5537319,
        size.width * 0.8395525,
        size.height * 0.5086387)
    ..cubicTo(
        size.width * 0.8468650,
        size.height * 0.4757227,
        size.width * 0.8503400,
        size.height * 0.4651894,
        size.width * 0.8503400,
        size.height * 0.4759512)
    ..cubicTo(
        size.width * 0.8503400,
        size.height * 0.4807088,
        size.width * 0.8516325,
        size.height * 0.4965112,
        size.width * 0.8527150,
        size.height * 0.5050084)
    ..cubicTo(
        size.width * 0.8562600,
        size.height * 0.5328043,
        size.width * 0.8636425,
        size.height * 0.5427019,
        size.width * 0.8709425,
        size.height * 0.5294471)
    ..cubicTo(
        size.width * 0.8722400,
        size.height * 0.5270931,
        size.width * 0.8733000,
        size.height * 0.5253649,
        size.width * 0.8733000,
        size.height * 0.5256033)
    ..cubicTo(
        size.width * 0.8733000,
        size.height * 0.5258466,
        size.width * 0.8718875,
        size.height * 0.5554353,
        size.width * 0.8701600,
        size.height * 0.5913558)
    ..cubicTo(
        size.width * 0.8680100,
        size.height * 0.6360715,
        size.width * 0.8668600,
        size.height * 0.6642647,
        size.width * 0.8665100,
        size.height * 0.6807425)
    ..cubicTo(
        size.width * 0.8659225,
        size.height * 0.7084242,
        size.width * 0.8663175,
        size.height * 0.7060851,
        size.width * 0.8611825,
        size.height * 0.7124369)
    ..cubicTo(
        size.width * 0.8523725,
        size.height * 0.7233377,
        size.width * 0.8417025,
        size.height * 0.7281648,
        size.width * 0.8265300,
        size.height * 0.7281201)
    ..cubicTo(
        size.width * 0.8195925,
        size.height * 0.7281052,
        size.width * 0.8089900,
        size.height * 0.7264962,
        size.width * 0.8084875,
        size.height * 0.7253838)
    ..cubicTo(
        size.width * 0.8078775,
        size.height * 0.7240379,
        size.width * 0.8016950,
        size.height * 0.6636489,
        size.width * 0.7997575,
        size.height * 0.6401041)
    ..cubicTo(
        size.width * 0.7922525,
        size.height * 0.5489146,
        size.width * 0.7949400,
        size.height * 0.5154325,
        size.width * 0.8135150,
        size.height * 0.4687502)
    ..cubicTo(
        size.width * 0.8178425,
        size.height * 0.4578692,
        size.width * 0.8261500,
        size.height * 0.4400405,
        size.width * 0.8269150,
        size.height * 0.4399859)
    ..cubicTo(
        size.width * 0.8272875,
        size.height * 0.4399611,
        size.width * 0.8282625,
        size.height * 0.4394346,
        size.width * 0.8290825,
        size.height * 0.4388188)
    ..cubicTo(
        size.width * 0.8311325,
        size.height * 0.4372793,
        size.width * 0.8312950,
        size.height * 0.4372892,
        size.width * 0.8310475,
        size.height * 0.4389778)
    ..moveTo(size.width * 0.5140250, size.height * 0.4461539)
    ..cubicTo(
        size.width * 0.5140200,
        size.height * 0.4476934,
        size.width * 0.5139375,
        size.height * 0.4478126,
        size.width * 0.5136050,
        size.height * 0.4467896)
    ..cubicTo(
        size.width * 0.5130725,
        size.height * 0.4451507,
        size.width * 0.5130725,
        size.height * 0.4442568,
        size.width * 0.5136050,
        size.height * 0.4442568)
    ..cubicTo(
        size.width * 0.5138400,
        size.height * 0.4442568,
        size.width * 0.5140275,
        size.height * 0.4451110,
        size.width * 0.5140250,
        size.height * 0.4461539)
    ..moveTo(size.width * 0.6111900, size.height * 0.4486917)
    ..cubicTo(
        size.width * 0.6110350,
        size.height * 0.4511301,
        size.width * 0.6105400,
        size.height * 0.4581970,
        size.width * 0.6100875,
        size.height * 0.4643998)
    ..cubicTo(
        size.width * 0.6096375,
        size.height * 0.4705976,
        size.width * 0.6092675,
        size.height * 0.4760157,
        size.width * 0.6092675,
        size.height * 0.4764329)
    ..cubicTo(
        size.width * 0.6092675,
        size.height * 0.4800234,
        size.width * 0.6043075,
        size.height * 0.4740888,
        size.width * 0.6031050,
        size.height * 0.4690581)
    ..cubicTo(
        size.width * 0.6004750,
        size.height * 0.4580678,
        size.width * 0.6044300,
        size.height * 0.4442568,
        size.width * 0.6102075,
        size.height * 0.4442568)
    ..lineTo(size.width * 0.6114750, size.height * 0.4442568)
    ..lineTo(size.width * 0.6111900, size.height * 0.4486917)
    ..moveTo(size.width * 0.4092750, size.height * 0.4573577)
    ..cubicTo(
        size.width * 0.4192700,
        size.height * 0.4622395,
        size.width * 0.4275550,
        size.height * 0.4664458,
        size.width * 0.4276850,
        size.height * 0.4667041)
    ..cubicTo(
        size.width * 0.4281775,
        size.height * 0.4676824,
        size.width * 0.4245625,
        size.height * 0.4797255,
        size.width * 0.4239000,
        size.height * 0.4793182)
    ..arcToPoint(Offset(size.width * 0.4118000, size.height * 0.4733886),
        radius:
            Radius.elliptical(size.width * 1.045650, size.height * 2.077165),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.4010000,
        size.height * 0.4681691,
        size.width * 0.4002200,
        size.height * 0.4676626,
        size.width * 0.3981950,
        size.height * 0.4645438)
    ..cubicTo(
        size.width * 0.3949150,
        size.height * 0.4594931,
        size.width * 0.3895700,
        size.height * 0.4484781,
        size.width * 0.3903975,
        size.height * 0.4484781)
    ..cubicTo(
        size.width * 0.3907850,
        size.height * 0.4484781,
        size.width * 0.3992800,
        size.height * 0.4524759,
        size.width * 0.4092750,
        size.height * 0.4573577)
    ..moveTo(size.width * 0.5250050, size.height * 0.4544922)
    ..cubicTo(
        size.width * 0.5242550,
        size.height * 0.4554109,
        size.width * 0.5241500,
        size.height * 0.4549391,
        size.width * 0.5244925,
        size.height * 0.4522177)
    ..lineTo(size.width * 0.5247925, size.height * 0.4498438)
    ..lineTo(size.width * 0.5251650, size.height * 0.4518402)
    ..cubicTo(
        size.width * 0.5254200,
        size.height * 0.4532159,
        size.width * 0.5253700,
        size.height * 0.4540403,
        size.width * 0.5250050,
        size.height * 0.4544922)
    ..moveTo(size.width * 0.5264925, size.height * 0.4659939)
    ..cubicTo(
        size.width * 0.5273225,
        size.height * 0.4759164,
        size.width * 0.5282500,
        size.height * 0.4743123,
        size.width * 0.5182825,
        size.height * 0.4801575)
    ..cubicTo(
        size.width * 0.5137225,
        size.height * 0.4828343,
        size.width * 0.5098000,
        size.height * 0.4852131,
        size.width * 0.5095675,
        size.height * 0.4854515)
    ..cubicTo(
        size.width * 0.5078500,
        size.height * 0.4871897,
        size.width * 0.5139425,
        size.height * 0.4764974,
        size.width * 0.5159450,
        size.height * 0.4742626)
    ..cubicTo(
        size.width * 0.5193475,
        size.height * 0.4704585,
        size.width * 0.5219575,
        size.height * 0.4658946,
        size.width * 0.5229625,
        size.height * 0.4619812)
    ..cubicTo(
        size.width * 0.5237450,
        size.height * 0.4589270,
        size.width * 0.5242875,
        size.height * 0.4579884,
        size.width * 0.5253375,
        size.height * 0.4578344)
    ..cubicTo(
        size.width * 0.5256050,
        size.height * 0.4577997,
        size.width * 0.5261000,
        size.height * 0.4613108,
        size.width * 0.5264925,
        size.height * 0.4659939)
    ..moveTo(size.width * 0.5003425, size.height * 0.4621302)
    ..cubicTo(
        size.width * 0.5004400,
        size.height * 0.4638088,
        size.width * 0.5002475,
        size.height * 0.4645239,
        size.width * 0.4995800,
        size.height * 0.4649460)
    ..cubicTo(
        size.width * 0.4985375,
        size.height * 0.4656016,
        size.width * 0.4974500,
        size.height * 0.4641167,
        size.width * 0.4974500,
        size.height * 0.4620309)
    ..cubicTo(
        size.width * 0.4974500,
        size.height * 0.4584353,
        size.width * 0.5001350,
        size.height * 0.4585297,
        size.width * 0.5003425,
        size.height * 0.4621302)
    ..moveTo(size.width * 0.5067325, size.height * 0.4644296)
    ..cubicTo(
        size.width * 0.5068850,
        size.height * 0.4665452,
        size.width * 0.5062300,
        size.height * 0.4678711,
        size.width * 0.5050250,
        size.height * 0.4678910)
    ..cubicTo(
        size.width * 0.5040925,
        size.height * 0.4679059,
        size.width * 0.5036050,
        size.height * 0.4648020,
        size.width * 0.5042850,
        size.height * 0.4631781)
    ..cubicTo(
        size.width * 0.5050775,
        size.height * 0.4612810,
        size.width * 0.5065625,
        size.height * 0.4620408,
        size.width * 0.5067325,
        size.height * 0.4644296)
    ..moveTo(size.width * 0.3943375, size.height * 0.4736071)
    ..cubicTo(
        size.width * 0.3954775,
        size.height * 0.4767408,
        size.width * 0.4000525,
        size.height * 0.4864249,
        size.width * 0.4045075,
        size.height * 0.4951207)
    ..cubicTo(
        size.width * 0.4103225,
        size.height * 0.5064784,
        size.width * 0.4124750,
        size.height * 0.5111963,
        size.width * 0.4121450,
        size.height * 0.5118518)
    ..cubicTo(
        size.width * 0.4112450,
        size.height * 0.5136446,
        size.width * 0.3847150,
        size.height * 0.4983736,
        size.width * 0.3850400,
        size.height * 0.4962530)
    ..cubicTo(
        size.width * 0.3855125,
        size.height * 0.4931491,
        size.width * 0.3917050,
        size.height * 0.4679059,
        size.width * 0.3919900,
        size.height * 0.4679059)
    ..cubicTo(
        size.width * 0.3921425,
        size.height * 0.4679059,
        size.width * 0.3932000,
        size.height * 0.4704685,
        size.width * 0.3943375,
        size.height * 0.4736071)
    ..moveTo(size.width * 0.4747625, size.height * 0.4699073)
    ..arcToPoint(Offset(size.width * 0.4725300, size.height * 0.4817566),
        radius: Radius.elliptical(
            size.width * 0.003360000, size.height * 0.006674579),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..cubicTo(
        size.width * 0.4693325,
        size.height * 0.4817566,
        size.width * 0.4679300,
        size.height * 0.4748934,
        size.width * 0.4702050,
        size.height * 0.4703741)
    ..cubicTo(
        size.width * 0.4717200,
        size.height * 0.4673646,
        size.width * 0.4731875,
        size.height * 0.4672156,
        size.width * 0.4747625,
        size.height * 0.4699073)
    ..moveTo(size.width * 0.4985125, size.height * 0.4733936)
    ..cubicTo(
        size.width * 0.4985125,
        size.height * 0.4756432,
        size.width * 0.4983700,
        size.height * 0.4759263,
        size.width * 0.4972425,
        size.height * 0.4759263)
    ..cubicTo(
        size.width * 0.4958200,
        size.height * 0.4759263,
        size.width * 0.4953050,
        size.height * 0.4737859,
        size.width * 0.4962075,
        size.height * 0.4716256)
    ..cubicTo(
        size.width * 0.4971250,
        size.height * 0.4694256,
        size.width * 0.4985125,
        size.height * 0.4704933,
        size.width * 0.4985125,
        size.height * 0.4733936)
    ..moveTo(size.width * 0.4216575, size.height * 0.4814438)
    ..lineTo(size.width * 0.4238825, size.height * 0.4823774)
    ..lineTo(size.width * 0.4230350, size.height * 0.4856799)
    ..lineTo(size.width * 0.4221875, size.height * 0.4889775)
    ..lineTo(size.width * 0.4191075, size.height * 0.4867725)
    ..cubicTo(
        size.width * 0.4137450,
        size.height * 0.4829336,
        size.width * 0.4047625,
        size.height * 0.4745656,
        size.width * 0.4047625,
        size.height * 0.4734085)
    ..cubicTo(
        size.width * 0.4047625,
        size.height * 0.4732744,
        size.width * 0.4080625,
        size.height * 0.4748189,
        size.width * 0.4120950,
        size.height * 0.4768401)
    ..cubicTo(
        size.width * 0.4161300,
        size.height * 0.4788613,
        size.width * 0.4204325,
        size.height * 0.4809323,
        size.width * 0.4216575,
        size.height * 0.4814438)
    ..moveTo(size.width * 0.5040050, size.height * 0.4768103)
    ..cubicTo(
        size.width * 0.5043400,
        size.height * 0.4794523,
        size.width * 0.5040300,
        size.height * 0.4805747,
        size.width * 0.5029650,
        size.height * 0.4805747)
    ..cubicTo(
        size.width * 0.5018575,
        size.height * 0.4805747,
        size.width * 0.5010450,
        size.height * 0.4774956,
        size.width * 0.5017225,
        size.height * 0.4758717)
    ..cubicTo(
        size.width * 0.5025325,
        size.height * 0.4739398,
        size.width * 0.5037025,
        size.height * 0.4744166,
        size.width * 0.5040050,
        size.height * 0.4768103)
    ..moveTo(size.width * 0.4578600, size.height * 0.4833111)
    ..cubicTo(
        size.width * 0.4583575,
        size.height * 0.4880836,
        size.width * 0.4583400,
        size.height * 0.4886646,
        size.width * 0.4576850,
        size.height * 0.4894344)
    ..cubicTo(
        size.width * 0.4572875,
        size.height * 0.4899012,
        size.width * 0.4555975,
        size.height * 0.4922055,
        size.width * 0.4539275,
        size.height * 0.4945496)
    ..cubicTo(
        size.width * 0.4511625,
        size.height * 0.4984282,
        size.width * 0.4430700,
        size.height * 0.5045863,
        size.width * 0.4424225,
        size.height * 0.5033050)
    ..cubicTo(
        size.width * 0.4420075,
        size.height * 0.5024757,
        size.width * 0.4396500,
        size.height * 0.4886895,
        size.width * 0.4398550,
        size.height * 0.4882822)
    ..cubicTo(
        size.width * 0.4401650,
        size.height * 0.4876664,
        size.width * 0.4558350,
        size.height * 0.4781661,
        size.width * 0.4566550,
        size.height * 0.4780966)
    ..cubicTo(
        size.width * 0.4571275,
        size.height * 0.4780568,
        size.width * 0.4574675,
        size.height * 0.4795268,
        size.width * 0.4578600,
        size.height * 0.4833111)
    ..moveTo(size.width * 0.5280550, size.height * 0.4841603)
    ..cubicTo(
        size.width * 0.5283275,
        size.height * 0.4875323,
        size.width * 0.5286350,
        size.height * 0.4908895,
        size.width * 0.5287375,
        size.height * 0.4916344)
    ..cubicTo(
        size.width * 0.5288950,
        size.height * 0.4927518,
        size.width * 0.4992600,
        size.height * 0.5114099,
        size.width * 0.4861925,
        size.height * 0.5184271)
    ..cubicTo(
        size.width * 0.4854900,
        size.height * 0.5188045,
        size.width * 0.4855650,
        size.height * 0.5183427,
        size.width * 0.4866700,
        size.height * 0.5155616)
    ..cubicTo(
        size.width * 0.4873925,
        size.height * 0.5137390,
        size.width * 0.4885850,
        size.height * 0.5098753,
        size.width * 0.4893175,
        size.height * 0.5069800)
    ..cubicTo(
        size.width * 0.4903675,
        size.height * 0.5028382,
        size.width * 0.4910350,
        size.height * 0.5013086,
        size.width * 0.4924550,
        size.height * 0.4998088)
    ..cubicTo(
        size.width * 0.4938325,
        size.height * 0.4983487,
        size.width * 0.5259100,
        size.height * 0.4783449,
        size.width * 0.5272800,
        size.height * 0.4780916)
    ..cubicTo(
        size.width * 0.5274325,
        size.height * 0.4780618,
        size.width * 0.5277825,
        size.height * 0.4807932,
        size.width * 0.5280550,
        size.height * 0.4841603)
    ..moveTo(size.width * 0.5093525, size.height * 0.4808975)
    ..cubicTo(
        size.width * 0.5093525,
        size.height * 0.4819652,
        size.width * 0.5072925,
        size.height * 0.4837084,
        size.width * 0.5067775,
        size.height * 0.4830777)
    ..cubicTo(
        size.width * 0.5063175,
        size.height * 0.4825115,
        size.width * 0.5064200,
        size.height * 0.4821639,
        size.width * 0.5072375,
        size.height * 0.4815133)
    ..cubicTo(
        size.width * 0.5083400,
        size.height * 0.4806392,
        size.width * 0.5093525,
        size.height * 0.4803462,
        size.width * 0.5093525,
        size.height * 0.4808975)
    ..moveTo(size.width * 0.5063775, size.height * 0.4869712)
    ..cubicTo(
        size.width * 0.5063775,
        size.height * 0.4872940,
        size.width * 0.5007375,
        size.height * 0.4907107,
        size.width * 0.5002000,
        size.height * 0.4907107)
    ..cubicTo(
        size.width * 0.5000100,
        size.height * 0.4907107,
        size.width * 0.5008925,
        size.height * 0.4892556,
        size.width * 0.5021600,
        size.height * 0.4874777)
    ..cubicTo(
        size.width * 0.5042800,
        size.height * 0.4845079,
        size.width * 0.5063775,
        size.height * 0.4842546,
        size.width * 0.5063775,
        size.height * 0.4869712)
    ..moveTo(size.width * 0.4750225, size.height * 0.4866335)
    ..cubicTo(
        size.width * 0.4754300,
        size.height * 0.4870506,
        size.width * 0.4757650,
        size.height * 0.4879396,
        size.width * 0.4757650,
        size.height * 0.4886100)
    ..cubicTo(
        size.width * 0.4757650,
        size.height * 0.4898665,
        size.width * 0.4756175,
        size.height * 0.4899658,
        size.width * 0.4713000,
        size.height * 0.4916195)
    ..cubicTo(
        size.width * 0.4642450,
        size.height * 0.4943162,
        size.width * 0.4614525,
        size.height * 0.5020635,
        size.width * 0.4653900,
        size.height * 0.5080279)
    ..cubicTo(
        size.width * 0.4731800,
        size.height * 0.5198325,
        size.width * 0.4788575,
        size.height * 0.5183625,
        size.width * 0.4851675,
        size.height * 0.5029127)
    ..cubicTo(
        size.width * 0.4883500,
        size.height * 0.4951257,
        size.width * 0.4897825,
        size.height * 0.4978174,
        size.width * 0.4874650,
        size.height * 0.5072233)
    ..cubicTo(
        size.width * 0.4841900,
        size.height * 0.5205228,
        size.width * 0.4791175,
        size.height * 0.5252805,
        size.width * 0.4664125,
        size.height * 0.5269739)
    ..cubicTo(
        size.width * 0.4584750,
        size.height * 0.5280317,
        size.width * 0.4574275,
        size.height * 0.5285681,
        size.width * 0.4528525,
        size.height * 0.5339316)
    ..cubicTo(
        size.width * 0.4322350,
        size.height * 0.5581021,
        size.width * 0.4032850,
        size.height * 0.5790098,
        size.width * 0.3925950,
        size.height * 0.5774455)
    ..cubicTo(
        size.width * 0.3817450,
        size.height * 0.5758613,
        size.width * 0.3769925,
        size.height * 0.5506677,
        size.width * 0.3813175,
        size.height * 0.5176772)
    ..cubicTo(
        size.width * 0.3830375,
        size.height * 0.5045565,
        size.width * 0.3838425,
        size.height * 0.5006779,
        size.width * 0.3847250,
        size.height * 0.5012242)
    ..cubicTo(
        size.width * 0.3851050,
        size.height * 0.5014625,
        size.width * 0.3902850,
        size.height * 0.5043380,
        size.width * 0.3962350,
        size.height * 0.5076107)
    ..cubicTo(
        size.width * 0.4021850,
        size.height * 0.5108834,
        size.width * 0.4071175,
        size.height * 0.5136844,
        size.width * 0.4071925,
        size.height * 0.5138383)
    ..cubicTo(
        size.width * 0.4072700,
        size.height * 0.5139873,
        size.width * 0.4041475,
        size.height * 0.5152090,
        size.width * 0.4002550,
        size.height * 0.5165449)
    ..cubicTo(
        size.width * 0.3937925,
        size.height * 0.5187698,
        size.width * 0.3921225,
        size.height * 0.5199368,
        size.width * 0.3929725,
        size.height * 0.5216253)
    ..cubicTo(
        size.width * 0.3930950,
        size.height * 0.5218687,
        size.width * 0.3971775,
        size.height * 0.5206669,
        size.width * 0.4020475,
        size.height * 0.5189535)
    ..cubicTo(
        size.width * 0.4094200,
        size.height * 0.5163661,
        size.width * 0.4112125,
        size.height * 0.5159887,
        size.width * 0.4127700,
        size.height * 0.5166939)
    ..cubicTo(
        size.width * 0.4145600,
        size.height * 0.5175084,
        size.width * 0.4466475,
        size.height * 0.5089963,
        size.width * 0.4479875,
        size.height * 0.5073525)
    ..cubicTo(
        size.width * 0.4482300,
        size.height * 0.5070545,
        size.width * 0.4505425,
        size.height * 0.5036626,
        size.width * 0.4531275,
        size.height * 0.4998138)
    ..cubicTo(
        size.width * 0.4587300,
        size.height * 0.4914755,
        size.width * 0.4631525,
        size.height * 0.4865788,
        size.width * 0.4658925,
        size.height * 0.4856799)
    ..cubicTo(
        size.width * 0.4682125,
        size.height * 0.4849201,
        size.width * 0.4739275,
        size.height * 0.4855161,
        size.width * 0.4750225,
        size.height * 0.4866335)
    ..moveTo(size.width * 0.4832050, size.height * 0.4902836)
    ..cubicTo(
        size.width * 0.4832050,
        size.height * 0.4931839,
        size.width * 0.4815400,
        size.height * 0.4939785,
        size.width * 0.4807400,
        size.height * 0.4914556)
    ..cubicTo(
        size.width * 0.4800325,
        size.height * 0.4892308,
        size.width * 0.4807000,
        size.height * 0.4871797,
        size.width * 0.4820350,
        size.height * 0.4874827)
    ..cubicTo(
        size.width * 0.4830225,
        size.height * 0.4877111,
        size.width * 0.4832050,
        size.height * 0.4881482,
        size.width * 0.4832050,
        size.height * 0.4902836)
    ..moveTo(size.width * 0.4752075, size.height * 0.4971568)
    ..cubicTo(
        size.width * 0.4755600,
        size.height * 0.4989794,
        size.width * 0.4748050,
        size.height * 0.5012639,
        size.width * 0.4738525,
        size.height * 0.5012639)
    ..arcToPoint(Offset(size.width * 0.4726625, size.height * 0.4999032),
        radius: Radius.elliptical(
            size.width * 0.001585000, size.height * 0.003148574),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..cubicTo(
        size.width * 0.4720125,
        size.height * 0.4978571,
        size.width * 0.4726125,
        size.height * 0.4957762,
        size.width * 0.4738525,
        size.height * 0.4957762)
    ..cubicTo(
        size.width * 0.4744675,
        size.height * 0.4957762,
        size.width * 0.4750575,
        size.height * 0.4963772,
        size.width * 0.4752075,
        size.height * 0.4971568)
    ..moveTo(size.width * 0.5298925, size.height * 0.5048197)
    ..cubicTo(
        size.width * 0.5302700,
        size.height * 0.5093290,
        size.width * 0.5305375,
        size.height * 0.5130884,
        size.width * 0.5304900,
        size.height * 0.5131729)
    ..cubicTo(
        size.width * 0.5293425,
        size.height * 0.5151444,
        size.width * 0.4628575,
        size.height * 0.5532352,
        size.width * 0.4613125,
        size.height * 0.5528032)
    ..cubicTo(
        size.width * 0.4554300,
        size.height * 0.5511544,
        size.width * 0.4502050,
        size.height * 0.5479909,
        size.width * 0.4497900,
        size.height * 0.5458257)
    ..cubicTo(
        size.width * 0.4492875,
        size.height * 0.5431936,
        size.width * 0.4474925,
        size.height * 0.5445990,
        size.width * 0.4623725,
        size.height * 0.5359578)
    ..cubicTo(
        size.width * 0.4693875,
        size.height * 0.5318855,
        size.width * 0.4766575,
        size.height * 0.5275947,
        size.width * 0.4785300,
        size.height * 0.5264227)
    ..cubicTo(
        size.width * 0.4882175,
        size.height * 0.5203540,
        size.width * 0.5279425,
        size.height * 0.4967149,
        size.width * 0.5285275,
        size.height * 0.4966702)
    ..cubicTo(
        size.width * 0.5290600,
        size.height * 0.4966304,
        size.width * 0.5293550,
        size.height * 0.4983934,
        size.width * 0.5298925,
        size.height * 0.5048197)
    ..moveTo(size.width * 0.4806225, size.height * 0.5004792)
    ..cubicTo(
        size.width * 0.4816775,
        size.height * 0.5025700,
        size.width * 0.4804000,
        size.height * 0.5064734,
        size.width * 0.4790075,
        size.height * 0.5054156)
    ..cubicTo(
        size.width * 0.4779400,
        size.height * 0.5045962,
        size.width * 0.4776725,
        size.height * 0.5015619,
        size.width * 0.4785575,
        size.height * 0.5002756)
    ..cubicTo(
        size.width * 0.4795475,
        size.height * 0.4988404,
        size.width * 0.4798100,
        size.height * 0.4988652,
        size.width * 0.4806225,
        size.height * 0.5004792)
    ..moveTo(size.width * 0.4238825, size.height * 0.5075859)
    ..cubicTo(
        size.width * 0.4259700,
        size.height * 0.5097064,
        size.width * 0.4263200,
        size.height * 0.5108089,
        size.width * 0.4250800,
        size.height * 0.5113801)
    ..cubicTo(
        size.width * 0.4239475,
        size.height * 0.5119015,
        size.width * 0.4156050,
        size.height * 0.5143548,
        size.width * 0.4149600,
        size.height * 0.5143548)
    ..cubicTo(
        size.width * 0.4142075,
        size.height * 0.5143548,
        size.width * 0.4162300,
        size.height * 0.5075561,
        size.width * 0.4172175,
        size.height * 0.5067665)
    ..cubicTo(
        size.width * 0.4188975,
        size.height * 0.5054256,
        size.width * 0.4221425,
        size.height * 0.5058229,
        size.width * 0.4238825,
        size.height * 0.5075859)
    ..moveTo(size.width * 0.5775925, size.height * 0.5122491)
    ..cubicTo(
        size.width * 0.5897525,
        size.height * 0.5166045,
        size.width * 0.5991075,
        size.height * 0.5279076,
        size.width * 0.6026475,
        size.height * 0.5425132)
    ..cubicTo(
        size.width * 0.6051550,
        size.height * 0.5528628,
        size.width * 0.6053700,
        size.height * 0.5520831,
        size.width * 0.5998225,
        size.height * 0.5527684)
    ..cubicTo(
        size.width * 0.5892075,
        size.height * 0.5540845,
        size.width * 0.5737825,
        size.height * 0.5582213,
        size.width * 0.5592100,
        size.height * 0.5636643)
    ..cubicTo(
        size.width * 0.5543900,
        size.height * 0.5654620,
        size.width * 0.5517350,
        size.height * 0.5661176,
        size.width * 0.5514600,
        size.height * 0.5655663)
    ..cubicTo(
        size.width * 0.5511850,
        size.height * 0.5650200,
        size.width * 0.5507825,
        size.height * 0.5651690,
        size.width * 0.5502875,
        size.height * 0.5660083)
    ..cubicTo(
        size.width * 0.5498725,
        size.height * 0.5667085,
        size.width * 0.5448450,
        size.height * 0.5755832,
        size.width * 0.5391150,
        size.height * 0.5857341)
    ..arcToPoint(Offset(size.width * 0.5279875, size.height * 0.6053754),
        radius:
            Radius.elliptical(size.width * 2.304538, size.height * 4.577922),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..cubicTo(
        size.width * 0.5273725,
        size.height * 0.6064034,
        size.width * 0.5262475,
        size.height * 0.6062743,
        size.width * 0.5196950,
        size.height * 0.6044269)
    ..cubicTo(
        size.width * 0.5082075,
        size.height * 0.6011790,
        size.width * 0.5017550,
        size.height * 0.6003745,
        size.width * 0.4889450,
        size.height * 0.6005830)
    ..lineTo(size.width * 0.4772525, size.height * 0.6007717)
    ..lineTo(size.width * 0.4665300, size.height * 0.5930543)
    ..cubicTo(
        size.width * 0.4606325,
        size.height * 0.5888082,
        size.width * 0.4555475,
        size.height * 0.5855354,
        size.width * 0.4552300,
        size.height * 0.5857738)
    ..cubicTo(
        size.width * 0.4549125,
        size.height * 0.5860172,
        size.width * 0.4514725,
        size.height * 0.5843088,
        size.width * 0.4475825,
        size.height * 0.5819796)
    ..cubicTo(
        size.width * 0.4387025,
        size.height * 0.5766608,
        size.width * 0.4319850,
        size.height * 0.5731447,
        size.width * 0.4271400,
        size.height * 0.5712775)
    ..cubicTo(
        size.width * 0.4235475,
        size.height * 0.5698969,
        size.width * 0.4234200,
        size.height * 0.5697727,
        size.width * 0.4244625,
        size.height * 0.5686901)
    ..cubicTo(
        size.width * 0.4278950,
        size.height * 0.5651194,
        size.width * 0.4379850,
        size.height * 0.5588967,
        size.width * 0.4467825,
        size.height * 0.5549287)
    ..lineTo(size.width * 0.4523050, size.height * 0.5524406)
    ..lineTo(size.width * 0.4571250, size.height * 0.5544420)
    ..cubicTo(
        size.width * 0.4780075,
        size.height * 0.5631080,
        size.width * 0.4992275,
        size.height * 0.5586335,
        size.width * 0.5135050,
        size.height * 0.5425529)
    ..cubicTo(
        size.width * 0.5192600,
        size.height * 0.5360720,
        size.width * 0.5247925,
        size.height * 0.5283694,
        size.width * 0.5280650,
        size.height * 0.5222958)
    ..cubicTo(
        size.width * 0.5304900,
        size.height * 0.5177865,
        size.width * 0.5420925,
        size.height * 0.5128699,
        size.width * 0.5554850,
        size.height * 0.5106699)
    ..cubicTo(
        size.width * 0.5600575,
        size.height * 0.5099200,
        size.width * 0.5738400,
        size.height * 0.5109033,
        size.width * 0.5775925,
        size.height * 0.5122491)
    ..moveTo(size.width * 0.3800725, size.height * 0.5145634)
    ..arcToPoint(Offset(size.width * 0.3796775, size.height * 0.5175481),
        radius: Radius.elliptical(
            size.width * 0.02024500, size.height * 0.04021633),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.3796775,
        size.height * 0.5176822,
        size.width * 0.3792950,
        size.height * 0.5173892,
        size.width * 0.3788275,
        size.height * 0.5168925)
    ..cubicTo(
        size.width * 0.3778025,
        size.height * 0.5158000,
        size.width * 0.3777575,
        size.height * 0.5149110,
        size.width * 0.3786450,
        size.height * 0.5131480)
    ..cubicTo(
        size.width * 0.3798350,
        size.height * 0.5107841,
        size.width * 0.3805200,
        size.height * 0.5114645,
        size.width * 0.3800725,
        size.height * 0.5145634)
    ..moveTo(size.width * 0.5265725, size.height * 0.5207960)
    ..cubicTo(
        size.width * 0.5106900,
        size.height * 0.5475986,
        size.width * 0.4897050,
        size.height * 0.5601482,
        size.width * 0.4693875,
        size.height * 0.5549784)
    ..lineTo(size.width * 0.4670500, size.height * 0.5543874)
    ..lineTo(size.width * 0.4687500, size.height * 0.5533743)
    ..cubicTo(
        size.width * 0.4696850,
        size.height * 0.5528230,
        size.width * 0.4830775,
        size.height * 0.5447977,
        size.width * 0.4985125,
        size.height * 0.5355456)
    ..cubicTo(
        size.width * 0.5287025,
        size.height * 0.5174488,
        size.width * 0.5284900,
        size.height * 0.5175580,
        size.width * 0.5265725,
        size.height * 0.5207960)
    ..moveTo(size.width * 0.4596100, size.height * 0.5342196)
    ..cubicTo(
        size.width * 0.4521425,
        size.height * 0.5387339,
        size.width * 0.4518825,
        size.height * 0.5387836,
        size.width * 0.4552450,
        size.height * 0.5349944)
    ..cubicTo(
        size.width * 0.4581000,
        size.height * 0.5317812,
        size.width * 0.4602925,
        size.height * 0.5306191,
        size.width * 0.4636475,
        size.height * 0.5305397)
    ..cubicTo(
        size.width * 0.4655575,
        size.height * 0.5304950,
        size.width * 0.4651450,
        size.height * 0.5308724,
        size.width * 0.4596100,
        size.height * 0.5342196)
    ..moveTo(size.width * 0.3784425, size.height * 0.5496745)
    ..lineTo(size.width * 0.3789075, size.height * 0.5565576)
    ..lineTo(size.width * 0.3771350, size.height * 0.5631180)
    ..cubicTo(
        size.width * 0.3752075,
        size.height * 0.5702445,
        size.width * 0.3701950,
        size.height * 0.5854510,
        size.width * 0.3692700,
        size.height * 0.5869756)
    ..cubicTo(
        size.width * 0.3687425,
        size.height * 0.5878447,
        size.width * 0.3691600,
        size.height * 0.5826302,
        size.width * 0.3705375,
        size.height * 0.5711583)
    ..cubicTo(
        size.width * 0.3707600,
        size.height * 0.5692959,
        size.width * 0.3706825,
        size.height * 0.5692562,
        size.width * 0.3671425,
        size.height * 0.5692562)
    ..cubicTo(
        size.width * 0.3628500,
        size.height * 0.5692562,
        size.width * 0.3627550,
        size.height * 0.5688887,
        size.width * 0.3660850,
        size.height * 0.5650995)
    ..cubicTo(
        size.width * 0.3690950,
        size.height * 0.5616778,
        size.width * 0.3690325,
        size.height * 0.5624128,
        size.width * 0.3669600,
        size.height * 0.5548244)
    ..cubicTo(
        size.width * 0.3643550,
        size.height * 0.5452943,
        size.width * 0.3657450,
        size.height * 0.5434319,
        size.width * 0.3699925,
        size.height * 0.5507670)
    ..cubicTo(
        size.width * 0.3728875,
        size.height * 0.5557730,
        size.width * 0.3727475,
        size.height * 0.5558524,
        size.width * 0.3758975,
        size.height * 0.5472609)
    ..cubicTo(
        size.width * 0.3769000,
        size.height * 0.5445245,
        size.width * 0.3777800,
        size.height * 0.5423990,
        size.width * 0.3778475,
        size.height * 0.5425380)
    ..cubicTo(
        size.width * 0.3779175,
        size.height * 0.5426771,
        size.width * 0.3781850,
        size.height * 0.5458853,
        size.width * 0.3784425,
        size.height * 0.5496745)
    ..moveTo(size.width * 0.6409200, size.height * 0.5648512)
    ..cubicTo(
        size.width * 0.6372700,
        size.height * 0.5650747,
        size.width * 0.6367925,
        size.height * 0.5649108,
        size.width * 0.6352200,
        size.height * 0.5629392)
    ..cubicTo(
        size.width * 0.6331675,
        size.height * 0.5603667,
        size.width * 0.6331925,
        size.height * 0.5607739,
        size.width * 0.6347800,
        size.height * 0.5551919)
    ..cubicTo(
        size.width * 0.6354800,
        size.height * 0.5527187,
        size.width * 0.6360550,
        size.height * 0.5497440,
        size.width * 0.6360550,
        size.height * 0.5485769)
    ..cubicTo(
        size.width * 0.6360550,
        size.height * 0.5467593,
        size.width * 0.6366825,
        size.height * 0.5477476,
        size.width * 0.6404775,
        size.height * 0.5555346)
    ..lineTo(size.width * 0.6449025, size.height * 0.5646078)
    ..lineTo(size.width * 0.6409200, size.height * 0.5648512)
    ..moveTo(size.width * 0.6045875, size.height * 0.5606001)
    ..cubicTo(
        size.width * 0.6045800,
        size.height * 0.5722806,
        size.width * 0.6032300,
        size.height * 0.5816419,
        size.width * 0.6004125,
        size.height * 0.5895134)
    ..cubicTo(
        size.width * 0.5877325,
        size.height * 0.6249621,
        size.width * 0.5554075,
        size.height * 0.6797592,
        size.width * 0.5173200,
        size.height * 0.7303698)
    ..lineTo(size.width * 0.5055175, size.height * 0.7460531)
    ..lineTo(size.width * 0.5044100, size.height * 0.7440120)
    ..cubicTo(
        size.width * 0.4975500,
        size.height * 0.7313780,
        size.width * 0.4885350,
        size.height * 0.6991920,
        size.width * 0.4883625,
        size.height * 0.6867169)
    ..lineTo(size.width * 0.4883075, size.height * 0.6827638)
    ..lineTo(size.width * 0.5198525, size.height * 0.6265116)
    ..cubicTo(
        size.width * 0.5372025,
        size.height * 0.5955721,
        size.width * 0.5515750,
        size.height * 0.5700409,
        size.width * 0.5517900,
        size.height * 0.5697777)
    ..cubicTo(
        size.width * 0.5520050,
        size.height * 0.5695145,
        size.width * 0.5562725,
        size.height * 0.5677961,
        size.width * 0.5612725,
        size.height * 0.5659636)
    ..arcToPoint(Offset(size.width * 0.5956625, size.height * 0.5563043),
        radius:
            Radius.elliptical(size.width * 0.3923725, size.height * 0.7794409),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..cubicTo(
        size.width * 0.5964825,
        size.height * 0.5561405,
        size.width * 0.5988250,
        size.height * 0.5559517,
        size.width * 0.6008725,
        size.height * 0.5558773)
    ..lineTo(size.width * 0.6045925, size.height * 0.5557432)
    ..lineTo(size.width * 0.6045875, size.height * 0.5606001)
    ..moveTo(size.width * 0.3686175, size.height * 0.5732689)
    ..cubicTo(
        size.width * 0.3686125,
        size.height * 0.5740784,
        size.width * 0.3680775,
        size.height * 0.5793475,
        size.width * 0.3674250,
        size.height * 0.5849693)
    ..lineTo(size.width * 0.3662375, size.height * 0.5951947)
    ..lineTo(size.width * 0.3603125, size.height * 0.6090405)
    ..cubicTo(
        size.width * 0.3529525,
        size.height * 0.6262434,
        size.width * 0.3495975,
        size.height * 0.6351131,
        size.width * 0.3477425,
        size.height * 0.6423091)
    ..lineTo(size.width * 0.3463000, size.height * 0.6478911)
    ..lineTo(size.width * 0.3373625, size.height * 0.6476378)
    ..lineTo(size.width * 0.3284225, size.height * 0.6473796)
    ..lineTo(size.width * 0.3254700, size.height * 0.6444942)
    ..cubicTo(
        size.width * 0.3218650,
        size.height * 0.6409632,
        size.width * 0.3189425,
        size.height * 0.6353216,
        size.width * 0.3172325,
        size.height * 0.6285924)
    ..cubicTo(
        size.width * 0.3161575,
        size.height * 0.6243712,
        size.width * 0.3103750,
        size.height * 0.5771624,
        size.width * 0.3103750,
        size.height * 0.5726233)
    ..cubicTo(
        size.width * 0.3103750,
        size.height * 0.5720820,
        size.width * 0.3205550,
        size.height * 0.5717890,
        size.width * 0.3394975,
        size.height * 0.5717890)
    ..cubicTo(
        size.width * 0.3669550,
        size.height * 0.5717890,
        size.width * 0.3686225,
        size.height * 0.5718734,
        size.width * 0.3686175,
        size.height * 0.5732689)
    ..moveTo(size.width * 0.6896250, size.height * 0.5726233)
    ..cubicTo(
        size.width * 0.6896250,
        size.height * 0.5761592,
        size.width * 0.6838100,
        size.height * 0.6246145,
        size.width * 0.6829550,
        size.height * 0.6282150)
    ..cubicTo(
        size.width * 0.6814525,
        size.height * 0.6345270,
        size.width * 0.6781025,
        size.height * 0.6410079,
        size.width * 0.6745425,
        size.height * 0.6444843)
    ..lineTo(size.width * 0.6715775, size.height * 0.6473796)
    ..lineTo(size.width * 0.6560225, size.height * 0.6473796)
    ..lineTo(size.width * 0.6541875, size.height * 0.6384404)
    ..cubicTo(
        size.width * 0.6506050,
        size.height * 0.6209842,
        size.width * 0.6460775,
        size.height * 0.6084445,
        size.width * 0.6374150,
        size.height * 0.5919667)
    ..cubicTo(
        size.width * 0.6329550,
        size.height * 0.5834844,
        size.width * 0.6322000,
        size.height * 0.5816568,
        size.width * 0.6318275,
        size.height * 0.5784536)
    ..cubicTo(
        size.width * 0.6309675,
        size.height * 0.5710341,
        size.width * 0.6277175,
        size.height * 0.5717890,
        size.width * 0.6605025,
        size.height * 0.5717890)
    ..cubicTo(
        size.width * 0.6793675,
        size.height * 0.5717890,
        size.width * 0.6896250,
        size.height * 0.5720820,
        size.width * 0.6896250,
        size.height * 0.5726233)
    ..moveTo(size.width * 0.4258575, size.height * 0.5739294)
    ..cubicTo(
        size.width * 0.4341450,
        size.height * 0.5767651,
        size.width * 0.4507425,
        size.height * 0.5865187,
        size.width * 0.4702400,
        size.height * 0.6000119)
    ..cubicTo(
        size.width * 0.4851125,
        size.height * 0.6103019,
        size.width * 0.5132125,
        size.height * 0.6311749,
        size.width * 0.5130725,
        size.height * 0.6318304)
    ..cubicTo(
        size.width * 0.5130150,
        size.height * 0.6321035,
        size.width * 0.5071325,
        size.height * 0.6427064,
        size.width * 0.5000000,
        size.height * 0.6553901)
    ..arcToPoint(Offset(size.width * 0.4857500, size.height * 0.6808419),
        radius:
            Radius.elliptical(size.width * 2.827323, size.height * 5.616425),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..lineTo(size.width * 0.4844650, size.height * 0.6832256)
    ..lineTo(size.width * 0.4770325, size.height * 0.6806184)
    ..cubicTo(
        size.width * 0.4686525,
        size.height * 0.6776784,
        size.width * 0.4581725,
        size.height * 0.6771321,
        size.width * 0.4453650,
        size.height * 0.6789597)
    ..cubicTo(
        size.width * 0.4242525,
        size.height * 0.6819742,
        size.width * 0.4145150,
        size.height * 0.6795109,
        size.width * 0.4004500,
        size.height * 0.6675920)
    ..cubicTo(
        size.width * 0.3889100,
        size.height * 0.6578086,
        size.width * 0.3839525,
        size.height * 0.6430987,
        size.width * 0.3844250,
        size.height * 0.6200307)
    ..cubicTo(
        size.width * 0.3849075,
        size.height * 0.5966448,
        size.width * 0.3941450,
        size.height * 0.5815078,
        size.width * 0.4128400,
        size.height * 0.5734676)
    ..cubicTo(
        size.width * 0.4154275,
        size.height * 0.5723551,
        size.width * 0.4219425,
        size.height * 0.5725836,
        size.width * 0.4258575,
        size.height * 0.5739294)
    ..moveTo(size.width * 0.3855475, size.height * 0.5759705)
    ..cubicTo(
        size.width * 0.3864100,
        size.height * 0.5773362,
        size.width * 0.3880125,
        size.height * 0.5789304,
        size.width * 0.3891050,
        size.height * 0.5795065)
    ..cubicTo(
        size.width * 0.3909675,
        size.height * 0.5804848,
        size.width * 0.3920075,
        size.height * 0.5825905,
        size.width * 0.3920075,
        size.height * 0.5853914)
    ..cubicTo(
        size.width * 0.3920075,
        size.height * 0.5858582,
        size.width * 0.3908000,
        size.height * 0.5886393,
        size.width * 0.3893275,
        size.height * 0.5915644)
    ..cubicTo(
        size.width * 0.3878525,
        size.height * 0.5944945,
        size.width * 0.3860825,
        size.height * 0.5990336,
        size.width * 0.3853950,
        size.height * 0.6016508)
    ..cubicTo(
        size.width * 0.3823575,
        size.height * 0.6131972,
        size.width * 0.3741000,
        size.height * 0.6000516,
        size.width * 0.3758250,
        size.height * 0.5864194)
    ..cubicTo(
        size.width * 0.3771400,
        size.height * 0.5760103,
        size.width * 0.3821700,
        size.height * 0.5706070,
        size.width * 0.3855475,
        size.height * 0.5759705)
    ..moveTo(size.width * 0.6197200, size.height * 0.5750816)
    ..cubicTo(
        size.width * 0.6243175,
        size.height * 0.5792284,
        size.width * 0.6258675,
        size.height * 0.5913161,
        size.width * 0.6228600,
        size.height * 0.5995699)
    ..cubicTo(
        size.width * 0.6190375,
        size.height * 0.6100486,
        size.width * 0.6111200,
        size.height * 0.6084843,
        size.width * 0.6083750,
        size.height * 0.5967044)
    ..cubicTo(
        size.width * 0.6051300,
        size.height * 0.5827842,
        size.width * 0.6125600,
        size.height * 0.5686205,
        size.width * 0.6197200,
        size.height * 0.5750816)
    ..moveTo(size.width * 0.5913125, size.height * 0.6617766)
    ..cubicTo(
        size.width * 0.5934150,
        size.height * 0.6635247,
        size.width * 0.5947600,
        size.height * 0.6660972,
        size.width * 0.5957100,
        size.height * 0.6701844)
    ..cubicTo(
        size.width * 0.5996000,
        size.height * 0.6869304,
        size.width * 0.5888350,
        size.height * 0.7018489,
        size.width * 0.5822700,
        size.height * 0.6888126)
    ..cubicTo(
        size.width * 0.5757750,
        size.height * 0.6759055,
        size.width * 0.5828500,
        size.height * 0.6547544,
        size.width * 0.5913125,
        size.height * 0.6617766)
    ..moveTo(size.width * 0.5697075, size.height * 0.6630827)
    ..cubicTo(
        size.width * 0.5747275,
        size.height * 0.6686896,
        size.width * 0.5731975,
        size.height * 0.6864686,
        size.width * 0.5671775,
        size.height * 0.6925274)
    ..cubicTo(
        size.width * 0.5643575,
        size.height * 0.6953631,
        size.width * 0.5522625,
        size.height * 0.6971012,
        size.width * 0.5471100,
        size.height * 0.6954078)
    ..lineTo(size.width * 0.5455350, size.height * 0.6948913)
    ..lineTo(size.width * 0.5519975, size.height * 0.6849241)
    ..arcToPoint(Offset(size.width * 0.5625000, size.height * 0.6681979),
        radius:
            Radius.elliptical(size.width * 0.4190725, size.height * 0.8324800),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.5669200,
        size.height * 0.6608032,
        size.width * 0.5673750,
        size.height * 0.6604804,
        size.width * 0.5697075,
        size.height * 0.6630827)
    ..moveTo(size.width * 0.4107150, size.height * 0.6781254)
    ..arcToPoint(Offset(size.width * 0.4153175, size.height * 0.6807326),
        radius:
            Radius.elliptical(size.width * 0.1650000, size.height * 0.3277695),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.4202075,
        size.height * 0.6832703,
        size.width * 0.4192375,
        size.height * 0.7009500,
        size.width * 0.4139275,
        size.height * 0.7060553)
    ..cubicTo(
        size.width * 0.4046875,
        size.height * 0.7149448,
        size.width * 0.3969750,
        size.height * 0.6893490,
        size.width * 0.4053375,
        size.height * 0.6775443)
    ..cubicTo(
        size.width * 0.4060625,
        size.height * 0.6765213,
        size.width * 0.4069475,
        size.height * 0.6758608,
        size.width * 0.4073050,
        size.height * 0.6760793)
    ..lineTo(size.width * 0.4107150, size.height * 0.6781254)
    ..moveTo(size.width * 0.4929375, size.height * 0.7233675)
    ..lineTo(size.width * 0.4965900, size.height * 0.7351672)
    ..lineTo(size.width * 0.4937250, size.height * 0.7388571)
    ..cubicTo(
        size.width * 0.4778000,
        size.height * 0.7593576,
        size.width * 0.4572325,
        size.height * 0.7654362,
        size.width * 0.4522675,
        size.height * 0.7511087)
    ..cubicTo(
        size.width * 0.4464025,
        size.height * 0.7341739,
        size.width * 0.4507325,
        size.height * 0.7257860,
        size.width * 0.4674750,
        size.height * 0.7216492)
    ..cubicTo(
        size.width * 0.4774000,
        size.height * 0.7191959,
        size.width * 0.4816525,
        size.height * 0.7172690,
        size.width * 0.4857575,
        size.height * 0.7133556)
    ..cubicTo(
        size.width * 0.4873925,
        size.height * 0.7117962,
        size.width * 0.4888575,
        size.height * 0.7107583,
        size.width * 0.4890100,
        size.height * 0.7110463)
    ..cubicTo(
        size.width * 0.4891600,
        size.height * 0.7113344,
        size.width * 0.4909300,
        size.height * 0.7168767,
        size.width * 0.4929375,
        size.height * 0.7233675)
    ..moveTo(size.width * 0.6421275, size.height * 0.7232880)
    ..cubicTo(
        size.width * 0.6428800,
        size.height * 0.7239287,
        size.width * 0.6440700,
        size.height * 0.7256768,
        size.width * 0.6447700,
        size.height * 0.7271716)
    ..lineTo(size.width * 0.6460450, size.height * 0.7298881)
    ..lineTo(size.width * 0.6432825, size.height * 0.7348791)
    ..cubicTo(
        size.width * 0.6417625,
        size.height * 0.7376205,
        size.width * 0.6387975,
        size.height * 0.7423285,
        size.width * 0.6366925,
        size.height * 0.7453380)
    ..lineTo(size.width * 0.6328650, size.height * 0.7508157)
    ..lineTo(size.width * 0.6319625, size.height * 0.7486256)
    ..cubicTo(
        size.width * 0.6265650,
        size.height * 0.7355496,
        size.width * 0.6341900,
        size.height * 0.7165439,
        size.width * 0.6421275,
        size.height * 0.7232880)
    ..moveTo(size.width * 0.3679725, size.height * 0.7299676)
    ..cubicTo(
        size.width * 0.3711700,
        size.height * 0.7335631,
        size.width * 0.3728825,
        size.height * 0.7412558,
        size.width * 0.3720300,
        size.height * 0.7481985)
    ..cubicTo(
        size.width * 0.3709725,
        size.height * 0.7568198,
        size.width * 0.3704100,
        size.height * 0.7568000,
        size.width * 0.3644625,
        size.height * 0.7479303)
    ..cubicTo(
        size.width * 0.3582450,
        size.height * 0.7386584,
        size.width * 0.3567625,
        size.height * 0.7353410,
        size.width * 0.3578100,
        size.height * 0.7330417)
    ..cubicTo(
        size.width * 0.3601550,
        size.height * 0.7278967,
        size.width * 0.3648650,
        size.height * 0.7264714,
        size.width * 0.3679725,
        size.height * 0.7299676)
    ..moveTo(size.width * 0.5476000, size.height * 0.7388918)
    ..cubicTo(
        size.width * 0.5524075,
        size.height * 0.7438233,
        size.width * 0.5532950,
        size.height * 0.7582948,
        size.width * 0.5492500,
        size.height * 0.7657938)
    ..cubicTo(
        size.width * 0.5441700,
        size.height * 0.7752047,
        size.width * 0.5358125,
        size.height * 0.7685599,
        size.width * 0.5353700,
        size.height * 0.7547539)
    ..cubicTo(
        size.width * 0.5349325,
        size.height * 0.7411415,
        size.width * 0.5414950,
        size.height * 0.7326295,
        size.width * 0.5476000,
        size.height * 0.7388918)
    ..moveTo(size.width * 0.7635650, size.height * 0.7531647)
    ..cubicTo(
        size.width * 0.7648850,
        size.height * 0.7631915,
        size.width * 0.7670075,
        size.height * 0.7966885,
        size.width * 0.7670075,
        size.height * 0.8074950)
    ..lineTo(size.width * 0.7670075, size.height * 0.8118206)
    ..lineTo(size.width * 0.7554600, size.height * 0.8115276)
    ..lineTo(size.width * 0.7439125, size.height * 0.8112296)
    ..lineTo(size.width * 0.7433100, size.height * 0.8148102)
    ..cubicTo(
        size.width * 0.7427400,
        size.height * 0.8181872,
        size.width * 0.7427650,
        size.height * 0.8186441,
        size.width * 0.7437675,
        size.height * 0.8228356)
    ..cubicTo(
        size.width * 0.7452175,
        size.height * 0.8288993,
        size.width * 0.7455025,
        size.height * 0.8310795,
        size.width * 0.7448500,
        size.height * 0.8310795)
    ..cubicTo(
        size.width * 0.7424350,
        size.height * 0.8310795,
        size.width * 0.7165400,
        size.height * 0.8404358,
        size.width * 0.7155350,
        size.height * 0.8416675)
    ..cubicTo(
        size.width * 0.7153700,
        size.height * 0.8418711,
        size.width * 0.7147975,
        size.height * 0.8396661,
        size.width * 0.7142625,
        size.height * 0.8367708)
    ..cubicTo(
        size.width * 0.7137300,
        size.height * 0.8338705,
        size.width * 0.7120000,
        size.height * 0.8254180,
        size.width * 0.7104225,
        size.height * 0.8179886)
    ..cubicTo(
        size.width * 0.7061650,
        size.height * 0.7979351,
        size.width * 0.7058125,
        size.height * 0.7919508,
        size.width * 0.7082850,
        size.height * 0.7816111)
    ..cubicTo(
        size.width * 0.7123650,
        size.height * 0.7645622,
        size.width * 0.7207025,
        size.height * 0.7584190,
        size.width * 0.7476625,
        size.height * 0.7526135)
    ..cubicTo(
        size.width * 0.7635375,
        size.height * 0.7491918,
        size.width * 0.7630400,
        size.height * 0.7491719,
        size.width * 0.7635650,
        size.height * 0.7531647)
    ..moveTo(size.width * 0.4545450, size.height * 0.7592731)
    ..cubicTo(
        size.width * 0.4558100,
        size.height * 0.7606587,
        size.width * 0.4571900,
        size.height * 0.7618009,
        size.width * 0.4576100,
        size.height * 0.7618059)
    ..cubicTo(
        size.width * 0.4596750,
        size.height * 0.7618506,
        size.width * 0.4610450,
        size.height * 0.7739582,
        size.width * 0.4596650,
        size.height * 0.7799872)
    ..cubicTo(
        size.width * 0.4553225,
        size.height * 0.7989879,
        size.width * 0.4406100,
        size.height * 0.7882361,
        size.width * 0.4439375,
        size.height * 0.7684954)
    ..cubicTo(
        size.width * 0.4456350,
        size.height * 0.7584190,
        size.width * 0.4501675,
        size.height * 0.7544808,
        size.width * 0.4545450,
        size.height * 0.7592731)
    ..moveTo(size.width * 0.1457025, size.height * 0.7783235)
    ..cubicTo(
        size.width * 0.1373300,
        size.height * 0.7788996,
        size.width * 0.1303800,
        size.height * 0.7795700,
        size.width * 0.1302600,
        size.height * 0.7798084)
    ..cubicTo(
        size.width * 0.1296225,
        size.height * 0.7810748,
        size.width * 0.1239250,
        size.height * 0.8568541,
        size.width * 0.1244475,
        size.height * 0.8571124)
    ..cubicTo(
        size.width * 0.1248550,
        size.height * 0.8573160,
        size.width * 0.1250000,
        size.height * 0.8595905,
        size.width * 0.1250000,
        size.height * 0.8657684)
    ..lineTo(size.width * 0.1250000, size.height * 0.8741514)
    ..lineTo(size.width * 0.1197075, size.height * 0.8741514)
    ..cubicTo(
        size.width * 0.1154000,
        size.height * 0.8741514,
        size.width * 0.1143450,
        size.height * 0.8743947,
        size.width * 0.1140500,
        size.height * 0.8754426)
    ..cubicTo(
        size.width * 0.1137500,
        size.height * 0.8765054,
        size.width * 0.1118525,
        size.height * 0.8746629,
        size.width * 0.1032925,
        size.height * 0.8649987)
    ..cubicTo(
        size.width * 0.09757750,
        size.height * 0.8585476,
        size.width * 0.09265000,
        size.height * 0.8534523,
        size.width * 0.09234750,
        size.height * 0.8536807)
    ..cubicTo(
        size.width * 0.09094000,
        size.height * 0.8547385,
        size.width * 0.09256500,
        size.height * 0.8574203,
        size.width * 0.09920000,
        size.height * 0.8649838)
    ..cubicTo(
        size.width * 0.1031300,
        size.height * 0.8694633,
        size.width * 0.1064525,
        size.height * 0.8733419,
        size.width * 0.1065825,
        size.height * 0.8736002)
    ..cubicTo(
        size.width * 0.1068675,
        size.height * 0.8741663,
        size.width * 0.1063525,
        size.height * 0.8738932,
        size.width * 0.09318250,
        size.height * 0.8664736)
    ..cubicTo(
        size.width * 0.08749250,
        size.height * 0.8632655,
        size.width * 0.08247000,
        size.height * 0.8606384,
        size.width * 0.08202250,
        size.height * 0.8606384)
    ..cubicTo(
        size.width * 0.08057500,
        size.height * 0.8606384,
        size.width * 0.08102000,
        size.height * 0.8638366,
        size.width * 0.08259000,
        size.height * 0.8647156)
    ..cubicTo(
        size.width * 0.08335000,
        size.height * 0.8651427,
        size.width * 0.08794000,
        size.height * 0.8676953,
        size.width * 0.09279250,
        size.height * 0.8703870)
    ..cubicTo(
        size.width * 0.09764250,
        size.height * 0.8730837,
        size.width * 0.1015450,
        size.height * 0.8754227,
        size.width * 0.1014625,
        size.height * 0.8755866)
    ..cubicTo(
        size.width * 0.1013800,
        size.height * 0.8757505,
        size.width * 0.09733250,
        size.height * 0.8750950,
        size.width * 0.09246750,
        size.height * 0.8741315)
    ..cubicTo(
        size.width * 0.08527250,
        size.height * 0.8727062,
        size.width * 0.08355750,
        size.height * 0.8725870,
        size.width * 0.08328000,
        size.height * 0.8734760)
    ..cubicTo(
        size.width * 0.08250500,
        size.height * 0.8759690,
        size.width * 0.08426250,
        size.height * 0.8766345,
        size.width * 0.09938750,
        size.height * 0.8795894)
    ..lineTo(size.width * 0.1148075, size.height * 0.8825989)
    ..lineTo(size.width * 0.1498300, size.height * 0.8825989)
    ..cubicTo(
        size.width * 0.1891800,
        size.height * 0.8825989,
        size.width * 0.1874850,
        size.height * 0.8828522,
        size.width * 0.1911975,
        size.height * 0.8763663)
    ..cubicTo(
        size.width * 0.1935850,
        size.height * 0.8721897,
        size.width * 0.1947050,
        size.height * 0.8675364,
        size.width * 0.1947175,
        size.height * 0.8617409)
    ..lineTo(size.width * 0.1947275, size.height * 0.8573557)
    ..lineTo(size.width * 0.1967475, size.height * 0.8570975)
    ..lineTo(size.width * 0.1987675, size.height * 0.8568392)
    ..lineTo(size.width * 0.1829300, size.height * 0.8391347)
    ..cubicTo(
        size.width * 0.1727850,
        size.height * 0.8277919,
        size.width * 0.1670925,
        size.height * 0.8209633,
        size.width * 0.1670975,
        size.height * 0.8201290)
    ..cubicTo(
        size.width * 0.1671000,
        size.height * 0.8194189,
        size.width * 0.1680550,
        size.height * 0.8097099,
        size.width * 0.1692225,
        size.height * 0.7985608)
    ..cubicTo(
        size.width * 0.1703875,
        size.height * 0.7874117,
        size.width * 0.1713425,
        size.height * 0.7780057,
        size.width * 0.1713425,
        size.height * 0.7776580)
    ..cubicTo(
        size.width * 0.1713450,
        size.height * 0.7768436,
        size.width * 0.1645850,
        size.height * 0.7770174,
        size.width * 0.1457025,
        size.height * 0.7783235)
    ..moveTo(size.width * 0.9778750, size.height * 0.8049622)
    ..cubicTo(
        size.width * 0.9838425,
        size.height * 0.8059257,
        size.width * 0.9888075,
        size.height * 0.8068742,
        size.width * 0.9889075,
        size.height * 0.8070729)
    ..cubicTo(
        size.width * 0.9890050,
        size.height * 0.8072715,
        size.width * 0.9888175,
        size.height * 0.8074404,
        size.width * 0.9884850,
        size.height * 0.8074503)
    ..cubicTo(
        size.width * 0.9881525,
        size.height * 0.8074602,
        size.width * 0.9632025,
        size.height * 0.8135091,
        size.width * 0.9330350,
        size.height * 0.8208839)
    ..cubicTo(
        size.width * 0.9028700,
        size.height * 0.8282587,
        size.width * 0.8781225,
        size.height * 0.8341685,
        size.width * 0.8780400,
        size.height * 0.8340096)
    ..cubicTo(
        size.width * 0.8778825,
        size.height * 0.8337116,
        size.width * 0.8803475,
        size.height * 0.8125655,
        size.width * 0.8805950,
        size.height * 0.8120937)
    ..cubicTo(
        size.width * 0.8807950,
        size.height * 0.8117113,
        size.width * 0.9168525,
        size.height * 0.8078079,
        size.width * 0.9392000,
        size.height * 0.8057568)
    ..lineTo(size.width * 0.9604600, size.height * 0.8037952)
    ..cubicTo(
        size.width * 0.9619800,
        size.height * 0.8036512,
        size.width * 0.9640775,
        size.height * 0.8034575,
        size.width * 0.9651225,
        size.height * 0.8033681)
    ..cubicTo(
        size.width * 0.9661675,
        size.height * 0.8032787,
        size.width * 0.9719075,
        size.height * 0.8039988,
        size.width * 0.9778750,
        size.height * 0.8049622)
    ..moveTo(size.width * 0.7611600, size.height * 0.8158730)
    ..cubicTo(
        size.width * 0.7664500,
        size.height * 0.8158780,
        size.width * 0.7674325,
        size.height * 0.8160766,
        size.width * 0.7674325,
        size.height * 0.8171443)
    ..cubicTo(
        size.width * 0.7674325,
        size.height * 0.8182468,
        size.width * 0.7695225,
        size.height * 0.8184256,
        size.width * 0.7834825,
        size.height * 0.8185200)
    ..cubicTo(
        size.width * 0.8014000,
        size.height * 0.8186392,
        size.width * 0.8013575,
        size.height * 0.8187087,
        size.width * 0.7810375,
        size.height * 0.8226668)
    ..cubicTo(
        size.width * 0.7691150,
        size.height * 0.8249860,
        size.width * 0.7532975,
        size.height * 0.8285467,
        size.width * 0.7491275,
        size.height * 0.8298529)
    ..cubicTo(
        size.width * 0.7476200,
        size.height * 0.8303246,
        size.width * 0.7476175,
        size.height * 0.8303246,
        size.width * 0.7468900,
        size.height * 0.8257359)
    ..cubicTo(
        size.width * 0.7464900,
        size.height * 0.8232130,
        size.width * 0.7456725,
        size.height * 0.8197119,
        size.width * 0.7450725,
        size.height * 0.8179538)
    ..lineTo(size.width * 0.7439825, size.height * 0.8147655)
    ..lineTo(size.width * 0.7494350, size.height * 0.8153168)
    ..cubicTo(
        size.width * 0.7524350,
        size.height * 0.8156197,
        size.width * 0.7577125,
        size.height * 0.8158730,
        size.width * 0.7611600,
        size.height * 0.8158730)
    ..moveTo(size.width * 0.8063100, size.height * 0.8253783)
    ..cubicTo(
        size.width * 0.8064350,
        size.height * 0.8268880,
        size.width * 0.8067350,
        size.height * 0.8305928,
        size.width * 0.8069775,
        size.height * 0.8336123)
    ..cubicTo(
        size.width * 0.8072200,
        size.height * 0.8366317,
        size.width * 0.8077225,
        size.height * 0.8416873,
        size.width * 0.8080950,
        size.height * 0.8448458)
    ..cubicTo(
        size.width * 0.8089400,
        size.height * 0.8519823,
        size.width * 0.8106700,
        size.height * 0.8507159,
        size.width * 0.7946975,
        size.height * 0.8546342)
    ..lineTo(size.width * 0.7809650, size.height * 0.8580063)
    ..lineTo(size.width * 0.7452125, size.height * 0.8542469)
    ..cubicTo(
        size.width * 0.7255475,
        size.height * 0.8521809,
        size.width * 0.7093925,
        size.height * 0.8503533,
        size.width * 0.7093100,
        size.height * 0.8501895)
    ..cubicTo(
        size.width * 0.7092275,
        size.height * 0.8500256,
        size.width * 0.7112225,
        size.height * 0.8487046,
        size.width * 0.7137425,
        size.height * 0.8472495)
    ..cubicTo(
        size.width * 0.7266750,
        size.height * 0.8397952,
        size.width * 0.7530950,
        size.height * 0.8321919,
        size.width * 0.7916675,
        size.height * 0.8248171)
    ..cubicTo(
        size.width * 0.8063550,
        size.height * 0.8220112,
        size.width * 0.8060325,
        size.height * 0.8219963,
        size.width * 0.8063100,
        size.height * 0.8253783)
    ..moveTo(size.width * 0.06776750, size.height * 0.8542369)
    ..cubicTo(
        size.width * 0.06739500,
        size.height * 0.8561787,
        size.width * 0.06751000,
        size.height * 0.8563128,
        size.width * 0.07148250,
        size.height * 0.8586072)
    ..cubicTo(
        size.width * 0.07579000,
        size.height * 0.8610952,
        size.width * 0.07653000,
        size.height * 0.8611499,
        size.width * 0.07653000,
        size.height * 0.8589846)
    ..cubicTo(
        size.width * 0.07653000,
        size.height * 0.8576735,
        size.width * 0.07593250,
        size.height * 0.8569783,
        size.width * 0.07363000,
        size.height * 0.8556175)
    ..cubicTo(
        size.width * 0.06892000,
        size.height * 0.8528315,
        size.width * 0.06808000,
        size.height * 0.8526328,
        size.width * 0.06776750,
        size.height * 0.8542369)
    ..moveTo(size.width * 0.7438350, size.height * 0.8585774)
    ..lineTo(size.width * 0.7808250, size.height * 0.8623865)
    ..lineTo(size.width * 0.7947575, size.height * 0.8589796)
    ..cubicTo(
        size.width * 0.8024200,
        size.height * 0.8571074,
        size.width * 0.8088550,
        size.height * 0.8555728,
        size.width * 0.8090550,
        size.height * 0.8555728)
    ..cubicTo(
        size.width * 0.8092550,
        size.height * 0.8555728,
        size.width * 0.8096675,
        size.height * 0.8568094,
        size.width * 0.8099725,
        size.height * 0.8583191)
    ..cubicTo(
        size.width * 0.8102750,
        size.height * 0.8598289,
        size.width * 0.8108400,
        size.height * 0.8620140,
        size.width * 0.8112300,
        size.height * 0.8631761)
    ..cubicTo(
        size.width * 0.8124350,
        size.height * 0.8667667,
        size.width * 0.8119075,
        size.height * 0.9010335,
        size.width * 0.8106400,
        size.height * 0.9016145)
    ..cubicTo(
        size.width * 0.8101425,
        size.height * 0.9018430,
        size.width * 0.8021275,
        size.height * 0.9039089,
        size.width * 0.7928250,
        size.height * 0.9062083)
    ..lineTo(size.width * 0.7759150, size.height * 0.9103898)
    ..lineTo(size.width * 0.7403175, size.height * 0.9015698)
    ..cubicTo(
        size.width * 0.7207375,
        size.height * 0.8967228,
        size.width * 0.7040975,
        size.height * 0.8925413,
        size.width * 0.7033375,
        size.height * 0.8922830)
    ..lineTo(size.width * 0.7019550, size.height * 0.8918112)
    ..lineTo(size.width * 0.7019550, size.height * 0.8539091)
    ..lineTo(size.width * 0.7044000, size.height * 0.8543412)
    ..cubicTo(
        size.width * 0.7057450,
        size.height * 0.8545796,
        size.width * 0.7234900,
        size.height * 0.8564866,
        size.width * 0.7438350,
        size.height * 0.8585774)
    ..moveTo(size.width * 0.1607150, size.height * 0.8657089)
    ..lineTo(size.width * 0.1607150, size.height * 0.8741514)
    ..lineTo(size.width * 0.1347800, size.height * 0.8741514)
    ..lineTo(size.width * 0.1347800, size.height * 0.8662700)
    ..cubicTo(
        size.width * 0.1347800,
        size.height * 0.8619345,
        size.width * 0.1349075,
        size.height * 0.8581354,
        size.width * 0.1350625,
        size.height * 0.8578225)
    ..cubicTo(
        size.width * 0.1352175,
        size.height * 0.8575146,
        size.width * 0.1410525,
        size.height * 0.8572613,
        size.width * 0.1480300,
        size.height * 0.8572613)
    ..lineTo(size.width * 0.1607150, size.height * 0.8572613)
    ..lineTo(size.width * 0.1607150, size.height * 0.8657089)
    ..moveTo(size.width * 0.1875000, size.height * 0.8590045)
    ..cubicTo(
        size.width * 0.1875000,
        size.height * 0.8674669,
        size.width * 0.1818625,
        size.height * 0.8741514,
        size.width * 0.1747300,
        size.height * 0.8741514)
    ..lineTo(size.width * 0.1704925, size.height * 0.8741514)
    ..lineTo(size.width * 0.1704925, size.height * 0.8662700)
    ..cubicTo(
        size.width * 0.1704925,
        size.height * 0.8619345,
        size.width * 0.1706200,
        size.height * 0.8581354,
        size.width * 0.1707775,
        size.height * 0.8578225)
    ..cubicTo(
        size.width * 0.1716300,
        size.height * 0.8561290,
        size.width * 0.1875000,
        size.height * 0.8572514,
        size.width * 0.1875000,
        size.height * 0.8590045)
    ..moveTo(size.width * 0.05173000, size.height * 0.8677549)
    ..cubicTo(
        size.width * 0.05130250,
        size.height * 0.8699599,
        size.width * 0.05245000,
        size.height * 0.8705658,
        size.width * 0.05960500,
        size.height * 0.8719464)
    ..cubicTo(
        size.width * 0.06878250,
        size.height * 0.8737094,
        size.width * 0.06887750,
        size.height * 0.8737044,
        size.width * 0.06887750,
        size.height * 0.8716335)
    ..cubicTo(
        size.width * 0.06887750,
        size.height * 0.8695527,
        size.width * 0.06869250,
        size.height * 0.8694732,
        size.width * 0.05994250,
        size.height * 0.8678543)
    ..cubicTo(
        size.width * 0.05145000,
        size.height * 0.8662800,
        size.width * 0.05201250,
        size.height * 0.8662849,
        size.width * 0.05173000,
        size.height * 0.8677549)
    ..moveTo(size.width * 0.9570575, size.height * 0.9049121)
    ..lineTo(size.width * 0.9570575, size.height * 0.9398990)
    ..lineTo(size.width * 0.9558875, size.height * 0.9403608)
    ..cubicTo(
        size.width * 0.9552450,
        size.height * 0.9406191,
        size.width * 0.9288900,
        size.height * 0.9488282,
        size.width * 0.8973225,
        size.height * 0.9586116)
    ..cubicTo(
        size.width * 0.8657525,
        size.height * 0.9683951,
        size.width * 0.8393675,
        size.height * 0.9766191,
        size.width * 0.8386900,
        size.height * 0.9768873)
    ..cubicTo(
        size.width * 0.8376100,
        size.height * 0.9773243,
        size.width * 0.8375000,
        size.height * 0.9771902,
        size.width * 0.8377900,
        size.height * 0.9758096)
    ..cubicTo(
        size.width * 0.8407325,
        size.height * 0.9618993,
        size.width * 0.8458775,
        size.height * 0.9269223,
        size.width * 0.8477750,
        size.height * 0.9079365)
    ..lineTo(size.width * 0.8489150, size.height * 0.8965341)
    ..lineTo(size.width * 0.9013925, size.height * 0.8833240)
    ..cubicTo(
        size.width * 0.9302550,
        size.height * 0.8760584,
        size.width * 0.9545875,
        size.height * 0.8700741,
        size.width * 0.9554625,
        size.height * 0.8700245)
    ..lineTo(size.width * 0.9570575, size.height * 0.8699301)
    ..lineTo(size.width * 0.9570575, size.height * 0.9049121)
    ..moveTo(size.width * 0.9575100, size.height * 0.9451284)
    ..cubicTo(
        size.width * 0.9586700,
        size.height * 0.9463600,
        size.width * 0.9585875,
        size.height * 0.9499853,
        size.width * 0.9573775,
        size.height * 0.9510283)
    ..cubicTo(
        size.width * 0.9564150,
        size.height * 0.9518576,
        size.width * 0.8358075,
        size.height * 0.9882996,
        size.width * 0.8355725,
        size.height * 0.9878328)
    ..cubicTo(
        size.width * 0.8355025,
        size.height * 0.9876987,
        size.width * 0.8357175,
        size.height * 0.9862982,
        size.width * 0.8360525,
        size.height * 0.9847240)
    ..lineTo(size.width * 0.8366575, size.height * 0.9818634)
    ..lineTo(size.width * 0.8965400, size.height * 0.9631408)
    ..cubicTo(
        size.width * 0.9294725,
        size.height * 0.9528459,
        size.width * 0.9564800,
        size.height * 0.9443835,
        size.width * 0.9565550,
        size.height * 0.9443388)
    ..cubicTo(
        size.width * 0.9566275,
        size.height * 0.9442941,
        size.width * 0.9570575,
        size.height * 0.9446467,
        size.width * 0.9575100,
        size.height * 0.9451284);
}

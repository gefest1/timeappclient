part of 'take_away_bloc.dart';

abstract class TakeAwayState extends Equatable {
  const TakeAwayState();
  
  @override
  List<Object> get props => [];
}

class TakeAwayInitial extends TakeAwayState {}

// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';

class TimeCustomPainter extends CustomPainter {
 final Color color;
  const TimeCustomPainter({
    Listenable? repaint,
     this.color = Colors.white,
  }) : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint_0_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.09523810;
    paint_0_stroke.color = color;
    canvas.drawCircle(Offset(size.width * 0.5000000, size.height * 0.4545455),
        size.width * 0.3928571, paint_0_stroke);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.5000000, size.height * 0.2840909);
    path_1.lineTo(size.width * 0.5000000, size.height * 0.4886364);
    path_1.lineTo(size.width * 0.5892857, size.height * 0.5738636);

    Paint paint_1_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.09523810;
    paint_1_stroke.color = color;
    paint_1_stroke.strokeCap = StrokeCap.round;
    paint_1_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_1, paint_1_stroke);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}

// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';
import 'package:time_app_components/utils/color.dart';

//Add this CustomPaint widget to the Widget Tree
// CustomPaint(
//     size: Size(WIDTH, (WIDTH*0.9107142857142857).toDouble()), //You can Replace [WIDTH] with your desired width for Custom Paint and height will be calculated automatically
//     painter: RPSCustomPainter(),
// )

//Copy this CustomPainter code to the Bottom of the File
class TimeServiceCustomPainter extends CustomPainter {
  final bool active;

  const TimeServiceCustomPainter({this.active = false, Listenable? repaint})
      : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    final Color activeColor =
        active ? const Color(0xff25BA00) : ColorData.allMainGray;

    Path path_0 = Path()..fillType = PathFillType.evenOdd;
    path_0.moveTo(size.width * 0.3962920, size.height * 0.03240804);
    path_0.cubicTo(
        size.width * 0.3994241,
        size.height * 0.03589618,
        size.width * 0.4026491,
        size.height * 0.03948392,
        size.width * 0.4071634,
        size.height * 0.04127784);
    path_0.cubicTo(
        size.width * 0.4090063,
        size.height * 0.04197549,
        size.width * 0.4109411,
        size.height * 0.04237412,
        size.width * 0.4128750,
        size.height * 0.04237412);
    path_0.cubicTo(
        size.width * 0.4199696,
        size.height * 0.04237412,
        size.width * 0.4269714,
        size.height * 0.03769010,
        size.width * 0.4293670,
        size.height * 0.03031520);
    path_0.cubicTo(
        size.width * 0.4300116,
        size.height * 0.02852127,
        size.width * 0.4291821,
        size.height * 0.02652804,
        size.width * 0.4275241,
        size.height * 0.02583039);
    path_0.cubicTo(
        size.width * 0.4258661,
        size.height * 0.02513284,
        size.width * 0.4240232,
        size.height * 0.02602971,
        size.width * 0.4233786,
        size.height * 0.02782363);
    path_0.cubicTo(
        size.width * 0.4214438,
        size.height * 0.03350431,
        size.width * 0.4146259,
        size.height * 0.03679314,
        size.width * 0.4093741,
        size.height * 0.03470020);
    path_0.cubicTo(
        size.width * 0.4062848,
        size.height * 0.03345873,
        size.width * 0.4037866,
        size.height * 0.03075304,
        size.width * 0.4011518,
        size.height * 0.02789853);
    path_0.cubicTo(
        size.width * 0.4010366,
        size.height * 0.02777412,
        size.width * 0.4009214,
        size.height * 0.02764951,
        size.width * 0.4008063,
        size.height * 0.02752471);
    path_0.cubicTo(
        size.width * 0.3986875,
        size.height * 0.02513284,
        size.width * 0.3964759,
        size.height * 0.02264127,
        size.width * 0.3937125,
        size.height * 0.02074775);
    path_0.cubicTo(
        size.width * 0.3842232,
        size.height * 0.01397078,
        size.width * 0.3700348,
        size.height * 0.01626294,
        size.width * 0.3627571,
        size.height * 0.02583039);
    path_0.cubicTo(
        size.width * 0.3616509,
        size.height * 0.02732529,
        size.width * 0.3618357,
        size.height * 0.02951784,
        size.width * 0.3632179,
        size.height * 0.03071382);
    path_0.cubicTo(
        size.width * 0.3645991,
        size.height * 0.03190971,
        size.width * 0.3666259,
        size.height * 0.03171039,
        size.width * 0.3677321,
        size.height * 0.03021549);
    path_0.cubicTo(
        size.width * 0.3729830,
        size.height * 0.02323922,
        size.width * 0.3833018,
        size.height * 0.02164471,
        size.width * 0.3902116,
        size.height * 0.02652804);
    path_0.cubicTo(
        size.width * 0.3922000,
        size.height * 0.02796216,
        size.width * 0.3938902,
        size.height * 0.02979931,
        size.width * 0.3956848,
        size.height * 0.03174951);
    path_0.cubicTo(
        size.width * 0.3958857,
        size.height * 0.03196775,
        size.width * 0.3960884,
        size.height * 0.03218735,
        size.width * 0.3962920,
        size.height * 0.03240804);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Colors.black;
    canvas.drawPath(path_0, paint_0_fill);

    Path path_1 = Path()..fillType = PathFillType.evenOdd;
    path_1.moveTo(size.width * 0.03147509, size.height * 0.7420059);
    path_1.cubicTo(
        size.width * 0.02972464,
        size.height * 0.7420059,
        size.width * 0.02825054,
        size.height * 0.7404118,
        size.width * 0.02825054,
        size.height * 0.7385176);
    path_1.cubicTo(
        size.width * 0.02825054,
        size.height * 0.7261598,
        size.width * 0.03783205,
        size.height * 0.7146000,
        size.width * 0.04925607,
        size.height * 0.7133039);
    path_1.cubicTo(
        size.width * 0.05248063,
        size.height * 0.7129049,
        size.width * 0.05570518,
        size.height * 0.7133039,
        size.width * 0.05874545,
        size.height * 0.7137029);
    path_1.cubicTo(
        size.width * 0.06261491,
        size.height * 0.7142010,
        size.width * 0.06630009,
        size.height * 0.7145990,
        size.width * 0.06952464,
        size.height * 0.7134039);
    path_1.cubicTo(
        size.width * 0.07477598,
        size.height * 0.7114108,
        size.width * 0.07809268,
        size.height * 0.7042343,
        size.width * 0.07634223,
        size.height * 0.6984549);
    path_1.cubicTo(
        size.width * 0.07578946,
        size.height * 0.6966608,
        size.width * 0.07671071,
        size.height * 0.6946676,
        size.width * 0.07836911,
        size.height * 0.6940696);
    path_1.cubicTo(
        size.width * 0.08002741,
        size.height * 0.6934716,
        size.width * 0.08187000,
        size.height * 0.6944676,
        size.width * 0.08242277,
        size.height * 0.6962618);
    path_1.cubicTo(
        size.width * 0.08537089,
        size.height * 0.7057294,
        size.width * 0.08030375,
        size.height * 0.7167922,
        size.width * 0.07164357,
        size.height * 0.7199814);
    path_1.cubicTo(
        size.width * 0.06703714,
        size.height * 0.7216755,
        size.width * 0.06243063,
        size.height * 0.7211775,
        size.width * 0.05800839,
        size.height * 0.7206784);
    path_1.cubicTo(
        size.width * 0.05515241,
        size.height * 0.7203794,
        size.width * 0.05248063,
        size.height * 0.7199814,
        size.width * 0.04990098,
        size.height * 0.7202804);
    path_1.cubicTo(
        size.width * 0.04170152,
        size.height * 0.7211775,
        size.width * 0.03469964,
        size.height * 0.7295490,
        size.width * 0.03469964,
        size.height * 0.7385176);
    path_1.cubicTo(
        size.width * 0.03469964,
        size.height * 0.7404118,
        size.width * 0.03322554,
        size.height * 0.7420059,
        size.width * 0.03147509,
        size.height * 0.7420059);
    path_1.close();

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = Colors.white;
    canvas.drawPath(path_1, paint_1_fill);

    Path path_2 = Path()..fillType = PathFillType.evenOdd;
    path_2.moveTo(size.width * 0.04990098, size.height * 0.7202804);
    path_2.cubicTo(
        size.width * 0.04170152,
        size.height * 0.7211775,
        size.width * 0.03469964,
        size.height * 0.7295490,
        size.width * 0.03469964,
        size.height * 0.7385176);
    path_2.cubicTo(
        size.width * 0.03469964,
        size.height * 0.7404118,
        size.width * 0.03322554,
        size.height * 0.7420059,
        size.width * 0.03147509,
        size.height * 0.7420059);
    path_2.cubicTo(
        size.width * 0.02972464,
        size.height * 0.7420059,
        size.width * 0.02825054,
        size.height * 0.7404118,
        size.width * 0.02825054,
        size.height * 0.7385176);
    path_2.cubicTo(
        size.width * 0.02825054,
        size.height * 0.7261598,
        size.width * 0.03783205,
        size.height * 0.7146000,
        size.width * 0.04925607,
        size.height * 0.7133039);
    path_2.cubicTo(
        size.width * 0.05248063,
        size.height * 0.7129049,
        size.width * 0.05570518,
        size.height * 0.7133039,
        size.width * 0.05874545,
        size.height * 0.7137029);
    path_2.cubicTo(
        size.width * 0.06261491,
        size.height * 0.7142010,
        size.width * 0.06630009,
        size.height * 0.7145990,
        size.width * 0.06952464,
        size.height * 0.7134039);
    path_2.cubicTo(
        size.width * 0.07477598,
        size.height * 0.7114108,
        size.width * 0.07809268,
        size.height * 0.7042343,
        size.width * 0.07634223,
        size.height * 0.6984549);
    path_2.cubicTo(
        size.width * 0.07578946,
        size.height * 0.6966608,
        size.width * 0.07671071,
        size.height * 0.6946676,
        size.width * 0.07836911,
        size.height * 0.6940696);
    path_2.cubicTo(
        size.width * 0.08002741,
        size.height * 0.6934716,
        size.width * 0.08187000,
        size.height * 0.6944676,
        size.width * 0.08242277,
        size.height * 0.6962618);
    path_2.cubicTo(
        size.width * 0.08537089,
        size.height * 0.7057294,
        size.width * 0.08030375,
        size.height * 0.7167922,
        size.width * 0.07164357,
        size.height * 0.7199814);
    path_2.cubicTo(
        size.width * 0.06703714,
        size.height * 0.7216755,
        size.width * 0.06243063,
        size.height * 0.7211775,
        size.width * 0.05800839,
        size.height * 0.7206784);
    path_2.lineTo(size.width * 0.05800848, size.height * 0.7206784);
    path_2.lineTo(size.width * 0.05800839, size.height * 0.7206784);
    path_2.cubicTo(
        size.width * 0.05758375,
        size.height * 0.7206343,
        size.width * 0.05716312,
        size.height * 0.7205873,
        size.width * 0.05674634,
        size.height * 0.7205412);
    path_2.cubicTo(
        size.width * 0.05435991,
        size.height * 0.7202765,
        size.width * 0.05209705,
        size.height * 0.7200255,
        size.width * 0.04990098,
        size.height * 0.7202804);
    path_2.close();

    Paint paint_2_fill = Paint()..style = PaintingStyle.fill;
    paint_2_fill.color = Colors.black;
    canvas.drawPath(path_2, paint_2_fill);

    Path path_3 = Path()..fillType = PathFillType.evenOdd;
    path_3.moveTo(size.width * 0.7434563, size.height * 0.04777735);
    path_3.cubicTo(
        size.width * 0.7434563,
        size.height * 0.06910480,
        size.width * 0.7594866,
        size.height * 0.08654549,
        size.width * 0.7792946,
        size.height * 0.08654549);
    path_3.cubicTo(
        size.width * 0.7991027,
        size.height * 0.08654549,
        size.width * 0.8151330,
        size.height * 0.06920451,
        size.width * 0.8151330,
        size.height * 0.04777735);
    path_3.cubicTo(
        size.width * 0.8151330,
        size.height * 0.02644990,
        size.width * 0.7991027,
        size.height * 0.009009265,
        size.width * 0.7792946,
        size.height * 0.009009265);
    path_3.cubicTo(
        size.width * 0.7595786,
        size.height * 0.009009265,
        size.width * 0.7434563,
        size.height * 0.02635029,
        size.width * 0.7434563,
        size.height * 0.04777735);
    path_3.close();
    path_3.moveTo(size.width * 0.7489839, size.height * 0.04777735);
    path_3.cubicTo(
        size.width * 0.7489839,
        size.height * 0.02973873,
        size.width * 0.7625268,
        size.height * 0.01498892,
        size.width * 0.7792946,
        size.height * 0.01498892);
    path_3.cubicTo(
        size.width * 0.7959696,
        size.height * 0.01498892,
        size.width * 0.8096054,
        size.height * 0.02963912,
        size.width * 0.8096054,
        size.height * 0.04777735);
    path_3.cubicTo(
        size.width * 0.8096054,
        size.height * 0.06581598,
        size.width * 0.7960625,
        size.height * 0.08056578,
        size.width * 0.7792946,
        size.height * 0.08056578);
    path_3.cubicTo(
        size.width * 0.7626188,
        size.height * 0.08056578,
        size.width * 0.7489839,
        size.height * 0.06591569,
        size.width * 0.7489839,
        size.height * 0.04777735);
    path_3.close();

    Paint paint_3_fill = Paint()..style = PaintingStyle.fill;
    paint_3_fill.color = Colors.black;
    canvas.drawPath(path_3, paint_3_fill);

    Path path_4 = Path()..fillType = PathFillType.evenOdd;
    path_4.moveTo(size.width * 0.06740554, size.height * 0.4385363);
    path_4.cubicTo(
        size.width * 0.05119071,
        size.height * 0.4385363,
        size.width * 0.03801616,
        size.height * 0.4242843,
        size.width * 0.03801616,
        size.height * 0.4067441);
    path_4.cubicTo(
        size.width * 0.03801616,
        size.height * 0.3892039,
        size.width * 0.05119071,
        size.height * 0.3749520,
        size.width * 0.06740554,
        size.height * 0.3749520);
    path_4.cubicTo(
        size.width * 0.08362036,
        size.height * 0.3749520,
        size.width * 0.09679464,
        size.height * 0.3892039,
        size.width * 0.09679464,
        size.height * 0.4067441);
    path_4.cubicTo(
        size.width * 0.09679464,
        size.height * 0.4242843,
        size.width * 0.08362036,
        size.height * 0.4385363,
        size.width * 0.06740554,
        size.height * 0.4385363);
    path_4.close();
    path_4.moveTo(size.width * 0.06740554, size.height * 0.3809314);
    path_4.cubicTo(
        size.width * 0.05423098,
        size.height * 0.3809314,
        size.width * 0.04354393,
        size.height * 0.3924922,
        size.width * 0.04354393,
        size.height * 0.4067441);
    path_4.cubicTo(
        size.width * 0.04354393,
        size.height * 0.4209951,
        size.width * 0.05423098,
        size.height * 0.4325559,
        size.width * 0.06740554,
        size.height * 0.4325559);
    path_4.cubicTo(
        size.width * 0.08058009,
        size.height * 0.4325559,
        size.width * 0.09126696,
        size.height * 0.4209951,
        size.width * 0.09126696,
        size.height * 0.4067441);
    path_4.cubicTo(
        size.width * 0.09126696,
        size.height * 0.3924922,
        size.width * 0.08058009,
        size.height * 0.3809314,
        size.width * 0.06740554,
        size.height * 0.3809314);
    path_4.close();

    Paint paint_4_fill = Paint()..style = PaintingStyle.fill;
    paint_4_fill.color = Colors.white;
    canvas.drawPath(path_4, paint_4_fill);

    Path path_5 = Path()..fillType = PathFillType.evenOdd;
    path_5.moveTo(size.width * 0.03801616, size.height * 0.4067441);
    path_5.cubicTo(
        size.width * 0.03801616,
        size.height * 0.4242843,
        size.width * 0.05119071,
        size.height * 0.4385363,
        size.width * 0.06740554,
        size.height * 0.4385363);
    path_5.cubicTo(
        size.width * 0.08362036,
        size.height * 0.4385363,
        size.width * 0.09679464,
        size.height * 0.4242843,
        size.width * 0.09679464,
        size.height * 0.4067441);
    path_5.cubicTo(
        size.width * 0.09679464,
        size.height * 0.3892039,
        size.width * 0.08362036,
        size.height * 0.3749520,
        size.width * 0.06740554,
        size.height * 0.3749520);
    path_5.cubicTo(
        size.width * 0.05119071,
        size.height * 0.3749520,
        size.width * 0.03801616,
        size.height * 0.3892039,
        size.width * 0.03801616,
        size.height * 0.4067441);
    path_5.close();
    path_5.moveTo(size.width * 0.04354393, size.height * 0.4067441);
    path_5.cubicTo(
        size.width * 0.04354393,
        size.height * 0.3924922,
        size.width * 0.05423098,
        size.height * 0.3809314,
        size.width * 0.06740554,
        size.height * 0.3809314);
    path_5.cubicTo(
        size.width * 0.08058009,
        size.height * 0.3809314,
        size.width * 0.09126696,
        size.height * 0.3924922,
        size.width * 0.09126696,
        size.height * 0.4067441);
    path_5.cubicTo(
        size.width * 0.09126696,
        size.height * 0.4209951,
        size.width * 0.08058009,
        size.height * 0.4325559,
        size.width * 0.06740554,
        size.height * 0.4325559);
    path_5.cubicTo(
        size.width * 0.05423098,
        size.height * 0.4325559,
        size.width * 0.04354393,
        size.height * 0.4209951,
        size.width * 0.04354393,
        size.height * 0.4067441);
    path_5.close();

    Paint paint_5_fill = Paint()..style = PaintingStyle.fill;
    paint_5_fill.color = Colors.black;
    canvas.drawPath(path_5, paint_5_fill);

    Path path_6 = Path()..fillType = PathFillType.evenOdd;
    path_6.moveTo(size.width * 0.9363571, size.height * 0.2156235);
    path_6.cubicTo(
        size.width * 0.9373482,
        size.height * 0.2158775,
        size.width * 0.9384107,
        size.height * 0.2152824,
        size.width * 0.9386875,
        size.height * 0.2142098);
    path_6.cubicTo(
        size.width * 0.9390536,
        size.height * 0.2131137,
        size.width * 0.9384107,
        size.height * 0.2120176,
        size.width * 0.9374821,
        size.height * 0.2116186);
    path_6.lineTo(size.width * 0.9126071, size.height * 0.2031471);
    path_6.lineTo(size.width * 0.9357321, size.height * 0.1900922);
    path_6.cubicTo(
        size.width * 0.9366607,
        size.height * 0.1895931,
        size.width * 0.9370268,
        size.height * 0.1882980,
        size.width * 0.9365625,
        size.height * 0.1873010);
    path_6.cubicTo(
        size.width * 0.9361161,
        size.height * 0.1863324,
        size.width * 0.9349732,
        size.height * 0.1859294,
        size.width * 0.9340625,
        size.height * 0.1863647);
    path_6.cubicTo(
        size.width * 0.9340446,
        size.height * 0.1863735,
        size.width * 0.9340268,
        size.height * 0.1863843,
        size.width * 0.9340000,
        size.height * 0.1863941);
    path_6.cubicTo(
        size.width * 0.9340000,
        size.height * 0.1863971,
        size.width * 0.9339911,
        size.height * 0.1864010,
        size.width * 0.9339821,
        size.height * 0.1864039);
    path_6.lineTo(size.width * 0.9108571, size.height * 0.1994598);
    path_6.lineTo(size.width * 0.9186875, size.height * 0.1725520);
    path_6.cubicTo(
        size.width * 0.9186964,
        size.height * 0.1725284,
        size.width * 0.9187054,
        size.height * 0.1725039,
        size.width * 0.9187054,
        size.height * 0.1724804);
    path_6.cubicTo(
        size.width * 0.9189464,
        size.height * 0.1714039,
        size.width * 0.9183929,
        size.height * 0.1702529,
        size.width * 0.9174018,
        size.height * 0.1699598);
    path_6.cubicTo(
        size.width * 0.9164196,
        size.height * 0.1696696,
        size.width * 0.9153393,
        size.height * 0.1702275,
        size.width * 0.9150268,
        size.height * 0.1712667);
    path_6.cubicTo(
        size.width * 0.9150268,
        size.height * 0.1712961,
        size.width * 0.9150179,
        size.height * 0.1713255,
        size.width * 0.9150089,
        size.height * 0.1713559);
    path_6.lineTo(size.width * 0.9071786, size.height * 0.1982637);
    path_6.lineTo(size.width * 0.8951071, size.height * 0.1732490);
    path_6.cubicTo(
        size.width * 0.8951071,
        size.height * 0.1732510,
        size.width * 0.8951071,
        size.height * 0.1732471,
        size.width * 0.8951071,
        size.height * 0.1732490);
    path_6.cubicTo(
        size.width * 0.8950982,
        size.height * 0.1732275,
        size.width * 0.8950804,
        size.height * 0.1732010,
        size.width * 0.8950714,
        size.height * 0.1731794);
    path_6.cubicTo(
        size.width * 0.8945893,
        size.height * 0.1722343,
        size.width * 0.8934286,
        size.height * 0.1718657,
        size.width * 0.8925268,
        size.height * 0.1723520);
    path_6.cubicTo(
        size.width * 0.8916054,
        size.height * 0.1728500,
        size.width * 0.8912375,
        size.height * 0.1741461,
        size.width * 0.8916982,
        size.height * 0.1751431);
    path_6.lineTo(size.width * 0.9037679, size.height * 0.2001578);
    path_6.lineTo(size.width * 0.8788920, size.height * 0.1916863);
    path_6.cubicTo(
        size.width * 0.8788509,
        size.height * 0.1916745,
        size.width * 0.8788107,
        size.height * 0.1916637,
        size.width * 0.8787696,
        size.height * 0.1916549);
    path_6.cubicTo(
        size.width * 0.8777911,
        size.height * 0.1914363,
        size.width * 0.8767616,
        size.height * 0.1920294,
        size.width * 0.8764964,
        size.height * 0.1930814);
    path_6.cubicTo(
        size.width * 0.8762214,
        size.height * 0.1941716,
        size.width * 0.8767661,
        size.height * 0.1953588,
        size.width * 0.8777679,
        size.height * 0.1956667);
    path_6.cubicTo(
        size.width * 0.8777741,
        size.height * 0.1956686,
        size.width * 0.8777804,
        size.height * 0.1956706,
        size.width * 0.8777866,
        size.height * 0.1956725);
    path_6.lineTo(size.width * 0.9026607, size.height * 0.2041441);
    path_6.lineTo(size.width * 0.8795366, size.height * 0.2172000);
    path_6.cubicTo(
        size.width * 0.8786152,
        size.height * 0.2176980,
        size.width * 0.8782473,
        size.height * 0.2189931,
        size.width * 0.8787080,
        size.height * 0.2199902);
    path_6.cubicTo(
        size.width * 0.8789839,
        size.height * 0.2204882,
        size.width * 0.8793527,
        size.height * 0.2208873,
        size.width * 0.8798134,
        size.height * 0.2209863);
    path_6.cubicTo(
        size.width * 0.8802473,
        size.height * 0.2211745,
        size.width * 0.8807625,
        size.height * 0.2210971,
        size.width * 0.8812062,
        size.height * 0.2209206);
    path_6.cubicTo(
        size.width * 0.8812268,
        size.height * 0.2209127,
        size.width * 0.8812473,
        size.height * 0.2209039,
        size.width * 0.8812679,
        size.height * 0.2208951);
    path_6.cubicTo(
        size.width * 0.8812741,
        size.height * 0.2208931,
        size.width * 0.8812804,
        size.height * 0.2208902,
        size.width * 0.8812875,
        size.height * 0.2208873);
    path_6.lineTo(size.width * 0.9044107, size.height * 0.2078314);
    path_6.lineTo(size.width * 0.8965804, size.height * 0.2347402);
    path_6.cubicTo(
        size.width * 0.8965714,
        size.height * 0.2347637,
        size.width * 0.8965714,
        size.height * 0.2347873,
        size.width * 0.8965625,
        size.height * 0.2348108);
    path_6.cubicTo(
        size.width * 0.8963304,
        size.height * 0.2358873,
        size.width * 0.8968750,
        size.height * 0.2370382,
        size.width * 0.8978750,
        size.height * 0.2373314);
    path_6.cubicTo(
        size.width * 0.8988571,
        size.height * 0.2376216,
        size.width * 0.8999286,
        size.height * 0.2370637,
        size.width * 0.9002411,
        size.height * 0.2360245);
    path_6.cubicTo(
        size.width * 0.9002500,
        size.height * 0.2359951,
        size.width * 0.9002589,
        size.height * 0.2359657,
        size.width * 0.9002679,
        size.height * 0.2359363);
    path_6.lineTo(size.width * 0.9080982, size.height * 0.2090275);
    path_6.lineTo(size.width * 0.9201696, size.height * 0.2340422);
    path_6.cubicTo(
        size.width * 0.9201607,
        size.height * 0.2340412,
        size.width * 0.9201696,
        size.height * 0.2340441,
        size.width * 0.9201696,
        size.height * 0.2340422);
    path_6.cubicTo(
        size.width * 0.9201786,
        size.height * 0.2340667,
        size.width * 0.9201964,
        size.height * 0.2340961,
        size.width * 0.9202143,
        size.height * 0.2341206);
    path_6.cubicTo(
        size.width * 0.9204821,
        size.height * 0.2345824,
        size.width * 0.9208393,
        size.height * 0.2349441,
        size.width * 0.9212679,
        size.height * 0.2350392);
    path_6.cubicTo(
        size.width * 0.9217321,
        size.height * 0.2352382,
        size.width * 0.9222857,
        size.height * 0.2351382,
        size.width * 0.9227411,
        size.height * 0.2349392);
    path_6.cubicTo(
        size.width * 0.9236696,
        size.height * 0.2344412,
        size.width * 0.9240357,
        size.height * 0.2331451,
        size.width * 0.9235714,
        size.height * 0.2321490);
    path_6.lineTo(size.width * 0.9114107, size.height * 0.2071333);
    path_6.lineTo(size.width * 0.9362857, size.height * 0.2156049);
    path_6.cubicTo(
        size.width * 0.9363125,
        size.height * 0.2156118,
        size.width * 0.9363304,
        size.height * 0.2156176,
        size.width * 0.9363571,
        size.height * 0.2156235);
    path_6.close();

    Paint paint_6_fill = Paint()..style = PaintingStyle.fill;
    paint_6_fill.color = Colors.black;
    canvas.drawPath(path_6, paint_6_fill);

    Path path_7 = Path()..fillType = PathFillType.evenOdd;
    path_7.moveTo(size.width * 0.09855446, size.height * 0.05327549);
    path_7.lineTo(size.width * 0.1212188, size.height * 0.02875892);
    path_7.cubicTo(
        size.width * 0.1221402,
        size.height * 0.02776225,
        size.width * 0.1236143,
        size.height * 0.02776225,
        size.width * 0.1245357,
        size.height * 0.02875892);
    path_7.cubicTo(
        size.width * 0.1254571,
        size.height * 0.02975549,
        size.width * 0.1254571,
        size.height * 0.03135010,
        size.width * 0.1245357,
        size.height * 0.03234667);
    path_7.lineTo(size.width * 0.1018714, size.height * 0.05686333);
    path_7.lineTo(size.width * 0.1339330, size.height * 0.05686333);
    path_7.cubicTo(
        size.width * 0.1352223,
        size.height * 0.05686333,
        size.width * 0.1363277,
        size.height * 0.05795951,
        size.width * 0.1364205,
        size.height * 0.05935480);
    path_7.cubicTo(
        size.width * 0.1364205,
        size.height * 0.06084971,
        size.width * 0.1353143,
        size.height * 0.06194598,
        size.width * 0.1340250,
        size.height * 0.06194598);
    path_7.lineTo(size.width * 0.1019634, size.height * 0.06194598);
    path_7.lineTo(size.width * 0.1246277, size.height * 0.08646255);
    path_7.cubicTo(
        size.width * 0.1255491,
        size.height * 0.08745922,
        size.width * 0.1255491,
        size.height * 0.08905373,
        size.width * 0.1246277,
        size.height * 0.09005039);
    path_7.cubicTo(
        size.width * 0.1241670,
        size.height * 0.09054873,
        size.width * 0.1236143,
        size.height * 0.09074804,
        size.width * 0.1229696,
        size.height * 0.09074804);
    path_7.cubicTo(
        size.width * 0.1224161,
        size.height * 0.09074804,
        size.width * 0.1217714,
        size.height * 0.09054873,
        size.width * 0.1213107,
        size.height * 0.09005039);
    path_7.lineTo(size.width * 0.09864732, size.height * 0.06553382);
    path_7.lineTo(size.width * 0.09864732, size.height * 0.1002157);
    path_7.cubicTo(
        size.width * 0.09864732,
        size.height * 0.1017108,
        size.width * 0.09754107,
        size.height * 0.1028069,
        size.width * 0.09625179,
        size.height * 0.1028069);
    path_7.cubicTo(
        size.width * 0.09486964,
        size.height * 0.1028069,
        size.width * 0.09385625,
        size.height * 0.1016108,
        size.width * 0.09385625,
        size.height * 0.1002157);
    path_7.lineTo(size.width * 0.09385625, size.height * 0.06563343);
    path_7.lineTo(size.width * 0.07119241, size.height * 0.09015000);
    path_7.cubicTo(
        size.width * 0.07073179,
        size.height * 0.09064833,
        size.width * 0.07017902,
        size.height * 0.09084765,
        size.width * 0.06953411,
        size.height * 0.09084765);
    path_7.cubicTo(
        size.width * 0.06898134,
        size.height * 0.09084765,
        size.width * 0.06833643,
        size.height * 0.09064833,
        size.width * 0.06787571,
        size.height * 0.09015000);
    path_7.cubicTo(
        size.width * 0.06695446,
        size.height * 0.08915343,
        size.width * 0.06695446,
        size.height * 0.08755882,
        size.width * 0.06787571,
        size.height * 0.08656225);
    path_7.lineTo(size.width * 0.09053929, size.height * 0.06204569);
    path_7.lineTo(size.width * 0.05838643, size.height * 0.06204569);
    path_7.cubicTo(
        size.width * 0.05700446,
        size.height * 0.06204569,
        size.width * 0.05599107,
        size.height * 0.06084971,
        size.width * 0.05599107,
        size.height * 0.05945451);
    path_7.cubicTo(
        size.width * 0.05599107,
        size.height * 0.05795951,
        size.width * 0.05709661,
        size.height * 0.05686333,
        size.width * 0.05838643,
        size.height * 0.05686333);
    path_7.lineTo(size.width * 0.09053929, size.height * 0.05686333);
    path_7.lineTo(size.width * 0.06778366, size.height * 0.03224706);
    path_7.cubicTo(
        size.width * 0.06686232,
        size.height * 0.03125039,
        size.width * 0.06686232,
        size.height * 0.02965588,
        size.width * 0.06778366,
        size.height * 0.02865922);
    path_7.cubicTo(
        size.width * 0.06870491,
        size.height * 0.02766265,
        size.width * 0.07017902,
        size.height * 0.02766265,
        size.width * 0.07110027,
        size.height * 0.02865922);
    path_7.lineTo(size.width * 0.09376429, size.height * 0.05317588);
    path_7.lineTo(size.width * 0.09376429, size.height * 0.01849382);
    path_7.cubicTo(
        size.width * 0.09376429,
        size.height * 0.01699892,
        size.width * 0.09486964,
        size.height * 0.01590265,
        size.width * 0.09615982,
        size.height * 0.01590265);
    path_7.cubicTo(
        size.width * 0.09754107,
        size.height * 0.01590265,
        size.width * 0.09855446,
        size.height * 0.01709863,
        size.width * 0.09855446,
        size.height * 0.01849382);
    path_7.lineTo(size.width * 0.09855446, size.height * 0.05327549);
    path_7.close();

    Paint paint_7_fill = Paint()..style = PaintingStyle.fill;
    paint_7_fill.color = Colors.black;
    canvas.drawPath(path_7, paint_7_fill);

    Path path_8 = Path()..fillType = PathFillType.evenOdd;
    path_8.moveTo(size.width * 0.1784438, size.height * 0.6515843);
    path_8.cubicTo(
        size.width * 0.1531080,
        size.height * 0.6585608,
        size.width * 0.1421446,
        size.height * 0.6747059,
        size.width * 0.1403027,
        size.height * 0.6875627);
    path_8.cubicTo(
        size.width * 0.1388286,
        size.height * 0.6982265,
        size.width * 0.1432509,
        size.height * 0.7078931,
        size.width * 0.1524634,
        size.height * 0.7139725);
    path_8.cubicTo(
        size.width * 0.1679411,
        size.height * 0.7242373,
        size.width * 0.1881179,
        size.height * 0.7182578,
        size.width * 0.2219295,
        size.height * 0.6934422);
    path_8.cubicTo(
        size.width * 0.2353804,
        size.height * 0.6835755,
        size.width * 0.2484625,
        size.height * 0.6789912,
        size.width * 0.2571223,
        size.height * 0.6768990);
    path_8.cubicTo(
        size.width * 0.2580438,
        size.height * 0.6766990,
        size.width * 0.2589652,
        size.height * 0.6769980,
        size.width * 0.2596098,
        size.height * 0.6776961);
    path_8.cubicTo(
        size.width * 0.2603009,
        size.height * 0.6783931,
        size.width * 0.2609920,
        size.height * 0.6791157,
        size.width * 0.2616830,
        size.height * 0.6798382);
    path_8.cubicTo(
        size.width * 0.2623741,
        size.height * 0.6805608,
        size.width * 0.2630652,
        size.height * 0.6812833,
        size.width * 0.2637563,
        size.height * 0.6819814);
    path_8.lineTo(size.width * 0.2737982, size.height * 0.6756029);
    path_8.cubicTo(
        size.width * 0.2773911,
        size.height * 0.6667333,
        size.width * 0.2839321,
        size.height * 0.6538765,
        size.width * 0.2945277,
        size.height * 0.6412196);
    path_8.cubicTo(
        size.width * 0.3241929,
        size.height * 0.6057402,
        size.width * 0.3336821,
        size.height * 0.5835167,
        size.width * 0.3270491,
        size.height * 0.5644814);
    path_8.cubicTo(
        size.width * 0.3233643,
        size.height * 0.5539167,
        size.width * 0.3159018,
        size.height * 0.5477382,
        size.width * 0.3065045,
        size.height * 0.5474392);
    path_8.cubicTo(
        size.width * 0.2944348,
        size.height * 0.5470402,
        size.width * 0.2760089,
        size.height * 0.5571059,
        size.width * 0.2640321,
        size.height * 0.5865059);
    path_8.cubicTo(
        size.width * 0.2568464,
        size.height * 0.6041461,
        size.width * 0.2509500,
        size.height * 0.6245765,
        size.width * 0.2637563,
        size.height * 0.6621490);
    path_8.cubicTo(
        size.width * 0.2642170,
        size.height * 0.6634441,
        size.width * 0.2637563,
        size.height * 0.6649392,
        size.width * 0.2626509,
        size.height * 0.6657363);
    path_8.cubicTo(
        size.width * 0.2615446,
        size.height * 0.6665333,
        size.width * 0.2601634,
        size.height * 0.6663343,
        size.width * 0.2592420,
        size.height * 0.6653382);
    path_8.cubicTo(
        size.width * 0.2428429,
        size.height * 0.6474990,
        size.width * 0.2195339,
        size.height * 0.6194941,
        size.width * 0.2012920,
        size.height * 0.5874029);
    path_8.cubicTo(
        size.width * 0.2007393,
        size.height * 0.5864069,
        size.width * 0.2007393,
        size.height * 0.5851108,
        size.width * 0.2013839,
        size.height * 0.5842137);
    path_8.cubicTo(
        size.width * 0.2064518,
        size.height * 0.5759422,
        size.width * 0.2154804,
        size.height * 0.5638833,
        size.width * 0.2293000,
        size.height * 0.5532196);
    path_8.cubicTo(
        size.width * 0.2632955,
        size.height * 0.5269088,
        size.width * 0.2761018,
        size.height * 0.5083716,
        size.width * 0.2735214,
        size.height * 0.4894363);
    path_8.cubicTo(
        size.width * 0.2721402,
        size.height * 0.4789716,
        size.width * 0.2663357,
        size.height * 0.4715971,
        size.width * 0.2577679,
        size.height * 0.4693049);
    path_8.cubicTo(
        size.width * 0.2466196,
        size.height * 0.4663147,
        size.width * 0.2278259,
        size.height * 0.4715971,
        size.width * 0.2113339,
        size.height * 0.4961137);
    path_8.cubicTo(
        size.width * 0.2012000,
        size.height * 0.5112618,
        size.width * 0.1918027,
        size.height * 0.5292010,
        size.width * 0.1974232,
        size.height * 0.5683676);
    path_8.cubicTo(
        size.width * 0.1976071,
        size.height * 0.5698627,
        size.width * 0.1968696,
        size.height * 0.5712578,
        size.width * 0.1955804,
        size.height * 0.5716569);
    path_8.cubicTo(
        size.width * 0.1942902,
        size.height * 0.5721549,
        size.width * 0.1929080,
        size.height * 0.5714569,
        size.width * 0.1922634,
        size.height * 0.5701618);
    path_8.cubicTo(
        size.width * 0.1844000,
        size.height * 0.5535265,
        size.width * 0.1768670,
        size.height * 0.5325873,
        size.width * 0.1699018,
        size.height * 0.5079412);
    path_8.cubicTo(
        size.width * 0.1695232,
        size.height * 0.5066010,
        size.width * 0.1691455,
        size.height * 0.5052490,
        size.width * 0.1687705,
        size.height * 0.5038873);
    path_8.cubicTo(
        size.width * 0.1687080,
        size.height * 0.5037059,
        size.width * 0.1686687,
        size.height * 0.5035147,
        size.width * 0.1686509,
        size.height * 0.5033186);
    path_8.cubicTo(
        size.width * 0.1686509,
        size.height * 0.5033176,
        size.width * 0.1686509,
        size.height * 0.5033167,
        size.width * 0.1686509,
        size.height * 0.5033157);
    path_8.cubicTo(
        size.width * 0.1685929,
        size.height * 0.5026510,
        size.width * 0.1687830,
        size.height * 0.5019343,
        size.width * 0.1691393,
        size.height * 0.5013961);
    path_8.cubicTo(
        size.width * 0.1694170,
        size.height * 0.5009412,
        size.width * 0.1697009,
        size.height * 0.5004931,
        size.width * 0.1699893,
        size.height * 0.5000539);
    path_8.cubicTo(
        size.width * 0.1736277,
        size.height * 0.4945206,
        size.width * 0.1781411,
        size.height * 0.4901980,
        size.width * 0.1836036,
        size.height * 0.4872441);
    path_8.cubicTo(
        size.width * 0.2070045,
        size.height * 0.4745873,
        size.width * 0.2161250,
        size.height * 0.4642225,
        size.width * 0.2151116,
        size.height * 0.4512667);
    path_8.cubicTo(
        size.width * 0.2144670,
        size.height * 0.4431941,
        size.width * 0.2104134,
        size.height * 0.4372137,
        size.width * 0.2041482,
        size.height * 0.4351216);
    path_8.cubicTo(
        size.width * 0.1938295,
        size.height * 0.4317324,
        size.width * 0.1833268,
        size.height * 0.4399049,
        size.width * 0.1768777,
        size.height * 0.4485755);
    path_8.cubicTo(
        size.width * 0.1697179,
        size.height * 0.4583735,
        size.width * 0.1621545,
        size.height * 0.4748127,
        size.width * 0.1653045,
        size.height * 0.4988735);
    path_8.cubicTo(
        size.width * 0.1655187,
        size.height * 0.5005098,
        size.width * 0.1657821,
        size.height * 0.5021804,
        size.width * 0.1660991,
        size.height * 0.5038873);
    path_8.cubicTo(
        size.width * 0.1661536,
        size.height * 0.5041833,
        size.width * 0.1661688,
        size.height * 0.5044765,
        size.width * 0.1661473,
        size.height * 0.5047588);
    path_8.cubicTo(
        size.width * 0.1661411,
        size.height * 0.5048382,
        size.width * 0.1661321,
        size.height * 0.5049167,
        size.width * 0.1661205,
        size.height * 0.5049951);
    path_8.cubicTo(
        size.width * 0.1660134,
        size.height * 0.5057098,
        size.width * 0.1656696,
        size.height * 0.5063441,
        size.width * 0.1651527,
        size.height * 0.5067922);
    path_8.cubicTo(
        size.width * 0.1650884,
        size.height * 0.5068480,
        size.width * 0.1650223,
        size.height * 0.5069000,
        size.width * 0.1649527,
        size.height * 0.5069500);
    path_8.cubicTo(
        size.width * 0.1647687,
        size.height * 0.5070814,
        size.width * 0.1645661,
        size.height * 0.5071912,
        size.width * 0.1643482,
        size.height * 0.5072755);
    path_8.cubicTo(
        size.width * 0.1637955,
        size.height * 0.5074892,
        size.width * 0.1632089,
        size.height * 0.5075010,
        size.width * 0.1626688,
        size.height * 0.5073441);
    path_8.cubicTo(
        size.width * 0.1626455,
        size.height * 0.5073373,
        size.width * 0.1626223,
        size.height * 0.5073304,
        size.width * 0.1625991,
        size.height * 0.5073225);
    path_8.cubicTo(
        size.width * 0.1619063,
        size.height * 0.5071000,
        size.width * 0.1612964,
        size.height * 0.5065971,
        size.width * 0.1609393,
        size.height * 0.5058804);
    path_8.cubicTo(
        size.width * 0.1606223,
        size.height * 0.5052353,
        size.width * 0.1603009,
        size.height * 0.5046020,
        size.width * 0.1599759,
        size.height * 0.5039784);
    path_8.cubicTo(
        size.width * 0.1480473,
        size.height * 0.4810863,
        size.width * 0.1313125,
        size.height * 0.4724039,
        size.width * 0.1192045,
        size.height * 0.4691059);
    path_8.cubicTo(
        size.width * 0.1024375,
        size.height * 0.4646206,
        size.width * 0.09175000,
        size.height * 0.4702020,
        size.width * 0.08732795,
        size.height * 0.4769784);
    path_8.cubicTo(
        size.width * 0.08336634,
        size.height * 0.4830578,
        size.width * 0.08355063,
        size.height * 0.4909314,
        size.width * 0.08788071,
        size.height * 0.4980069);
    path_8.cubicTo(
        size.width * 0.09488214,
        size.height * 0.5095676,
        size.width * 0.1089786,
        size.height * 0.5119598,
        size.width * 0.1368018,
        size.height * 0.5063784);
    path_8.cubicTo(
        size.width * 0.1445500,
        size.height * 0.5047931,
        size.width * 0.1515580,
        size.height * 0.5060676,
        size.width * 0.1569045,
        size.height * 0.5079931);
    path_8.cubicTo(
        size.width * 0.1570036,
        size.height * 0.5080284,
        size.width * 0.1571018,
        size.height * 0.5080647,
        size.width * 0.1572000,
        size.height * 0.5081010);
    path_8.cubicTo(
        size.width * 0.1572080,
        size.height * 0.5081039,
        size.width * 0.1572161,
        size.height * 0.5081069,
        size.width * 0.1572250,
        size.height * 0.5081098);
    path_8.cubicTo(
        size.width * 0.1588063,
        size.height * 0.5086941,
        size.width * 0.1602375,
        size.height * 0.5093333,
        size.width * 0.1614920,
        size.height * 0.5099667);
    path_8.cubicTo(
        size.width * 0.1622295,
        size.height * 0.5103647,
        size.width * 0.1627821,
        size.height * 0.5109627,
        size.width * 0.1629661,
        size.height * 0.5117608);
    path_8.cubicTo(
        size.width * 0.1630223,
        size.height * 0.5119559,
        size.width * 0.1630777,
        size.height * 0.5121500,
        size.width * 0.1631339,
        size.height * 0.5123451);
    path_8.cubicTo(
        size.width * 0.1697205,
        size.height * 0.5353176,
        size.width * 0.1768482,
        size.height * 0.5551510,
        size.width * 0.1842482,
        size.height * 0.5713578);
    path_8.cubicTo(
        size.width * 0.1847107,
        size.height * 0.5724422,
        size.width * 0.1845929,
        size.height * 0.5737363,
        size.width * 0.1838929,
        size.height * 0.5745971);
    path_8.cubicTo(
        size.width * 0.1837571,
        size.height * 0.5747647,
        size.width * 0.1835991,
        size.height * 0.5749157,
        size.width * 0.1834187,
        size.height * 0.5750451);
    path_8.cubicTo(
        size.width * 0.1833589,
        size.height * 0.5750931,
        size.width * 0.1832982,
        size.height * 0.5751392,
        size.width * 0.1832366,
        size.height * 0.5751814);
    path_8.cubicTo(
        size.width * 0.1821554,
        size.height * 0.5759284,
        size.width * 0.1807893,
        size.height * 0.5757882,
        size.width * 0.1799179,
        size.height * 0.5748461);
    path_8.cubicTo(
        size.width * 0.1639795,
        size.height * 0.5586010,
        size.width * 0.1509893,
        size.height * 0.5561098,
        size.width * 0.1390125,
        size.height * 0.5549137);
    path_8.cubicTo(
        size.width * 0.1157036,
        size.height * 0.5526216,
        size.width * 0.1017920,
        size.height * 0.5629863,
        size.width * 0.09654107,
        size.height * 0.5734510);
    path_8.cubicTo(
        size.width * 0.09202679,
        size.height * 0.5826196,
        size.width * 0.09304018,
        size.height * 0.5928843,
        size.width * 0.09948929,
        size.height * 0.6016549);
    path_8.cubicTo(
        size.width * 0.1100839,
        size.height * 0.6160059,
        size.width * 0.1290625,
        size.height * 0.6174010,
        size.width * 0.1650857,
        size.height * 0.6067373);
    path_8.cubicTo(
        size.width * 0.1798259,
        size.height * 0.6023520,
        size.width * 0.1927241,
        size.height * 0.6027510,
        size.width * 0.2009241,
        size.height * 0.6039471);
    path_8.cubicTo(
        size.width * 0.2017527,
        size.height * 0.6040461,
        size.width * 0.2023982,
        size.height * 0.6045451,
        size.width * 0.2028580,
        size.height * 0.6052422);
    path_8.cubicTo(
        size.width * 0.2134536,
        size.height * 0.6220853,
        size.width * 0.2259830,
        size.height * 0.6391275,
        size.width * 0.2402634,
        size.height * 0.6558696);
    path_8.cubicTo(
        size.width * 0.2411839,
        size.height * 0.6569667,
        size.width * 0.2412768,
        size.height * 0.6585608,
        size.width * 0.2404473,
        size.height * 0.6597569);
    path_8.cubicTo(
        size.width * 0.2396179,
        size.height * 0.6609529,
        size.width * 0.2381438,
        size.height * 0.6612520,
        size.width * 0.2369464,
        size.height * 0.6605539);
    path_8.cubicTo(
        size.width * 0.2209161,
        size.height * 0.6514853,
        size.width * 0.2085705,
        size.height * 0.6484951,
        size.width * 0.1981598,
        size.height * 0.6484951);
    path_8.cubicTo(
        size.width * 0.1908812,
        size.height * 0.6484951,
        size.width * 0.1845250,
        size.height * 0.6498902,
        size.width * 0.1784438,
        size.height * 0.6515843);
    path_8.close();
    path_8.moveTo(size.width * 0.09497500, size.height * 0.6057402);
    path_8.cubicTo(
        size.width * 0.08723580,
        size.height * 0.5951765,
        size.width * 0.08585384,
        size.height * 0.5822206,
        size.width * 0.09147411,
        size.height * 0.5709588);
    path_8.cubicTo(
        size.width * 0.09755446,
        size.height * 0.5588000,
        size.width * 0.1132161,
        size.height * 0.5467412,
        size.width * 0.1392893,
        size.height * 0.5492333);
    path_8.cubicTo(
        size.width * 0.1476045,
        size.height * 0.5500578,
        size.width * 0.1568687,
        size.height * 0.5515667,
        size.width * 0.1672893,
        size.height * 0.5577206);
    path_8.cubicTo(
        size.width * 0.1694518,
        size.height * 0.5589980,
        size.width * 0.1716652,
        size.height * 0.5604755,
        size.width * 0.1739295,
        size.height * 0.5621892);
    path_8.cubicTo(
        size.width * 0.1728786,
        size.height * 0.5596157,
        size.width * 0.1718366,
        size.height * 0.5569706,
        size.width * 0.1708054,
        size.height * 0.5542539);
    path_8.cubicTo(
        size.width * 0.1663205,
        size.height * 0.5424480,
        size.width * 0.1620188,
        size.height * 0.5293088,
        size.width * 0.1578991,
        size.height * 0.5150490);
    path_8.cubicTo(
        size.width * 0.1530161,
        size.height * 0.5128569,
        size.width * 0.1458304,
        size.height * 0.5107637,
        size.width * 0.1375384,
        size.height * 0.5124578);
    path_8.cubicTo(
        size.width * 0.1072277,
        size.height * 0.5185373,
        size.width * 0.09138125,
        size.height * 0.5153480,
        size.width * 0.08299786,
        size.height * 0.5014951);
    path_8.cubicTo(
        size.width * 0.07737795,
        size.height * 0.4922265,
        size.width * 0.07728580,
        size.height * 0.4818618,
        size.width * 0.08262929,
        size.height * 0.4737892);
    path_8.cubicTo(
        size.width * 0.09101339,
        size.height * 0.4610333,
        size.width * 0.1075964,
        size.height * 0.4601363,
        size.width * 0.1203107,
        size.height * 0.4635245);
    path_8.cubicTo(
        size.width * 0.1302759,
        size.height * 0.4661539,
        size.width * 0.1430911,
        size.height * 0.4721745,
        size.width * 0.1543071,
        size.height * 0.4856520);
    path_8.cubicTo(
        size.width * 0.1558455,
        size.height * 0.4875010,
        size.width * 0.1573536,
        size.height * 0.4894892,
        size.width * 0.1588205,
        size.height * 0.4916284);
    path_8.cubicTo(
        size.width * 0.1587071,
        size.height * 0.4889461,
        size.width * 0.1587196,
        size.height * 0.4863608,
        size.width * 0.1588420,
        size.height * 0.4838716);
    path_8.cubicTo(
        size.width * 0.1597134,
        size.height * 0.4662010,
        size.width * 0.1661339,
        size.height * 0.4533735,
        size.width * 0.1722714,
        size.height * 0.4449873);
    path_8.cubicTo(
        size.width * 0.1819446,
        size.height * 0.4318324,
        size.width * 0.1946589,
        size.height * 0.4259520,
        size.width * 0.2054384,
        size.height * 0.4294402);
    path_8.cubicTo(
        size.width * 0.2139143,
        size.height * 0.4322314,
        size.width * 0.2195339,
        size.height * 0.4402039,
        size.width * 0.2203634,
        size.height * 0.4507676);
    path_8.cubicTo(
        size.width * 0.2215607,
        size.height * 0.4665147,
        size.width * 0.2116107,
        size.height * 0.4785735,
        size.width * 0.1858143,
        size.height * 0.4925255);
    path_8.cubicTo(
        size.width * 0.1812080,
        size.height * 0.4950176,
        size.width * 0.1773384,
        size.height * 0.4987049,
        size.width * 0.1742062,
        size.height * 0.5033892);
    path_8.cubicTo(
        size.width * 0.1779062,
        size.height * 0.5165598,
        size.width * 0.1817045,
        size.height * 0.5287216,
        size.width * 0.1856366,
        size.height * 0.5397186);
    path_8.cubicTo(
        size.width * 0.1870893,
        size.height * 0.5437824,
        size.width * 0.1885607,
        size.height * 0.5476873,
        size.width * 0.1900527,
        size.height * 0.5514255);
    path_8.cubicTo(
        size.width * 0.1898714,
        size.height * 0.5469618,
        size.width * 0.1899161,
        size.height * 0.5428118,
        size.width * 0.1901536,
        size.height * 0.5389333);
    path_8.cubicTo(
        size.width * 0.1914857,
        size.height * 0.5171814,
        size.width * 0.1988946,
        size.height * 0.5040137,
        size.width * 0.2066357,
        size.height * 0.4924265);
    path_8.cubicTo(
        size.width * 0.2215607,
        size.height * 0.4702020,
        size.width * 0.2420134,
        size.height * 0.4588402,
        size.width * 0.2587813,
        size.height * 0.4633255);
    path_8.cubicTo(
        size.width * 0.2696527,
        size.height * 0.4662157,
        size.width * 0.2769304,
        size.height * 0.4753843,
        size.width * 0.2786812,
        size.height * 0.4883402);
    path_8.cubicTo(
        size.width * 0.2816295,
        size.height * 0.5098667,
        size.width * 0.2680866,
        size.height * 0.5299980,
        size.width * 0.2321554,
        size.height * 0.5578039);
    path_8.cubicTo(
        size.width * 0.2198107,
        size.height * 0.5673716,
        size.width * 0.2116107,
        size.height * 0.5779353,
        size.width * 0.2066357,
        size.height * 0.5857088);
    path_8.cubicTo(
        size.width * 0.2195098,
        size.height * 0.6079735,
        size.width * 0.2347821,
        size.height * 0.6282363,
        size.width * 0.2482563,
        size.height * 0.6442578);
    path_8.cubicTo(
        size.width * 0.2504036,
        size.height * 0.6468108,
        size.width * 0.2525045,
        size.height * 0.6492559,
        size.width * 0.2545429,
        size.height * 0.6515843);
    path_8.cubicTo(
        size.width * 0.2536670,
        size.height * 0.6482304,
        size.width * 0.2529473,
        size.height * 0.6450127,
        size.width * 0.2523696,
        size.height * 0.6419206);
    path_8.cubicTo(
        size.width * 0.2474170,
        size.height * 0.6154304,
        size.width * 0.2528313,
        size.height * 0.5981941,
        size.width * 0.2586893,
        size.height * 0.5839147);
    path_8.cubicTo(
        size.width * 0.2694679,
        size.height * 0.5575049,
        size.width * 0.2881705,
        size.height * 0.5407618,
        size.width * 0.3064125,
        size.height * 0.5412598);
    path_8.cubicTo(
        size.width * 0.3182045,
        size.height * 0.5416588,
        size.width * 0.3275098,
        size.height * 0.5492333,
        size.width * 0.3320241,
        size.height * 0.5621892);
    path_8.cubicTo(
        size.width * 0.3394866,
        size.height * 0.5836157,
        size.width * 0.3297205,
        size.height * 0.6076343,
        size.width * 0.2983964,
        size.height * 0.6450069);
    path_8.cubicTo(
        size.width * 0.2878938,
        size.height * 0.6574647,
        size.width * 0.2817214,
        size.height * 0.6702216,
        size.width * 0.2783125,
        size.height * 0.6786922);
    path_8.cubicTo(
        size.width * 0.2780366,
        size.height * 0.6792902,
        size.width * 0.2776679,
        size.height * 0.6797882,
        size.width * 0.2772071,
        size.height * 0.6800873);
    path_8.lineTo(size.width * 0.2645848, size.height * 0.6880608);
    path_8.cubicTo(
        size.width * 0.2635714,
        size.height * 0.6887578,
        size.width * 0.2621902,
        size.height * 0.6885588,
        size.width * 0.2612687,
        size.height * 0.6876618);
    path_8.cubicTo(
        size.width * 0.2604857,
        size.height * 0.6868647,
        size.width * 0.2597250,
        size.height * 0.6860676,
        size.width * 0.2589652,
        size.height * 0.6852696);
    path_8.cubicTo(
        size.width * 0.2582054,
        size.height * 0.6844725,
        size.width * 0.2574455,
        size.height * 0.6836755,
        size.width * 0.2566625,
        size.height * 0.6828784);
    path_8.cubicTo(
        size.width * 0.2485545,
        size.height * 0.6849716,
        size.width * 0.2368545,
        size.height * 0.6893559,
        size.width * 0.2248777,
        size.height * 0.6981265);
    path_8.cubicTo(
        size.width * 0.2007393,
        size.height * 0.7162647,
        size.width * 0.1828661,
        size.height * 0.7248353,
        size.width * 0.1683098,
        size.height * 0.7248353);
    path_8.cubicTo(
        size.width * 0.1614000,
        size.height * 0.7248353,
        size.width * 0.1552277,
        size.height * 0.7229422,
        size.width * 0.1495152,
        size.height * 0.7191549);
    path_8.cubicTo(
        size.width * 0.1384598,
        size.height * 0.7118794,
        size.width * 0.1329321,
        size.height * 0.6997206,
        size.width * 0.1347750,
        size.height * 0.6867647);
    path_8.cubicTo(
        size.width * 0.1368018,
        size.height * 0.6720147,
        size.width * 0.1489625,
        size.height * 0.6535775,
        size.width * 0.1769696,
        size.height * 0.6459039);
    path_8.cubicTo(
        size.width * 0.1880670,
        size.height * 0.6428814,
        size.width * 0.2010312,
        size.height * 0.6403824,
        size.width * 0.2185018,
        size.height * 0.6459951);
    path_8.cubicTo(
        size.width * 0.2211929,
        size.height * 0.6468588,
        size.width * 0.2239902,
        size.height * 0.6479157,
        size.width * 0.2269045,
        size.height * 0.6491931);
    path_8.cubicTo(
        size.width * 0.2249179,
        size.height * 0.6467020,
        size.width * 0.2229687,
        size.height * 0.6442108,
        size.width * 0.2210562,
        size.height * 0.6417196);
    path_8.cubicTo(
        size.width * 0.2129482,
        size.height * 0.6311549,
        size.width * 0.2055143,
        size.height * 0.6205902,
        size.width * 0.1988045,
        size.height * 0.6100265);
    path_8.cubicTo(
        size.width * 0.1910661,
        size.height * 0.6090294,
        size.width * 0.1795500,
        size.height * 0.6089294,
        size.width * 0.1663750,
        size.height * 0.6128167);
    path_8.cubicTo(
        size.width * 0.1278652,
        size.height * 0.6242775,
        size.width * 0.1071357,
        size.height * 0.6221843,
        size.width * 0.09497500,
        size.height * 0.6057402);
    path_8.close();
    path_8.moveTo(size.width * 0.2284062, size.height * 0.6488461);
    path_8.lineTo(size.width * 0.2274241, size.height * 0.6503706);
    path_8.cubicTo(
        size.width * 0.2285848,
        size.height * 0.6508784,
        size.width * 0.2297643,
        size.height * 0.6514196,
        size.width * 0.2309625,
        size.height * 0.6519941);
    path_8.cubicTo(
        size.width * 0.2301036,
        size.height * 0.6509451,
        size.width * 0.2292518,
        size.height * 0.6498961,
        size.width * 0.2284062,
        size.height * 0.6488461);
    path_8.close();
    path_8.moveTo(size.width * 0.2559821, size.height * 0.6520363);
    path_8.lineTo(size.width * 0.2544455, size.height * 0.6530088);
    path_8.cubicTo(
        size.width * 0.2553366,
        size.height * 0.6540265,
        size.width * 0.2562161,
        size.height * 0.6550225,
        size.width * 0.2570821,
        size.height * 0.6559951);
    path_8.cubicTo(
        size.width * 0.2566920,
        size.height * 0.6546549,
        size.width * 0.2563250,
        size.height * 0.6533353,
        size.width * 0.2559821,
        size.height * 0.6520363);
    path_8.close();
    path_8.moveTo(size.width * 0.1913857, size.height * 0.5520069);
    path_8.lineTo(size.width * 0.1895321, size.height * 0.5524804);
    path_8.cubicTo(
        size.width * 0.1902652,
        size.height * 0.5543108,
        size.width * 0.1910018,
        size.height * 0.5561020,
        size.width * 0.1917411,
        size.height * 0.5578529);
    path_8.cubicTo(
        size.width * 0.1915813,
        size.height * 0.5558480,
        size.width * 0.1914634,
        size.height * 0.5539010,
        size.width * 0.1913857,
        size.height * 0.5520069);
    path_8.close();

    Paint paint_8_fill = Paint()..style = PaintingStyle.fill;
    paint_8_fill.color = Colors.black;
    canvas.drawPath(path_8, paint_8_fill);

    Path path_9 = Path()..fillType = PathFillType.evenOdd;
    path_9.moveTo(size.width * 0.7673152, size.height * 0.6903539);
    path_9.cubicTo(
        size.width * 0.7760679,
        size.height * 0.6911510,
        size.width * 0.7896107,
        size.height * 0.6936431,
        size.width * 0.8042589,
        size.height * 0.7014167);
    path_9.cubicTo(
        size.width * 0.8409268,
        size.height * 0.7208500,
        size.width * 0.8616563,
        size.height * 0.7237402,
        size.width * 0.8756598,
        size.height * 0.7112833);
    path_9.cubicTo(
        size.width * 0.8840438,
        size.height * 0.7038078,
        size.width * 0.8871759,
        size.height * 0.6936431,
        size.width * 0.8843196,
        size.height * 0.6832784);
    path_9.cubicTo(
        size.width * 0.8809107,
        size.height * 0.6708206,
        size.width * 0.8680125,
        size.height * 0.6564696,
        size.width * 0.8419402,
        size.height * 0.6533794);
    path_9.cubicTo(
        size.width * 0.8268312,
        size.height * 0.6515863,
        size.width * 0.8099714,
        size.height * 0.6514863,
        size.width * 0.7852804,
        size.height * 0.6712196);
    path_9.cubicTo(
        size.width * 0.7841750,
        size.height * 0.6721157,
        size.width * 0.7827009,
        size.height * 0.6720167,
        size.width * 0.7816875,
        size.height * 0.6709206);
    path_9.cubicTo(
        size.width * 0.7806741,
        size.height * 0.6699235,
        size.width * 0.7805821,
        size.height * 0.6683294,
        size.width * 0.7813187,
        size.height * 0.6670333);
    path_9.cubicTo(
        size.width * 0.7932955,
        size.height * 0.6482971,
        size.width * 0.8035223,
        size.height * 0.6294608,
        size.width * 0.8118134,
        size.height * 0.6112235);
    path_9.cubicTo(
        size.width * 0.8121821,
        size.height * 0.6104255,
        size.width * 0.8128268,
        size.height * 0.6099275,
        size.width * 0.8135643,
        size.height * 0.6096284);
    path_9.cubicTo(
        size.width * 0.8214875,
        size.height * 0.6072373,
        size.width * 0.8342009,
        size.height * 0.6048451,
        size.width * 0.8494027,
        size.height * 0.6070373);
    path_9.cubicTo(
        size.width * 0.8865312,
        size.height * 0.6122196,
        size.width * 0.9051429,
        size.height * 0.6079343,
        size.width * 0.9137054,
        size.height * 0.5920882);
    path_9.cubicTo(
        size.width * 0.9189643,
        size.height * 0.5824216,
        size.width * 0.9186875,
        size.height * 0.5721559,
        size.width * 0.9129732,
        size.height * 0.5637843);
    path_9.cubicTo(
        size.width * 0.9064286,
        size.height * 0.5542176,
        size.width * 0.8914134,
        size.height * 0.5460451,
        size.width * 0.8685652,
        size.height * 0.5518255);
    path_9.cubicTo(
        size.width * 0.8568652,
        size.height * 0.5548157,
        size.width * 0.8443357,
        size.height * 0.5592000,
        size.width * 0.8306080,
        size.height * 0.5777373);
    path_9.cubicTo(
        size.width * 0.8297884,
        size.height * 0.5788206,
        size.width * 0.8283384,
        size.height * 0.5792225,
        size.width * 0.8271482,
        size.height * 0.5785578);
    path_9.cubicTo(
        size.width * 0.8271339,
        size.height * 0.5785500,
        size.width * 0.8271205,
        size.height * 0.5785422,
        size.width * 0.8271071,
        size.height * 0.5785343);
    path_9.cubicTo(
        size.width * 0.8269089,
        size.height * 0.5784186,
        size.width * 0.8267277,
        size.height * 0.5782814,
        size.width * 0.8265670,
        size.height * 0.5781265);
    path_9.cubicTo(
        size.width * 0.8257545,
        size.height * 0.5773441,
        size.width * 0.8254330,
        size.height * 0.5761108,
        size.width * 0.8258170,
        size.height * 0.5749471);
    path_9.cubicTo(
        size.width * 0.8315295,
        size.height * 0.5565098,
        size.width * 0.8362277,
        size.height * 0.5339863,
        size.width * 0.8399134,
        size.height * 0.5079745);
    path_9.cubicTo(
        size.width * 0.8400054,
        size.height * 0.5070775,
        size.width * 0.8405580,
        size.height * 0.5061804,
        size.width * 0.8413875,
        size.height * 0.5057824);
    path_9.cubicTo(
        size.width * 0.8481125,
        size.height * 0.5019951,
        size.width * 0.8551143,
        size.height * 0.5002010,
        size.width * 0.8623009,
        size.height * 0.5005000);
    path_9.cubicTo(
        size.width * 0.8904920,
        size.height * 0.5018951,
        size.width * 0.9042232,
        size.height * 0.4973108,
        size.width * 0.9096518,
        size.height * 0.4848529);
    path_9.cubicTo(
        size.width * 0.9129732,
        size.height * 0.4771794,
        size.width * 0.9121429,
        size.height * 0.4693059,
        size.width * 0.9074464,
        size.height * 0.4639245);
    path_9.cubicTo(
        size.width * 0.9021964,
        size.height * 0.4579451,
        size.width * 0.8908607,
        size.height * 0.4539578,
        size.width * 0.8748304,
        size.height * 0.4609343);
    path_9.cubicTo(
        size.width * 0.8611955,
        size.height * 0.4668147,
        size.width * 0.8501393,
        size.height * 0.4776775,
        size.width * 0.8428616,
        size.height * 0.4923275);
    path_9.cubicTo(
        size.width * 0.8423750,
        size.height * 0.4933804,
        size.width * 0.8413634,
        size.height * 0.4939225,
        size.width * 0.8403420,
        size.height * 0.4939118);
    path_9.cubicTo(
        size.width * 0.8400098,
        size.height * 0.4939078,
        size.width * 0.8396768,
        size.height * 0.4938451,
        size.width * 0.8393607,
        size.height * 0.4937235);
    path_9.cubicTo(
        size.width * 0.8392687,
        size.height * 0.4936735,
        size.width * 0.8391536,
        size.height * 0.4936235,
        size.width * 0.8390384,
        size.height * 0.4935735);
    path_9.cubicTo(
        size.width * 0.8389991,
        size.height * 0.4935569,
        size.width * 0.8389598,
        size.height * 0.4935402,
        size.width * 0.8389223,
        size.height * 0.4935225);
    path_9.cubicTo(
        size.width * 0.8388473,
        size.height * 0.4934902,
        size.width * 0.8387768,
        size.height * 0.4934569,
        size.width * 0.8387152,
        size.height * 0.4934245);
    path_9.cubicTo(
        size.width * 0.8377018,
        size.height * 0.4929255,
        size.width * 0.8370571,
        size.height * 0.4919294,
        size.width * 0.8370571,
        size.height * 0.4908333);
    path_9.cubicTo(
        size.width * 0.8363205,
        size.height * 0.4717980,
        size.width * 0.8294107,
        size.height * 0.4547559,
        size.width * 0.8172491,
        size.height * 0.4415010);
    path_9.cubicTo(
        size.width * 0.8035223,
        size.height * 0.4265520,
        size.width * 0.7903473,
        size.height * 0.4251559,
        size.width * 0.7832536,
        size.height * 0.4285451);
    path_9.cubicTo(
        size.width * 0.7771732,
        size.height * 0.4314353,
        size.width * 0.7739482,
        size.height * 0.4378137,
        size.width * 0.7743170,
        size.height * 0.4460853);
    path_9.cubicTo(
        size.width * 0.7749616,
        size.height * 0.4603363,
        size.width * 0.7866625,
        size.height * 0.4712000,
        size.width * 0.8147616,
        size.height * 0.4838569);
    path_9.cubicTo(
        size.width * 0.8215795,
        size.height * 0.4869461,
        size.width * 0.8275679,
        size.height * 0.4917294,
        size.width * 0.8325429,
        size.height * 0.4982078);
    path_9.cubicTo(
        size.width * 0.8330036,
        size.height * 0.4989059,
        size.width * 0.8332795,
        size.height * 0.4997029,
        size.width * 0.8331875,
        size.height * 0.5005000);
    path_9.cubicTo(
        size.width * 0.8293187,
        size.height * 0.5299000,
        size.width * 0.8240670,
        size.height * 0.5549147,
        size.width * 0.8178018,
        size.height * 0.5748471);
    path_9.cubicTo(
        size.width * 0.8173420,
        size.height * 0.5762422,
        size.width * 0.8160518,
        size.height * 0.5770392,
        size.width * 0.8146696,
        size.height * 0.5768402);
    path_9.cubicTo(
        size.width * 0.8132875,
        size.height * 0.5765412,
        size.width * 0.8123661,
        size.height * 0.5753451,
        size.width * 0.8123661,
        size.height * 0.5738500);
    path_9.cubicTo(
        size.width * 0.8129196,
        size.height * 0.5341853,
        size.width * 0.8013107,
        size.height * 0.5178412,
        size.width * 0.7892420,
        size.height * 0.5043863);
    path_9.cubicTo(
        size.width * 0.7698027,
        size.height * 0.4825608,
        size.width * 0.7504554,
        size.height * 0.4801696,
        size.width * 0.7398607,
        size.height * 0.4848529);
    path_9.cubicTo(
        size.width * 0.7316607,
        size.height * 0.4884412,
        size.width * 0.7268705,
        size.height * 0.4965137,
        size.width * 0.7268705,
        size.height * 0.5071775);
    path_9.cubicTo(
        size.width * 0.7267777,
        size.height * 0.5263118,
        size.width * 0.7418875,
        size.height * 0.5427559,
        size.width * 0.7789232,
        size.height * 0.5636853);
    path_9.cubicTo(
        size.width * 0.7940330,
        size.height * 0.5722559,
        size.width * 0.8044438,
        size.height * 0.5828196,
        size.width * 0.8106161,
        size.height * 0.5901951);
    path_9.cubicTo(
        size.width * 0.8113527,
        size.height * 0.5910922,
        size.width * 0.8115375,
        size.height * 0.5922873,
        size.width * 0.8110768,
        size.height * 0.5933843);
    path_9.cubicTo(
        size.width * 0.7971652,
        size.height * 0.6279667,
        size.width * 0.7776339,
        size.height * 0.6591598,
        size.width * 0.7637223,
        size.height * 0.6792912);
    path_9.cubicTo(
        size.width * 0.7629848,
        size.height * 0.6803882,
        size.width * 0.7615107,
        size.height * 0.6807863,
        size.width * 0.7604054,
        size.height * 0.6801882);
    path_9.cubicTo(
        size.width * 0.7592080,
        size.height * 0.6795902,
        size.width * 0.7585625,
        size.height * 0.6781951,
        size.width * 0.7588393,
        size.height * 0.6768000);
    path_9.cubicTo(
        size.width * 0.7667625,
        size.height * 0.6376333,
        size.width * 0.7581946,
        size.height * 0.6182990,
        size.width * 0.7488893,
        size.height * 0.6019549);
    path_9.cubicTo(
        size.width * 0.7332268,
        size.height * 0.5746480,
        size.width * 0.7137875,
        size.height * 0.5674725,
        size.width * 0.7018107,
        size.height * 0.5696647);
    path_9.cubicTo(
        size.width * 0.6925062,
        size.height * 0.5713588,
        size.width * 0.6858723,
        size.height * 0.5786343,
        size.width * 0.6836616,
        size.height * 0.5896961);
    path_9.cubicTo(
        size.width * 0.6796080,
        size.height * 0.6095294,
        size.width * 0.6917687,
        size.height * 0.6301588,
        size.width * 0.7257643,
        size.height * 0.6607549);
    path_9.cubicTo(
        size.width * 0.7379259,
        size.height * 0.6717176,
        size.width * 0.7459411,
        size.height * 0.6834775,
        size.width * 0.7507321,
        size.height * 0.6917490);
    path_9.lineTo(size.width * 0.7615107, size.height * 0.6965333);
    path_9.cubicTo(
        size.width * 0.7627089,
        size.height * 0.6949382,
        size.width * 0.7639063,
        size.height * 0.6933441,
        size.width * 0.7650116,
        size.height * 0.6916500);
    path_9.cubicTo(
        size.width * 0.7654723,
        size.height * 0.6907529,
        size.width * 0.7663018,
        size.height * 0.6903539,
        size.width * 0.7671313,
        size.height * 0.6903539);
    path_9.lineTo(size.width * 0.7673152, size.height * 0.6903539);
    path_9.close();
    path_9.moveTo(size.width * 0.7652009, size.height * 0.6659931);
    path_9.cubicTo(
        size.width * 0.7650384,
        size.height * 0.6672147,
        size.width * 0.7648589,
        size.height * 0.6684559,
        size.width * 0.7646616,
        size.height * 0.6697176);
    path_9.cubicTo(
        size.width * 0.7653321,
        size.height * 0.6687157,
        size.width * 0.7660107,
        size.height * 0.6676941,
        size.width * 0.7666973,
        size.height * 0.6666520);
    path_9.lineTo(size.width * 0.7652009, size.height * 0.6659931);
    path_9.close();
    path_9.moveTo(size.width * 0.8162759, size.height * 0.5570186);
    path_9.cubicTo(
        size.width * 0.8164348,
        size.height * 0.5588706,
        size.width * 0.8165616,
        size.height * 0.5607794,
        size.width * 0.8166554,
        size.height * 0.5627480);
    path_9.cubicTo(
        size.width * 0.8171509,
        size.height * 0.5609324,
        size.width * 0.8176384,
        size.height * 0.5590814,
        size.width * 0.8181188,
        size.height * 0.5571931);
    path_9.lineTo(size.width * 0.8162759, size.height * 0.5570186);
    path_9.close();
    path_9.moveTo(size.width * 0.9115893, size.height * 0.4598382);
    path_9.cubicTo(
        size.width * 0.9016429,
        size.height * 0.4484775,
        size.width * 0.8850571,
        size.height * 0.4500716,
        size.width * 0.8728955,
        size.height * 0.4553539);
    path_9.cubicTo(
        size.width * 0.8621830,
        size.height * 0.4600059,
        size.width * 0.8529964,
        size.height * 0.4674588,
        size.width * 0.8457304,
        size.height * 0.4771627);
    path_9.cubicTo(
        size.width * 0.8444366,
        size.height * 0.4788912,
        size.width * 0.8432027,
        size.height * 0.4806922,
        size.width * 0.8420321,
        size.height * 0.4825608);
    path_9.cubicTo(
        size.width * 0.8417455,
        size.height * 0.4803137,
        size.width * 0.8413768,
        size.height * 0.4780971,
        size.width * 0.8409277,
        size.height * 0.4759127);
    path_9.cubicTo(
        size.width * 0.8379098,
        size.height * 0.4612422,
        size.width * 0.8312464,
        size.height * 0.4480588,
        size.width * 0.8213027,
        size.height * 0.4372157);
    path_9.cubicTo(
        size.width * 0.8056411,
        size.height * 0.4201735,
        size.width * 0.7902554,
        size.height * 0.4186784,
        size.width * 0.7811348,
        size.height * 0.4230637);
    path_9.cubicTo(
        size.width * 0.7730268,
        size.height * 0.4269500,
        size.width * 0.7684205,
        size.height * 0.4356206,
        size.width * 0.7688812,
        size.height * 0.4463843);
    path_9.cubicTo(
        size.width * 0.7696188,
        size.height * 0.4633265,
        size.width * 0.7823321,
        size.height * 0.4757843,
        size.width * 0.8127348,
        size.height * 0.4894373);
    path_9.cubicTo(
        size.width * 0.8183554,
        size.height * 0.4920284,
        size.width * 0.8233304,
        size.height * 0.4959157,
        size.width * 0.8275679,
        size.height * 0.5010980);
    path_9.cubicTo(
        size.width * 0.8254813,
        size.height * 0.5167461,
        size.width * 0.8230277,
        size.height * 0.5310882,
        size.width * 0.8202063,
        size.height * 0.5440382);
    path_9.cubicTo(
        size.width * 0.8192902,
        size.height * 0.5482431,
        size.width * 0.8183348,
        size.height * 0.5523020,
        size.width * 0.8173420,
        size.height * 0.5562108);
    path_9.cubicTo(
        size.width * 0.8169411,
        size.height * 0.5518039,
        size.width * 0.8163625,
        size.height * 0.5477333,
        size.width * 0.8156321,
        size.height * 0.5439559);
    path_9.cubicTo(
        size.width * 0.8114893,
        size.height * 0.5225324,
        size.width * 0.8024589,
        size.height * 0.5105520,
        size.width * 0.7932955,
        size.height * 0.5003010);
    path_9.cubicTo(
        size.width * 0.7756991,
        size.height * 0.4806676,
        size.width * 0.7538643,
        size.height * 0.4723961,
        size.width * 0.7378339,
        size.height * 0.4793716);
    path_9.cubicTo(
        size.width * 0.7274232,
        size.height * 0.4838569,
        size.width * 0.7214348,
        size.height * 0.4940216,
        size.width * 0.7213429,
        size.height * 0.5071775);
    path_9.cubicTo(
        size.width * 0.7212500,
        size.height * 0.5289029,
        size.width * 0.7371884,
        size.height * 0.5468422,
        size.width * 0.7763437,
        size.height * 0.5689667);
    path_9.cubicTo(
        size.width * 0.7897946,
        size.height * 0.5765412,
        size.width * 0.7993759,
        size.height * 0.5859098,
        size.width * 0.8052723,
        size.height * 0.5927863);
    path_9.cubicTo(
        size.width * 0.7954143,
        size.height * 0.6167804,
        size.width * 0.7829482,
        size.height * 0.6391422,
        size.width * 0.7716652,
        size.height * 0.6570539);
    path_9.cubicTo(
        size.width * 0.7698732,
        size.height * 0.6598980,
        size.width * 0.7681107,
        size.height * 0.6626304,
        size.width * 0.7663937,
        size.height * 0.6652392);
    path_9.cubicTo(
        size.width * 0.7668250,
        size.height * 0.6617980,
        size.width * 0.7671196,
        size.height * 0.6585147,
        size.width * 0.7672920,
        size.height * 0.6553755);
    path_9.cubicTo(
        size.width * 0.7687768,
        size.height * 0.6283363,
        size.width * 0.7611821,
        size.height * 0.6120706,
        size.width * 0.7535875,
        size.height * 0.5987657);
    path_9.cubicTo(
        size.width * 0.7395839,
        size.height * 0.5742490,
        size.width * 0.7188554,
        size.height * 0.5604961,
        size.width * 0.7008893,
        size.height * 0.5637843);
    path_9.cubicTo(
        size.width * 0.6892812,
        size.height * 0.5659775,
        size.width * 0.6809893,
        size.height * 0.5749471,
        size.width * 0.6782259,
        size.height * 0.5884010);
    path_9.cubicTo(
        size.width * 0.6736196,
        size.height * 0.6107255,
        size.width * 0.6863330,
        size.height * 0.6330490,
        size.width * 0.7221714,
        size.height * 0.6653392);
    path_9.cubicTo(
        size.width * 0.7341482,
        size.height * 0.6761029,
        size.width * 0.7419795,
        size.height * 0.6877627,
        size.width * 0.7464018,
        size.height * 0.6956363);
    path_9.cubicTo(
        size.width * 0.7466786,
        size.height * 0.6961343,
        size.width * 0.7471384,
        size.height * 0.6966324,
        size.width * 0.7476920,
        size.height * 0.6968324);
    path_9.lineTo(size.width * 0.7612348, size.height * 0.7028118);
    path_9.cubicTo(
        size.width * 0.7624321,
        size.height * 0.7033098,
        size.width * 0.7637223,
        size.height * 0.7029118,
        size.width * 0.7644589,
        size.height * 0.7019147);
    path_9.cubicTo(
        size.width * 0.7657491,
        size.height * 0.7002206,
        size.width * 0.7671313,
        size.height * 0.6983265,
        size.width * 0.7684205,
        size.height * 0.6965333);
    path_9.cubicTo(
        size.width * 0.7767125,
        size.height * 0.6973304,
        size.width * 0.7887813,
        size.height * 0.6998216,
        size.width * 0.8018634,
        size.height * 0.7067980);
    path_9.cubicTo(
        size.width * 0.8246196,
        size.height * 0.7189569,
        size.width * 0.8417563,
        size.height * 0.7248363,
        size.width * 0.8552071,
        size.height * 0.7248363);
    path_9.cubicTo(
        size.width * 0.8648804,
        size.height * 0.7248363,
        size.width * 0.8726196,
        size.height * 0.7217471,
        size.width * 0.8793446,
        size.height * 0.7156676);
    path_9.cubicTo(
        size.width * 0.8892946,
        size.height * 0.7067980,
        size.width * 0.8932589,
        size.height * 0.6939422,
        size.width * 0.8897554,
        size.height * 0.6813843);
    path_9.cubicTo(
        size.width * 0.8857938,
        size.height * 0.6671333,
        size.width * 0.8714214,
        size.height * 0.6506892,
        size.width * 0.8426768,
        size.height * 0.6473010);
    path_9.cubicTo(
        size.width * 0.8313393,
        size.height * 0.6459186,
        size.width * 0.8182018,
        size.height * 0.6454353,
        size.width * 0.8015857,
        size.height * 0.6536353);
    path_9.cubicTo(
        size.width * 0.7990295,
        size.height * 0.6548961,
        size.width * 0.7963911,
        size.height * 0.6563627,
        size.width * 0.7936643,
        size.height * 0.6580637);
    path_9.cubicTo(
        size.width * 0.7953205,
        size.height * 0.6553010,
        size.width * 0.7969393,
        size.height * 0.6525412,
        size.width * 0.7985196,
        size.height * 0.6497863);
    path_9.cubicTo(
        size.width * 0.8052054,
        size.height * 0.6381324,
        size.width * 0.8112045,
        size.height * 0.6265559,
        size.width * 0.8164205,
        size.height * 0.6151098);
    path_9.cubicTo(
        size.width * 0.8238830,
        size.height * 0.6130176,
        size.width * 0.8353071,
        size.height * 0.6111235,
        size.width * 0.8488500,
        size.height * 0.6130176);
    path_9.cubicTo(
        size.width * 0.8884652,
        size.height * 0.6184980,
        size.width * 0.9086429,
        size.height * 0.6133167,
        size.width * 0.9185893,
        size.height * 0.5951775);
    path_9.cubicTo(
        size.width * 0.9249464,
        size.height * 0.5836176,
        size.width * 0.9245804,
        size.height * 0.5705618,
        size.width * 0.9175804,
        size.height * 0.5602961);
    path_9.cubicTo(
        size.width * 0.9104643,
        size.height * 0.5498814,
        size.width * 0.8948571,
        size.height * 0.5407029,
        size.width * 0.8718393,
        size.height * 0.5450706);
    path_9.cubicTo(
        size.width * 0.8704080,
        size.height * 0.5453422,
        size.width * 0.8689482,
        size.height * 0.5456657,
        size.width * 0.8674598,
        size.height * 0.5460451);
    path_9.cubicTo(
        size.width * 0.8660420,
        size.height * 0.5463922,
        size.width * 0.8645946,
        size.height * 0.5467696,
        size.width * 0.8631205,
        size.height * 0.5471961);
    path_9.cubicTo(
        size.width * 0.8561911,
        size.height * 0.5492020,
        size.width * 0.8486741,
        size.height * 0.5523098,
        size.width * 0.8408321,
        size.height * 0.5586451);
    path_9.cubicTo(
        size.width * 0.8388562,
        size.height * 0.5602412,
        size.width * 0.8368589,
        size.height * 0.5620431,
        size.width * 0.8348464,
        size.height * 0.5640833);
    path_9.cubicTo(
        size.width * 0.8355473,
        size.height * 0.5613784,
        size.width * 0.8362339,
        size.height * 0.5586020,
        size.width * 0.8369054,
        size.height * 0.5557559);
    path_9.cubicTo(
        size.width * 0.8401205,
        size.height * 0.5421402,
        size.width * 0.8429866,
        size.height * 0.5269353,
        size.width * 0.8453491,
        size.height * 0.5103667);
    path_9.cubicTo(
        size.width * 0.8507848,
        size.height * 0.5074765,
        size.width * 0.8564964,
        size.height * 0.5061804,
        size.width * 0.8622089,
        size.height * 0.5064794);
    path_9.cubicTo(
        size.width * 0.8930714,
        size.height * 0.5079745,
        size.width * 0.9082768,
        size.height * 0.5023931,
        size.width * 0.9148125,
        size.height * 0.4873451);
    path_9.cubicTo(
        size.width * 0.9191429,
        size.height * 0.4773784,
        size.width * 0.9179464,
        size.height * 0.4671137,
        size.width * 0.9115893,
        size.height * 0.4598382);
    path_9.close();
    path_9.moveTo(size.width * 0.7922562, size.height * 0.6580912);
    path_9.cubicTo(
        size.width * 0.7915223,
        size.height * 0.6593137,
        size.width * 0.7907812,
        size.height * 0.6605373,
        size.width * 0.7900330,
        size.height * 0.6617608);
    path_9.cubicTo(
        size.width * 0.7911884,
        size.height * 0.6609647,
        size.width * 0.7923286,
        size.height * 0.6602108,
        size.width * 0.7934545,
        size.height * 0.6594961);
    path_9.lineTo(size.width * 0.7922562, size.height * 0.6580912);
    path_9.close();
    path_9.fillType = PathFillType.evenOdd;
    Paint paint_9_fill = Paint()..style = PaintingStyle.fill;
    paint_9_fill.color = Colors.black;
    canvas.drawPath(path_9, paint_9_fill);

    Path path_10 = Path()..fillType = PathFillType.evenOdd;
    path_10.moveTo(size.width * 0.4563813, size.height * 0.2375716);
    path_10.cubicTo(
        size.width * 0.4564045,
        size.height * 0.2375873,
        size.width * 0.4564277,
        size.height * 0.2376020,
        size.width * 0.4564509,
        size.height * 0.2376157);
    path_10.cubicTo(
        size.width * 0.4564875,
        size.height * 0.2376363,
        size.width * 0.4565241,
        size.height * 0.2376559,
        size.width * 0.4565607,
        size.height * 0.2376735);
    path_10.cubicTo(
        size.width * 0.4572750,
        size.height * 0.2380049,
        size.width * 0.4581688,
        size.height * 0.2376088,
        size.width * 0.4585929,
        size.height * 0.2368745);
    path_10.cubicTo(
        size.width * 0.4587768,
        size.height * 0.2364755,
        size.width * 0.4588688,
        size.height * 0.2359775,
        size.width * 0.4586848,
        size.height * 0.2355784);
    path_10.cubicTo(
        size.width * 0.4586000,
        size.height * 0.2352098,
        size.width * 0.4583571,
        size.height * 0.2348422,
        size.width * 0.4580304,
        size.height * 0.2346314);
    path_10.cubicTo(
        size.width * 0.4580036,
        size.height * 0.2346137,
        size.width * 0.4579759,
        size.height * 0.2345971,
        size.width * 0.4579482,
        size.height * 0.2345814);
    path_10.lineTo(size.width * 0.4266241, size.height * 0.2159451);
    path_10.cubicTo(
        size.width * 0.4266009,
        size.height * 0.2159284,
        size.width * 0.4265786,
        size.height * 0.2159137,
        size.width * 0.4265554,
        size.height * 0.2159000);
    path_10.cubicTo(
        size.width * 0.4265188,
        size.height * 0.2158775,
        size.width * 0.4264821,
        size.height * 0.2158588,
        size.width * 0.4264455,
        size.height * 0.2158422);
    path_10.cubicTo(
        size.width * 0.4262286,
        size.height * 0.2157461,
        size.width * 0.4260116,
        size.height * 0.2157461,
        size.width * 0.4257946,
        size.height * 0.2157461);
    path_10.cubicTo(
        size.width * 0.4252420,
        size.height * 0.2157461,
        size.width * 0.4246893,
        size.height * 0.2160451,
        size.width * 0.4244125,
        size.height * 0.2166431);
    path_10.cubicTo(
        size.width * 0.4242286,
        size.height * 0.2170412,
        size.width * 0.4241366,
        size.height * 0.2175402,
        size.width * 0.4243205,
        size.height * 0.2179382);
    path_10.cubicTo(
        size.width * 0.4243991,
        size.height * 0.2182765,
        size.width * 0.4246098,
        size.height * 0.2186157,
        size.width * 0.4248973,
        size.height * 0.2188314);
    path_10.cubicTo(
        size.width * 0.4249482,
        size.height * 0.2188706,
        size.width * 0.4250018,
        size.height * 0.2189049,
        size.width * 0.4250580,
        size.height * 0.2189353);
    path_10.lineTo(size.width * 0.4563813, size.height * 0.2375716);
    path_10.close();
    path_10.moveTo(size.width * 0.4228464, size.height * 0.2233206);
    path_10.cubicTo(
        size.width * 0.4213723,
        size.height * 0.2224235,
        size.width * 0.4203589,
        size.height * 0.2210275,
        size.width * 0.4198982,
        size.height * 0.2193333);
    path_10.cubicTo(
        size.width * 0.4194375,
        size.height * 0.2176392,
        size.width * 0.4196223,
        size.height * 0.2157461,
        size.width * 0.4204509,
        size.height * 0.2142510);
    path_10.cubicTo(
        size.width * 0.4221098,
        size.height * 0.2109618,
        size.width * 0.4258866,
        size.height * 0.2097667,
        size.width * 0.4289268,
        size.height * 0.2115598);
    path_10.lineTo(size.width * 0.4602509, size.height * 0.2301971);
    path_10.cubicTo(
        size.width * 0.4617250,
        size.height * 0.2310941,
        size.width * 0.4627384,
        size.height * 0.2324892,
        size.width * 0.4631991,
        size.height * 0.2341833);
    path_10.cubicTo(
        size.width * 0.4636598,
        size.height * 0.2358775,
        size.width * 0.4634759,
        size.height * 0.2377706,
        size.width * 0.4626464,
        size.height * 0.2392657);
    path_10.cubicTo(
        size.width * 0.4614491,
        size.height * 0.2414588,
        size.width * 0.4594223,
        size.height * 0.2427539,
        size.width * 0.4571187,
        size.height * 0.2427539);
    path_10.cubicTo(
        size.width * 0.4561054,
        size.height * 0.2427539,
        size.width * 0.4550000,
        size.height * 0.2424549,
        size.width * 0.4541705,
        size.height * 0.2419569);
    path_10.lineTo(size.width * 0.4228464, size.height * 0.2233206);
    path_10.close();

    Paint paint_10_fill = Paint()..style = PaintingStyle.fill;
    paint_10_fill.color = Colors.black;
    canvas.drawPath(path_10, paint_10_fill);

    Path path_11 = Path()..fillType = PathFillType.evenOdd;
    path_11.moveTo(size.width * 0.4483741, size.height * 0.1756980);
    path_11.cubicTo(
        size.width * 0.4480982,
        size.height * 0.1773922,
        size.width * 0.4485580,
        size.height * 0.1791863,
        size.width * 0.4494795,
        size.height * 0.1806814);
    path_11.lineTo(size.width * 0.4727884, size.height * 0.2159608);
    path_11.cubicTo(
        size.width * 0.4739866,
        size.height * 0.2176549,
        size.width * 0.4758286,
        size.height * 0.2187520,
        size.width * 0.4778554,
        size.height * 0.2187520);
    path_11.cubicTo(
        size.width * 0.4791455,
        size.height * 0.2187520,
        size.width * 0.4803429,
        size.height * 0.2182529,
        size.width * 0.4814491,
        size.height * 0.2175559);
    path_11.cubicTo(
        size.width * 0.4827384,
        size.height * 0.2164588,
        size.width * 0.4836598,
        size.height * 0.2149647,
        size.width * 0.4839366,
        size.height * 0.2131706);
    path_11.cubicTo(
        size.width * 0.4842125,
        size.height * 0.2114765,
        size.width * 0.4837518,
        size.height * 0.2096824,
        size.width * 0.4828304,
        size.height * 0.2081873);
    path_11.lineTo(size.width * 0.4595214, size.height * 0.1729078);
    path_11.cubicTo(
        size.width * 0.4592009,
        size.height * 0.1724176,
        size.width * 0.4588330,
        size.height * 0.1719882,
        size.width * 0.4584312,
        size.height * 0.1716206);
    path_11.cubicTo(
        size.width * 0.4572937,
        size.height * 0.1705814,
        size.width * 0.4558795,
        size.height * 0.1700382,
        size.width * 0.4544545,
        size.height * 0.1700373);
    path_11.cubicTo(
        size.width * 0.4532964,
        size.height * 0.1700353,
        size.width * 0.4521304,
        size.height * 0.1703931,
        size.width * 0.4511009,
        size.height * 0.1711324);
    path_11.cubicTo(
        size.width * 0.4510330,
        size.height * 0.1711814,
        size.width * 0.4509652,
        size.height * 0.1712314,
        size.width * 0.4508982,
        size.height * 0.1712843);
    path_11.cubicTo(
        size.width * 0.4508866,
        size.height * 0.1712931,
        size.width * 0.4508741,
        size.height * 0.1713029,
        size.width * 0.4508616,
        size.height * 0.1713127);
    path_11.cubicTo(
        size.width * 0.4502911,
        size.height * 0.1717980,
        size.width * 0.4497929,
        size.height * 0.1723608,
        size.width * 0.4493902,
        size.height * 0.1729922);
    path_11.cubicTo(
        size.width * 0.4488830,
        size.height * 0.1737902,
        size.width * 0.4485286,
        size.height * 0.1746971,
        size.width * 0.4483741,
        size.height * 0.1756980);
    path_11.close();
    path_11.moveTo(size.width * 0.4555562, size.height * 0.1753716);
    path_11.cubicTo(
        size.width * 0.4552795,
        size.height * 0.1750912,
        size.width * 0.4548964,
        size.height * 0.1749010,
        size.width * 0.4544545,
        size.height * 0.1749010);
    path_11.cubicTo(
        size.width * 0.4541063,
        size.height * 0.1749010,
        size.width * 0.4538402,
        size.height * 0.1749892,
        size.width * 0.4535795,
        size.height * 0.1751676);
    path_11.cubicTo(
        size.width * 0.4535643,
        size.height * 0.1751784,
        size.width * 0.4535482,
        size.height * 0.1751882,
        size.width * 0.4535330,
        size.height * 0.1752000);
    path_11.cubicTo(
        size.width * 0.4535080,
        size.height * 0.1752206,
        size.width * 0.4534839,
        size.height * 0.1752412,
        size.width * 0.4534607,
        size.height * 0.1752627);
    path_11.cubicTo(
        size.width * 0.4533071,
        size.height * 0.1754020,
        size.width * 0.4531893,
        size.height * 0.1755598,
        size.width * 0.4530982,
        size.height * 0.1757265);
    path_11.cubicTo(
        size.width * 0.4530000,
        size.height * 0.1759088,
        size.width * 0.4529330,
        size.height * 0.1761020,
        size.width * 0.4528884,
        size.height * 0.1762961);
    path_11.cubicTo(
        size.width * 0.4527964,
        size.height * 0.1767941,
        size.width * 0.4528884,
        size.height * 0.1771931,
        size.width * 0.4531652,
        size.height * 0.1775912);
    path_11.lineTo(size.width * 0.4764741, size.height * 0.2128716);
    path_11.cubicTo(
        size.width * 0.4764813,
        size.height * 0.2128863,
        size.width * 0.4764902,
        size.height * 0.2129000,
        size.width * 0.4764982,
        size.height * 0.2129137);
    path_11.cubicTo(
        size.width * 0.4765116,
        size.height * 0.2129363,
        size.width * 0.4765259,
        size.height * 0.2129578,
        size.width * 0.4765402,
        size.height * 0.2129784);
    path_11.cubicTo(
        size.width * 0.4768330,
        size.height * 0.2133990,
        size.width * 0.4773375,
        size.height * 0.2135686,
        size.width * 0.4777634,
        size.height * 0.2135686);
    path_11.cubicTo(
        size.width * 0.4780402,
        size.height * 0.2135686,
        size.width * 0.4783161,
        size.height * 0.2134696,
        size.width * 0.4786848,
        size.height * 0.2132706);
    path_11.cubicTo(
        size.width * 0.4790536,
        size.height * 0.2129716,
        size.width * 0.4792375,
        size.height * 0.2125725,
        size.width * 0.4793295,
        size.height * 0.2121735);
    path_11.cubicTo(
        size.width * 0.4794071,
        size.height * 0.2117539,
        size.width * 0.4793545,
        size.height * 0.2114059,
        size.width * 0.4791705,
        size.height * 0.2110676);
    path_11.cubicTo(
        size.width * 0.4791366,
        size.height * 0.2110049,
        size.width * 0.4790973,
        size.height * 0.2109412,
        size.width * 0.4790536,
        size.height * 0.2108784);
    path_11.lineTo(size.width * 0.4557446, size.height * 0.1755980);
    path_11.cubicTo(
        size.width * 0.4557357,
        size.height * 0.1755853,
        size.width * 0.4557259,
        size.height * 0.1755716,
        size.width * 0.4557161,
        size.height * 0.1755588);
    path_11.cubicTo(
        size.width * 0.4557009,
        size.height * 0.1755382,
        size.width * 0.4556857,
        size.height * 0.1755176,
        size.width * 0.4556696,
        size.height * 0.1754980);
    path_11.cubicTo(
        size.width * 0.4556339,
        size.height * 0.1754539,
        size.width * 0.4555964,
        size.height * 0.1754118,
        size.width * 0.4555562,
        size.height * 0.1753716);
    path_11.close();
    path_11.fillType = PathFillType.evenOdd;
    Paint paint_11_fill = Paint()..style = PaintingStyle.fill;
    paint_11_fill.color = Colors.black;
    canvas.drawPath(path_11, paint_11_fill);

    Path path_12 = Path()..fillType = PathFillType.evenOdd;
    path_12.moveTo(size.width * 0.5002366, size.height * 0.1618265);
    path_12.cubicTo(
        size.width * 0.5002366,
        size.height * 0.1618206,
        size.width * 0.5002366,
        size.height * 0.1618137,
        size.width * 0.5002366,
        size.height * 0.1618078);
    path_12.cubicTo(
        size.width * 0.5002384,
        size.height * 0.1616843,
        size.width * 0.5002536,
        size.height * 0.1615618,
        size.width * 0.5002813,
        size.height * 0.1614441);
    path_12.cubicTo(
        size.width * 0.5004545,
        size.height * 0.1607147,
        size.width * 0.5011071,
        size.height * 0.1601324,
        size.width * 0.5018946,
        size.height * 0.1601324);
    path_12.cubicTo(
        size.width * 0.5027241,
        size.height * 0.1601324,
        size.width * 0.5034607,
        size.height * 0.1609294,
        size.width * 0.5033688,
        size.height * 0.1618265);
    path_12.lineTo(size.width * 0.5031848, size.height * 0.1995980);
    path_12.cubicTo(
        size.width * 0.5031848,
        size.height * 0.1996039,
        size.width * 0.5031848,
        size.height * 0.1996098,
        size.width * 0.5031848,
        size.height * 0.1996167);
    path_12.cubicTo(
        size.width * 0.5031759,
        size.height * 0.2006039,
        size.width * 0.5024429,
        size.height * 0.2013912,
        size.width * 0.5016188,
        size.height * 0.2013912);
    path_12.cubicTo(
        size.width * 0.5007893,
        size.height * 0.2013912,
        size.width * 0.5000527,
        size.height * 0.2005941,
        size.width * 0.5000527,
        size.height * 0.1995980);
    path_12.lineTo(size.width * 0.5002366, size.height * 0.1618265);
    path_12.close();
    path_12.moveTo(size.width * 0.4957723, size.height * 0.1618000);
    path_12.lineTo(size.width * 0.4989045, size.height * 0.1618186);
    path_12.lineTo(size.width * 0.4989062, size.height * 0.1615373);
    path_12.lineTo(size.width * 0.4989330, size.height * 0.1612765);
    path_12.cubicTo(
        size.width * 0.4989143,
        size.height * 0.1614569,
        size.width * 0.4989000,
        size.height * 0.1616314,
        size.width * 0.4989045,
        size.height * 0.1618000);
    path_12.cubicTo(
        size.width * 0.4989054,
        size.height * 0.1618059,
        size.width * 0.4989143,
        size.height * 0.1618127,
        size.width * 0.4989143,
        size.height * 0.1618186);
    path_12.moveTo(size.width * 0.4957723, size.height * 0.1618000);
    path_12.cubicTo(
        size.width * 0.4957723,
        size.height * 0.1618059,
        size.width * 0.4957723,
        size.height * 0.1618206,
        size.width * 0.4957723,
        size.height * 0.1618265);
    path_12.close();
    path_12.moveTo(size.width * 0.4957223, size.height * 0.1618265);
    path_12.lineTo(size.width * 0.4955384, size.height * 0.1996971);
    path_12.cubicTo(
        size.width * 0.4954455,
        size.height * 0.2033853,
        size.width * 0.4982098,
        size.height * 0.2063745,
        size.width * 0.5017107,
        size.height * 0.2063745);
    path_12.cubicTo(
        size.width * 0.5051196,
        size.height * 0.2063745,
        size.width * 0.5078830,
        size.height * 0.2033853,
        size.width * 0.5078830,
        size.height * 0.1996971);
    path_12.lineTo(size.width * 0.5080679, size.height * 0.1619255);
    path_12.cubicTo(
        size.width * 0.5081598,
        size.height * 0.1582382,
        size.width * 0.5053955,
        size.height * 0.1551490,
        size.width * 0.5018946,
        size.height * 0.1551490);
    path_12.cubicTo(
        size.width * 0.4984857,
        size.height * 0.1551490,
        size.width * 0.4957223,
        size.height * 0.1581392,
        size.width * 0.4957223,
        size.height * 0.1618265);
    path_12.close();

    Paint paint_12_fill = Paint()..style = PaintingStyle.fill;
    paint_12_fill.color = Colors.black;
    canvas.drawPath(path_12, paint_12_fill);

    Path path_13 = Path()..fillType = PathFillType.evenOdd;
    path_13.moveTo(size.width * 0.2602446, size.height * 0.9424853);
    path_13.lineTo(size.width * 0.7678786, size.height * 0.9424853);
    path_13.lineTo(size.width * 0.8170759, size.height * 0.5036784);
    path_13.cubicTo(
        size.width * 0.8184580,
        size.height * 0.4914196,
        size.width * 0.8128375,
        size.height * 0.4793608,
        size.width * 0.8027955,
        size.height * 0.4734814);
    path_13.cubicTo(
        size.width * 0.6131929,
        size.height * 0.3610637,
        size.width * 0.4194446,
        size.height * 0.3593696,
        size.width * 0.2213661,
        size.height * 0.4733814);
    path_13.cubicTo(
        size.width * 0.2110473,
        size.height * 0.4793608,
        size.width * 0.2052429,
        size.height * 0.4915196,
        size.width * 0.2068089,
        size.height * 0.5040765);
    path_13.lineTo(size.width * 0.2602446, size.height * 0.9424853);
    path_13.close();

    Paint paint_13_fill = Paint()..style = PaintingStyle.fill;
    paint_13_fill.color = Colors.white;
    canvas.drawPath(path_13, paint_13_fill);

    Path path_14 = Path()..fillType = PathFillType.evenOdd;
    path_14.moveTo(size.width * 0.2030411, size.height * 0.5047745);
    path_14.lineTo(size.width * 0.2565688, size.height * 0.9424853);
    path_14.lineTo(size.width * 0.2640312, size.height * 0.9424853);
    path_14.lineTo(size.width * 0.2104116, size.height * 0.5035784);
    path_14.cubicTo(
        size.width * 0.2091214,
        size.height * 0.4927157,
        size.width * 0.2141893,
        size.height * 0.4820520,
        size.width * 0.2230330,
        size.height * 0.4769696);
    path_14.cubicTo(
        size.width * 0.3203223,
        size.height * 0.4209598,
        size.width * 0.4182563,
        size.height * 0.3925569,
        size.width * 0.5140705,
        size.height * 0.3925569);
    path_14.lineTo(size.width * 0.5151768, size.height * 0.3925569);
    path_14.cubicTo(
        size.width * 0.6106223,
        size.height * 0.3927559,
        size.width * 0.7068062,
        size.height * 0.4211588,
        size.width * 0.8009625,
        size.height * 0.4769696);
    path_14.cubicTo(
        size.width * 0.8097143,
        size.height * 0.4821520,
        size.width * 0.8145973,
        size.height * 0.4924167,
        size.width * 0.8134000,
        size.height * 0.5031804);
    path_14.lineTo(size.width * 0.7641107, size.height * 0.9424853);
    path_14.lineTo(size.width * 0.7715732, size.height * 0.9424853);
    path_14.lineTo(size.width * 0.8206786, size.height * 0.5041765);
    path_14.cubicTo(
        size.width * 0.8222446,
        size.height * 0.4901245,
        size.width * 0.8158875,
        size.height * 0.4767696,
        size.width * 0.8044634,
        size.height * 0.4699931);
    path_14.cubicTo(
        size.width * 0.7092018,
        size.height * 0.4134853,
        size.width * 0.6118205,
        size.height * 0.3847833,
        size.width * 0.5151768,
        size.height * 0.3845833);
    path_14.lineTo(size.width * 0.5140705, size.height * 0.3845833);
    path_14.cubicTo(
        size.width * 0.4169661,
        size.height * 0.3845833,
        size.width * 0.3179268,
        size.height * 0.4132863,
        size.width * 0.2195321,
        size.height * 0.4699931);
    path_14.cubicTo(
        size.width * 0.2079241,
        size.height * 0.4766706,
        size.width * 0.2012911,
        size.height * 0.4906225,
        size.width * 0.2030411,
        size.height * 0.5047745);
    path_14.close();

    Paint paint_14_fill = Paint()..style = PaintingStyle.fill;
    paint_14_fill.color = Colors.black;
    canvas.drawPath(path_14, paint_14_fill);

    Path path_15 = Path()..fillType = PathFillType.evenOdd;
    path_15.moveTo(size.width * 0.6575134, size.height * 0.9035157);
    path_15.lineTo(size.width * 0.3498929, size.height * 0.9035157);
    path_15.lineTo(size.width * 0.3498929, size.height * 0.5333745);
    path_15.cubicTo(
        size.width * 0.4524330,
        size.height * 0.5059676,
        size.width * 0.5549732,
        size.height * 0.5059676,
        size.width * 0.6575134,
        size.height * 0.5333745);
    path_15.lineTo(size.width * 0.6575134, size.height * 0.9035157);
    path_15.close();

    Paint paint_15_fill = Paint()..style = PaintingStyle.fill;
    paint_15_fill.color = activeColor;
    canvas.drawPath(path_15, paint_15_fill);

    Path path_16 = Path()..fillType = PathFillType.evenOdd;
    path_16.moveTo(size.width * 0.3543571, size.height * 0.8986137);
    path_16.lineTo(size.width * 0.6530491, size.height * 0.8986137);
    path_16.lineTo(size.width * 0.6530491, size.height * 0.5372373);
    path_16.cubicTo(
        size.width * 0.5534759,
        size.height * 0.5112167,
        size.width * 0.4539304,
        size.height * 0.5112167,
        size.width * 0.3543571,
        size.height * 0.5372373);
    path_16.lineTo(size.width * 0.3543571, size.height * 0.8986137);
    path_16.close();
    path_16.moveTo(size.width * 0.3498929, size.height * 0.5333745);
    path_16.cubicTo(
        size.width * 0.4524330,
        size.height * 0.5059676,
        size.width * 0.5549732,
        size.height * 0.5059676,
        size.width * 0.6575134,
        size.height * 0.5333745);
    path_16.lineTo(size.width * 0.6575134, size.height * 0.9035157);
    path_16.lineTo(size.width * 0.3498929, size.height * 0.9035157);
    path_16.lineTo(size.width * 0.3498929, size.height * 0.5333745);
    path_16.close();

    // path_16.fillType = PathFillType.evenOdd
    Paint paint_16_fill = Paint()..style = PaintingStyle.fill;
    paint_16_fill.color = Colors.black;
    canvas.drawPath(path_16, paint_16_fill);

    Path path_17 = Path()..fillType = PathFillType.evenOdd;
    path_17.moveTo(size.width * 0.4727045, size.height * 0.5994441);
    path_17.lineTo(size.width * 0.5036598, size.height * 0.5662569);
    path_17.lineTo(size.width * 0.4781402, size.height * 0.5128392);
    path_17.lineTo(size.width * 0.4079375, size.height * 0.5208118);
    path_17.lineTo(size.width * 0.4727045, size.height * 0.5994441);
    path_17.close();
    path_17.moveTo(size.width * 0.4729045, size.height * 0.5923794);
    path_17.lineTo(size.width * 0.4981616, size.height * 0.5653010);
    path_17.lineTo(size.width * 0.4755911, size.height * 0.5180569);
    path_17.lineTo(size.width * 0.4171536, size.height * 0.5246931);
    path_17.lineTo(size.width * 0.4729045, size.height * 0.5923794);
    path_17.close();

    path_17.fillType = PathFillType.evenOdd;
    Paint paint_17_fill = Paint()..style = PaintingStyle.fill;
    paint_17_fill.color = Colors.black;
    canvas.drawPath(path_17, paint_17_fill);

    Path path_18 = Path()..fillType = PathFillType.evenOdd;
    path_18.moveTo(size.width * 0.5036580, size.height * 0.5662667);
    path_18.lineTo(size.width * 0.5362714, size.height * 0.5994539);
    path_18.lineTo(size.width * 0.6073955, size.height * 0.5208216);
    path_18.lineTo(size.width * 0.5493536, size.height * 0.5128480);
    path_18.lineTo(size.width * 0.5036580, size.height * 0.5662667);
    path_18.close();
    path_18.moveTo(size.width * 0.5099884, size.height * 0.5660255);
    path_18.lineTo(size.width * 0.5361429, size.height * 0.5926402);
    path_18.lineTo(size.width * 0.5978241, size.height * 0.5244471);
    path_18.lineTo(size.width * 0.5510536, size.height * 0.5180225);
    path_18.lineTo(size.width * 0.5099884, size.height * 0.5660255);
    path_18.close();

    path_18.fillType = PathFillType.evenOdd;
    Paint paint_18_fill = Paint()..style = PaintingStyle.fill;
    paint_18_fill.color = Colors.black;
    canvas.drawPath(path_18, paint_18_fill);

    Path path_19 = Path()..fillType = PathFillType.evenOdd;
    path_19.moveTo(size.width * 0.3498813, size.height * 0.5334902);
    path_19.cubicTo(
        size.width * 0.2596857,
        size.height * 0.5660794,
        size.width * 0.2062509,
        size.height * 0.6601598,
        size.width * 0.2062509,
        size.height * 0.7499539);
    path_19.lineTo(size.width * 0.2062509, size.height * 0.7549373);
    path_19.cubicTo(
        size.width * 0.2126080,
        size.height * 0.8800118,
        size.width * 0.2824420,
        size.height * 0.9036314,
        size.width * 0.3082384,
        size.height * 0.9036314);
    path_19.cubicTo(
        size.width * 0.3267562,
        size.height * 0.9036314,
        size.width * 0.3498813,
        size.height * 0.8942627,
        size.width * 0.3498813,
        size.height * 0.8826029);
    path_19.lineTo(size.width * 0.3498813, size.height * 0.5334902);
    path_19.close();

    Paint paint_19_fill = Paint()..style = PaintingStyle.fill;
    paint_19_fill.color = activeColor;
    canvas.drawPath(path_19, paint_19_fill);

    Path path_20 = Path()..fillType = PathFillType.evenOdd;
    path_20.moveTo(size.width * 0.3454170, size.height * 0.5403725);
    path_20.cubicTo(
        size.width * 0.2607241,
        size.height * 0.5741422,
        size.width * 0.2107152,
        size.height * 0.6640922,
        size.width * 0.2107152,
        size.height * 0.7499539);
    path_20.lineTo(size.width * 0.2107152, size.height * 0.7548000);
    path_20.cubicTo(
        size.width * 0.2138616,
        size.height * 0.8162157,
        size.width * 0.2325152,
        size.height * 0.8520588,
        size.width * 0.2529652,
        size.height * 0.8725676);
    path_20.cubicTo(
        size.width * 0.2735464,
        size.height * 0.8932069,
        size.width * 0.2963491,
        size.height * 0.8987294,
        size.width * 0.3082384,
        size.height * 0.8987294);
    path_20.cubicTo(
        size.width * 0.3169080,
        size.height * 0.8987294,
        size.width * 0.3267205,
        size.height * 0.8965157,
        size.width * 0.3342491,
        size.height * 0.8929824);
    path_20.cubicTo(
        size.width * 0.3380187,
        size.height * 0.8912127,
        size.width * 0.3409839,
        size.height * 0.8892206,
        size.width * 0.3429321,
        size.height * 0.8872235);
    path_20.cubicTo(
        size.width * 0.3449063,
        size.height * 0.8852010,
        size.width * 0.3454170,
        size.height * 0.8836343,
        size.width * 0.3454170,
        size.height * 0.8826029);
    path_20.lineTo(size.width * 0.3454170, size.height * 0.5403725);
    path_20.close();
    path_20.moveTo(size.width * 0.3454170, size.height * 0.5351647);
    path_20.cubicTo(
        size.width * 0.2579259,
        size.height * 0.5692039,
        size.width * 0.2062509,
        size.height * 0.6616510,
        size.width * 0.2062509,
        size.height * 0.7499539);
    path_20.lineTo(size.width * 0.2062509, size.height * 0.7549373);
    path_20.cubicTo(
        size.width * 0.2126080,
        size.height * 0.8800118,
        size.width * 0.2824420,
        size.height * 0.9036314,
        size.width * 0.3082384,
        size.height * 0.9036314);
    path_20.cubicTo(
        size.width * 0.3267562,
        size.height * 0.9036314,
        size.width * 0.3498813,
        size.height * 0.8942627,
        size.width * 0.3498813,
        size.height * 0.8826029);
    path_20.lineTo(size.width * 0.3498813, size.height * 0.5334902);
    path_20.cubicTo(
        size.width * 0.3483830,
        size.height * 0.5340314,
        size.width * 0.3468946,
        size.height * 0.5345902,
        size.width * 0.3454170,
        size.height * 0.5351647);
    path_20.close();
    path_20.fillType = PathFillType.evenOdd;
    Paint paint_20_fill = Paint()..style = PaintingStyle.fill;
    paint_20_fill.color = Colors.black;
    canvas.drawPath(path_20, paint_20_fill);

    Path path_21 = Path()..fillType = PathFillType.evenOdd;
    path_21.moveTo(size.width * 0.8384348, size.height * 0.7350892);
    path_21.cubicTo(
        size.width * 0.8248000,
        size.height * 0.6364245,
        size.width * 0.7476875,
        size.height * 0.5659647,
        size.width * 0.6574929,
        size.height * 0.5333755);
    path_21.lineTo(size.width * 0.6574929, size.height * 0.8216941);
    path_21.cubicTo(
        size.width * 0.6574929,
        size.height * 0.8216941,
        size.width * 0.6711277,
        size.height * 0.8398324,
        size.width * 0.7028205,
        size.height * 0.8766078);
    path_21.cubicTo(
        size.width * 0.7308277,
        size.height * 0.9090971,
        size.width * 0.8456214,
        size.height * 0.9381980,
        size.width * 0.8519777,
        size.height * 0.8131235);
    path_21.lineTo(size.width * 0.8384348, size.height * 0.7350892);
    path_21.close();

    Paint paint_21_fill = Paint()..style = PaintingStyle.fill;
    paint_21_fill.color = activeColor;
    canvas.drawPath(path_21, paint_21_fill);

    Path path_22 = Path()..fillType = PathFillType.evenOdd;
    path_22.moveTo(size.width * 0.8340339, size.height * 0.7359157);
    path_22.lineTo(size.width * 0.8340214, size.height * 0.7358245);
    path_22.cubicTo(
        size.width * 0.8210295,
        size.height * 0.6418157,
        size.width * 0.7487134,
        size.height * 0.5733706,
        size.width * 0.6619571,
        size.height * 0.5402098);
    path_22.lineTo(size.width * 0.6619571, size.height * 0.8199118);
    path_22.cubicTo(
        size.width * 0.6623554,
        size.height * 0.8204294,
        size.width * 0.6628714,
        size.height * 0.8210971,
        size.width * 0.6635098,
        size.height * 0.8219157);
    path_22.cubicTo(
        size.width * 0.6652661,
        size.height * 0.8241686,
        size.width * 0.6679438,
        size.height * 0.8275637,
        size.width * 0.6716125,
        size.height * 0.8321088);
    path_22.cubicTo(
        size.width * 0.6789491,
        size.height * 0.8411990,
        size.width * 0.6902482,
        size.height * 0.8548882,
        size.width * 0.7060625,
        size.height * 0.8732382);
    path_22.cubicTo(
        size.width * 0.7123786,
        size.height * 0.8805657,
        size.width * 0.7240545,
        size.height * 0.8881324,
        size.width * 0.7384420,
        size.height * 0.8933265);
    path_22.cubicTo(
        size.width * 0.7527500,
        size.height * 0.8984922,
        size.width * 0.7692625,
        size.height * 0.9011333,
        size.width * 0.7849920,
        size.height * 0.8990843);
    path_22.cubicTo(
        size.width * 0.8006813,
        size.height * 0.8970402,
        size.width * 0.8154321,
        size.height * 0.8903647,
        size.width * 0.8266518,
        size.height * 0.8770490);
    path_22.cubicTo(
        size.width * 0.8377982,
        size.height * 0.8638206,
        size.width * 0.8458679,
        size.height * 0.8435539,
        size.width * 0.8474893,
        size.height * 0.8134471);
    path_22.lineTo(size.width * 0.8340339, size.height * 0.7359157);
    path_22.close();
    path_22.moveTo(size.width * 0.8519777, size.height * 0.8131235);
    path_22.cubicTo(
        size.width * 0.8456214,
        size.height * 0.9381980,
        size.width * 0.7308277,
        size.height * 0.9090971,
        size.width * 0.7028205,
        size.height * 0.8766078);
    path_22.cubicTo(
        size.width * 0.6711277,
        size.height * 0.8398324,
        size.width * 0.6574929,
        size.height * 0.8216941,
        size.width * 0.6574929,
        size.height * 0.8216941);
    path_22.lineTo(size.width * 0.6574929, size.height * 0.5333755);
    path_22.cubicTo(
        size.width * 0.6589839,
        size.height * 0.5339147,
        size.width * 0.6604723,
        size.height * 0.5344637,
        size.width * 0.6619571,
        size.height * 0.5350235);
    path_22.cubicTo(
        size.width * 0.7502205,
        size.height * 0.5683029,
        size.width * 0.8250250,
        size.height * 0.6380569,
        size.width * 0.8384348,
        size.height * 0.7350892);
    path_22.lineTo(size.width * 0.8519777, size.height * 0.8131235);
    path_22.close();

    Paint paint_22_fill = Paint()..style = PaintingStyle.fill;
    paint_22_fill.color = Colors.black;
    canvas.drawPath(path_22, paint_22_fill);

    Path path_23 = Path()..fillType = PathFillType.evenOdd;
    path_23.moveTo(size.width * 0.6868920, size.height * 0.9424804);
    path_23.cubicTo(
        size.width * 0.6735330,
        size.height * 0.8799931,
        size.width * 0.6575027,
        size.height * 0.8216912,
        size.width * 0.6575027,
        size.height * 0.8216912);
    path_23.lineTo(size.width * 0.3498821, size.height * 0.8216912);
    path_23.cubicTo(
        size.width * 0.3498821,
        size.height * 0.8216912,
        size.width * 0.3339437,
        size.height * 0.8729167,
        size.width * 0.3237170,
        size.height * 0.9424804);
    path_23.lineTo(size.width * 0.6868920, size.height * 0.9424804);
    path_23.close();

    Paint paint_23_fill = Paint()..style = PaintingStyle.fill;
    paint_23_fill.color = activeColor;
    canvas.drawPath(path_23, paint_23_fill);

    Path path_24 = Path()..fillType = PathFillType.evenOdd;
    path_24.moveTo(size.width * 0.3498821, size.height * 0.8216912);
    path_24.cubicTo(
        size.width * 0.3498821,
        size.height * 0.8216912,
        size.width * 0.3346884,
        size.height * 0.8705235,
        size.width * 0.3244518,
        size.height * 0.9375784);
    path_24.cubicTo(
        size.width * 0.3242045,
        size.height * 0.9392020,
        size.width * 0.3239589,
        size.height * 0.9408363,
        size.width * 0.3237170,
        size.height * 0.9424804);
    path_24.lineTo(size.width * 0.6868920, size.height * 0.9424804);
    path_24.cubicTo(
        size.width * 0.6865420,
        size.height * 0.9408431,
        size.width * 0.6861902,
        size.height * 0.9392088,
        size.width * 0.6858366,
        size.height * 0.9375784);
    path_24.cubicTo(
        size.width * 0.6727045,
        size.height * 0.8769775,
        size.width * 0.6575027,
        size.height * 0.8216912,
        size.width * 0.6575027,
        size.height * 0.8216912);
    path_24.lineTo(size.width * 0.3498821, size.height * 0.8216912);
    path_24.close();
    path_24.moveTo(size.width * 0.6541759, size.height * 0.8265931);
    path_24.lineTo(size.width * 0.3531134, size.height * 0.8265931);
    path_24.cubicTo(
        size.width * 0.3526920,
        size.height * 0.8280304,
        size.width * 0.3521464,
        size.height * 0.8299186,
        size.width * 0.3514982,
        size.height * 0.8322265);
    path_24.cubicTo(
        size.width * 0.3498580,
        size.height * 0.8380647,
        size.width * 0.3475616,
        size.height * 0.8465843,
        size.width * 0.3449455,
        size.height * 0.8572716);
    path_24.cubicTo(
        size.width * 0.3400018,
        size.height * 0.8774716,
        size.width * 0.3339223,
        size.height * 0.9053931,
        size.width * 0.3289786,
        size.height * 0.9375784);
    path_24.lineTo(size.width * 0.6812473, size.height * 0.9375784);
    path_24.cubicTo(
        size.width * 0.6749455,
        size.height * 0.9085843,
        size.width * 0.6681893,
        size.height * 0.8808725,
        size.width * 0.6628875,
        size.height * 0.8599676);
    path_24.cubicTo(
        size.width * 0.6600545,
        size.height * 0.8487971,
        size.width * 0.6576384,
        size.height * 0.8395755,
        size.width * 0.6559304,
        size.height * 0.8331500);
    path_24.cubicTo(
        size.width * 0.6552134,
        size.height * 0.8304500,
        size.width * 0.6546214,
        size.height * 0.8282431,
        size.width * 0.6541759,
        size.height * 0.8265931);
    path_24.close();

    Paint paint_24_fill = Paint()..style = PaintingStyle.fill;
    paint_24_fill.color = activeColor;
    canvas.drawPath(path_24, paint_24_fill);

    Path path_25 = Path()..fillType = PathFillType.evenOdd;
    path_25.moveTo(size.width * 0.5333946, size.height * 0.5408392);
    path_25.lineTo(size.width * 0.5036366, size.height * 0.5662529);
    path_25.lineTo(size.width * 0.4770116, size.height * 0.5408392);
    path_25.lineTo(size.width * 0.4770116, size.height * 0.4754618);
    path_25.lineTo(size.width * 0.5333946, size.height * 0.4754618);
    path_25.lineTo(size.width * 0.5333946, size.height * 0.5408392);
    path_25.close();

    Paint paint_25_fill = Paint()..style = PaintingStyle.fill;
    paint_25_fill.color = Colors.white;
    canvas.drawPath(path_25, paint_25_fill);

    Path path_26 = Path()..fillType = PathFillType.evenOdd;
    path_26.moveTo(size.width * 0.5289304, size.height * 0.5384412);
    path_26.lineTo(size.width * 0.5289304, size.height * 0.4803637);
    path_26.lineTo(size.width * 0.4814759, size.height * 0.4803637);
    path_26.lineTo(size.width * 0.4814759, size.height * 0.5386049);
    path_26.lineTo(size.width * 0.5037946, size.height * 0.5599078);
    path_26.lineTo(size.width * 0.5289304, size.height * 0.5384412);
    path_26.close();
    path_26.moveTo(size.width * 0.5036366, size.height * 0.5662529);
    path_26.lineTo(size.width * 0.5333946, size.height * 0.5408392);
    path_26.lineTo(size.width * 0.5333946, size.height * 0.4754618);
    path_26.lineTo(size.width * 0.4770116, size.height * 0.4754618);
    path_26.lineTo(size.width * 0.4770116, size.height * 0.5408392);
    path_26.lineTo(size.width * 0.5036366, size.height * 0.5662529);
    path_26.close();
    path_26.fillType = PathFillType.evenOdd;
    Paint paint_26_fill = Paint()..style = PaintingStyle.fill;
    paint_26_fill.color = Colors.black;
    canvas.drawPath(path_26, paint_26_fill);

    Path path_27 = Path()..fillType = PathFillType.evenOdd;
    path_27.moveTo(size.width * 0.5737437, size.height * 0.3691275);
    path_27.cubicTo(
        size.width * 0.5771527,
        size.height * 0.4033118,
        size.width * 0.5606616,
        size.height * 0.4588225,
        size.width * 0.5363393,
        size.height * 0.4757647);
    path_27.cubicTo(
        size.width * 0.5231652,
        size.height * 0.4848343,
        size.width * 0.5075946,
        size.height * 0.4901167,
        size.width * 0.4910116,
        size.height * 0.4901167);
    path_27.cubicTo(
        size.width * 0.4700063,
        size.height * 0.4901167,
        size.width * 0.4326018,
        size.height * 0.4618127,
        size.width * 0.4254152,
        size.height * 0.4384922);
    path_27.cubicTo(
        size.width * 0.4230196,
        size.height * 0.4305186,
        size.width * 0.4211777,
        size.height * 0.4105873,
        size.width * 0.4211777,
        size.height * 0.3973324);
    path_27.cubicTo(
        size.width * 0.4211777,
        size.height * 0.3861696,
        size.width * 0.4230196,
        size.height * 0.3775990,
        size.width * 0.4212696,
        size.height * 0.3714196);
    path_27.cubicTo(
        size.width * 0.4212696,
        size.height * 0.3714196,
        size.width * 0.4205321,
        size.height * 0.3534814,
        size.width * 0.4241259,
        size.height * 0.3436147);
    path_27.cubicTo(
        size.width * 0.4339839,
        size.height * 0.3164069,
        size.width * 0.4506589,
        size.height * 0.2866088,
        size.width * 0.4506589,
        size.height * 0.2866088);
    path_27.cubicTo(
        size.width * 0.5044625,
        size.height * 0.2726559,
        size.width * 0.5644393,
        size.height * 0.2767422,
        size.width * 0.5737437,
        size.height * 0.3691275);
    path_27.close();

    Paint paint_27_fill = Paint()..style = PaintingStyle.fill;
    paint_27_fill.color = Colors.white;
    canvas.drawPath(path_27, paint_27_fill);

    Path path_28 = Path()..fillType = PathFillType.evenOdd;
    path_28.moveTo(size.width * 0.4282741, size.height * 0.3454265);
    path_28.lineTo(size.width * 0.4282705, size.height * 0.3454343);
    path_28.cubicTo(
        size.width * 0.4267679,
        size.height * 0.3495608,
        size.width * 0.4260705,
        size.height * 0.3558118,
        size.width * 0.4258089,
        size.height * 0.3614667);
    path_28.cubicTo(
        size.width * 0.4256821,
        size.height * 0.3642078,
        size.width * 0.4256625,
        size.height * 0.3666598,
        size.width * 0.4256750,
        size.height * 0.3684245);
    path_28.cubicTo(
        size.width * 0.4256812,
        size.height * 0.3693059,
        size.width * 0.4256946,
        size.height * 0.3700118,
        size.width * 0.4257071,
        size.height * 0.3704912);
    path_28.lineTo(size.width * 0.4257116, size.height * 0.3706422);
    path_28.cubicTo(
        size.width * 0.4266652,
        size.height * 0.3745059,
        size.width * 0.4265634,
        size.height * 0.3787549,
        size.width * 0.4263357,
        size.height * 0.3828088);
    path_28.cubicTo(
        size.width * 0.4262670,
        size.height * 0.3840324,
        size.width * 0.4261848,
        size.height * 0.3852588,
        size.width * 0.4261018,
        size.height * 0.3865029);
    path_28.cubicTo(
        size.width * 0.4258759,
        size.height * 0.3898853,
        size.width * 0.4256420,
        size.height * 0.3933980,
        size.width * 0.4256420,
        size.height * 0.3973324);
    path_28.cubicTo(
        size.width * 0.4256420,
        size.height * 0.4037480,
        size.width * 0.4260902,
        size.height * 0.4118637,
        size.width * 0.4268357,
        size.height * 0.4193598);
    path_28.cubicTo(
        size.width * 0.4275920,
        size.height * 0.4269578,
        size.width * 0.4286134,
        size.height * 0.4334775,
        size.width * 0.4296500,
        size.height * 0.4369382);
    path_28.cubicTo(
        size.width * 0.4327911,
        size.height * 0.4471088,
        size.width * 0.4429875,
        size.height * 0.4592529,
        size.width * 0.4556080,
        size.height * 0.4690461);
    path_28.cubicTo(
        size.width * 0.4682429,
        size.height * 0.4788510,
        size.width * 0.4819063,
        size.height * 0.4852147,
        size.width * 0.4910116,
        size.height * 0.4852147);
    path_28.cubicTo(
        size.width * 0.5067179,
        size.height * 0.4852147,
        size.width * 0.5214670,
        size.height * 0.4802147,
        size.width * 0.5339589,
        size.height * 0.4716186);
    path_28.cubicTo(
        size.width * 0.5448652,
        size.height * 0.4640147,
        size.width * 0.5546268,
        size.height * 0.4471490,
        size.width * 0.5612473,
        size.height * 0.4272314);
    path_28.cubicTo(
        size.width * 0.5678223,
        size.height * 0.4074500,
        size.width * 0.5709152,
        size.height * 0.3858049,
        size.width * 0.5693071,
        size.height * 0.3696667);
    path_28.cubicTo(
        size.width * 0.5647821,
        size.height * 0.3247382,
        size.width * 0.5481616,
        size.height * 0.3024471,
        size.width * 0.5271223,
        size.height * 0.2924931);
    path_28.cubicTo(
        size.width * 0.5060839,
        size.height * 0.2825402,
        size.width * 0.4795670,
        size.height * 0.2843775,
        size.width * 0.4535045,
        size.height * 0.2909147);
    path_28.cubicTo(
        size.width * 0.4530562,
        size.height * 0.2917373,
        size.width * 0.4524625,
        size.height * 0.2928343,
        size.width * 0.4517509,
        size.height * 0.2941676);
    path_28.cubicTo(
        size.width * 0.4500518,
        size.height * 0.2973529,
        size.width * 0.4476821,
        size.height * 0.3018853,
        size.width * 0.4450062,
        size.height * 0.3072598);
    path_28.cubicTo(
        size.width * 0.4396420,
        size.height * 0.3180373,
        size.width * 0.4331054,
        size.height * 0.3320902,
        size.width * 0.4282741,
        size.height * 0.3454265);
    path_28.close();
    path_28.moveTo(size.width * 0.4506589, size.height * 0.2866088);
    path_28.cubicTo(
        size.width * 0.5044625,
        size.height * 0.2726559,
        size.width * 0.5644393,
        size.height * 0.2767422,
        size.width * 0.5737437,
        size.height * 0.3691275);
    path_28.cubicTo(
        size.width * 0.5771527,
        size.height * 0.4033118,
        size.width * 0.5606616,
        size.height * 0.4588225,
        size.width * 0.5363393,
        size.height * 0.4757647);
    path_28.cubicTo(
        size.width * 0.5231652,
        size.height * 0.4848343,
        size.width * 0.5075946,
        size.height * 0.4901167,
        size.width * 0.4910116,
        size.height * 0.4901167);
    path_28.cubicTo(
        size.width * 0.4700063,
        size.height * 0.4901167,
        size.width * 0.4326018,
        size.height * 0.4618127,
        size.width * 0.4254152,
        size.height * 0.4384922);
    path_28.cubicTo(
        size.width * 0.4230196,
        size.height * 0.4305186,
        size.width * 0.4211777,
        size.height * 0.4105873,
        size.width * 0.4211777,
        size.height * 0.3973324);
    path_28.cubicTo(
        size.width * 0.4211777,
        size.height * 0.3932029,
        size.width * 0.4214295,
        size.height * 0.3894284,
        size.width * 0.4216589,
        size.height * 0.3859980);
    path_28.cubicTo(
        size.width * 0.4220491,
        size.height * 0.3801559,
        size.width * 0.4223723,
        size.height * 0.3753137,
        size.width * 0.4212696,
        size.height * 0.3714196);
    path_28.cubicTo(
        size.width * 0.4212696,
        size.height * 0.3714196,
        size.width * 0.4205321,
        size.height * 0.3534814,
        size.width * 0.4241259,
        size.height * 0.3436147);
    path_28.cubicTo(
        size.width * 0.4339839,
        size.height * 0.3164069,
        size.width * 0.4506589,
        size.height * 0.2866088,
        size.width * 0.4506589,
        size.height * 0.2866088);
    path_28.close();

    Paint paint_28_fill = Paint()..style = PaintingStyle.fill;
    paint_28_fill.color = Colors.black;
    canvas.drawPath(path_28, paint_28_fill);

    Path path_29 = Path()..fillType = PathFillType.evenOdd;
    path_29.moveTo(size.width * 0.5938402, size.height * 0.2945833);
    path_29.cubicTo(
        size.width * 0.5690571,
        size.height * 0.2598010,
        size.width * 0.5322054,
        size.height * 0.2457490,
        size.width * 0.5163589,
        size.height * 0.2457490);
    path_29.cubicTo(
        size.width * 0.4974723,
        size.height * 0.2457490,
        size.width * 0.4802446,
        size.height * 0.2524265,
        size.width * 0.4685437,
        size.height * 0.2646843);
    path_29.cubicTo(
        size.width * 0.4145563,
        size.height * 0.2520275,
        size.width * 0.3689518,
        size.height * 0.3178039,
        size.width * 0.4000920,
        size.height * 0.3713216);
    path_29.lineTo(size.width * 0.4187937, size.height * 0.3713216);
    path_29.cubicTo(
        size.width * 0.4187937,
        size.height * 0.3715216,
        size.width * 0.4187937,
        size.height * 0.3716206,
        size.width * 0.4187937,
        size.height * 0.3718206);
    path_29.cubicTo(
        size.width * 0.4193464,
        size.height * 0.3826833,
        size.width * 0.4183330,
        size.height * 0.3956392,
        size.width * 0.4117920,
        size.height * 0.4064029);
    path_29.cubicTo(
        size.width * 0.3886679,
        size.height * 0.4449716,
        size.width * 0.3735580,
        size.height * 0.5020775,
        size.width * 0.3893125,
        size.height * 0.5274902);
    path_29.cubicTo(
        size.width * 0.4081991,
        size.height * 0.5579873,
        size.width * 0.4732429,
        size.height * 0.5494157,
        size.width * 0.4769277,
        size.height * 0.5404461);
    path_29.cubicTo(
        size.width * 0.4808893,
        size.height * 0.5306794,
        size.width * 0.4777571,
        size.height * 0.4965961,
        size.width * 0.4768357,
        size.height * 0.4871275);
    path_29.cubicTo(
        size.width * 0.4768357,
        size.height * 0.4871275,
        size.width * 0.4696491,
        size.height * 0.4843373,
        size.width * 0.4613580,
        size.height * 0.4791549);
    path_29.cubicTo(
        size.width * 0.4539875,
        size.height * 0.4745706,
        size.width * 0.4459723,
        size.height * 0.4676941,
        size.width * 0.4404446,
        size.height * 0.4619137);
    path_29.cubicTo(
        size.width * 0.4279143,
        size.height * 0.4487588,
        size.width * 0.4248741,
        size.height * 0.4405863,
        size.width * 0.4230321,
        size.height * 0.4262353);
    path_29.cubicTo(
        size.width * 0.4193464,
        size.height * 0.3983304,
        size.width * 0.4230321,
        size.height * 0.3847765,
        size.width * 0.4217420,
        size.height * 0.3716206);
    path_29.lineTo(size.width * 0.4451429, size.height * 0.3714216);
    path_29.cubicTo(
        size.width * 0.4451429,
        size.height * 0.3714216,
        size.width * 0.4389705,
        size.height * 0.3144157,
        size.width * 0.4642134,
        size.height * 0.2929882);
    path_29.cubicTo(
        size.width * 0.4977491,
        size.height * 0.3114255,
        size.width * 0.4990384,
        size.height * 0.3662392,
        size.width * 0.4990384,
        size.height * 0.3662392);
    path_29.lineTo(size.width * 0.5581857, size.height * 0.3662392);
    path_29.cubicTo(
        size.width * 0.5581857,
        size.height * 0.3717206,
        size.width * 0.5669384,
        size.height * 0.4016186,
        size.width * 0.5539482,
        size.height * 0.4233451);
    path_29.cubicTo(
        size.width * 0.5308232,
        size.height * 0.4619137,
        size.width * 0.5114759,
        size.height * 0.5104490,
        size.width * 0.5278750,
        size.height * 0.5353637);
    path_29.cubicTo(
        size.width * 0.5510000,
        size.height * 0.5703451,
        size.width * 0.6110688,
        size.height * 0.5418422,
        size.width * 0.6147536,
        size.height * 0.5327725);
    path_29.cubicTo(
        size.width * 0.6586071,
        size.height * 0.4259363,
        size.width * 0.6239670,
        size.height * 0.3367392,
        size.width * 0.5938402,
        size.height * 0.2945833);
    path_29.close();

    Paint paint_29_fill = Paint()..style = PaintingStyle.fill;
    paint_29_fill.color = Colors.black;
    canvas.drawPath(path_29, paint_29_fill);

    Path path_30 = Path()..fillType = PathFillType.evenOdd;
    path_30.moveTo(size.width * 0.3623411, size.height * 0.4522706);
    path_30.cubicTo(
        size.width * 0.3560759,
        size.height * 0.4479853,
        size.width * 0.3474161,
        size.height * 0.4505765,
        size.width * 0.3429938,
        size.height * 0.4580510);
    path_30.lineTo(size.width * 0.3272393, size.height * 0.4848598);
    path_30.lineTo(size.width * 0.3429938, size.height * 0.4310431);
    path_30.cubicTo(
        size.width * 0.3457580,
        size.height * 0.4226716,
        size.width * 0.3423491,
        size.height * 0.4137020,
        size.width * 0.3353473,
        size.height * 0.4109108);
    path_30.cubicTo(
        size.width * 0.3283455,
        size.height * 0.4082206,
        size.width * 0.3215277,
        size.height * 0.4127049,
        size.width * 0.3176580,
        size.height * 0.4211765);
    path_30.lineTo(size.width * 0.3055893, size.height * 0.4509745);
    path_30.lineTo(size.width * 0.3077080, size.height * 0.4185853);
    path_30.cubicTo(
        size.width * 0.3085375,
        size.height * 0.4097157,
        size.width * 0.3032857,
        size.height * 0.4018422,
        size.width * 0.2958232,
        size.height * 0.4010451);
    path_30.cubicTo(
        size.width * 0.2884527,
        size.height * 0.4002471,
        size.width * 0.2827411,
        size.height * 0.4063265,
        size.width * 0.2808982,
        size.height * 0.4155951);
    path_30.lineTo(size.width * 0.2687375, size.height * 0.4894441);
    path_30.cubicTo(
        size.width * 0.2683687,
        size.height * 0.4908392,
        size.width * 0.2681848,
        size.height * 0.4924333,
        size.width * 0.2680000,
        size.height * 0.4941284);
    path_30.lineTo(size.width * 0.2679080, size.height * 0.4945265);
    path_30.cubicTo(
        size.width * 0.2668946,
        size.height * 0.5049912,
        size.width * 0.2687375,
        size.height * 0.5201392,
        size.width * 0.2733437,
        size.height * 0.5355873);
    path_30.cubicTo(
        size.width * 0.2762920,
        size.height * 0.5454539,
        size.width * 0.2890982,
        size.height * 0.5604029,
        size.width * 0.2916777,
        size.height * 0.5778431);
    path_30.cubicTo(
        size.width * 0.2614589,
        size.height * 0.6541833,
        size.width * 0.2486527,
        size.height * 0.6977353,
        size.width * 0.2284768,
        size.height * 0.7552402);
    path_30.cubicTo(
        size.width * 0.2011143,
        size.height * 0.8332745,
        size.width * 0.2208304,
        size.height * 0.8831049,
        size.width * 0.2540884,
        size.height * 0.8969578);
    path_30.cubicTo(
        size.width * 0.2876241,
        size.height * 0.9108108,
        size.width * 0.3238313,
        size.height * 0.8959608,
        size.width * 0.3366366,
        size.height * 0.8544020);
    path_30.cubicTo(
        size.width * 0.3601304,
        size.height * 0.7778627,
        size.width * 0.3659339,
        size.height * 0.6376402,
        size.width * 0.3577348,
        size.height * 0.5789392);
    path_30.cubicTo(
        size.width * 0.3576429,
        size.height * 0.5781422,
        size.width * 0.3574580,
        size.height * 0.5773451,
        size.width * 0.3572741,
        size.height * 0.5765480);
    path_30.cubicTo(
        size.width * 0.3698955,
        size.height * 0.5502373,
        size.width * 0.3546946,
        size.height * 0.5266176,
        size.width * 0.3440991,
        size.height * 0.5138608);
    path_30.lineTo(size.width * 0.3657500, size.height * 0.4738971);
    path_30.cubicTo(
        size.width * 0.3701723,
        size.height * 0.4662225,
        size.width * 0.3686982,
        size.height * 0.4565559,
        size.width * 0.3623411,
        size.height * 0.4522706);
    path_30.close();

    Paint paint_30_fill = Paint()..style = PaintingStyle.fill;
    paint_30_fill.color = Colors.white;
    canvas.drawPath(path_30, paint_30_fill);

    Path path_31 = Path()..fillType = PathFillType.evenOdd;
    path_31.moveTo(size.width * 0.3600054, size.height * 0.4564480);
    path_31.lineTo(size.width * 0.3599804, size.height * 0.4564314);
    path_31.cubicTo(
        size.width * 0.3561062,
        size.height * 0.4537814,
        size.width * 0.3500571,
        size.height * 0.4551157,
        size.width * 0.3467411,
        size.height * 0.4607157);
    path_31.cubicTo(
        size.width * 0.3467402,
        size.height * 0.4607176,
        size.width * 0.3467420,
        size.height * 0.4607127,
        size.width * 0.3467411,
        size.height * 0.4607157);
    path_31.lineTo(size.width * 0.3309911, size.height * 0.4875176);
    path_31.lineTo(size.width * 0.3229893, size.height * 0.4833598);
    path_31.lineTo(size.width * 0.3387688, size.height * 0.4294569);
    path_31.lineTo(size.width * 0.3387964, size.height * 0.4293716);
    path_31.cubicTo(
        size.width * 0.3408402,
        size.height * 0.4231824,
        size.width * 0.3381920,
        size.height * 0.4172775,
        size.width * 0.3338482,
        size.height * 0.4155284);
    path_31.cubicTo(
        size.width * 0.3298018,
        size.height * 0.4139902,
        size.width * 0.3249902,
        size.height * 0.4161343,
        size.width * 0.3216938,
        size.height * 0.4232765);
    path_31.lineTo(size.width * 0.3096679, size.height * 0.4529667);
    path_31.lineTo(size.width * 0.3011366, size.height * 0.4506235);
    path_31.lineTo(size.width * 0.3032598, size.height * 0.4181588);
    path_31.lineTo(size.width * 0.3032670, size.height * 0.4180843);
    path_31.cubicTo(
        size.width * 0.3038866,
        size.height * 0.4114608,
        size.width * 0.2999938,
        size.height * 0.4064157,
        size.width * 0.2953911,
        size.height * 0.4059235);
    path_31.cubicTo(
        size.width * 0.2911036,
        size.height * 0.4054598,
        size.width * 0.2868473,
        size.height * 0.4087843,
        size.width * 0.2852768,
        size.height * 0.4165578);
    path_31.lineTo(size.width * 0.2730893, size.height * 0.4905657);
    path_31.lineTo(size.width * 0.2730250, size.height * 0.4908098);
    path_31.cubicTo(
        size.width * 0.2727750,
        size.height * 0.4917549,
        size.width * 0.2726250,
        size.height * 0.4929480,
        size.width * 0.2724330,
        size.height * 0.4947088);
    path_31.lineTo(size.width * 0.2723991, size.height * 0.4950255);
    path_31.lineTo(size.width * 0.2723161, size.height * 0.4953833);
    path_31.cubicTo(
        size.width * 0.2714616,
        size.height * 0.5049402,
        size.width * 0.2731554,
        size.height * 0.5192010,
        size.width * 0.2775866,
        size.height * 0.5340618);
    path_31.cubicTo(
        size.width * 0.2781598,
        size.height * 0.5359804,
        size.width * 0.2792920,
        size.height * 0.5383706,
        size.width * 0.2809277,
        size.height * 0.5413971);
    path_31.cubicTo(
        size.width * 0.2817304,
        size.height * 0.5428794,
        size.width * 0.2826098,
        size.height * 0.5444382,
        size.width * 0.2835545,
        size.height * 0.5461118);
    path_31.lineTo(size.width * 0.2835687, size.height * 0.5461363);
    path_31.cubicTo(
        size.width * 0.2845009,
        size.height * 0.5477882,
        size.width * 0.2854911,
        size.height * 0.5495422,
        size.width * 0.2864813,
        size.height * 0.5513598);
    path_31.cubicTo(
        size.width * 0.2904250,
        size.height * 0.5586049,
        size.width * 0.2946518,
        size.height * 0.5673706,
        size.width * 0.2960839,
        size.height * 0.5770578);
    path_31.lineTo(size.width * 0.2962946, size.height * 0.5784775);
    path_31.lineTo(size.width * 0.2957723, size.height * 0.5797971);
    path_31.cubicTo(
        size.width * 0.2745330,
        size.height * 0.6334520,
        size.width * 0.2619491,
        size.height * 0.6707765,
        size.width * 0.2492946,
        size.height * 0.7083088);
    path_31.cubicTo(
        size.width * 0.2439723,
        size.height * 0.7240931,
        size.width * 0.2386384,
        size.height * 0.7399137,
        size.width * 0.2326429,
        size.height * 0.7570020);
    path_31.cubicTo(
        size.width * 0.2191875,
        size.height * 0.7953745,
        size.width * 0.2175054,
        size.height * 0.8262627,
        size.width * 0.2228705,
        size.height * 0.8489343);
    path_31.cubicTo(
        size.width * 0.2282134,
        size.height * 0.8715127,
        size.width * 0.2405402,
        size.height * 0.8860676,
        size.width * 0.2556634,
        size.height * 0.8923706);
    path_31.cubicTo(
        size.width * 0.2714438,
        size.height * 0.8988882,
        size.width * 0.2876616,
        size.height * 0.8985833,
        size.width * 0.3013375,
        size.height * 0.8919725);
    path_31.cubicTo(
        size.width * 0.3149670,
        size.height * 0.8853843,
        size.width * 0.3263804,
        size.height * 0.8723873,
        size.width * 0.3324063,
        size.height * 0.8528363);
    path_31.cubicTo(
        size.width * 0.3439741,
        size.height * 0.8151490,
        size.width * 0.3512643,
        size.height * 0.7614412,
        size.width * 0.3546170,
        size.height * 0.7096333);
    path_31.cubicTo(
        size.width * 0.3579705,
        size.height * 0.6578235,
        size.width * 0.3573482,
        size.height * 0.6085088,
        size.width * 0.3533214,
        size.height * 0.5796824);
    path_31.lineTo(size.width * 0.3533045, size.height * 0.5795569);
    path_31.cubicTo(
        size.width * 0.3532527,
        size.height * 0.5791088,
        size.width * 0.3531438,
        size.height * 0.5786049,
        size.width * 0.3529464,
        size.height * 0.5777529);
    path_31.lineTo(size.width * 0.3525250, size.height * 0.5759284);
    path_31.lineTo(size.width * 0.3533241, size.height * 0.5742627);
    path_31.cubicTo(
        size.width * 0.3644482,
        size.height * 0.5510755,
        size.width * 0.3513607,
        size.height * 0.5298784,
        size.width * 0.3408009,
        size.height * 0.5171637);
    path_31.lineTo(size.width * 0.3385705, size.height * 0.5144784);
    path_31.lineTo(size.width * 0.3619759, size.height * 0.4712745);
    path_31.cubicTo(
        size.width * 0.3653393,
        size.height * 0.4654392,
        size.width * 0.3638321,
        size.height * 0.4590275,
        size.width * 0.3600054,
        size.height * 0.4564480);
    path_31.close();
    path_31.moveTo(size.width * 0.3366366, size.height * 0.8544020);
    path_31.cubicTo(
        size.width * 0.3238313,
        size.height * 0.8959608,
        size.width * 0.2876241,
        size.height * 0.9108108,
        size.width * 0.2540884,
        size.height * 0.8969578);
    path_31.cubicTo(
        size.width * 0.2208304,
        size.height * 0.8831049,
        size.width * 0.2011143,
        size.height * 0.8332745,
        size.width * 0.2284768,
        size.height * 0.7552402);
    path_31.cubicTo(
        size.width * 0.2344384,
        size.height * 0.7382490,
        size.width * 0.2397562,
        size.height * 0.7224775,
        size.width * 0.2450696,
        size.height * 0.7067176);
    path_31.cubicTo(
        size.width * 0.2577402,
        size.height * 0.6691373,
        size.width * 0.2703875,
        size.height * 0.6316284,
        size.width * 0.2916777,
        size.height * 0.5778431);
    path_31.cubicTo(
        size.width * 0.2900661,
        size.height * 0.5669480,
        size.width * 0.2844634,
        size.height * 0.5570245,
        size.width * 0.2797661,
        size.height * 0.5487059);
    path_31.cubicTo(
        size.width * 0.2769446,
        size.height * 0.5437078,
        size.width * 0.2744500,
        size.height * 0.5392892,
        size.width * 0.2733437,
        size.height * 0.5355873);
    path_31.cubicTo(
        size.width * 0.2687375,
        size.height * 0.5201392,
        size.width * 0.2668946,
        size.height * 0.5049912,
        size.width * 0.2679080,
        size.height * 0.4945265);
    path_31.lineTo(size.width * 0.2680000, size.height * 0.4941284);
    path_31.cubicTo(
        size.width * 0.2681848,
        size.height * 0.4924333,
        size.width * 0.2683687,
        size.height * 0.4908392,
        size.width * 0.2687375,
        size.height * 0.4894441);
    path_31.lineTo(size.width * 0.2808982, size.height * 0.4155951);
    path_31.cubicTo(
        size.width * 0.2827411,
        size.height * 0.4063265,
        size.width * 0.2884527,
        size.height * 0.4002471,
        size.width * 0.2958232,
        size.height * 0.4010451);
    path_31.cubicTo(
        size.width * 0.3032857,
        size.height * 0.4018422,
        size.width * 0.3085375,
        size.height * 0.4097157,
        size.width * 0.3077080,
        size.height * 0.4185853);
    path_31.lineTo(size.width * 0.3055893, size.height * 0.4509745);
    path_31.lineTo(size.width * 0.3176580, size.height * 0.4211765);
    path_31.cubicTo(
        size.width * 0.3215277,
        size.height * 0.4127049,
        size.width * 0.3283455,
        size.height * 0.4082206,
        size.width * 0.3353473,
        size.height * 0.4109108);
    path_31.cubicTo(
        size.width * 0.3423491,
        size.height * 0.4137020,
        size.width * 0.3457580,
        size.height * 0.4226716,
        size.width * 0.3429938,
        size.height * 0.4310431);
    path_31.lineTo(size.width * 0.3272393, size.height * 0.4848598);
    path_31.lineTo(size.width * 0.3429938, size.height * 0.4580510);
    path_31.cubicTo(
        size.width * 0.3474161,
        size.height * 0.4505765,
        size.width * 0.3560759,
        size.height * 0.4479853,
        size.width * 0.3623411,
        size.height * 0.4522706);
    path_31.cubicTo(
        size.width * 0.3686982,
        size.height * 0.4565559,
        size.width * 0.3701723,
        size.height * 0.4662225,
        size.width * 0.3657500,
        size.height * 0.4738971);
    path_31.lineTo(size.width * 0.3440991, size.height * 0.5138608);
    path_31.cubicTo(
        size.width * 0.3546946,
        size.height * 0.5266176,
        size.width * 0.3698955,
        size.height * 0.5502373,
        size.width * 0.3572741,
        size.height * 0.5765480);
    path_31.cubicTo(
        size.width * 0.3574580,
        size.height * 0.5773451,
        size.width * 0.3576429,
        size.height * 0.5781422,
        size.width * 0.3577348,
        size.height * 0.5789392);
    path_31.cubicTo(
        size.width * 0.3659339,
        size.height * 0.6376402,
        size.width * 0.3601304,
        size.height * 0.7778627,
        size.width * 0.3366366,
        size.height * 0.8544020);
    path_31.close();

    Paint paint_31_fill = Paint()..style = PaintingStyle.fill;
    paint_31_fill.color = Colors.black;
    canvas.drawPath(path_31, paint_31_fill);

    Path path_32 = Path()..fillType = PathFillType.evenOdd;
    path_32.moveTo(size.width * 0.3441938, size.height * 0.4560039);
    path_32.cubicTo(
        size.width * 0.3452312,
        size.height * 0.4567363,
        size.width * 0.3455304,
        size.height * 0.4582529,
        size.width * 0.3448643,
        size.height * 0.4593912);
    path_32.lineTo(size.width * 0.3171330, size.height * 0.5067304);
    path_32.cubicTo(
        size.width * 0.3164661,
        size.height * 0.5078686,
        size.width * 0.3150848,
        size.height * 0.5081980,
        size.width * 0.3140482,
        size.height * 0.5074657);
    path_32.cubicTo(
        size.width * 0.3130116,
        size.height * 0.5067333,
        size.width * 0.3127116,
        size.height * 0.5052167,
        size.width * 0.3133786,
        size.height * 0.5040784);
    path_32.lineTo(size.width * 0.3411089, size.height * 0.4567392);
    path_32.cubicTo(
        size.width * 0.3417759,
        size.height * 0.4556010,
        size.width * 0.3431571,
        size.height * 0.4552716,
        size.width * 0.3441938,
        size.height * 0.4560039);
    path_32.close();

    Paint paint_32_fill = Paint()..style = PaintingStyle.fill;
    paint_32_fill.color = Colors.black;
    canvas.drawPath(path_32, paint_32_fill);

    Path path_33 = Path()..fillType = PathFillType.evenOdd;
    path_33.moveTo(size.width * 0.3185670, size.height * 0.4190529);
    path_33.cubicTo(
        size.width * 0.3196929,
        size.height * 0.4196049,
        size.width * 0.3201991,
        size.height * 0.4210539,
        size.width * 0.3196964,
        size.height * 0.4222902);
    path_33.lineTo(size.width * 0.2946375, size.height * 0.4839804);
    path_33.cubicTo(
        size.width * 0.2941348,
        size.height * 0.4852167,
        size.width * 0.2928152,
        size.height * 0.4857716,
        size.width * 0.2916893,
        size.height * 0.4852196);
    path_33.cubicTo(
        size.width * 0.2905634,
        size.height * 0.4846686,
        size.width * 0.2900580,
        size.height * 0.4832196,
        size.width * 0.2905598,
        size.height * 0.4819833);
    path_33.lineTo(size.width * 0.3156196, size.height * 0.4202931);
    path_33.cubicTo(
        size.width * 0.3161214,
        size.height * 0.4190569,
        size.width * 0.3174411,
        size.height * 0.4185020,
        size.width * 0.3185670,
        size.height * 0.4190529);
    path_33.close();

    Paint paint_33_fill = Paint()..style = PaintingStyle.fill;
    paint_33_fill.color = Colors.black;
    canvas.drawPath(path_33, paint_33_fill);

    Path path_34 = Path()..fillType = PathFillType.evenOdd;
    path_34.moveTo(size.width * 0.2820000, size.height * 0.6026539);
    path_34.cubicTo(
        size.width * 0.2584143,
        size.height * 0.6641451,
        size.width * 0.2463455,
        size.height * 0.7042088,
        size.width * 0.2284723,
        size.height * 0.7551353);
    path_34.cubicTo(
        size.width * 0.2011098,
        size.height * 0.8331696,
        size.width * 0.2208259,
        size.height * 0.8830000,
        size.width * 0.2540848,
        size.height * 0.8968529);
    path_34.cubicTo(
        size.width * 0.2876196,
        size.height * 0.9107059,
        size.width * 0.3238268,
        size.height * 0.8958569,
        size.width * 0.3366321,
        size.height * 0.8542980);
    path_34.cubicTo(
        size.width * 0.3552429,
        size.height * 0.7937039,
        size.width * 0.3627973,
        size.height * 0.6932461,
        size.width * 0.3609545,
        size.height * 0.6248784);
    path_34.cubicTo(
        size.width * 0.3336839,
        size.height * 0.6241804,
        size.width * 0.3047554,
        size.height * 0.6194971,
        size.width * 0.2820000,
        size.height * 0.6026539);
    path_34.close();

    Paint paint_34_fill = Paint()..style = PaintingStyle.fill;
    paint_34_fill.color = Colors.white;
    canvas.drawPath(path_34, paint_34_fill);

    Path path_35 = Path()..fillType = PathFillType.evenOdd;
    path_35.moveTo(size.width * 0.2326384, size.height * 0.7568961);
    path_35.cubicTo(
        size.width * 0.2191830,
        size.height * 0.7952696,
        size.width * 0.2175009,
        size.height * 0.8261578,
        size.width * 0.2228661,
        size.height * 0.8488294);
    path_35.cubicTo(
        size.width * 0.2282098,
        size.height * 0.8714108,
        size.width * 0.2405384,
        size.height * 0.8859667,
        size.width * 0.2556643,
        size.height * 0.8922686);
    path_35.cubicTo(
        size.width * 0.2714429,
        size.height * 0.8987833,
        size.width * 0.2876580,
        size.height * 0.8984784,
        size.width * 0.3013330,
        size.height * 0.8918686);
    path_35.cubicTo(
        size.width * 0.3149643,
        size.height * 0.8852794,
        size.width * 0.3263786,
        size.height * 0.8722804,
        size.width * 0.3324036,
        size.height * 0.8527265);
    path_35.cubicTo(
        size.width * 0.3503607,
        size.height * 0.7942539,
        size.width * 0.3579848,
        size.height * 0.6975627,
        size.width * 0.3566018,
        size.height * 0.6296382);
    path_35.cubicTo(
        size.width * 0.3320170,
        size.height * 0.6286294,
        size.width * 0.3058446,
        size.height * 0.6241157,
        size.width * 0.2840741,
        size.height * 0.6098971);
    path_35.cubicTo(
        size.width * 0.2694884,
        size.height * 0.6482363,
        size.width * 0.2594000,
        size.height * 0.6781794,
        size.width * 0.2492607,
        size.height * 0.7082725);
    path_35.cubicTo(
        size.width * 0.2439518,
        size.height * 0.7240304,
        size.width * 0.2386286,
        size.height * 0.7398294,
        size.width * 0.2326384,
        size.height * 0.7568961);
    path_35.close();
    path_35.moveTo(size.width * 0.2802402, size.height * 0.6072578);
    path_35.cubicTo(
        size.width * 0.2654652,
        size.height * 0.6460520,
        size.width * 0.2552563,
        size.height * 0.6763510,
        size.width * 0.2450313,
        size.height * 0.7066971);
    path_35.cubicTo(
        size.width * 0.2397286,
        size.height * 0.7224333,
        size.width * 0.2344223,
        size.height * 0.7381824,
        size.width * 0.2284723,
        size.height * 0.7551353);
    path_35.cubicTo(
        size.width * 0.2011098,
        size.height * 0.8331696,
        size.width * 0.2208259,
        size.height * 0.8830000,
        size.width * 0.2540848,
        size.height * 0.8968529);
    path_35.cubicTo(
        size.width * 0.2876196,
        size.height * 0.9107059,
        size.width * 0.3238268,
        size.height * 0.8958569,
        size.width * 0.3366321,
        size.height * 0.8542980);
    path_35.cubicTo(
        size.width * 0.3552429,
        size.height * 0.7937039,
        size.width * 0.3627973,
        size.height * 0.6932461,
        size.width * 0.3609545,
        size.height * 0.6248784);
    path_35.cubicTo(
        size.width * 0.3351875,
        size.height * 0.6242196,
        size.width * 0.3079402,
        size.height * 0.6200010,
        size.width * 0.2858179,
        size.height * 0.6053294);
    path_35.cubicTo(
        size.width * 0.2845268,
        size.height * 0.6044735,
        size.width * 0.2832536,
        size.height * 0.6035824,
        size.width * 0.2820000,
        size.height * 0.6026539);
    path_35.cubicTo(
        size.width * 0.2814062,
        size.height * 0.6042020,
        size.width * 0.2808196,
        size.height * 0.6057363,
        size.width * 0.2802402,
        size.height * 0.6072578);
    path_35.close();
    path_35.fillType = PathFillType.evenOdd;
    Paint paint_35_fill = Paint()..style = PaintingStyle.fill;
    paint_35_fill.color = Colors.black;
    canvas.drawPath(path_35, paint_35_fill);

    Path path_36 = Path()..fillType = PathFillType.evenOdd;
    path_36.moveTo(size.width * 0.2813170, size.height * 0.6044873);
    path_36.cubicTo(
        size.width * 0.2824518,
        size.height * 0.6050176,
        size.width * 0.2829795,
        size.height * 0.6064578,
        size.width * 0.2824964,
        size.height * 0.6077029);
    path_36.cubicTo(
        size.width * 0.2657830,
        size.height * 0.6507961,
        size.width * 0.2567768,
        size.height * 0.6779971,
        size.width * 0.2473580,
        size.height * 0.7064422);
    path_36.cubicTo(
        size.width * 0.2422991,
        size.height * 0.7217235,
        size.width * 0.2371205,
        size.height * 0.7373637,
        size.width * 0.2305643,
        size.height * 0.7560206);
    path_36.lineTo(size.width * 0.2305625, size.height * 0.7560235);
    path_36.cubicTo(
        size.width * 0.2255420,
        size.height * 0.7702431,
        size.width * 0.2221750,
        size.height * 0.7834451,
        size.width * 0.2201759,
        size.height * 0.7957324);
    path_36.cubicTo(
        size.width * 0.2199589,
        size.height * 0.7970647,
        size.width * 0.2188000,
        size.height * 0.7979520,
        size.width * 0.2175866,
        size.height * 0.7977137);
    path_36.cubicTo(
        size.width * 0.2163723,
        size.height * 0.7974765,
        size.width * 0.2155643,
        size.height * 0.7962029,
        size.width * 0.2157813,
        size.height * 0.7948706);
    path_36.cubicTo(
        size.width * 0.2178357,
        size.height * 0.7822422,
        size.width * 0.2212866,
        size.height * 0.7687363,
        size.width * 0.2264000,
        size.height * 0.7542529);
    path_36.cubicTo(
        size.width * 0.2328973,
        size.height * 0.7357637,
        size.width * 0.2380571,
        size.height * 0.7201824,
        size.width * 0.2431107,
        size.height * 0.7049206);
    path_36.cubicTo(
        size.width * 0.2525580,
        size.height * 0.6763912,
        size.width * 0.2616348,
        size.height * 0.6489794,
        size.width * 0.2783893,
        size.height * 0.6057824);
    path_36.cubicTo(
        size.width * 0.2788723,
        size.height * 0.6045373,
        size.width * 0.2801830,
        size.height * 0.6039578,
        size.width * 0.2813170,
        size.height * 0.6044873);
    path_36.close();

    Paint paint_36_fill = Paint()..style = PaintingStyle.fill;
    paint_36_fill.color = Colors.black;
    canvas.drawPath(path_36, paint_36_fill);

    Path path_37 = Path()..fillType = PathFillType.evenOdd;
    path_37.moveTo(size.width * 0.5538768, size.height * 0.6657157);
    path_37.cubicTo(
        size.width * 0.4844107,
        size.height * 0.6104039,
        size.width * 0.3927420,
        size.height * 0.5902716,
        size.width * 0.2656955,
        size.height * 0.6027294);
    path_37.cubicTo(
        size.width * 0.2646821,
        size.height * 0.6028294,
        size.width * 0.2636688,
        size.height * 0.6034275,
        size.width * 0.2631161,
        size.height * 0.6043245);
    path_37.cubicTo(
        size.width * 0.2624705,
        size.height * 0.6052216,
        size.width * 0.2622866,
        size.height * 0.6064167,
        size.width * 0.2624705,
        size.height * 0.6075137);
    path_37.lineTo(size.width * 0.3235527, size.height * 0.9424735);
    path_37.lineTo(size.width * 0.3311071, size.height * 0.9424735);
    path_37.lineTo(size.width * 0.2703937, size.height * 0.6103039);
    path_37.cubicTo(
        size.width * 0.2718929,
        size.height * 0.6101657,
        size.width * 0.2733857,
        size.height * 0.6100324,
        size.width * 0.2748741,
        size.height * 0.6099029);
    path_37.cubicTo(
        size.width * 0.3957312,
        size.height * 0.5994275,
        size.width * 0.4832955,
        size.height * 0.6193284,
        size.width * 0.5498232,
        size.height * 0.6723922);
    path_37.cubicTo(
        size.width * 0.5499152,
        size.height * 0.6724422,
        size.width * 0.5499839,
        size.height * 0.6724922,
        size.width * 0.5500536,
        size.height * 0.6725422);
    path_37.cubicTo(
        size.width * 0.5501223,
        size.height * 0.6725922,
        size.width * 0.5501920,
        size.height * 0.6726422,
        size.width * 0.5502839,
        size.height * 0.6726912);
    path_37.cubicTo(
        size.width * 0.5537848,
        size.height * 0.6747843,
        size.width * 0.5576545,
        size.height * 0.6759804,
        size.width * 0.5618920,
        size.height * 0.6707980);
    path_37.cubicTo(
        size.width * 0.5982830,
        size.height * 0.6290402,
        size.width * 0.6379911,
        size.height * 0.5960520,
        size.width * 0.6800018,
        size.height * 0.5727314);
    path_37.cubicTo(
        size.width * 0.7185750,
        size.height * 0.5512422,
        size.width * 0.7601616,
        size.height * 0.5373931,
        size.width * 0.8035982,
        size.height * 0.5314559);
    path_37.cubicTo(
        size.width * 0.8050839,
        size.height * 0.5312529,
        size.width * 0.8065723,
        size.height * 0.5310588,
        size.width * 0.8080625,
        size.height * 0.5308745);
    path_37.lineTo(size.width * 0.8080625, size.height * 0.8801853);
    path_37.cubicTo(
        size.width * 0.7670643,
        size.height * 0.8899990,
        size.width * 0.7248268,
        size.height * 0.9090147,
        size.width * 0.6802563,
        size.height * 0.9375716);
    path_37.cubicTo(
        size.width * 0.6777527,
        size.height * 0.9391755,
        size.width * 0.6752420,
        size.height * 0.9408098,
        size.width * 0.6727241,
        size.height * 0.9424735);
    path_37.lineTo(size.width * 0.6870045, size.height * 0.9424735);
    path_37.cubicTo(
        size.width * 0.7307661,
        size.height * 0.9147676,
        size.width * 0.7723161,
        size.height * 0.8965304,
        size.width * 0.8124848,
        size.height * 0.8872618);
    path_37.cubicTo(
        size.width * 0.8141429,
        size.height * 0.8868627,
        size.width * 0.8154330,
        size.height * 0.8852686,
        size.width * 0.8154330,
        size.height * 0.8833745);
    path_37.lineTo(size.width * 0.8154330, size.height * 0.5263892);
    path_37.cubicTo(
        size.width * 0.8154330,
        size.height * 0.5252931,
        size.width * 0.8149723,
        size.height * 0.5241971,
        size.width * 0.8142348,
        size.height * 0.5233990);
    path_37.cubicTo(
        size.width * 0.8134982,
        size.height * 0.5226020,
        size.width * 0.8123929,
        size.height * 0.5223029,
        size.width * 0.8113795,
        size.height * 0.5224029);
    path_37.cubicTo(
        size.width * 0.7638402,
        size.height * 0.5277843,
        size.width * 0.7185125,
        size.height * 0.5422353,
        size.width * 0.6765937,
        size.height * 0.5655559);
    path_37.cubicTo(
        size.width * 0.6338455,
        size.height * 0.5892755,
        size.width * 0.5934929,
        size.height * 0.6228608,
        size.width * 0.5564562,
        size.height * 0.6653167);
    path_37.lineTo(size.width * 0.5563643, size.height * 0.6654167);
    path_37.cubicTo(
        size.width * 0.5558116,
        size.height * 0.6661137,
        size.width * 0.5555348,
        size.height * 0.6663137,
        size.width * 0.5553509,
        size.height * 0.6664127);
    path_37.cubicTo(
        size.width * 0.5551670,
        size.height * 0.6664127,
        size.width * 0.5547982,
        size.height * 0.6663137,
        size.width * 0.5538768,
        size.height * 0.6657157);
    path_37.close();

    Paint paint_37_fill = Paint()..style = PaintingStyle.fill;
    paint_37_fill.color = Colors.black;
    canvas.drawPath(path_37, paint_37_fill);

    Path path_38 = Path()..fillType = PathFillType.evenOdd;
    path_38.moveTo(size.width * 0.6797330, size.height * 0.9424902);
    path_38.cubicTo(
        size.width * 0.7234018,
        size.height * 0.9141863,
        size.width * 0.7673482,
        size.height * 0.8935569,
        size.width * 0.8116625,
        size.height * 0.8833912);
    path_38.lineTo(size.width * 0.8116625, size.height * 0.5264059);
    path_38.cubicTo(
        size.width * 0.7180589,
        size.height * 0.5369696,
        size.width * 0.6334839,
        size.height * 0.5827137,
        size.width * 0.5590429,
        size.height * 0.6681235);
    path_38.cubicTo(
        size.width * 0.5565554,
        size.height * 0.6711127,
        size.width * 0.5548973,
        size.height * 0.6709137,
        size.width * 0.5518571,
        size.height * 0.6691196);
    path_38.cubicTo(
        size.width * 0.4741920,
        size.height * 0.6071304,
        size.width * 0.3746920,
        size.height * 0.5960686,
        size.width * 0.2658866,
        size.height * 0.6066324);
    path_38.lineTo(size.width * 0.3273366, size.height * 0.9424902);
    path_38.lineTo(size.width * 0.6797330, size.height * 0.9424902);
    path_38.close();

    Paint paint_38_fill = Paint()..style = PaintingStyle.fill;
    paint_38_fill.color = Colors.white;
    canvas.drawPath(path_38, paint_38_fill);

    Path path_39 = Path()..fillType = PathFillType.evenOdd;
    path_39.moveTo(size.width * 0.5494768, size.height * 0.6732804);
    path_39.lineTo(size.width * 0.5492321, size.height * 0.6730853);
    path_39.cubicTo(
        size.width * 0.4740446,
        size.height * 0.6130735,
        size.width * 0.3777098,
        size.height * 0.6013402,
        size.width * 0.2712482,
        size.height * 0.6110480);
    path_39.lineTo(size.width * 0.3309938, size.height * 0.9375882);
    path_39.lineTo(size.width * 0.6785161, size.height * 0.9375882);
    path_39.cubicTo(
        size.width * 0.7209804,
        size.height * 0.9101873,
        size.width * 0.7638536,
        size.height * 0.8899147,
        size.width * 0.8071982,
        size.height * 0.8794294);
    path_39.lineTo(size.width * 0.8071982, size.height * 0.5318706);
    path_39.cubicTo(
        size.width * 0.7167000,
        size.height * 0.5433324,
        size.width * 0.6347527,
        size.height * 0.5883637,
        size.width * 0.5623018,
        size.height * 0.6714735);
    path_39.cubicTo(
        size.width * 0.5607652,
        size.height * 0.6733108,
        size.width * 0.5587643,
        size.height * 0.6750539,
        size.width * 0.5560223,
        size.height * 0.6753010);
    path_39.cubicTo(
        size.width * 0.5534991,
        size.height * 0.6755284,
        size.width * 0.5512982,
        size.height * 0.6743549,
        size.width * 0.5497437,
        size.height * 0.6734382);
    path_39.lineTo(size.width * 0.5494768, size.height * 0.6732804);
    path_39.close();
    path_39.moveTo(size.width * 0.5590429, size.height * 0.6681235);
    path_39.cubicTo(
        size.width * 0.6322982,
        size.height * 0.5840735,
        size.width * 0.7153679,
        size.height * 0.5384363,
        size.width * 0.8071982,
        size.height * 0.5269373);
    path_39.cubicTo(
        size.width * 0.8086839,
        size.height * 0.5267510,
        size.width * 0.8101723,
        size.height * 0.5265735,
        size.width * 0.8116625,
        size.height * 0.5264059);
    path_39.lineTo(size.width * 0.8116625, size.height * 0.8833912);
    path_39.cubicTo(
        size.width * 0.7673482,
        size.height * 0.8935569,
        size.width * 0.7234018,
        size.height * 0.9141863,
        size.width * 0.6797330,
        size.height * 0.9424902);
    path_39.lineTo(size.width * 0.3273366, size.height * 0.9424902);
    path_39.lineTo(size.width * 0.2658866, size.height * 0.6066324);
    path_39.cubicTo(
        size.width * 0.2673804,
        size.height * 0.6064873,
        size.width * 0.2688723,
        size.height * 0.6063461,
        size.width * 0.2703625,
        size.height * 0.6062098);
    path_39.cubicTo(
        size.width * 0.3774196,
        size.height * 0.5963814,
        size.width * 0.4752580,
        size.height * 0.6079824,
        size.width * 0.5518571,
        size.height * 0.6691196);
    path_39.cubicTo(
        size.width * 0.5548973,
        size.height * 0.6709137,
        size.width * 0.5565554,
        size.height * 0.6711127,
        size.width * 0.5590429,
        size.height * 0.6681235);
    path_39.close();

    Paint paint_39_fill = Paint()..style = PaintingStyle.fill;
    paint_39_fill.color = Colors.black;
    canvas.drawPath(path_39, paint_39_fill);

    Path path_40 = Path()..fillType = PathFillType.evenOdd;
    path_40.moveTo(size.width * 0.6104223, size.height * 0.7516725);
    path_40.cubicTo(
        size.width * 0.6100357,
        size.height * 0.7519873,
        size.width * 0.6096187,
        size.height * 0.7523167,
        size.width * 0.6092018,
        size.height * 0.7526471);
    path_40.cubicTo(
        size.width * 0.6079125,
        size.height * 0.7536686,
        size.width * 0.6066223,
        size.height * 0.7546902,
        size.width * 0.6062080,
        size.height * 0.7552382);
    path_40.cubicTo(
        size.width * 0.6060688,
        size.height * 0.7533814,
        size.width * 0.6059232,
        size.height * 0.7514020,
        size.width * 0.6057714,
        size.height * 0.7493216);
    path_40.cubicTo(
        size.width * 0.6047598,
        size.height * 0.7354637,
        size.width * 0.6034902,
        size.height * 0.7171510,
        size.width * 0.6023768,
        size.height * 0.7011098);
    path_40.cubicTo(
        size.width * 0.6009937,
        size.height * 0.6811725,
        size.width * 0.5998545,
        size.height * 0.6647461,
        size.width * 0.5997589,
        size.height * 0.6647461);
    path_40.lineTo(size.width * 0.5995741, size.height * 0.6590657);
    path_40.cubicTo(
        size.width * 0.6078661,
        size.height * 0.6504941,
        size.width * 0.6158813,
        size.height * 0.6431196,
        size.width * 0.6248179,
        size.height * 0.6354451);
    path_40.cubicTo(
        size.width * 0.6514437,
        size.height * 0.6137196,
        size.width * 0.6804643,
        size.height * 0.5960794,
        size.width * 0.7110518,
        size.height * 0.5817284);
    path_40.cubicTo(
        size.width * 0.7341759,
        size.height * 0.5715627,
        size.width * 0.7579455,
        size.height * 0.5633912,
        size.width * 0.7818991,
        size.height * 0.5557167);
    path_40.cubicTo(
        size.width * 0.7836491,
        size.height * 0.5551686,
        size.width * 0.7853768,
        size.height * 0.5546206,
        size.width * 0.7871045,
        size.height * 0.5540725);
    path_40.cubicTo(
        size.width * 0.7873054,
        size.height * 0.5540088,
        size.width * 0.7875071,
        size.height * 0.5539451,
        size.width * 0.7877080,
        size.height * 0.5538804);
    path_40.cubicTo(
        size.width * 0.7892348,
        size.height * 0.5533961,
        size.width * 0.7907634,
        size.height * 0.5529118,
        size.width * 0.7923098,
        size.height * 0.5524284);
    path_40.cubicTo(
        size.width * 0.7921643,
        size.height * 0.5541696,
        size.width * 0.7920268,
        size.height * 0.5559118,
        size.width * 0.7918982,
        size.height * 0.5576559);
    path_40.cubicTo(
        size.width * 0.7899973,
        size.height * 0.5834657,
        size.width * 0.7899437,
        size.height * 0.6095873,
        size.width * 0.7903750,
        size.height * 0.6354451);
    path_40.cubicTo(
        size.width * 0.7904661,
        size.height * 0.6394745,
        size.width * 0.7905562,
        size.height * 0.6434069,
        size.width * 0.7906473,
        size.height * 0.6473382);
    path_40.lineTo(size.width * 0.7906518, size.height * 0.6475049);
    path_40.cubicTo(
        size.width * 0.7691848,
        size.height * 0.6525873,
        size.width * 0.7479955,
        size.height * 0.6593637,
        size.width * 0.7275429,
        size.height * 0.6682343);
    path_40.cubicTo(
        size.width * 0.6846098,
        size.height * 0.6867706,
        size.width * 0.6433357,
        size.height * 0.7123833,
        size.width * 0.6121964,
        size.height * 0.7500559);
    path_40.cubicTo(
        size.width * 0.6119152,
        size.height * 0.7504265,
        size.width * 0.6112339,
        size.height * 0.7510147,
        size.width * 0.6104223,
        size.height * 0.7516725);
    path_40.close();
    path_40.moveTo(size.width * 0.7259089, size.height * 0.6636725);
    path_40.cubicTo(
        size.width * 0.7454634,
        size.height * 0.6551922,
        size.width * 0.7656580,
        size.height * 0.6486127,
        size.width * 0.7860955,
        size.height * 0.6435853);
    path_40.cubicTo(
        size.width * 0.7860339,
        size.height * 0.6409373,
        size.width * 0.7859732,
        size.height * 0.6382735,
        size.width * 0.7859116,
        size.height * 0.6355667);
    path_40.lineTo(size.width * 0.7859116, size.height * 0.6355353);
    path_40.cubicTo(
        size.width * 0.7854893,
        size.height * 0.6102382,
        size.width * 0.7855268,
        size.height * 0.5845588,
        size.width * 0.7873161,
        size.height * 0.5591078);
    path_40.cubicTo(
        size.width * 0.7859330,
        size.height * 0.5595461,
        size.width * 0.7845420,
        size.height * 0.5599863,
        size.width * 0.7831366,
        size.height * 0.5604265);
    path_40.cubicTo(
        size.width * 0.7592473,
        size.height * 0.5680804,
        size.width * 0.7356616,
        size.height * 0.5761931,
        size.width * 0.7127580,
        size.height * 0.5862578);
    path_40.cubicTo(
        size.width * 0.6824821,
        size.height * 0.6004676,
        size.width * 0.6538062,
        size.height * 0.6179059,
        size.width * 0.6275250,
        size.height * 0.6393441);
    path_40.cubicTo(
        size.width * 0.6192179,
        size.height * 0.6464784,
        size.width * 0.6117464,
        size.height * 0.6533255,
        size.width * 0.6041080,
        size.height * 0.6611206);
    path_40.lineTo(size.width * 0.6042098, size.height * 0.6642520);
    path_40.cubicTo(
        size.width * 0.6042125,
        size.height * 0.6642765,
        size.width * 0.6042152,
        size.height * 0.6642990,
        size.width * 0.6042179,
        size.height * 0.6643167);
    path_40.cubicTo(
        size.width * 0.6042384,
        size.height * 0.6644990,
        size.width * 0.6042589,
        size.height * 0.6647235,
        size.width * 0.6042804,
        size.height * 0.6649598);
    path_40.cubicTo(
        size.width * 0.6043241,
        size.height * 0.6654461,
        size.width * 0.6043795,
        size.height * 0.6661412,
        size.width * 0.6044464,
        size.height * 0.6670069);
    path_40.cubicTo(
        size.width * 0.6045804,
        size.height * 0.6687461,
        size.width * 0.6047634,
        size.height * 0.6712529,
        size.width * 0.6049839,
        size.height * 0.6743402);
    path_40.cubicTo(
        size.width * 0.6054250,
        size.height * 0.6805176,
        size.width * 0.6060187,
        size.height * 0.6890598,
        size.width * 0.6066750,
        size.height * 0.6985196);
    path_40.lineTo(size.width * 0.6068277, size.height * 0.7007284);
    path_40.cubicTo(
        size.width * 0.6078509,
        size.height * 0.7154706,
        size.width * 0.6090054,
        size.height * 0.7321186,
        size.width * 0.6099696,
        size.height * 0.7454647);
    path_40.cubicTo(
        size.width * 0.6416080,
        size.height * 0.7077412,
        size.width * 0.6831125,
        size.height * 0.6821500,
        size.width * 0.7259089,
        size.height * 0.6636725);
    path_40.close();

    Paint paint_40_fill = Paint()..style = PaintingStyle.fill;
    paint_40_fill.color = Colors.black;
    canvas.drawPath(path_40, paint_40_fill);

    Path path_41 = Path()..fillType = PathFillType.evenOdd;
    path_41.moveTo(size.width * 0.4015009, size.height * 0.7377147);
    path_41.cubicTo(
        size.width * 0.3727438,
        size.height * 0.7289598,
        size.width * 0.3433884,
        size.height * 0.7245676,
        size.width * 0.3144812,
        size.height * 0.7267716);
    path_41.cubicTo(
        size.width * 0.3189634,
        size.height * 0.7530941,
        size.width * 0.3234080,
        size.height * 0.7793961,
        size.width * 0.3278509,
        size.height * 0.8056931);
    path_41.cubicTo(
        size.width * 0.3323429,
        size.height * 0.8322755,
        size.width * 0.3368348,
        size.height * 0.8588578,
        size.width * 0.3413643,
        size.height * 0.8854569);
    path_41.cubicTo(
        size.width * 0.3427652,
        size.height * 0.8853020,
        size.width * 0.3441679,
        size.height * 0.8851725,
        size.width * 0.3455955,
        size.height * 0.8850696);
    path_41.cubicTo(
        size.width * 0.3713179,
        size.height * 0.8825373,
        size.width * 0.3969384,
        size.height * 0.8848833,
        size.width * 0.4220857,
        size.height * 0.8892000);
    path_41.cubicTo(
        size.width * 0.4458134,
        size.height * 0.8932578,
        size.width * 0.4681125,
        size.height * 0.9007637,
        size.width * 0.4900812,
        size.height * 0.9113824);
    path_41.cubicTo(
        size.width * 0.4963125,
        size.height * 0.9144078,
        size.width * 0.5037223,
        size.height * 0.9195108,
        size.width * 0.5107268,
        size.height * 0.9245667);
    path_41.cubicTo(
        size.width * 0.5128759,
        size.height * 0.9261176,
        size.width * 0.5149866,
        size.height * 0.9276637,
        size.width * 0.5170330,
        size.height * 0.9291627);
    path_41.cubicTo(
        size.width * 0.5190661,
        size.height * 0.9306520,
        size.width * 0.5210357,
        size.height * 0.9320941,
        size.width * 0.5229170,
        size.height * 0.9334500);
    path_41.lineTo(size.width * 0.5179125, size.height * 0.7946225);
    path_41.cubicTo(
        size.width * 0.4811759,
        size.height * 0.7703706,
        size.width * 0.4426080,
        size.height * 0.7501843,
        size.width * 0.4015009,
        size.height * 0.7377147);
    path_41.close();
    path_41.moveTo(size.width * 0.5222777, size.height * 0.7917902);
    path_41.lineTo(size.width * 0.5277134, size.height * 0.9425775);
    path_41.cubicTo(
        size.width * 0.5262786,
        size.height * 0.9416382,
        size.width * 0.5247429,
        size.height * 0.9405843,
        size.width * 0.5231330,
        size.height * 0.9394490);
    path_41.cubicTo(
        size.width * 0.5204116,
        size.height * 0.9375304,
        size.width * 0.5174750,
        size.height * 0.9353794,
        size.width * 0.5144420,
        size.height * 0.9331578);
    path_41.cubicTo(
        size.width * 0.5055661,
        size.height * 0.9266559,
        size.width * 0.4958607,
        size.height * 0.9195480,
        size.width * 0.4882821,
        size.height * 0.9158686);
    path_41.cubicTo(
        size.width * 0.4666321,
        size.height * 0.9054039,
        size.width * 0.4447054,
        size.height * 0.8980294,
        size.width * 0.4213964,
        size.height * 0.8940431);
    path_41.cubicTo(
        size.width * 0.3964295,
        size.height * 0.8897569,
        size.width * 0.3711857,
        size.height * 0.8874647,
        size.width * 0.3459420,
        size.height * 0.8899569);
    path_41.cubicTo(
        size.width * 0.3431786,
        size.height * 0.8901559,
        size.width * 0.3405062,
        size.height * 0.8904549,
        size.width * 0.3377429,
        size.height * 0.8908539);
    path_41.cubicTo(
        size.width * 0.3329518,
        size.height * 0.8627490,
        size.width * 0.3282071,
        size.height * 0.8346696,
        size.width * 0.3234625,
        size.height * 0.8065902);
    path_41.cubicTo(
        size.width * 0.3189902,
        size.height * 0.7801225,
        size.width * 0.3145179,
        size.height * 0.7536549,
        size.width * 0.3100071,
        size.height * 0.7271657);
    path_41.cubicTo(
        size.width * 0.3097321,
        size.height * 0.7255529,
        size.width * 0.3094571,
        size.height * 0.7239402,
        size.width * 0.3091821,
        size.height * 0.7223265);
    path_41.cubicTo(
        size.width * 0.3106723,
        size.height * 0.7221745,
        size.width * 0.3121634,
        size.height * 0.7220392,
        size.width * 0.3136554,
        size.height * 0.7219216);
    path_41.cubicTo(
        size.width * 0.3433438,
        size.height * 0.7195784,
        size.width * 0.3733946,
        size.height * 0.7240706,
        size.width * 0.4026938,
        size.height * 0.7329902);
    path_41.cubicTo(
        size.width * 0.4450732,
        size.height * 0.7458471,
        size.width * 0.4846893,
        size.height * 0.7667755,
        size.width * 0.5222777,
        size.height * 0.7917902);
    path_41.close();

    Paint paint_41_fill = Paint()..style = PaintingStyle.fill;
    paint_41_fill.color = Colors.black;
    canvas.drawPath(path_41, paint_41_fill);

    Path path_42 = Path()..fillType = PathFillType.evenOdd;
    path_42.moveTo(size.width * 0.8568786, size.height * 0.7882216);
    path_42.cubicTo(
        size.width * 0.8567866,
        size.height * 0.7880225,
        size.width * 0.8566018,
        size.height * 0.7877235,
        size.width * 0.8565098,
        size.height * 0.7875235);
    path_42.cubicTo(
        size.width * 0.8537464,
        size.height * 0.7821422,
        size.width * 0.8589973,
        size.height * 0.7761627,
        size.width * 0.8542071,
        size.height * 0.7651000);
    path_42.cubicTo(
        size.width * 0.8514429,
        size.height * 0.7588216,
        size.width * 0.8572473,
        size.height * 0.7564294,
        size.width * 0.8562339,
        size.height * 0.7473608);
    path_42.cubicTo(
        size.width * 0.8551277,
        size.height * 0.7381912,
        size.width * 0.8501527,
        size.height * 0.7372951,
        size.width * 0.8491393,
        size.height * 0.7323118);
    path_42.cubicTo(
        size.width * 0.8481259,
        size.height * 0.7272284,
        size.width * 0.8512589,
        size.height * 0.7257343,
        size.width * 0.8487714,
        size.height * 0.7158676);
    path_42.cubicTo(
        size.width * 0.8471125,
        size.height * 0.7091902,
        size.width * 0.8373473,
        size.height * 0.7032108,
        size.width * 0.8300687,
        size.height * 0.7039078);
    path_42.lineTo(size.width * 0.8142223, size.height * 0.7056020);
    path_42.lineTo(size.width * 0.8142223, size.height * 0.7054029);
    path_42.lineTo(size.width * 0.8117348, size.height * 0.7058020);
    path_42.lineTo(size.width * 0.7488107, size.height * 0.7170637);
    path_42.cubicTo(
        size.width * 0.7406107,
        size.height * 0.7181598,
        size.width * 0.7348071,
        size.height * 0.7256343,
        size.width * 0.7357277,
        size.height * 0.7336069);
    path_42.cubicTo(
        size.width * 0.7366491,
        size.height * 0.7415804,
        size.width * 0.7441116,
        size.height * 0.7471608,
        size.width * 0.7522196,
        size.height * 0.7459647);
    path_42.lineTo(size.width * 0.8168018, size.height * 0.7387892);
    path_42.lineTo(size.width * 0.7444804, size.height * 0.7514461);
    path_42.cubicTo(
        size.width * 0.7360964,
        size.height * 0.7526422,
        size.width * 0.7302000,
        size.height * 0.7603167,
        size.width * 0.7311214,
        size.height * 0.7686882);
    path_42.cubicTo(
        size.width * 0.7321348,
        size.height * 0.7770598,
        size.width * 0.7396893,
        size.height * 0.7828392,
        size.width * 0.7480732,
        size.height * 0.7816441);
    path_42.lineTo(size.width * 0.8218696, size.height * 0.7708804);
    path_42.lineTo(size.width * 0.8218696, size.height * 0.7711794);
    path_42.lineTo(size.width * 0.7565491, size.height * 0.7832382);
    path_42.cubicTo(
        size.width * 0.7483500,
        size.height * 0.7843343,
        size.width * 0.7425455,
        size.height * 0.7918088,
        size.width * 0.7434670,
        size.height * 0.7997824);
    path_42.cubicTo(
        size.width * 0.7443884,
        size.height * 0.8077549,
        size.width * 0.7518509,
        size.height * 0.8133363,
        size.width * 0.7599580,
        size.height * 0.8121402);
    path_42.lineTo(size.width * 0.7653938, size.height * 0.8115422);
    path_42.cubicTo(
        size.width * 0.7649330,
        size.height * 0.8131363,
        size.width * 0.7646571,
        size.height * 0.8147314,
        size.width * 0.7647491,
        size.height * 0.8165255);
    path_42.cubicTo(
        size.width * 0.7651170,
        size.height * 0.8245980,
        size.width * 0.7708295,
        size.height * 0.8291824,
        size.width * 0.7793973,
        size.height * 0.8322716);
    path_42.cubicTo(
        size.width * 0.7947830,
        size.height * 0.8353608,
        size.width * 0.8286866,
        size.height * 0.8342647,
        size.width * 0.8393741,
        size.height * 0.8303784);
    path_42.cubicTo(
        size.width * 0.8474813,
        size.height * 0.8274882,
        size.width * 0.8527330,
        size.height * 0.8141333,
        size.width * 0.8565098,
        size.height * 0.8061608);
    path_42.cubicTo(
        size.width * 0.8573393,
        size.height * 0.8044657,
        size.width * 0.8578920,
        size.height * 0.8026725,
        size.width * 0.8580759,
        size.height * 0.8007784);
    path_42.cubicTo(
        size.width * 0.8589054,
        size.height * 0.7958951,
        size.width * 0.8587214,
        size.height * 0.7916098,
        size.width * 0.8568786,
        size.height * 0.7882216);
    path_42.close();

    Paint paint_42_fill = Paint()..style = PaintingStyle.fill;
    paint_42_fill.color = Colors.white;
    canvas.drawPath(path_42, paint_42_fill);

    Path path_43 = Path()..fillType = PathFillType.evenOdd;
    path_43.moveTo(size.width * 0.8529313, size.height * 0.7905137);
    path_43.lineTo(size.width * 0.8528813, size.height * 0.7904225);
    path_43.cubicTo(
        size.width * 0.8528500,
        size.height * 0.7903657,
        size.width * 0.8527946,
        size.height * 0.7902657,
        size.width * 0.8527446,
        size.height * 0.7901725);
    path_43.cubicTo(
        size.width * 0.8527054,
        size.height * 0.7900990,
        size.width * 0.8526464,
        size.height * 0.7899882,
        size.width * 0.8525830,
        size.height * 0.7898569);
    path_43.cubicTo(
        size.width * 0.8514080,
        size.height * 0.7875294,
        size.width * 0.8511554,
        size.height * 0.7852216,
        size.width * 0.8511946,
        size.height * 0.7832010);
    path_43.cubicTo(
        size.width * 0.8512205,
        size.height * 0.7818569,
        size.width * 0.8514018,
        size.height * 0.7803186,
        size.width * 0.8515509,
        size.height * 0.7790461);
    path_43.cubicTo(
        size.width * 0.8516009,
        size.height * 0.7786255,
        size.width * 0.8516473,
        size.height * 0.7782333,
        size.width * 0.8516830,
        size.height * 0.7778873);
    path_43.cubicTo(
        size.width * 0.8519973,
        size.height * 0.7748539,
        size.width * 0.8520830,
        size.height * 0.7716157,
        size.width * 0.8501804,
        size.height * 0.7672167);
    path_43.cubicTo(
        size.width * 0.8491420,
        size.height * 0.7648500,
        size.width * 0.8488196,
        size.height * 0.7625843,
        size.width * 0.8490687,
        size.height * 0.7603578);
    path_43.cubicTo(
        size.width * 0.8492938,
        size.height * 0.7583441,
        size.width * 0.8499777,
        size.height * 0.7565863,
        size.width * 0.8504455,
        size.height * 0.7553824);
    path_43.lineTo(size.width * 0.8505187, size.height * 0.7551961);
    path_43.cubicTo(
        size.width * 0.8514455,
        size.height * 0.7528157,
        size.width * 0.8521411,
        size.height * 0.7510284,
        size.width * 0.8518054,
        size.height * 0.7479804);
    path_43.cubicTo(
        size.width * 0.8513893,
        size.height * 0.7445618,
        size.width * 0.8503429,
        size.height * 0.7428843,
        size.width * 0.8492188,
        size.height * 0.7413990);
    path_43.cubicTo(
        size.width * 0.8490723,
        size.height * 0.7412059,
        size.width * 0.8488893,
        size.height * 0.7409814,
        size.width * 0.8486821,
        size.height * 0.7407265);
    path_43.cubicTo(
        size.width * 0.8474884,
        size.height * 0.7392569,
        size.width * 0.8454741,
        size.height * 0.7367784,
        size.width * 0.8447830,
        size.height * 0.7333804);
    path_43.lineTo(size.width * 0.8447786, size.height * 0.7333598);
    path_43.cubicTo(
        size.width * 0.8441509,
        size.height * 0.7302108,
        size.width * 0.8446420,
        size.height * 0.7275882,
        size.width * 0.8449562,
        size.height * 0.7259118);
    path_43.cubicTo(
        size.width * 0.8450152,
        size.height * 0.7255951,
        size.width * 0.8450688,
        size.height * 0.7253118,
        size.width * 0.8451062,
        size.height * 0.7250657);
    path_43.cubicTo(
        size.width * 0.8453446,
        size.height * 0.7235176,
        size.width * 0.8455348,
        size.height * 0.7214029,
        size.width * 0.8444687,
        size.height * 0.7171755);
    path_43.lineTo(size.width * 0.8444643, size.height * 0.7171569);
    path_43.cubicTo(
        size.width * 0.8440839,
        size.height * 0.7156255,
        size.width * 0.8424080,
        size.height * 0.7133294,
        size.width * 0.8393054,
        size.height * 0.7113735);
    path_43.cubicTo(
        size.width * 0.8363161,
        size.height * 0.7094902,
        size.width * 0.8329732,
        size.height * 0.7085569,
        size.width * 0.8304768,
        size.height * 0.7087892);
    path_43.lineTo(size.width * 0.8113866, size.height * 0.7108304);
    path_43.lineTo(size.width * 0.7494393, size.height * 0.7219176);
    path_43.lineTo(size.width * 0.7493500, size.height * 0.7219294);
    path_43.cubicTo(
        size.width * 0.7431732,
        size.height * 0.7227549,
        size.width * 0.7395991,
        size.height * 0.7281657,
        size.width * 0.7401571,
        size.height * 0.7329902);
    path_43.cubicTo(
        size.width * 0.7407071,
        size.height * 0.7377539,
        size.width * 0.7454839,
        size.height * 0.7420127,
        size.width * 0.7516250,
        size.height * 0.7411069);
    path_43.lineTo(size.width * 0.7516973, size.height * 0.7410961);
    path_43.lineTo(size.width * 0.8163527, size.height * 0.7339127);
    path_43.lineTo(size.width * 0.8175045, size.height * 0.7436304);
    path_43.lineTo(size.width * 0.7451196, size.height * 0.7562980);
    path_43.lineTo(size.width * 0.7450554, size.height * 0.7563078);
    path_43.cubicTo(
        size.width * 0.7387473,
        size.height * 0.7572078,
        size.width * 0.7349821,
        size.height * 0.7627627,
        size.width * 0.7355500,
        size.height * 0.7680706);
    path_43.cubicTo(
        size.width * 0.7362036,
        size.height * 0.7733343,
        size.width * 0.7411661,
        size.height * 0.7776824,
        size.width * 0.7474929,
        size.height * 0.7767833);
    path_43.cubicTo(
        size.width * 0.7474946,
        size.height * 0.7767833,
        size.width * 0.7474902,
        size.height * 0.7767833,
        size.width * 0.7474929,
        size.height * 0.7767833);
    path_43.lineTo(size.width * 0.8263339, size.height * 0.7652843);
    path_43.lineTo(size.width * 0.8263339, size.height * 0.7753255);
    path_43.lineTo(size.width * 0.7571893, size.height * 0.7880912);
    path_43.lineTo(size.width * 0.7570893, size.height * 0.7881039);
    path_43.cubicTo(
        size.width * 0.7509116,
        size.height * 0.7889304,
        size.width * 0.7473384,
        size.height * 0.7943402,
        size.width * 0.7478955,
        size.height * 0.7991647);
    path_43.cubicTo(
        size.width * 0.7484464,
        size.height * 0.8039294,
        size.width * 0.7532223,
        size.height * 0.8081873,
        size.width * 0.7593634,
        size.height * 0.8072814);
    path_43.lineTo(size.width * 0.7594384, size.height * 0.8072706);
    path_43.lineTo(size.width * 0.7717009, size.height * 0.8059216);
    path_43.lineTo(size.width * 0.7696491, size.height * 0.8130245);
    path_43.cubicTo(
        size.width * 0.7692991,
        size.height * 0.8142363,
        size.width * 0.7691527,
        size.height * 0.8152167,
        size.width * 0.7692062,
        size.height * 0.8162490);
    path_43.lineTo(size.width * 0.7692080, size.height * 0.8162794);
    path_43.cubicTo(
        size.width * 0.7694241,
        size.height * 0.8210324,
        size.width * 0.7725045,
        size.height * 0.8245686,
        size.width * 0.7805000,
        size.height * 0.8275088);
    path_43.cubicTo(
        size.width * 0.7877571,
        size.height * 0.8289147,
        size.width * 0.7995411,
        size.height * 0.8294069,
        size.width * 0.8110179,
        size.height * 0.8290412);
    path_43.cubicTo(
        size.width * 0.8227857,
        size.height * 0.8286667,
        size.width * 0.8333223,
        size.height * 0.8274147,
        size.width * 0.8379705,
        size.height * 0.8257245);
    path_43.lineTo(size.width * 0.8379955, size.height * 0.8257157);
    path_43.cubicTo(
        size.width * 0.8405152,
        size.height * 0.8248167,
        size.width * 0.8431402,
        size.height * 0.8220059,
        size.width * 0.8458304,
        size.height * 0.8174912);
    path_43.cubicTo(
        size.width * 0.8478321,
        size.height * 0.8141314,
        size.width * 0.8495054,
        size.height * 0.8104892,
        size.width * 0.8510786,
        size.height * 0.8070667);
    path_43.cubicTo(
        size.width * 0.8515768,
        size.height * 0.8059833,
        size.width * 0.8520643,
        size.height * 0.8049216,
        size.width * 0.8525500,
        size.height * 0.8038980);
    path_43.lineTo(size.width * 0.8525768, size.height * 0.8038402);
    path_43.cubicTo(
        size.width * 0.8531741,
        size.height * 0.8026206,
        size.width * 0.8535223,
        size.height * 0.8014333,
        size.width * 0.8536375,
        size.height * 0.8002578);
    path_43.lineTo(size.width * 0.8536554, size.height * 0.8000676);
    path_43.lineTo(size.width * 0.8536875, size.height * 0.7998804);
    path_43.cubicTo(
        size.width * 0.8544304,
        size.height * 0.7955039,
        size.width * 0.8540795,
        size.height * 0.7926363,
        size.width * 0.8530455,
        size.height * 0.7907343);
    path_43.lineTo(size.width * 0.8529670, size.height * 0.7905902);
    path_43.lineTo(size.width * 0.8529313, size.height * 0.7905137);
    path_43.close();
    path_43.moveTo(size.width * 0.7793973, size.height * 0.8322716);
    path_43.cubicTo(
        size.width * 0.7708348,
        size.height * 0.8291843,
        size.width * 0.7651241,
        size.height * 0.8246029,
        size.width * 0.7647491,
        size.height * 0.8165392);
    path_43.cubicTo(
        size.width * 0.7647491,
        size.height * 0.8165343,
        size.width * 0.7647491,
        size.height * 0.8165294,
        size.width * 0.7647491,
        size.height * 0.8165255);
    path_43.cubicTo(
        size.width * 0.7646571,
        size.height * 0.8147314,
        size.width * 0.7649330,
        size.height * 0.8131363,
        size.width * 0.7653938,
        size.height * 0.8115422);
    path_43.lineTo(size.width * 0.7599580, size.height * 0.8121402);
    path_43.cubicTo(
        size.width * 0.7518509,
        size.height * 0.8133363,
        size.width * 0.7443884,
        size.height * 0.8077549,
        size.width * 0.7434670,
        size.height * 0.7997824);
    path_43.cubicTo(
        size.width * 0.7428661,
        size.height * 0.7945804,
        size.width * 0.7451277,
        size.height * 0.7895912,
        size.width * 0.7490491,
        size.height * 0.7864461);
    path_43.cubicTo(
        size.width * 0.7511384,
        size.height * 0.7847716,
        size.width * 0.7536991,
        size.height * 0.7836196,
        size.width * 0.7565491,
        size.height * 0.7832382);
    path_43.lineTo(size.width * 0.8218696, size.height * 0.7711794);
    path_43.lineTo(size.width * 0.8218696, size.height * 0.7708804);
    path_43.lineTo(size.width * 0.7480732, size.height * 0.7816441);
    path_43.cubicTo(
        size.width * 0.7478705,
        size.height * 0.7816725,
        size.width * 0.7476687,
        size.height * 0.7816971,
        size.width * 0.7474670,
        size.height * 0.7817186);
    path_43.cubicTo(
        size.width * 0.7393286,
        size.height * 0.7825588,
        size.width * 0.7321107,
        size.height * 0.7768569,
        size.width * 0.7311214,
        size.height * 0.7686882);
    path_43.cubicTo(
        size.width * 0.7302000,
        size.height * 0.7603167,
        size.width * 0.7360964,
        size.height * 0.7526422,
        size.width * 0.7444804,
        size.height * 0.7514461);
    path_43.lineTo(size.width * 0.8168018, size.height * 0.7387892);
    path_43.lineTo(size.width * 0.7522196, size.height * 0.7459647);
    path_43.cubicTo(
        size.width * 0.7506527,
        size.height * 0.7461961,
        size.width * 0.7491098,
        size.height * 0.7461745,
        size.width * 0.7476339,
        size.height * 0.7459304);
    path_43.cubicTo(
        size.width * 0.7414714,
        size.height * 0.7449147,
        size.width * 0.7364714,
        size.height * 0.7400392,
        size.width * 0.7357277,
        size.height * 0.7336069);
    path_43.cubicTo(
        size.width * 0.7348071,
        size.height * 0.7256343,
        size.width * 0.7406107,
        size.height * 0.7181598,
        size.width * 0.7488107,
        size.height * 0.7170637);
    path_43.lineTo(size.width * 0.8117348, size.height * 0.7058020);
    path_43.lineTo(size.width * 0.8142223, size.height * 0.7054029);
    path_43.lineTo(size.width * 0.8142223, size.height * 0.7056020);
    path_43.lineTo(size.width * 0.8300687, size.height * 0.7039078);
    path_43.cubicTo(
        size.width * 0.8373473,
        size.height * 0.7032108,
        size.width * 0.8471125,
        size.height * 0.7091902,
        size.width * 0.8487714,
        size.height * 0.7158676);
    path_43.cubicTo(
        size.width * 0.8502580,
        size.height * 0.7217667,
        size.width * 0.8497366,
        size.height * 0.7246735,
        size.width * 0.8492929,
        size.height * 0.7271431);
    path_43.cubicTo(
        size.width * 0.8489946,
        size.height * 0.7288039,
        size.width * 0.8487321,
        size.height * 0.7302676,
        size.width * 0.8491393,
        size.height * 0.7323118);
    path_43.cubicTo(
        size.width * 0.8495536,
        size.height * 0.7343471,
        size.width * 0.8506286,
        size.height * 0.7357000,
        size.width * 0.8518304,
        size.height * 0.7372147);
    path_43.cubicTo(
        size.width * 0.8535714,
        size.height * 0.7394069,
        size.width * 0.8555795,
        size.height * 0.7419363,
        size.width * 0.8562339,
        size.height * 0.7473608);
    path_43.cubicTo(
        size.width * 0.8567232,
        size.height * 0.7517412,
        size.width * 0.8556223,
        size.height * 0.7545637,
        size.width * 0.8546643,
        size.height * 0.7570196);
    path_43.cubicTo(
        size.width * 0.8536393,
        size.height * 0.7596471,
        size.width * 0.8527777,
        size.height * 0.7618539,
        size.width * 0.8542071,
        size.height * 0.7651000);
    path_43.cubicTo(
        size.width * 0.8569491,
        size.height * 0.7714333,
        size.width * 0.8564009,
        size.height * 0.7761010,
        size.width * 0.8559491,
        size.height * 0.7799431);
    path_43.cubicTo(
        size.width * 0.8556116,
        size.height * 0.7828127,
        size.width * 0.8553286,
        size.height * 0.7852235,
        size.width * 0.8565098,
        size.height * 0.7875235);
    path_43.cubicTo(
        size.width * 0.8565562,
        size.height * 0.7876235,
        size.width * 0.8566250,
        size.height * 0.7877480,
        size.width * 0.8566938,
        size.height * 0.7878725);
    path_43.cubicTo(
        size.width * 0.8567634,
        size.height * 0.7879971,
        size.width * 0.8568321,
        size.height * 0.7881216,
        size.width * 0.8568786,
        size.height * 0.7882216);
    path_43.cubicTo(
        size.width * 0.8587214,
        size.height * 0.7916098,
        size.width * 0.8589054,
        size.height * 0.7958951,
        size.width * 0.8580759,
        size.height * 0.8007784);
    path_43.cubicTo(
        size.width * 0.8578920,
        size.height * 0.8026725,
        size.width * 0.8573393,
        size.height * 0.8044657,
        size.width * 0.8565098,
        size.height * 0.8061608);
    path_43.cubicTo(
        size.width * 0.8560750,
        size.height * 0.8070794,
        size.width * 0.8556196,
        size.height * 0.8080696,
        size.width * 0.8551437,
        size.height * 0.8091069);
    path_43.cubicTo(
        size.width * 0.8514848,
        size.height * 0.8170716,
        size.width * 0.8465473,
        size.height * 0.8278206,
        size.width * 0.8393741,
        size.height * 0.8303784);
    path_43.cubicTo(
        size.width * 0.8286866,
        size.height * 0.8342647,
        size.width * 0.7947830,
        size.height * 0.8353608,
        size.width * 0.7793973,
        size.height * 0.8322716);
    path_43.close();

    Paint paint_43_fill = Paint()..style = PaintingStyle.fill;
    paint_43_fill.color = Colors.black;
    canvas.drawPath(path_43, paint_43_fill);

    Path path_44 = Path()..fillType = PathFillType.evenOdd;
    path_44.moveTo(size.width * 0.7693643, size.height * 0.8141382);
    path_44.cubicTo(
        size.width * 0.7678902,
        size.height * 0.8141382,
        size.width * 0.7665080,
        size.height * 0.8130412,
        size.width * 0.7662321,
        size.height * 0.8113471);
    path_44.cubicTo(
        size.width * 0.7658634,
        size.height * 0.8094539,
        size.width * 0.7669687,
        size.height * 0.8076598,
        size.width * 0.7687196,
        size.height * 0.8072608);
    path_44.lineTo(size.width * 0.8254714, size.height * 0.7944049);
    path_44.cubicTo(
        size.width * 0.8272214,
        size.height * 0.7940059,
        size.width * 0.8288804,
        size.height * 0.7952020,
        size.width * 0.8292482,
        size.height * 0.7970961);
    path_44.cubicTo(
        size.width * 0.8296170,
        size.height * 0.7989892,
        size.width * 0.8285116,
        size.height * 0.8007833,
        size.width * 0.8267607,
        size.height * 0.8011824);
    path_44.lineTo(size.width * 0.7700089, size.height * 0.8140382);
    path_44.cubicTo(
        size.width * 0.7698250,
        size.height * 0.8140382,
        size.width * 0.7695491,
        size.height * 0.8141382,
        size.width * 0.7693643,
        size.height * 0.8141382);
    path_44.close();

    Paint paint_44_fill = Paint()..style = PaintingStyle.fill;
    paint_44_fill.color = Colors.white;
    canvas.drawPath(path_44, paint_44_fill);

    Path path_45 = Path()..fillType = PathFillType.evenOdd;
    path_45.moveTo(size.width * 0.7700089, size.height * 0.8140382);
    path_45.cubicTo(
        size.width * 0.7699170,
        size.height * 0.8140382,
        size.width * 0.7698018,
        size.height * 0.8140627,
        size.width * 0.7696866,
        size.height * 0.8140882);
    path_45.cubicTo(
        size.width * 0.7695714,
        size.height * 0.8141127,
        size.width * 0.7694563,
        size.height * 0.8141382,
        size.width * 0.7693643,
        size.height * 0.8141382);
    path_45.cubicTo(
        size.width * 0.7678902,
        size.height * 0.8141382,
        size.width * 0.7665080,
        size.height * 0.8130412,
        size.width * 0.7662321,
        size.height * 0.8113471);
    path_45.cubicTo(
        size.width * 0.7658634,
        size.height * 0.8094539,
        size.width * 0.7669687,
        size.height * 0.8076598,
        size.width * 0.7687196,
        size.height * 0.8072608);
    path_45.lineTo(size.width * 0.8254714, size.height * 0.7944049);
    path_45.cubicTo(
        size.width * 0.8272214,
        size.height * 0.7940059,
        size.width * 0.8288804,
        size.height * 0.7952020,
        size.width * 0.8292482,
        size.height * 0.7970961);
    path_45.cubicTo(
        size.width * 0.8296170,
        size.height * 0.7989892,
        size.width * 0.8285116,
        size.height * 0.8007833,
        size.width * 0.8267607,
        size.height * 0.8011824);
    path_45.lineTo(size.width * 0.7700089, size.height * 0.8140382);
    path_45.close();

    Paint paint_45_fill = Paint()..style = PaintingStyle.fill;
    paint_45_fill.color = Colors.black;
    canvas.drawPath(path_45, paint_45_fill);

    Path path_46 = Path()..fillType = PathFillType.evenOdd;
    path_46.moveTo(size.width * 0.7547938, size.height * 0.7496549);
    path_46.cubicTo(
        size.width * 0.7531357,
        size.height * 0.7496549,
        size.width * 0.7517536,
        size.height * 0.7483588,
        size.width * 0.7515696,
        size.height * 0.7465657);
    path_46.cubicTo(
        size.width * 0.7513857,
        size.height * 0.7446716,
        size.width * 0.7526750,
        size.height * 0.7429775,
        size.width * 0.7544259,
        size.height * 0.7426784);
    path_46.lineTo(size.width * 0.8169813, size.height * 0.7353039);
    path_46.cubicTo(
        size.width * 0.8187321,
        size.height * 0.7351039,
        size.width * 0.8202982,
        size.height * 0.7364990,
        size.width * 0.8205741,
        size.height * 0.7383931);
    path_46.cubicTo(
        size.width * 0.8207589,
        size.height * 0.7402863,
        size.width * 0.8194688,
        size.height * 0.7419804,
        size.width * 0.8177188,
        size.height * 0.7422794);
    path_46.lineTo(size.width * 0.7551625, size.height * 0.7496549);
    path_46.cubicTo(
        size.width * 0.7549786,
        size.height * 0.7496549,
        size.width * 0.7548866,
        size.height * 0.7496549,
        size.width * 0.7547938,
        size.height * 0.7496549);
    path_46.close();

    Paint paint_46_fill = Paint()..style = PaintingStyle.fill;
    paint_46_fill.color = Colors.white;
    canvas.drawPath(path_46, paint_46_fill);

    Path path_47 = Path()..fillType = PathFillType.evenOdd;
    path_47.moveTo(size.width * 0.7544259, size.height * 0.7426784);
    path_47.lineTo(size.width * 0.8169813, size.height * 0.7353039);
    path_47.cubicTo(
        size.width * 0.8187321,
        size.height * 0.7351039,
        size.width * 0.8202982,
        size.height * 0.7364990,
        size.width * 0.8205741,
        size.height * 0.7383931);
    path_47.cubicTo(
        size.width * 0.8207589,
        size.height * 0.7402863,
        size.width * 0.8194688,
        size.height * 0.7419804,
        size.width * 0.8177188,
        size.height * 0.7422794);
    path_47.lineTo(size.width * 0.7551625, size.height * 0.7496549);
    path_47.lineTo(size.width * 0.7547938, size.height * 0.7496549);
    path_47.cubicTo(
        size.width * 0.7531357,
        size.height * 0.7496549,
        size.width * 0.7517536,
        size.height * 0.7483588,
        size.width * 0.7515696,
        size.height * 0.7465657);
    path_47.cubicTo(
        size.width * 0.7513857,
        size.height * 0.7446716,
        size.width * 0.7526750,
        size.height * 0.7429775,
        size.width * 0.7544259,
        size.height * 0.7426784);
    path_47.close();

    Paint paint_47_fill = Paint()..style = PaintingStyle.fill;
    paint_47_fill.color = Colors.black;
    canvas.drawPath(path_47, paint_47_fill);

    Path path_48 = Path()..fillType = PathFillType.evenOdd;
    path_48.moveTo(size.width * 0.5015205, size.height * 0.7032775);
    path_48.cubicTo(
        size.width * 0.4982875,
        size.height * 0.7016784,
        size.width * 0.4951116,
        size.height * 0.7001078,
        size.width * 0.4919438,
        size.height * 0.6986392);
    path_48.cubicTo(
        size.width * 0.4915375,
        size.height * 0.6984510,
        size.width * 0.4906830,
        size.height * 0.6980824,
        size.width * 0.4898929,
        size.height * 0.6977412);
    path_48.cubicTo(
        size.width * 0.4895420,
        size.height * 0.6975902,
        size.width * 0.4892045,
        size.height * 0.6974441,
        size.width * 0.4889241,
        size.height * 0.6973216);
    path_48.cubicTo(
        size.width * 0.4887313,
        size.height * 0.6972382,
        size.width * 0.4885170,
        size.height * 0.6971441,
        size.width * 0.4883750,
        size.height * 0.6970775);
    path_48.cubicTo(
        size.width * 0.4883455,
        size.height * 0.6970637,
        size.width * 0.4882518,
        size.height * 0.6970196,
        size.width * 0.4881473,
        size.height * 0.6969647);
    path_48.cubicTo(
        size.width * 0.4881473,
        size.height * 0.6969647,
        size.width * 0.4879330,
        size.height * 0.6968539,
        size.width * 0.4877009,
        size.height * 0.6966941);
    path_48.cubicTo(
        size.width * 0.4877009,
        size.height * 0.6966941,
        size.width * 0.4876696,
        size.height * 0.6966725,
        size.width * 0.4876500,
        size.height * 0.6966578);
    path_48.cubicTo(
        size.width * 0.4872277,
        size.height * 0.6964696,
        size.width * 0.4868259,
        size.height * 0.6962951,
        size.width * 0.4864205,
        size.height * 0.6961304);
    path_48.lineTo(size.width * 0.4863080, size.height * 0.6960853);
    path_48.cubicTo(
        size.width * 0.4604580,
        size.height * 0.6847216,
        size.width * 0.4337902,
        size.height * 0.6756314,
        size.width * 0.4065786,
        size.height * 0.6690127);
    path_48.cubicTo(
        size.width * 0.3999920,
        size.height * 0.6674294,
        size.width * 0.3933313,
        size.height * 0.6659490,
        size.width * 0.3866830,
        size.height * 0.6646686);
    path_48.lineTo(size.width * 0.3866366, size.height * 0.6646598);
    path_48.cubicTo(
        size.width * 0.3837071,
        size.height * 0.6640598,
        size.width * 0.3807071,
        size.height * 0.6635353,
        size.width * 0.3776920,
        size.height * 0.6630078);
    path_48.cubicTo(
        size.width * 0.3772750,
        size.height * 0.6629353,
        size.width * 0.3768580,
        size.height * 0.6628618,
        size.width * 0.3764411,
        size.height * 0.6627892);
    path_48.cubicTo(
        size.width * 0.3762277,
        size.height * 0.6627529,
        size.width * 0.3760196,
        size.height * 0.6627167,
        size.width * 0.3758134,
        size.height * 0.6626814);
    path_48.cubicTo(
        size.width * 0.3742804,
        size.height * 0.6624176,
        size.width * 0.3729321,
        size.height * 0.6621863,
        size.width * 0.3715161,
        size.height * 0.6620255);
    path_48.lineTo(size.width * 0.3714357, size.height * 0.6620157);
    path_48.lineTo(size.width * 0.3713545, size.height * 0.6620039);
    path_48.cubicTo(
        size.width * 0.3710089,
        size.height * 0.6619500,
        size.width * 0.3706893,
        size.height * 0.6618971,
        size.width * 0.3703946,
        size.height * 0.6618480);
    path_48.cubicTo(
        size.width * 0.3701080,
        size.height * 0.6618000,
        size.width * 0.3698357,
        size.height * 0.6617549,
        size.width * 0.3695571,
        size.height * 0.6617118);
    path_48.cubicTo(
        size.width * 0.3567518,
        size.height * 0.6600304,
        size.width * 0.3440143,
        size.height * 0.6587549,
        size.width * 0.3312232,
        size.height * 0.6583618);
    path_48.cubicTo(
        size.width * 0.3206750,
        size.height * 0.6580216,
        size.width * 0.3101473,
        size.height * 0.6581225,
        size.width * 0.2996482,
        size.height * 0.6588510);
    path_48.cubicTo(
        size.width * 0.3014509,
        size.height * 0.6691471,
        size.width * 0.3032063,
        size.height * 0.6794392,
        size.width * 0.3049536,
        size.height * 0.6897206);
    path_48.cubicTo(
        size.width * 0.3161866,
        size.height * 0.6886686,
        size.width * 0.3274705,
        size.height * 0.6884951,
        size.width * 0.3386714,
        size.height * 0.6891284);
    path_48.cubicTo(
        size.width * 0.3910705,
        size.height * 0.6921647,
        size.width * 0.4415250,
        size.height * 0.7110775,
        size.width * 0.4877295,
        size.height * 0.7360686);
    path_48.cubicTo(
        size.width * 0.4964732,
        size.height * 0.7407980,
        size.width * 0.5050429,
        size.height * 0.7457618,
        size.width * 0.5135205,
        size.height * 0.7509118);
    path_48.lineTo(size.width * 0.5094795, size.height * 0.7072735);
    path_48.cubicTo(
        size.width * 0.5074277,
        size.height * 0.7061647,
        size.width * 0.5064848,
        size.height * 0.7057049,
        size.width * 0.5045509,
        size.height * 0.7047618);
    path_48.cubicTo(
        size.width * 0.5037571,
        size.height * 0.7043755,
        size.width * 0.5027929,
        size.height * 0.7039049,
        size.width * 0.5015205,
        size.height * 0.7032775);
    path_48.close();
    path_48.moveTo(size.width * 0.5136723, size.height * 0.7040892);
    path_48.lineTo(size.width * 0.5188312, size.height * 0.7598000);
    path_48.cubicTo(
        size.width * 0.5172464,
        size.height * 0.7588127,
        size.width * 0.5156607,
        size.height * 0.7578314,
        size.width * 0.5140714,
        size.height * 0.7568559);
    path_48.cubicTo(
        size.width * 0.5047580,
        size.height * 0.7511431,
        size.width * 0.4953580,
        size.height * 0.7456588,
        size.width * 0.4857571,
        size.height * 0.7404657);
    path_48.cubicTo(
        size.width * 0.4398759,
        size.height * 0.7156500,
        size.width * 0.3900339,
        size.height * 0.6970137,
        size.width * 0.3384420,
        size.height * 0.6940235);
    path_48.cubicTo(
        size.width * 0.3260964,
        size.height * 0.6933265,
        size.width * 0.3136589,
        size.height * 0.6936255,
        size.width * 0.3013134,
        size.height * 0.6950206);
    path_48.cubicTo(
        size.width * 0.2992848,
        size.height * 0.6830814,
        size.width * 0.2972554,
        size.height * 0.6711412,
        size.width * 0.2951634,
        size.height * 0.6592020);
    path_48.cubicTo(
        size.width * 0.2948804,
        size.height * 0.6575873,
        size.width * 0.2945964,
        size.height * 0.6559735,
        size.width * 0.2943116,
        size.height * 0.6543588);
    path_48.cubicTo(
        size.width * 0.2958062,
        size.height * 0.6542265,
        size.width * 0.2973009,
        size.height * 0.6541069,
        size.width * 0.2987955,
        size.height * 0.6540000);
    path_48.cubicTo(
        size.width * 0.3096464,
        size.height * 0.6532235,
        size.width * 0.3204973,
        size.height * 0.6531118,
        size.width * 0.3313473,
        size.height * 0.6534618);
    path_48.cubicTo(
        size.width * 0.3443375,
        size.height * 0.6538608,
        size.width * 0.3572357,
        size.height * 0.6551559,
        size.width * 0.3701339,
        size.height * 0.6568500);
    path_48.cubicTo(
        size.width * 0.3704563,
        size.height * 0.6569000,
        size.width * 0.3707563,
        size.height * 0.6569500,
        size.width * 0.3710554,
        size.height * 0.6570000);
    path_48.cubicTo(
        size.width * 0.3713545,
        size.height * 0.6570500,
        size.width * 0.3716545,
        size.height * 0.6571000,
        size.width * 0.3719768,
        size.height * 0.6571490);
    path_48.cubicTo(
        size.width * 0.3735161,
        size.height * 0.6573245,
        size.width * 0.3749848,
        size.height * 0.6575775,
        size.width * 0.3765071,
        size.height * 0.6578382);
    path_48.cubicTo(
        size.width * 0.3767152,
        size.height * 0.6578745,
        size.width * 0.3769250,
        size.height * 0.6579108,
        size.width * 0.3771357,
        size.height * 0.6579471);
    path_48.cubicTo(
        size.width * 0.3775527,
        size.height * 0.6580196,
        size.width * 0.3779714,
        size.height * 0.6580931,
        size.width * 0.3783911,
        size.height * 0.6581667);
    path_48.cubicTo(
        size.width * 0.3814000,
        size.height * 0.6586922,
        size.width * 0.3844625,
        size.height * 0.6592275,
        size.width * 0.3874545,
        size.height * 0.6598402);
    path_48.cubicTo(
        size.width * 0.3941804,
        size.height * 0.6611353,
        size.width * 0.4009054,
        size.height * 0.6626304,
        size.width * 0.4075384,
        size.height * 0.6642255);
    path_48.cubicTo(
        size.width * 0.4342866,
        size.height * 0.6707304,
        size.width * 0.4605089,
        size.height * 0.6796010,
        size.width * 0.4859518,
        size.height * 0.6906520);
    path_48.cubicTo(
        size.width * 0.4866241,
        size.height * 0.6909431,
        size.width * 0.4872964,
        size.height * 0.6912373,
        size.width * 0.4879679,
        size.height * 0.6915324);
    path_48.cubicTo(
        size.width * 0.4887054,
        size.height * 0.6918314,
        size.width * 0.4894188,
        size.height * 0.6921549,
        size.width * 0.4901330,
        size.height * 0.6924794);
    path_48.cubicTo(
        size.width * 0.4908473,
        size.height * 0.6928029,
        size.width * 0.4915607,
        size.height * 0.6931265,
        size.width * 0.4922982,
        size.height * 0.6934255);
    path_48.cubicTo(
        size.width * 0.4889777,
        size.height * 0.6920794,
        size.width * 0.4899696,
        size.height * 0.6925069,
        size.width * 0.4914420,
        size.height * 0.6931431);
    path_48.cubicTo(
        size.width * 0.4917107,
        size.height * 0.6932588,
        size.width * 0.4919946,
        size.height * 0.6933814,
        size.width * 0.4922723,
        size.height * 0.6935020);
    path_48.cubicTo(
        size.width * 0.4925545,
        size.height * 0.6934382,
        size.width * 0.4923071,
        size.height * 0.6935020,
        size.width * 0.4931295,
        size.height * 0.6943716);
    path_48.cubicTo(
        size.width * 0.4942482,
        size.height * 0.6939725,
        size.width * 0.4942134,
        size.height * 0.6943716,
        size.width * 0.4942134,
        size.height * 0.6943716);
    path_48.cubicTo(
        size.width * 0.4972598,
        size.height * 0.6957941,
        size.width * 0.5003054,
        size.height * 0.6973000,
        size.width * 0.5033518,
        size.height * 0.6988069);
    path_48.cubicTo(
        size.width * 0.5045982,
        size.height * 0.6994216,
        size.width * 0.5055509,
        size.height * 0.6998863,
        size.width * 0.5063437,
        size.height * 0.7002725);
    path_48.cubicTo(
        size.width * 0.5088902,
        size.height * 0.7015147,
        size.width * 0.5098080,
        size.height * 0.7019618,
        size.width * 0.5136723,
        size.height * 0.7040892);
    path_48.close();
    path_48.fillType = PathFillType.evenOdd;
    Paint paint_48_fill = Paint()..style = PaintingStyle.fill;
    paint_48_fill.color = Colors.black;
    canvas.drawPath(path_48, paint_48_fill);

    Path path_49 = Path()..fillType = PathFillType.evenOdd;
    path_49.moveTo(size.width * 0.8871857, size.height * 0.3255549);
    path_49.lineTo(size.width * 0.7573750, size.height * 0.3255549);
    path_49.cubicTo(
        size.width * 0.7573750,
        size.height * 0.3255549,
        size.width * 0.7593098,
        size.height * 0.3350225,
        size.width * 0.7480696,
        size.height * 0.3368167);
    path_49.cubicTo(
        size.width * 0.7477009,
        size.height * 0.3369157,
        size.width * 0.7473330,
        size.height * 0.3369157,
        size.width * 0.7470562,
        size.height * 0.3369157);
    path_49.cubicTo(
        size.width * 0.7383045,
        size.height * 0.3377137,
        size.width * 0.7313946,
        size.height * 0.3289431,
        size.width * 0.7330527,
        size.height * 0.3195745);
    path_49.cubicTo(
        size.width * 0.7375670,
        size.height * 0.2942608,
        size.width * 0.7481616,
        size.height * 0.2653598,
        size.width * 0.7573750,
        size.height * 0.2603765);
    path_49.cubicTo(
        size.width * 0.7676938,
        size.height * 0.2547951,
        size.width * 0.8248134,
        size.height * 0.2458255,
        size.width * 0.8426866,
        size.height * 0.2570873);
    path_49.cubicTo(
        size.width * 0.8655348,
        size.height * 0.2716382,
        size.width * 0.8871857,
        size.height * 0.3255549,
        size.width * 0.8871857,
        size.height * 0.3255549);
    path_49.close();

    Paint paint_49_fill = Paint()..style = PaintingStyle.fill;
    paint_49_fill.color = Colors.white;
    canvas.drawPath(path_49, paint_49_fill);

    Path path_50 = Path()..fillType = PathFillType.evenOdd;
    path_50.moveTo(size.width * 0.8871857, size.height * 0.3255549);
    path_50.cubicTo(
        size.width * 0.8871857,
        size.height * 0.3255549,
        size.width * 0.8864518,
        size.height * 0.3237265,
        size.width * 0.8851107,
        size.height * 0.3206529);
    path_50.cubicTo(
        size.width * 0.8791714,
        size.height * 0.3070333,
        size.width * 0.8613277,
        size.height * 0.2689588,
        size.width * 0.8426866,
        size.height * 0.2570873);
    path_50.cubicTo(
        size.width * 0.8248134,
        size.height * 0.2458255,
        size.width * 0.7676938,
        size.height * 0.2547951,
        size.width * 0.7573750,
        size.height * 0.2603765);
    path_50.cubicTo(
        size.width * 0.7481616,
        size.height * 0.2653598,
        size.width * 0.7375670,
        size.height * 0.2942608,
        size.width * 0.7330527,
        size.height * 0.3195745);
    path_50.cubicTo(
        size.width * 0.7313946,
        size.height * 0.3289431,
        size.width * 0.7383045,
        size.height * 0.3377137,
        size.width * 0.7470562,
        size.height * 0.3369157);
    path_50.cubicTo(
        size.width * 0.7473330,
        size.height * 0.3369157,
        size.width * 0.7477009,
        size.height * 0.3369157,
        size.width * 0.7480696,
        size.height * 0.3368167);
    path_50.cubicTo(
        size.width * 0.7549920,
        size.height * 0.3357118,
        size.width * 0.7569170,
        size.height * 0.3316961,
        size.width * 0.7573750,
        size.height * 0.3287745);
    path_50.cubicTo(
        size.width * 0.7576607,
        size.height * 0.3269520,
        size.width * 0.7573750,
        size.height * 0.3255549,
        size.width * 0.7573750,
        size.height * 0.3255549);
    path_50.lineTo(size.width * 0.8871857, size.height * 0.3255549);
    path_50.close();
    path_50.moveTo(size.width * 0.7374348, size.height * 0.3205127);
    path_50.cubicTo(
        size.width * 0.7363250,
        size.height * 0.3267912,
        size.width * 0.7409777,
        size.height * 0.3325510,
        size.width * 0.7466875,
        size.height * 0.3320304);
    path_50.lineTo(size.width * 0.7468714, size.height * 0.3320137);
    path_50.lineTo(size.width * 0.7470562, size.height * 0.3320137);
    path_50.lineTo(size.width * 0.7471616, size.height * 0.3320137);
    path_50.lineTo(size.width * 0.7472125, size.height * 0.3320000);
    path_50.lineTo(size.width * 0.7474277, size.height * 0.3319657);
    path_50.cubicTo(
        size.width * 0.7516902,
        size.height * 0.3312853,
        size.width * 0.7525223,
        size.height * 0.3294510,
        size.width * 0.7527786,
        size.height * 0.3287304);
    path_50.cubicTo(
        size.width * 0.7529795,
        size.height * 0.3281647,
        size.width * 0.7530402,
        size.height * 0.3275765,
        size.width * 0.7530375,
        size.height * 0.3270961);
    path_50.cubicTo(
        size.width * 0.7530357,
        size.height * 0.3268647,
        size.width * 0.7530196,
        size.height * 0.3266892,
        size.width * 0.7530089,
        size.height * 0.3265951);
    path_50.cubicTo(
        size.width * 0.7530062,
        size.height * 0.3265765,
        size.width * 0.7530045,
        size.height * 0.3265627,
        size.width * 0.7530036,
        size.height * 0.3265520);
    path_50.lineTo(size.width * 0.7517982, size.height * 0.3206529);
    path_50.lineTo(size.width * 0.8801518, size.height * 0.3206529);
    path_50.cubicTo(
        size.width * 0.8799277,
        size.height * 0.3201510,
        size.width * 0.8796929,
        size.height * 0.3196265,
        size.width * 0.8794464,
        size.height * 0.3190814);
    path_50.cubicTo(
        size.width * 0.8770946,
        size.height * 0.3138765,
        size.width * 0.8737277,
        size.height * 0.3067951,
        size.width * 0.8696652,
        size.height * 0.2992794);
    path_50.cubicTo(
        size.width * 0.8655964,
        size.height * 0.2917510,
        size.width * 0.8608705,
        size.height * 0.2838637,
        size.width * 0.8558107,
        size.height * 0.2770108);
    path_50.cubicTo(
        size.width * 0.8507134,
        size.height * 0.2701059,
        size.width * 0.8454821,
        size.height * 0.2645373,
        size.width * 0.8404563,
        size.height * 0.2613343);
    path_50.cubicTo(
        size.width * 0.8368937,
        size.height * 0.2590931,
        size.width * 0.8308187,
        size.height * 0.2575980,
        size.width * 0.8229366,
        size.height * 0.2569539);
    path_50.cubicTo(
        size.width * 0.8152054,
        size.height * 0.2563225,
        size.width * 0.8063080,
        size.height * 0.2565490,
        size.width * 0.7975464,
        size.height * 0.2573029);
    path_50.cubicTo(
        size.width * 0.7887973,
        size.height * 0.2580559,
        size.width * 0.7802884,
        size.height * 0.2593265,
        size.width * 0.7733625,
        size.height * 0.2607431);
    path_50.cubicTo(
        size.width * 0.7662455,
        size.height * 0.2622000,
        size.width * 0.7613080,
        size.height * 0.2637127,
        size.width * 0.7593473,
        size.height * 0.2647735);
    path_50.cubicTo(
        size.width * 0.7581027,
        size.height * 0.2654471,
        size.width * 0.7562482,
        size.height * 0.2673225,
        size.width * 0.7540134,
        size.height * 0.2709275);
    path_50.cubicTo(
        size.width * 0.7518616,
        size.height * 0.2743980,
        size.width * 0.7496598,
        size.height * 0.2789735,
        size.width * 0.7475455,
        size.height * 0.2843078);
    path_50.cubicTo(
        size.width * 0.7433205,
        size.height * 0.2949676,
        size.width * 0.7396348,
        size.height * 0.3081765,
        size.width * 0.7374348,
        size.height * 0.3205127);
    path_50.close();

    Paint paint_50_fill = Paint()..style = PaintingStyle.fill;
    paint_50_fill.color = Colors.black;
    canvas.drawPath(path_50, paint_50_fill);

    Path path_51 = Path()..fillType = PathFillType.evenOdd;
    path_51.moveTo(size.width * 0.8093304, size.height * 0.3300441);
    path_51.cubicTo(
        size.width * 0.8078563,
        size.height * 0.3300441,
        size.width * 0.8062902,
        size.height * 0.3298441,
        size.width * 0.8046313,
        size.height * 0.3294461);
    path_51.cubicTo(
        size.width * 0.7991955,
        size.height * 0.3282500,
        size.width * 0.7920098,
        size.height * 0.3252598,
        size.width * 0.7881402,
        size.height * 0.3178853);
    path_51.lineTo(size.width * 0.7488009, size.height * 0.2423422);
    path_51.lineTo(size.width * 0.7628973, size.height * 0.2320775);
    path_51.lineTo(size.width * 0.8157795, size.height * 0.3006441);
    path_51.cubicTo(
        size.width * 0.8197411,
        size.height * 0.3058265,
        size.width * 0.8214911,
        size.height * 0.3124039,
        size.width * 0.8203857,
        size.height * 0.3186824);
    path_51.cubicTo(
        size.width * 0.8192804,
        size.height * 0.3263569,
        size.width * 0.8155946,
        size.height * 0.3300441,
        size.width * 0.8093304,
        size.height * 0.3300441);
    path_51.close();

    Paint paint_51_fill = Paint()..style = PaintingStyle.fill;
    paint_51_fill.color = activeColor;
    canvas.drawPath(path_51, paint_51_fill);

    Path path_52 = Path()..fillType = PathFillType.evenOdd;
    path_52.moveTo(size.width * 0.8055866, size.height * 0.3246569);
    path_52.lineTo(size.width * 0.8055866, size.height * 0.3246578);
    path_52.cubicTo(
        size.width * 0.8069777,
        size.height * 0.3249922,
        size.width * 0.8082214,
        size.height * 0.3251422,
        size.width * 0.8093304,
        size.height * 0.3251422);
    path_52.cubicTo(
        size.width * 0.8116455,
        size.height * 0.3251422,
        size.width * 0.8129384,
        size.height * 0.3244824,
        size.width * 0.8137527,
        size.height * 0.3236510);
    path_52.cubicTo(
        size.width * 0.8146232,
        size.height * 0.3227627,
        size.width * 0.8155187,
        size.height * 0.3210912,
        size.width * 0.8159768,
        size.height * 0.3179167);
    path_52.lineTo(size.width * 0.8159884, size.height * 0.3178343);
    path_52.lineTo(size.width * 0.8160027, size.height * 0.3177520);
    path_52.cubicTo(
        size.width * 0.8168250,
        size.height * 0.3130843,
        size.width * 0.8155562,
        size.height * 0.3079814,
        size.width * 0.8123661,
        size.height * 0.3038039);
    path_52.cubicTo(
        size.width * 0.8123643,
        size.height * 0.3038010,
        size.width * 0.8123616,
        size.height * 0.3037980,
        size.width * 0.8123598,
        size.height * 0.3037951);
    path_52.lineTo(size.width * 0.7620562, size.height * 0.2385716);
    path_52.lineTo(size.width * 0.7547509, size.height * 0.2438922);
    path_52.lineTo(size.width * 0.7920089, size.height * 0.3154382);
    path_52.cubicTo(
        size.width * 0.7920098,
        size.height * 0.3154402,
        size.width * 0.7920107,
        size.height * 0.3154422,
        size.width * 0.7920125,
        size.height * 0.3154451);
    path_52.cubicTo(
        size.width * 0.7949196,
        size.height * 0.3209804,
        size.width * 0.8005241,
        size.height * 0.3235431,
        size.width * 0.8055089,
        size.height * 0.3246392);
    path_52.lineTo(size.width * 0.8055866, size.height * 0.3246569);
    path_52.close();
    path_52.moveTo(size.width * 0.7488009, size.height * 0.2423422);
    path_52.lineTo(size.width * 0.7881402, size.height * 0.3178853);
    path_52.cubicTo(
        size.width * 0.7920098,
        size.height * 0.3252598,
        size.width * 0.7991955,
        size.height * 0.3282500,
        size.width * 0.8046313,
        size.height * 0.3294461);
    path_52.cubicTo(
        size.width * 0.8062902,
        size.height * 0.3298441,
        size.width * 0.8078563,
        size.height * 0.3300441,
        size.width * 0.8093304,
        size.height * 0.3300441);
    path_52.cubicTo(
        size.width * 0.8155946,
        size.height * 0.3300441,
        size.width * 0.8192804,
        size.height * 0.3263569,
        size.width * 0.8203857,
        size.height * 0.3186824);
    path_52.cubicTo(
        size.width * 0.8214911,
        size.height * 0.3124039,
        size.width * 0.8197411,
        size.height * 0.3058265,
        size.width * 0.8157795,
        size.height * 0.3006441);
    path_52.lineTo(size.width * 0.7628973, size.height * 0.2320775);
    path_52.lineTo(size.width * 0.7488009, size.height * 0.2423422);
    path_52.close();

    Paint paint_52_fill = Paint()..style = PaintingStyle.fill;
    paint_52_fill.color = activeColor;
    canvas.drawPath(path_52, paint_52_fill);

    Path path_53 = Path()..fillType = PathFillType.evenOdd;
    path_53.moveTo(size.width * 0.7635509, size.height * 0.2281902);
    path_53.lineTo(size.width * 0.8180920, size.height * 0.2987500);
    path_53.cubicTo(
        size.width * 0.8225143,
        size.height * 0.3044304,
        size.width * 0.8245411,
        size.height * 0.3120049,
        size.width * 0.8233438,
        size.height * 0.3192804);
    path_53.cubicTo(
        size.width * 0.8218696,
        size.height * 0.3279510,
        size.width * 0.8173545,
        size.height * 0.3330333,
        size.width * 0.8094321,
        size.height * 0.3330333);
    path_53.cubicTo(
        size.width * 0.8078652,
        size.height * 0.3330333,
        size.width * 0.8061152,
        size.height * 0.3328343,
        size.width * 0.8041804,
        size.height * 0.3324353);
    path_53.cubicTo(
        size.width * 0.7965339,
        size.height * 0.3306412,
        size.width * 0.7896241,
        size.height * 0.3266549,
        size.width * 0.7858464,
        size.height * 0.3193794);
    path_53.lineTo(size.width * 0.7452179, size.height * 0.2414451);
    path_53.lineTo(size.width * 0.7635509, size.height * 0.2281902);
    path_53.close();
    path_53.moveTo(size.width * 0.8189330, size.height * 0.3185069);
    path_53.cubicTo(
        size.width * 0.8189393,
        size.height * 0.3184696,
        size.width * 0.8189464,
        size.height * 0.3184324,
        size.width * 0.8189527,
        size.height * 0.3183951);
    path_53.cubicTo(
        size.width * 0.8198920,
        size.height * 0.3126618,
        size.width * 0.8183027,
        size.height * 0.3065647,
        size.width * 0.8146982,
        size.height * 0.3019353);
    path_53.lineTo(size.width * 0.8146884, size.height * 0.3019225);
    path_53.lineTo(size.width * 0.8146223, size.height * 0.3018363);
    path_53.lineTo(size.width * 0.8137625, size.height * 0.3026363);
    path_53.moveTo(size.width * 0.8171589, size.height * 0.2994569);
    path_53.cubicTo(
        size.width * 0.8171839,
        size.height * 0.2994882,
        size.width * 0.8172080,
        size.height * 0.2995196,
        size.width * 0.8172321,
        size.height * 0.2995510);
    path_53.cubicTo(
        size.width * 0.8172330,
        size.height * 0.2995520,
        size.width * 0.8172312,
        size.height * 0.2995500,
        size.width * 0.8172321,
        size.height * 0.2995510);
    path_53.cubicTo(
        size.width * 0.8214652,
        size.height * 0.3050333,
        size.width * 0.8233589,
        size.height * 0.3120990,
        size.width * 0.8222259,
        size.height * 0.3190510);
    path_53.moveTo(size.width * 0.8189330, size.height * 0.3185069);
    path_53.cubicTo(
        size.width * 0.8183063,
        size.height * 0.3220892,
        size.width * 0.8171455,
        size.height * 0.3244078,
        size.width * 0.8157643,
        size.height * 0.3258206);
    path_53.cubicTo(
        size.width * 0.8144295,
        size.height * 0.3271843,
        size.width * 0.8124759,
        size.height * 0.3281314,
        size.width * 0.8094321,
        size.height * 0.3281314);
    path_53.cubicTo(
        size.width * 0.8094009,
        size.height * 0.3281314,
        size.width * 0.8093705,
        size.height * 0.3281314,
        size.width * 0.8093393,
        size.height * 0.3281314);
    path_53.cubicTo(
        size.width * 0.8081411,
        size.height * 0.3281235,
        size.width * 0.8067205,
        size.height * 0.3279686,
        size.width * 0.8050589,
        size.height * 0.3276284);
    path_53.cubicTo(
        size.width * 0.8050491,
        size.height * 0.3276265,
        size.width * 0.8050402,
        size.height * 0.3276245,
        size.width * 0.8050312,
        size.height * 0.3276225);
    path_53.cubicTo(
        size.width * 0.8050241,
        size.height * 0.3276206,
        size.width * 0.8050179,
        size.height * 0.3276196,
        size.width * 0.8050107,
        size.height * 0.3276176);
    path_53.cubicTo(
        size.width * 0.7981643,
        size.height * 0.3259902,
        size.width * 0.7927259,
        size.height * 0.3225971,
        size.width * 0.7898080,
        size.height * 0.3171098);
    path_53.cubicTo(
        size.width * 0.7897804,
        size.height * 0.3170569,
        size.width * 0.7897527,
        size.height * 0.3170049,
        size.width * 0.7897250,
        size.height * 0.3169520);
    path_53.lineTo(size.width * 0.7511759, size.height * 0.2430069);
    path_53.lineTo(size.width * 0.7627054, size.height * 0.2346706);

    Paint paint_53_fill = Paint()..style = PaintingStyle.fill;
    paint_53_fill.color = Colors.black;
    canvas.drawPath(path_53, paint_53_fill);

    Path path_54 = Path()..fillType = PathFillType.evenOdd;
    path_54.moveTo(size.width * 0.7282366, size.height * 0.2465353);
    path_54.cubicTo(
        size.width * 0.7293437,
        size.height * 0.2471304,
        size.width * 0.7298018,
        size.height * 0.2485990,
        size.width * 0.7292598,
        size.height * 0.2498147);
    path_54.lineTo(size.width * 0.7204152, size.height * 0.2696471);
    path_54.cubicTo(
        size.width * 0.7198732,
        size.height * 0.2708627,
        size.width * 0.7185357,
        size.height * 0.2713657,
        size.width * 0.7174286,
        size.height * 0.2707706);
    path_54.cubicTo(
        size.width * 0.7163214,
        size.height * 0.2701755,
        size.width * 0.7158634,
        size.height * 0.2687069,
        size.width * 0.7164054,
        size.height * 0.2674912);
    path_54.lineTo(size.width * 0.7252500, size.height * 0.2476588);
    path_54.cubicTo(
        size.width * 0.7257920,
        size.height * 0.2464431,
        size.width * 0.7271295,
        size.height * 0.2459402,
        size.width * 0.7282366,
        size.height * 0.2465353);
    path_54.close();

    Paint paint_54_fill = Paint()..style = PaintingStyle.fill;
    paint_54_fill.color = Colors.black;
    canvas.drawPath(path_54, paint_54_fill);

    Path path_55 = Path()..fillType = PathFillType.evenOdd;
    path_55.moveTo(size.width * 0.7179920, size.height * 0.2409745);
    path_55.cubicTo(
        size.width * 0.7189348,
        size.height * 0.2418471,
        size.width * 0.7190545,
        size.height * 0.2433931,
        size.width * 0.7182598,
        size.height * 0.2444284);
    path_55.lineTo(size.width * 0.7042563, size.height * 0.2626657);
    path_55.cubicTo(
        size.width * 0.7034616,
        size.height * 0.2637010,
        size.width * 0.7020536,
        size.height * 0.2638324,
        size.width * 0.7011107,
        size.height * 0.2629598);
    path_55.cubicTo(
        size.width * 0.7001679,
        size.height * 0.2620873,
        size.width * 0.7000482,
        size.height * 0.2605412,
        size.width * 0.7008429,
        size.height * 0.2595059);
    path_55.lineTo(size.width * 0.7148464, size.height * 0.2412686);
    path_55.cubicTo(
        size.width * 0.7156411,
        size.height * 0.2402333,
        size.width * 0.7170500,
        size.height * 0.2401020,
        size.width * 0.7179920,
        size.height * 0.2409745);
    path_55.close();

    Paint paint_55_fill = Paint()..style = PaintingStyle.fill;
    paint_55_fill.color = Colors.black;
    canvas.drawPath(path_55, paint_55_fill);

    Path path_56 = Path()..fillType = PathFillType.evenOdd;
    path_56.moveTo(size.width * 0.7065616, size.height * 0.2311147);
    path_56.cubicTo(
        size.width * 0.7073589,
        size.height * 0.2321471,
        size.width * 0.7072429,
        size.height * 0.2336941,
        size.width * 0.7063027,
        size.height * 0.2345696);
    path_56.lineTo(size.width * 0.6904563, size.height * 0.2493186);
    path_56.cubicTo(
        size.width * 0.6895161,
        size.height * 0.2501941,
        size.width * 0.6881071,
        size.height * 0.2500667,
        size.width * 0.6873098,
        size.height * 0.2490343);
    path_56.cubicTo(
        size.width * 0.6865134,
        size.height * 0.2480020,
        size.width * 0.6866295,
        size.height * 0.2464549,
        size.width * 0.6875696,
        size.height * 0.2455794);
    path_56.lineTo(size.width * 0.7034161, size.height * 0.2308304);
    path_56.cubicTo(
        size.width * 0.7043563,
        size.height * 0.2299549,
        size.width * 0.7057643,
        size.height * 0.2300824,
        size.width * 0.7065616,
        size.height * 0.2311147);
    path_56.close();

    Paint paint_56_fill = Paint()..style = PaintingStyle.fill;
    paint_56_fill.color = Colors.black;
    canvas.drawPath(path_56, paint_56_fill);

    Path path_57 = Path()..fillType = PathFillType.evenOdd;
    path_57.moveTo(size.width * 0.6977080, size.height * 0.2191824);
    path_57.cubicTo(
        size.width * 0.6982902,
        size.height * 0.2203755,
        size.width * 0.6978813,
        size.height * 0.2218608,
        size.width * 0.6967946,
        size.height * 0.2225000);
    path_57.lineTo(size.width * 0.6800277, size.height * 0.2323667);
    path_57.cubicTo(
        size.width * 0.6789411,
        size.height * 0.2330059,
        size.width * 0.6775875,
        size.height * 0.2325569,
        size.width * 0.6770054,
        size.height * 0.2313637);
    path_57.cubicTo(
        size.width * 0.6764232,
        size.height * 0.2301706,
        size.width * 0.6768321,
        size.height * 0.2286853,
        size.width * 0.6779188,
        size.height * 0.2280461);
    path_57.lineTo(size.width * 0.6946866, size.height * 0.2181794);
    path_57.cubicTo(
        size.width * 0.6957732,
        size.height * 0.2175402,
        size.width * 0.6971259,
        size.height * 0.2179892,
        size.width * 0.6977080,
        size.height * 0.2191824);
    path_57.close();

    Paint paint_57_fill = Paint()..style = PaintingStyle.fill;
    paint_57_fill.color = Colors.black;
    canvas.drawPath(path_57, paint_57_fill);

    Path path_58 = Path()..fillType = PathFillType.evenOdd;
    path_58.moveTo(size.width * 0.6922812, size.height * 0.2077471);
    path_58.cubicTo(
        size.width * 0.6925482,
        size.height * 0.2090686,
        size.width * 0.6917884,
        size.height * 0.2103775,
        size.width * 0.6905839,
        size.height * 0.2106696);
    path_58.lineTo(size.width * 0.6733562, size.height * 0.2148549);
    path_58.cubicTo(
        size.width * 0.6721527,
        size.height * 0.2151480,
        size.width * 0.6709607,
        size.height * 0.2143137,
        size.width * 0.6706946,
        size.height * 0.2129922);
    path_58.cubicTo(
        size.width * 0.6704277,
        size.height * 0.2116696,
        size.width * 0.6711875,
        size.height * 0.2103618,
        size.width * 0.6723920,
        size.height * 0.2100686);
    path_58.lineTo(size.width * 0.6896196, size.height * 0.2058833);
    path_58.cubicTo(
        size.width * 0.6908232,
        size.height * 0.2055912,
        size.width * 0.6920152,
        size.height * 0.2064255,
        size.width * 0.6922812,
        size.height * 0.2077471);
    path_58.close();

    Paint paint_58_fill = Paint()..style = PaintingStyle.fill;
    paint_58_fill.color = Colors.black;
    canvas.drawPath(path_58, paint_58_fill);

    Path path_59 = Path()..fillType = PathFillType.evenOdd;
    path_59.moveTo(size.width * 0.6837821, size.height * 0.1924784);
    path_59.cubicTo(
        size.width * 0.6838536,
        size.height * 0.1938294,
        size.width * 0.6829152,
        size.height * 0.1949892,
        size.width * 0.6816839,
        size.height * 0.1950686);
    path_59.lineTo(size.width * 0.6677723, size.height * 0.1959657);
    path_59.cubicTo(
        size.width * 0.6665420,
        size.height * 0.1960451,
        size.width * 0.6654857,
        size.height * 0.1950137,
        size.width * 0.6654134,
        size.height * 0.1936627);
    path_59.cubicTo(
        size.width * 0.6653411,
        size.height * 0.1923118,
        size.width * 0.6662804,
        size.height * 0.1911520,
        size.width * 0.6675107,
        size.height * 0.1910725);
    path_59.lineTo(size.width * 0.6814223, size.height * 0.1901755);
    path_59.cubicTo(
        size.width * 0.6826536,
        size.height * 0.1900961,
        size.width * 0.6837098,
        size.height * 0.1911275,
        size.width * 0.6837821,
        size.height * 0.1924784);
    path_59.close();

    Paint paint_59_fill = Paint()..style = PaintingStyle.fill;
    paint_59_fill.color = Colors.black;
    canvas.drawPath(path_59, paint_59_fill);

    Path path_60 = Path()..fillType = PathFillType.evenOdd;
    path_60.moveTo(size.width * 0.6600759, size.height * 0.1731304);
    path_60.cubicTo(
        size.width * 0.6602196,
        size.height * 0.1717863,
        size.width * 0.6613286,
        size.height * 0.1708245,
        size.width * 0.6625527,
        size.height * 0.1709824);
    path_60.lineTo(size.width * 0.6764643, size.height * 0.1727765);
    path_60.cubicTo(
        size.width * 0.6776893,
        size.height * 0.1729343,
        size.width * 0.6785652,
        size.height * 0.1741520,
        size.width * 0.6784214,
        size.height * 0.1754961);
    path_60.cubicTo(
        size.width * 0.6782777,
        size.height * 0.1768412,
        size.width * 0.6771687,
        size.height * 0.1778029,
        size.width * 0.6759437,
        size.height * 0.1776451);
    path_60.lineTo(size.width * 0.6620321, size.height * 0.1758510);
    path_60.cubicTo(
        size.width * 0.6608080,
        size.height * 0.1756931,
        size.width * 0.6599321,
        size.height * 0.1744755,
        size.width * 0.6600759,
        size.height * 0.1731304);
    path_60.close();

    Paint paint_60_fill = Paint()..style = PaintingStyle.fill;
    paint_60_fill.color = Colors.black;
    canvas.drawPath(path_60, paint_60_fill);

    Path path_61 = Path()..fillType = PathFillType.evenOdd;
    path_61.moveTo(size.width * 0.6639705, size.height * 0.1528059);
    path_61.cubicTo(
        size.width * 0.6646009,
        size.height * 0.1516422,
        size.width * 0.6659705,
        size.height * 0.1512598,
        size.width * 0.6670304,
        size.height * 0.1519510);
    path_61.lineTo(size.width * 0.6781777, size.height * 0.1592265);
    path_61.cubicTo(
        size.width * 0.6792375,
        size.height * 0.1599176,
        size.width * 0.6795857,
        size.height * 0.1614216,
        size.width * 0.6789562,
        size.height * 0.1625853);
    path_61.cubicTo(
        size.width * 0.6783259,
        size.height * 0.1637490,
        size.width * 0.6769563,
        size.height * 0.1641314,
        size.width * 0.6758964,
        size.height * 0.1634402);
    path_61.lineTo(size.width * 0.6647491, size.height * 0.1561647);
    path_61.cubicTo(
        size.width * 0.6636893,
        size.height * 0.1554735,
        size.width * 0.6633411,
        size.height * 0.1539696,
        size.width * 0.6639705,
        size.height * 0.1528059);
    path_61.close();

    Paint paint_61_fill = Paint()..style = PaintingStyle.fill;
    paint_61_fill.color = Colors.black;
    canvas.drawPath(path_61, paint_61_fill);

    Path path_62 = Path()..fillType = PathFillType.evenOdd;
    path_62.moveTo(size.width * 0.7452062, size.height * 0.2543196);
    path_62.cubicTo(
        size.width * 0.7277938,
        size.height * 0.2543196,
        size.width * 0.7022741,
        size.height * 0.2413637,
        size.width * 0.6873491,
        size.height * 0.2164490);
    path_62.cubicTo(
        size.width * 0.6746348,
        size.height * 0.1953206,
        size.width * 0.6694759,
        size.height * 0.1721000,
        size.width * 0.6737143,
        size.height * 0.1556559);
    path_62.cubicTo(
        size.width * 0.6760170,
        size.height * 0.1469853,
        size.width * 0.6807161,
        size.height * 0.1408059,
        size.width * 0.6878098,
        size.height * 0.1372186);
    path_62.cubicTo(
        size.width * 0.6903893,
        size.height * 0.1359225,
        size.width * 0.6931536,
        size.height * 0.1352255,
        size.width * 0.6959170,
        size.height * 0.1352255);
    path_62.cubicTo(
        size.width * 0.7090000,
        size.height * 0.1352255,
        size.width * 0.7248455,
        size.height * 0.1500745,
        size.width * 0.7417054,
        size.height * 0.1781794);
    path_62.cubicTo(
        size.width * 0.7525768,
        size.height * 0.1962176,
        size.width * 0.7596705,
        size.height * 0.2115657,
        size.width * 0.7628036,
        size.height * 0.2237245);
    path_62.cubicTo(
        size.width * 0.7662125,
        size.height * 0.2371784,
        size.width * 0.7647384,
        size.height * 0.2462471,
        size.width * 0.7582893,
        size.height * 0.2508324);
    path_62.cubicTo(
        size.width * 0.7550643,
        size.height * 0.2531245,
        size.width * 0.7505500,
        size.height * 0.2543196,
        size.width * 0.7452062,
        size.height * 0.2543196);
    path_62.close();

    Paint paint_62_fill = Paint()..style = PaintingStyle.fill;
    paint_62_fill.color = activeColor;
    canvas.drawPath(path_62, paint_62_fill);

    Path path_63 = Path()..fillType = PathFillType.evenOdd;
    path_63.moveTo(size.width * 0.6910741, size.height * 0.2137461);
    path_63.lineTo(size.width * 0.6910786, size.height * 0.2137549);
    path_63.cubicTo(
        size.width * 0.7052241,
        size.height * 0.2373696,
        size.width * 0.7293786,
        size.height * 0.2494176,
        size.width * 0.7452062,
        size.height * 0.2494176);
    path_63.cubicTo(
        size.width * 0.7500214,
        size.height * 0.2494176,
        size.width * 0.7535964,
        size.height * 0.2483275,
        size.width * 0.7558625,
        size.height * 0.2467167);
    path_63.cubicTo(
        size.width * 0.7596866,
        size.height * 0.2439990,
        size.width * 0.7617911,
        size.height * 0.2380265,
        size.width * 0.7585045,
        size.height * 0.2250480);
    path_63.cubicTo(
        size.width * 0.7555536,
        size.height * 0.2136000,
        size.width * 0.7487473,
        size.height * 0.1987461,
        size.width * 0.7379830,
        size.height * 0.1808843);
    path_63.lineTo(size.width * 0.7379777, size.height * 0.1808755);
    path_63.cubicTo(
        size.width * 0.7296768,
        size.height * 0.1670392,
        size.width * 0.7217661,
        size.height * 0.1567088,
        size.width * 0.7144955,
        size.height * 0.1498902);
    path_63.cubicTo(
        size.width * 0.7071625,
        size.height * 0.1430137,
        size.width * 0.7009357,
        size.height * 0.1401275,
        size.width * 0.6959170,
        size.height * 0.1401275);
    path_63.cubicTo(
        size.width * 0.6938393,
        size.height * 0.1401275,
        size.width * 0.6917080,
        size.height * 0.1406520,
        size.width * 0.6896723,
        size.height * 0.1416735);
    path_63.cubicTo(
        size.width * 0.6838411,
        size.height * 0.1446245,
        size.width * 0.6799723,
        size.height * 0.1496176,
        size.width * 0.6780045,
        size.height * 0.1570098);
    path_63.cubicTo(
        size.width * 0.6742696,
        size.height * 0.1715373,
        size.width * 0.6787393,
        size.height * 0.1932480,
        size.width * 0.6910741,
        size.height * 0.2137461);
    path_63.close();
    path_63.moveTo(size.width * 0.6878098, size.height * 0.1372186);
    path_63.cubicTo(
        size.width * 0.6807161,
        size.height * 0.1408059,
        size.width * 0.6760170,
        size.height * 0.1469853,
        size.width * 0.6737143,
        size.height * 0.1556559);
    path_63.cubicTo(
        size.width * 0.6694759,
        size.height * 0.1721000,
        size.width * 0.6746348,
        size.height * 0.1953206,
        size.width * 0.6873491,
        size.height * 0.2164490);
    path_63.cubicTo(
        size.width * 0.7022741,
        size.height * 0.2413637,
        size.width * 0.7277938,
        size.height * 0.2543196,
        size.width * 0.7452062,
        size.height * 0.2543196);
    path_63.cubicTo(
        size.width * 0.7505500,
        size.height * 0.2543196,
        size.width * 0.7550643,
        size.height * 0.2531245,
        size.width * 0.7582893,
        size.height * 0.2508324);
    path_63.cubicTo(
        size.width * 0.7647384,
        size.height * 0.2462471,
        size.width * 0.7662125,
        size.height * 0.2371784,
        size.width * 0.7628036,
        size.height * 0.2237245);
    path_63.cubicTo(
        size.width * 0.7596705,
        size.height * 0.2115657,
        size.width * 0.7525768,
        size.height * 0.1962176,
        size.width * 0.7417054,
        size.height * 0.1781794);
    path_63.cubicTo(
        size.width * 0.7248455,
        size.height * 0.1500745,
        size.width * 0.7090000,
        size.height * 0.1352255,
        size.width * 0.6959170,
        size.height * 0.1352255);
    path_63.cubicTo(
        size.width * 0.6931536,
        size.height * 0.1352255,
        size.width * 0.6903893,
        size.height * 0.1359225,
        size.width * 0.6878098,
        size.height * 0.1372186);
    path_63.close();

    Paint paint_63_fill = Paint()..style = PaintingStyle.fill;
    paint_63_fill.color = activeColor;
    canvas.drawPath(path_63, paint_63_fill);

    Path path_64 = Path()..fillType = PathFillType.evenOdd;
    path_64.moveTo(size.width * 0.6914679, size.height * 0.1388608);
    path_64.cubicTo(
        size.width * 0.6906223,
        size.height * 0.1391225,
        size.width * 0.6897821,
        size.height * 0.1394696,
        size.width * 0.6889304,
        size.height * 0.1399118);
    path_64.cubicTo(
        size.width * 0.6825732,
        size.height * 0.1432010,
        size.width * 0.6783348,
        size.height * 0.1487814,
        size.width * 0.6763080,
        size.height * 0.1565549);
    path_64.cubicTo(
        size.width * 0.6722545,
        size.height * 0.1721020,
        size.width * 0.6773214,
        size.height * 0.1944265,
        size.width * 0.6895750,
        size.height * 0.2148569);
    path_64.cubicTo(
        size.width * 0.6962080,
        size.height * 0.2258196,
        size.width * 0.7054214,
        size.height * 0.2351882,
        size.width * 0.7163848,
        size.height * 0.2419647);
    path_64.cubicTo(
        size.width * 0.7260580,
        size.height * 0.2479441,
        size.width * 0.7365607,
        size.height * 0.2513333,
        size.width * 0.7451295,
        size.height * 0.2513333);
    path_64.cubicTo(
        size.width * 0.7499196,
        size.height * 0.2513333,
        size.width * 0.7539732,
        size.height * 0.2503363,
        size.width * 0.7567375,
        size.height * 0.2483431);
    path_64.cubicTo(
        size.width * 0.7594089,
        size.height * 0.2464490,
        size.width * 0.7608830,
        size.height * 0.2438578,
        size.width * 0.7615286,
        size.height * 0.2400716);
    path_64.cubicTo(
        size.width * 0.7621732,
        size.height * 0.2359853,
        size.width * 0.7617125,
        size.height * 0.2308029,
        size.width * 0.7601464,
        size.height * 0.2245245);
    path_64.cubicTo(
        size.width * 0.7571062,
        size.height * 0.2125647,
        size.width * 0.7501045,
        size.height * 0.1975157,
        size.width * 0.7394170,
        size.height * 0.1797765);
    path_64.cubicTo(
        size.width * 0.7219902,
        size.height * 0.1508706,
        size.width * 0.7090562,
        size.height * 0.1411941,
        size.width * 0.7003964,
        size.height * 0.1388451);
    path_64.cubicTo(
        size.width * 0.6987554,
        size.height * 0.1384000,
        size.width * 0.6972679,
        size.height * 0.1382176,
        size.width * 0.6959321,
        size.height * 0.1382176);
    path_64.cubicTo(
        size.width * 0.6943652,
        size.height * 0.1382176,
        size.width * 0.6929071,
        size.height * 0.1384157,
        size.width * 0.6914679,
        size.height * 0.1388608);
    path_64.close();
    path_64.moveTo(size.width * 0.6849688, size.height * 0.2181461);
    path_64.cubicTo(
        size.width * 0.6651607,
        size.height * 0.1852578,
        size.width * 0.6628571,
        size.height * 0.1465892,
        size.width * 0.6866268,
        size.height * 0.1345304);
    path_64.cubicTo(
        size.width * 0.6882196,
        size.height * 0.1336951,
        size.width * 0.6898375,
        size.height * 0.1331059,
        size.width * 0.6914679,
        size.height * 0.1327343);
    path_64.cubicTo(
        size.width * 0.6929491,
        size.height * 0.1323961,
        size.width * 0.6944402,
        size.height * 0.1322382,
        size.width * 0.6959321,
        size.height * 0.1322382);
    path_64.cubicTo(
        size.width * 0.6974080,
        size.height * 0.1322382,
        size.width * 0.6988964,
        size.height * 0.1323990,
        size.width * 0.7003964,
        size.height * 0.1327137);
    path_64.cubicTo(
        size.width * 0.7143491,
        size.height * 0.1356422,
        size.width * 0.7292911,
        size.height * 0.1519216,
        size.width * 0.7439313,
        size.height * 0.1764873);
    path_64.cubicTo(
        size.width * 0.7637393,
        size.height * 0.2093755,
        size.width * 0.7759929,
        size.height * 0.2418647,
        size.width * 0.7596857,
        size.height * 0.2533265);
    path_64.cubicTo(
        size.width * 0.7559080,
        size.height * 0.2560167,
        size.width * 0.7508411,
        size.height * 0.2573127,
        size.width * 0.7451295,
        size.height * 0.2573127);
    path_64.cubicTo(
        size.width * 0.7264268,
        size.height * 0.2573127,
        size.width * 0.7001696,
        size.height * 0.2433598,
        size.width * 0.6849688,
        size.height * 0.2181461);
    path_64.close();

    Paint paint_64_fill = Paint()..style = PaintingStyle.fill;
    paint_64_fill.color = Colors.black;
    canvas.drawPath(path_64, paint_64_fill);

    Path path_65 = Path()..fillType = PathFillType.evenOdd;
    path_65.moveTo(size.width * 0.8871813, size.height * 0.3255627);
    path_65.lineTo(size.width * 0.8465518, size.height * 0.3043353);
    path_65.lineTo(size.width * 0.8175313, size.height * 0.2891873);
    path_65.lineTo(size.width * 0.8162411, size.height * 0.2927745);
    path_65.cubicTo(
        size.width * 0.8083179,
        size.height * 0.3009471,
        size.width * 0.7709134,
        size.height * 0.3128069,
        size.width * 0.7593054,
        size.height * 0.3168931);
    path_65.cubicTo(
        size.width * 0.7490786,
        size.height * 0.3204804,
        size.width * 0.7486179,
        size.height * 0.3366255,
        size.width * 0.7577393,
        size.height * 0.3433029);
    path_65.cubicTo(
        size.width * 0.7650170,
        size.height * 0.3485853,
        size.width * 0.7798500,
        size.height * 0.3493824,
        size.width * 0.7953277,
        size.height * 0.3484853);
    path_65.cubicTo(
        size.width * 0.8176232,
        size.height * 0.3517745,
        size.width * 0.8266518,
        size.height * 0.3740980,
        size.width * 0.8506973,
        size.height * 0.3714069);
    path_65.lineTo(size.width * 0.9952500, size.height * 0.5266794);
    path_65.lineTo(size.width * 0.9952500, size.height * 0.3736990);
    path_65.lineTo(size.width * 0.8871813, size.height * 0.3255627);
    path_65.close();

    Paint paint_65_fill = Paint()..style = PaintingStyle.fill;
    paint_65_fill.color = Colors.white;
    canvas.drawPath(path_65, paint_65_fill);

    Path path_66 = Path()..fillType = PathFillType.evenOdd;
    path_66.moveTo(size.width * 0.8853813, size.height * 0.3300520);
    path_66.lineTo(size.width * 0.8198196, size.height * 0.2958088);
    path_66.lineTo(size.width * 0.8192973, size.height * 0.2963480);
    path_66.cubicTo(
        size.width * 0.8166982,
        size.height * 0.2990294,
        size.width * 0.8122179,
        size.height * 0.3015863,
        size.width * 0.8074357,
        size.height * 0.3039020);
    path_66.cubicTo(
        size.width * 0.8024741,
        size.height * 0.3063039,
        size.width * 0.7965643,
        size.height * 0.3087294,
        size.width * 0.7906196,
        size.height * 0.3109971);
    path_66.cubicTo(
        size.width * 0.7819679,
        size.height * 0.3142961,
        size.width * 0.7730009,
        size.height * 0.3173471,
        size.width * 0.7665884,
        size.height * 0.3195284);
    path_66.cubicTo(
        size.width * 0.7642179,
        size.height * 0.3203353,
        size.width * 0.7621955,
        size.height * 0.3210225,
        size.width * 0.7606679,
        size.height * 0.3215608);
    path_66.cubicTo(
        size.width * 0.7576625,
        size.height * 0.3226147,
        size.width * 0.7559018,
        size.height * 0.3254912,
        size.width * 0.7557250,
        size.height * 0.3291951);
    path_66.cubicTo(
        size.width * 0.7555455,
        size.height * 0.3329667,
        size.width * 0.7570964,
        size.height * 0.3369363,
        size.width * 0.7602080,
        size.height * 0.3392186);
    path_66.cubicTo(
        size.width * 0.7629241,
        size.height * 0.3411863,
        size.width * 0.7675616,
        size.height * 0.3426098,
        size.width * 0.7739196,
        size.height * 0.3433353);
    path_66.cubicTo(
        size.width * 0.7801482,
        size.height * 0.3440461,
        size.width * 0.7874688,
        size.height * 0.3440324,
        size.width * 0.7950929,
        size.height * 0.3435902);
    path_66.lineTo(size.width * 0.7955089, size.height * 0.3435657);
    path_66.lineTo(size.width * 0.7959223, size.height * 0.3436265);
    path_66.cubicTo(
        size.width * 0.8068786,
        size.height * 0.3452431,
        size.width * 0.8150179,
        size.height * 0.3510824,
        size.width * 0.8221036,
        size.height * 0.3561657);
    path_66.cubicTo(
        size.width * 0.8230054,
        size.height * 0.3568127,
        size.width * 0.8238911,
        size.height * 0.3574471,
        size.width * 0.8247625,
        size.height * 0.3580598);
    path_66.cubicTo(
        size.width * 0.8325973,
        size.height * 0.3635647,
        size.width * 0.8397786,
        size.height * 0.3677020,
        size.width * 0.8502446,
        size.height * 0.3665304);
    path_66.lineTo(size.width * 0.8523250, size.height * 0.3662980);
    path_66.lineTo(size.width * 0.9907857, size.height * 0.5150265);
    path_66.lineTo(size.width * 0.9907857, size.height * 0.3770010);
    path_66.lineTo(size.width * 0.8853813, size.height * 0.3300520);
    path_66.close();
    path_66.moveTo(size.width * 0.9952500, size.height * 0.3736990);
    path_66.lineTo(size.width * 0.9952500, size.height * 0.5266794);
    path_66.lineTo(size.width * 0.8506973, size.height * 0.3714069);
    path_66.cubicTo(
        size.width * 0.8373223,
        size.height * 0.3729039,
        size.width * 0.8285929,
        size.height * 0.3666608,
        size.width * 0.8196429,
        size.height * 0.3602588);
    path_66.cubicTo(
        size.width * 0.8125027,
        size.height * 0.3551520,
        size.width * 0.8052214,
        size.height * 0.3499451,
        size.width * 0.7953277,
        size.height * 0.3484853);
    path_66.cubicTo(
        size.width * 0.7798500,
        size.height * 0.3493824,
        size.width * 0.7650170,
        size.height * 0.3485853,
        size.width * 0.7577393,
        size.height * 0.3433029);
    path_66.cubicTo(
        size.width * 0.7486179,
        size.height * 0.3366255,
        size.width * 0.7490786,
        size.height * 0.3204804,
        size.width * 0.7593054,
        size.height * 0.3168931);
    path_66.cubicTo(
        size.width * 0.7608893,
        size.height * 0.3163353,
        size.width * 0.7629545,
        size.height * 0.3156324,
        size.width * 0.7653598,
        size.height * 0.3148137);
    path_66.cubicTo(
        size.width * 0.7802580,
        size.height * 0.3097451,
        size.width * 0.8082045,
        size.height * 0.3002382,
        size.width * 0.8157848,
        size.height * 0.2932196);
    path_66.cubicTo(
        size.width * 0.8159464,
        size.height * 0.2930696,
        size.width * 0.8160982,
        size.height * 0.2929216,
        size.width * 0.8162411,
        size.height * 0.2927745);
    path_66.lineTo(size.width * 0.8175313, size.height * 0.2891873);
    path_66.lineTo(size.width * 0.8871813, size.height * 0.3255627);
    path_66.lineTo(size.width * 0.9952500, size.height * 0.3736990);
    path_66.close();

    Paint paint_66_fill = Paint()..style = PaintingStyle.fill;
    paint_66_fill.color = Colors.black;
    canvas.drawPath(path_66, paint_66_fill);

    Path path_67 = Path()..fillType = PathFillType.evenOdd;
    path_67.moveTo(size.width * 0.07102750, size.height * 0.1950461);
    path_67.lineTo(size.width * 0.1994562, size.height * 0.2156755);
    path_67.cubicTo(
        size.width * 0.1994562,
        size.height * 0.2156755,
        size.width * 0.1963241,
        size.height * 0.2247451,
        size.width * 0.2071027,
        size.height * 0.2282333);
    path_67.cubicTo(
        size.width * 0.2074714,
        size.height * 0.2283333,
        size.width * 0.2077482,
        size.height * 0.2284324,
        size.width * 0.2081161,
        size.height * 0.2285324);
    path_67.cubicTo(
        size.width * 0.2166848,
        size.height * 0.2307245,
        size.width * 0.2247000,
        size.height * 0.2231500,
        size.width * 0.2243313,
        size.height * 0.2136824);
    path_67.cubicTo(
        size.width * 0.2233179,
        size.height * 0.1878706,
        size.width * 0.2167768,
        size.height * 0.1576735,
        size.width * 0.2083009,
        size.height * 0.1512951);
    path_67.cubicTo(
        size.width * 0.1989036,
        size.height * 0.1441196,
        size.width * 0.1435339,
        size.height * 0.1261804,
        size.width * 0.1243705,
        size.height * 0.1345520);
    path_67.cubicTo(
        size.width * 0.09967946,
        size.height * 0.1451157,
        size.width * 0.07102750,
        size.height * 0.1950461,
        size.width * 0.07102750,
        size.height * 0.1950461);
    path_67.close();

    Paint paint_67_fill = Paint()..style = PaintingStyle.fill;
    paint_67_fill.color = Colors.white;
    canvas.drawPath(path_67, paint_67_fill);

    Path path_68 = Path()..fillType = PathFillType.evenOdd;
    path_68.moveTo(size.width * 0.2036554, size.height * 0.2173431);
    path_68.lineTo(size.width * 0.2056009, size.height * 0.2117088);
    path_68.lineTo(size.width * 0.07864929, size.height * 0.1913167);
    path_68.cubicTo(
        size.width * 0.07893670,
        size.height * 0.1908569,
        size.width * 0.07923857,
        size.height * 0.1903775,
        size.width * 0.07955429,
        size.height * 0.1898794);
    path_68.cubicTo(
        size.width * 0.08258098,
        size.height * 0.1851049,
        size.width * 0.08686527,
        size.height * 0.1786382,
        size.width * 0.09190000,
        size.height * 0.1718578);
    path_68.cubicTo(
        size.width * 0.09694286,
        size.height * 0.1650667,
        size.width * 0.1026866,
        size.height * 0.1580314,
        size.width * 0.1086277,
        size.height * 0.1520833);
    path_68.cubicTo(
        size.width * 0.1146170,
        size.height * 0.1460853,
        size.width * 0.1205616,
        size.height * 0.1414422,
        size.width * 0.1259911,
        size.height * 0.1391196);
    path_68.lineTo(size.width * 0.1260205, size.height * 0.1391069);
    path_68.cubicTo(
        size.width * 0.1298366,
        size.height * 0.1374402,
        size.width * 0.1360455,
        size.height * 0.1369118,
        size.width * 0.1439357,
        size.height * 0.1375176);
    path_68.cubicTo(
        size.width * 0.1516687,
        size.height * 0.1381108,
        size.width * 0.1604429,
        size.height * 0.1397402,
        size.width * 0.1690107,
        size.height * 0.1418735);
    path_68.cubicTo(
        size.width * 0.1775661,
        size.height * 0.1440039,
        size.width * 0.1858143,
        size.height * 0.1466108,
        size.width * 0.1924714,
        size.height * 0.1491127);
    path_68.cubicTo(
        size.width * 0.1993170,
        size.height * 0.1516853,
        size.width * 0.2039786,
        size.height * 0.1539657,
        size.width * 0.2057518,
        size.height * 0.1553196);
    path_68.lineTo(size.width * 0.2057768, size.height * 0.1553382);
    path_68.cubicTo(
        size.width * 0.2069170,
        size.height * 0.1561971,
        size.width * 0.2084982,
        size.height * 0.1583324,
        size.width * 0.2102205,
        size.height * 0.1622392);
    path_68.cubicTo(
        size.width * 0.2118786,
        size.height * 0.1660020,
        size.width * 0.2134357,
        size.height * 0.1708657,
        size.width * 0.2148018,
        size.height * 0.1764686);
    path_68.cubicTo(
        size.width * 0.2175321,
        size.height * 0.1876667,
        size.width * 0.2193768,
        size.height * 0.2013127,
        size.width * 0.2198714,
        size.height * 0.2138941);
    path_68.cubicTo(
        size.width * 0.2201152,
        size.height * 0.2201971,
        size.width * 0.2147616,
        size.height * 0.2251775,
        size.width * 0.2091580,
        size.height * 0.2237657);
    path_68.cubicTo(
        size.width * 0.2090643,
        size.height * 0.2237392,
        size.width * 0.2089714,
        size.height * 0.2237108,
        size.width * 0.2088187,
        size.height * 0.2236637);
    path_68.lineTo(size.width * 0.2088036, size.height * 0.2236588);
    path_68.cubicTo(
        size.width * 0.2086705,
        size.height * 0.2236186,
        size.width * 0.2084911,
        size.height * 0.2235627,
        size.width * 0.2082830,
        size.height * 0.2235039);
    path_68.cubicTo(
        size.width * 0.2043045,
        size.height * 0.2221941,
        size.width * 0.2037446,
        size.height * 0.2202990,
        size.width * 0.2035884,
        size.height * 0.2195510);
    path_68.cubicTo(
        size.width * 0.2034652,
        size.height * 0.2189588,
        size.width * 0.2034821,
        size.height * 0.2183637,
        size.width * 0.2035491,
        size.height * 0.2178843);
    path_68.cubicTo(
        size.width * 0.2035804,
        size.height * 0.2176539,
        size.width * 0.2036196,
        size.height * 0.2174804,
        size.width * 0.2036437,
        size.height * 0.2173873);
    path_68.cubicTo(
        size.width * 0.2036482,
        size.height * 0.2173686,
        size.width * 0.2036518,
        size.height * 0.2173539,
        size.width * 0.2036554,
        size.height * 0.2173431);
    path_68.close();
    path_68.moveTo(size.width * 0.2081161, size.height * 0.2285324);
    path_68.cubicTo(
        size.width * 0.2079321,
        size.height * 0.2284824,
        size.width * 0.2077705,
        size.height * 0.2284324,
        size.width * 0.2076098,
        size.height * 0.2283824);
    path_68.cubicTo(
        size.width * 0.2074482,
        size.height * 0.2283333,
        size.width * 0.2072875,
        size.height * 0.2282833,
        size.width * 0.2071027,
        size.height * 0.2282333);
    path_68.cubicTo(
        size.width * 0.2004464,
        size.height * 0.2260794,
        size.width * 0.1990955,
        size.height * 0.2217971,
        size.width * 0.1990348,
        size.height * 0.2188363);
    path_68.cubicTo(
        size.width * 0.1989982,
        size.height * 0.2170029,
        size.width * 0.1994562,
        size.height * 0.2156755,
        size.width * 0.1994562,
        size.height * 0.2156755);
    path_68.lineTo(size.width * 0.07102750, size.height * 0.1950461);
    path_68.cubicTo(
        size.width * 0.07102750,
        size.height * 0.1950461,
        size.width * 0.07199839,
        size.height * 0.1933539,
        size.width * 0.07373688,
        size.height * 0.1905275);
    path_68.cubicTo(
        size.width * 0.08144214,
        size.height * 0.1779971,
        size.width * 0.1042250,
        size.height * 0.1431716,
        size.width * 0.1243705,
        size.height * 0.1345520);
    path_68.cubicTo(
        size.width * 0.1435339,
        size.height * 0.1261804,
        size.width * 0.1989036,
        size.height * 0.1441196,
        size.width * 0.2083009,
        size.height * 0.1512951);
    path_68.cubicTo(
        size.width * 0.2167768,
        size.height * 0.1576735,
        size.width * 0.2233179,
        size.height * 0.1878706,
        size.width * 0.2243313,
        size.height * 0.2136824);
    path_68.cubicTo(
        size.width * 0.2247000,
        size.height * 0.2231500,
        size.width * 0.2166848,
        size.height * 0.2307245,
        size.width * 0.2081161,
        size.height * 0.2285324);
    path_68.close();

    Paint paint_68_fill = Paint()..style = PaintingStyle.fill;
    paint_68_fill.color = Colors.black;
    canvas.drawPath(path_68, paint_68_fill);

    Path path_69 = Path()..fillType = PathFillType.evenOdd;
    path_69.moveTo(size.width * 0.2388786, size.height * 0.1704480);
    path_69.lineTo(size.width * 0.2625562, size.height * 0.1373598);
    path_69.lineTo(size.width * 0.3474071, size.height * 0.2113088);
    path_69.cubicTo(
        size.width * 0.3482366,
        size.height * 0.2181853,
        size.width * 0.3466705,
        size.height * 0.2247627,
        size.width * 0.3429848,
        size.height * 0.2300451);
    path_69.cubicTo(
        size.width * 0.3388393,
        size.height * 0.2359245,
        size.width * 0.3322982,
        size.height * 0.2398118,
        size.width * 0.3245589,
        size.height * 0.2410078);
    path_69.lineTo(size.width * 0.2388786, size.height * 0.1704480);
    path_69.close();

    Paint paint_69_fill = Paint()..style = PaintingStyle.fill;
    paint_69_fill.color = activeColor;
    canvas.drawPath(path_69, paint_69_fill);

    Path path_70 = Path()..fillType = PathFillType.evenOdd;
    path_70.moveTo(size.width * 0.2620759, size.height * 0.1331765);
    path_70.lineTo(size.width * 0.2349893, size.height * 0.1710471);
    path_70.lineTo(size.width * 0.3237107, size.height * 0.2441990);
    path_70.cubicTo(
        size.width * 0.3422286,
        size.height * 0.2420059,
        size.width * 0.3525473,
        size.height * 0.2265588,
        size.width * 0.3499679,
        size.height * 0.2098157);
    path_70.lineTo(size.width * 0.2620759, size.height * 0.1331765);
    path_70.close();
    path_70.moveTo(size.width * 0.2629973, size.height * 0.1416471);
    path_70.lineTo(size.width * 0.2427286, size.height * 0.1699510);
    path_70.lineTo(size.width * 0.3251848, size.height * 0.2379196);
    path_70.cubicTo(
        size.width * 0.3318179,
        size.height * 0.2367245,
        size.width * 0.3372536,
        size.height * 0.2333353,
        size.width * 0.3407545,
        size.height * 0.2283529);
    path_70.cubicTo(
        size.width * 0.3437946,
        size.height * 0.2239676,
        size.width * 0.3451768,
        size.height * 0.2185853,
        size.width * 0.3448080,
        size.height * 0.2129049);
    path_70.lineTo(size.width * 0.2629973, size.height * 0.1416471);
    path_70.close();

    Paint paint_70_fill = Paint()..style = PaintingStyle.fill;
    paint_70_fill.color = Colors.black;
    canvas.drawPath(path_70, paint_70_fill);

    Path path_71 = Path()..fillType = PathFillType.evenOdd;
    path_71.moveTo(size.width * 0.1298884, size.height * 0.2659461);
    path_71.cubicTo(
        size.width * 0.1295196,
        size.height * 0.2659461,
        size.width * 0.1292438,
        size.height * 0.2659461,
        size.width * 0.1288750,
        size.height * 0.2659461);
    path_71.cubicTo(
        size.width * 0.1172661,
        size.height * 0.2656471,
        size.width * 0.1099884,
        size.height * 0.2596676,
        size.width * 0.1073161,
        size.height * 0.2481069);
    path_71.cubicTo(
        size.width * 0.1056580,
        size.height * 0.2407324,
        size.width * 0.1071321,
        size.height * 0.2339549,
        size.width * 0.1115545,
        size.height * 0.2293706);
    path_71.lineTo(size.width * 0.1679375, size.height * 0.1705706);
    path_71.cubicTo(
        size.width * 0.1684902,
        size.height * 0.1699725,
        size.width * 0.1687670,
        size.height * 0.1691755,
        size.width * 0.1687670,
        size.height * 0.1683784);
    path_71.cubicTo(
        size.width * 0.1687670,
        size.height * 0.1675804,
        size.width * 0.1683982,
        size.height * 0.1667833,
        size.width * 0.1678455,
        size.height * 0.1661853);
    path_71.cubicTo(
        size.width * 0.1640679,
        size.height * 0.1625980,
        size.width * 0.1604750,
        size.height * 0.1590098,
        size.width * 0.1573429,
        size.height * 0.1558206);
    path_71.lineTo(size.width * 0.1549473, size.height * 0.1534294);
    path_71.cubicTo(
        size.width * 0.1519991,
        size.height * 0.1504392,
        size.width * 0.1483143,
        size.height * 0.1465520,
        size.width * 0.1459188,
        size.height * 0.1440608);
    path_71.cubicTo(
        size.width * 0.1763214,
        size.height * 0.1214382,
        size.width * 0.1964982,
        size.height * 0.08934696,
        size.width * 0.2128973,
        size.height * 0.06333549);
    path_71.cubicTo(
        size.width * 0.2153848,
        size.height * 0.05934902,
        size.width * 0.2177795,
        size.height * 0.05566157,
        size.width * 0.2200830,
        size.height * 0.05207373);
    path_71.cubicTo(
        size.width * 0.2225705,
        size.height * 0.05426627,
        size.width * 0.2266241,
        size.height * 0.05775441,
        size.width * 0.2295723,
        size.height * 0.06084392);
    path_71.cubicTo(
        size.width * 0.2301250,
        size.height * 0.06144186,
        size.width * 0.2310464,
        size.height * 0.06233882,
        size.width * 0.2323366,
        size.height * 0.06353480);
    path_71.cubicTo(
        size.width * 0.2396143,
        size.height * 0.07041137,
        size.width * 0.2565661,
        size.height * 0.08655647,
        size.width * 0.2672536,
        size.height * 0.1070863);
    path_71.cubicTo(
        size.width * 0.2802438,
        size.height * 0.1322010,
        size.width * 0.2788616,
        size.height * 0.1548245,
        size.width * 0.2632000,
        size.height * 0.1744578);
    path_71.cubicTo(
        size.width * 0.2513152,
        size.height * 0.1893069,
        size.width * 0.2381402,
        size.height * 0.1968814,
        size.width * 0.2240446,
        size.height * 0.1968814);
    path_71.cubicTo(
        size.width * 0.2141866,
        size.height * 0.1968814,
        size.width * 0.2038679,
        size.height * 0.1933931,
        size.width * 0.1925366,
        size.height * 0.1861176);
    path_71.cubicTo(
        size.width * 0.1920759,
        size.height * 0.1858186,
        size.width * 0.1916152,
        size.height * 0.1857186,
        size.width * 0.1911545,
        size.height * 0.1857186);
    path_71.cubicTo(
        size.width * 0.1902330,
        size.height * 0.1857186,
        size.width * 0.1893116,
        size.height * 0.1862176,
        size.width * 0.1888509,
        size.height * 0.1871147);
    path_71.lineTo(size.width * 0.1479455, size.height * 0.2578735);
    path_71.cubicTo(
        size.width * 0.1439839,
        size.height * 0.2645510,
        size.width * 0.1359687,
        size.height * 0.2659461,
        size.width * 0.1298884,
        size.height * 0.2659461);
    path_71.close();

    Paint paint_71_fill = Paint()..style = PaintingStyle.fill;
    paint_71_fill.color = activeColor;
    canvas.drawPath(path_71, paint_71_fill);

    Path path_72 = Path()..fillType = PathFillType.evenOdd;
    path_72.moveTo(size.width * 0.1146188, size.height * 0.2329353);
    path_72.cubicTo(
        size.width * 0.1115866,
        size.height * 0.2360794,
        size.width * 0.1103036,
        size.height * 0.2409206,
        size.width * 0.1116473,
        size.height * 0.2469157);
    path_72.cubicTo(
        size.width * 0.1127839,
        size.height * 0.2518255,
        size.width * 0.1147902,
        size.height * 0.2551539,
        size.width * 0.1174286,
        size.height * 0.2573324);
    path_72.cubicTo(
        size.width * 0.1200902,
        size.height * 0.2595314,
        size.width * 0.1238152,
        size.height * 0.2609039,
        size.width * 0.1289286,
        size.height * 0.2610441);
    path_72.lineTo(size.width * 0.1298839, size.height * 0.2610441);
    path_72.cubicTo(
        size.width * 0.1358348,
        size.height * 0.2610441,
        size.width * 0.1415929,
        size.height * 0.2595725,
        size.width * 0.1441920,
        size.height * 0.2552196);
    path_72.lineTo(size.width * 0.1850348, size.height * 0.1845686);
    path_72.cubicTo(
        size.width * 0.1864259,
        size.height * 0.1819873,
        size.width * 0.1889286,
        size.height * 0.1808167,
        size.width * 0.1911545,
        size.height * 0.1808167);
    path_72.cubicTo(
        size.width * 0.1922464,
        size.height * 0.1808167,
        size.width * 0.1935223,
        size.height * 0.1810667,
        size.width * 0.1947964,
        size.height * 0.1818902);
    path_72.cubicTo(
        size.width * 0.2056482,
        size.height * 0.1888559,
        size.width * 0.2151893,
        size.height * 0.1919794,
        size.width * 0.2240446,
        size.height * 0.1919794);
    path_72.cubicTo(
        size.width * 0.2365116,
        size.height * 0.1919794,
        size.width * 0.2485509,
        size.height * 0.1853343,
        size.width * 0.2598455,
        size.height * 0.1712225);
    path_72.cubicTo(
        size.width * 0.2671321,
        size.height * 0.1620863,
        size.width * 0.2709116,
        size.height * 0.1525137,
        size.width * 0.2715170,
        size.height * 0.1425029);
    path_72.cubicTo(
        size.width * 0.2721241,
        size.height * 0.1324451,
        size.width * 0.2695545,
        size.height * 0.1214637,
        size.width * 0.2633750,
        size.height * 0.1095147);
    path_72.cubicTo(
        size.width * 0.2531188,
        size.height * 0.08981343,
        size.width * 0.2367393,
        size.height * 0.07416157,
        size.width * 0.2294402,
        size.height * 0.06726520);
    path_72.cubicTo(
        size.width * 0.2281643,
        size.height * 0.06608235,
        size.width * 0.2271473,
        size.height * 0.06509892,
        size.width * 0.2264670,
        size.height * 0.06436598);
    path_72.cubicTo(
        size.width * 0.2248509,
        size.height * 0.06267402,
        size.width * 0.2228696,
        size.height * 0.06084333,
        size.width * 0.2209893,
        size.height * 0.05916216);
    path_72.cubicTo(
        size.width * 0.2195482,
        size.height * 0.06140471,
        size.width * 0.2180821,
        size.height * 0.06369804,
        size.width * 0.2165795,
        size.height * 0.06610618);
    path_72.lineTo(size.width * 0.2165679, size.height * 0.06612559);
    path_72.lineTo(size.width * 0.2162509, size.height * 0.06662745);
    path_72.cubicTo(
        size.width * 0.2007625,
        size.height * 0.09119598,
        size.width * 0.1814357,
        size.height * 0.1218520,
        size.width * 0.1529429,
        size.height * 0.1446314);
    path_72.cubicTo(
        size.width * 0.1546241,
        size.height * 0.1463853,
        size.width * 0.1563964,
        size.height * 0.1482245,
        size.width * 0.1579643,
        size.height * 0.1498157);
    path_72.cubicTo(
        size.width * 0.1579679,
        size.height * 0.1498196,
        size.width * 0.1579723,
        size.height * 0.1498235,
        size.width * 0.1579768,
        size.height * 0.1498275);
    path_72.lineTo(size.width * 0.1603786, size.height * 0.1522265);
    path_72.cubicTo(
        size.width * 0.1634884,
        size.height * 0.1553922,
        size.width * 0.1670411,
        size.height * 0.1589402,
        size.width * 0.1707661,
        size.height * 0.1624784);
    path_72.lineTo(size.width * 0.1708759, size.height * 0.1625824);
    path_72.lineTo(size.width * 0.1709786, size.height * 0.1626931);
    path_72.cubicTo(
        size.width * 0.1721607,
        size.height * 0.1639716,
        size.width * 0.1732313,
        size.height * 0.1659608,
        size.width * 0.1732313,
        size.height * 0.1683784);
    path_72.cubicTo(
        size.width * 0.1732313,
        size.height * 0.1704108,
        size.width * 0.1725241,
        size.height * 0.1724902,
        size.width * 0.1710705,
        size.height * 0.1740627);
    path_72.lineTo(size.width * 0.1710125, size.height * 0.1741255);
    path_72.lineTo(size.width * 0.1146188, size.height * 0.2329353);
    path_72.close();
    path_72.moveTo(size.width * 0.1549473, size.height * 0.1534294);
    path_72.cubicTo(
        size.width * 0.1531616,
        size.height * 0.1516176,
        size.width * 0.1511045,
        size.height * 0.1494765,
        size.width * 0.1492277,
        size.height * 0.1475167);
    path_72.cubicTo(
        size.width * 0.1488446,
        size.height * 0.1471157,
        size.width * 0.1484687,
        size.height * 0.1467235,
        size.width * 0.1481045,
        size.height * 0.1463422);
    path_72.cubicTo(
        size.width * 0.1473080,
        size.height * 0.1455098,
        size.width * 0.1465661,
        size.height * 0.1447343,
        size.width * 0.1459188,
        size.height * 0.1440608);
    path_72.cubicTo(
        size.width * 0.1471813,
        size.height * 0.1431216,
        size.width * 0.1484259,
        size.height * 0.1421657,
        size.width * 0.1496536,
        size.height * 0.1411951);
    path_72.cubicTo(
        size.width * 0.1778902,
        size.height * 0.1188627,
        size.width * 0.1970420,
        size.height * 0.08848373,
        size.width * 0.2127295,
        size.height * 0.06360167);
    path_72.lineTo(size.width * 0.2128973, size.height * 0.06333549);
    path_72.cubicTo(
        size.width * 0.2144687,
        size.height * 0.06081608,
        size.width * 0.2160045,
        size.height * 0.05841608,
        size.width * 0.2175027,
        size.height * 0.05608520);
    path_72.cubicTo(
        size.width * 0.2178580,
        size.height * 0.05553216,
        size.width * 0.2182116,
        size.height * 0.05498294,
        size.width * 0.2185634,
        size.height * 0.05443706);
    path_72.cubicTo(
        size.width * 0.2190741,
        size.height * 0.05364314,
        size.width * 0.2195813,
        size.height * 0.05285608,
        size.width * 0.2200830,
        size.height * 0.05207373);
    path_72.cubicTo(
        size.width * 0.2203848,
        size.height * 0.05233961,
        size.width * 0.2207089,
        size.height * 0.05262451,
        size.width * 0.2210518,
        size.height * 0.05292539);
    path_72.cubicTo(
        size.width * 0.2218187,
        size.height * 0.05359775,
        size.width * 0.2226759,
        size.height * 0.05435010,
        size.width * 0.2235705,
        size.height * 0.05514873);
    path_72.cubicTo(
        size.width * 0.2255795,
        size.height * 0.05694078,
        size.width * 0.2277804,
        size.height * 0.05896598,
        size.width * 0.2295723,
        size.height * 0.06084392);
    path_72.cubicTo(
        size.width * 0.2301250,
        size.height * 0.06144186,
        size.width * 0.2310464,
        size.height * 0.06233882,
        size.width * 0.2323366,
        size.height * 0.06353480);
    path_72.cubicTo(
        size.width * 0.2396143,
        size.height * 0.07041137,
        size.width * 0.2565661,
        size.height * 0.08655647,
        size.width * 0.2672536,
        size.height * 0.1070863);
    path_72.cubicTo(
        size.width * 0.2802438,
        size.height * 0.1322010,
        size.width * 0.2788616,
        size.height * 0.1548245,
        size.width * 0.2632000,
        size.height * 0.1744578);
    path_72.cubicTo(
        size.width * 0.2513152,
        size.height * 0.1893069,
        size.width * 0.2381402,
        size.height * 0.1968814,
        size.width * 0.2240446,
        size.height * 0.1968814);
    path_72.cubicTo(
        size.width * 0.2141866,
        size.height * 0.1968814,
        size.width * 0.2038679,
        size.height * 0.1933931,
        size.width * 0.1925366,
        size.height * 0.1861176);
    path_72.cubicTo(
        size.width * 0.1920759,
        size.height * 0.1858186,
        size.width * 0.1916152,
        size.height * 0.1857186,
        size.width * 0.1911545,
        size.height * 0.1857186);
    path_72.cubicTo(
        size.width * 0.1902330,
        size.height * 0.1857186,
        size.width * 0.1893116,
        size.height * 0.1862176,
        size.width * 0.1888509,
        size.height * 0.1871147);
    path_72.lineTo(size.width * 0.1479455, size.height * 0.2578735);
    path_72.cubicTo(
        size.width * 0.1439839,
        size.height * 0.2645510,
        size.width * 0.1359687,
        size.height * 0.2659461,
        size.width * 0.1298884,
        size.height * 0.2659461);
    path_72.lineTo(size.width * 0.1288750, size.height * 0.2659461);
    path_72.cubicTo(
        size.width * 0.1172661,
        size.height * 0.2656471,
        size.width * 0.1099884,
        size.height * 0.2596676,
        size.width * 0.1073161,
        size.height * 0.2481069);
    path_72.cubicTo(
        size.width * 0.1056580,
        size.height * 0.2407324,
        size.width * 0.1071321,
        size.height * 0.2339549,
        size.width * 0.1115545,
        size.height * 0.2293706);
    path_72.lineTo(size.width * 0.1679375, size.height * 0.1705706);
    path_72.cubicTo(
        size.width * 0.1684902,
        size.height * 0.1699725,
        size.width * 0.1687670,
        size.height * 0.1691755,
        size.width * 0.1687670,
        size.height * 0.1683784);
    path_72.cubicTo(
        size.width * 0.1687670,
        size.height * 0.1675804,
        size.width * 0.1683982,
        size.height * 0.1667833,
        size.width * 0.1678455,
        size.height * 0.1661853);
    path_72.cubicTo(
        size.width * 0.1640679,
        size.height * 0.1625980,
        size.width * 0.1604750,
        size.height * 0.1590098,
        size.width * 0.1573429,
        size.height * 0.1558206);
    path_72.lineTo(size.width * 0.1549473, size.height * 0.1534294);
    path_72.close();

    Paint paint_72_fill = Paint()..style = PaintingStyle.fill;
    paint_72_fill.color = activeColor;
    canvas.drawPath(path_72, paint_72_fill);

    Path path_73 = Path()..fillType = PathFillType.evenOdd;
    path_73.moveTo(size.width * 0.1146188, size.height * 0.2329353);
    path_73.cubicTo(
        size.width * 0.1115866,
        size.height * 0.2360794,
        size.width * 0.1103036,
        size.height * 0.2409206,
        size.width * 0.1116473,
        size.height * 0.2469157);
    path_73.cubicTo(
        size.width * 0.1127839,
        size.height * 0.2518255,
        size.width * 0.1147902,
        size.height * 0.2551539,
        size.width * 0.1174286,
        size.height * 0.2573324);
    path_73.cubicTo(
        size.width * 0.1200902,
        size.height * 0.2595314,
        size.width * 0.1238152,
        size.height * 0.2609039,
        size.width * 0.1289286,
        size.height * 0.2610441);
    path_73.lineTo(size.width * 0.1298839, size.height * 0.2610441);
    path_73.cubicTo(
        size.width * 0.1358348,
        size.height * 0.2610441,
        size.width * 0.1415929,
        size.height * 0.2595725,
        size.width * 0.1441920,
        size.height * 0.2552196);
    path_73.lineTo(size.width * 0.1850348, size.height * 0.1845686);
    path_73.cubicTo(
        size.width * 0.1864259,
        size.height * 0.1819873,
        size.width * 0.1889286,
        size.height * 0.1808167,
        size.width * 0.1911545,
        size.height * 0.1808167);
    path_73.cubicTo(
        size.width * 0.1922464,
        size.height * 0.1808167,
        size.width * 0.1935223,
        size.height * 0.1810667,
        size.width * 0.1947964,
        size.height * 0.1818902);
    path_73.cubicTo(
        size.width * 0.2056482,
        size.height * 0.1888559,
        size.width * 0.2151893,
        size.height * 0.1919794,
        size.width * 0.2240446,
        size.height * 0.1919794);
    path_73.cubicTo(
        size.width * 0.2365116,
        size.height * 0.1919794,
        size.width * 0.2485509,
        size.height * 0.1853343,
        size.width * 0.2598455,
        size.height * 0.1712225);
    path_73.cubicTo(
        size.width * 0.2671321,
        size.height * 0.1620863,
        size.width * 0.2709116,
        size.height * 0.1525137,
        size.width * 0.2715170,
        size.height * 0.1425029);
    path_73.cubicTo(
        size.width * 0.2721241,
        size.height * 0.1324451,
        size.width * 0.2695545,
        size.height * 0.1214637,
        size.width * 0.2633750,
        size.height * 0.1095147);
    path_73.cubicTo(
        size.width * 0.2531188,
        size.height * 0.08981343,
        size.width * 0.2367393,
        size.height * 0.07416157,
        size.width * 0.2294402,
        size.height * 0.06726520);
    path_73.cubicTo(
        size.width * 0.2281643,
        size.height * 0.06608235,
        size.width * 0.2271473,
        size.height * 0.06509892,
        size.width * 0.2264670,
        size.height * 0.06436598);
    path_73.cubicTo(
        size.width * 0.2248509,
        size.height * 0.06267402,
        size.width * 0.2228696,
        size.height * 0.06084333,
        size.width * 0.2209893,
        size.height * 0.05916216);
    path_73.cubicTo(
        size.width * 0.2195482,
        size.height * 0.06140471,
        size.width * 0.2180821,
        size.height * 0.06369804,
        size.width * 0.2165795,
        size.height * 0.06610618);
    path_73.lineTo(size.width * 0.2165679, size.height * 0.06612559);
    path_73.lineTo(size.width * 0.2162509, size.height * 0.06662745);
    path_73.cubicTo(
        size.width * 0.2007625,
        size.height * 0.09119598,
        size.width * 0.1814357,
        size.height * 0.1218520,
        size.width * 0.1529429,
        size.height * 0.1446314);
    path_73.cubicTo(
        size.width * 0.1546241,
        size.height * 0.1463853,
        size.width * 0.1563964,
        size.height * 0.1482245,
        size.width * 0.1579643,
        size.height * 0.1498157);
    path_73.cubicTo(
        size.width * 0.1579679,
        size.height * 0.1498196,
        size.width * 0.1579723,
        size.height * 0.1498235,
        size.width * 0.1579768,
        size.height * 0.1498275);
    path_73.lineTo(size.width * 0.1603786, size.height * 0.1522265);
    path_73.cubicTo(
        size.width * 0.1634884,
        size.height * 0.1553922,
        size.width * 0.1670411,
        size.height * 0.1589402,
        size.width * 0.1707661,
        size.height * 0.1624784);
    path_73.lineTo(size.width * 0.1708759, size.height * 0.1625824);
    path_73.lineTo(size.width * 0.1709786, size.height * 0.1626931);
    path_73.cubicTo(
        size.width * 0.1721607,
        size.height * 0.1639716,
        size.width * 0.1732313,
        size.height * 0.1659608,
        size.width * 0.1732313,
        size.height * 0.1683784);
    path_73.cubicTo(
        size.width * 0.1732313,
        size.height * 0.1704108,
        size.width * 0.1725241,
        size.height * 0.1724902,
        size.width * 0.1710705,
        size.height * 0.1740627);
    path_73.lineTo(size.width * 0.1710125, size.height * 0.1741255);
    path_73.lineTo(size.width * 0.1146188, size.height * 0.2329353);
    path_73.close();
    path_73.moveTo(size.width * 0.1549473, size.height * 0.1534294);
    path_73.cubicTo(
        size.width * 0.1531616,
        size.height * 0.1516176,
        size.width * 0.1511045,
        size.height * 0.1494765,
        size.width * 0.1492277,
        size.height * 0.1475167);
    path_73.cubicTo(
        size.width * 0.1488446,
        size.height * 0.1471157,
        size.width * 0.1484687,
        size.height * 0.1467235,
        size.width * 0.1481045,
        size.height * 0.1463422);
    path_73.cubicTo(
        size.width * 0.1473080,
        size.height * 0.1455098,
        size.width * 0.1465661,
        size.height * 0.1447343,
        size.width * 0.1459188,
        size.height * 0.1440608);
    path_73.cubicTo(
        size.width * 0.1471813,
        size.height * 0.1431216,
        size.width * 0.1484259,
        size.height * 0.1421657,
        size.width * 0.1496536,
        size.height * 0.1411951);
    path_73.cubicTo(
        size.width * 0.1778902,
        size.height * 0.1188627,
        size.width * 0.1970420,
        size.height * 0.08848373,
        size.width * 0.2127295,
        size.height * 0.06360167);
    path_73.lineTo(size.width * 0.2128973, size.height * 0.06333549);
    path_73.cubicTo(
        size.width * 0.2144687,
        size.height * 0.06081608,
        size.width * 0.2160045,
        size.height * 0.05841608,
        size.width * 0.2175027,
        size.height * 0.05608520);
    path_73.cubicTo(
        size.width * 0.2178580,
        size.height * 0.05553216,
        size.width * 0.2182116,
        size.height * 0.05498294,
        size.width * 0.2185634,
        size.height * 0.05443706);
    path_73.cubicTo(
        size.width * 0.2190741,
        size.height * 0.05364314,
        size.width * 0.2195813,
        size.height * 0.05285608,
        size.width * 0.2200830,
        size.height * 0.05207373);
    path_73.cubicTo(
        size.width * 0.2203848,
        size.height * 0.05233961,
        size.width * 0.2207089,
        size.height * 0.05262451,
        size.width * 0.2210518,
        size.height * 0.05292539);
    path_73.cubicTo(
        size.width * 0.2218187,
        size.height * 0.05359775,
        size.width * 0.2226759,
        size.height * 0.05435010,
        size.width * 0.2235705,
        size.height * 0.05514873);
    path_73.cubicTo(
        size.width * 0.2255795,
        size.height * 0.05694078,
        size.width * 0.2277804,
        size.height * 0.05896598,
        size.width * 0.2295723,
        size.height * 0.06084392);
    path_73.cubicTo(
        size.width * 0.2301250,
        size.height * 0.06144186,
        size.width * 0.2310464,
        size.height * 0.06233882,
        size.width * 0.2323366,
        size.height * 0.06353480);
    path_73.cubicTo(
        size.width * 0.2396143,
        size.height * 0.07041137,
        size.width * 0.2565661,
        size.height * 0.08655647,
        size.width * 0.2672536,
        size.height * 0.1070863);
    path_73.cubicTo(
        size.width * 0.2802438,
        size.height * 0.1322010,
        size.width * 0.2788616,
        size.height * 0.1548245,
        size.width * 0.2632000,
        size.height * 0.1744578);
    path_73.cubicTo(
        size.width * 0.2513152,
        size.height * 0.1893069,
        size.width * 0.2381402,
        size.height * 0.1968814,
        size.width * 0.2240446,
        size.height * 0.1968814);
    path_73.cubicTo(
        size.width * 0.2141866,
        size.height * 0.1968814,
        size.width * 0.2038679,
        size.height * 0.1933931,
        size.width * 0.1925366,
        size.height * 0.1861176);
    path_73.cubicTo(
        size.width * 0.1920759,
        size.height * 0.1858186,
        size.width * 0.1916152,
        size.height * 0.1857186,
        size.width * 0.1911545,
        size.height * 0.1857186);
    path_73.cubicTo(
        size.width * 0.1902330,
        size.height * 0.1857186,
        size.width * 0.1893116,
        size.height * 0.1862176,
        size.width * 0.1888509,
        size.height * 0.1871147);
    path_73.lineTo(size.width * 0.1479455, size.height * 0.2578735);
    path_73.cubicTo(
        size.width * 0.1439839,
        size.height * 0.2645510,
        size.width * 0.1359687,
        size.height * 0.2659461,
        size.width * 0.1298884,
        size.height * 0.2659461);
    path_73.lineTo(size.width * 0.1288750, size.height * 0.2659461);
    path_73.cubicTo(
        size.width * 0.1172661,
        size.height * 0.2656471,
        size.width * 0.1099884,
        size.height * 0.2596676,
        size.width * 0.1073161,
        size.height * 0.2481069);
    path_73.cubicTo(
        size.width * 0.1056580,
        size.height * 0.2407324,
        size.width * 0.1071321,
        size.height * 0.2339549,
        size.width * 0.1115545,
        size.height * 0.2293706);
    path_73.lineTo(size.width * 0.1679375, size.height * 0.1705706);
    path_73.cubicTo(
        size.width * 0.1684902,
        size.height * 0.1699725,
        size.width * 0.1687670,
        size.height * 0.1691755,
        size.width * 0.1687670,
        size.height * 0.1683784);
    path_73.cubicTo(
        size.width * 0.1687670,
        size.height * 0.1675804,
        size.width * 0.1683982,
        size.height * 0.1667833,
        size.width * 0.1678455,
        size.height * 0.1661853);
    path_73.cubicTo(
        size.width * 0.1640679,
        size.height * 0.1625980,
        size.width * 0.1604750,
        size.height * 0.1590098,
        size.width * 0.1573429,
        size.height * 0.1558206);
    path_73.lineTo(size.width * 0.1549473, size.height * 0.1534294);
    path_73.close();

    Paint paint_73_fill = Paint()..style = PaintingStyle.fill;
    paint_73_fill.color = Colors.black;
    canvas.drawPath(path_73, paint_73_fill);

    Path path_74 = Path()..fillType = PathFillType.evenOdd;
    path_74.moveTo(size.width * 0.2195304, size.height * 0.04777500);
    path_74.cubicTo(
        size.width * 0.2195304,
        size.height * 0.04777500,
        size.width * 0.1916152,
        size.height * 0.06083059,
        size.width * 0.1698723,
        size.height * 0.09023059);
    path_74.cubicTo(
        size.width * 0.1468402,
        size.height * 0.1214245,
        size.width * 0.1415884,
        size.height * 0.1435490,
        size.width * 0.1415884,
        size.height * 0.1435490);
    path_74.cubicTo(
        size.width * 0.1473929,
        size.height * 0.1483333,
        size.width * 0.1776116,
        size.height * 0.1261088,
        size.width * 0.1930893,
        size.height * 0.1050804);
    path_74.cubicTo(
        size.width * 0.2080143,
        size.height * 0.08504814,
        size.width * 0.2245973,
        size.height * 0.05355529,
        size.width * 0.2195304,
        size.height * 0.04777500);
    path_74.close();

    Paint paint_74_fill = Paint()..style = PaintingStyle.fill;
    paint_74_fill.color = activeColor;
    canvas.drawPath(path_74, paint_74_fill);

    Path path_75 = Path()..fillType = PathFillType.evenOdd;
    path_75.moveTo(size.width * 0.2195304, size.height * 0.04777500);
    path_75.cubicTo(
        size.width * 0.2195304,
        size.height * 0.04777500,
        size.width * 0.1916152,
        size.height * 0.06083059,
        size.width * 0.1698723,
        size.height * 0.09023059);
    path_75.cubicTo(
        size.width * 0.1468402,
        size.height * 0.1214245,
        size.width * 0.1415884,
        size.height * 0.1435490,
        size.width * 0.1415884,
        size.height * 0.1435490);
    path_75.cubicTo(
        size.width * 0.1421420,
        size.height * 0.1440049,
        size.width * 0.1429170,
        size.height * 0.1442157,
        size.width * 0.1438804,
        size.height * 0.1442049);
    path_75.cubicTo(
        size.width * 0.1445518,
        size.height * 0.1441980,
        size.width * 0.1453143,
        size.height * 0.1440833,
        size.width * 0.1461571,
        size.height * 0.1438696);
    path_75.cubicTo(
        size.width * 0.1567848,
        size.height * 0.1411725,
        size.width * 0.1801152,
        size.height * 0.1227059,
        size.width * 0.1930893,
        size.height * 0.1050804);
    path_75.cubicTo(
        size.width * 0.2054866,
        size.height * 0.08844078,
        size.width * 0.2190277,
        size.height * 0.06389392,
        size.width * 0.2203545,
        size.height * 0.05274382);
    path_75.cubicTo(
        size.width * 0.2204777,
        size.height * 0.05171520,
        size.width * 0.2204955,
        size.height * 0.05080059,
        size.width * 0.2204000,
        size.height * 0.05001676);
    path_75.cubicTo(
        size.width * 0.2202839,
        size.height * 0.04906814,
        size.width * 0.2200000,
        size.height * 0.04831098,
        size.width * 0.2195304,
        size.height * 0.04777500);
    path_75.close();
    path_75.moveTo(size.width * 0.2159580, size.height * 0.05056402);
    path_75.cubicTo(
        size.width * 0.2159589,
        size.height * 0.05056363,
        size.width * 0.2159616,
        size.height * 0.05057225,
        size.width * 0.2159652,
        size.height * 0.05059137);
    path_75.cubicTo(
        size.width * 0.2159598,
        size.height * 0.05057402,
        size.width * 0.2159580,
        size.height * 0.05056441,
        size.width * 0.2159580,
        size.height * 0.05056402);
    path_75.close();
    path_75.moveTo(size.width * 0.2151741, size.height * 0.05561931);
    path_75.cubicTo(
        size.width * 0.2141750,
        size.height * 0.05914275,
        size.width * 0.2123429,
        size.height * 0.06366765,
        size.width * 0.2098652,
        size.height * 0.06878078);
    path_75.cubicTo(
        size.width * 0.2046330,
        size.height * 0.07958020,
        size.width * 0.1969214,
        size.height * 0.09219451,
        size.width * 0.1896339,
        size.height * 0.1019765);
    path_75.lineTo(size.width * 0.1896170, size.height * 0.1019990);
    path_75.cubicTo(
        size.width * 0.1822179,
        size.height * 0.1120510,
        size.width * 0.1711509,
        size.height * 0.1225510,
        size.width * 0.1612098,
        size.height * 0.1299333);
    path_75.cubicTo(
        size.width * 0.1562375,
        size.height * 0.1336265,
        size.width * 0.1517179,
        size.height * 0.1364118,
        size.width * 0.1482241,
        size.height * 0.1379735);
    path_75.cubicTo(
        size.width * 0.1481884,
        size.height * 0.1379892,
        size.width * 0.1481527,
        size.height * 0.1380049,
        size.width * 0.1481179,
        size.height * 0.1380206);
    path_75.cubicTo(
        size.width * 0.1488732,
        size.height * 0.1359961,
        size.width * 0.1498536,
        size.height * 0.1335529,
        size.width * 0.1511054,
        size.height * 0.1307373);
    path_75.cubicTo(
        size.width * 0.1551554,
        size.height * 0.1216265,
        size.width * 0.1620464,
        size.height * 0.1086137,
        size.width * 0.1733393,
        size.height * 0.09331873);
    path_75.moveTo(size.width * 0.1436482, size.height * 0.1392951);
    path_75.cubicTo(
        size.width * 0.1436482,
        size.height * 0.1392941,
        size.width * 0.1436571,
        size.height * 0.1392951,
        size.width * 0.1436723,
        size.height * 0.1392990);
    path_75.close();
    path_75.moveTo(size.width * 0.2151741, size.height * 0.05561931);
    path_75.cubicTo(
        size.width * 0.2125116,
        size.height * 0.05719647,
        size.width * 0.2091232,
        size.height * 0.05934284,
        size.width * 0.2052920,
        size.height * 0.06208461);
    path_75.cubicTo(
        size.width * 0.1958607,
        size.height * 0.06883363,
        size.width * 0.1838375,
        size.height * 0.07912382,
        size.width * 0.1733402,
        size.height * 0.09331765);

    Paint paint_75_fill = Paint()..style = PaintingStyle.fill;
    paint_75_fill.color = Colors.black;
    canvas.drawPath(path_75, paint_75_fill);

    Path path_76 = Path()..fillType = PathFillType.evenOdd;
    path_76.moveTo(size.width * 0.1987179, size.height * 0.2066951);
    path_76.cubicTo(
        size.width * 0.1877545,
        size.height * 0.2008147,
        size.width * 0.1523759,
        size.height * 0.1831755,
        size.width * 0.1456509,
        size.height * 0.1738069);
    path_76.lineTo(size.width * 0.1448214, size.height * 0.1701196);
    path_76.cubicTo(
        size.width * 0.1286071,
        size.height * 0.1668304,
        size.width * 0.1077857,
        size.height * 0.1687245,
        size.width * 0.1002312,
        size.height * 0.1741059);
    path_76.lineTo(size.width * 0.07102580, size.height * 0.1950343);
    path_76.lineTo(size.width * 0.0002702982, size.height * 0.2139706);
    path_76.lineTo(size.width * 0.0002702982, size.height * 0.3261882);
    path_76.lineTo(size.width * 0.1008759, size.height * 0.2460608);
    path_76.cubicTo(
        size.width * 0.1243688,
        size.height * 0.2525392,
        size.width * 0.1362536,
        size.height * 0.2318098,
        size.width * 0.1587330,
        size.height * 0.2322078);
    path_76.cubicTo(
        size.width * 0.1738429,
        size.height * 0.2355971,
        size.width * 0.1886750,
        size.height * 0.2370912,
        size.width * 0.1965982,
        size.height * 0.2330059);
    path_76.cubicTo(
        size.width * 0.2066402,
        size.height * 0.2278235,
        size.width * 0.2082991,
        size.height * 0.2117775,
        size.width * 0.1987179,
        size.height * 0.2066951);
    path_76.close();

    Paint paint_76_fill = Paint()..style = PaintingStyle.fill;
    paint_76_fill.color = Colors.white;
    canvas.drawPath(path_76, paint_76_fill);

    Path path_77 = Path()..fillType = PathFillType.evenOdd;
    path_77.moveTo(size.width * 0.1967759, size.height * 0.2111088);
    path_77.lineTo(size.width * 0.1967580, size.height * 0.2111000);
    path_77.cubicTo(
        size.width * 0.1953545,
        size.height * 0.2103471,
        size.width * 0.1935098,
        size.height * 0.2093804,
        size.width * 0.1913509,
        size.height * 0.2082500);
    path_77.cubicTo(
        size.width * 0.1852821,
        size.height * 0.2050706,
        size.width * 0.1767348,
        size.height * 0.2005931,
        size.width * 0.1685348,
        size.height * 0.1959078);
    path_77.cubicTo(
        size.width * 0.1629607,
        size.height * 0.1927235,
        size.width * 0.1574429,
        size.height * 0.1893863,
        size.width * 0.1528607,
        size.height * 0.1862206);
    path_77.cubicTo(
        size.width * 0.1484446,
        size.height * 0.1831696,
        size.width * 0.1443562,
        size.height * 0.1799216,
        size.width * 0.1421446,
        size.height * 0.1768412);
    path_77.lineTo(size.width * 0.1415482, size.height * 0.1760098);
    path_77.lineTo(size.width * 0.1411929, size.height * 0.1744304);
    path_77.cubicTo(
        size.width * 0.1342214,
        size.height * 0.1733167,
        size.width * 0.1265357,
        size.height * 0.1731275,
        size.width * 0.1196429,
        size.height * 0.1737471);
    path_77.cubicTo(
        size.width * 0.1115812,
        size.height * 0.1744716,
        size.width * 0.1054545,
        size.height * 0.1762314,
        size.width * 0.1026661,
        size.height * 0.1782147);
    path_77.cubicTo(
        size.width * 0.1026643,
        size.height * 0.1782157,
        size.width * 0.1026679,
        size.height * 0.1782137,
        size.width * 0.1026661,
        size.height * 0.1782147);
    path_77.lineTo(size.width * 0.07282563, size.height * 0.1995990);
    path_77.lineTo(size.width * 0.004734580, size.height * 0.2178206);
    path_77.lineTo(size.width * 0.004734580, size.height * 0.3165775);
    path_77.lineTo(size.width * 0.09994196, size.height * 0.2407490);
    path_77.lineTo(size.width * 0.1019634, size.height * 0.2413069);
    path_77.cubicTo(
        size.width * 0.1121920,
        size.height * 0.2441275,
        size.width * 0.1198580,
        size.height * 0.2411559,
        size.width * 0.1283473,
        size.height * 0.2369471);
    path_77.cubicTo(
        size.width * 0.1292777,
        size.height * 0.2364853,
        size.width * 0.1302241,
        size.height * 0.2360049,
        size.width * 0.1311875,
        size.height * 0.2355147);
    path_77.cubicTo(
        size.width * 0.1388866,
        size.height * 0.2316029,
        size.width * 0.1477268,
        size.height * 0.2271108,
        size.width * 0.1588054,
        size.height * 0.2273069);
    path_77.lineTo(size.width * 0.1592205, size.height * 0.2273147);
    path_77.lineTo(size.width * 0.1596268, size.height * 0.2274059);
    path_77.cubicTo(
        size.width * 0.1670545,
        size.height * 0.2290716,
        size.width * 0.1742812,
        size.height * 0.2302392,
        size.width * 0.1805375,
        size.height * 0.2305118);
    path_77.cubicTo(
        size.width * 0.1869277,
        size.height * 0.2307902,
        size.width * 0.1917330,
        size.height * 0.2300990,
        size.width * 0.1947009,
        size.height * 0.2285686);
    path_77.cubicTo(
        size.width * 0.1981634,
        size.height * 0.2267814,
        size.width * 0.2002402,
        size.height * 0.2230775,
        size.width * 0.2005741,
        size.height * 0.2193353);
    path_77.cubicTo(
        size.width * 0.2009018,
        size.height * 0.2156529,
        size.width * 0.1995509,
        size.height * 0.2125814,
        size.width * 0.1967759,
        size.height * 0.2111088);
    path_77.close();
    path_77.moveTo(size.width * 0.0002702982, size.height * 0.3261882);
    path_77.lineTo(size.width * 0.0002702982, size.height * 0.2139706);
    path_77.lineTo(size.width * 0.07102580, size.height * 0.1950343);
    path_77.lineTo(size.width * 0.1002312, size.height * 0.1741059);
    path_77.cubicTo(
        size.width * 0.1077857,
        size.height * 0.1687245,
        size.width * 0.1286071,
        size.height * 0.1668304,
        size.width * 0.1448214,
        size.height * 0.1701196);
    path_77.lineTo(size.width * 0.1456509, size.height * 0.1738069);
    path_77.cubicTo(
        size.width * 0.1514848,
        size.height * 0.1819333,
        size.width * 0.1788759,
        size.height * 0.1962824,
        size.width * 0.1931929,
        size.height * 0.2037824);
    path_77.cubicTo(
        size.width * 0.1953813,
        size.height * 0.2049294,
        size.width * 0.1972634,
        size.height * 0.2059157,
        size.width * 0.1987179,
        size.height * 0.2066951);
    path_77.cubicTo(
        size.width * 0.2082991,
        size.height * 0.2117775,
        size.width * 0.2066411,
        size.height * 0.2278235,
        size.width * 0.1965982,
        size.height * 0.2330059);
    path_77.cubicTo(
        size.width * 0.1886750,
        size.height * 0.2370922,
        size.width * 0.1738429,
        size.height * 0.2355971,
        size.width * 0.1587330,
        size.height * 0.2322078);
    path_77.cubicTo(
        size.width * 0.1487375,
        size.height * 0.2320314,
        size.width * 0.1408357,
        size.height * 0.2360314,
        size.width * 0.1330777,
        size.height * 0.2399598);
    path_77.cubicTo(
        size.width * 0.1233884,
        size.height * 0.2448657,
        size.width * 0.1139223,
        size.height * 0.2496588,
        size.width * 0.1008759,
        size.height * 0.2460608);
    path_77.lineTo(size.width * 0.0002702982, size.height * 0.3261882);
    path_77.close();

    Paint paint_77_fill = Paint()..style = PaintingStyle.fill;
    paint_77_fill.color = Colors.black;
    canvas.drawPath(path_77, paint_77_fill);

    Path path_78 = Path()..fillType = PathFillType.evenOdd;
    path_78.moveTo(size.width * 0.1467938, size.height * 0.1711451);
    path_78.cubicTo(
        size.width * 0.1473241,
        size.height * 0.1700363,
        size.width * 0.1470063,
        size.height * 0.1686471,
        size.width * 0.1460313,
        size.height * 0.1679520);
    path_78.cubicTo(
        size.width * 0.1449964,
        size.height * 0.1672157,
        size.width * 0.1436143,
        size.height * 0.1675392,
        size.width * 0.1429437,
        size.height * 0.1686745);
    path_78.cubicTo(
        size.width * 0.1416161,
        size.height * 0.1709225,
        size.width * 0.1425964,
        size.height * 0.1732853,
        size.width * 0.1435804,
        size.height * 0.1747902);
    path_78.cubicTo(
        size.width * 0.1446375,
        size.height * 0.1764098,
        size.width * 0.1463259,
        size.height * 0.1780853,
        size.width * 0.1483071,
        size.height * 0.1797461);
    path_78.cubicTo(
        size.width * 0.1523116,
        size.height * 0.1831020,
        size.width * 0.1581152,
        size.height * 0.1868882,
        size.width * 0.1643420,
        size.height * 0.1906000);
    path_78.cubicTo(
        size.width * 0.1741821,
        size.height * 0.1964647,
        size.width * 0.1853616,
        size.height * 0.2023069,
        size.width * 0.1927339,
        size.height * 0.2061598);
    path_78.cubicTo(
        size.width * 0.1947045,
        size.height * 0.2071902,
        size.width * 0.1964027,
        size.height * 0.2080775,
        size.width * 0.1977312,
        size.height * 0.2087853);
    path_78.cubicTo(
        size.width * 0.2030625,
        size.height * 0.2116735,
        size.width * 0.2042634,
        size.height * 0.2189853,
        size.width * 0.2012723,
        size.height * 0.2249324);
    path_78.cubicTo(
        size.width * 0.2006768,
        size.height * 0.2261176,
        size.width * 0.2010679,
        size.height * 0.2276088,
        size.width * 0.2021473,
        size.height * 0.2282627);
    path_78.cubicTo(
        size.width * 0.2032268,
        size.height * 0.2289176,
        size.width * 0.2045848,
        size.height * 0.2284873,
        size.width * 0.2051804,
        size.height * 0.2273020);
    path_78.cubicTo(
        size.width * 0.2090054,
        size.height * 0.2196971,
        size.width * 0.2079973,
        size.height * 0.2088735,
        size.width * 0.1996982,
        size.height * 0.2043853);
    path_78.lineTo(size.width * 0.1996982, size.height * 0.2043853);
    path_78.lineTo(size.width * 0.1996866, size.height * 0.2043794);
    path_78.cubicTo(
        size.width * 0.1983179,
        size.height * 0.2036490,
        size.width * 0.1965839,
        size.height * 0.2027431,
        size.width * 0.1945830,
        size.height * 0.2016971);
    path_78.cubicTo(
        size.width * 0.1872036,
        size.height * 0.1978402,
        size.width * 0.1761920,
        size.height * 0.1920853,
        size.width * 0.1664723,
        size.height * 0.1862912);
    path_78.cubicTo(
        size.width * 0.1602848,
        size.height * 0.1826029,
        size.width * 0.1547330,
        size.height * 0.1789647,
        size.width * 0.1510161,
        size.height * 0.1758490);
    path_78.cubicTo(
        size.width * 0.1491366,
        size.height * 0.1742735,
        size.width * 0.1478723,
        size.height * 0.1729520,
        size.width * 0.1472080,
        size.height * 0.1719333);
    path_78.cubicTo(
        size.width * 0.1469411,
        size.height * 0.1715255,
        size.width * 0.1468348,
        size.height * 0.1712765,
        size.width * 0.1467938,
        size.height * 0.1711451);
    path_78.close();
    path_78.moveTo(size.width * 0.1467786, size.height * 0.1710882);
    path_78.cubicTo(
        size.width * 0.1467696,
        size.height * 0.1710451,
        size.width * 0.1467705,
        size.height * 0.1710265,
        size.width * 0.1467741,
        size.height * 0.1710275);
    path_78.cubicTo(
        size.width * 0.1467768,
        size.height * 0.1710294,
        size.width * 0.1467821,
        size.height * 0.1710510,
        size.width * 0.1467786,
        size.height * 0.1710882);
    path_78.close();

    Paint paint_78_fill = Paint()..style = PaintingStyle.fill;
    paint_78_fill.color = Colors.black;
    canvas.drawPath(path_78, paint_78_fill);

    Path path_79 = Path()..fillType = PathFillType.evenOdd;
    path_79.moveTo(size.width * 0.9952679, size.height * 0.9449196);
    path_79.lineTo(size.width * 0.0002702982, size.height * 0.9449196);
    path_79.lineTo(size.width * 0.0002702982, size.height * 0.9400176);
    path_79.lineTo(size.width * 0.9952679, size.height * 0.9400176);
    path_79.lineTo(size.width * 0.9952679, size.height * 0.9449196);
    path_79.close();

    Paint paint_79_fill = Paint()..style = PaintingStyle.fill;
    paint_79_fill.color = Colors.black;
    canvas.drawPath(path_79, paint_79_fill);

    Path path_80 = Path()..fillType = PathFillType.evenOdd;
    path_80.moveTo(size.width * 0.2815241, size.height * 0.8584559);
    path_80.cubicTo(
        size.width * 0.2261545,
        size.height * 0.8584559,
        size.width * 0.2250491,
        size.height * 0.9438657,
        size.width * 0.2261545,
        size.height * 0.9726676);
    path_80.cubicTo(
        size.width * 0.2264304,
        size.height * 0.9784480,
        size.width * 0.2296554,
        size.height * 0.9836275,
        size.width * 0.2346304,
        size.height * 0.9858235);
    path_80.cubicTo(
        size.width * 0.2659545,
        size.height * 0.9997745,
        size.width * 0.2973705,
        size.height * 0.9996765,
        size.width * 0.3289705,
        size.height * 0.9858235);
    path_80.cubicTo(
        size.width * 0.3339455,
        size.height * 0.9836275,
        size.width * 0.3372625,
        size.height * 0.9784480,
        size.width * 0.3374473,
        size.height * 0.9725676);
    path_80.cubicTo(
        size.width * 0.3384607,
        size.height * 0.9436657,
        size.width * 0.3368018,
        size.height * 0.8584559,
        size.width * 0.2815241,
        size.height * 0.8584559);
    path_80.close();

    Paint paint_80_fill = Paint()..style = PaintingStyle.fill;
    paint_80_fill.color = Colors.white;
    canvas.drawPath(path_80, paint_80_fill);

    Path path_81 = Path()..fillType = PathFillType.evenOdd;
    path_81.moveTo(size.width * 0.2477973, size.height * 0.8704990);
    path_81.cubicTo(
        size.width * 0.2560848,
        size.height * 0.8615961,
        size.width * 0.2670955,
        size.height * 0.8560049,
        size.width * 0.2815241,
        size.height * 0.8560049);
    path_81.cubicTo(
        size.width * 0.2959295,
        size.height * 0.8560049,
        size.width * 0.3069545,
        size.height * 0.8615824,
        size.width * 0.3152804,
        size.height * 0.8704637);
    path_81.cubicTo(
        size.width * 0.3235518,
        size.height * 0.8792853,
        size.width * 0.3290598,
        size.height * 0.8912549,
        size.width * 0.3327223,
        size.height * 0.9039324);
    path_81.cubicTo(
        size.width * 0.3400393,
        size.height * 0.9292608,
        size.width * 0.3401866,
        size.height * 0.9581314,
        size.width * 0.3396777,
        size.height * 0.9726569);
    path_81.cubicTo(
        size.width * 0.3394616,
        size.height * 0.9795069,
        size.width * 0.3355946,
        size.height * 0.9855490,
        size.width * 0.3298027,
        size.height * 0.9880980);
    path_81.lineTo(size.width * 0.3297991, size.height * 0.9880980);
    path_81.cubicTo(
        size.width * 0.2976732,
        size.height * 1.002186,
        size.width * 0.2656598,
        size.height * 1.002284,
        size.width * 0.2337911,
        size.height * 0.9880980);
    path_81.cubicTo(
        size.width * 0.2279830,
        size.height * 0.9855294,
        size.width * 0.2242455,
        size.height * 0.9794892,
        size.width * 0.2239259,
        size.height * 0.9727961);
    path_81.lineTo(size.width * 0.2239241, size.height * 0.9727706);
    path_81.lineTo(size.width * 0.2239241, size.height * 0.9727706);
    path_81.cubicTo(
        size.width * 0.2233687,
        size.height * 0.9583020,
        size.width * 0.2233527,
        size.height * 0.9294029,
        size.width * 0.2305295,
        size.height * 0.9040333);
    path_81.cubicTo(
        size.width * 0.2341214,
        size.height * 0.8913373,
        size.width * 0.2395652,
        size.height * 0.8793422,
        size.width * 0.2477973,
        size.height * 0.8704990);
    path_81.close();
    path_81.moveTo(size.width * 0.2283839, size.height * 0.9725510);
    path_81.cubicTo(
        size.width * 0.2286205,
        size.height * 0.9774108,
        size.width * 0.2313286,
        size.height * 0.9817255,
        size.width * 0.2354616,
        size.height * 0.9835490);
    path_81.lineTo(size.width * 0.2354696, size.height * 0.9835490);
    path_81.cubicTo(
        size.width * 0.2662491,
        size.height * 0.9972647,
        size.width * 0.2970679,
        size.height * 0.9971667,
        size.width * 0.3281429,
        size.height * 0.9835490);
    path_81.cubicTo(
        size.width * 0.3322973,
        size.height * 0.9817157,
        size.width * 0.3350625,
        size.height * 0.9773902,
        size.width * 0.3352161,
        size.height * 0.9724833);
    path_81.lineTo(size.width * 0.3352170, size.height * 0.9724735);
    path_81.cubicTo(
        size.width * 0.3357205,
        size.height * 0.9580961,
        size.width * 0.3355446,
        size.height * 0.9299147,
        size.width * 0.3284670,
        size.height * 0.9054147);
    path_81.cubicTo(
        size.width * 0.3249321,
        size.height * 0.8931775,
        size.width * 0.3197250,
        size.height * 0.8820373,
        size.width * 0.3121696,
        size.height * 0.8739794);
    path_81.cubicTo(
        size.width * 0.3046696,
        size.height * 0.8659804,
        size.width * 0.2947580,
        size.height * 0.8609069,
        size.width * 0.2815241,
        size.height * 0.8609069);
    path_81.cubicTo(
        size.width * 0.2682688,
        size.height * 0.8609069,
        size.width * 0.2583768,
        size.height * 0.8659922,
        size.width * 0.2509196,
        size.height * 0.8740029);
    path_81.cubicTo(
        size.width * 0.2434063,
        size.height * 0.8820745,
        size.width * 0.2382598,
        size.height * 0.8932324,
        size.width * 0.2347929,
        size.height * 0.9054882);
    path_81.cubicTo(
        size.width * 0.2278536,
        size.height * 0.9300167,
        size.width * 0.2278348,
        size.height * 0.9582127,
        size.width * 0.2283839,
        size.height * 0.9725510);
    path_81.close();

    Paint paint_81_fill = Paint()..style = PaintingStyle.fill;
    paint_81_fill.color = Colors.black;
    canvas.drawPath(path_81, paint_81_fill);

    Path path_82 = Path()..fillType = PathFillType.evenOdd;
    path_82.moveTo(size.width * 0.3006063, size.height * 0.8625412);
    path_82.lineTo(size.width * 0.2631098, size.height * 0.8625412);
    path_82.lineTo(size.width * 0.2631098, size.height * 0.8346363);
    path_82.cubicTo(
        size.width * 0.2631098,
        size.height * 0.8319451,
        size.width * 0.2651366,
        size.height * 0.8297529,
        size.width * 0.2676241,
        size.height * 0.8297529);
    path_82.lineTo(size.width * 0.2961839, size.height * 0.8297529);
    path_82.cubicTo(
        size.width * 0.2986714,
        size.height * 0.8297529,
        size.width * 0.3006982,
        size.height * 0.8319451,
        size.width * 0.3006982,
        size.height * 0.8346363);
    path_82.lineTo(size.width * 0.3006982, size.height * 0.8625412);
    path_82.lineTo(size.width * 0.3006063, size.height * 0.8625412);
    path_82.close();

    Paint paint_82_fill = Paint()..style = PaintingStyle.fill;
    paint_82_fill.color = Colors.white;
    canvas.drawPath(path_82, paint_82_fill);

    Path path_83 = Path()..fillType = PathFillType.evenOdd;
    path_83.moveTo(size.width * 0.2675741, size.height * 0.8576392);
    path_83.lineTo(size.width * 0.2962339, size.height * 0.8576392);
    path_83.lineTo(size.width * 0.2962339, size.height * 0.8346706);
    path_83.cubicTo(
        size.width * 0.2962286,
        size.height * 0.8346667,
        size.width * 0.2962232,
        size.height * 0.8346627,
        size.width * 0.2962179,
        size.height * 0.8346608);
    path_83.cubicTo(
        size.width * 0.2962134,
        size.height * 0.8346578,
        size.width * 0.2962045,
        size.height * 0.8346549,
        size.width * 0.2961839,
        size.height * 0.8346549);
    path_83.lineTo(size.width * 0.2676241, size.height * 0.8346549);
    path_83.cubicTo(
        size.width * 0.2676036,
        size.height * 0.8346549,
        size.width * 0.2675946,
        size.height * 0.8346578,
        size.width * 0.2675902,
        size.height * 0.8346608);
    path_83.cubicTo(
        size.width * 0.2675848,
        size.height * 0.8346627,
        size.width * 0.2675786,
        size.height * 0.8346667,
        size.width * 0.2675741,
        size.height * 0.8346706);
    path_83.lineTo(size.width * 0.2675741, size.height * 0.8576392);
    path_83.close();
    path_83.moveTo(size.width * 0.3006982, size.height * 0.8625412);
    path_83.lineTo(size.width * 0.3006982, size.height * 0.8346363);
    path_83.cubicTo(
        size.width * 0.3006982,
        size.height * 0.8319451,
        size.width * 0.2986714,
        size.height * 0.8297529,
        size.width * 0.2961839,
        size.height * 0.8297529);
    path_83.lineTo(size.width * 0.2676241, size.height * 0.8297529);
    path_83.cubicTo(
        size.width * 0.2651366,
        size.height * 0.8297529,
        size.width * 0.2631098,
        size.height * 0.8319451,
        size.width * 0.2631098,
        size.height * 0.8346363);
    path_83.lineTo(size.width * 0.2631098, size.height * 0.8625412);
    path_83.lineTo(size.width * 0.3006982, size.height * 0.8625412);
    path_83.close();

    Paint paint_83_fill = Paint()..style = PaintingStyle.fill;
    paint_83_fill.color = Colors.black;
    canvas.drawPath(path_83, paint_83_fill);

    Path path_84 = Path()..fillType = PathFillType.evenOdd;
    path_84.moveTo(size.width * 0.2045080, size.height * 0.9883333);
    path_84.cubicTo(
        size.width * 0.1895830,
        size.height * 0.9960098,
        size.width * 0.1730000,
        size.height * 0.9969118,
        size.width * 0.1542054,
        size.height * 0.9883333);
    path_84.lineTo(size.width * 0.1542054, size.height * 0.9518608);
    path_84.lineTo(size.width * 0.2045080, size.height * 0.9518608);
    path_84.lineTo(size.width * 0.2045080, size.height * 0.9883333);
    path_84.close();

    Paint paint_84_fill = Paint()..style = PaintingStyle.fill;
    paint_84_fill.color = Colors.white;
    canvas.drawPath(path_84, paint_84_fill);

    Path path_85 = Path()..fillType = PathFillType.evenOdd;
    path_85.moveTo(size.width * 0.1519732, size.height * 0.9518608);
    path_85.cubicTo(
        size.width * 0.1519732,
        size.height * 0.9505078,
        size.width * 0.1529723,
        size.height * 0.9494098,
        size.width * 0.1542054,
        size.height * 0.9494098);
    path_85.lineTo(size.width * 0.2045080, size.height * 0.9494098);
    path_85.cubicTo(
        size.width * 0.2057411,
        size.height * 0.9494098,
        size.width * 0.2067402,
        size.height * 0.9505078,
        size.width * 0.2067402,
        size.height * 0.9518608);
    path_85.lineTo(size.width * 0.2067402, size.height * 0.9883333);
    path_85.cubicTo(
        size.width * 0.2067402,
        size.height * 0.9892843,
        size.width * 0.2062393,
        size.height * 0.9901569,
        size.width * 0.2054545,
        size.height * 0.9905588);
    path_85.cubicTo(
        size.width * 0.1899321,
        size.height * 0.9985392,
        size.width * 0.1726929,
        size.height * 0.9994216,
        size.width * 0.1533491,
        size.height * 0.9905980);
    path_85.cubicTo(
        size.width * 0.1525161,
        size.height * 0.9902255,
        size.width * 0.1519732,
        size.height * 0.9893235,
        size.width * 0.1519732,
        size.height * 0.9883333);
    path_85.lineTo(size.width * 0.1519732, size.height * 0.9518608);
    path_85.close();
    path_85.moveTo(size.width * 0.1564375, size.height * 0.9543118);
    path_85.lineTo(size.width * 0.1564375, size.height * 0.9866863);
    path_85.cubicTo(
        size.width * 0.1735679,
        size.height * 0.9941078,
        size.width * 0.1886464,
        size.height * 0.9933529,
        size.width * 0.2022759,
        size.height * 0.9867549);
    path_85.lineTo(size.width * 0.2022759, size.height * 0.9543118);
    path_85.lineTo(size.width * 0.1564375, size.height * 0.9543118);
    path_85.close();

    Paint paint_85_fill = Paint()..style = PaintingStyle.fill;
    paint_85_fill.color = Colors.black;
    canvas.drawPath(path_85, paint_85_fill);

    Path path_86 = Path()..fillType = PathFillType.evenOdd;
    path_86.moveTo(size.width * 0.1989813, size.height * 0.9580373);
    path_86.cubicTo(
        size.width * 0.1856223,
        size.height * 0.9627216,
        size.width * 0.1720795,
        size.height * 0.9625225,
        size.width * 0.1584437,
        size.height * 0.9578382);
    path_86.cubicTo(
        size.width * 0.1504286,
        size.height * 0.9550480,
        size.width * 0.1445321,
        size.height * 0.9474735,
        size.width * 0.1436107,
        size.height * 0.9384049);
    path_86.lineTo(size.width * 0.1252777, size.height * 0.7568225);
    path_86.cubicTo(
        size.width * 0.1250009,
        size.height * 0.7542314,
        size.width * 0.1264750,
        size.height * 0.7517392,
        size.width * 0.1287786,
        size.height * 0.7510422);
    path_86.cubicTo(
        size.width * 0.1624973,
        size.height * 0.7395804,
        size.width * 0.1962170,
        size.height * 0.7395804,
        size.width * 0.2299366,
        size.height * 0.7510422);
    path_86.cubicTo(
        size.width * 0.2322402,
        size.height * 0.7518392,
        size.width * 0.2337143,
        size.height * 0.7543304,
        size.width * 0.2334375,
        size.height * 0.7569216);
    path_86.lineTo(size.width * 0.2136295, size.height * 0.9389029);
    path_86.cubicTo(
        size.width * 0.2127080,
        size.height * 0.9477725,
        size.width * 0.2069045,
        size.height * 0.9552471,
        size.width * 0.1989813,
        size.height * 0.9580373);
    path_86.close();

    Paint paint_86_fill = Paint()..style = PaintingStyle.fill;
    paint_86_fill.color = Colors.white;
    canvas.drawPath(path_86, paint_86_fill);

    Path path_87 = Path()..fillType = PathFillType.evenOdd;
    path_87.moveTo(size.width * 0.2292687, size.height * 0.7533814);
    path_87.cubicTo(
        size.width * 0.1959821,
        size.height * 0.7420686,
        size.width * 0.1627250,
        size.height * 0.7420686,
        size.width * 0.1294375,
        size.height * 0.7533833);
    path_87.lineTo(size.width * 0.1293714, size.height * 0.7534059);
    path_87.cubicTo(
        size.width * 0.1281634,
        size.height * 0.7537716,
        size.width * 0.1273411,
        size.height * 0.7551088,
        size.width * 0.1274937,
        size.height * 0.7565373);
    path_87.lineTo(size.width * 0.1274946, size.height * 0.7565520);
    path_87.lineTo(size.width * 0.1458286, size.height * 0.9381324);
    path_87.cubicTo(
        size.width * 0.1466473,
        size.height * 0.9461892,
        size.width * 0.1518991,
        size.height * 0.9529882,
        size.width * 0.1591170,
        size.height * 0.9555020);
    path_87.cubicTo(
        size.width * 0.1723402,
        size.height * 0.9600431,
        size.width * 0.1854098,
        size.height * 0.9602225,
        size.width * 0.1983009,
        size.height * 0.9557029);
    path_87.cubicTo(
        size.width * 0.2054348,
        size.height * 0.9531892,
        size.width * 0.2105946,
        size.height * 0.9464833,
        size.width * 0.2114107,
        size.height * 0.9386245);
    path_87.lineTo(size.width * 0.2114125, size.height * 0.9386118);
    path_87.lineTo(size.width * 0.2312196, size.height * 0.7566363);
    path_87.cubicTo(
        size.width * 0.2312196,
        size.height * 0.7566363,
        size.width * 0.2312196,
        size.height * 0.7566373,
        size.width * 0.2312196,
        size.height * 0.7566363);
    path_87.cubicTo(
        size.width * 0.2313687,
        size.height * 0.7552314,
        size.width * 0.2305616,
        size.height * 0.7538314,
        size.width * 0.2292687,
        size.height * 0.7533814);
    path_87.close();
    path_87.moveTo(size.width * 0.1281473, size.height * 0.7486902);
    path_87.cubicTo(
        size.width * 0.1622866,
        size.height * 0.7370931,
        size.width * 0.1964562,
        size.height * 0.7370971,
        size.width * 0.2305955,
        size.height * 0.7487000);
    path_87.lineTo(size.width * 0.2306071, size.height * 0.7487039);
    path_87.cubicTo(
        size.width * 0.2339179,
        size.height * 0.7498500,
        size.width * 0.2360563,
        size.height * 0.7534314,
        size.width * 0.2356536,
        size.height * 0.7572069);
    path_87.lineTo(size.width * 0.2158464, size.height * 0.9391804);
    path_87.cubicTo(
        size.width * 0.2158464,
        size.height * 0.9391824,
        size.width * 0.2158464,
        size.height * 0.9391784,
        size.width * 0.2158464,
        size.height * 0.9391804);
    path_87.cubicTo(
        size.width * 0.2148179,
        size.height * 0.9490578,
        size.width * 0.2083705,
        size.height * 0.9573039,
        size.width * 0.1996616,
        size.height * 0.9603716);
    path_87.cubicTo(
        size.width * 0.1858375,
        size.height * 0.9652186,
        size.width * 0.1718205,
        size.height * 0.9650020,
        size.width * 0.1577768,
        size.height * 0.9601775);
    path_87.cubicTo(
        size.width * 0.1489643,
        size.height * 0.9571098,
        size.width * 0.1424161,
        size.height * 0.9487569,
        size.width * 0.1413920,
        size.height * 0.9386765);
    path_87.lineTo(size.width * 0.1230598, size.height * 0.7571069);
    path_87.cubicTo(
        size.width * 0.1230589,
        size.height * 0.7571049,
        size.width * 0.1230598,
        size.height * 0.7571098,
        size.width * 0.1230598,
        size.height * 0.7571069);
    path_87.cubicTo(
        size.width * 0.1226643,
        size.height * 0.7533686,
        size.width * 0.1247723,
        size.height * 0.7497333,
        size.width * 0.1281473,
        size.height * 0.7486902);
    path_87.close();

    Paint paint_87_fill = Paint()..style = PaintingStyle.fill;
    paint_87_fill.color = Colors.black;
    canvas.drawPath(path_87, paint_87_fill);

    Path path_88 = Path()..fillType = PathFillType.evenOdd;
    path_88.moveTo(size.width * 0.08819250, size.height * 0.9909902);
    path_88.cubicTo(
        size.width * 0.07326750,
        size.height * 0.9986667,
        size.width * 0.05668420,
        size.height * 0.9995588,
        size.width * 0.03788973,
        size.height * 0.9909902);
    path_88.lineTo(size.width * 0.03788973, size.height * 0.9545137);
    path_88.lineTo(size.width * 0.08819250, size.height * 0.9545137);
    path_88.lineTo(size.width * 0.08819250, size.height * 0.9909902);
    path_88.close();

    Paint paint_88_fill = Paint()..style = PaintingStyle.fill;
    paint_88_fill.color = Colors.white;
    canvas.drawPath(path_88, paint_88_fill);

    Path path_89 = Path()..fillType = PathFillType.evenOdd;
    path_89.moveTo(size.width * 0.03565759, size.height * 0.9545137);
    path_89.cubicTo(
        size.width * 0.03565759,
        size.height * 0.9531598,
        size.width * 0.03665696,
        size.height * 0.9520627,
        size.width * 0.03788973,
        size.height * 0.9520627);
    path_89.lineTo(size.width * 0.08819250, size.height * 0.9520627);
    path_89.cubicTo(
        size.width * 0.08942500,
        size.height * 0.9520627,
        size.width * 0.09042500,
        size.height * 0.9531598,
        size.width * 0.09042500,
        size.height * 0.9545137);
    path_89.lineTo(size.width * 0.09042500, size.height * 0.9909902);
    path_89.cubicTo(
        size.width * 0.09042500,
        size.height * 0.9919412,
        size.width * 0.08992321,
        size.height * 0.9928039,
        size.width * 0.08913911,
        size.height * 0.9932059);
    path_89.cubicTo(
        size.width * 0.07361661,
        size.height * 1.001186,
        size.width * 0.05637768,
        size.height * 1.002078,
        size.width * 0.03703357,
        size.height * 0.9932549);
    path_89.cubicTo(
        size.width * 0.03620054,
        size.height * 0.9928725,
        size.width * 0.03565759,
        size.height * 0.9919804,
        size.width * 0.03565759,
        size.height * 0.9909902);
    path_89.lineTo(size.width * 0.03565759, size.height * 0.9545137);
    path_89.close();
    path_89.moveTo(size.width * 0.04012187, size.height * 0.9569647);
    path_89.lineTo(size.width * 0.04012187, size.height * 0.9893333);
    path_89.cubicTo(
        size.width * 0.05725268,
        size.height * 0.9967647,
        size.width * 0.07233054,
        size.height * 0.9960098,
        size.width * 0.08596036,
        size.height * 0.9894118);
    path_89.lineTo(size.width * 0.08596036, size.height * 0.9569647);
    path_89.lineTo(size.width * 0.04012187, size.height * 0.9569647);
    path_89.close();

    Paint paint_89_fill = Paint()..style = PaintingStyle.fill;
    paint_89_fill.color = Colors.black;
    canvas.drawPath(path_89, paint_89_fill);

    Path path_90 = Path()..fillType = PathFillType.evenOdd;
    path_90.moveTo(size.width * 0.08266545, size.height * 0.9606892);
    path_90.cubicTo(
        size.width * 0.06930670,
        size.height * 0.9653735,
        size.width * 0.05576357,
        size.height * 0.9651745,
        size.width * 0.04212839,
        size.height * 0.9604902);
    path_90.cubicTo(
        size.width * 0.03411313,
        size.height * 0.9577000,
        size.width * 0.02821687,
        size.height * 0.9501255,
        size.width * 0.02729554,
        size.height * 0.9410569);
    path_90.lineTo(size.width * 0.008961786, size.height * 0.7594745);
    path_90.cubicTo(
        size.width * 0.008685384,
        size.height * 0.7568833,
        size.width * 0.01015946,
        size.height * 0.7543912,
        size.width * 0.01246268,
        size.height * 0.7536941);
    path_90.cubicTo(
        size.width * 0.04618214,
        size.height * 0.7422324,
        size.width * 0.07990161,
        size.height * 0.7422324,
        size.width * 0.1136214,
        size.height * 0.7536941);
    path_90.cubicTo(
        size.width * 0.1159241,
        size.height * 0.7544912,
        size.width * 0.1173982,
        size.height * 0.7569824,
        size.width * 0.1171223,
        size.height * 0.7595735);
    path_90.lineTo(size.width * 0.09731429, size.height * 0.9415549);
    path_90.cubicTo(
        size.width * 0.09639286,
        size.height * 0.9504245,
        size.width * 0.09058839,
        size.height * 0.9578990,
        size.width * 0.08266545,
        size.height * 0.9606892);
    path_90.close();

    Paint paint_90_fill = Paint()..style = PaintingStyle.fill;
    paint_90_fill.color = Colors.white;
    canvas.drawPath(path_90, paint_90_fill);

    Path path_91 = Path()..fillType = PathFillType.evenOdd;
    path_91.moveTo(size.width * 0.1129536, size.height * 0.7560333);
    path_91.cubicTo(
        size.width * 0.07966625,
        size.height * 0.7447206,
        size.width * 0.04640911,
        size.height * 0.7447206,
        size.width * 0.01312179,
        size.height * 0.7560353);
    path_91.lineTo(size.width * 0.01305562, size.height * 0.7560578);
    path_91.cubicTo(
        size.width * 0.01184786,
        size.height * 0.7564235,
        size.width * 0.01102554,
        size.height * 0.7577618,
        size.width * 0.01117777,
        size.height * 0.7591892);
    path_91.lineTo(size.width * 0.01117938, size.height * 0.7592039);
    path_91.lineTo(size.width * 0.02951304, size.height * 0.9407843);
    path_91.cubicTo(
        size.width * 0.03033143,
        size.height * 0.9488412,
        size.width * 0.03558330,
        size.height * 0.9556402,
        size.width * 0.04280098,
        size.height * 0.9581539);
    path_91.cubicTo(
        size.width * 0.05602473,
        size.height * 0.9626951,
        size.width * 0.06909464,
        size.height * 0.9628745,
        size.width * 0.08198545,
        size.height * 0.9583549);
    path_91.cubicTo(
        size.width * 0.08911938,
        size.height * 0.9558412,
        size.width * 0.09427946,
        size.height * 0.9491353,
        size.width * 0.09509554,
        size.height * 0.9412775);
    path_91.lineTo(size.width * 0.09509643, size.height * 0.9412637);
    path_91.lineTo(size.width * 0.1149036, size.height * 0.7592882);
    path_91.cubicTo(
        size.width * 0.1149045,
        size.height * 0.7592882,
        size.width * 0.1149036,
        size.height * 0.7592892,
        size.width * 0.1149036,
        size.height * 0.7592882);
    path_91.cubicTo(
        size.width * 0.1150527,
        size.height * 0.7578833,
        size.width * 0.1142455,
        size.height * 0.7564833,
        size.width * 0.1129536,
        size.height * 0.7560333);
    path_91.close();
    path_91.moveTo(size.width * 0.01183179, size.height * 0.7513422);
    path_91.cubicTo(
        size.width * 0.04597116,
        size.height * 0.7397451,
        size.width * 0.08014071,
        size.height * 0.7397490,
        size.width * 0.1142804,
        size.height * 0.7513520);
    path_91.lineTo(size.width * 0.1142911, size.height * 0.7513559);
    path_91.cubicTo(
        size.width * 0.1176027,
        size.height * 0.7525020,
        size.width * 0.1197411,
        size.height * 0.7560833,
        size.width * 0.1193384,
        size.height * 0.7598588);
    path_91.lineTo(size.width * 0.09953125, size.height * 0.9418324);
    path_91.cubicTo(
        size.width * 0.09953036,
        size.height * 0.9418343,
        size.width * 0.09953125,
        size.height * 0.9418304,
        size.width * 0.09953125,
        size.height * 0.9418324);
    path_91.cubicTo(
        size.width * 0.09850268,
        size.height * 0.9517098,
        size.width * 0.09205536,
        size.height * 0.9599559,
        size.width * 0.08334625,
        size.height * 0.9630235);
    path_91.cubicTo(
        size.width * 0.06952179,
        size.height * 0.9678706,
        size.width * 0.05550518,
        size.height * 0.9676539,
        size.width * 0.04146098,
        size.height * 0.9628294);
    path_91.cubicTo(
        size.width * 0.03264866,
        size.height * 0.9597618,
        size.width * 0.02610036,
        size.height * 0.9514098,
        size.width * 0.02507625,
        size.height * 0.9413284);
    path_91.lineTo(size.width * 0.006743830, size.height * 0.7597588);
    path_91.cubicTo(
        size.width * 0.006743536,
        size.height * 0.7597569,
        size.width * 0.006744116,
        size.height * 0.7597618,
        size.width * 0.006743830,
        size.height * 0.7597588);
    path_91.cubicTo(
        size.width * 0.006348732,
        size.height * 0.7560206,
        size.width * 0.008456714,
        size.height * 0.7523863,
        size.width * 0.01183179,
        size.height * 0.7513422);
    path_91.close();

    Paint paint_91_fill = Paint()..style = PaintingStyle.fill;
    paint_91_fill.color = Colors.black;
    canvas.drawPath(path_91, paint_91_fill);

    Path path_92 = Path()..fillType = PathFillType.evenOdd;
    path_92.moveTo(size.width * 0.9513304, size.height * 0.7483343);
    path_92.lineTo(size.width * 0.9107946, size.height * 0.7483343);
    path_92.cubicTo(
        size.width * 0.9013036,
        size.height * 0.7483343,
        size.width * 0.8936518,
        size.height * 0.7566059,
        size.width * 0.8936518,
        size.height * 0.7668706);
    path_92.lineTo(size.width * 0.8936518, size.height * 0.8402216);
    path_92.lineTo(size.width * 0.9107946, size.height * 0.8402216);
    path_92.lineTo(size.width * 0.9107946, size.height * 0.7718539);
    path_92.cubicTo(
        size.width * 0.9107946,
        size.height * 0.7691627,
        size.width * 0.9128214,
        size.height * 0.7669706,
        size.width * 0.9153036,
        size.height * 0.7669706);
    path_92.lineTo(size.width * 0.9512321, size.height * 0.7669706);
    path_92.lineTo(size.width * 0.9512321, size.height * 0.7483343);
    path_92.lineTo(size.width * 0.9513304, size.height * 0.7483343);
    path_92.close();

    Paint paint_92_fill = Paint()..style = PaintingStyle.fill;
    paint_92_fill.color = Colors.white;
    canvas.drawPath(path_92, paint_92_fill);

    Path path_93 = Path()..fillType = PathFillType.evenOdd;
    path_93.moveTo(size.width * 0.8914232, size.height * 0.7668706);
    path_93.cubicTo(
        size.width * 0.8914232,
        size.height * 0.7552176,
        size.width * 0.9000982,
        size.height * 0.7458833,
        size.width * 0.9107946,
        size.height * 0.7458833);
    path_93.lineTo(size.width * 0.9513304, size.height * 0.7458833);
    path_93.cubicTo(
        size.width * 0.9525625,
        size.height * 0.7458833,
        size.width * 0.9535625,
        size.height * 0.7469804,
        size.width * 0.9535625,
        size.height * 0.7483343);
    path_93.cubicTo(
        size.width * 0.9535625,
        size.height * 0.7485775,
        size.width * 0.9535268,
        size.height * 0.7488118,
        size.width * 0.9534643,
        size.height * 0.7490333);
    path_93.lineTo(size.width * 0.9534643, size.height * 0.7669706);
    path_93.cubicTo(
        size.width * 0.9534643,
        size.height * 0.7683245,
        size.width * 0.9524643,
        size.height * 0.7694216,
        size.width * 0.9512321,
        size.height * 0.7694216);
    path_93.lineTo(size.width * 0.9153036, size.height * 0.7694216);
    path_93.cubicTo(
        size.width * 0.9140179,
        size.height * 0.7694216,
        size.width * 0.9130268,
        size.height * 0.7705510,
        size.width * 0.9130268,
        size.height * 0.7718539);
    path_93.lineTo(size.width * 0.9130268, size.height * 0.8402216);
    path_93.cubicTo(
        size.width * 0.9130268,
        size.height * 0.8415755,
        size.width * 0.9120268,
        size.height * 0.8426725,
        size.width * 0.9107946,
        size.height * 0.8426725);
    path_93.lineTo(size.width * 0.8936518, size.height * 0.8426725);
    path_93.cubicTo(
        size.width * 0.8924223,
        size.height * 0.8426725,
        size.width * 0.8914232,
        size.height * 0.8415755,
        size.width * 0.8914232,
        size.height * 0.8402216);
    path_93.lineTo(size.width * 0.8914232, size.height * 0.7668706);
    path_93.close();
    path_93.moveTo(size.width * 0.9490000, size.height * 0.7507853);
    path_93.lineTo(size.width * 0.9107946, size.height * 0.7507853);
    path_93.cubicTo(
        size.width * 0.9025000,
        size.height * 0.7507853,
        size.width * 0.8958839,
        size.height * 0.7579941,
        size.width * 0.8958839,
        size.height * 0.7668706);
    path_93.lineTo(size.width * 0.8958839, size.height * 0.8377706);
    path_93.lineTo(size.width * 0.9085625, size.height * 0.8377706);
    path_93.lineTo(size.width * 0.9085625, size.height * 0.7718539);
    path_93.cubicTo(
        size.width * 0.9085625,
        size.height * 0.7677755,
        size.width * 0.9116161,
        size.height * 0.7645196,
        size.width * 0.9153036,
        size.height * 0.7645196);
    path_93.lineTo(size.width * 0.9490000, size.height * 0.7645196);
    path_93.lineTo(size.width * 0.9490000, size.height * 0.7507853);
    path_93.close();

    Paint paint_93_fill = Paint()..style = PaintingStyle.fill;
    paint_93_fill.color = Colors.black;
    canvas.drawPath(path_93, paint_93_fill);

    Path path_94 = Path()..fillType = PathFillType.evenOdd;
    path_94.moveTo(size.width * 0.9300357, size.height * 0.9883333);
    path_94.cubicTo(
        size.width * 0.9114286,
        size.height * 0.9962059,
        size.width * 0.8928152,
        size.height * 0.9961078,
        size.width * 0.8741134,
        size.height * 0.9883333);
    path_94.cubicTo(
        size.width * 0.8681250,
        size.height * 0.9858431,
        size.width * 0.8639786,
        size.height * 0.9798598,
        size.width * 0.8634259,
        size.height * 0.9728833);
    path_94.lineTo(size.width * 0.8514491, size.height * 0.8286745);
    path_94.cubicTo(
        size.width * 0.8511732,
        size.height * 0.8248873,
        size.width * 0.8542134,
        size.height * 0.8217980,
        size.width * 0.8576214,
        size.height * 0.8224951);
    path_94.cubicTo(
        size.width * 0.8862741,
        size.height * 0.8281765,
        size.width * 0.9306786,
        size.height * 0.8236912,
        size.width * 0.9471696,
        size.height * 0.8216980);
    path_94.cubicTo(
        size.width * 0.9504911,
        size.height * 0.8213000,
        size.width * 0.9533482,
        size.height * 0.8242892,
        size.width * 0.9530714,
        size.height * 0.8279765);
    path_94.lineTo(size.width * 0.9406339, size.height * 0.9730833);
    path_94.cubicTo(
        size.width * 0.9400804,
        size.height * 0.9798598,
        size.width * 0.9359286,
        size.height * 0.9857353,
        size.width * 0.9300357,
        size.height * 0.9883333);
    path_94.close();

    Paint paint_94_fill = Paint()..style = PaintingStyle.fill;
    paint_94_fill.color = Colors.white;
    canvas.drawPath(path_94, paint_94_fill);

    Path path_95 = Path()..fillType = PathFillType.evenOdd;
    path_95.moveTo(size.width * 0.9508482, size.height * 0.8277637);
    path_95.cubicTo(
        size.width * 0.9510000,
        size.height * 0.8256402,
        size.width * 0.9493571,
        size.height * 0.8239020,
        size.width * 0.9474107,
        size.height * 0.8241343);
    path_95.cubicTo(
        size.width * 0.9309732,
        size.height * 0.8261225,
        size.width * 0.8862464,
        size.height * 0.8306618,
        size.width * 0.8572250,
        size.height * 0.8249078);
    path_95.lineTo(size.width * 0.8572134, size.height * 0.8249049);
    path_95.cubicTo(
        size.width * 0.8552929,
        size.height * 0.8245118,
        size.width * 0.8535187,
        size.height * 0.8262490,
        size.width * 0.8536732,
        size.height * 0.8284676);
    path_95.lineTo(size.width * 0.8656500, size.height * 0.9726716);
    path_95.cubicTo(
        size.width * 0.8661277,
        size.height * 0.9787010,
        size.width * 0.8697098,
        size.height * 0.9838824,
        size.width * 0.8749045,
        size.height * 0.9860392);
    path_95.cubicTo(
        size.width * 0.8930982,
        size.height * 0.9935980,
        size.width * 0.9111429,
        size.height * 0.9936961,
        size.width * 0.9292232,
        size.height * 0.9860490);
    path_95.cubicTo(
        size.width * 0.9343750,
        size.height * 0.9837745,
        size.width * 0.9379375,
        size.height * 0.9786647,
        size.width * 0.9384107,
        size.height * 0.9728647);
    path_95.lineTo(size.width * 0.9384107, size.height * 0.9728539);
    path_95.lineTo(size.width * 0.9508482, size.height * 0.8277637);
    path_95.close();
    path_95.moveTo(size.width * 0.9469286, size.height * 0.8192618);
    path_95.cubicTo(
        size.width * 0.9516161,
        size.height * 0.8186980,
        size.width * 0.9556875,
        size.height * 0.8229314,
        size.width * 0.9552946,
        size.height * 0.8281784);
    path_95.lineTo(size.width * 0.9552946, size.height * 0.8282069);
    path_95.lineTo(size.width * 0.9428571, size.height * 0.9733020);
    path_95.cubicTo(
        size.width * 0.9428571,
        size.height * 0.9733039,
        size.width * 0.9428571,
        size.height * 0.9733000,
        size.width * 0.9428571,
        size.height * 0.9733020);
    path_95.cubicTo(
        size.width * 0.9422232,
        size.height * 0.9810490,
        size.width * 0.9374911,
        size.height * 0.9876961,
        size.width * 0.9308661,
        size.height * 0.9906078);
    path_95.lineTo(size.width * 0.9308393, size.height * 0.9906176);
    path_95.cubicTo(
        size.width * 0.9117054,
        size.height * 0.9987157,
        size.width * 0.8925259,
        size.height * 0.9986078,
        size.width * 0.8733232,
        size.height * 0.9906275);
    path_95.cubicTo(
        size.width * 0.8665420,
        size.height * 0.9878039,
        size.width * 0.8618321,
        size.height * 0.9810196,
        size.width * 0.8612027,
        size.height * 0.9731020);
    path_95.cubicTo(
        size.width * 0.8612027,
        size.height * 0.9731000,
        size.width * 0.8612027,
        size.height * 0.9731039,
        size.width * 0.8612027,
        size.height * 0.9731020);
    path_95.lineTo(size.width * 0.8492241, size.height * 0.8288706);
    path_95.cubicTo(
        size.width * 0.8488339,
        size.height * 0.8235225,
        size.width * 0.8531330,
        size.height * 0.8190882,
        size.width * 0.8580241,
        size.height * 0.8200843);
    path_95.cubicTo(
        size.width * 0.8863080,
        size.height * 0.8256902,
        size.width * 0.9303929,
        size.height * 0.8212608,
        size.width * 0.9469286,
        size.height * 0.8192618);
    path_95.close();

    Paint paint_95_fill = Paint()..style = PaintingStyle.fill;
    paint_95_fill.color = Colors.black;
    canvas.drawPath(path_95, paint_95_fill);
  }

  @override
  bool shouldRepaint(covariant TimeServiceCustomPainter oldDelegate) {
    return oldDelegate.active != active;
  }
}

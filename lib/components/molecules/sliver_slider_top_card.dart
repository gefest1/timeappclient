import 'package:flutter/material.dart';
import 'package:li/data/models/institution_minute.dart';
import 'package:li/utils/text_styles.dart';
import 'package:time_app_components/utils/testStyles.dart';

class SliverSliderTopCard extends StatefulWidget {
  final double imageHeight;
  final ScrollController scrollController;
  final InstitutionsMinute institution;

  const SliverSliderTopCard({
    required this.imageHeight,
    required this.scrollController,
    required this.institution,
    Key? key,
  }) : super(key: key);

  @override
  _SliverSliderTopCardState createState() => _SliverSliderTopCardState();
}

class _SliverSliderTopCardState extends State<SliverSliderTopCard> {
  final PageController pageController = PageController(initialPage: 0);
  @override
  void initState() {
    super.initState();
    widget.scrollController.addListener(() {
      setState(
        () {},
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    final _topPadding = MediaQuery.of(context).padding.top;
    final double val;
    if (widget.scrollController.offset < 0) {
      val = -widget.scrollController.offset;
    } else {
      val = 0;
    }

    return SliverPersistentHeader(
      pinned: true,
      floating: true,
      delegate: SliverHeaderTop(
        SizedBox(
          height: widget.imageHeight,
          child: Stack(
            clipBehavior: Clip.none,
            children: [
              Positioned(
                left: 0,
                right: 0,
                top: -widget.scrollController.offset - val,
                child: Stack(
                  children: [
                    SizedBox(
                      height: widget.imageHeight + val,
                      width: double.infinity,
                      child: PageView.builder(
                        controller: pageController,
                        physics: ClampingScrollPhysics(),
                        itemCount: widget.institution.galleryURLs?.length,
                        itemBuilder: (context, index) => Image.network(
                          widget.institution.galleryURLs![index]!.xl!,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Positioned(
                      bottom: 10,
                      left: 0,
                      right: 20,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          height: 30,
                          decoration: BoxDecoration(
                            color: Colors.black54,
                            borderRadius: BorderRadius.circular(6),
                          ),
                          padding: const EdgeInsets.symmetric(
                            horizontal: 5,
                            vertical: 6,
                          ),
                          child: AnimatedBuilder(
                            animation: pageController,
                            builder: (ctx, _) {
                              return Text(
                                '${(pageController.page ?? 0).round() + 1} из ${widget.institution.galleryURLs?.length ?? 0}',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: P4TextStyle.fontSize,
                                  fontWeight: P4TextStyle.fontWeight,
                                  height: P4TextStyle.height,
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        maxExtent: widget.imageHeight,
        minExtent: 56 + _topPadding,
      ),
    );
  }
}

class SliverHeaderTop extends SliverPersistentHeaderDelegate {
  final Widget child;

  final double minExtent;
  final double maxExtent;

  const SliverHeaderTop(
    this.child, {
    this.minExtent = 56,
    this.maxExtent = 100,
  });

  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) =>
      true;
  @override
  Widget build(BuildContext context, _, __) {
    return child;
  }
}

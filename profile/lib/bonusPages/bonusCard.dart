import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/components/molecules/sliver_footer.dart';
import 'package:li/const.dart';
import 'package:li/logic/blocs/bonus/bonus_bloc.dart';
import 'package:li/logic/blocs/get_debt/getdebt_bloc.dart';
import 'package:profile/bonusPages/Service_head.dart';
import 'package:profile/bonusPages/bottom_sheet_bonus.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/components/molecules/CustomTextField.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bloc/bloc.dart';

class BonusCard extends StatefulWidget {
  const BonusCard({Key? key}) : super(key: key);

  @override
  State<BonusCard> createState() => _BonusCardState();
}

class _BonusCardState extends State<BonusCard> {
  final TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: InkWell(
        onTap: () {
          showModalBottomSheet(
            isScrollControlled: true,
            context: context,
            backgroundColor: Colors.transparent,
            builder: (context) {
              return const BonnusBottomSheet();
            },
          );
        },
        child: Container(
          decoration: BoxDecoration(
            image: const DecorationImage(
              image: AssetImage(
                'assets/Bonus.png',
              ),
              fit: BoxFit.cover,
            ),
            color: ColorData.clientsButtonColorDefault,
            borderRadius: BorderRadius.circular(15),
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              // ignore: prefer_const_literals_to_create_immutables
              children: [
                const Text(
                  'Промокоды и баллы',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                  ),
                ),
                const SizedBox(height: 12),
                BlocBuilder<GetdebtBloc, GetdebtState>(
                  builder: (context, state) {
                    final double bonus;
                    if (state is DebtState || state is NoDebtState) {
                      bonus = (state as dynamic).client.bonus ?? 0;
                    } else {
                      bonus = 0;
                    }

                    return Text(
                      bonus.toStringAsFixed(0),
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: H1TextStyle.fontSize,
                        height: H1TextStyle.height,
                        fontWeight: H1TextStyle.fontWeight,
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

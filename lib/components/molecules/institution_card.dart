import 'package:flutter/material.dart';
import 'package:li/components/atoms/little_info_card.dart';
import 'package:li/components/atoms/tall_info_card.dart';
import 'package:li/utils/parallax.dart';
import 'package:take_away/data/models/institution_take_away.dart';

class InstitutionCard extends StatefulWidget {
  final InstitutionTakeAway institutionTakeAway;

  const InstitutionCard({
    required this.institutionTakeAway,
    Key? key,
  }) : super(key: key);

  @override
  State<InstitutionCard> createState() => _InstitutionCardState();
}

class _InstitutionCardState extends State<InstitutionCard> {
  @override
  Widget build(BuildContext context) {
    final _media = MediaQuery.of(context);
    return Stack(
      children: [
        ClipPath(
          clipper: RoundClipper(),
          child: SizedBox(
            height: 288 + _media.padding.top,
            width: double.infinity,
            child: Image.network(
              widget.institutionTakeAway.photoURL!.xl!,
              fit: BoxFit.cover,
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 140 + _media.padding.top,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  LittleInfoCard(
                    color: Color(0xff007AFF),
                    assetString: 'assets/svg/icon/star.svg',
                    title: '5,0',
                  ),
                  const SizedBox(height: 2),
                  Text(
                    'Рейтинг',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      height: 17 / 14,
                      color: Colors.white,
                    ),
                  ),
                  const SizedBox(height: 10),
                  TallInfoCard(
                    assetString: 'assets/svg/icon/verifiedBadge.svg',
                    title: '0',
                    subTitle: 'subTitle',
                  )
                ],
              ),
              const SizedBox(width: 10),
              Container(
                height: 210,
                width: 160,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color(0xFFFAFF00),
                      Color(0xFF00C2FF),
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                  ),
                  borderRadius: const BorderRadius.all(
                    Radius.circular(20),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Parallax(
                      child: Image.network(
                        widget.institutionTakeAway.specialist!.photoURL!.xl!,
                        fit: BoxFit.cover,
                        height: 200,
                        width: 150,
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(width: 10),
              Column(
                children: [
                  LittleInfoCard(
                    color: Color(0xffECC600),
                    assetString: 'assets/svg/icon/bullHorn.svg',
                    title: '0',
                  ),
                  const SizedBox(height: 2),
                  Text(
                    'Отзывов',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      height: 17 / 14,
                      color: Colors.white,
                    ),
                  ),
                  const SizedBox(height: 10),
                  TallInfoCard(
                    assetString: 'assets/svg/icon/motorcycle.svg',
                    title: '100%',
                    subTitle: 'subTitle',
                  ),
                ],
              ),
            ],
          ),
        )
      ],
    );
  }
}

class RoundClipper extends CustomClipper<Path> {
  Path _getPath(Path path, Offset xy1, Offset xy2, Offset xy3) {
    return path..cubicTo(xy1.dx, xy1.dy, xy2.dx, xy2.dy, xy3.dx, xy3.dy);
  }

  @override
  Path getClip(Size size) {
    final _path = Path();

    final _grad = 0.96;
    final _height = 0.9;
    final firstGrad = 0.16;
    final secondGrad = 0.32;

    _path.moveTo(0, size.height * _height);

    _getPath(
      _path,
      Offset(firstGrad * size.width, _grad * size.height),
      Offset(secondGrad * size.width, size.height),
      Offset(size.width / 2, size.height),
    );
    _getPath(
      _path,
      Offset((1 - secondGrad) * size.width, size.height),
      Offset((1 - firstGrad) * size.width, _grad * size.height),
      Offset(size.width, size.height * _height),
    );
    _path.lineTo(size.width, 0);
    _path.lineTo(0, 0);
    return _path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}

part of 'take_away_bloc.dart';

abstract class TakeAwayEvent extends Equatable {
  const TakeAwayEvent();

  @override
  List<Object> get props => [];
}

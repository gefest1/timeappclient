import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/const.dart';
import 'package:flutter/material.dart';

class NextPage extends StatelessWidget {
  final VoidCallback nextPage;

  const NextPage({
    required this.nextPage,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 20,
        left: 20,
        right: 20,
        bottom: 20,
      ),
      child: Column(
        children: [
          Expanded(
            child: Column(
              children: [
                SvgPicture.asset('assets/svg/nextPage.svg'),
                const SizedBox(
                  height: 24,
                ),
                const Text(
                  'Что дальше',
                  style: TextStyle(
                    color: mainBlack,
                    fontSize: 38,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(height: 4),
                Expanded(
                  child: const AutoSizeText(
                    "Вы принимаете заявку, статус автоматически меняется на «Занят». Человек приходит к вам в указанное им время (обычно не больше 20-30 минут). Вы оказываете услугу. Он расплачивается с вами, оставляет отзыв и уходит. Мы не берем денег за использование приложения.",
                    style: TextStyle(
                      color: mainBlack,
                      fontWeight: FontWeight.w400,
                    ),
                    minFontSize: 5,
                    maxFontSize: 18,
                  ),
                ),
              ],
            ),
          ),
          Center(
            child: SizedBox(
              child: ElevatedButton(
                onPressed: this.nextPage,
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Color(0xff007AFF)),
                  minimumSize: MaterialStateProperty.all(
                    Size(180, 58),
                  ),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16),
                      side: BorderSide.none,
                    ),
                  ),
                ),
                child: Text(
                  'Понятно',
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 18,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

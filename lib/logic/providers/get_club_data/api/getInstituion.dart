import 'package:graphql_flutter/graphql_flutter.dart';

final getInstitutionQuery = gql('''
  query(
    \$city: String!
    \$page: Int!
    \$type: AllowedInstitutionTypes
  ){
  listInstitutions(
    city: \$city
    page: \$page
    type: \$type
  ) {
    type
    avatarURL {
      M
      XL
      thumbnail
    }
    iconPath
    averagePrice
    city
    rating
    ratingCount
    contacts {
      email
      instagram
      phoneNumber
      whatsApp
    }
    tags {
      iconSVGPath
      name
    }
    dateAdded
    description
    galleryURLs {
      M
      XL
      thumbnail
    }
    location{
      latitude
      longitude
      address
    }
    name
    _id    
    workTimes {
      endTime
      startTime
    }
  }
}''');

final getFilteredInstitutions = gql('''
query(
	\$city: String!,
	\$latitude: Float,
	\$longtitude: Float,
  \$page:Int!,
  \$type: AllowedInstitutionTypes,

){
	listFilteredDistanceInstitution(
	  city:\$city,
		latitude:\$latitude,
		longitude:\$longtitude,
		page:\$page,
    type:\$type
	){
		type
    avatarURL {
      M
      XL
      thumbnail
    }
    iconPath
    averagePrice
    city
    rating
    ratingCount
    contacts {
      email
      instagram
      phoneNumber
      whatsApp
    }
    tags {
      iconSVGPath
      name
    }
    dateAdded
    description
    galleryURLs {
      M
      XL
      thumbnail
    }
    location{
      latitude
      longitude
      address
    }
    name
    _id    
    workTimes {
      endTime
      startTime
    }
  }
}
''');

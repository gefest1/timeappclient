part of '../MinuteInstiutionBloc.dart';

extension GetDataReaction on MinuteInstitutionBloc {
  void getCurrentService(
    GetCurrentServiceEvent event,
    Emitter<MinuteInstitutionState> emit,
  ) async {
    routerDelegate.blockWidgets();
    try {
      final _mutationResult = await graphqlProvider.client.mutate(
        MutationOptions(
          document: gql('''
          query (\$institutionId: String!){
            getCurrentServicesByInstitutionId(institutionId:  \$institutionId) {
              _id
              endTime
              bufferTimeMinutes
              institution {
                _id
                iconPath
                averagePrice
                avatarURL {
                  M
                  thumbnail
                  XL
                }
                city
                dateAdded
                description
                galleryURLs {
                  M
                  thumbnail
                  XL
                }
                name
                rating
                ratingCount
                location {
                  address
                  latitude
                  longitude
                }
                type
              } 
              name
              price
              startTime
            }
          }
          '''),
          variables: {
            'institutionId': event.institutionId,
          },
        ),
      );

      if (_mutationResult.hasException) throw _mutationResult.exception!;
      final List<ServiceMinute> _data = (_mutationResult
              .data!['getCurrentServicesByInstitutionId'] as List<dynamic>)
          .map((_) => ServiceMinute.fromMap(_)!)
          .toList();

      emit(FetchedService(_data));
    } catch (e) {
      print(e.toString());
      event.state.completeError(e);
      routerDelegate.unBlockWidgets();
      return;
    }
    event.state.complete(state);
    routerDelegate.unBlockWidgets();
  }
}

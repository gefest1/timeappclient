import 'package:flutter/material.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class ActiveChip extends StatelessWidget {
  final String title;
  final Widget child;
  final bool active;
  final VoidCallback? onTap;
  final String? subTitle;

  const ActiveChip({
    required this.title,
    required this.child,
    this.active = false,
    this.onTap,
    this.subTitle,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      active: onTap != null,
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          width: 105,
          height: 148,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: active ? Colors.white : Colors.transparent,
            border: Border.all(
              color: active ? const Color(0xff00D72C) : Colors.transparent,
              width: 2,
            ),
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(13),
            child: Column(
              children: [
                const SizedBox(height: 15),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 6),
                  child: Text(
                    title,
                    style: const TextStyle(
                      color: ColorData.allMainBlack,
                      fontSize: 14,
                      height: 15 / 14,
                      fontWeight: FontWeight.w500,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                if (subTitle != null)
                  Text(
                    subTitle!,
                    style: const TextStyle(
                      color: ColorData.allMainActivegray,
                      fontSize: P4TextStyle.fontSize,
                      height: P4TextStyle.height,
                      fontWeight: P4TextStyle.fontWeight,
                    ),
                  ),
                const SizedBox(height: 10),
                Expanded(
                  child: Stack(
                    clipBehavior: Clip.none,
                    children: [child],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

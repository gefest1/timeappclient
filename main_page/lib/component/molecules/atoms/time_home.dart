// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';
import 'package:time_app_components/utils/color.dart';

class TimeHomeCustomPainter extends CustomPainter {
  final bool active;

  const TimeHomeCustomPainter({
    this.active = false,
    Listenable? repaint,
  }) : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    final _activeColor =
        active ? const Color(0xff25BA00) : ColorData.allMainGray;

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.9118376, size.height * 0.9530172);
    path_1.cubicTo(
        size.width * 0.9118376,
        size.height * 0.9530172,
        size.width * 0.8892183,
        size.height * 0.9651364,
        size.width * 0.8683350,
        size.height * 0.9080020);
    path_1.cubicTo(
        size.width * 0.8474518,
        size.height * 0.8508667,
        size.width * 0.8257005,
        size.height * 0.8179707,
        size.width * 0.7473959,
        size.height * 0.8768374);
    path_1.cubicTo(
        size.width * 0.7473959,
        size.height * 0.8768374,
        size.width * 0.6969289,
        size.height * 0.9045394,
        size.width * 0.6490761,
        size.height * 0.8127768);
    path_1.cubicTo(
        size.width * 0.6098376,
        size.height * 0.7374626,
        size.width * 0.5083858,
        size.height * 0.7111455,
        size.width * 0.3866619,
        size.height * 0.8669687);
    path_1.cubicTo(
        size.width * 0.3733497,
        size.height * 0.8569263,
        size.width * 0.3588193,
        size.height * 0.8401323,
        size.width * 0.3445503,
        size.height * 0.8127768);
    path_1.cubicTo(
        size.width * 0.2966959,
        size.height * 0.7210152,
        size.width * 0.1566137,
        size.height * 0.7019697,
        0,
        size.height * 0.9963010);
    path_1.lineTo(size.width * 0.3045264, size.height * 0.9963010);
    path_1.lineTo(size.width * 0.6934518, size.height * 0.9963010);
    path_1.lineTo(size.width * 0.9979746, size.height * 0.9963010);
    path_1.cubicTo(
        size.width * 0.9979746,
        size.height * 0.9963010,
        size.width * 0.9692640,
        size.height * 0.9028081,
        size.width * 0.9118376,
        size.height * 0.9530172);
    path_1.close();

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = const Color(0xffDFE7F4);
    canvas.drawPath(path_1, paint_1_fill);

    Path path_2 = Path();
    path_2.moveTo(size.width * 0.5017731, size.height * 0.02570949);
    path_2.cubicTo(
        size.width * 0.5017731,
        size.height * 0.02570949,
        size.width * 0.4855898,
        size.height * 0.02051535,
        size.width * 0.4861985,
        size.height * 0.05271869);
    path_2.cubicTo(
        size.width * 0.4868076,
        size.height * 0.08492202,
        size.width * 0.4927244,
        size.height * 0.1192030,
        size.width * 0.4927244,
        size.height * 0.1192030);
    path_2.cubicTo(
        size.width * 0.4927244,
        size.height * 0.1192030,
        size.width * 0.4948995,
        size.height * 0.1384212,
        size.width * 0.4904619,
        size.height * 0.1569465);
    path_2.cubicTo(
        size.width * 0.4904619,
        size.height * 0.1569465,
        size.width * 0.5177817,
        size.height * 0.1495020,
        size.width * 0.5223959,
        size.height * 0.1240505);
    path_2.lineTo(size.width * 0.5212640, size.height * 0.03090354);
    path_2.lineTo(size.width * 0.5017731, size.height * 0.02570949);
    path_2.close();

    Paint paint_2_fill = Paint()..style = PaintingStyle.fill;
    paint_2_fill.color = const Color(0xff121212);
    canvas.drawPath(path_2, paint_2_fill);

    Path path_3 = Path();
    path_3.moveTo(size.width * 0.5086447, size.height * 0.2130303);
    path_3.lineTo(size.width * 0.5101218, size.height * 0.1254242);
    path_3.cubicTo(
        size.width * 0.5101218,
        size.height * 0.1254242,
        size.width * 0.4850650,
        size.height * 0.1385818,
        size.width * 0.4944619,
        size.height * 0.04318414);
    path_3.cubicTo(
        size.width * 0.4944619,
        size.height * 0.04318414,
        size.width * 0.5039457,
        size.height * 0.02413909,
        size.width * 0.5160406,
        size.height * 0.03608556);
    path_3.cubicTo(
        size.width * 0.5281320,
        size.height * 0.04820505,
        size.width * 0.5321371,
        size.height * 0.07088586,
        size.width * 0.5321371,
        size.height * 0.07088586);
    path_3.lineTo(size.width * 0.5394467, size.height * 0.2054131);
    path_3.lineTo(size.width * 0.5086447, size.height * 0.2130303);
    path_3.close();

    Paint paint_3_fill = Paint()..style = PaintingStyle.fill;
    paint_3_fill.color = Colors.white;
    canvas.drawPath(path_3, paint_3_fill);

    Path path_4 = Path();
    path_4.moveTo(size.width * 0.5073381, size.height * 0.2159869);
    path_4.lineTo(size.width * 0.5088173, size.height * 0.1283808);
    path_4.cubicTo(
        size.width * 0.5062939,
        size.height * 0.1289000,
        size.width * 0.5015086,
        size.height * 0.1285535,
        size.width * 0.4975061,
        size.height * 0.1211091);
    path_4.cubicTo(
        size.width * 0.4908066,
        size.height * 0.1086424,
        size.width * 0.4894147,
        size.height * 0.08215293,
        size.width * 0.4932426,
        size.height * 0.04267788);
    path_4.lineTo(size.width * 0.4933299, size.height * 0.04198525);
    path_4.lineTo(size.width * 0.4935909, size.height * 0.04146596);
    path_4.cubicTo(
        size.width * 0.4936777,
        size.height * 0.04129283,
        size.width * 0.5038579,
        size.height * 0.02120899,
        size.width * 0.5166497,
        size.height * 0.03402111);
    path_4.cubicTo(
        size.width * 0.5290914,
        size.height * 0.04648687,
        size.width * 0.5332640,
        size.height * 0.06934081,
        size.width * 0.5334416,
        size.height * 0.07020646);
    path_4.lineTo(size.width * 0.5335279, size.height * 0.07072596);
    path_4.lineTo(size.width * 0.5410102, size.height * 0.2078495);
    path_4.lineTo(size.width * 0.5073381, size.height * 0.2159869);
    path_4.close();
    path_4.moveTo(size.width * 0.5115127, size.height * 0.1219747);
    path_4.lineTo(size.width * 0.5100355, size.height * 0.2101010);
    path_4.lineTo(size.width * 0.5380508, size.height * 0.2031758);
    path_4.lineTo(size.width * 0.5309188, size.height * 0.07141848);
    path_4.cubicTo(
        size.width * 0.5303959,
        size.height * 0.06899455,
        size.width * 0.5263046,
        size.height * 0.04908394,
        size.width * 0.5155178,
        size.height * 0.03834949);
    path_4.cubicTo(
        size.width * 0.5055107,
        size.height * 0.02830758,
        size.width * 0.4973320,
        size.height * 0.04163909,
        size.width * 0.4957660,
        size.height * 0.04440929);
    path_4.cubicTo(
        size.width * 0.4912416,
        size.height * 0.09080980,
        size.width * 0.4951569,
        size.height * 0.1096818,
        size.width * 0.4993335,
        size.height * 0.1173000);
    path_4.cubicTo(
        size.width * 0.5039447,
        size.height * 0.1257828,
        size.width * 0.5098629,
        size.height * 0.1228404,
        size.width * 0.5098629,
        size.height * 0.1228404);
    path_4.lineTo(size.width * 0.5115127, size.height * 0.1219747);
    path_4.close();

    Paint paint_4_fill = Paint()..style = PaintingStyle.fill;
    paint_4_fill.color = const Color(0xff121212);
    canvas.drawPath(path_4, paint_4_fill);

    Path path_5 = Path();
    path_5.moveTo(size.width * 0.4949832, size.height * 0.03972343);
    path_5.cubicTo(
        size.width * 0.4949832,
        size.height * 0.03972343,
        size.width * 0.4995076,
        size.height * 0.05253545,
        size.width * 0.5050761,
        size.height * 0.05928778);
    path_5.cubicTo(
        size.width * 0.5097766,
        size.height * 0.06500131,
        size.width * 0.5154315,
        size.height * 0.06465505,
        size.width * 0.5175178,
        size.height * 0.06707899);
    path_5.cubicTo(
        size.width * 0.5175178,
        size.height * 0.06707899,
        size.width * 0.5232589,
        size.height * 0.1115747,
        size.width * 0.5115127,
        size.height * 0.1522616);
    path_5.cubicTo(
        size.width * 0.5115127,
        size.height * 0.1522616,
        size.width * 0.5283096,
        size.height * 0.1527818,
        size.width * 0.5403147,
        size.height * 0.1212707);
    path_5.cubicTo(
        size.width * 0.5403147,
        size.height * 0.1212707,
        size.width * 0.5381371,
        size.height * 0.1294081,
        size.width * 0.5358782,
        size.height * 0.1356414);
    path_5.cubicTo(
        size.width * 0.5358782,
        size.height * 0.1356414,
        size.width * 0.5562386,
        size.height * 0.1373727,
        size.width * 0.5438832,
        size.height * 0.04612949);
    path_5.cubicTo(
        size.width * 0.5358782,
        size.height * -0.01291000,
        size.width * 0.5006386,
        size.height * 0.01583071,
        size.width * 0.4953315,
        size.height * 0.02569939);
    path_5.lineTo(size.width * 0.4949832, size.height * 0.03972343);
    path_5.close();

    Paint paint_5_fill = Paint()..style = PaintingStyle.fill;
    paint_5_fill.color = const Color(0xff121212);
    canvas.drawPath(path_5, paint_5_fill);

    Path path_6 = Path();
    path_6.moveTo(size.width * 0.5366599, size.height * 0.7780071);
    path_6.cubicTo(
        size.width * 0.5455381,
        size.height * 0.8541869,
        size.width * 0.5545838,
        size.height * 0.9341758,
        size.width * 0.5550203,
        size.height * 0.9445646);
    path_6.cubicTo(
        size.width * 0.5553706,
        size.height * 0.9513162,
        size.width * 0.5410964,
        size.height * 0.9653404,
        size.width * 0.5410964,
        size.height * 0.9653404);
    path_6.lineTo(size.width * 0.5738122, size.height * 0.9639556);
    path_6.lineTo(size.width * 0.5621523, size.height * 0.7664071);
    path_6.lineTo(size.width * 0.5543249, size.height * 0.6336111);
    path_6.lineTo(size.width * 0.5196091, size.height * 0.6336111);
    path_6.cubicTo(
        size.width * 0.5197817,
        size.height * 0.6337848,
        size.width * 0.5281371,
        size.height * 0.7040778,
        size.width * 0.5366599,
        size.height * 0.7780071);
    path_6.close();

    Paint paint_6_fill = Paint()..style = PaintingStyle.fill;
    paint_6_fill.color = Colors.white;
    canvas.drawPath(path_6, paint_6_fill);

    Path path_7 = Path();
    path_7.moveTo(size.width * 0.5352690, size.height * 0.9682616);
    path_7.lineTo(size.width * 0.5405787, size.height * 0.9630677);
    path_7.cubicTo(
        size.width * 0.5476244,
        size.height * 0.9561424,
        size.width * 0.5538020,
        size.height * 0.9476586,
        size.width * 0.5538020,
        size.height * 0.9448879);
    path_7.cubicTo(
        size.width * 0.5534569,
        size.height * 0.9364051,
        size.width * 0.5471066,
        size.height * 0.8789232,
        size.width * 0.5354467,
        size.height * 0.7786778);
    path_7.cubicTo(
        size.width * 0.5270051,
        size.height * 0.7056141,
        size.width * 0.5186548,
        size.height * 0.6349747,
        size.width * 0.5185635,
        size.height * 0.6342818);
    path_7.lineTo(size.width * 0.5182183, size.height * 0.6311657);
    path_7.lineTo(size.width * 0.5557157, size.height * 0.6311657);
    path_7.lineTo(size.width * 0.5754670, size.height * 0.9665303);
    path_7.lineTo(size.width * 0.5352690, size.height * 0.9682616);
    path_7.close();
    path_7.moveTo(size.width * 0.5379695, size.height * 0.7774657);
    path_7.cubicTo(
        size.width * 0.5498020,
        size.height * 0.8794424,
        size.width * 0.5559797,
        size.height * 0.9355384,
        size.width * 0.5564162,
        size.height * 0.9443687);
    path_7.cubicTo(
        size.width * 0.5566751,
        size.height * 0.9499091,
        size.width * 0.5513655,
        size.height * 0.9571808,
        size.width * 0.5466701,
        size.height * 0.9625485);
    path_7.lineTo(size.width * 0.5725127, size.height * 0.9615091);
    path_7.lineTo(size.width * 0.5533706, size.height * 0.6363596);
    path_7.lineTo(size.width * 0.5215228, size.height * 0.6363596);
    path_7.cubicTo(
        size.width * 0.5231777,
        size.height * 0.6509030,
        size.width * 0.5304873,
        size.height * 0.7132313,
        size.width * 0.5379695,
        size.height * 0.7774657);
    path_7.close();

    Paint paint_7_fill = Paint()..style = PaintingStyle.fill;
    paint_7_fill.color = const Color(0xff121212);
    canvas.drawPath(path_7, paint_7_fill);

    Path path_8 = Path();
    path_8.moveTo(size.width * 0.5746853, size.height * 0.9478303);
    path_8.lineTo(size.width * 0.5765990, size.height * 0.9739737);
    path_8.cubicTo(
        size.width * 0.5770305,
        size.height * 0.9803798,
        size.width * 0.5745076,
        size.height * 0.9862657,
        size.width * 0.5712893,
        size.height * 0.9862657);
    path_8.lineTo(size.width * 0.5243046, size.height * 0.9862657);
    path_8.cubicTo(
        size.width * 0.5230863,
        size.height * 0.9862657,
        size.width * 0.5226497,
        size.height * 0.9829768,
        size.width * 0.5237817,
        size.height * 0.9817646);
    path_8.lineTo(size.width * 0.5451015, size.height * 0.9606424);
    path_8.cubicTo(
        size.width * 0.5453604,
        size.height * 0.9604687,
        size.width * 0.5455330,
        size.height * 0.9602960,
        size.width * 0.5457970,
        size.height * 0.9604687);
    path_8.cubicTo(
        size.width * 0.5486701,
        size.height * 0.9613343,
        size.width * 0.5668528,
        size.height * 0.9667020,
        size.width * 0.5725939,
        size.height * 0.9469636);
    path_8.cubicTo(
        size.width * 0.5730305,
        size.height * 0.9450596,
        size.width * 0.5745076,
        size.height * 0.9455788,
        size.width * 0.5746853,
        size.height * 0.9478303);
    path_8.close();

    Paint paint_8_fill = Paint()..style = PaintingStyle.fill;
    paint_8_fill.color = const Color(0xff121212);
    canvas.drawPath(path_8, paint_8_fill);

    Path path_9 = Path();
    path_9.moveTo(size.width * 0.5712893, size.height * 0.9888697);
    path_9.lineTo(size.width * 0.5243046, size.height * 0.9888697);
    path_9.cubicTo(
        size.width * 0.5230863,
        size.height * 0.9888697,
        size.width * 0.5221320,
        size.height * 0.9873121,
        size.width * 0.5218731,
        size.height * 0.9850616);
    path_9.cubicTo(
        size.width * 0.5216091,
        size.height * 0.9828101,
        size.width * 0.5221320,
        size.height * 0.9805596,
        size.width * 0.5231777,
        size.height * 0.9795212);
    path_9.lineTo(size.width * 0.5444924, size.height * 0.9583980);
    path_9.cubicTo(
        size.width * 0.5449289,
        size.height * 0.9578788,
        size.width * 0.5455381,
        size.height * 0.9577061,
        size.width * 0.5460609,
        size.height * 0.9578788);
    path_9.cubicTo(
        size.width * 0.5480609,
        size.height * 0.9585717,
        size.width * 0.5661574,
        size.height * 0.9641121,
        size.width * 0.5714670,
        size.height * 0.9457596);
    path_9.cubicTo(
        size.width * 0.5719848,
        size.height * 0.9438545,
        size.width * 0.5730305,
        size.height * 0.9429889,
        size.width * 0.5740761,
        size.height * 0.9433354);
    path_9.cubicTo(
        size.width * 0.5751168,
        size.height * 0.9436818,
        size.width * 0.5759036,
        size.height * 0.9454131,
        size.width * 0.5760761,
        size.height * 0.9474909);
    path_9.lineTo(size.width * 0.5779898, size.height * 0.9736343);
    path_9.cubicTo(
        size.width * 0.5782538,
        size.height * 0.9774434,
        size.width * 0.5777310,
        size.height * 0.9814253,
        size.width * 0.5764264,
        size.height * 0.9843687);
    path_9.cubicTo(
        size.width * 0.5751168,
        size.height * 0.9871384,
        size.width * 0.5732030,
        size.height * 0.9888697,
        size.width * 0.5712893,
        size.height * 0.9888697);
    path_9.close();
    path_9.moveTo(size.width * 0.5247411, size.height * 0.9836758);
    path_9.lineTo(size.width * 0.5712893, size.height * 0.9836758);
    path_9.cubicTo(
        size.width * 0.5725076,
        size.height * 0.9836758,
        size.width * 0.5736396,
        size.height * 0.9826374,
        size.width * 0.5744213,
        size.height * 0.9809061);
    path_9.cubicTo(
        size.width * 0.5752081,
        size.height * 0.9791747,
        size.width * 0.5755533,
        size.height * 0.9767505,
        size.width * 0.5753807,
        size.height * 0.9743273);
    path_9.lineTo(size.width * 0.5735533, size.height * 0.9488758);
    path_9.cubicTo(
        size.width * 0.5672893,
        size.height * 0.9691323,
        size.width * 0.5491929,
        size.height * 0.9641121,
        size.width * 0.5456244,
        size.height * 0.9629000);
    path_9.lineTo(size.width * 0.5247411, size.height * 0.9836758);
    path_9.close();
    path_9.moveTo(size.width * 0.5746853, size.height * 0.9478374);
    path_9.lineTo(size.width * 0.5733807, size.height * 0.9481828);
    path_9.lineTo(size.width * 0.5746853, size.height * 0.9478374);
    path_9.close();

    Paint paint_9_fill = Paint()..style = PaintingStyle.fill;
    paint_9_fill.color = const Color(0xff121212);
    canvas.drawPath(path_9, paint_9_fill);

    Path path_10 = Path();
    path_10.moveTo(size.width * 0.4817614, size.height * 0.9654788);
    path_10.lineTo(size.width * 0.5144772, size.height * 0.9640939);
    path_10.lineTo(size.width * 0.5129949, size.height * 0.7762404);
    path_10.lineTo(size.width * 0.5118680, size.height * 0.6337495);
    path_10.lineTo(size.width * 0.4771497, size.height * 0.6337495);
    path_10.cubicTo(
        size.width * 0.4771497,
        size.height * 0.6337495,
        size.width * 0.4808914,
        size.height * 0.6955596,
        size.width * 0.4849807,
        size.height * 0.7642939);
    path_10.cubicTo(
        size.width * 0.4897660,
        size.height * 0.8442838,
        size.width * 0.4950736,
        size.height * 0.9336212,
        size.width * 0.4955954,
        size.height * 0.9447020);
    path_10.cubicTo(
        size.width * 0.4959437,
        size.height * 0.9514545,
        size.width * 0.4817614,
        size.height * 0.9654788,
        size.width * 0.4817614,
        size.height * 0.9654788);
    path_10.close();

    Paint paint_10_fill = Paint()..style = PaintingStyle.fill;
    paint_10_fill.color = Colors.white;
    canvas.drawPath(path_10, paint_10_fill);

    Path path_11 = Path();
    path_11.moveTo(size.width * 0.4759305, size.height * 0.9682616);
    path_11.lineTo(size.width * 0.4812381, size.height * 0.9630677);
    path_11.cubicTo(
        size.width * 0.4882858,
        size.height * 0.9561424,
        size.width * 0.4944635,
        size.height * 0.9476586,
        size.width * 0.4944635,
        size.height * 0.9448879);
    path_11.cubicTo(
        size.width * 0.4939411,
        size.height * 0.9327687,
        size.width * 0.4875898,
        size.height * 0.8266364,
        size.width * 0.4838482,
        size.height * 0.7646535);
    path_11.cubicTo(
        size.width * 0.4797589,
        size.height * 0.6960909,
        size.width * 0.4760178,
        size.height * 0.6341081,
        size.width * 0.4760178,
        size.height * 0.6341081);
    path_11.lineTo(size.width * 0.4758437, size.height * 0.6311657);
    path_11.lineTo(size.width * 0.5132589, size.height * 0.6311657);
    path_11.lineTo(size.width * 0.5158680, size.height * 0.9665303);
    path_11.lineTo(size.width * 0.4759305, size.height * 0.9682616);
    path_11.close();
    path_11.moveTo(size.width * 0.4787147, size.height * 0.6363596);
    path_11.cubicTo(
        size.width * 0.4794980,
        size.height * 0.6493444,
        size.width * 0.4828041,
        size.height * 0.7040556,
        size.width * 0.4863716,
        size.height * 0.7639606);
    path_11.cubicTo(
        size.width * 0.4901127,
        size.height * 0.8261172,
        size.width * 0.4963777,
        size.height * 0.9322495,
        size.width * 0.4969863,
        size.height * 0.9443687);
    path_11.cubicTo(
        size.width * 0.4972477,
        size.height * 0.9499091,
        size.width * 0.4919401,
        size.height * 0.9571808,
        size.width * 0.4872416,
        size.height * 0.9625485);
    path_11.lineTo(size.width * 0.5131675, size.height * 0.9615091);
    path_11.lineTo(size.width * 0.5106447, size.height * 0.6363596);
    path_11.lineTo(size.width * 0.4787147, size.height * 0.6363596);
    path_11.lineTo(size.width * 0.4787147, size.height * 0.6363596);
    path_11.close();

    Paint paint_11_fill = Paint()..style = PaintingStyle.fill;
    paint_11_fill.color = const Color(0xff121212);
    canvas.drawPath(path_11, paint_11_fill);

    Path path_12 = Path();
    path_12.moveTo(size.width * 0.5152589, size.height * 0.9478303);
    path_12.lineTo(size.width * 0.5171726, size.height * 0.9739737);
    path_12.cubicTo(
        size.width * 0.5176091,
        size.height * 0.9803798,
        size.width * 0.5150863,
        size.height * 0.9862657,
        size.width * 0.5118629,
        size.height * 0.9862657);
    path_12.lineTo(size.width * 0.4648812, size.height * 0.9862657);
    path_12.cubicTo(
        size.width * 0.4636635,
        size.height * 0.9862657,
        size.width * 0.4632284,
        size.height * 0.9829768,
        size.width * 0.4643594,
        size.height * 0.9817646);
    path_12.lineTo(size.width * 0.4856761, size.height * 0.9606424);
    path_12.cubicTo(
        size.width * 0.4859371,
        size.height * 0.9604687,
        size.width * 0.4861112,
        size.height * 0.9602960,
        size.width * 0.4863721,
        size.height * 0.9604687);
    path_12.cubicTo(
        size.width * 0.4892437,
        size.height * 0.9613343,
        size.width * 0.5074279,
        size.height * 0.9667020,
        size.width * 0.5131726,
        size.height * 0.9469636);
    path_12.cubicTo(
        size.width * 0.5136904,
        size.height * 0.9450596,
        size.width * 0.5151726,
        size.height * 0.9455788,
        size.width * 0.5152589,
        size.height * 0.9478303);
    path_12.close();

    Paint paint_12_fill = Paint()..style = PaintingStyle.fill;
    paint_12_fill.color = const Color(0xff121212);
    canvas.drawPath(path_12, paint_12_fill);

    Path path_13 = Path();
    path_13.moveTo(size.width * 0.5118680, size.height * 0.9888697);
    path_13.lineTo(size.width * 0.4648827, size.height * 0.9888697);
    path_13.cubicTo(
        size.width * 0.4636645,
        size.height * 0.9888697,
        size.width * 0.4627076,
        size.height * 0.9873121,
        size.width * 0.4624467,
        size.height * 0.9850616);
    path_13.cubicTo(
        size.width * 0.4621853,
        size.height * 0.9828101,
        size.width * 0.4627076,
        size.height * 0.9805596,
        size.width * 0.4637518,
        size.height * 0.9795212);
    path_13.lineTo(size.width * 0.4850685, size.height * 0.9583980);
    path_13.cubicTo(
        size.width * 0.4855036,
        size.height * 0.9578788,
        size.width * 0.4861127,
        size.height * 0.9577061,
        size.width * 0.4866345,
        size.height * 0.9578788);
    path_13.cubicTo(
        size.width * 0.4886360,
        size.height * 0.9585717,
        size.width * 0.5067335,
        size.height * 0.9641121,
        size.width * 0.5120406,
        size.height * 0.9457596);
    path_13.cubicTo(
        size.width * 0.5125635,
        size.height * 0.9438545,
        size.width * 0.5136091,
        size.height * 0.9429889,
        size.width * 0.5146497,
        size.height * 0.9433354);
    path_13.cubicTo(
        size.width * 0.5156954,
        size.height * 0.9436818,
        size.width * 0.5164772,
        size.height * 0.9454131,
        size.width * 0.5166497,
        size.height * 0.9474909);
    path_13.lineTo(size.width * 0.5185685, size.height * 0.9736343);
    path_13.cubicTo(
        size.width * 0.5188274,
        size.height * 0.9774434,
        size.width * 0.5183046,
        size.height * 0.9814253,
        size.width * 0.5170000,
        size.height * 0.9843687);
    path_13.cubicTo(
        size.width * 0.5156954,
        size.height * 0.9873121,
        size.width * 0.5137817,
        size.height * 0.9888697,
        size.width * 0.5118680,
        size.height * 0.9888697);
    path_13.close();
    path_13.moveTo(size.width * 0.4653178, size.height * 0.9836758);
    path_13.lineTo(size.width * 0.5118680, size.height * 0.9836758);
    path_13.cubicTo(
        size.width * 0.5130863,
        size.height * 0.9836758,
        size.width * 0.5142183,
        size.height * 0.9826374,
        size.width * 0.5150000,
        size.height * 0.9809061);
    path_13.cubicTo(
        size.width * 0.5157817,
        size.height * 0.9791747,
        size.width * 0.5161320,
        size.height * 0.9767505,
        size.width * 0.5159543,
        size.height * 0.9743273);
    path_13.lineTo(size.width * 0.5141269, size.height * 0.9488758);
    path_13.cubicTo(
        size.width * 0.5078629,
        size.height * 0.9691323,
        size.width * 0.4897670,
        size.height * 0.9641121,
        size.width * 0.4861995,
        size.height * 0.9629000);
    path_13.lineTo(size.width * 0.4653178, size.height * 0.9836758);
    path_13.close();
    path_13.moveTo(size.width * 0.5152589, size.height * 0.9478374);
    path_13.lineTo(size.width * 0.5139543, size.height * 0.9481828);
    path_13.lineTo(size.width * 0.5152589, size.height * 0.9478374);
    path_13.close();

    Paint paint_13_fill = Paint()..style = PaintingStyle.fill;
    paint_13_fill.color = const Color(0xff121212);
    canvas.drawPath(path_13, paint_13_fill);

    Path path_14 = Path();
    path_14.moveTo(size.width * 0.5623299, size.height * 0.4071182);
    path_14.lineTo(size.width * 0.4781081, size.height * 0.4199303);
    path_14.cubicTo(
        size.width * 0.4781081,
        size.height * 0.4199303,
        size.width * 0.4661883,
        size.height * 0.6983333,
        size.width * 0.4667970,
        size.height * 0.7471576);
    path_14.cubicTo(
        size.width * 0.4667970,
        size.height * 0.7471576,
        size.width * 0.5511929,
        size.height * 0.7836899,
        size.width * 0.5947868,
        size.height * 0.7326141);
    path_14.cubicTo(
        size.width * 0.5947868,
        size.height * 0.7327879,
        size.width * 0.5781675,
        size.height * 0.5232929,
        size.width * 0.5623299,
        size.height * 0.4071182);
    path_14.close();

    Paint paint_14_fill = Paint()..style = PaintingStyle.fill;
    paint_14_fill.color = Colors.white;
    canvas.drawPath(path_14, paint_14_fill);

    Path path_15 = Path();
    path_15.moveTo(size.width * 0.5289188, size.height * 0.7627485);
    path_15.cubicTo(
        size.width * 0.4969000,
        size.height * 0.7627485,
        size.width * 0.4685355,
        size.height * 0.7508020,
        size.width * 0.4665345,
        size.height * 0.7499364);
    path_15.lineTo(size.width * 0.4654904, size.height * 0.7494172);
    path_15.lineTo(size.width * 0.4654904, size.height * 0.7473394);
    path_15.cubicTo(
        size.width * 0.4649680,
        size.height * 0.6990343,
        size.width * 0.4767142,
        size.height * 0.4225354,
        size.width * 0.4768010,
        size.height * 0.4197657);
    path_15.lineTo(size.width * 0.4768883, size.height * 0.4175141);
    path_15.lineTo(size.width * 0.5632843, size.height * 0.4043556);
    path_15.lineTo(size.width * 0.5635482, size.height * 0.4064333);
    path_15.cubicTo(
        size.width * 0.5792081,
        size.height * 0.5212232,
        size.width * 0.5958274,
        size.height * 0.7301990,
        size.width * 0.5960000,
        size.height * 0.7322768);
    path_15.lineTo(size.width * 0.5961777, size.height * 0.7340081);
    path_15.lineTo(size.width * 0.5953909, size.height * 0.7348737);
    path_15.cubicTo(
        size.width * 0.5771218,
        size.height * 0.7565152,
        size.width * 0.5520609,
        size.height * 0.7627485,
        size.width * 0.5289188,
        size.height * 0.7627485);
    path_15.close();
    path_15.moveTo(size.width * 0.4680132, size.height * 0.7452616);
    path_15.cubicTo(
        size.width * 0.4776711,
        size.height * 0.7490707,
        size.width * 0.5530203,
        size.height * 0.7771182,
        size.width * 0.5933046,
        size.height * 0.7315838);
    path_15.cubicTo(
        size.width * 0.5916497,
        size.height * 0.7111535,
        size.width * 0.5760761,
        size.height * 0.5193182,
        size.width * 0.5613706,
        size.height * 0.4100697);
    path_15.lineTo(size.width * 0.4793244, size.height * 0.4227081);
    path_15.cubicTo(
        size.width * 0.4781934,
        size.height * 0.4485061,
        size.width * 0.4678396,
        size.height * 0.6943596,
        size.width * 0.4680132,
        size.height * 0.7452616);
    path_15.close();

    Paint paint_15_fill = Paint()..style = PaintingStyle.fill;
    paint_15_fill.color = const Color(0xff121212);
    canvas.drawPath(path_15, paint_15_fill);

    Path path_16 = Path();
    path_16.moveTo(size.width * 0.4326888, size.height * 0.2241071);
    path_16.lineTo(size.width * 0.4193766, size.height * 0.2556172);
    path_16.lineTo(size.width * 0.4489594, size.height * 0.3233141);
    path_16.cubicTo(
        size.width * 0.4489594,
        size.height * 0.3233141,
        size.width * 0.4562680,
        size.height * 0.3345677,
        size.width * 0.4635766,
        size.height * 0.3151768);
    path_16.lineTo(size.width * 0.4831533, size.height * 0.2694687);
    path_16.cubicTo(
        size.width * 0.4831533,
        size.height * 0.2694687,
        size.width * 0.4865462,
        size.height * 0.3075586,
        size.width * 0.4840234,
        size.height * 0.3323172);
    path_16.cubicTo(
        size.width * 0.4840234,
        size.height * 0.3323172,
        size.width * 0.4775848,
        size.height * 0.4129980,
        size.width * 0.4759315,
        size.height * 0.4365444);
    path_16.cubicTo(
        size.width * 0.4759315,
        size.height * 0.4365444,
        size.width * 0.5369239,
        size.height * 0.4694404,
        size.width * 0.5691168,
        size.height * 0.4237323);
    path_16.lineTo(size.width * 0.5640711, size.height * 0.3553434);
    path_16.lineTo(size.width * 0.5808629, size.height * 0.3660788);
    path_16.cubicTo(
        size.width * 0.5885178,
        size.height * 0.3709263,
        size.width * 0.5966091,
        size.height * 0.3607111,
        size.width * 0.5973046,
        size.height * 0.3447828);
    path_16.cubicTo(
        size.width * 0.5976548,
        size.height * 0.3369919,
        size.width * 0.5972183,
        size.height * 0.3271232,
        size.width * 0.5952183,
        size.height * 0.3141374);
    path_16.lineTo(size.width * 0.5753807, size.height * 0.1875747);
    path_16.cubicTo(
        size.width * 0.5753807,
        size.height * 0.1875747,
        size.width * 0.5732081,
        size.height * 0.1683566,
        size.width * 0.5649391,
        size.height * 0.1642010);
    path_16.lineTo(size.width * 0.5372741, size.height * 0.1472343);
    path_16.cubicTo(
        size.width * 0.5372741,
        size.height * 0.1472343,
        size.width * 0.5168274,
        size.height * 0.1832465,
        size.width * 0.5101269,
        size.height * 0.1481000);
    path_16.lineTo(size.width * 0.4826310, size.height * 0.1697414);
    path_16.lineTo(size.width * 0.4501772, size.height * 0.2545788);
    path_16.lineTo(size.width * 0.4326888, size.height * 0.2241071);
    path_16.close();

    Paint paint_16_fill = Paint()..style = PaintingStyle.fill;
    paint_16_fill.color = _activeColor;
    canvas.drawPath(path_16, paint_16_fill);

    Path path_17 = Path();
    path_17.moveTo(size.width * 0.4926335, size.height * 0.3169232);
    path_17.lineTo(size.width * 0.5166497, size.height * 0.3299081);
    path_17.lineTo(size.width * 0.5331777, size.height * 0.3248879);
    path_17.lineTo(size.width * 0.5437056, size.height * 0.3312939);
    path_17.lineTo(size.width * 0.5466650, size.height * 0.3028990);
    path_17.lineTo(size.width * 0.5366599, size.height * 0.3008212);
    path_17.lineTo(size.width * 0.5318731, size.height * 0.2944152);
    path_17.cubicTo(
        size.width * 0.5201269,
        size.height * 0.2836808,
        size.width * 0.5069898,
        size.height * 0.2944152,
        size.width * 0.5069898,
        size.height * 0.2944152);
    path_17.cubicTo(
        size.width * 0.5100355,
        size.height * 0.3041111,
        size.width * 0.5126447,
        size.height * 0.2987434,
        size.width * 0.5165584,
        size.height * 0.3008212);
    path_17.cubicTo(
        size.width * 0.5204772,
        size.height * 0.3028990,
        size.width * 0.5191726,
        size.height * 0.3072273,
        size.width * 0.5191726,
        size.height * 0.3072273);
    path_17.lineTo(size.width * 0.5113401, size.height * 0.3115556);
    path_17.lineTo(size.width * 0.4969843, size.height * 0.3072273);
    path_17.cubicTo(
        size.width * 0.4948086,
        size.height * 0.3084394,
        size.width * 0.4926335,
        size.height * 0.3169232,
        size.width * 0.4926335,
        size.height * 0.3169232);
    path_17.close();

    Paint paint_17_fill = Paint()..style = PaintingStyle.fill;
    paint_17_fill.color = Colors.white;
    canvas.drawPath(path_17, paint_17_fill);

    Path path_18 = Path();
    path_18.moveTo(size.width * 0.5447563, size.height * 0.3347242);
    path_18.lineTo(size.width * 0.5330964, size.height * 0.3276263);
    path_18.lineTo(size.width * 0.5165635, size.height * 0.3326465);
    path_18.lineTo(size.width * 0.4908096, size.height * 0.3186232);
    path_18.lineTo(size.width * 0.4915056, size.height * 0.3158525);
    path_18.cubicTo(
        size.width * 0.4919406,
        size.height * 0.3142939,
        size.width * 0.4941157,
        size.height * 0.3061566,
        size.width * 0.4967259,
        size.height * 0.3047717);
    path_18.lineTo(size.width * 0.4969868, size.height * 0.3045990);
    path_18.lineTo(size.width * 0.5113452, size.height * 0.3089273);
    path_18.lineTo(size.width * 0.5181320, size.height * 0.3051182);
    path_18.cubicTo(
        size.width * 0.5179543,
        size.height * 0.3045990,
        size.width * 0.5174315,
        size.height * 0.3037333,
        size.width * 0.5163046,
        size.height * 0.3032141);
    path_18.cubicTo(
        size.width * 0.5151726,
        size.height * 0.3025212,
        size.width * 0.5142132,
        size.height * 0.3026939,
        size.width * 0.5131726,
        size.height * 0.3028677);
    path_18.cubicTo(
        size.width * 0.5109086,
        size.height * 0.3030404,
        size.width * 0.5083858,
        size.height * 0.3033869,
        size.width * 0.5059487,
        size.height * 0.2955960);
    path_18.lineTo(size.width * 0.5051655, size.height * 0.2929990);
    path_18.lineTo(size.width * 0.5065579, size.height * 0.2917869);
    path_18.cubicTo(
        size.width * 0.5070797,
        size.height * 0.2912677,
        size.width * 0.5203046,
        size.height * 0.2807061,
        size.width * 0.5324873,
        size.height * 0.2917869);
    path_18.lineTo(size.width * 0.5326599, size.height * 0.2919596);
    path_18.lineTo(size.width * 0.5371827, size.height * 0.2980192);
    path_18.lineTo(size.width * 0.5483198, size.height * 0.3004434);
    path_18.lineTo(size.width * 0.5447563, size.height * 0.3347242);
    path_18.close();
    path_18.moveTo(size.width * 0.5332690, size.height * 0.3222586);
    path_18.lineTo(size.width * 0.5427513, size.height * 0.3279717);
    path_18.lineTo(size.width * 0.5451878, size.height * 0.3051182);
    path_18.lineTo(size.width * 0.5363147, size.height * 0.3032141);
    path_18.lineTo(size.width * 0.5313553, size.height * 0.2964616);
    path_18.cubicTo(
        size.width * 0.5225685,
        size.height * 0.2884970,
        size.width * 0.5129949,
        size.height * 0.2931717,
        size.width * 0.5092538,
        size.height * 0.2954222);
    path_18.cubicTo(
        size.width * 0.5103858,
        size.height * 0.2978465,
        size.width * 0.5114315,
        size.height * 0.2976737,
        size.width * 0.5130812,
        size.height * 0.2975000);
    path_18.cubicTo(
        size.width * 0.5142132,
        size.height * 0.2973273,
        size.width * 0.5155178,
        size.height * 0.2971535,
        size.width * 0.5170863,
        size.height * 0.2980192);
    path_18.cubicTo(
        size.width * 0.5190000,
        size.height * 0.2990586,
        size.width * 0.5202183,
        size.height * 0.3007899,
        size.width * 0.5206548,
        size.height * 0.3032141);
    path_18.cubicTo(
        size.width * 0.5211726,
        size.height * 0.3058111,
        size.width * 0.5205685,
        size.height * 0.3082343,
        size.width * 0.5204772,
        size.height * 0.3084081);
    path_18.lineTo(size.width * 0.5202183, size.height * 0.3092737);
    path_18.lineTo(size.width * 0.5116041, size.height * 0.3139485);
    path_18.lineTo(size.width * 0.4972482, size.height * 0.3096202);
    path_18.cubicTo(
        size.width * 0.4964650,
        size.height * 0.3103121,
        size.width * 0.4955076,
        size.height * 0.3127364,
        size.width * 0.4947249,
        size.height * 0.3149869);
    path_18.lineTo(size.width * 0.5168223, size.height * 0.3269333);
    path_18.lineTo(size.width * 0.5332690, size.height * 0.3222586);
    path_18.close();

    Paint paint_18_fill = Paint()..style = PaintingStyle.fill;
    paint_18_fill.color = const Color(0xff121212);
    canvas.drawPath(path_18, paint_18_fill);

    Path path_19 = Path();
    path_19.moveTo(size.width * 0.4397340, size.height * 0.1403192);
    path_19.lineTo(size.width * 0.3857893, size.height * 0.1403192);
    path_19.lineTo(size.width * 0.4131964, size.height * 0.2833303);
    path_19.lineTo(size.width * 0.4671416, size.height * 0.2833303);
    path_19.lineTo(size.width * 0.4397340, size.height * 0.1403192);
    path_19.close();

    Paint paint_19_fill = Paint()..style = PaintingStyle.fill;
    paint_19_fill.color = Colors.white;
    canvas.drawPath(path_19, paint_19_fill);

    Path path_20 = Path();
    path_20.moveTo(size.width * 0.4690563, size.height * 0.2859253);
    path_20.lineTo(size.width * 0.4123274, size.height * 0.2859253);
    path_20.lineTo(size.width * 0.3838756, size.height * 0.1375475);
    path_20.lineTo(size.width * 0.4406046, size.height * 0.1375475);
    path_20.lineTo(size.width * 0.4690563, size.height * 0.2859253);
    path_20.close();
    path_20.moveTo(size.width * 0.4140675, size.height * 0.2807313);
    path_20.lineTo(size.width * 0.4652279, size.height * 0.2807313);
    path_20.lineTo(size.width * 0.4387777, size.height * 0.1427414);
    path_20.lineTo(size.width * 0.3876173, size.height * 0.1427414);
    path_20.lineTo(size.width * 0.4140675, size.height * 0.2807313);
    path_20.close();

    Paint paint_20_fill = Paint()..style = PaintingStyle.fill;
    paint_20_fill.color = const Color(0xff121212);
    canvas.drawPath(path_20, paint_20_fill);

    Path path_21 = Path();
    path_21.moveTo(size.width * 0.5668528, size.height * 0.3605394);
    path_21.lineTo(size.width * 0.5410102, size.height * 0.3420131);
    path_21.lineTo(size.width * 0.5455330, size.height * 0.2987293);
    path_21.lineTo(size.width * 0.5831218, size.height * 0.3070404);
    path_21.lineTo(size.width * 0.5828629, size.height * 0.3122343);
    path_21.lineTo(size.width * 0.5476244, size.height * 0.3044434);
    path_21.lineTo(size.width * 0.5440558, size.height * 0.3385505);
    path_21.lineTo(size.width * 0.5678071, size.height * 0.3556909);
    path_21.lineTo(size.width * 0.5668528, size.height * 0.3605394);
    path_21.close();

    Paint paint_21_fill = Paint()..style = PaintingStyle.fill;
    paint_21_fill.color = const Color(0xff121212);
    canvas.drawPath(path_21, paint_21_fill);

    Path path_22 = Path();
    path_22.moveTo(size.width * 0.4823680, size.height * 0.2783010);
    path_22.cubicTo(
        size.width * 0.4794096,
        size.height * 0.2298232,
        size.width * 0.4884584,
        size.height * 0.2102586,
        size.width * 0.4888934,
        size.height * 0.2095667);
    path_22.lineTo(size.width * 0.4908076, size.height * 0.2132020);
    path_22.cubicTo(
        size.width * 0.4907208,
        size.height * 0.2133758,
        size.width * 0.4821939,
        size.height * 0.2319010,
        size.width * 0.4849782,
        size.height * 0.2777818);
    path_22.lineTo(size.width * 0.4823680, size.height * 0.2783010);
    path_22.close();

    Paint paint_22_fill = Paint()..style = PaintingStyle.fill;
    paint_22_fill.color = const Color(0xff121212);
    canvas.drawPath(path_22, paint_22_fill);

    Path path_23 = Path();
    path_23.moveTo(size.width * 0.5551168, size.height * 0.2445020);
    path_23.lineTo(size.width * 0.5525990, size.height * 0.2458566);
    path_23.lineTo(size.width * 0.5607005, size.height * 0.3055283);
    path_23.lineTo(size.width * 0.5632234, size.height * 0.3041727);
    path_23.lineTo(size.width * 0.5551168, size.height * 0.2445020);
    path_23.close();

    Paint paint_23_fill = Paint()..style = PaintingStyle.fill;
    paint_23_fill.color = const Color(0xff121212);
    canvas.drawPath(path_23, paint_23_fill);

    Path path_24 = Path();
    path_24.moveTo(size.width * 0.3905746, size.height * 0.1661141);
    path_24.lineTo(size.width * 0.3934462, size.height * 0.1803111);
    path_24.lineTo(size.width * 0.3965782, size.height * 0.1775414);
    path_24.cubicTo(
        size.width * 0.3997107,
        size.height * 0.1747707,
        size.width * 0.4033650,
        size.height * 0.1780606,
        size.width * 0.4044091,
        size.height * 0.1844667);
    path_24.cubicTo(
        size.width * 0.4060619,
        size.height * 0.1946818,
        size.width * 0.4070193,
        size.height * 0.2097444,
        size.width * 0.4018858,
        size.height * 0.2232495);
    path_24.cubicTo(
        size.width * 0.4018858,
        size.height * 0.2232495,
        size.width * 0.3933589,
        size.height * 0.2216909,
        size.width * 0.3897046,
        size.height * 0.1972788);
    path_24.cubicTo(
        size.width * 0.3870944,
        size.height * 0.1799657,
        size.width * 0.3878777,
        size.height * 0.1742515,
        size.width * 0.3905746,
        size.height * 0.1661141);
    path_24.close();

    Paint paint_24_fill = Paint()..style = PaintingStyle.fill;
    paint_24_fill.color = Colors.white;
    canvas.drawPath(path_24, paint_24_fill);

    Path path_25 = Path();
    path_25.moveTo(size.width * 0.4024970, size.height * 0.2260414);
    path_25.lineTo(size.width * 0.4018010, size.height * 0.2258677);
    path_25.cubicTo(
        size.width * 0.4014528,
        size.height * 0.2258677,
        size.width * 0.3924041,
        size.height * 0.2239636,
        size.width * 0.3884888,
        size.height * 0.1979929);
    path_25.cubicTo(
        size.width * 0.3857914,
        size.height * 0.1801606,
        size.width * 0.3865746,
        size.height * 0.1735808,
        size.width * 0.3895330,
        size.height * 0.1645778);
    path_25.lineTo(size.width * 0.3909249, size.height * 0.1604222);
    path_25.lineTo(size.width * 0.3942310, size.height * 0.1766970);
    path_25.lineTo(size.width * 0.3961452, size.height * 0.1749657);
    path_25.cubicTo(
        size.width * 0.3979726,
        size.height * 0.1734081,
        size.width * 0.4001477,
        size.height * 0.1734081,
        size.width * 0.4019746,
        size.height * 0.1749657);
    path_25.cubicTo(
        size.width * 0.4038020,
        size.height * 0.1765242,
        size.width * 0.4051939,
        size.height * 0.1796404,
        size.width * 0.4058030,
        size.height * 0.1834495);
    path_25.cubicTo(
        size.width * 0.4075431,
        size.height * 0.1943576,
        size.width * 0.4085005,
        size.height * 0.2102859,
        size.width * 0.4030188,
        size.height * 0.2244828);
    path_25.lineTo(size.width * 0.4024970, size.height * 0.2260414);
    path_25.close();
    path_25.moveTo(size.width * 0.3903157, size.height * 0.1720232);
    path_25.cubicTo(
        size.width * 0.3890107,
        size.height * 0.1775626,
        size.width * 0.3889239,
        size.height * 0.1834495,
        size.width * 0.3909249,
        size.height * 0.1966081);
    path_25.cubicTo(
        size.width * 0.3937091,
        size.height * 0.2151333,
        size.width * 0.3993645,
        size.height * 0.2194616,
        size.width * 0.4012787,
        size.height * 0.2205010);
    path_25.cubicTo(
        size.width * 0.4055421,
        size.height * 0.2083818,
        size.width * 0.4046721,
        size.height * 0.1948768,
        size.width * 0.4031929,
        size.height * 0.1855273);
    path_25.cubicTo(
        size.width * 0.4028452,
        size.height * 0.1831030,
        size.width * 0.4019746,
        size.height * 0.1811990,
        size.width * 0.4008437,
        size.height * 0.1801596);
    path_25.cubicTo(
        size.width * 0.3997127,
        size.height * 0.1791212,
        size.width * 0.3983203,
        size.height * 0.1791212,
        size.width * 0.3971893,
        size.height * 0.1801596);
    path_25.lineTo(size.width * 0.3928391, size.height * 0.1839687);
    path_25.lineTo(size.width * 0.3903157, size.height * 0.1720232);
    path_25.close();

    Paint paint_25_fill = Paint()..style = PaintingStyle.fill;
    paint_25_fill.color = const Color(0xff121212);
    canvas.drawPath(path_25, paint_25_fill);

    Path path_26 = Path();
    path_26.moveTo(size.width * 0.6195787, size.height * 0.1635404);
    path_26.lineTo(size.width * 0.6103553, size.height * 0.3283657);
    path_26.cubicTo(
        size.width * 0.6098376,
        size.height * 0.3373687,
        size.width * 0.6134010,
        size.height * 0.3453333,
        size.width * 0.6179289,
        size.height * 0.3453333);
    path_26.lineTo(size.width * 0.7410406, size.height * 0.3453333);
    path_26.cubicTo(
        size.width * 0.7449594,
        size.height * 0.3453333,
        size.width * 0.7481777,
        size.height * 0.3394465,
        size.width * 0.7486142,
        size.height * 0.3318283);
    path_26.lineTo(size.width * 0.7578376, size.height * 0.1670030);
    path_26.cubicTo(
        size.width * 0.7583553,
        size.height * 0.1580000,
        size.width * 0.7547919,
        size.height * 0.1500354,
        size.width * 0.7502640,
        size.height * 0.1500354);
    path_26.lineTo(size.width * 0.6272386, size.height * 0.1500354);
    path_26.cubicTo(
        size.width * 0.6233198,
        size.height * 0.1500354,
        size.width * 0.6200152,
        size.height * 0.1557485,
        size.width * 0.6195787,
        size.height * 0.1635404);
    path_26.close();

    Paint paint_26_fill = Paint()..style = PaintingStyle.fill;
    paint_26_fill.color = const Color(0xff121212);
    canvas.drawPath(path_26, paint_26_fill);

    Path path_27 = Path();
    path_27.moveTo(size.width * 0.7644467, size.height * 0.1668152);
    path_27.lineTo(size.width * 0.7552234, size.height * 0.3316414);
    path_27.cubicTo(
        size.width * 0.7547919,
        size.height * 0.3394323,
        size.width * 0.7515736,
        size.height * 0.3451455,
        size.width * 0.7476548,
        size.height * 0.3451455);
    path_27.lineTo(size.width * 0.6245381, size.height * 0.3451455);
    path_27.cubicTo(
        size.width * 0.6200152,
        size.height * 0.3451455,
        size.width * 0.6164467,
        size.height * 0.3371818,
        size.width * 0.6169695,
        size.height * 0.3281788);
    path_27.lineTo(size.width * 0.6261929, size.height * 0.1633525);
    path_27.cubicTo(
        size.width * 0.6266294,
        size.height * 0.1555616,
        size.width * 0.6298477,
        size.height * 0.1498485,
        size.width * 0.6337614,
        size.height * 0.1498485);
    path_27.lineTo(size.width * 0.7568782, size.height * 0.1498485);
    path_27.cubicTo(
        size.width * 0.7614924,
        size.height * 0.1500212,
        size.width * 0.7649695,
        size.height * 0.1578121,
        size.width * 0.7644467,
        size.height * 0.1668152);
    path_27.close();

    Paint paint_27_fill = Paint()..style = PaintingStyle.fill;
    paint_27_fill.color = Colors.white;
    canvas.drawPath(path_27, paint_27_fill);

    Path path_28 = Path();
    path_28.moveTo(size.width * 0.7476548, size.height * 0.3479131);
    path_28.lineTo(size.width * 0.6245381, size.height * 0.3479131);
    path_28.cubicTo(
        size.width * 0.6220152,
        size.height * 0.3479131,
        size.width * 0.6195787,
        size.height * 0.3456626,
        size.width * 0.6178376,
        size.height * 0.3420263);
    path_28.cubicTo(
        size.width * 0.6161878,
        size.height * 0.3382172,
        size.width * 0.6153147,
        size.height * 0.3331970,
        size.width * 0.6156650,
        size.height * 0.3281758);
    path_28.lineTo(size.width * 0.6248883, size.height * 0.1633505);
    path_28.cubicTo(
        size.width * 0.6254112,
        size.height * 0.1543465,
        size.width * 0.6292386,
        size.height * 0.1474212,
        size.width * 0.6337614,
        size.height * 0.1474212);
    path_28.lineTo(size.width * 0.7568782, size.height * 0.1474212);
    path_28.cubicTo(
        size.width * 0.7594010,
        size.height * 0.1474212,
        size.width * 0.7618376,
        size.height * 0.1496717,
        size.width * 0.7635787,
        size.height * 0.1533081);
    path_28.cubicTo(
        size.width * 0.7652284,
        size.height * 0.1571172,
        size.width * 0.7661015,
        size.height * 0.1621384,
        size.width * 0.7657513,
        size.height * 0.1671586);
    path_28.lineTo(size.width * 0.7565279, size.height * 0.3319848);
    path_28.cubicTo(
        size.width * 0.7560964,
        size.height * 0.3411606,
        size.width * 0.7522690,
        size.height * 0.3479131,
        size.width * 0.7476548,
        size.height * 0.3479131);
    path_28.close();
    path_28.moveTo(size.width * 0.6337614, size.height * 0.1526152);
    path_28.cubicTo(
        size.width * 0.6305431,
        size.height * 0.1526152,
        size.width * 0.6278477,
        size.height * 0.1574636,
        size.width * 0.6274975,
        size.height * 0.1638697);
    path_28.lineTo(size.width * 0.6182741, size.height * 0.3286949);
    path_28.cubicTo(
        size.width * 0.6181015,
        size.height * 0.3323313,
        size.width * 0.6186244,
        size.height * 0.3357939,
        size.width * 0.6198426,
        size.height * 0.3385636);
    path_28.cubicTo(
        size.width * 0.6210609,
        size.height * 0.3411606,
        size.width * 0.6227970,
        size.height * 0.3427192,
        size.width * 0.6245381,
        size.height * 0.3427192);
    path_28.lineTo(size.width * 0.7476548, size.height * 0.3427192);
    path_28.cubicTo(
        size.width * 0.7508731,
        size.height * 0.3427192,
        size.width * 0.7535736,
        size.height * 0.3378717,
        size.width * 0.7539188,
        size.height * 0.3314657);
    path_28.lineTo(size.width * 0.7631421, size.height * 0.1666394);
    path_28.cubicTo(
        size.width * 0.7633147,
        size.height * 0.1630040,
        size.width * 0.7627970,
        size.height * 0.1595414,
        size.width * 0.7615787,
        size.height * 0.1567707);
    path_28.cubicTo(
        size.width * 0.7603604,
        size.height * 0.1541737,
        size.width * 0.7586193,
        size.height * 0.1526152,
        size.width * 0.7568782,
        size.height * 0.1526152);
    path_28.lineTo(size.width * 0.6337614, size.height * 0.1526152);
    path_28.close();

    Paint paint_28_fill = Paint()..style = PaintingStyle.fill;
    paint_28_fill.color = const Color(0xff121212);
    canvas.drawPath(path_28, paint_28_fill);

    Path path_29 = Path();
    path_29.moveTo(size.width * 0.6868376, size.height * 0.2606535);
    path_29.lineTo(size.width * 0.6737868, size.height * 0.2606535);
    path_29.lineTo(size.width * 0.6737868, size.height * 0.3453172);
    path_29.lineTo(size.width * 0.6868376, size.height * 0.3453172);
    path_29.lineTo(size.width * 0.6868376, size.height * 0.2606535);
    path_29.close();

    Paint paint_29_fill = Paint()..style = PaintingStyle.fill;
    paint_29_fill.color = const Color(0xff121212);
    canvas.drawPath(path_29, paint_29_fill);

    Path path_30 = Path();
    path_30.moveTo(size.width * 0.6737868, size.height * 0.2606535);
    path_30.lineTo(size.width * 0.7011066, size.height * 0.2606535);
    path_30.lineTo(size.width * 0.7185076, size.height * 0.3887747);
    path_30.lineTo(size.width * 0.6509036, size.height * 0.3887747);
    path_30.lineTo(size.width * 0.6509036, size.height * 0.3757889);
    path_30.lineTo(size.width * 0.6879695, size.height * 0.3757889);
    path_30.lineTo(size.width * 0.6737868, size.height * 0.2606535);
    path_30.close();

    Paint paint_30_fill = Paint()..style = PaintingStyle.fill;
    paint_30_fill.color = Colors.white;
    canvas.drawPath(path_30, paint_30_fill);

    Path path_31 = Path();
    path_31.moveTo(size.width * 0.7202487, size.height * 0.3913980);
    path_31.lineTo(size.width * 0.6495990, size.height * 0.3913980);
    path_31.lineTo(size.width * 0.6495990, size.height * 0.3732192);
    path_31.lineTo(size.width * 0.6863147, size.height * 0.3732192);
    path_31.lineTo(size.width * 0.6721371, size.height * 0.2582566);
    path_31.lineTo(size.width * 0.7020660, size.height * 0.2582566);
    path_31.lineTo(size.width * 0.7202487, size.height * 0.3913980);
    path_31.close();
    path_31.moveTo(size.width * 0.6522081, size.height * 0.3862040);
    path_31.lineTo(size.width * 0.7168579, size.height * 0.3862040);
    path_31.lineTo(size.width * 0.7001523, size.height * 0.2632778);
    path_31.lineTo(size.width * 0.6755279, size.height * 0.2632778);
    path_31.lineTo(size.width * 0.6897107, size.height * 0.3782394);
    path_31.lineTo(size.width * 0.6522995, size.height * 0.3782394);
    path_31.lineTo(size.width * 0.6522995, size.height * 0.3862040);
    path_31.lineTo(size.width * 0.6522081, size.height * 0.3862040);
    path_31.close();

    Paint paint_31_fill = Paint()..style = PaintingStyle.fill;
    paint_31_fill.color = const Color(0xff121212);
    canvas.drawPath(path_31, paint_31_fill);

    Path path_32 = Path();
    path_32.moveTo(size.width * 0.6582132, size.height * 0.3257475);
    path_32.lineTo(size.width * 0.6558629, size.height * 0.3257475);
    path_32.lineTo(size.width * 0.6558629, size.height * 0.3326727);
    path_32.lineTo(size.width * 0.6582132, size.height * 0.3326727);
    path_32.lineTo(size.width * 0.6582132, size.height * 0.3257475);
    path_32.close();

    Paint paint_32_fill = Paint()..style = PaintingStyle.fill;
    paint_32_fill.color = const Color(0xff133346);
    canvas.drawPath(path_32, paint_32_fill);

    Path path_33 = Path();
    path_33.moveTo(size.width * 0.6529036, size.height * 0.3257475);
    path_33.lineTo(size.width * 0.6505584, size.height * 0.3257475);
    path_33.lineTo(size.width * 0.6505584, size.height * 0.3326727);
    path_33.lineTo(size.width * 0.6529036, size.height * 0.3326727);
    path_33.lineTo(size.width * 0.6529036, size.height * 0.3257475);
    path_33.close();

    Paint paint_33_fill = Paint()..style = PaintingStyle.fill;
    paint_33_fill.color = const Color(0xff133346);
    canvas.drawPath(path_33, paint_33_fill);

    Path path_34 = Path();
    path_34.moveTo(size.width * 0.6475990, size.height * 0.3257475);
    path_34.lineTo(size.width * 0.6452487, size.height * 0.3257475);
    path_34.lineTo(size.width * 0.6452487, size.height * 0.3326727);
    path_34.lineTo(size.width * 0.6475990, size.height * 0.3326727);
    path_34.lineTo(size.width * 0.6475990, size.height * 0.3257475);
    path_34.close();

    Paint paint_34_fill = Paint()..style = PaintingStyle.fill;
    paint_34_fill.color = const Color(0xff133346);
    canvas.drawPath(path_34, paint_34_fill);

    Path path_35 = Path();
    path_35.moveTo(size.width * 0.6423756, size.height * 0.3257475);
    path_35.lineTo(size.width * 0.6400254, size.height * 0.3257475);
    path_35.lineTo(size.width * 0.6400254, size.height * 0.3326727);
    path_35.lineTo(size.width * 0.6423756, size.height * 0.3326727);
    path_35.lineTo(size.width * 0.6423756, size.height * 0.3257475);
    path_35.close();

    Paint paint_35_fill = Paint()..style = PaintingStyle.fill;
    paint_35_fill.color = const Color(0xff133346);
    canvas.drawPath(path_35, paint_35_fill);

    Path path_36 = Path();
    path_36.moveTo(size.width * 0.2792127, size.height * 0.9083475);
    path_36.lineTo(size.width * 0.2792127, size.height * 0.9931848);
    path_36.lineTo(size.width * 0.6804061, size.height * 0.9931848);
    path_36.lineTo(size.width * 0.6804061, size.height * 0.9078283);
    path_36.lineTo(size.width * 0.2792127, size.height * 0.9083475);
    path_36.close();

    Paint paint_36_fill = Paint()..style = PaintingStyle.fill;
    paint_36_fill.color = Colors.white;
    canvas.drawPath(path_36, paint_36_fill);

    Path path_37 = Path();
    path_37.moveTo(size.width * 0.6817056, size.height * 0.9957646);
    path_37.lineTo(size.width * 0.2779041, size.height * 0.9957646);
    path_37.lineTo(size.width * 0.2779041, size.height * 0.9057333);
    path_37.lineTo(size.width * 0.6817056, size.height * 0.9052141);
    path_37.lineTo(size.width * 0.6817056, size.height * 0.9957646);
    path_37.close();
    path_37.moveTo(size.width * 0.2805142, size.height * 0.9905707);
    path_37.lineTo(size.width * 0.6790964, size.height * 0.9905707);
    path_37.lineTo(size.width * 0.6790964, size.height * 0.9104081);
    path_37.lineTo(size.width * 0.2805142, size.height * 0.9109273);
    path_37.lineTo(size.width * 0.2805142, size.height * 0.9905707);
    path_37.close();

    Paint paint_37_fill = Paint()..style = PaintingStyle.fill;
    paint_37_fill.color = const Color(0xff121212);
    canvas.drawPath(path_37, paint_37_fill);

    Path path_38 = Path();
    path_38.moveTo(size.width * 0.6739645, size.height * 0.9083475);
    path_38.lineTo(size.width * 0.6739645, size.height * 0.9931848);
    path_38.lineTo(size.width * 0.8378883, size.height * 0.9931848);
    path_38.lineTo(size.width * 0.8378883, size.height * 0.9078283);
    path_38.lineTo(size.width * 0.6739645, size.height * 0.9083475);
    path_38.close();

    Paint paint_38_fill = Paint()..style = PaintingStyle.fill;
    paint_38_fill.color = const Color(0xff121212);
    canvas.drawPath(path_38, paint_38_fill);

    Path path_39 = Path();
    path_39.moveTo(size.width * 0.8391929, size.height * 0.9957646);
    path_39.lineTo(size.width * 0.6726599, size.height * 0.9957646);
    path_39.lineTo(size.width * 0.6726599, size.height * 0.9057333);
    path_39.lineTo(size.width * 0.8391929, size.height * 0.9052141);
    path_39.lineTo(size.width * 0.8391929, size.height * 0.9957646);
    path_39.close();
    path_39.moveTo(size.width * 0.6752690, size.height * 0.9905707);
    path_39.lineTo(size.width * 0.8365838, size.height * 0.9905707);
    path_39.lineTo(size.width * 0.8365838, size.height * 0.9104081);
    path_39.lineTo(size.width * 0.6752690, size.height * 0.9109273);
    path_39.lineTo(size.width * 0.6752690, size.height * 0.9905707);
    path_39.close();

    Paint paint_39_fill = Paint()..style = PaintingStyle.fill;
    paint_39_fill.color = const Color(0xff121212);
    canvas.drawPath(path_39, paint_39_fill);

    Path path_40 = Path();
    path_40.moveTo(size.width * 0.6914518, size.height * 0.3893030);
    path_40.lineTo(size.width * 0.2414487, size.height * 0.3893030);
    path_40.lineTo(size.width * 0.2414487, size.height * 0.9405697);
    path_40.lineTo(size.width * 0.6914518, size.height * 0.9405697);
    path_40.lineTo(size.width * 0.6914518, size.height * 0.3893030);
    path_40.close();

    Paint paint_40_fill = Paint()..style = PaintingStyle.fill;
    paint_40_fill.color = Colors.white;
    canvas.drawPath(path_40, paint_40_fill);

    Path path_41 = Path();
    path_41.moveTo(size.width * 0.6928477, size.height * 0.9431788);
    path_41.lineTo(size.width * 0.2401452, size.height * 0.9431788);
    path_41.lineTo(size.width * 0.2401452, size.height * 0.3867192);
    path_41.lineTo(size.width * 0.6927563, size.height * 0.3867192);
    path_41.lineTo(size.width * 0.6927563, size.height * 0.9431788);
    path_41.lineTo(size.width * 0.6928477, size.height * 0.9431788);
    path_41.close();
    path_41.moveTo(size.width * 0.2427553, size.height * 0.9379848);
    path_41.lineTo(size.width * 0.6901472, size.height * 0.9379848);
    path_41.lineTo(size.width * 0.6901472, size.height * 0.3919131);
    path_41.lineTo(size.width * 0.2427553, size.height * 0.3919131);
    path_41.lineTo(size.width * 0.2427553, size.height * 0.9379848);
    path_41.close();

    Paint paint_41_fill = Paint()..style = PaintingStyle.fill;
    paint_41_fill.color = const Color(0xff121212);
    canvas.drawPath(path_41, paint_41_fill);

    Path path_42 = Path();
    path_42.moveTo(size.width * 0.8573706, size.height * 0.3884354);
    path_42.lineTo(size.width * 0.6743096, size.height * 0.3884354);
    path_42.lineTo(size.width * 0.6743096, size.height * 0.9414323);
    path_42.lineTo(size.width * 0.8573706, size.height * 0.9414323);
    path_42.lineTo(size.width * 0.8573706, size.height * 0.3884354);
    path_42.close();

    Paint paint_42_fill = Paint()..style = PaintingStyle.fill;
    paint_42_fill.color = Colors.white;
    canvas.drawPath(path_42, paint_42_fill);

    Path path_43 = Path();
    path_43.moveTo(size.width * 0.8587665, size.height * 0.9440222);
    path_43.lineTo(size.width * 0.6730914, size.height * 0.9440222);
    path_43.lineTo(size.width * 0.6730914, size.height * 0.3858313);
    path_43.lineTo(size.width * 0.8587665, size.height * 0.3858313);
    path_43.lineTo(size.width * 0.8587665, size.height * 0.9440222);
    path_43.close();
    path_43.moveTo(size.width * 0.6756142, size.height * 0.9388283);
    path_43.lineTo(size.width * 0.8561574, size.height * 0.9388283);
    path_43.lineTo(size.width * 0.8561574, size.height * 0.3910253);
    path_43.lineTo(size.width * 0.6757005, size.height * 0.3910253);
    path_43.lineTo(size.width * 0.6757005, size.height * 0.9388283);
    path_43.lineTo(size.width * 0.6756142, size.height * 0.9388283);
    path_43.close();

    Paint paint_43_fill = Paint()..style = PaintingStyle.fill;
    paint_43_fill.color = const Color(0xff121212);
    canvas.drawPath(path_43, paint_43_fill);

    Path path_44 = Path();
    path_44.moveTo(size.width * 0.2302208, size.height * 0.3886232);
    path_44.lineTo(size.width * 0.2302208, size.height * 0.4306960);
    path_44.lineTo(size.width * 0.6915330, size.height * 0.4306960);
    path_44.lineTo(size.width * 0.6915330, size.height * 0.3882778);
    path_44.lineTo(size.width * 0.2302208, size.height * 0.3886232);
    path_44.close();

    Paint paint_44_fill = Paint()..style = PaintingStyle.fill;
    paint_44_fill.color = Colors.white;
    canvas.drawPath(path_44, paint_44_fill);

    Path path_45 = Path();
    path_45.moveTo(size.width * 0.6928426, size.height * 0.4332707);
    path_45.lineTo(size.width * 0.2289173, size.height * 0.4332707);
    path_45.lineTo(size.width * 0.2289173, size.height * 0.3860040);
    path_45.lineTo(size.width * 0.6927563, size.height * 0.3858313);
    path_45.lineTo(size.width * 0.6927563, size.height * 0.4332707);
    path_45.lineTo(size.width * 0.6928426, size.height * 0.4332707);
    path_45.close();
    path_45.moveTo(size.width * 0.2315274, size.height * 0.4280758);
    path_45.lineTo(size.width * 0.6901421, size.height * 0.4280758);
    path_45.lineTo(size.width * 0.6901421, size.height * 0.3910253);
    path_45.lineTo(size.width * 0.2315274, size.height * 0.3911980);
    path_45.lineTo(size.width * 0.2315274, size.height * 0.4280758);
    path_45.close();

    Paint paint_45_fill = Paint()..style = PaintingStyle.fill;
    paint_45_fill.color = const Color(0xff121212);
    canvas.drawPath(path_45, paint_45_fill);

    Path path_46 = Path();
    path_46.moveTo(size.width * 0.6751777, size.height * 0.3886141);
    path_46.lineTo(size.width * 0.6751777, size.height * 0.4306859);
    path_46.lineTo(size.width * 0.8817360, size.height * 0.4306859);
    path_46.lineTo(size.width * 0.8817360, size.height * 0.3882677);
    path_46.lineTo(size.width * 0.6751777, size.height * 0.3886141);
    path_46.close();

    Paint paint_46_fill = Paint()..style = PaintingStyle.fill;
    paint_46_fill.color = const Color(0xff121212);
    canvas.drawPath(path_46, paint_46_fill);

    Path path_47 = Path();
    path_47.moveTo(size.width * 0.8830406, size.height * 0.4332960);
    path_47.lineTo(size.width * 0.6737868, size.height * 0.4332960);
    path_47.lineTo(size.width * 0.6737868, size.height * 0.3860293);
    path_47.lineTo(size.width * 0.8830406, size.height * 0.3856828);
    path_47.lineTo(size.width * 0.8830406, size.height * 0.4332960);
    path_47.close();
    path_47.moveTo(size.width * 0.6764873, size.height * 0.4281010);
    path_47.lineTo(size.width * 0.8805178, size.height * 0.4281010);
    path_47.lineTo(size.width * 0.8805178, size.height * 0.3910505);
    path_47.lineTo(size.width * 0.6764873, size.height * 0.3912232);
    path_47.lineTo(size.width * 0.6764873, size.height * 0.4281010);
    path_47.close();

    Paint paint_47_fill = Paint()..style = PaintingStyle.fill;
    paint_47_fill.color = const Color(0xff121212);
    canvas.drawPath(path_47, paint_47_fill);

    Path path_48 = Path();
    path_48.moveTo(size.width * 0.6233249, size.height * 0.6344495);
    path_48.lineTo(size.width * 0.5898274, size.height * 0.6344495);
    path_48.lineTo(size.width * 0.5898274, size.height * 0.6292556);
    path_48.lineTo(size.width * 0.6207157, size.height * 0.6292556);
    path_48.lineTo(size.width * 0.6207157, size.height * 0.6128081);
    path_48.lineTo(size.width * 0.6233249, size.height * 0.6128081);
    path_48.lineTo(size.width * 0.6233249, size.height * 0.6344495);
    path_48.close();

    Paint paint_48_fill = Paint()..style = PaintingStyle.fill;
    paint_48_fill.color = const Color(0xff121212);
    canvas.drawPath(path_48, paint_48_fill);

    Path path_49 = Path();
    path_49.moveTo(size.width * 0.6233249, size.height * 0.6933202);
    path_49.lineTo(size.width * 0.5898274, size.height * 0.6933202);
    path_49.lineTo(size.width * 0.5898274, size.height * 0.6881253);
    path_49.lineTo(size.width * 0.6207157, size.height * 0.6881253);
    path_49.lineTo(size.width * 0.6207157, size.height * 0.6716778);
    path_49.lineTo(size.width * 0.6233249, size.height * 0.6716778);
    path_49.lineTo(size.width * 0.6233249, size.height * 0.6933202);
    path_49.close();

    Paint paint_49_fill = Paint()..style = PaintingStyle.fill;
    paint_49_fill.color = const Color(0xff121212);
    canvas.drawPath(path_49, paint_49_fill);

    Path path_50 = Path();
    path_50.moveTo(size.width * 0.6424670, size.height * 0.6621485);
    path_50.lineTo(size.width * 0.6089695, size.height * 0.6621485);
    path_50.lineTo(size.width * 0.6089695, size.height * 0.6569545);
    path_50.lineTo(size.width * 0.6398579, size.height * 0.6569545);
    path_50.lineTo(size.width * 0.6398579, size.height * 0.6405071);
    path_50.lineTo(size.width * 0.6424670, size.height * 0.6405071);
    path_50.lineTo(size.width * 0.6424670, size.height * 0.6621485);
    path_50.close();

    Paint paint_50_fill = Paint()..style = PaintingStyle.fill;
    paint_50_fill.color = const Color(0xff121212);
    canvas.drawPath(path_50, paint_50_fill);

    Path path_51 = Path();
    path_51.moveTo(size.width * 0.6041827, size.height * 0.6621485);
    path_51.lineTo(size.width * 0.5706853, size.height * 0.6621485);
    path_51.lineTo(size.width * 0.5706853, size.height * 0.6569545);
    path_51.lineTo(size.width * 0.6015736, size.height * 0.6569545);
    path_51.lineTo(size.width * 0.6015736, size.height * 0.6405071);
    path_51.lineTo(size.width * 0.6041827, size.height * 0.6405071);
    path_51.lineTo(size.width * 0.6041827, size.height * 0.6621485);
    path_51.close();

    Paint paint_51_fill = Paint()..style = PaintingStyle.fill;
    paint_51_fill.color = const Color(0xff121212);
    canvas.drawPath(path_51, paint_51_fill);

    Path path_52 = Path();
    path_52.moveTo(size.width * 0.4928096, size.height * 0.7036869);
    path_52.lineTo(size.width * 0.4593117, size.height * 0.7036869);
    path_52.lineTo(size.width * 0.4593117, size.height * 0.6984929);
    path_52.lineTo(size.width * 0.4901995, size.height * 0.6984929);
    path_52.lineTo(size.width * 0.4901995, size.height * 0.6820455);
    path_52.lineTo(size.width * 0.4928096, size.height * 0.6820455);
    path_52.lineTo(size.width * 0.4928096, size.height * 0.7036869);
    path_52.close();

    Paint paint_52_fill = Paint()..style = PaintingStyle.fill;
    paint_52_fill.color = const Color(0xff121212);
    canvas.drawPath(path_52, paint_52_fill);

    Path path_53 = Path();
    path_53.moveTo(size.width * 0.4084162, size.height * 0.6327141);
    path_53.lineTo(size.width * 0.3749183, size.height * 0.6327141);
    path_53.lineTo(size.width * 0.3749183, size.height * 0.6275192);
    path_53.lineTo(size.width * 0.4058061, size.height * 0.6275192);
    path_53.lineTo(size.width * 0.4058061, size.height * 0.6110717);
    path_53.lineTo(size.width * 0.4084162, size.height * 0.6110717);
    path_53.lineTo(size.width * 0.4084162, size.height * 0.6327141);
    path_53.close();

    Paint paint_53_fill = Paint()..style = PaintingStyle.fill;
    paint_53_fill.color = const Color(0xff121212);
    canvas.drawPath(path_53, paint_53_fill);

    Path path_54 = Path();
    path_54.moveTo(size.width * 0.5545888, size.height * 0.8387485);
    path_54.lineTo(size.width * 0.5210914, size.height * 0.8387485);
    path_54.lineTo(size.width * 0.5210914, size.height * 0.8335545);
    path_54.lineTo(size.width * 0.5519797, size.height * 0.8335545);
    path_54.lineTo(size.width * 0.5519797, size.height * 0.8171071);
    path_54.lineTo(size.width * 0.5545888, size.height * 0.8171071);
    path_54.lineTo(size.width * 0.5545888, size.height * 0.8387485);
    path_54.close();

    Paint paint_54_fill = Paint()..style = PaintingStyle.fill;
    paint_54_fill.color = const Color(0xff121212);
    canvas.drawPath(path_54, paint_54_fill);

    Path path_55 = Path();
    path_55.moveTo(size.width * 0.4719274, size.height * 0.7348586);
    path_55.lineTo(size.width * 0.4384294, size.height * 0.7348586);
    path_55.lineTo(size.width * 0.4384294, size.height * 0.7296636);
    path_55.lineTo(size.width * 0.4693173, size.height * 0.7296636);
    path_55.lineTo(size.width * 0.4693173, size.height * 0.7132162);
    path_55.lineTo(size.width * 0.4719274, size.height * 0.7132162);
    path_55.lineTo(size.width * 0.4719274, size.height * 0.7348586);
    path_55.close();

    Paint paint_55_fill = Paint()..style = PaintingStyle.fill;
    paint_55_fill.color = const Color(0xff121212);
    canvas.drawPath(path_55, paint_55_fill);

    Path path_56 = Path();
    path_56.moveTo(size.width * 0.3302812, size.height * 0.1678394);
    path_56.lineTo(size.width * 0.3320213, size.height * 0.1264606);
    path_56.cubicTo(
        size.width * 0.3320213,
        size.height * 0.1264606,
        size.width * 0.3539472,
        size.height * 0.1307889,
        size.width * 0.3424624,
        size.height * 0.03954586);
    path_56.lineTo(size.width * 0.3112264, size.height * 0.03954586);
    path_56.lineTo(size.width * 0.3034827, size.height * 0.1678394);
    path_56.lineTo(size.width * 0.3302812, size.height * 0.1678394);
    path_56.close();

    Paint paint_56_fill = Paint()..style = PaintingStyle.fill;
    paint_56_fill.color = Colors.white;
    canvas.drawPath(path_56, paint_56_fill);

    Path path_57 = Path();
    path_57.moveTo(size.width * 0.3315010, size.height * 0.1704202);
    path_57.lineTo(size.width * 0.3020924, size.height * 0.1704202);
    path_57.lineTo(size.width * 0.3100970, size.height * 0.03693182);
    path_57.lineTo(size.width * 0.3435081, size.height * 0.03693182);
    path_57.lineTo(size.width * 0.3437690, size.height * 0.03883626);
    path_57.cubicTo(
        size.width * 0.3487284,
        size.height * 0.07865768,
        size.width * 0.3482066,
        size.height * 0.1058404,
        size.width * 0.3421157,
        size.height * 0.1198636);
    path_57.cubicTo(
        size.width * 0.3389838,
        size.height * 0.1271354,
        size.width * 0.3352421,
        size.height * 0.1286939,
        size.width * 0.3332411,
        size.height * 0.1288667);
    path_57.lineTo(size.width * 0.3315010, size.height * 0.1704202);
    path_57.close();
    path_57.moveTo(size.width * 0.3049635, size.height * 0.1652263);
    path_57.lineTo(size.width * 0.3290650, size.height * 0.1652263);
    path_57.lineTo(size.width * 0.3308051, size.height * 0.1236727);
    path_57.lineTo(size.width * 0.3321102, size.height * 0.1238465);
    path_57.cubicTo(
        size.width * 0.3322843,
        size.height * 0.1238465,
        size.width * 0.3366345,
        size.height * 0.1245384,
        size.width * 0.3401147,
        size.height * 0.1164010);
    path_57.cubicTo(
        size.width * 0.3437690,
        size.height * 0.1077444,
        size.width * 0.3469883,
        size.height * 0.08783394,
        size.width * 0.3414198,
        size.height * 0.04212586);
    path_57.lineTo(size.width * 0.3123594, size.height * 0.04212586);
    path_57.lineTo(size.width * 0.3049635, size.height * 0.1652263);
    path_57.close();

    Paint paint_57_fill = Paint()..style = PaintingStyle.fill;
    paint_57_fill.color = const Color(0xff121212);
    canvas.drawPath(path_57, paint_57_fill);

    Path path_58 = Path();
    path_58.moveTo(size.width * 0.3354137, size.height * 0.08523778);
    path_58.cubicTo(
        size.width * 0.3354137,
        size.height * 0.08523778,
        size.width * 0.3349787,
        size.height * 0.07000182,
        size.width * 0.3281051,
        size.height * 0.07554212);
    path_58.cubicTo(
        size.width * 0.3250599,
        size.height * 0.07796606,
        size.width * 0.3254076,
        size.height * 0.09077808,
        size.width * 0.3294975,
        size.height * 0.09943495);
    path_58.cubicTo(
        size.width * 0.3294975,
        size.height * 0.09943495,
        size.width * 0.3276701,
        size.height * 0.1200384,
        size.width * 0.3147929,
        size.height * 0.1191727);
    path_58.cubicTo(
        size.width * 0.3035690,
        size.height * 0.1184798,
        size.width * 0.3021766,
        size.height * 0.1042828,
        size.width * 0.3021766,
        size.height * 0.1042828);
    path_58.cubicTo(
        size.width * 0.3021766,
        size.height * 0.1042828,
        size.width * 0.2953904,
        size.height * 0.07104051,
        size.width * 0.3011325,
        size.height * 0.03814465);
    path_58.cubicTo(
        size.width * 0.3011325,
        size.height * 0.03814465,
        size.width * 0.3033949,
        size.height * 0.01009659,
        size.width * 0.3261909,
        size.height * 0.01459818);
    path_58.cubicTo(
        size.width * 0.3324553,
        size.height * 0.01581010,
        size.width * 0.3389812,
        size.height * 0.01061606,
        size.width * 0.3412431,
        size.height * 0.003690566);
    path_58.cubicTo(
        size.width * 0.3425482,
        size.height * -0.0002915687,
        size.width * 0.3452457,
        size.height * -0.001157273,
        size.width * 0.3471599,
        size.height * 0.001612909);
    path_58.cubicTo(
        size.width * 0.3516843,
        size.height * 0.008538364,
        size.width * 0.3569046,
        size.height * 0.02152354,
        size.width * 0.3537721,
        size.height * 0.04247303);
    path_58.cubicTo(
        size.width * 0.3537721,
        size.height * 0.04247303,
        size.width * 0.3523802,
        size.height * 0.05701657,
        size.width * 0.3401990,
        size.height * 0.06134495);
    path_58.lineTo(size.width * 0.3375888, size.height * 0.08385263);
    path_58.lineTo(size.width * 0.3354137, size.height * 0.08523778);
    path_58.close();

    Paint paint_58_fill = Paint()..style = PaintingStyle.fill;
    paint_58_fill.color = const Color(0xff121212);
    canvas.drawPath(path_58, paint_58_fill);

    Path path_59 = Path();
    path_59.moveTo(size.width * 0.3348944, size.height * 0.9069606);
    path_59.lineTo(size.width * 0.3280208, size.height * 0.9590737);
    path_59.lineTo(size.width * 0.3096619, size.height * 0.9590737);
    path_59.lineTo(size.width * 0.3138386, size.height * 0.9069606);
    path_59.lineTo(size.width * 0.3348944, size.height * 0.9069606);
    path_59.close();

    Paint paint_59_fill = Paint()..style = PaintingStyle.fill;
    paint_59_fill.color = Colors.white;
    canvas.drawPath(path_59, paint_59_fill);

    Path path_60 = Path();
    path_60.moveTo(size.width * 0.3290670, size.height * 0.9616545);
    path_60.lineTo(size.width * 0.3080980, size.height * 0.9616545);
    path_60.lineTo(size.width * 0.3127096, size.height * 0.9043465);
    path_60.lineTo(size.width * 0.3366365, size.height * 0.9043465);
    path_60.lineTo(size.width * 0.3290670, size.height * 0.9616545);
    path_60.close();
    path_60.moveTo(size.width * 0.3111431, size.height * 0.9564606);
    path_60.lineTo(size.width * 0.3269787, size.height * 0.9564606);
    path_60.lineTo(size.width * 0.3331563, size.height * 0.9095404);
    path_60.lineTo(size.width * 0.3148848, size.height * 0.9095404);
    path_60.lineTo(size.width * 0.3111431, size.height * 0.9564606);
    path_60.close();

    Paint paint_60_fill = Paint()..style = PaintingStyle.fill;
    paint_60_fill.color = const Color(0xff121212);
    canvas.drawPath(path_60, paint_60_fill);

    Path path_61 = Path();
    path_61.moveTo(size.width * 0.3596061, size.height * 0.9928202);
    path_61.cubicTo(
        size.width * 0.3596061,
        size.height * 0.9928202,
        size.width * 0.3623030,
        size.height * 0.9834717,
        size.width * 0.3569086,
        size.height * 0.9767192);
    path_61.cubicTo(
        size.width * 0.3569086,
        size.height * 0.9767192,
        size.width * 0.3350701,
        size.height * 0.9578475,
        size.width * 0.3289792,
        size.height * 0.9438232);
    path_61.cubicTo(
        size.width * 0.3289792,
        size.height * 0.9438232,
        size.width * 0.3208878,
        size.height * 0.9606172,
        size.width * 0.3095766,
        size.height * 0.9438232);
    path_61.lineTo(size.width * 0.3040949, size.height * 0.9928202);
    path_61.lineTo(size.width * 0.3596061, size.height * 0.9928202);
    path_61.close();

    Paint paint_61_fill = Paint()..style = PaintingStyle.fill;
    paint_61_fill.color = Colors.white;
    canvas.drawPath(path_61, paint_61_fill);

    Path path_62 = Path();
    path_62.moveTo(size.width * 0.3602995, size.height * 0.9954111);
    path_62.lineTo(size.width * 0.3023528, size.height * 0.9954111);
    path_62.lineTo(size.width * 0.3086173, size.height * 0.9393152);
    path_62.lineTo(size.width * 0.3102706, size.height * 0.9417394);
    path_62.cubicTo(
        size.width * 0.3205371,
        size.height * 0.9569747,
        size.width * 0.3276721,
        size.height * 0.9426051,
        size.width * 0.3279330,
        size.height * 0.9420848);
    path_62.lineTo(size.width * 0.3288898, size.height * 0.9400081);
    path_62.lineTo(size.width * 0.3298472, size.height * 0.9420848);
    path_62.cubicTo(
        size.width * 0.3356766,
        size.height * 0.9555899,
        size.width * 0.3570802,
        size.height * 0.9741152,
        size.width * 0.3573416,
        size.height * 0.9742889);
    path_62.lineTo(size.width * 0.3575157, size.height * 0.9744616);
    path_62.cubicTo(
        size.width * 0.3626487,
        size.height * 0.9808677,
        size.width * 0.3617787,
        size.height * 0.9900434,
        size.width * 0.3606477,
        size.height * 0.9940263);
    path_62.lineTo(size.width * 0.3602995, size.height * 0.9954111);
    path_62.close();
    path_62.moveTo(size.width * 0.3056589, size.height * 0.9902172);
    path_62.lineTo(size.width * 0.3587335, size.height * 0.9902172);
    path_62.cubicTo(
        size.width * 0.3590817,
        size.height * 0.9877929,
        size.width * 0.3594294,
        size.height * 0.9829455,
        size.width * 0.3562975,
        size.height * 0.9789636);
    path_62.cubicTo(
        size.width * 0.3545574,
        size.height * 0.9775778,
        size.width * 0.3358508,
        size.height * 0.9611303,
        size.width * 0.3288898,
        size.height * 0.9472788);
    path_62.cubicTo(
        size.width * 0.3263670,
        size.height * 0.9510879,
        size.width * 0.3194061,
        size.height * 0.9592253,
        size.width * 0.3104442,
        size.height * 0.9479717);
    path_62.lineTo(size.width * 0.3056589, size.height * 0.9902172);
    path_62.close();

    Paint paint_62_fill = Paint()..style = PaintingStyle.fill;
    paint_62_fill.color = const Color(0xff121212);
    canvas.drawPath(path_62, paint_62_fill);

    Path path_63 = Path();
    path_63.moveTo(size.width * 0.2591970, size.height * 0.9069606);
    path_63.lineTo(size.width * 0.2529325, size.height * 0.9573424);
    path_63.lineTo(size.width * 0.2336168, size.height * 0.9573424);
    path_63.lineTo(size.width * 0.2372711, size.height * 0.9069606);
    path_63.lineTo(size.width * 0.2591970, size.height * 0.9069606);
    path_63.close();

    Paint paint_63_fill = Paint()..style = PaintingStyle.fill;
    paint_63_fill.color = Colors.white;
    canvas.drawPath(path_63, paint_63_fill);

    Path path_64 = Path();
    path_64.moveTo(size.width * 0.2538888, size.height * 0.9599232);
    path_64.lineTo(size.width * 0.2320503, size.height * 0.9599232);
    path_64.lineTo(size.width * 0.2360523, size.height * 0.9043465);
    path_64.lineTo(size.width * 0.2608497, size.height * 0.9043465);
    path_64.lineTo(size.width * 0.2538888, size.height * 0.9599232);
    path_64.close();
    path_64.moveTo(size.width * 0.2350954, size.height * 0.9547293);
    path_64.lineTo(size.width * 0.2518878, size.height * 0.9547293);
    path_64.lineTo(size.width * 0.2575431, size.height * 0.9095404);
    path_64.lineTo(size.width * 0.2384015, size.height * 0.9095404);
    path_64.lineTo(size.width * 0.2350954, size.height * 0.9547293);
    path_64.close();

    Paint paint_64_fill = Paint()..style = PaintingStyle.fill;
    paint_64_fill.color = const Color(0xff121212);
    canvas.drawPath(path_64, paint_64_fill);

    Path path_65 = Path();
    path_65.moveTo(size.width * 0.2842569, size.height * 0.9928505);
    path_65.cubicTo(
        size.width * 0.2842569,
        size.height * 0.9928505,
        size.width * 0.2869543,
        size.height * 0.9835010,
        size.width * 0.2815599,
        size.height * 0.9767485);
    path_65.cubicTo(
        size.width * 0.2815599,
        size.height * 0.9767485,
        size.width * 0.2597208,
        size.height * 0.9578768,
        size.width * 0.2536305,
        size.height * 0.9438525);
    path_65.cubicTo(
        size.width * 0.2536305,
        size.height * 0.9438525,
        size.width * 0.2455386,
        size.height * 0.9606465,
        size.width * 0.2342279,
        size.height * 0.9438525);
    path_65.lineTo(size.width * 0.2278761, size.height * 0.9928505);
    path_65.lineTo(size.width * 0.2842569, size.height * 0.9928505);
    path_65.close();

    Paint paint_65_fill = Paint()..style = PaintingStyle.fill;
    paint_65_fill.color = Colors.white;
    canvas.drawPath(path_65, paint_65_fill);

    Path path_66 = Path();
    path_66.moveTo(size.width * 0.2850376, size.height * 0.9954253);
    path_66.lineTo(size.width * 0.2262203, size.height * 0.9954253);
    path_66.lineTo(size.width * 0.2334421, size.height * 0.9395020);
    path_66.lineTo(size.width * 0.2350081, size.height * 0.9417535);
    path_66.cubicTo(
        size.width * 0.2452751,
        size.height * 0.9568162,
        size.width * 0.2524096,
        size.height * 0.9426192,
        size.width * 0.2526706,
        size.height * 0.9420990);
    path_66.lineTo(size.width * 0.2536279, size.height * 0.9400222);
    path_66.lineTo(size.width * 0.2545848, size.height * 0.9420990);
    path_66.cubicTo(
        size.width * 0.2604142,
        size.height * 0.9556040,
        size.width * 0.2818183,
        size.height * 0.9741303,
        size.width * 0.2820792,
        size.height * 0.9743030);
    path_66.lineTo(size.width * 0.2822533, size.height * 0.9744758);
    path_66.cubicTo(
        size.width * 0.2873868,
        size.height * 0.9808818,
        size.width * 0.2865168,
        size.height * 0.9900586,
        size.width * 0.2853858,
        size.height * 0.9940404);
    path_66.lineTo(size.width * 0.2850376, size.height * 0.9954253);
    path_66.close();
    path_66.moveTo(size.width * 0.2296137, size.height * 0.9902313);
    path_66.lineTo(size.width * 0.2834716, size.height * 0.9902313);
    path_66.cubicTo(
        size.width * 0.2838193,
        size.height * 0.9878071,
        size.width * 0.2841675,
        size.height * 0.9829596,
        size.width * 0.2810350,
        size.height * 0.9789778);
    path_66.cubicTo(
        size.width * 0.2792949,
        size.height * 0.9775929,
        size.width * 0.2605883,
        size.height * 0.9611444,
        size.width * 0.2536279,
        size.height * 0.9472939);
    path_66.cubicTo(
        size.width * 0.2511046,
        size.height * 0.9512758,
        size.width * 0.2441442,
        size.height * 0.9592404,
        size.width * 0.2350954,
        size.height * 0.9479859);
    path_66.lineTo(size.width * 0.2296137, size.height * 0.9902313);
    path_66.close();

    Paint paint_66_fill = Paint()..style = PaintingStyle.fill;
    paint_66_fill.color = const Color(0xff121212);
    canvas.drawPath(path_66, paint_66_fill);

    Path path_67 = Path();
    path_67.moveTo(size.width * 0.2618066, size.height * 0.2301566);
    path_67.lineTo(size.width * 0.2545848, size.height * 0.3172444);
    path_67.cubicTo(
        size.width * 0.2537147,
        size.height * 0.3281515,
        size.width * 0.2564990,
        size.height * 0.3390596,
        size.width * 0.2614584,
        size.height * 0.3440798);
    path_67.lineTo(size.width * 0.3052234, size.height * 0.3877101);
    path_67.lineTo(size.width * 0.3113137, size.height * 0.3478889);
    path_67.lineTo(size.width * 0.2888655, size.height * 0.2994111);
    path_67.lineTo(size.width * 0.2923462, size.height * 0.2490283);
    path_67.lineTo(size.width * 0.2939990, size.height * 0.2256545);
    path_67.lineTo(size.width * 0.2618066, size.height * 0.2301566);
    path_67.close();

    Paint paint_67_fill = Paint()..style = PaintingStyle.fill;
    paint_67_fill.color = Colors.white;
    canvas.drawPath(path_67, paint_67_fill);

    Path path_68 = Path();
    path_68.moveTo(size.width * 0.3060949, size.height * 0.3913455);
    path_68.lineTo(size.width * 0.2609381, size.height * 0.3463303);
    path_68.cubicTo(
        size.width * 0.2553695,
        size.height * 0.3407899,
        size.width * 0.2523244,
        size.height * 0.3290172,
        size.width * 0.2533685,
        size.height * 0.3167242);
    path_68.lineTo(size.width * 0.2606772, size.height * 0.2275586);
    path_68.lineTo(size.width * 0.2956543, size.height * 0.2225374);
    path_68.lineTo(size.width * 0.2903467, size.height * 0.2983717);
    path_68.lineTo(size.width * 0.3128817, size.height * 0.3470232);
    path_68.lineTo(size.width * 0.3060949, size.height * 0.3913455);
    path_68.close();
    path_68.moveTo(size.width * 0.2629391, size.height * 0.2325798);
    path_68.lineTo(size.width * 0.2558919, size.height * 0.3177626);
    path_68.cubicTo(
        size.width * 0.2551086,
        size.height * 0.3276313,
        size.width * 0.2575447,
        size.height * 0.3373273,
        size.width * 0.2620690,
        size.height * 0.3418293);
    path_68.lineTo(size.width * 0.3045289, size.height * 0.3840737);
    path_68.lineTo(size.width * 0.3099234, size.height * 0.3485808);
    path_68.lineTo(size.width * 0.2875624, size.height * 0.3002758);
    path_68.lineTo(size.width * 0.2925218, size.height * 0.2284242);
    path_68.lineTo(size.width * 0.2629391, size.height * 0.2325798);
    path_68.close();

    Paint paint_68_fill = Paint()..style = PaintingStyle.fill;
    paint_68_fill.color = const Color(0xff121212);
    canvas.drawPath(path_68, paint_68_fill);

    Path path_69 = Path();
    path_69.moveTo(size.width * 0.2299640, size.height * 0.9284414);
    path_69.lineTo(size.width * 0.2660721, size.height * 0.9284414);
    path_69.cubicTo(
        size.width * 0.2660721,
        size.height * 0.9284414,
        size.width * 0.3099239,
        size.height * 0.6470949,
        size.width * 0.3113157,
        size.height * 0.6238949);
    path_69.lineTo(size.width * 0.3000046, size.height * 0.9284414);
    path_69.lineTo(size.width * 0.3390711, size.height * 0.9284414);
    path_69.lineTo(size.width * 0.3636944, size.height * 0.6015596);
    path_69.lineTo(size.width * 0.3556025, size.height * 0.4015879);
    path_69.cubicTo(
        size.width * 0.3556025,
        size.height * 0.4015879,
        size.width * 0.2691173,
        size.height * 0.3856586,
        size.width * 0.2679858,
        size.height * 0.3920646);
    path_69.cubicTo(
        size.width * 0.2619827,
        size.height * 0.4277313,
        size.width * 0.2622437,
        size.height * 0.4613192,
        size.width * 0.2616345,
        size.height * 0.5179354);
    path_69.lineTo(size.width * 0.2638964, size.height * 0.6257990);
    path_69.cubicTo(
        size.width * 0.2639838,
        size.height * 0.6257990,
        size.width * 0.2286584,
        size.height * 0.9284414,
        size.width * 0.2299640,
        size.height * 0.9284414);
    path_69.close();

    Paint paint_69_fill = Paint()..style = PaintingStyle.fill;
    paint_69_fill.color = _activeColor;
    canvas.drawPath(path_69, paint_69_fill);

    Path path_70 = Path();
    path_70.moveTo(size.width * 0.2614604, size.height * 0.4310313);
    path_70.cubicTo(
        size.width * 0.2614604,
        size.height * 0.4310313,
        size.width * 0.2832122,
        size.height * 0.4483455,
        size.width * 0.3067041,
        size.height * 0.4483455);
    path_70.cubicTo(
        size.width * 0.3475980,
        size.height * 0.4483455,
        size.width * 0.3592569,
        size.height * 0.4362263,
        size.width * 0.3592569,
        size.height * 0.4362263);
    path_70.lineTo(size.width * 0.3549066, size.height * 0.2613586);
    path_70.lineTo(size.width * 0.3688279, size.height * 0.2613586);
    path_70.cubicTo(
        size.width * 0.3653472,
        size.height * 0.2319253,
        size.width * 0.3586477,
        size.height * 0.1985101,
        size.width * 0.3540365,
        size.height * 0.1799838);
    path_70.cubicTo(
        size.width * 0.3456838,
        size.height * 0.1462222,
        size.width * 0.3366350,
        size.height * 0.1418939,
        size.width * 0.3322843,
        size.height * 0.1367000);
    path_70.cubicTo(
        size.width * 0.3305442,
        size.height * 0.1346222,
        size.width * 0.3053990,
        size.height * 0.1313333,
        size.width * 0.3053990,
        size.height * 0.1313333);
    path_70.cubicTo(
        size.width * 0.2997437,
        size.height * 0.1325444,
        size.width * 0.2860832,
        size.height * 0.1370465,
        size.width * 0.2746853,
        size.height * 0.1654404);
    path_70.cubicTo(
        size.width * 0.2661589,
        size.height * 0.1869091,
        size.width * 0.2583279,
        size.height * 0.2388505,
        size.width * 0.2583279,
        size.height * 0.2388505);
    path_70.lineTo(size.width * 0.2750335, size.height * 0.2400626);
    path_70.lineTo(size.width * 0.2614604, size.height * 0.4310313);
    path_70.close();

    Paint paint_70_fill = Paint()..style = PaintingStyle.fill;
    paint_70_fill.color = Colors.white;
    canvas.drawPath(path_70, paint_70_fill);

    Path path_71 = Path();
    path_71.moveTo(size.width * 0.3067046, size.height * 0.4509253);
    path_71.cubicTo(
        size.width * 0.2832127,
        size.height * 0.4509253,
        size.width * 0.2612000,
        size.height * 0.4336121,
        size.width * 0.2609386,
        size.height * 0.4334384);
    path_71.lineTo(size.width * 0.2599817, size.height * 0.4327465);
    path_71.lineTo(size.width * 0.2734680, size.height * 0.2426424);
    path_71.lineTo(size.width * 0.2565015, size.height * 0.2414303);
    path_71.lineTo(size.width * 0.2570234, size.height * 0.2381404);
    path_71.cubicTo(
        size.width * 0.2573716,
        size.height * 0.2360636,
        size.width * 0.2650279,
        size.height * 0.1855071,
        size.width * 0.2736416,
        size.height * 0.1638657);
    path_71.cubicTo(
        size.width * 0.2818203,
        size.height * 0.1434354,
        size.width * 0.2924355,
        size.height * 0.1316626,
        size.width * 0.3052254,
        size.height * 0.1287192);
    path_71.lineTo(size.width * 0.3053127, size.height * 0.1287192);
    path_71.lineTo(size.width * 0.3053995, size.height * 0.1287192);
    path_71.cubicTo(
        size.width * 0.3130563,
        size.height * 0.1297576,
        size.width * 0.3309797,
        size.height * 0.1321818,
        size.width * 0.3328071,
        size.height * 0.1344323);
    path_71.cubicTo(
        size.width * 0.3334162,
        size.height * 0.1351253,
        size.width * 0.3341990,
        size.height * 0.1359909,
        size.width * 0.3350690,
        size.height * 0.1368566);
    path_71.cubicTo(
        size.width * 0.3398548,
        size.height * 0.1417040,
        size.width * 0.3478594,
        size.height * 0.1496687,
        size.width * 0.3550807,
        size.height * 0.1789283);
    path_71.cubicTo(
        size.width * 0.3600401,
        size.height * 0.1990121,
        size.width * 0.3665660,
        size.height * 0.2322545,
        size.width * 0.3699594,
        size.height * 0.2608222);
    path_71.lineTo(size.width * 0.3703071, size.height * 0.2639384);
    path_71.lineTo(size.width * 0.3561249, size.height * 0.2639384);
    path_71.lineTo(size.width * 0.3604756, size.height * 0.4377667);
    path_71.lineTo(size.width * 0.3597792, size.height * 0.4384596);
    path_71.cubicTo(
        size.width * 0.3593442,
        size.height * 0.4389788,
        size.width * 0.3474244,
        size.height * 0.4509253,
        size.width * 0.3067046,
        size.height * 0.4509253);
    path_71.close();
    path_71.moveTo(size.width * 0.2629401, size.height * 0.4292838);
    path_71.cubicTo(
        size.width * 0.2673772,
        size.height * 0.4325727,
        size.width * 0.2865188,
        size.height * 0.4457313,
        size.width * 0.3067046,
        size.height * 0.4457313);
    path_71.cubicTo(
        size.width * 0.3411599,
        size.height * 0.4457313,
        size.width * 0.3547330,
        size.height * 0.4370747,
        size.width * 0.3578650,
        size.height * 0.4344778);
    path_71.lineTo(size.width * 0.3535147, size.height * 0.2587444);
    path_71.lineTo(size.width * 0.3671751, size.height * 0.2587444);
    path_71.cubicTo(
        size.width * 0.3637817,
        size.height * 0.2312152,
        size.width * 0.3576041,
        size.height * 0.2002242,
        size.width * 0.3529056,
        size.height * 0.1811788);
    path_71.cubicTo(
        size.width * 0.3460320,
        size.height * 0.1534778,
        size.width * 0.3388975,
        size.height * 0.1463788,
        size.width * 0.3341122,
        size.height * 0.1415313);
    path_71.cubicTo(
        size.width * 0.3332421,
        size.height * 0.1406657,
        size.width * 0.3323721,
        size.height * 0.1397990,
        size.width * 0.3316761,
        size.height * 0.1389333);
    path_71.cubicTo(
        size.width * 0.3302838,
        size.height * 0.1377222,
        size.width * 0.3171457,
        size.height * 0.1354707,
        size.width * 0.3055736,
        size.height * 0.1340859);
    path_71.cubicTo(
        size.width * 0.2935665,
        size.height * 0.1368566,
        size.width * 0.2835609,
        size.height * 0.1479374,
        size.width * 0.2758173,
        size.height * 0.1671545);
    path_71.cubicTo(
        size.width * 0.2686822,
        size.height * 0.1848152,
        size.width * 0.2619827,
        size.height * 0.2248091,
        size.width * 0.2601558,
        size.height * 0.2364091);
    path_71.lineTo(size.width * 0.2766000, size.height * 0.2376212);
    path_71.lineTo(size.width * 0.2629401, size.height * 0.4292838);
    path_71.close();

    Paint paint_71_fill = Paint()..style = PaintingStyle.fill;
    paint_71_fill.color = const Color(0xff121212);
    canvas.drawPath(path_71, paint_71_fill);

    Path path_72 = Path();
    path_72.moveTo(size.width * 0.3338508, size.height * 0.2623778);
    path_72.lineTo(size.width * 0.3365477, size.height * 0.2777879);
    path_72.lineTo(size.width * 0.3493381, size.height * 0.3501586);
    path_72.cubicTo(
        size.width * 0.3508168,
        size.height * 0.3596808,
        size.width * 0.3549061,
        size.height * 0.3666061,
        size.width * 0.3598660,
        size.height * 0.3683374);
    path_72.lineTo(size.width * 0.4207711, size.height * 0.3879020);
    path_72.lineTo(size.width * 0.4611426, size.height * 0.3879020);
    path_72.lineTo(size.width * 0.4579234, size.height * 0.3804576);
    path_72.cubicTo(
        size.width * 0.4565310,
        size.height * 0.3773404,
        size.width * 0.4547909,
        size.height * 0.3749172,
        size.width * 0.4527898,
        size.height * 0.3735313);
    path_72.lineTo(size.width * 0.4317340, size.height * 0.3589879);
    path_72.cubicTo(
        size.width * 0.4305157,
        size.height * 0.3581222,
        size.width * 0.4291239,
        size.height * 0.3577768,
        size.width * 0.4278188,
        size.height * 0.3581222);
    path_72.lineTo(size.width * 0.4181609, size.height * 0.3603737);
    path_72.lineTo(size.width * 0.3708289, size.height * 0.3229758);
    path_72.lineTo(size.width * 0.3631721, size.height * 0.2656677);
    path_72.lineTo(size.width * 0.3625629, size.height * 0.2611667);
    path_72.lineTo(size.width * 0.3338508, size.height * 0.2623778);
    path_72.close();

    Paint paint_72_fill = Paint()..style = PaintingStyle.fill;
    paint_72_fill.color = Colors.white;
    canvas.drawPath(path_72, paint_72_fill);

    Path path_73 = Path();
    path_73.moveTo(size.width * 0.4640142, size.height * 0.3903101);
    path_73.lineTo(size.width * 0.4205975, size.height * 0.3903101);
    path_73.lineTo(size.width * 0.3596924, size.height * 0.3707455);
    path_73.cubicTo(
        size.width * 0.3542107,
        size.height * 0.3690141,
        size.width * 0.3497736,
        size.height * 0.3613960,
        size.width * 0.3481203,
        size.height * 0.3508354);
    path_73.lineTo(size.width * 0.3320239, size.height * 0.2597657);
    path_73.lineTo(size.width * 0.3635208, size.height * 0.2582071);
    path_73.lineTo(size.width * 0.3718731, size.height * 0.3207091);
    path_73.lineTo(size.width * 0.4183355, size.height * 0.3574141);
    path_73.lineTo(size.width * 0.4277320, size.height * 0.3553364);
    path_73.cubicTo(
        size.width * 0.4292112,
        size.height * 0.3549899,
        size.width * 0.4307777,
        size.height * 0.3553364,
        size.width * 0.4321695,
        size.height * 0.3563758);
    path_73.lineTo(size.width * 0.4532254, size.height * 0.3709192);
    path_73.cubicTo(
        size.width * 0.4554005,
        size.height * 0.3724768,
        size.width * 0.4574020,
        size.height * 0.3750737,
        size.width * 0.4589680,
        size.height * 0.3787101);
    path_73.lineTo(size.width * 0.4640142, size.height * 0.3903101);
    path_73.close();
    path_73.moveTo(size.width * 0.4208584, size.height * 0.3851162);
    path_73.lineTo(size.width * 0.4582721, size.height * 0.3851162);
    path_73.lineTo(size.width * 0.4568797, size.height * 0.3820000);
    path_73.cubicTo(
        size.width * 0.4556614,
        size.height * 0.3792293,
        size.width * 0.4540954,
        size.height * 0.3769788,
        size.width * 0.4522685,
        size.height * 0.3757667);
    path_73.lineTo(size.width * 0.4312127, size.height * 0.3612232);
    path_73.cubicTo(
        size.width * 0.4301685,
        size.height * 0.3605303,
        size.width * 0.4289503,
        size.height * 0.3601838,
        size.width * 0.4279061,
        size.height * 0.3605303);
    path_73.lineTo(size.width * 0.4179005, size.height * 0.3627818);
    path_73.lineTo(size.width * 0.4176391, size.height * 0.3626081);
    path_73.lineTo(size.width * 0.3696980, size.height * 0.3246909);
    path_73.lineTo(size.width * 0.3616066, size.height * 0.2635747);
    path_73.lineTo(size.width * 0.3357650, size.height * 0.2647859);
    path_73.lineTo(size.width * 0.3506437, size.height * 0.3492768);
    path_73.cubicTo(
        size.width * 0.3520355,
        size.height * 0.3579333,
        size.width * 0.3556898,
        size.height * 0.3641667,
        size.width * 0.3601274,
        size.height * 0.3657242);
    path_73.lineTo(size.width * 0.4208584, size.height * 0.3851162);
    path_73.close();

    Paint paint_73_fill = Paint()..style = PaintingStyle.fill;
    paint_73_fill.color = const Color(0xff121212);
    canvas.drawPath(path_73, paint_73_fill);

    Path path_74 = Path();
    path_74.moveTo(size.width * 0.3292132, size.height * 0.2275687);
    path_74.lineTo(size.width * 0.3267492, size.height * 0.2292848);
    path_74.lineTo(size.width * 0.3328437, size.height * 0.2639303);
    path_74.lineTo(size.width * 0.3353076, size.height * 0.2622141);
    path_74.lineTo(size.width * 0.3292132, size.height * 0.2275687);
    path_74.close();

    Paint paint_74_fill = Paint()..style = PaintingStyle.fill;
    paint_74_fill.color = const Color(0xff121212);
    canvas.drawPath(path_74, paint_74_fill);

    Path path_75 = Path();
    path_75.moveTo(size.width * 0.2774701, size.height * 0.2041899);
    path_75.lineTo(size.width * 0.2750335, size.height * 0.2402030);
    path_75.lineTo(size.width * 0.2774701, size.height * 0.2041899);
    path_75.close();

    Paint paint_75_fill = Paint()..style = PaintingStyle.fill;
    paint_75_fill.color = Colors.white;
    canvas.drawPath(path_75, paint_75_fill);

    Path path_76 = Path();
    path_76.moveTo(size.width * 0.2761350, size.height * 0.2037505);
    path_76.lineTo(size.width * 0.2736558, size.height * 0.2397707);
    path_76.lineTo(size.width * 0.2762416, size.height * 0.2404758);
    path_76.lineTo(size.width * 0.2787208, size.height * 0.2044556);
    path_76.lineTo(size.width * 0.2761350, size.height * 0.2037505);
    path_76.close();

    Paint paint_76_fill = Paint()..style = PaintingStyle.fill;
    paint_76_fill.color = const Color(0xff121212);
    canvas.drawPath(path_76, paint_76_fill);

    Path path_77 = Path();
    path_77.moveTo(size.width * 0.3056589, size.height * 0.4722222);
    path_77.lineTo(size.width * 0.3110538, size.height * 0.6259677);
    path_77.lineTo(size.width * 0.3056589, size.height * 0.4722222);
    path_77.close();

    Paint paint_77_fill = Paint()..style = PaintingStyle.fill;
    paint_77_fill.color = Colors.white;
    canvas.drawPath(path_77, paint_77_fill);

    Path path_78 = Path();
    path_78.moveTo(size.width * 0.3069741, size.height * 0.4720687);
    path_78.lineTo(size.width * 0.3043701, size.height * 0.4724293);
    path_78.lineTo(size.width * 0.3097508, size.height * 0.6261505);
    path_78.lineTo(size.width * 0.3123548, size.height * 0.6257899);
    path_78.lineTo(size.width * 0.3069741, size.height * 0.4720687);
    path_78.close();

    Paint paint_78_fill = Paint()..style = PaintingStyle.fill;
    paint_78_fill.color = const Color(0xff121212);
    canvas.drawPath(path_78, paint_78_fill);

    Path path_79 = Path();
    path_79.moveTo(size.width * 0.1213741, size.height * 0.9916566);
    path_79.cubicTo(
        size.width * 0.1251706,
        size.height * 0.9916566,
        size.width * 0.1282477,
        size.height * 0.9855333,
        size.width * 0.1282477,
        size.height * 0.9779788);
    path_79.cubicTo(
        size.width * 0.1282477,
        size.height * 0.9704253,
        size.width * 0.1251706,
        size.height * 0.9643010,
        size.width * 0.1213741,
        size.height * 0.9643010);
    path_79.cubicTo(
        size.width * 0.1175782,
        size.height * 0.9643010,
        size.width * 0.1145005,
        size.height * 0.9704253,
        size.width * 0.1145005,
        size.height * 0.9779788);
    path_79.cubicTo(
        size.width * 0.1145005,
        size.height * 0.9855333,
        size.width * 0.1175782,
        size.height * 0.9916566,
        size.width * 0.1213741,
        size.height * 0.9916566);
    path_79.close();

    Paint paint_79_fill = Paint()..style = PaintingStyle.fill;
    paint_79_fill.color = const Color(0xff121212);
    canvas.drawPath(path_79, paint_79_fill);

    Path path_80 = Path();
    path_80.moveTo(size.width * 0.1407766, size.height * 0.9919828);
    path_80.cubicTo(
        size.width * 0.1445731,
        size.height * 0.9919828,
        size.width * 0.1476503,
        size.height * 0.9858586,
        size.width * 0.1476503,
        size.height * 0.9783051);
    path_80.cubicTo(
        size.width * 0.1476503,
        size.height * 0.9707505,
        size.width * 0.1445731,
        size.height * 0.9646263,
        size.width * 0.1407766,
        size.height * 0.9646263);
    path_80.cubicTo(
        size.width * 0.1369807,
        size.height * 0.9646263,
        size.width * 0.1339030,
        size.height * 0.9707505,
        size.width * 0.1339030,
        size.height * 0.9783051);
    path_80.cubicTo(
        size.width * 0.1339030,
        size.height * 0.9858586,
        size.width * 0.1369807,
        size.height * 0.9919828,
        size.width * 0.1407766,
        size.height * 0.9919828);
    path_80.close();

    Paint paint_80_fill = Paint()..style = PaintingStyle.fill;
    paint_80_fill.color = const Color(0xff121212);
    canvas.drawPath(path_80, paint_80_fill);

    Path path_81 = Path();
    path_81.moveTo(size.width * 0.1857629, size.height * 0.9916566);
    path_81.cubicTo(
        size.width * 0.1895594,
        size.height * 0.9916566,
        size.width * 0.1926365,
        size.height * 0.9855333,
        size.width * 0.1926365,
        size.height * 0.9779788);
    path_81.cubicTo(
        size.width * 0.1926365,
        size.height * 0.9704253,
        size.width * 0.1895594,
        size.height * 0.9643010,
        size.width * 0.1857629,
        size.height * 0.9643010);
    path_81.cubicTo(
        size.width * 0.1819670,
        size.height * 0.9643010,
        size.width * 0.1788893,
        size.height * 0.9704253,
        size.width * 0.1788893,
        size.height * 0.9779788);
    path_81.cubicTo(
        size.width * 0.1788893,
        size.height * 0.9855333,
        size.width * 0.1819670,
        size.height * 0.9916566,
        size.width * 0.1857629,
        size.height * 0.9916566);
    path_81.close();

    Paint paint_81_fill = Paint()..style = PaintingStyle.fill;
    paint_81_fill.color = const Color(0xff121212);
    canvas.drawPath(path_81, paint_81_fill);

    Path path_82 = Path();
    path_82.moveTo(size.width * 0.1836761, size.height * 0.6652667);
    path_82.lineTo(size.width * 0.1793259, size.height * 0.6652667);
    path_82.lineTo(size.width * 0.1793259, size.height * 0.5435515);
    path_82.cubicTo(
        size.width * 0.1793259,
        size.height * 0.5343758,
        size.width * 0.1755843,
        size.height * 0.5267576,
        size.width * 0.1708858,
        size.height * 0.5267576);
    path_82.lineTo(size.width * 0.1476548, size.height * 0.5267576);
    path_82.cubicTo(
        size.width * 0.1430437,
        size.height * 0.5267576,
        size.width * 0.1392152,
        size.height * 0.5342030,
        size.width * 0.1392152,
        size.height * 0.5435515);
    path_82.lineTo(size.width * 0.1392152, size.height * 0.6652667);
    path_82.lineTo(size.width * 0.1348650, size.height * 0.6652667);
    path_82.lineTo(size.width * 0.1348650, size.height * 0.5435515);
    path_82.cubicTo(
        size.width * 0.1348650,
        size.height * 0.5295283,
        size.width * 0.1406071,
        size.height * 0.5181010,
        size.width * 0.1476548,
        size.height * 0.5181010);
    path_82.lineTo(size.width * 0.1708858, size.height * 0.5181010);
    path_82.cubicTo(
        size.width * 0.1779335,
        size.height * 0.5181010,
        size.width * 0.1836761,
        size.height * 0.5295283,
        size.width * 0.1836761,
        size.height * 0.5435515);
    path_82.lineTo(size.width * 0.1836761, size.height * 0.6652667);
    path_82.close();

    Paint paint_82_fill = Paint()..style = PaintingStyle.fill;
    paint_82_fill.color = const Color(0xff121212);
    canvas.drawPath(path_82, paint_82_fill);

    Path path_83 = Path();
    path_83.moveTo(size.width * 0.2051655, size.height * 0.9919828);
    path_83.cubicTo(
        size.width * 0.2089614,
        size.height * 0.9919828,
        size.width * 0.2120391,
        size.height * 0.9858586,
        size.width * 0.2120391,
        size.height * 0.9783051);
    path_83.cubicTo(
        size.width * 0.2120391,
        size.height * 0.9707505,
        size.width * 0.2089614,
        size.height * 0.9646263,
        size.width * 0.2051655,
        size.height * 0.9646263);
    path_83.cubicTo(
        size.width * 0.2013690,
        size.height * 0.9646263,
        size.width * 0.1982919,
        size.height * 0.9707505,
        size.width * 0.1982919,
        size.height * 0.9783051);
    path_83.cubicTo(
        size.width * 0.1982919,
        size.height * 0.9858586,
        size.width * 0.2013690,
        size.height * 0.9919828,
        size.width * 0.2051655,
        size.height * 0.9919828);
    path_83.close();

    Paint paint_83_fill = Paint()..style = PaintingStyle.fill;
    paint_83_fill.color = const Color(0xff121212);
    canvas.drawPath(path_83, paint_83_fill);

    Path path_84 = Path();
    path_84.moveTo(size.width * 0.1352112, size.height * 0.6535081);
    path_84.lineTo(size.width * 0.2077756, size.height * 0.6535081);
    path_84.cubicTo(
        size.width * 0.2160416,
        size.height * 0.6535081,
        size.width * 0.2227411,
        size.height * 0.6670121,
        size.width * 0.2227411,
        size.height * 0.6836333);
    path_84.lineTo(size.width * 0.2227411, size.height * 0.9454152);
    path_84.cubicTo(
        size.width * 0.2227411,
        size.height * 0.9620364,
        size.width * 0.2160416,
        size.height * 0.9755414,
        size.width * 0.2077756,
        size.height * 0.9755414);
    path_84.lineTo(size.width * 0.1352112, size.height * 0.9755414);
    path_84.cubicTo(
        size.width * 0.1269457,
        size.height * 0.9755414,
        size.width * 0.1202462,
        size.height * 0.9620364,
        size.width * 0.1202462,
        size.height * 0.9454152);
    path_84.lineTo(size.width * 0.1202462, size.height * 0.6836333);
    path_84.cubicTo(
        size.width * 0.1202462,
        size.height * 0.6670121,
        size.width * 0.1269457,
        size.height * 0.6535081,
        size.width * 0.1352112,
        size.height * 0.6535081);
    path_84.close();

    Paint paint_84_fill = Paint()..style = PaintingStyle.fill;
    paint_84_fill.color = const Color(0xff121212);
    canvas.drawPath(path_84, paint_84_fill);

    Path path_85 = Path();
    path_85.moveTo(size.width * 0.2025553, size.height * 0.6865677);
    path_85.lineTo(size.width * 0.2025553, size.height * 0.9459263);
    path_85.cubicTo(
        size.width * 0.2025553,
        size.height * 0.9623737,
        size.width * 0.1958558,
        size.height * 0.9757051,
        size.width * 0.1875904,
        size.height * 0.9757051);
    path_85.lineTo(size.width * 0.1150259, size.height * 0.9757051);
    path_85.cubicTo(
        size.width * 0.1069340,
        size.height * 0.9757051,
        size.width * 0.1003213,
        size.height * 0.9628929,
        size.width * 0.1000604,
        size.height * 0.9467919);
    path_85.cubicTo(
        size.width * 0.1000604,
        size.height * 0.9464455,
        size.width * 0.1000604,
        size.height * 0.9460990,
        size.width * 0.1000604,
        size.height * 0.9459263);
    path_85.lineTo(size.width * 0.1000604, size.height * 0.6865677);
    path_85.cubicTo(
        size.width * 0.1000604,
        size.height * 0.6858747,
        size.width * 0.1000604,
        size.height * 0.6851828,
        size.width * 0.1000604,
        size.height * 0.6844899);
    path_85.cubicTo(
        size.width * 0.1005827,
        size.height * 0.6689081,
        size.width * 0.1071081,
        size.height * 0.6566152,
        size.width * 0.1150259,
        size.height * 0.6566152);
    path_85.lineTo(size.width * 0.1875904, size.height * 0.6566152);
    path_85.cubicTo(
        size.width * 0.1958558,
        size.height * 0.6566152,
        size.width * 0.2025553,
        size.height * 0.6699465,
        size.width * 0.2025553,
        size.height * 0.6865677);
    path_85.close();

    Paint paint_85_fill = Paint()..style = PaintingStyle.fill;
    paint_85_fill.color = Colors.white;
    canvas.drawPath(path_85, paint_85_fill);

    Path path_86 = Path();
    path_86.moveTo(size.width * 0.1875863, size.height * 0.9781121);
    path_86.lineTo(size.width * 0.1150223, size.height * 0.9781121);
    path_86.cubicTo(
        size.width * 0.1061472,
        size.height * 0.9781121,
        size.width * 0.09901269,
        size.height * 0.9642616,
        size.width * 0.09875178,
        size.height * 0.9466010);
    path_86.cubicTo(
        size.width * 0.09875178,
        size.height * 0.9462556,
        size.width * 0.09875178,
        size.height * 0.9459091,
        size.width * 0.09875178,
        size.height * 0.9455626);
    path_86.lineTo(size.width * 0.09875178, size.height * 0.6865505);
    path_86.cubicTo(
        size.width * 0.09875178,
        size.height * 0.6858576,
        size.width * 0.09875178,
        size.height * 0.6851657,
        size.width * 0.09875178,
        size.height * 0.6843000);
    path_86.cubicTo(
        size.width * 0.09936091,
        size.height * 0.6673323,
        size.width * 0.1064954,
        size.height * 0.6540010,
        size.width * 0.1150223,
        size.height * 0.6540010);
    path_86.lineTo(size.width * 0.1875863, size.height * 0.6540010);
    path_86.cubicTo(
        size.width * 0.1965482,
        size.height * 0.6540010,
        size.width * 0.2038569,
        size.height * 0.6685444,
        size.width * 0.2038569,
        size.height * 0.6863778);
    path_86.lineTo(size.width * 0.2038569, size.height * 0.9457354);
    path_86.cubicTo(
        size.width * 0.2038569,
        size.height * 0.9635687,
        size.width * 0.1965482,
        size.height * 0.9781121,
        size.width * 0.1875863,
        size.height * 0.9781121);
    path_86.close();
    path_86.moveTo(size.width * 0.1150223, size.height * 0.6591949);
    path_86.cubicTo(
        size.width * 0.1078005,
        size.height * 0.6591949,
        size.width * 0.1018843,
        size.height * 0.6702758,
        size.width * 0.1013619,
        size.height * 0.6846465);
    path_86.cubicTo(
        size.width * 0.1013619,
        size.height * 0.6853384,
        size.width * 0.1013619,
        size.height * 0.6858576,
        size.width * 0.1013619,
        size.height * 0.6865505);
    path_86.lineTo(size.width * 0.1013619, size.height * 0.9466010);
    path_86.cubicTo(
        size.width * 0.1016228,
        size.height * 0.9614909,
        size.width * 0.1076264,
        size.height * 0.9730909,
        size.width * 0.1150223,
        size.height * 0.9730909);
    path_86.lineTo(size.width * 0.1875863, size.height * 0.9730909);
    path_86.cubicTo(
        size.width * 0.1951563,
        size.height * 0.9730909,
        size.width * 0.2012467,
        size.height * 0.9607990,
        size.width * 0.2012467,
        size.height * 0.9459091);
    path_86.lineTo(size.width * 0.2012467, size.height * 0.6865505);
    path_86.cubicTo(
        size.width * 0.2012467,
        size.height * 0.6714879,
        size.width * 0.1950690,
        size.height * 0.6593687,
        size.width * 0.1875863,
        size.height * 0.6593687);
    path_86.lineTo(size.width * 0.1150223, size.height * 0.6593687);
    path_86.lineTo(size.width * 0.1150223, size.height * 0.6591949);
    path_86.close();

    Paint paint_86_fill = Paint()..style = PaintingStyle.fill;
    paint_86_fill.color = const Color(0xff121212);
    canvas.drawPath(path_86, paint_86_fill);

    Path path_87 = Path();
    path_87.moveTo(size.width * 0.1855888, size.height * 0.7165071);
    path_87.lineTo(size.width * 0.1855888, size.height * 0.9204606);
    path_87.cubicTo(
        size.width * 0.1855888,
        size.height * 0.9317152,
        size.width * 0.1810645,
        size.height * 0.9407182,
        size.width * 0.1754091,
        size.height * 0.9407182);
    path_87.lineTo(size.width * 0.1102406, size.height * 0.9407182);
    path_87.cubicTo(
        size.width * 0.1045848,
        size.height * 0.9407182,
        size.width * 0.1000604,
        size.height * 0.9317152,
        size.width * 0.1000604,
        size.height * 0.9204606);
    path_87.lineTo(size.width * 0.1000604, size.height * 0.7165071);
    path_87.cubicTo(
        size.width * 0.1000604,
        size.height * 0.7052525,
        size.width * 0.1045848,
        size.height * 0.6962495,
        size.width * 0.1102406,
        size.height * 0.6962495);
    path_87.lineTo(size.width * 0.1754091, size.height * 0.6962495);
    path_87.cubicTo(
        size.width * 0.1809777,
        size.height * 0.6962495,
        size.width * 0.1855888,
        size.height * 0.7052525,
        size.width * 0.1855888,
        size.height * 0.7165071);
    path_87.close();

    Paint paint_87_fill = Paint()..style = PaintingStyle.fill;
    paint_87_fill.color = Colors.white;
    canvas.drawPath(path_87, paint_87_fill);

    Path path_88 = Path();
    path_88.moveTo(size.width * 0.1754056, size.height * 0.9433273);
    path_88.lineTo(size.width * 0.1102365, size.height * 0.9433273);
    path_88.cubicTo(
        size.width * 0.1038853,
        size.height * 0.9433273,
        size.width * 0.09875178,
        size.height * 0.9331121,
        size.width * 0.09875178,
        size.height * 0.9204737);
    path_88.lineTo(size.width * 0.09875178, size.height * 0.7165192);
    path_88.cubicTo(
        size.width * 0.09875178,
        size.height * 0.7038798,
        size.width * 0.1038853,
        size.height * 0.6936657,
        size.width * 0.1102365,
        size.height * 0.6936657);
    path_88.lineTo(size.width * 0.1754056, size.height * 0.6936657);
    path_88.cubicTo(
        size.width * 0.1817569,
        size.height * 0.6936657,
        size.width * 0.1868904,
        size.height * 0.7038798,
        size.width * 0.1868904,
        size.height * 0.7165192);
    path_88.lineTo(size.width * 0.1868904, size.height * 0.9204737);
    path_88.cubicTo(
        size.width * 0.1868904,
        size.height * 0.9331121,
        size.width * 0.1817569,
        size.height * 0.9433273,
        size.width * 0.1754056,
        size.height * 0.9433273);
    path_88.close();
    path_88.moveTo(size.width * 0.1102365, size.height * 0.6988596);
    path_88.cubicTo(
        size.width * 0.1053645,
        size.height * 0.6988596,
        size.width * 0.1013619,
        size.height * 0.7068232,
        size.width * 0.1013619,
        size.height * 0.7165192);
    path_88.lineTo(size.width * 0.1013619, size.height * 0.9204737);
    path_88.cubicTo(
        size.width * 0.1013619,
        size.height * 0.9301697,
        size.width * 0.1053645,
        size.height * 0.9381333,
        size.width * 0.1102365,
        size.height * 0.9381333);
    path_88.lineTo(size.width * 0.1754056, size.height * 0.9381333);
    path_88.cubicTo(
        size.width * 0.1802777,
        size.height * 0.9381333,
        size.width * 0.1842802,
        size.height * 0.9301697,
        size.width * 0.1842802,
        size.height * 0.9204737);
    path_88.lineTo(size.width * 0.1842802, size.height * 0.7165192);
    path_88.cubicTo(
        size.width * 0.1842802,
        size.height * 0.7068232,
        size.width * 0.1802777,
        size.height * 0.6988596,
        size.width * 0.1754056,
        size.height * 0.6988596);
    path_88.lineTo(size.width * 0.1102365, size.height * 0.6988596);
    path_88.lineTo(size.width * 0.1102365, size.height * 0.6988596);
    path_88.close();

    Paint paint_88_fill = Paint()..style = PaintingStyle.fill;
    paint_88_fill.color = const Color(0xff121212);
    canvas.drawPath(path_88, paint_88_fill);

    Path path_89 = Path();
    path_89.moveTo(size.width * 0.1671442, size.height * 0.7191253);
    path_89.cubicTo(
        size.width * 0.1690584,
        size.height * 0.7191253,
        size.width * 0.1707112,
        size.height * 0.7222414,
        size.width * 0.1707112,
        size.height * 0.7262232);
    path_89.lineTo(size.width * 0.1707112, size.height * 0.9168465);
    path_89.cubicTo(
        size.width * 0.1707112,
        size.height * 0.9206556,
        size.width * 0.1691452,
        size.height * 0.9239455,
        size.width * 0.1671442,
        size.height * 0.9239455);
    path_89.cubicTo(
        size.width * 0.1652299,
        size.height * 0.9239455,
        size.width * 0.1635766,
        size.height * 0.9208283,
        size.width * 0.1635766,
        size.height * 0.9168465);
    path_89.lineTo(size.width * 0.1635766, size.height * 0.7260505);
    path_89.cubicTo(
        size.width * 0.1635766,
        size.height * 0.7222414,
        size.width * 0.1652299,
        size.height * 0.7191253,
        size.width * 0.1671442,
        size.height * 0.7191253);
    path_89.close();

    Paint paint_89_fill = Paint()..style = PaintingStyle.fill;
    paint_89_fill.color = _activeColor;
    canvas.drawPath(path_89, paint_89_fill);

    Path path_90 = Path();
    path_90.moveTo(size.width * 0.1497396, size.height * 0.7191253);
    path_90.cubicTo(
        size.width * 0.1516538,
        size.height * 0.7191253,
        size.width * 0.1533066,
        size.height * 0.7222414,
        size.width * 0.1533066,
        size.height * 0.7262232);
    path_90.lineTo(size.width * 0.1533066, size.height * 0.9168465);
    path_90.cubicTo(
        size.width * 0.1533066,
        size.height * 0.9206556,
        size.width * 0.1517406,
        size.height * 0.9239455,
        size.width * 0.1497396,
        size.height * 0.9239455);
    path_90.cubicTo(
        size.width * 0.1478254,
        size.height * 0.9239455,
        size.width * 0.1461721,
        size.height * 0.9208283,
        size.width * 0.1461721,
        size.height * 0.9168465);
    path_90.lineTo(size.width * 0.1461721, size.height * 0.7260505);
    path_90.cubicTo(
        size.width * 0.1462589,
        size.height * 0.7222414,
        size.width * 0.1478254,
        size.height * 0.7191253,
        size.width * 0.1497396,
        size.height * 0.7191253);
    path_90.close();

    Paint paint_90_fill = Paint()..style = PaintingStyle.fill;
    paint_90_fill.color = _activeColor;
    canvas.drawPath(path_90, paint_90_fill);

    Path path_91 = Path();
    path_91.moveTo(size.width * 0.1323396, size.height * 0.7191253);
    path_91.cubicTo(
        size.width * 0.1342538,
        size.height * 0.7191253,
        size.width * 0.1359071,
        size.height * 0.7222414,
        size.width * 0.1359071,
        size.height * 0.7262232);
    path_91.lineTo(size.width * 0.1359071, size.height * 0.9168465);
    path_91.cubicTo(
        size.width * 0.1359071,
        size.height * 0.9206556,
        size.width * 0.1343411,
        size.height * 0.9239455,
        size.width * 0.1323396,
        size.height * 0.9239455);
    path_91.cubicTo(
        size.width * 0.1304254,
        size.height * 0.9239455,
        size.width * 0.1287726,
        size.height * 0.9208283,
        size.width * 0.1287726,
        size.height * 0.9168465);
    path_91.lineTo(size.width * 0.1287726, size.height * 0.7260505);
    path_91.cubicTo(
        size.width * 0.1288594,
        size.height * 0.7222414,
        size.width * 0.1304254,
        size.height * 0.7191253,
        size.width * 0.1323396,
        size.height * 0.7191253);
    path_91.close();

    Paint paint_91_fill = Paint()..style = PaintingStyle.fill;
    paint_91_fill.color = _activeColor;
    canvas.drawPath(path_91, paint_91_fill);

    Path path_92 = Path();
    path_92.moveTo(size.width * 0.1149350, size.height * 0.7191253);
    path_92.cubicTo(
        size.width * 0.1168492,
        size.height * 0.7191253,
        size.width * 0.1185025,
        size.height * 0.7222414,
        size.width * 0.1185025,
        size.height * 0.7262232);
    path_92.lineTo(size.width * 0.1185025, size.height * 0.9168465);
    path_92.cubicTo(
        size.width * 0.1185025,
        size.height * 0.9206556,
        size.width * 0.1169360,
        size.height * 0.9239455,
        size.width * 0.1149350,
        size.height * 0.9239455);
    path_92.cubicTo(
        size.width * 0.1130208,
        size.height * 0.9239455,
        size.width * 0.1113680,
        size.height * 0.9208283,
        size.width * 0.1113680,
        size.height * 0.9168465);
    path_92.lineTo(size.width * 0.1113680, size.height * 0.7260505);
    path_92.cubicTo(
        size.width * 0.1114548,
        size.height * 0.7222414,
        size.width * 0.1130208,
        size.height * 0.7191253,
        size.width * 0.1149350,
        size.height * 0.7191253);
    path_92.close();

    Paint paint_92_fill = Paint()..style = PaintingStyle.fill;
    paint_92_fill.color = _activeColor;
    canvas.drawPath(path_92, paint_92_fill);

    Path path_93 = Path();
    path_93.moveTo(size.width * 0.1718406, size.height * 0.5227556);
    path_93.cubicTo(
        size.width * 0.1718406,
        size.height * 0.5270838,
        size.width * 0.1701005,
        size.height * 0.5305465,
        size.width * 0.1679254,
        size.height * 0.5305465);
    path_93.lineTo(size.width * 0.1505239, size.height * 0.5305465);
    path_93.cubicTo(
        size.width * 0.1483482,
        size.height * 0.5305465,
        size.width * 0.1466081,
        size.height * 0.5270838,
        size.width * 0.1466081,
        size.height * 0.5227556);
    path_93.cubicTo(
        size.width * 0.1466081,
        size.height * 0.5184273,
        size.width * 0.1483482,
        size.height * 0.5149636,
        size.width * 0.1505239,
        size.height * 0.5149636);
    path_93.lineTo(size.width * 0.1679254, size.height * 0.5149636);
    path_93.cubicTo(
        size.width * 0.1701005,
        size.height * 0.5149636,
        size.width * 0.1718406,
        size.height * 0.5184273,
        size.width * 0.1718406,
        size.height * 0.5227556);
    path_93.close();

    Paint paint_93_fill = Paint()..style = PaintingStyle.fill;
    paint_93_fill.color = _activeColor;
    canvas.drawPath(path_93, paint_93_fill);

    Path path_94 = Path();
    path_94.moveTo(size.width * 0.8883452, size.height * 0.9911121);
    path_94.lineTo(size.width * 0.07917614, size.height * 0.9911121);
    path_94.lineTo(size.width * 0.07917614, size.height * 0.9963061);
    path_94.lineTo(size.width * 0.8883452, size.height * 0.9963061);
    path_94.lineTo(size.width * 0.8883452, size.height * 0.9911121);
    path_94.close();

    Paint paint_94_fill = Paint()..style = PaintingStyle.fill;
    paint_94_fill.color = const Color(0xff121212);
    canvas.drawPath(path_94, paint_94_fill);
  }

  @override
  bool shouldRepaint(covariant TimeHomeCustomPainter oldDelegate) {
    return true;
  }
}

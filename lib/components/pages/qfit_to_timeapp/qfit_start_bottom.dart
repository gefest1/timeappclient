import 'dart:async';
import 'dart:io';

// TODO DELETE IT
// import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/atoms/error_overlay.dart';
import 'package:li/components/atoms/switch_work_button.dart';
import 'package:li/components/molecules/card_institution_short.dart';
import 'package:li/components/molecules/self_kill.dart';
import 'package:li/components/pages/qfit_to_timeapp/ExtendedFeaturesPage.dart';
import 'package:li/components/pages/qfit_to_timeapp/extended_features_ofClub.dart';
import 'package:li/components/pages/qfit_to_timeapp/head_train.dart';
import 'package:li/components/pages/qfit_to_timeapp/start_train/card_data.dart';
import 'package:li/components/pages/qfit_to_timeapp/start_train/get_card_data.dart';
import 'package:li/components/pages/qfit_to_timeapp/start_train/start_button.dart';
import 'package:li/data/models/client_model.dart';
import 'package:li/data/models/credit_card_model.dart';
import 'package:li/data/models/service_model.dart';
import 'package:li/logic/blocs/get_debt/getdebt_bloc.dart';
import 'package:li/logic/blocs/qfit_bloc/session/session_minute_bloc.dart';
import 'package:li/logic/navigation.dart';
import 'package:li/main.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/text_styles.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:profile/bonusPages/bottom_sheet_bonus.dart';
import 'package:time_app_components/utils/testStyles.dart';

class QFitStartBottom extends StatefulWidget {
  final List<ServiceMinute> service;
  final String institutionId;
  const QFitStartBottom({
    required this.institutionId,
    required this.service,
    Key? key,
  }) : super(key: key);

  @override
  _QFitStartBottomState createState() => _QFitStartBottomState();
}

class _QFitStartBottomState extends State<QFitStartBottom> {
  ServiceMinute? choosenServiceMinute;

  final ValueNotifier<CreditCardModel?> creditCard =
      ValueNotifier<CreditCardModel?>(null);
  final ValueNotifier<ServiceMinute?> serviceMinute =
      ValueNotifier<ServiceMinute?>(null);
  late final StreamSubscription<SessionState> sub;
  bool showError = false;
  Timer? errorTimer;
  bool useBonus = false;
  @override
  void initState() {
    super.initState();
    sub = BlocProvider.of<SessionMinuteBloc>(context, listen: false)
        .stream
        .listen(listError);
    if (widget.service.length == 1) serviceMinute.value = widget.service.first;
  }

  void sessionStart() async {
    if (serviceMinute.value == null) {
      navigatorKey.currentState?.overlay?.insert(
        ErrorOverlayLoader(text: 'Необходимо выбрать услугу'),
      );
      return;
    }
    if (creditCard.value == null) {
      navigatorKey.currentState?.overlay?.insert(
        ErrorOverlayLoader(text: 'Необходимо заполнить данные о карте'),
      );
      return;
    }

    // LOADER START
    final _initBlock = !blockBackRoutes;
    if (_initBlock) routerDelegate.blockWidgets();

    // DEBT START
    try {
      await BlocProvider.of<GetdebtBloc>(
        context,
        listen: false,
      ).closeDebt(creditCard.value!.id!);
    } catch (e) {
      // LOADER END
      if (_initBlock) routerDelegate.unBlockWidgets();

      // SHOW ERROR
      navigatorKey.currentState?.overlay?.insert(
        ErrorOverlayLoader(text: 'Долг не списался'),
      );
      return;
    }

    // LOADER END
    if (_initBlock) routerDelegate.unBlockWidgets();

    // SESSION START
    final completer = Completer();
    BlocProvider.of<SessionMinuteBloc>(
      context,
      listen: false,
    ).add(
      StartSessionEvent(
        creditCardId: creditCard.value!.id!,
        institutionId: widget.institutionId,
        serviceId: serviceMinute.value!.id!,
        completer: completer,
        useBonus: useBonus,
      ),
    );
    FirebaseAnalytics()
        .logEvent(name: 'PayPerMinute', parameters: <String, dynamic>{
      'InstitutionName': widget.service.first.institution!.name,
      'InstitutionType': widget.service.first.institution!.type.toString(),
      'Service': widget.service.first.name.toString(),
      'Price': widget.service.first.price?.toDouble(),
    });
  }

  void listError(SessionState event) {
    if (event is ErrorSessionState) {
      setState(() {
        showError = true;
      });
      errorTimer?.cancel();
      errorTimer = Timer(Duration(seconds: 5, milliseconds: 400), () {
        setState(() {
          showError = false;
        });
      });
    }
  }

  @override
  void dispose() {
    sub.cancel();
    errorTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.vertical(
        top: Radius.circular(16),
      ),
      color: Colors.white,
      child: Stack(
        children: [
          CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Padding(
                  padding: EdgeInsets.only(
                    top: 62,
                    left: 20,
                    right: 20,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 20),
                      CardInstitutionShort(
                        service: widget.service.first,
                        priceTitle: widget.service.length == 1
                            ? widget.service.first.price!.toStringAsFixed(1)
                            : widget.service.first.institution!.averagePrice!
                                .toStringAsFixed(1),
                      ),
                      const SizedBox(height: 20),
                      ValueListenableBuilder(
                          valueListenable: serviceMinute,
                          builder: (context, ServiceMinute? _, __) {
                            if (widget.service.length == 1) return SizedBox();
                            double _sum =
                                serviceMinute.value?.price?.toDouble() ?? 0.0;

                            return Padding(
                              padding:
                                  const EdgeInsets.only(bottom: 40.0, top: 20),
                              child: ExtendedFeaturesCard(
                                title: _ == null ? 'Выберите услугу' : _.name!,
                                subTitle: _ == null ? 'выбрать' : 'изменить',
                                price: _sum.toStringAsFixed(1),
                                onTap: () {
                                  routerDelegate.push(
                                    ExtendedFeaturesPage(
                                      listServices: widget.service,
                                      serviceMinute: serviceMinute,
                                    ),
                                  );
                                },
                              ),
                            );
                          }),
                      const GetCardData(),
                      const Text(
                        'Способы оплаты',
                        style: TextStyle(
                          fontWeight: H2TextStyle.fontWeight,
                          fontSize: H2TextStyle.fontSize,
                          height: H2TextStyle.height,
                          color: ColorData.allMainBlack,
                        ),
                      ),
                      const SizedBox(height: 10),
                      CardData(creditCard: creditCard),
                      AnimatedCrossFade(
                        firstChild: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 20),
                          child: Material(
                            color: ColorData.allButtonsError,
                            borderRadius: BorderRadius.circular(16),
                            child: Padding(
                              padding: EdgeInsets.all(16),
                              child: Row(
                                children: [
                                  SvgPicture.asset('assets/svg/emojiSad.svg'),
                                  const SizedBox(width: 10),
                                  Expanded(
                                    child: Text(
                                      'Оплата не прошла. Попробуйте использовать другую карту',
                                      style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.white,
                                        height: 19 / 16,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        secondChild: SizedBox(height: 15),
                        crossFadeState: showError
                            ? CrossFadeState.showFirst
                            : CrossFadeState.showSecond,
                        duration: Duration(milliseconds: 400),
                      ),
                      BlocBuilder<GetdebtBloc, GetdebtState>(
                          builder: (context, state) {
                        final double bonus;
                        if (state is NoDebtState || state is DebtState) {
                          bonus = (state as dynamic).client?.bonus ?? 0;
                        } else {
                          bonus = 0.0;
                        }

                        return GestureDetector(
                          onTap: () {
                            setState(() {
                              useBonus = !useBonus;
                            });
                          },
                          behavior: HitTestBehavior.opaque,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Потратить баллы: ' + bonus.toStringAsFixed(0),
                                style: TextStyle(
                                  fontSize: 18,
                                  color: ColorData.allMainBlack,
                                ),
                              ),
                              IgnorePointer(
                                child: Switch.adaptive(
                                  activeColor:
                                      ColorData.clientsButtonColorDefault,
                                  value: useBonus,
                                  onChanged: (b) {},
                                ),
                              )
                            ],
                          ),
                        );
                      }),
                      const SizedBox(height: 15),
                      Divider(
                        color: Color(0xffB7B7B7),
                        thickness: 0.5,
                        height: 0,
                      ),
                      GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: () {
                          showModalBottomSheet(
                            isScrollControlled: true,
                            context: context,
                            backgroundColor: Colors.transparent,
                            builder: (context) {
                              return const BonnusBottomSheet();
                            },
                          );
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 20),
                            Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    'Ввести промокод',
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: ColorData.allMainBlack,
                                    ),
                                  ),
                                ),
                                Icon(Icons.chevron_right),
                              ],
                            ),
                            SizedBox(height: 20),
                            Divider(
                              color: Color(0xffB7B7B7),
                              thickness: 0.5,
                              height: 0,
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 40),
                      const Text(
                        'Поминутная оплата',
                        style: TextStyle(
                          fontWeight: H2TextStyle.fontWeight,
                          fontSize: H2TextStyle.fontSize,
                          height: H2TextStyle.height,
                          color: ColorData.allMainBlack,
                        ),
                      ),
                      const SizedBox(height: 10),
                      RichText(
                        text: TextSpan(
                          style: TextStyle(
                            fontSize: P3TextStyle.fontSize,
                            fontWeight: P3TextStyle.fontWeight,
                            height: P3TextStyle.height,
                            color: ColorData.allMainBlack,
                          ),
                          children: [
                            TextSpan(
                              text: 'При старте мы зарезервируем 750',
                              style: TextStyle(
                                fontFamily:
                                    Platform.isIOS ? 'SfProDisplay' : 'Roboto',
                              ),
                            ),
                            const TextSpan(
                              text: ' ₸',
                            ),
                            TextSpan(
                              text:
                                  ' — это залог. Когда вы закончите сеанс, спишется сумма, высчитанная из продолжительности сеанса, а залог вернется к вам на карту.',
                              style: TextStyle(
                                fontFamily:
                                    Platform.isIOS ? 'SfProDisplay' : 'Roboto',
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SliverFillRemaining(
                hasScrollBody: false,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    children: [
                      const SizedBox(height: 40),
                      StartButton(onTap: sessionStart),
                      const SizedBox(height: 40),
                    ],
                  ),
                ),
              ),
              // SliverLayoutBuilder(
              //   builder: (ctx, SliverConstraints constraints) {
              //     final _dd = constraints.viewportMainAxisExtent -
              //         constraints.precedingScrollExtent -
              //         140;
              //     return SliverToBoxAdapter(
              //       child: Padding(
              //         padding: const EdgeInsets.symmetric(horizontal: 20),
              //         child: Column(
              //           children: [
              //             if (_dd > 0) SizedBox(height: _dd),
              //             const SizedBox(height: 20),
              //             StartButton(onTap: sessionStart),
              //             const SizedBox(height: 40),
              //           ],
              //         ),
              //       ),
              //     );
              //   },
              // ),
            ],
          ),
          const HeadTrain(),
        ],
      ),
    );
  }
}

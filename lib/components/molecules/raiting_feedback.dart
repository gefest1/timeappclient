import 'package:flutter_svg/svg.dart';
import 'package:li/utils/color.dart';
import 'package:flutter/material.dart';
import 'package:time_app_components/utils/testStyles.dart';

class RaingFeedback extends StatelessWidget {
  const RaingFeedback({Key? key}) : super(key: key);

  Widget getElement(int count, double percent) {
    return Row(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: List.generate(
            5,
            (index) => (4 - index) < count
                ? SvgPicture.asset(
                    'assets/svg/icon/star.svg',
                    height: 10,
                    width: 10,
                    color: ColorData.allMainBlack,
                  )
                : const SizedBox(
                    width: 10,
                    height: 10,
                  ),
          ),
        ),
        const SizedBox(width: 6),
        Expanded(
          child: Row(
            children: [
              Expanded(
                flex: (percent * 100).round(),
                child: Container(
                  decoration: BoxDecoration(
                    color: ColorData.specsButtonColorDefault,
                    borderRadius: BorderRadius.all(
                      Radius.circular(0.5),
                    ),
                  ),
                  height: 1,
                ),
              ),
              Expanded(
                flex: ((1 - percent) * 100).round(),
                child: Container(
                  decoration: BoxDecoration(
                    color: ColorData.allTextInactive,
                    borderRadius: BorderRadius.all(
                      Radius.circular(0.5),
                    ),
                  ),
                  height: 1,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text(
          'Рейтинг и отзывы',
          style: const TextStyle(
            fontSize: H2TextStyle.fontSize,
            fontWeight: H2TextStyle.fontWeight,
            height: H2TextStyle.height,
          ),
        ),
        const SizedBox(height: 6),
        Column(
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        '4,6',
                        style: TextStyle(
                          fontSize: 80,
                          color: ColorData.allMainBlack,
                          height: 91 / 80,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      getElement(5, 0.5),
                      const SizedBox(height: 6),
                      getElement(4, 1),
                      const SizedBox(height: 6),
                      getElement(3, 0),
                      const SizedBox(height: 6),
                      getElement(2, 0.6),
                      const SizedBox(height: 6),
                      getElement(1, 0.9),
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(height: 4),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Средняя оценка',
                  style: TextStyle(
                    fontSize: P3TextStyle.fontSize,
                    color: ColorData.allMainBlack,
                    height: P3TextStyle.height,
                    fontWeight: P3TextStyle.fontWeight,
                  ),
                ),
                const Text(
                  '215 отзывов',
                  style: TextStyle(
                    fontSize: P3TextStyle.fontSize,
                    color: ColorData.allMainBlack,
                    height: P3TextStyle.height,
                    fontWeight: P3TextStyle.fontWeight,
                  ),
                ),
              ],
            )
          ],
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/logic/blocs/get_debt/getdebt_bloc.dart';
import 'package:li/utils/color.dart';

class GetCardData extends StatelessWidget {
  const GetCardData({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GetdebtBloc, GetdebtState>(
      builder: (context, state) {
        if (state is DebtState) {
          final cient = state.client.debt;
          return Column(
            children: [
              SizedBox(height: 30),
              Container(
                decoration: BoxDecoration(
                  color: ColorData.allMainBlack,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Padding(
                  padding: EdgeInsets.all(15),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      SvgPicture.asset('assets/svg/icon/circleAlert.svg'),
                      SizedBox(width: 15),
                      Flexible(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            RichText(
                              text: TextSpan(
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                ),
                                children: [
                                  TextSpan(
                                    text:
                                        'Накопился долг — ${cient.toString()}',
                                  ),
                                  TextSpan(
                                    text: ' ₸.',
                                  )
                                ],
                              ),
                            ),
                            Text(
                              'При начале новой сессии мы предварительно спишем сумму долга.',
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(height: 30),
            ],
          );
        }
        return SizedBox();
      },
    );
  }
}

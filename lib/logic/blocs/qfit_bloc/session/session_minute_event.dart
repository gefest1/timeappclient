part of 'session_minute_bloc.dart';

class SessionEvent extends Equatable {
  const SessionEvent();
  @override
  List<Object?> get props => [];
}

class StartSessionEvent extends SessionEvent {
  final String institutionId;
  final String creditCardId;
  final String serviceId;
  final Completer? completer;
  final bool useBonus;

  const StartSessionEvent({
    required this.institutionId,
    required this.creditCardId,
    required this.serviceId,
    required this.useBonus,
    this.completer,
  });

  @override
  List<Object?> get props => [];
}

class FinishSessionEvent extends SessionEvent {
  final SessionMinute sessionId;
  final Completer<SessionMinute> completer;

  const FinishSessionEvent({
    required this.sessionId,
    required this.completer,
  });
}

class GetActiveSessionEvent extends SessionEvent {
  const GetActiveSessionEvent();
}

class LeaveCommentEvent extends SessionEvent {
  final String institutionId;
  final String text;
  final double? rating;
  final Completer completer;
  const LeaveCommentEvent({
    required this.institutionId,
    required this.rating,
    required this.text,
    required this.completer,
  });
}

import 'package:flutter/material.dart';

class FadeAnimation extends StatefulWidget {
  final bool open;
  final Widget child;

  const FadeAnimation({
    this.open = true,
    required this.child,
    Key? key,
  }) : super(key: key);

  @override
  _FadeAnimationState createState() => _FadeAnimationState();
}

class _FadeAnimationState extends State<FadeAnimation>
    with SingleTickerProviderStateMixin {
  late final AnimationController _animationController = AnimationController(
    vsync: this,
    duration: Duration(
      milliseconds: 300,
    ),
    value: widget.open ? 1 : 0,
  );

  @override
  void didUpdateWidget(covariant FadeAnimation oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.open) {
      _animationController.forward();
    } else {
      _animationController.reverse();
    }
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      opacity: _animationController,
      child: widget.child,
    );
  }
}

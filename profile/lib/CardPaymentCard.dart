import 'package:flutter/material.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/components/pages/add_new_card_payment.dart';
import 'package:li/components/pages/payment_way_page.dart';
import 'package:li/data/models/credit_card_model.dart';
import 'package:li/logic/providers/CreditCard.dart';
import 'package:li/main.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';
import 'package:provider/provider.dart';

class PaymentCard extends StatefulWidget {
  const PaymentCard({Key? key}) : super(key: key);

  @override
  State<PaymentCard> createState() => _PaymentCardState();
}

class _PaymentCardState extends State<PaymentCard> {
  final ValueNotifier<CreditCardModel?> creditCard =
      ValueNotifier<CreditCardModel?>(null);
  @override
  Widget build(BuildContext context) {
    return Consumer<CreditCardProvider>(
        builder: (_, CreditCardProvider creditCardProvider, __) {
      if (creditCardProvider.creditCards.isEmpty) {
        return SizeTapAnimation(
          child: InkWell(
            onTap: () {
              routerDelegate.push(
                const AddNewCardPayment(),
              );
            },
            child: Container(
              decoration: BoxDecoration(
                image: const DecorationImage(
                  image: AssetImage(
                    'assets/Payment.png',
                  ),
                  fit: BoxFit.cover,
                ),
                color: const Color(0xFF222222),
                borderRadius: BorderRadius.circular(15),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text(
                      'Способ оплаты',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                    ),
                    SizedBox(height: 14),
                    Padding(
                      padding: EdgeInsets.only(bottom: 4.0),
                      child: Text(
                        'Добавить',
                        style: TextStyle(
                          color: ColorData.clientsButtonColorDefault,
                          fontSize: 14,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      }

      creditCard.value ??= creditCardProvider.creditCards.first;

      return SizeTapAnimation(
        child: GestureDetector(
          onTap: () {
            routerDelegate.push(
              PaymentWayPage(
                creditCard: creditCard,
              ),
            );
          },
          child: Container(
            decoration: BoxDecoration(
              image: const DecorationImage(
                image: AssetImage(
                  'assets/Payment.png',
                ),
                fit: BoxFit.cover,
              ),
              color: const Color(0xFF222222),
              borderRadius: BorderRadius.circular(15),
            ),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                // ignore: prefer_const_literals_to_create_immutables
                children: [
                  const Text(
                    'Способ оплаты',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                  SizedBox(height: 14),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        creditCard.value!.cardType!,
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 14,
                          height: H1TextStyle.height,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Text(
                        '**' + creditCard.value!.last4Digits!,
                        style: const TextStyle(
                            color: Colors.white,
                            fontSize: H1TextStyle.fontSize,
                            height: H1TextStyle.height,
                            fontWeight: H1TextStyle.fontWeight),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}

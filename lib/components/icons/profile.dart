// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';

// size: Size(WIDTH, (WIDTH*1).toDouble()), //You can Replace [WIDTH] with your desired width for Custom Paint and height will be calculated automatically
class ProfileCustomPainter extends CustomPainter {
  final Color color;

  const ProfileCustomPainter({
    this.color = const Color(0xff979797),
    Listenable? repaint,
  }) : super(repaint: repaint);
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.8987714, size.height * 0.7221905);
    path_0.cubicTo(
        size.width * 0.8131238,
        size.height * 0.6764238,
        size.width * 0.6624238,
        size.height * 0.6110952,
        size.width * 0.5000000,
        size.height * 0.6110952);
    path_0.cubicTo(
        size.width * 0.3375757,
        size.height * 0.6110952,
        size.width * 0.1868767,
        size.height * 0.6764238,
        size.width * 0.1012276,
        size.height * 0.7221905);
    path_0.cubicTo(
        size.width * 0.05322857,
        size.height * 0.7478381,
        size.width * 0.02410895,
        size.height * 0.7967810,
        size.width * 0.01769705,
        size.height * 0.8508238);
    path_0.lineTo(0, size.height * 0.9999857);
    path_0.lineTo(size.width, size.height * 0.9999857);
    path_0.lineTo(size.width * 0.9823048, size.height * 0.8508238);
    path_0.cubicTo(
        size.width * 0.9758905,
        size.height * 0.7967810,
        size.width * 0.9467714,
        size.height * 0.7478381,
        size.width * 0.8987714,
        size.height * 0.7221905);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = color;
    canvas.drawPath(path_0, paint_0_fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.5000000, size.height * 0.5000000);
    path_1.cubicTo(
        size.width * 0.6380714,
        size.height * 0.5000000,
        size.width * 0.7500000,
        size.height * 0.3880714,
        size.width * 0.7500000,
        size.height * 0.2500000);
    path_1.cubicTo(size.width * 0.7500000, size.height * 0.1119290,
        size.width * 0.6380714, 0, size.width * 0.5000000, 0);
    path_1.cubicTo(
        size.width * 0.3619286,
        0,
        size.width * 0.2500000,
        size.height * 0.1119290,
        size.width * 0.2500000,
        size.height * 0.2500000);
    path_1.cubicTo(
        size.width * 0.2500000,
        size.height * 0.3880714,
        size.width * 0.3619286,
        size.height * 0.5000000,
        size.width * 0.5000000,
        size.height * 0.5000000);
    path_1.close();

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = color;
    canvas.drawPath(path_1, paint_1_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}

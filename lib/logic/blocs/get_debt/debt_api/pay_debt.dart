import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/data/models/client_model.dart';

mixin PayDebtSession {
  static Future<Client> payDebtOfUser({
    required String creditCardId,
    required GraphQLClient client,
  }) async {
    final _mutationResult = await client.mutate(
      MutationOptions(
        document: gql('''
        mutation (\$creditCardId: String!){
          payDebt(creditCardId:\$creditCardId){
          _id,
          email,
          dateOfBirth,
          fullName,
          }
        }'''),
        variables: {
          "creditCardId": creditCardId,
        },
      ),
    );
    if (_mutationResult.hasException) throw _mutationResult.exception!;
    return Client.fromMap(_mutationResult.data!['payDebt']);
  }
}

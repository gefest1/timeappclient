import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/const.dart';
import 'package:flutter/material.dart';

class ClientsFindsYouPage extends StatelessWidget {
  final VoidCallback nextPage;

  const ClientsFindsYouPage({required this.nextPage, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 20,
        left: 20,
        right: 20,
        bottom: 20,
      ),
      child: Column(
        children: [
          Expanded(
            child: Column(
              children: [
                RepaintBoundary(
                    child: SvgPicture.asset('assets/svg/foundUs.svg')),
                const SizedBox(
                  height: 24,
                ),
                const Text(
                  'Как клиенты находят вас',
                  style: const TextStyle(
                    color: mainBlack,
                    fontSize: 38,
                    fontWeight: FontWeight.w600,
                    letterSpacing: -0.6,
                  ),
                ),
                const SizedBox(height: 4),
                Expanded(
                  child: const AutoSizeText(
                    "Если вы «Свободны». Человек, неподалеку от вас, которому понадобилась ваша услуга в ближайшее время, находит вас через приложение для клиентов. Смотрит на ваши отзывы и рейтинг и отправляет вам заявку.",
                    style: TextStyle(
                      color: mainBlack,
                      fontWeight: FontWeight.w400,
                    ),
                    minFontSize: 5,
                    maxFontSize: 18,
                  ),
                ),
              ],
            ),
          ),
          Center(
            child: SizedBox(
              child: ElevatedButton(
                onPressed: this.nextPage,
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Color(0xff007AFF)),
                  minimumSize: MaterialStateProperty.all(
                    Size(180, 58),
                  ),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16),
                      side: BorderSide.none,
                    ),
                  ),
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 2),
                      child: Text(
                        'Далее',
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 18,
                        ),
                      ),
                    ),
                    const SizedBox(width: 10),
                    RepaintBoundary(
                        child: SvgPicture.asset('assets/svg/icon/forward.svg')),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

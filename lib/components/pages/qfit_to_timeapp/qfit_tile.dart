import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/atoms/load_location.dart';
import 'package:li/const.dart';
import 'package:li/utils/color.dart';

class QfitTile extends StatefulWidget {
  final String? title;
  final String? photoUrl;
  final String? rating;
  final String? label;
  final String? locationLabel;
  final VoidCallback? onTap;

  const QfitTile({
    this.title,
    this.photoUrl,
    this.rating,
    this.label,
    this.onTap,
    this.locationLabel,
    Key? key,
  }) : super(key: key);

  @override
  _QfitTileState createState() => _QfitTileState();
}

class _QfitTileState extends State<QfitTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapUp: (TapUpDetails details) {
        widget.onTap?.call();
      },
      child: IntrinsicWidth(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 156,
              width: 156,
              decoration: BoxDecoration(
                color: Colors.grey,
                boxShadow: bigBlur,
                borderRadius: BorderRadius.circular(15),
                image: widget.photoUrl == null
                    ? null
                    : DecorationImage(
                        image: NetworkImage(widget.photoUrl!),
                      ),
              ),
              child: Stack(
                children: [
                  Positioned(
                    left: 0,
                    right: 10,
                    top: 10,
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: widget.locationLabel != null
                          ? Container(
                              height: 22,
                              padding: EdgeInsets.symmetric(
                                horizontal: 8,
                                vertical: 3,
                              ),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(6),
                              ),
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  SvgPicture.asset(
                                    'assets/svg/icon/sendFilled.svg',
                                    color: ColorData.allMainBlack,
                                    height: 12,

                                    // width: 12,
                                  ),
                                  const SizedBox(width: 1),
                                  Text(
                                    widget.locationLabel!,
                                    style: TextStyle(
                                      color: ColorData.allMainBlack,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : const LoadLocation(),
                    ),
                  ),
                  if (widget.rating != null)
                    Positioned(
                      top: 10,
                      left: 10,
                      child: Container(
                        height: 22,
                        padding: EdgeInsets.symmetric(
                          horizontal: 8,
                          vertical: 4,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(6),
                        ),
                        child: Center(
                          child: Row(
                            children: [
                              SvgPicture.asset(
                                'assets/svg/icon/star.svg',
                                color: Color(0xff00DF31),
                                height: 12,
                                width: 12,
                              ),
                              Text(
                                widget.rating!,
                                style: TextStyle(
                                  color: Colors.green,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  if (widget.label != null)
                    Positioned(
                      left: 10,
                      bottom: 10,
                      child: Container(
                        height: 25,
                        width: 77,
                        decoration: BoxDecoration(
                          color: Color(0xFF434343),
                          borderRadius: BorderRadius.circular(6),
                        ),
                        child: Center(
                          child: Text(
                            widget.label!,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      ),
                    )
                ],
              ),
            ),
            const SizedBox(height: 5),
            if (widget.title != null)
              Text(
                widget.title!,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w400,
                  height: 21 / 18,
                ),
              )
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/const.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class CreditCard extends StatelessWidget {
  final String title;
  final String cartType;

  const CreditCard({
    required this.title,
    required this.cartType,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: bigBlur,
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
      ),
      padding: EdgeInsets.all(20),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(
                    fontSize: H3TextStyle.fontSize,
                    fontWeight: H3TextStyle.fontWeight,
                    height: H3TextStyle.height,
                    color: ColorData.allMainBlack,
                  ),
                ),
                Text(
                  'изменить способ оплаты',
                  style: TextStyle(
                    fontSize: P4TextStyle.fontSize,
                    fontWeight: P4TextStyle.fontWeight,
                    height: P4TextStyle.height,
                    color: ColorData.clientsButtonColorPressed,
                  ),
                ),
              ],
            ),
          ),
          if (cartType == 'Visa')
            RepaintBoundary(
              child: SvgPicture.asset(
                'assets/svg/visa.svg',
                height: 40,
              ),
            )
          else
            RepaintBoundary(
                child: SvgPicture.asset('assets/svg/icon/mastercard.svg')),
        ],
      ),
    );
  }
}

class CreditCardAdd extends StatelessWidget {
  final VoidCallback? onTap;

  const CreditCardAdd({this.onTap, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: SizeTapAnimation(
        child: DecoratedBox(
          decoration: BoxDecoration(
            boxShadow: bigBlur,
            color: Colors.white,
            borderRadius: BorderRadius.circular(16),
          ),
          child: Padding(
            padding: EdgeInsets.all(20),
            child: Text(
              'Добавить способ оплаты',
              style: TextStyle(
                fontSize: P3TextStyle.fontSize,
                fontWeight: P3TextStyle.fontWeight,
                height: P3TextStyle.height,
                color: ColorData.clientsButtonColorPressed,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

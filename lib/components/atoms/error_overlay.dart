import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/utils/color.dart';

class ErrorOverlayLoader extends OverlayEntry {
  final String text;

  ErrorOverlayLoader({
    required this.text,
  }) : super(
          builder: (_) {
            return SizedBox();
          },
        );

  Widget _buider(BuildContext context) {
    return ErrorLoaderWidget(overlayEntry: this, text: text);
  }

  WidgetBuilder get builder => _buider;
}

class ErrorLoaderWidget extends StatefulWidget {
  final ErrorOverlayLoader overlayEntry;
  final String text;

  const ErrorLoaderWidget({
    required this.overlayEntry,
    required this.text,
    Key? key,
  }) : super(key: key);

  @override
  _ErrorLoaderWidgetState createState() => _ErrorLoaderWidgetState();
}

class _ErrorLoaderWidgetState extends State<ErrorLoaderWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController animationController = AnimationController(
    vsync: this,
    duration: Duration(milliseconds: 250),
    value: 0,
  );
  late Animation<Offset> offset =
      Tween<Offset>(begin: Offset.zero, end: Offset(0.0, 0.5))
          .animate(animationController);

  void _init() async {
    await animationController.forward();
    await Future.delayed(Duration(seconds: 2));
    await animationController.reverse();
    widget.overlayEntry.remove();
  }

  @override
  void initState() {
    super.initState();
    _init();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final topPadding = MediaQuery.of(context).padding.top;
    return Positioned(
      top: topPadding + 32,
      left: 20,
      right: 20,
      child: SlideTransition(
        position: offset,
        child: Material(
          color: ColorData.allButtonsError,
          borderRadius: BorderRadius.circular(16),
          child: Padding(
            padding: EdgeInsets.all(16),
            child: Row(
              children: [
                RepaintBoundary(
                  child: SvgPicture.asset('assets/svg/emojiSad.svg'),
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Text(
                    widget.text,
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      height: 19 / 16,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

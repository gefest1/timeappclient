import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/data/models/category_model.dart';
import 'package:li/main.dart';
import 'package:li/network/graphql_provider.dart';
import 'package:main_page/model/main_slug_model.dart';

part 'main_page_event.dart';
part 'main_page_state.dart';
part 'reactions/check_fetch_data.dart';

class MainPageBloc extends Bloc<MainPageEvent, MainPageState> {
  final GraphqlProvider graphqlProvider;

  MainPageBloc(
    this.graphqlProvider,
  ) : super(const MainPageInitial()) {
    on<GetDataMainPage>(checkFetchData);

    if (isRegistered()) add(const GetDataMainPage());
  }
}

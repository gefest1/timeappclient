import 'package:flutter_svg/svg.dart';
import 'package:li/utils/color.dart';
import 'package:flutter/material.dart';
import 'package:readmore/readmore.dart';
import 'package:time_app_components/utils/testStyles.dart';

class ReviewCard extends StatelessWidget {
  const ReviewCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        color: Color(0xffF8F8F8),
        borderRadius: BorderRadius.circular(16),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Row(
                children: [
                  Expanded(
                    child: Row(
                      children: [
                        Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.black26,
                          ),
                        ),
                        const SizedBox(width: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'data',
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                height: 19 / 16,
                              ),
                            ),
                            const SizedBox(height: 2),
                            Text(
                              'Татуировки',
                              style: const TextStyle(
                                fontSize: P4TextStyle.fontSize,
                                fontWeight: P4TextStyle.fontWeight,
                                height: P4TextStyle.height,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Row(
                        children: List.generate(
                          5,
                          (index) => SvgPicture.asset(
                            'assets/svg/icon/star.svg',
                            color: Color(0xffffe600),
                            height: 20,
                            width: 20,
                          ),
                        ),
                      ),
                      Text(
                        '15 авг.',
                        style: TextStyle(
                          fontSize: P3TextStyle.fontSize,
                          fontWeight: P3TextStyle.fontWeight,
                          height: P3TextStyle.height,
                          color: ColorData.allMainActivegray,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            const SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: ReadMoreText(
                'Начали с ним работать еще год назад, узнал об Иване от подруги. За год успели разработать эскиз рукава (фотки добавил). Первые пару сеансов, когда он работал на съемной квартире, конечно побаивался за качество. Но за полгода он обзавелся целой студией, с тех пор всех.',
                trimLines: 7,
                trimMode: TrimMode.Line,
                trimCollapsedText: '...читать полностью',
                delimiter: ' ',
                trimExpandedText: 'скрыть',
                style: TextStyle(
                  fontWeight: P3TextStyle.fontWeight,
                  fontSize: P3TextStyle.fontSize,
                  height: P3TextStyle.height,
                ),
                lessStyle: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: P3TextStyle.fontSize,
                  height: P3TextStyle.height,
                  color: ColorData.specsButtonColorDefault,
                ),
                moreStyle: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: P3TextStyle.fontSize,
                  height: P3TextStyle.height,
                  color: ColorData.specsButtonColorDefault,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: SizedBox(
                height: 68,
                child: ListView.separated(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (ctx, _) => Container(
                    height: 68,
                    width: 92,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(16),
                      ),
                      color: Colors.black26,
                    ),
                  ),
                  separatorBuilder: (ctx, _) => SizedBox(width: 10),
                  itemCount: 8,
                ),
              ),
            ),
            const SizedBox(height: 6),
            Center(
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  // log('daw');
                },
                child: IgnorePointer(
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                    child: Text(
                      'Ответить',
                      style: TextStyle(
                        fontWeight: P3TextStyle.fontWeight,
                        fontSize: P3TextStyle.fontSize,
                        height: P3TextStyle.height,
                        color: ColorData.specsButtonColorDefault,
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

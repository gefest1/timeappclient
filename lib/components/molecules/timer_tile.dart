import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/text_styles.dart';
import 'package:time_app_components/utils/testStyles.dart';

class TimeTile extends StatelessWidget {
  final int minutes;

  const TimeTile({
    required this.minutes,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: Color(0xffF9F9F9),
      ),
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Row(
          children: [
            SvgPicture.asset(
              'assets/svg/icon/timer.svg',
              height: 24,
              width: 24,
            ),
            const SizedBox(width: 16),
            Expanded(
              child: Text(
                'Первые ${this.minutes} минут сессии — бесплатно',
                style: TextStyle(
                  fontSize: P2TextStyle.fontSize,
                  fontWeight: P2TextStyle.fontWeight,
                  height: P2TextStyle.height,
                  color: ColorData.allMainBlack,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

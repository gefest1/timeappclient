import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:li/utils/determinePosition.dart';

class LocationProvider extends ChangeNotifier {
  Position? location;
  StreamSubscription<Position>? streamSub;
  late StreamSubscription<ServiceStatus> _statusListener;

  LocationProvider() {
    _statusListener = Geolocator.getServiceStatusStream().listen(statusChanged);
  }
  void statusChanged(ServiceStatus status) async {
    if (status == ServiceStatus.enabled) {
      try {
        location = await determinePosition();
        notifyListeners();
      } catch (e) {}
    }
  }

  void onceGet() async {
    if (!(await getPermsissionsLocation())) return;
    final event = await Geolocator.getCurrentPosition();
    location = event;
    notifyListeners();
  }

  void activate({void Function(Position)? callBack}) async {
    getPermsissionsLocation().catchError((_) {
      log(_.toString());
    });
    await streamSub?.cancel();
    streamSub = Geolocator.getPositionStream().listen((event) {
      location = event;
      callBack?.call(event);
      notifyListeners();
    })
      ..onError((_) {});
  }

  Future<void> disactivate() async {
    streamSub?.cancel();
  }

  @override
  void dispose() {
    streamSub?.cancel();
    _statusListener.cancel();
    super.dispose();
  }
}

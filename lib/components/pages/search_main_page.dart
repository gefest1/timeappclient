import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/atoms/search_text_field.dart';
import 'package:li/components/molecules/search_app_bar.dart';
import 'package:li/components/pages/return_search_page.dart';
import 'package:li/main.dart';
import 'package:li/utils/typesense.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class SearchMainPage extends StatefulWidget {
  const SearchMainPage({Key? key}) : super(key: key);

  @override
  _SearchMainPageState createState() => _SearchMainPageState();
}

class _SearchMainPageState extends State<SearchMainPage> {
  final TextEditingController _searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final _f = FocusScope.of(context);

        if (_f.hasFocus || _f.hasPrimaryFocus) {
          _f.unfocus();
        }
      },
      child: Material(
        color: Colors.white,
        child: Stack(
          children: [
            ListView(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).padding.top + 72 + 20,
              ),
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    'Частые запросы',
                    style: TextStyle(
                      fontSize: H2TextStyle.fontSize,
                      fontWeight: H2TextStyle.fontWeight,
                      color: ColorData.allMainBlack,
                      height: H2TextStyle.height,
                    ),
                  ),
                ),
                for (int i = 0; i < 2; ++i)
                  InkWell(
                    onTap: () {
                      routerDelegate.push(const ReturnSearchPage());
                    },
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 20, horizontal: 20),
                          child: Text(
                            'Укладка',
                            style: TextStyle(
                              fontSize: P3TextStyle.fontSize,
                              fontWeight: P3TextStyle.fontWeight,
                              height: P3TextStyle.height,
                              color: ColorData.allMainBlack,
                            ),
                          ),
                        ),
                        Divider(
                          height: 0,
                          thickness: 0.5,
                          indent: 20,
                          endIndent: 20,
                          color: ColorData.allMainGray,
                        ),
                      ],
                    ),
                  ),
                const SizedBox(height: 20),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Text(
                    'История',
                    style: TextStyle(
                      fontSize: H2TextStyle.fontSize,
                      fontWeight: H2TextStyle.fontWeight,
                      color: ColorData.allMainBlack,
                      height: H2TextStyle.height,
                    ),
                  ),
                ),
                for (int i = 0; i < 2; ++i)
                  InkWell(
                    onTap: () {},
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 20, horizontal: 20),
                          child: Row(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    color: ColorData.allMainGray,
                                    shape: BoxShape.circle),
                                height: 20,
                                width: 20,
                              ),
                              const SizedBox(width: 10),
                              Expanded(
                                child: Text(
                                  'Мужская стрижка',
                                  style: TextStyle(
                                    fontSize: P3TextStyle.fontSize,
                                    fontWeight: P3TextStyle.fontWeight,
                                    height: P3TextStyle.height,
                                    color: ColorData.allMainBlack,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Divider(
                          height: 0,
                          thickness: 0.5,
                          indent: 20,
                          endIndent: 20,
                          color: ColorData.allMainGray,
                        ),
                      ],
                    ),
                  ),
              ],
            ),
            Positioned(
              left: 0,
              right: 0,
              top: 0,
              child: SearchAppBar(
                backgroundColor: ColorData.clientsButtonColorDefault,
                toolbarHeight: 72,
                titleSpacing: 0,
                title: Padding(
                  padding: const EdgeInsets.only(
                    bottom: 16.0,
                    top: 16,
                    left: 20,
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: SearchTextField(
                          controller: _searchController,
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          typesenseClient
                              .collection('service')
                              .documents
                              .search(
                            {
                              'q': 'Okay',
                              'query_by': 'name',
                            },
                          );
                        },
                        icon: SvgPicture.asset('assets/svg/icon/close.svg'),
                      ),
                    ],
                  ),
                ),
                automaticallyImplyLeading: false,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/const.dart';
import 'package:flutter/material.dart';

class DescriptionPage extends StatelessWidget {
  final VoidCallback nextPage;

  const DescriptionPage({
    required this.nextPage,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 40,
        left: 20,
        right: 20,
        bottom: 20,
      ),
      child: Column(
        children: [
          Expanded(
            child: const AutoSizeText(
              'TimeApp Pro помогает находить клиентов, которые находятся недалеко от вас и готовы прийти в ближайшее время',
              style: const TextStyle(
                fontSize: 38,
                color: mainBlack,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Center(
            child: SizedBox(
              child: ElevatedButton(
                onPressed: this.nextPage,
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Color(0xff007AFF)),
                  minimumSize: MaterialStateProperty.all(
                    Size(180, 58),
                  ),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16),
                      side: BorderSide.none,
                    ),
                  ),
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 2),
                      child: Text(
                        'Далее',
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 18,
                        ),
                      ),
                    ),
                    const SizedBox(width: 10),
                    RepaintBoundary(
                        child: SvgPicture.asset('assets/svg/icon/forward.svg')),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

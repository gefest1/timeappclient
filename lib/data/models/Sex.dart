class SexEnum {
  final String _value;

  const SexEnum._internal(this._value);

  String toString() => _value;
  String toMap() => _value;

  static const SexEnum Male = const SexEnum._internal('Male');
  static const SexEnum male = const SexEnum._internal('Male');
  static const SexEnum Female = const SexEnum._internal('Female');

  bool isMale() {
    return _value == "Male";
  }

  static SexEnum? fromMap(String? value) {
    if ('Male' == value) return Male;
    if ('Female' == value) return Female;
    return null;
  }
}

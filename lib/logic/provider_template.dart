import 'package:flutter/material.dart';
import 'package:li/logic/providers/CreditCard.dart';
import 'package:li/logic/providers/geo_institution/GeoInstitutionProvider.dart';
import 'package:li/logic/providers/get_club_data/GetClubData.dart';
import 'package:li/logic/providers/location/location_provider.dart';
import 'package:li/network/graphql_provider.dart';
import 'package:provider/provider.dart';
import 'package:take_away/logic/provider/institution/institution_provider.dart';

class ProviderTemplate extends StatefulWidget {
  final Widget child;
  const ProviderTemplate({
    required this.child,
    Key? key,
  }) : super(key: key);

  @override
  _ProviderTemplateState createState() => _ProviderTemplateState();
}

class _ProviderTemplateState extends State<ProviderTemplate> {
  late GraphqlProvider graphqlProvider = GraphqlProvider();
  late GraphqlMinuteProvider graphqlMinuteProvider = GraphqlMinuteProvider();
  late GraphqlTakeAwayProvider graphqlTakeAwayProvider =
      GraphqlTakeAwayProvider();
  LocationProvider locationProvider = LocationProvider();

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<GraphqlProvider>(
          create: (_) => graphqlProvider,
        ),
        Provider<GraphqlMinuteProvider>(
          create: (_) => graphqlMinuteProvider,
        ),
        Provider<GraphqlTakeAwayProvider>(
          create: (_) => graphqlTakeAwayProvider,
        ),
        ChangeNotifierProvider<InstitutionTakeAwayProvider>(
          create: (_) => InstitutionTakeAwayProvider(),
        ),
        ChangeNotifierProvider<CreditCardProvider>(
          create: (_) => CreditCardProvider(
            Provider.of<GraphqlProvider>(_, listen: false),
          ),
        ),
        ChangeNotifierProvider<ClubDataProvider>(
          create: (_) =>
              ClubDataProvider(graphqlMinuteProvider: graphqlMinuteProvider),
        ),
        ChangeNotifierProvider<GeoInstitutionProvider>(
          create: (_) => GeoInstitutionProvider(
            graphqlMinuteProvider: graphqlMinuteProvider,
          ),
        ),
        ChangeNotifierProvider(
          create: (_) => locationProvider,
        ),
      ],
      child: widget.child,
    );
  }
}

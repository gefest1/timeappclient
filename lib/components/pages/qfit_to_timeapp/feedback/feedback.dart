import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:li/components/molecules/rating_bar.dart';
import 'package:li/components/pages/qfit_to_timeapp/qfit_page.dart';
import 'package:li/const.dart';
import 'package:li/data/models/session_minute.dart';
import 'package:li/logic/blocs/qfit_bloc/session/session_minute_bloc.dart';

import 'package:li/main.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class FeedbackPage extends StatefulWidget {
  final SessionMinute session;
  final String institutionId;
  const FeedbackPage({
    required this.institutionId,
    required this.session,
    Key? key,
  }) : super(key: key);

  @override
  _FeedbackPageState createState() => _FeedbackPageState();
}

class _FeedbackPageState extends State<FeedbackPage> {
  final TextEditingController reviewController = TextEditingController();
  int? rating;
  late Duration diffTime =
      widget.session.endTime!.difference(widget.session.startTime!);

  @override
  void dispose() {
    reviewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final _f = FocusScope.of(context);
        if (_f.hasFocus || _f.hasPrimaryFocus) {
          _f.unfocus();
        }
      },
      child: Material(
        color: Colors.white,
        child: CustomScrollView(
          physics: ClampingScrollPhysics(),
          slivers: [
            SliverToBoxAdapter(child: const SizedBox(height: 57)),
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Text(
                  'Сессия \nзавершена',
                  style: TextStyle(
                    fontSize: H1TextStyle.fontSize,
                    fontWeight: H1TextStyle.fontWeight,
                    height: H1TextStyle.height,
                    color: ColorData.allMainBlack,
                  ),
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 20),
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Услуга',
                              style: TextStyle(
                                fontSize: P3TextStyle.fontSize,
                                fontWeight: P3TextStyle.fontWeight,
                                height: P3TextStyle.height,
                                color: ColorData.allMainActivegray,
                              ),
                            ),
                          ),
                          Text(
                            widget.session.service!.name!,
                            style: TextStyle(
                              color: Color(0xff434343),
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 5),
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Длительность',
                              style: TextStyle(
                                fontSize: P3TextStyle.fontSize,
                                fontWeight: P3TextStyle.fontWeight,
                                height: P3TextStyle.height,
                                color: ColorData.allMainActivegray,
                              ),
                            ),
                          ),
                          Text(
                            diffTime.inMinutes.toString() + ' мин',
                            style: TextStyle(
                              color: Color(0xff434343),
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 5),
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Стоимость',
                              style: TextStyle(
                                fontSize: P3TextStyle.fontSize,
                                fontWeight: P3TextStyle.fontWeight,
                                height: P3TextStyle.height,
                                color: ColorData.allMainActivegray,
                              ),
                            ),
                          ),
                          RichText(
                            text: TextSpan(
                                style: TextStyle(
                                    fontSize: P3TextStyle.fontSize,
                                    color: Colors.black),
                                children: [
                                  TextSpan(
                                    text: widget.session.price.toString(),
                                  ),
                                  TextSpan(text: ' ₸')
                                ]),
                          ),
                        ],
                      ),
                      SizedBox(height: 5),
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Оплачено баллами',
                              style: TextStyle(
                                fontSize: P3TextStyle.fontSize,
                                fontWeight: P3TextStyle.fontWeight,
                                height: P3TextStyle.height,
                                color: ColorData.allMainActivegray,
                              ),
                            ),
                          ),
                          RichText(
                            text: TextSpan(
                                style: TextStyle(
                                  fontSize: P3TextStyle.fontSize,
                                  color: Colors.black,
                                ),
                                children: [
                                  TextSpan(
                                    text: (widget.session.usedNumOfBonus ?? 0)
                                        .toString(),
                                  ),
                                  TextSpan(text: ' ₸')
                                ]),
                          ),
                        ],
                      ),
                      SizedBox(height: 5),
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              'Кэшбек',
                              style: TextStyle(
                                fontSize: P3TextStyle.fontSize,
                                fontWeight: P3TextStyle.fontWeight,
                                height: P3TextStyle.height,
                                color: ColorData.allMainActivegray,
                              ),
                            ),
                          ),
                          RichText(
                            text: TextSpan(
                                style: TextStyle(
                                  fontSize: P3TextStyle.fontSize,
                                  color: Colors.black,
                                ),
                                children: [
                                  TextSpan(
                                    text: (widget.session.bonusEarnt ?? 0)
                                        .toString(),
                                  ),
                                  TextSpan(text: ' ₸')
                                ]),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SliverToBoxAdapter(child: const SizedBox(height: 30)),
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Вам все понравилось?',
                      style: TextStyle(
                        fontSize: H2TextStyle.fontSize,
                        fontWeight: H2TextStyle.fontWeight,
                        height: H2TextStyle.height,
                        color: ColorData.allMainBlack,
                      ),
                    ),
                    const SizedBox(height: 10),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: bigBlur,
                        color: Colors.white,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(
                          left: 15,
                          right: 15,
                          bottom: 15,
                          top: 20,
                        ),
                        child: Column(
                          children: [
                            LocalRatingBar(
                              star: rating,
                              onTap: (_) {
                                rating = _;
                              },
                            ),
                            const SizedBox(height: 20),
                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: ColorData.allInnerDefault,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 20,
                                  vertical: 4,
                                ),
                                child: TextField(
                                  scrollPadding: EdgeInsets.only(bottom: 400),
                                  controller: reviewController,
                                  maxLines: null,
                                  minLines: 4,
                                  cursorColor: ColorData.allMainActivegray,
                                  decoration: InputDecoration(
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.transparent),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.transparent),
                                      ),
                                      hintText: 'Ваш комментарий',
                                      hintStyle: TextStyle(
                                        color: ColorData.allMainActivegray,
                                      )),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: SizedBox(
                  height: 40 + MediaQuery.of(context).viewInsets.bottom),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: 120,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15),
                    ),
                    color: Colors.white,
                    boxShadow: bigBlur,
                  ),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: ElevatedButton(
                        onPressed: () async {
                          if (rating != null) {
                            final Completer<SessionState> completer =
                                Completer<SessionState>();

                            BlocProvider.of<SessionMinuteBloc>(
                              context,
                              listen: false,
                            ).add(
                              LeaveCommentEvent(
                                institutionId: widget.institutionId,
                                rating: rating?.toDouble(),
                                text: reviewController.text,
                                completer: completer,
                              ),
                            );
                            final result = await completer.future;
                            if (result is LeaveCommentSuccess) {
                              routerDelegate.pushAndRemoveUntil(QfitPage());
                            }
                          } else {
                            Navigator.pop(context);
                          }
                        },
                        style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                            ColorData.clientsButtonColorDefault,
                          ),
                          minimumSize: MaterialStateProperty.all(
                            Size(
                              double.infinity,
                              60,
                            ),
                          ),
                          elevation: MaterialStateProperty.all(0),
                          shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(16),
                              side: BorderSide.none,
                            ),
                          ),
                        ),
                        child: Text(
                          'До встречи!',
                          style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

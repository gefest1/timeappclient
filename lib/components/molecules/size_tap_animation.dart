import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SizeTapAnimation extends StatefulWidget {
  final Widget child;
  final bool active;

  const SizeTapAnimation({
    required this.child,
    this.active = true,
    Key? key,
  }) : super(key: key);

  @override
  _SizeTapAnimationState createState() => _SizeTapAnimationState();
}

class _SizeTapAnimationState extends State<SizeTapAnimation>
    with SingleTickerProviderStateMixin {
  late final AnimationController _animationController = AnimationController(
    vsync: this,
    duration: Duration(
      milliseconds: 160,
    ),
    lowerBound: 0.94,
    upperBound: 1,
    value: 1,
  );

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RepaintBoundary(
      child: AnimatedBuilder(
        animation: _animationController,
        child: _ListenTap(
          controller: _animationController,
          child: widget.child,
          active: widget.active,
        ),
        builder: (ctx, child) {
          return ScaleTransition(
            scale: _animationController,
            child: RepaintBoundary(child: child),
          );
        },
      ),
    );
  }
}

class _ListenTap extends StatefulWidget {
  final AnimationController controller;
  final Widget child;
  final bool active;

  const _ListenTap({
    required this.controller,
    required this.child,
    required this.active,
    Key? key,
  }) : super(key: key);

  @override
  __ListenTapState createState() => __ListenTapState();
}

class __ListenTapState extends State<_ListenTap> {
  bool openStarted = false;
  bool onPointerUpWas = false;
  bool onForawrdedHappen = false;

  @override
  Widget build(BuildContext context) {
    return Listener(
      onPointerUp: (_) async {
        onPointerUpWas = true;

        if (openStarted) return;
        onForawrdedHappen = true;
        widget.controller.forward();
      },
      onPointerDown: (_) async {
        if (!widget.active) return;
        HapticFeedback.lightImpact();
        onPointerUpWas = false;
        onForawrdedHappen = false;
        openStarted = true;
        await widget.controller.reverse();
        openStarted = false;

        if (onPointerUpWas && !onForawrdedHappen) {
          widget.controller.forward();
        }
      },
      child: widget.child,
    );
  }
}

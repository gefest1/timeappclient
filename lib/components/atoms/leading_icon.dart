import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class LeadingIcon extends StatelessWidget {
  const LeadingIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () => Navigator.pop(context),
      icon: RepaintBoundary(
        child: SvgPicture.asset(
          'assets/svg/icon/backArrow.svg',
          color: Color(0xff14142B),
        ),
      ),
    );
  }
}

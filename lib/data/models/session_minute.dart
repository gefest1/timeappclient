import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:li/data/models/client_model.dart';
import 'package:li/data/models/institution_minute.dart';
import 'package:li/data/models/service_model.dart';
import 'package:li/utils/date_time_parse.dart';

class SessionMinute extends Equatable {
  final String? id;
  final DateTime? endTime;
  final num? price;
  final DateTime? startTime;
  final String? clientId;
  final String? serviceId;
  final String? institutionId;
  final Client? client;
  final ServiceMinute? service;
  final num? usedNumOfBonus;
  final num? bonusEarnt;
  final InstitutionsMinute? institution;

  const SessionMinute({
    this.id,
    this.endTime,
    this.price,
    this.startTime,
    this.clientId,
    this.serviceId,
    this.institutionId,
    this.client,
    this.service,
    this.institution,
    this.usedNumOfBonus,
    this.bonusEarnt,
  });

  SessionMinute copyWith({
    String? id,
    String? code,
    DateTime? endTime,
    num? price,
    DateTime? startTime,
    String? clientId,
    String? serviceId,
    String? institutionId,
    Client? client,
    ServiceMinute? service,
    InstitutionsMinute? institution,
    num? bufferTimeMinutes,
    num? usedNumOfBonus,
    num? bonusEarnt,
  }) {
    return SessionMinute(
      id: id ?? this.id,
      endTime: endTime ?? this.endTime,
      price: price ?? this.price,
      startTime: startTime ?? this.startTime,
      clientId: clientId ?? this.clientId,
      serviceId: serviceId ?? this.serviceId,
      institutionId: institutionId ?? this.institutionId,
      client: client ?? this.client,
      service: service ?? this.service,
      institution: institution ?? this.institution,
      usedNumOfBonus: usedNumOfBonus ?? this.usedNumOfBonus,
      bonusEarnt: bonusEarnt ?? this.bonusEarnt,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'endTime': endTime?.toIso8601String(),
      'price': price,
      'startTime': startTime?.toIso8601String(),
      'clientId': clientId,
      'serviceId': serviceId,
      'institutionId': institutionId,
      'client': client?.toMap(),
      'service': service?.toMap(),
      'institution': institution?.toMap(),
      'usedNumOfBonus': usedNumOfBonus,
      'bonusEarnt': bonusEarnt,
    };
  }

  static SessionMinute? fromMap(Map<String, dynamic>? map) {
    if (map == null) return null;
    return SessionMinute(
      id: map['_id'],
      endTime: dateTimeParse(map['endTime']),
      price: map['price'],
      startTime: dateTimeParse(map['startTime']),
      clientId: map['clientId'],
      serviceId: map['serviceId'],
      institutionId: map['institutionId'],
      client: map['client'] == null ? null : Client.fromMap(map['client']),
      service:
          map['service'] == null ? null : ServiceMinute.fromMap(map['service']),
      institution: map['institution'] == null
          ? null
          : InstitutionsMinute.fromMap(map['institution']),
      usedNumOfBonus: map['usedNumOfBonus'],
      bonusEarnt: map['bonusEarnt'],
    );
  }

  String toJson() => json.encode(toMap());

  factory SessionMinute.fromJson(String source) =>
      SessionMinute.fromMap(json.decode(source))!;

  @override
  bool get stringify => true;

  @override
  List<Object?> get props {
    return [
      id,
      endTime,
      price,
      startTime,
      clientId,
      serviceId,
      institutionId,
      client,
      service,
      institution,
      usedNumOfBonus,
      bonusEarnt,
    ];
  }
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:li/data/models/institution_minute.dart';

import 'package:yandex_mapkit/yandex_mapkit.dart';

class YandexDetailMap extends StatefulWidget {
  final InstitutionsMinute institutionsMinute;

  const YandexDetailMap({
    required this.institutionsMinute,
    Key? key,
  }) : super(key: key);

  @override
  _YandexDetailMapState createState() => _YandexDetailMapState();
}

class _YandexDetailMapState extends State<YandexDetailMap> {
  late YandexMapController yandexMapController;
  final StreamController<Null> streamController = StreamController<Null>();
  Point? initPosition;

  @override
  void dispose() {
    streamController.close();
    super.dispose();
  }

  void cameraTracking(
    CameraPosition cameraPosition,
    bool finished,
  ) async {
    streamController.add(null);
  }

  // void _initS() async {
  //   final _initPosition = await determinePosition();
  //   yandexMapController.updateMapObjects([
  //     Placemark(
  //       mapId: MapObjectId('my_location'),
  //       point: Point(
  //         latitude: _initPosition.latitude,
  //         longitude: _initPosition.longitude,
  //       ),
  //       style: PlacemarkStyle(
  //         icon: PlacemarkIcon.fromIconName(iconName: 'assets/location.png'),
  //       ),
  //     )
  //   ]);
  //   setState(() {
  //     initPosition = Point(
  //       latitude: _initPosition.latitude,
  //       longitude: _initPosition.longitude,
  //     );
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        YandexMap(
          onMapCreated: (YandexMapController controller) async {
            this.yandexMapController = yandexMapController;
            await yandexMapController.moveCamera(
              CameraUpdate.newCameraPosition(
                CameraPosition(
                  target: Point(
                    latitude: widget.institutionsMinute.location!.latitude!
                        .toDouble(),
                    longitude: widget.institutionsMinute.location!.longitude!
                        .toDouble(),
                  ),
                ),
              ),
              animation: MapAnimation(
                duration: 1.1,
              ),
            );
          },
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:li/components/atoms/star_painter.dart';

class LocalRatingBar extends StatefulWidget {
  final int? star;
  final Function(int)? onTap;
  const LocalRatingBar({this.star, this.onTap, Key? key}) : super(key: key);

  @override
  State<LocalRatingBar> createState() => _LocalRatingBarState();
}

class _LocalRatingBarState extends State<LocalRatingBar> {
  int? star;

  @override
  void initState() {
    super.initState();
    star = widget.star;
  }

  Widget build(BuildContext context) {
    return Row(
      // mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: List.generate(
        5,
        (index) => GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            setState(() {
              star = index + 1;
            });
            this.widget.onTap?.call(index + 1);
          },
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 4),
            child: StarWidget(
              choosen: star == null ? false : (index + 1) <= star!,
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:li/components/molecules/load_waves_finish.dart';
import 'package:li/components/molecules/timer_tile.dart';
import 'package:li/components/pages/qfit_to_timeapp/finish_minute_page.dart';
import 'package:li/data/models/session_minute.dart';
import 'package:li/main.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/text_styles.dart';
import 'package:time_app_components/utils/testStyles.dart';

import 'active_timer_session.dart';

class ActiveMinutePage extends StatefulWidget {
  final SessionMinute session;

  const ActiveMinutePage({
    required this.session,
    Key? key,
  }) : super(key: key);

  @override
  _ActiveMinutePageState createState() => _ActiveMinutePageState();
}

class _ActiveMinutePageState extends State<ActiveMinutePage> {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Stack(
        children: [
          ListView(
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).padding.top,
              bottom: 240,
            ),
            children: [
              const SizedBox(height: 16),
              Center(
                child: LoadWavesFinish(
                  onTap: () {
                    routerDelegate.push(
                      FinishMinutePage(
                        sessionMinute: widget.session,
                      ),
                    );
                  },
                  title: 'Завершить',
                  stackWidget: [
                    Center(
                      child: Text(
                        "Сессия: " +
                            widget.session.id!
                                .substring(widget.session.id!.length - 5),
                        style: TextStyle(
                          fontSize: H2TextStyle.fontSize,
                          fontWeight: H2TextStyle.fontWeight,
                          height: H2TextStyle.height,
                          color: ColorData.allMainBlack,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  'Для того, чтобы завершить сессию просканируйте QR-код еще раз',
                  style: TextStyle(
                    color: ColorData.allMainActivegray,
                    fontWeight: P3TextStyle.fontWeight,
                    fontSize: P3TextStyle.fontSize,
                    height: P3TextStyle.height,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              const SizedBox(height: 20),
              // TODO
              // Padding(
              //   padding: const EdgeInsets.symmetric(horizontal: 20),
              //   child: Text(
              //     widget.session.service!.name!,
              //   ),
              // ),
              if (widget.session.service?.bufferTimeMinutes != null)
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: TimeTile(
                    minutes: widget.session.service!.bufferTimeMinutes!.toInt(),
                  ),
                ),
            ],
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: DecoratedBox(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(16),
                ),
                boxShadow: [
                  BoxShadow(
                    offset: Offset(0, -10),
                    blurRadius: 30,
                    spreadRadius: 0,
                    color: Color(0xff1a323232),
                  ),
                ],
              ),
              child: Material(
                color: Colors.white,
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(16),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    children: [
                      SizedBox(height: 15),
                      Text(
                        widget.session.service!.name!,
                        style: TextStyle(
                          fontSize: P2TextStyle.fontSize,
                          fontWeight: P2TextStyle.fontWeight,
                          height: P2TextStyle.height,
                          color: ColorData.allMainActivegray,
                        ),
                      ),
                      SizedBox(height: 15),
                      SizedBox(
                        height: 86,
                        child: ActiveTimerSession(
                          startTime: widget.session.startTime!,
                          price: widget.session.service!.price!,
                          bufferTime: widget.session.service?.bufferTimeMinutes
                                  ?.toInt() ??
                              0,
                          // type: widget.session.institution?.type,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      SizedBox(
                        width: double.infinity,
                        child: ElevatedButton(
                          style: ButtonStyle(
                            padding: MaterialStateProperty.all(
                              EdgeInsets.symmetric(
                                vertical: 20,
                              ),
                            ),
                            backgroundColor: MaterialStateProperty.all(
                              Colors.transparent,
                            ),
                            elevation: MaterialStateProperty.all(0),
                            shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(16),
                                side: BorderSide(
                                  color: ColorData.clientsButtonColorPressed,
                                  width: 1,
                                ),
                              ),
                            ),
                          ),
                          onPressed: () {
                            Navigator.popUntil(
                              context,
                              (route) => route.isFirst,
                            );
                          },
                          child: Text(
                            'Скрыть информацию о сессии',
                            style: TextStyle(
                              fontSize: P2TextStyle.fontSize,
                              fontWeight: P2TextStyle.fontWeight,
                              height: P2TextStyle.height,
                              color: ColorData.clientsButtonColorPressed,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).padding.bottom + 20,
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';
import 'package:url_launcher/url_launcher.dart';

class StartButton extends StatelessWidget {
  final VoidCallback onTap;

  const StartButton({
    required this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ElevatedButton(
          onPressed: onTap,
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(
              ColorData.clientsButtonColorDefault,
            ),
            minimumSize: MaterialStateProperty.all(
              Size(
                double.infinity,
                60,
              ),
            ),
            shape: MaterialStateProperty.all(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16),
                side: BorderSide.none,
              ),
            ),
          ),
          child: Text(
            'Старт',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 18,
            ),
          ),
        ),
        const SizedBox(height: 20),
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            style: TextStyle(
              fontSize: P4TextStyle.fontSize,
              fontWeight: P4TextStyle.fontWeight,
              height: P4TextStyle.height,
              color: ColorData.allMainActivegray,
            ),
            children: [
              TextSpan(text: 'Нажимая «Старт» вы принимаете'),
              TextSpan(
                text: ' условия посещения места',
                style: TextStyle(
                  color: ColorData.clientsButtonColorPressed,
                ),
                recognizer: TapGestureRecognizer()
                  ..onTap = () async {
                    await launch('https://timeapp.kz/partnerterms');
                  },
              ),
            ],
          ),
        )
      ],
    );
  }
}

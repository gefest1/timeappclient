import 'dart:async';

import 'package:flutter/material.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

import 'package:li/components/atoms/map_pointer.dart';
import 'package:li/components/pages/qfit_to_timeapp/qfit_detail_page.dart';
import 'package:li/data/models/institution_minute.dart';
import 'package:li/main.dart';
import 'package:li/utils/flow_menu_delegate.dart';
import 'package:li/utils/locationsBetween.dart';
import 'package:li/utils/offset_flow_mixin.dart';

extension ListenFlow on _FlowLocalOffsetsState {
  void listenChanges([Null _]) async {
    if (notFinished) return;
    notFinished = true;

    final List<Offset?> _newOffsets = await Future.wait([
      getOffset(
        latitude: widget.institutionsMinute.location!.latitude!.toDouble(),
        longitude: widget.institutionsMinute.location!.longitude!.toDouble(),
      )
    ]);
    setState(() {
      _institutions = [widget.institutionsMinute];
    });
    offsetListen.value = _newOffsets as List<Offset>;
    notFinished = false;
  }
}

class FlowLocalOffsets extends StatefulWidget {
  final InstitutionsMinute institutionsMinute;
  final YandexMapController yandexMapController;
  final StreamController<Null> streamController;
  final Point myLocation;

  const FlowLocalOffsets({
    required this.institutionsMinute,
    required this.yandexMapController,
    required this.streamController,
    required this.myLocation,
    Key? key,
  }) : super(key: key);

  @override
  _FlowLocalOffsetsState createState() => _FlowLocalOffsetsState();
}

class _FlowLocalOffsetsState extends State<FlowLocalOffsets>
    with OffsetExtension {
  final ValueNotifier<List<Offset>> offsetListen =
      ValueNotifier<List<Offset>>([]);
  late final StreamSubscription<Null> streamSubscription;
  bool notFinished = false;
  List<InstitutionsMinute> _institutions = [];

  @override
  void initState() {
    yandexMapController = widget.yandexMapController;
    streamSubscription = widget.streamController.stream.listen(listenChanges);
    super.initState();
  }

  @override
  void dispose() {
    streamSubscription.cancel();
    super.dispose();
  }

  @override
  void setState(VoidCallback fn) => super.setState(fn);

  @override
  Widget build(BuildContext context) {
    return Flow(
      delegate: FlowMenuDelegate(
        offsetListen: offsetListen,
      ),
      children: _institutions
          .map(
            (club) => MapPointer(
              key: ValueKey(club.id),
              title: '${club.averagePrice} ₸/мин',
              label: club.name,
              tile: locationBetweenText(
                club.location!.latitude!.toDouble(),
                club.location!.longitude!.toDouble(),
                widget.myLocation.latitude,
                widget.myLocation.longitude,
              ),
              onTap: () {
                routerDelegate.push(
                  QfitDetailPage(
                    institution: club,
                  ),
                );
              },
            ),
          )
          .toList(),
    );
  }
}

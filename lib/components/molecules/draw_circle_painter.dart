import 'package:flutter/material.dart';
import 'dart:math' as math;

class DrawCirclePainter extends CustomPainter {
  final double strokeWidth;
  final Color valueColor;
  final double percentage;

  const DrawCirclePainter({
    required this.strokeWidth,
    required this.valueColor,
    required this.percentage,
    Listenable? repaint,
  }) : super(
          repaint: repaint,
        );

  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..color = valueColor
      ..strokeWidth = strokeWidth
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;

    canvas.drawArc(
      Offset.zero & size,
      math.pi * 1.5,
      -math.pi * 2 * this.percentage,
      false,
      paint,
    );
  }

  @override
  bool shouldRepaint(DrawCirclePainter oldDelegate) => true;

  @override
  bool shouldRebuildSemantics(DrawCirclePainter oldDelegate) => true;
}

class BookCard extends StatefulWidget {
  const BookCard({Key? key}) : super(key: key);

  @override
  _BookCardState createState() => _BookCardState();
}

class _BookCardState extends State<BookCard>
    with SingleTickerProviderStateMixin {
  late final AnimationController _animationController = AnimationController(
    vsync: this,
    duration: Duration(minutes: 3),
  );

  @override
  void initState() {
    _animationController.forward();
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  List<Color> _colorPick() {
    final Color firstColor;
    if (_animationController.value < 0.5) {
      firstColor = Color.lerp(
        Color(0xff00EF32),
        Color(0xff007AFF),
        _animationController.value * 2,
      )!;
    } else {
      firstColor = Color.lerp(
        Color(0xff007AFF),
        Color(0xffFA3E3E),
        (_animationController.value - 0.5) * 2,
      )!;
    }

    final Color secondColor;
    if (_animationController.value < 0.5) {
      secondColor = Color.lerp(
        Color(0xff00D72C),
        Colors.purple[900],
        _animationController.value * 2,
      )!;
    } else {
      secondColor = Color.lerp(
        Colors.purple[900],
        Color(0xffFBC0000),
        (_animationController.value - 0.5) * 2,
      )!;
    }
    return [firstColor, secondColor];
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (ctx, child) {
        final _revValue = (1 - _animationController.value);

        return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            gradient: LinearGradient(
              colors: _colorPick(),
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                Text(
                  (Duration(minutes: 3) * _revValue).toString().substring(3, 7),
                  style: TextStyle(
                    fontSize: 36,
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    height: 43 / 36,
                  ),
                ),
                const SizedBox(height: 8),
                CustomPaint(
                  foregroundPainter: DrawCirclePainter(
                    strokeWidth: 3,
                    valueColor: Colors.white,
                    percentage: _revValue,
                  ),
                  child: CircleAvatar(
                    radius: 60,
                    backgroundColor: Colors.blue[700],
                  ),
                ),
                const SizedBox(height: 10),
                Text(
                  'Ждем, пока Иван ответит на вашу заявку',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                    fontSize: 18,
                    height: 21 / 18,
                  ),
                  textAlign: TextAlign.center,
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

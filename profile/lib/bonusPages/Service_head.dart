import 'package:flutter/material.dart';
import 'package:main_page/component/molecules/atoms/time_food.dart';
import 'package:main_page/component/molecules/atoms/time_home.dart';
import 'package:main_page/component/molecules/atoms/time_service.dart';
import 'package:profile/bonusPages/service_chip.dart';
import 'package:profile/icon/time_minutePay.dart';

class Servicehead extends StatelessWidget {
  final int index;
  final void Function(int) onTap;

  const Servicehead({
    required this.index,
    required this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 113,
      child: ListView(
        scrollDirection: Axis.horizontal,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        children: [
          ServiceChip(
            onTap: () => onTap.call(0),
            title: 'На поминутное посещение',
            active: false,
            child: CustomPaint(
              painter: TimeAppMinutePayWide(),
              size: const Size(139, 128),
            ),
          ),
          const SizedBox(width: 10),
          ServiceChip(
            onTap: () => onTap.call(1),
            title: 'На аренду недвижимости',
            active: true,
            child: CustomPaint(
              painter: TimeHomeCustomPainter(active: 1 == index),
              size: const Size(197, 100),
            ),
          ),
          const SizedBox(width: 10),
          ServiceChip(
            onTap: () => onTap.call(2),
            title: 'На оплату еды и доставки',
            active: true,
            child: CustomPaint(
              painter: TimeFoodCustomPainter(active: 2 == index),
              size: const Size(130, 101),
            ),
          ),
          const SizedBox(width: 10),
          ServiceChip(
            onTap: () => onTap.call(3),
            title: 'На оплату услуг',
            active: true,
            child: CustomPaint(
              painter: TimeServiceCustomPainter(active: 3 == index),
              size: const Size(112, 92),
            ),
          ),
        ],
      ),
    );
  }
}

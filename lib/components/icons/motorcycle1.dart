import 'package:flutter/material.dart';

class Motorcycle1CustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.8535143, size.height * 0.5585929);
    path_0.cubicTo(
        size.width * 0.8295143,
        size.height * 0.5585929,
        size.width * 0.8071750,
        size.height * 0.5649500,
        size.width * 0.7871643,
        size.height * 0.5752250);
    path_0.lineTo(size.width * 0.7475679, size.height * 0.5281679);
    path_0.cubicTo(
        size.width * 0.7651214,
        size.height * 0.5177929,
        size.width * 0.7841107,
        size.height * 0.5098107,
        size.width * 0.8041857,
        size.height * 0.5053179);
    path_0.cubicTo(
        size.width * 0.8205500,
        size.height * 0.5016321,
        size.width * 0.8367679,
        size.height * 0.5002821,
        size.width * 0.8536750,
        size.height * 0.5000321);
    path_0.cubicTo(
        size.width * 0.8755107,
        size.height * 0.5002000,
        size.width * 0.8969964,
        size.height * 0.4915857,
        size.width * 0.9130714,
        size.height * 0.4756536);
    path_0.cubicTo(
        size.width * 0.9298393,
        size.height * 0.4590607,
        size.width * 0.9394536,
        size.height * 0.4359750,
        size.width * 0.9394536,
        size.height * 0.4086750);
    path_0.cubicTo(
        size.width * 0.9394536,
        size.height * 0.2586289,
        size.width * 0.8130500,
        size.height * 0.1484375,
        size.width * 0.6795893,
        size.height * 0.1484375);
    path_0.lineTo(size.width * 0.6464857, size.height * 0.1484375);
    path_0.cubicTo(
        size.width * 0.6302964,
        size.height * 0.1484375,
        size.width * 0.6171857,
        size.height * 0.1615446,
        size.width * 0.6171857,
        size.height * 0.1777343);
    path_0.cubicTo(
        size.width * 0.6171857,
        size.height * 0.1939239,
        size.width * 0.6302964,
        size.height * 0.2070314,
        size.width * 0.6464857,
        size.height * 0.2070314);
    path_0.lineTo(size.width * 0.6795893, size.height * 0.2070314);
    path_0.cubicTo(
        size.width * 0.6883179,
        size.height * 0.2070314,
        size.width * 0.6966393,
        size.height * 0.2085343,
        size.width * 0.7050786,
        size.height * 0.2096175);
    path_0.lineTo(size.width * 0.7050786, size.height * 0.2662279);
    path_0.cubicTo(
        size.width * 0.6880643,
        size.height * 0.2677689,
        size.width * 0.6720429,
        size.height * 0.2730332,
        size.width * 0.6568821,
        size.height * 0.2800904);
    path_0.cubicTo(
        size.width * 0.6566929,
        size.height * 0.2798689,
        size.width * 0.6564500,
        size.height * 0.2797243,
        size.width * 0.6562643,
        size.height * 0.2795029);
    path_0.cubicTo(
        size.width * 0.6322179,
        size.height * 0.2907486,
        size.width * 0.6114893,
        size.height * 0.3083343,
        size.width * 0.5964750,
        size.height * 0.3308564);
    path_0.lineTo(size.width * 0.5297857, size.height * 0.4308786);
    path_0.cubicTo(
        size.width * 0.5252071,
        size.height * 0.4377143,
        size.width * 0.5179964,
        size.height * 0.4423214,
        size.width * 0.5098429,
        size.height * 0.4435821);
    path_0.cubicTo(
        size.width * 0.4505321,
        size.height * 0.4526964,
        size.width * 0.3991179,
        size.height * 0.4905107,
        size.width * 0.3721179,
        size.height * 0.5457214);
    path_0.cubicTo(
        size.width * 0.3971393,
        size.height * 0.5831214,
        size.width * 0.4121107,
        size.height * 0.6276786,
        size.width * 0.4121107,
        size.height * 0.6757821);
    path_0.lineTo(size.width * 0.3515625, size.height * 0.6757821);
    path_0.cubicTo(
        size.width * 0.3515625,
        size.height * 0.5788500,
        size.width * 0.2727129,
        size.height * 0.5000000,
        size.width * 0.1757814,
        size.height * 0.5000000);
    path_0.cubicTo(size.width * 0.07884964, size.height * 0.5000000, 0,
        size.height * 0.5788500, 0, size.height * 0.6757821);
    path_0.cubicTo(
        0,
        size.height * 0.7727143,
        size.width * 0.07884964,
        size.height * 0.8515643,
        size.width * 0.1757814,
        size.height * 0.8515643);
    path_0.cubicTo(
        size.width * 0.2520675,
        size.height * 0.8515643,
        size.width * 0.3164979,
        size.height * 0.8024286,
        size.width * 0.3407668,
        size.height * 0.7343750);
    path_0.lineTo(size.width * 0.6171857, size.height * 0.7343750);
    path_0.cubicTo(
        size.width * 0.6333786,
        size.height * 0.7343750,
        size.width * 0.6464857,
        size.height * 0.7212679,
        size.width * 0.6464857,
        size.height * 0.7050786);
    path_0.cubicTo(
        size.width * 0.6464857,
        size.height * 0.6887964,
        size.width * 0.6484286,
        size.height * 0.6721714,
        size.width * 0.6522357,
        size.height * 0.6557000);
    path_0.cubicTo(
        size.width * 0.6602179,
        size.height * 0.6214893,
        size.width * 0.6780107,
        size.height * 0.5903536,
        size.width * 0.7018893,
        size.height * 0.5649179);
    path_0.lineTo(size.width * 0.7414321, size.height * 0.6119071);
    path_0.cubicTo(
        size.width * 0.7202750,
        size.height * 0.6373071,
        size.width * 0.7070321,
        size.height * 0.6695179,
        size.width * 0.7070321,
        size.height * 0.7050786);
    path_0.cubicTo(
        size.width * 0.7070321,
        size.height * 0.7858429,
        size.width * 0.7727500,
        size.height * 0.8515643,
        size.width * 0.8535143,
        size.height * 0.8515643);
    path_0.cubicTo(size.width * 0.9342821, size.height * 0.8515643, size.width,
        size.height * 0.7858429, size.width, size.height * 0.7050786);
    path_0.cubicTo(
        size.width,
        size.height * 0.6243143,
        size.width * 0.9342821,
        size.height * 0.5585929,
        size.width * 0.8535143,
        size.height * 0.5585929);
    path_0.close();
    path_0.moveTo(size.width * 0.1757814, size.height * 0.7929679);
    path_0.cubicTo(
        size.width * 0.1111525,
        size.height * 0.7929679,
        size.width * 0.05859357,
        size.height * 0.7404107,
        size.width * 0.05859357,
        size.height * 0.6757821);
    path_0.cubicTo(
        size.width * 0.05859357,
        size.height * 0.6111536,
        size.width * 0.1111525,
        size.height * 0.5585929,
        size.width * 0.1757814,
        size.height * 0.5585929);
    path_0.cubicTo(
        size.width * 0.2404100,
        size.height * 0.5585929,
        size.width * 0.2929686,
        size.height * 0.6111536,
        size.width * 0.2929686,
        size.height * 0.6757821);
    path_0.lineTo(size.width * 0.2050782, size.height * 0.6757821);
    path_0.cubicTo(
        size.width * 0.1889114,
        size.height * 0.6757821,
        size.width * 0.1757814,
        size.height * 0.6626500,
        size.width * 0.1757814,
        size.height * 0.6464857);
    path_0.cubicTo(
        size.width * 0.1757814,
        size.height * 0.6302964,
        size.width * 0.1626739,
        size.height * 0.6171857,
        size.width * 0.1464843,
        size.height * 0.6171857);
    path_0.cubicTo(
        size.width * 0.1302946,
        size.height * 0.6171857,
        size.width * 0.1171875,
        size.height * 0.6302964,
        size.width * 0.1171875,
        size.height * 0.6464857);
    path_0.cubicTo(
        size.width * 0.1171875,
        size.height * 0.6949464,
        size.width * 0.1566161,
        size.height * 0.7343750,
        size.width * 0.2050782,
        size.height * 0.7343750);
    path_0.lineTo(size.width * 0.2766725, size.height * 0.7343750);
    path_0.cubicTo(
        size.width * 0.2563475,
        size.height * 0.7692321,
        size.width * 0.2189714,
        size.height * 0.7929679,
        size.width * 0.1757814,
        size.height * 0.7929679);
    path_0.close();
    path_0.moveTo(size.width * 0.8535143, size.height * 0.7929679);
    path_0.cubicTo(
        size.width * 0.8050536,
        size.height * 0.7929679,
        size.width * 0.7656250,
        size.height * 0.7535393,
        size.width * 0.7656250,
        size.height * 0.7050786);
    path_0.cubicTo(
        size.width * 0.7656250,
        size.height * 0.6881036,
        size.width * 0.7706821,
        size.height * 0.6723786,
        size.width * 0.7790536,
        size.height * 0.6589214);
    path_0.lineTo(size.width * 0.8306286, size.height * 0.7233893);
    path_0.cubicTo(
        size.width * 0.8408143,
        size.height * 0.7360929,
        size.width * 0.8592607,
        size.height * 0.7380071,
        size.width * 0.8718250,
        size.height * 0.7279679);
    path_0.cubicTo(
        size.width * 0.8844464,
        size.height * 0.7178357,
        size.width * 0.8865036,
        size.height * 0.6994179,
        size.width * 0.8764036,
        size.height * 0.6867679);
    path_0.lineTo(size.width * 0.8248893, size.height * 0.6223750);
    path_0.cubicTo(
        size.width * 0.8339071,
        size.height * 0.6192393,
        size.width * 0.8434357,
        size.height * 0.6171857,
        size.width * 0.8535143,
        size.height * 0.6171857);
    path_0.cubicTo(
        size.width * 0.9019786,
        size.height * 0.6171857,
        size.width * 0.9414071,
        size.height * 0.6566179,
        size.width * 0.9414071,
        size.height * 0.7050786);
    path_0.cubicTo(
        size.width * 0.9414071,
        size.height * 0.7535393,
        size.width * 0.9019786,
        size.height * 0.7929679,
        size.width * 0.8535143,
        size.height * 0.7929679);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_0, paint_0_fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.4975286, size.height * 0.3736500);
    path_1.lineTo(size.width * 0.5477214, size.height * 0.2983550);
    path_1.cubicTo(
        size.width * 0.5637679,
        size.height * 0.2742768,
        size.width * 0.5848107,
        size.height * 0.2547150,
        size.width * 0.6087107,
        size.height * 0.2395250);
    path_1.cubicTo(
        size.width * 0.5765607,
        size.height * 0.2192229,
        size.width * 0.5395000,
        size.height * 0.2070314,
        size.width * 0.5000000,
        size.height * 0.2070314);
    path_1.cubicTo(
        size.width * 0.4307786,
        size.height * 0.2070314,
        size.width * 0.3668393,
        size.height * 0.2423171,
        size.width * 0.3292018,
        size.height * 0.2989043);
    path_1.lineTo(size.width * 0.3760607, size.height * 0.3270264);
    path_1.cubicTo(
        size.width * 0.4137036,
        size.height * 0.3496093,
        size.width * 0.4549250,
        size.height * 0.3647321,
        size.width * 0.4975286,
        size.height * 0.3736500);
    path_1.close();

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_1, paint_1_fill);

    Path path_2 = Path();
    path_2.moveTo(size.width * 0.3459093, size.height * 0.3772571);
    path_2.lineTo(size.width * 0.1579057, size.height * 0.2656250);
    path_2.lineTo(size.width * 0.02929686, size.height * 0.2656250);
    path_2.cubicTo(size.width * 0.01310729, size.height * 0.2656250, 0,
        size.height * 0.2787321, 0, size.height * 0.2949218);
    path_2.cubicTo(
        0,
        size.height * 0.3756857,
        size.width * 0.06571964,
        size.height * 0.4414071,
        size.width * 0.1464843,
        size.height * 0.4414071);
    path_2.cubicTo(
        size.width * 0.2050857,
        size.height * 0.4414071,
        size.width * 0.2658768,
        size.height * 0.4426821,
        size.width * 0.3310089,
        size.height * 0.4992964);
    path_2.cubicTo(
        size.width * 0.3312607,
        size.height * 0.4988321,
        size.width * 0.3312607,
        size.height * 0.4983071,
        size.width * 0.3315429,
        size.height * 0.4978571);
    path_2.cubicTo(
        size.width * 0.3542711,
        size.height * 0.4613107,
        size.width * 0.3863607,
        size.height * 0.4329750,
        size.width * 0.4231500,
        size.height * 0.4130643);
    path_2.cubicTo(
        size.width * 0.3965143,
        size.height * 0.4033214,
        size.width * 0.3704393,
        size.height * 0.3919679,
        size.width * 0.3459093,
        size.height * 0.3772571);
    path_2.close();

    Paint paint_2_fill = Paint()..style = PaintingStyle.fill;
    paint_2_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_2, paint_2_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}

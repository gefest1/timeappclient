import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/network/graphql_provider.dart';
import 'package:take_away/data/models/institution_take_away.dart';

class InstitutionTakeAwayProvider extends ChangeNotifier {
  late GraphqlProvider graphqlProvider = GraphqlProvider();
  late GraphqlTakeAwayProvider graphqlTakeAwayProvider =
      GraphqlTakeAwayProvider();
  final Map<String, InstitutionTakeAway> institutionTakeAwayMap = {};
  final Map<String, bool> institutionTakeAwayMapLoading = {};

  void getinstitution(String id) async {
    try {
      if (institutionTakeAwayMapLoading[id] == true) return;
      institutionTakeAwayMapLoading[id] = true;
      final _queryResult = await graphqlTakeAwayProvider.client.query(
        QueryOptions(
          document: gql('''
        query(\$institutionId: String!){
          getInstitutionByIdwithSpecDish(institutionId: \$institutionId){
            _id	
        		location {
              address
              latitude
              longitude
            }
        		name
        		photoURL {
              XL
            }
        		specialistId
            dishes {
              _id 
            }
            specialist {
              _id
        			certifications {
          			M
          			XL
          			thumbnail
        			}
        			contacts {
          			telegram
          			whatsapp
          			instagram
          			phoneNumber
        			}
        			dateOfBirth
        			description
        			email
        			fullName
        			instituionIds
        			phoneNumber
        			photoURL {
                M
                XL
                thumbnail
              }
        			videoUrl
            }
          }
        }
        '''),
          variables: {"institutionId": id},
        ),
      );
      if (_queryResult.hasException) throw _queryResult.exception!;
      final _inst = InstitutionTakeAway.fromMap(
        _queryResult.data!['getInstitutionByIdwithSpecDish'],
      );
      institutionTakeAwayMap[id] = _inst;
      notifyListeners();
    } finally {
      institutionTakeAwayMapLoading[id] = false;
    }
  }
}

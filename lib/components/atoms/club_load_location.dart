import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ClubLoadLocation extends StatefulWidget {
  const ClubLoadLocation({Key? key}) : super(key: key);

  @override
  _ClubLoadLocationState createState() => _ClubLoadLocationState();
}

class _ClubLoadLocationState extends State<ClubLoadLocation>
    with SingleTickerProviderStateMixin {
  late Animation<double> animation;
  late AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        vsync: this, duration: Duration(milliseconds: 1500));

    animation = Tween<double>(begin: .0, end: .4).animate(controller)
      ..addListener(() {
        if (this.mounted) {
          setState(() {});
        }
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reset();
          controller.forward();
        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      });

    controller.forward();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  List<double> _stops = List<double>.generate(8, (index) => index * 0.2 - 0.4);
  List<Color> _colors = List<Color>.generate(
    8,
    (index) =>
        index.isOdd ? Colors.transparent : Color(0xffF9F9F9).withOpacity(0.2),
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      decoration: BoxDecoration(
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(6),
      ),
      foregroundDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        gradient: LinearGradient(
          begin: Alignment(-1.0, -1.0),
          end: Alignment(1.0, 1.0),
          colors: _colors,
          stops: _stops.map((s) => s + animation.value).toList(),
        ),
      ),
      alignment: Alignment.center,
      child: RepaintBoundary(
        child: SvgPicture.asset('assets/svg/icon/sendFilled.svg'),
      ),
    );
  }
}

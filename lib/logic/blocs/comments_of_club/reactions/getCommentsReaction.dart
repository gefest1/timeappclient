part of '../commentsofclub_bloc.dart';

extension GetCommentsReaction on CommentsofclubBloc {
  void getCommentsOfInstitution(
    GetCommentsOfClubEvent event,
    Emitter<CommentsofclubState> emit,
  ) async {
    try {
      final _result = await GetCommentsAPI.getCommentsAPI(
        graphqlMinuteProvider: graphqlMinuteProvider,
        institutionId: event.institutionId,
        page: 1,
      );
      final CommentsFetched? cachedState =
          state is CommentsFetched ? (state as CommentsFetched) : null;
      final Map<String, List<CommentModel>> newCommentMap = {
        ...cachedState?.comments ?? {},
        event.institutionId: _result.getCommentsOfAInstitution
      };
      final Map<String, MetaRatingModel> newMetaData = {
        ...cachedState?.metaRatingModel ?? {},
        event.institutionId: _result.getRatingMetaOfInstitution
      };

      emit(
        CommentsFetched(newCommentMap, newMetaData),
      );
    } catch (e) {
      log(e.toString());
    }
  }
}

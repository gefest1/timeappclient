import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class LittleInfoCard extends StatelessWidget {
  final Color color;
  final String assetString;
  final String title;

  const LittleInfoCard({
    required this.color,
    required this.assetString,
    required this.title,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 76,
      width: 76,
      decoration: BoxDecoration(
        color: this.color,
        shape: BoxShape.circle,
      ),
      alignment: Alignment.center,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          RepaintBoundary(
            child: SvgPicture.asset(
              assetString,
              height: 24,
              width: 24,
              color: Colors.white,
            ),
          ),
          Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 24,
              height: 28 / 24,
              color: Colors.white,
            ),
          )
        ],
      ),
    );
  }
}

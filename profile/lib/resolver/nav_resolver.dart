import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:li/main.dart';
import 'package:profile/notification/notification_page.dart';

class NavigationResolver {
  Map<int, Widget> map = {
    0: const NotificationPage(),
    1: const NotificationPage(),
    2: const NotificationPage()
  };

  showPage(int index) {
    routerDelegate.push(map[index] ?? const NotificationPage());
  }
}

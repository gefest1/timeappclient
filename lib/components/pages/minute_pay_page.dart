import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:li/components/molecules/load_waves_finish.dart';
import 'package:li/components/molecules/minute/main_page_card.dart';
import 'package:li/components/pages/auth/your_phone_number_page.dart';
import 'package:li/components/pages/qfit_to_timeapp/qr_scan_page.dart';
import 'package:li/logic/blocs/qfit_bloc/session/session_minute_bloc.dart';
import 'package:li/main.dart';
import 'package:main_page/component/molecules/wrap_services.dart';

class MinutePayPage extends StatelessWidget {
  const MinutePayPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverList(
      delegate: SliverChildListDelegate([
        BlocBuilder<SessionMinuteBloc, SessionState>(
          builder: (ctx, state) {
            if (state is StartedSessionState) {
              if (state.sessions.isEmpty) return const SizedBox(height: 20);
              return Padding(
                padding: const EdgeInsets.only(
                  left: 20,
                  right: 20,
                  top: 20,
                ),
                child: ActiveServices(sessions: state.sessions),
              );
            } else {
              return const SizedBox(height: 20);
            }
          },
        ),
        LoadWavesFinish(
          onTap: () {
            routerDelegate.push(
              isRegistered() ? const QrScanPage() : const YourPhoneNumber(),
            );
          },
          title: 'Начать',
        ),
        const SizedBox(height: 20),
        const MainPageCards(),
      ]),
    );
  }
}

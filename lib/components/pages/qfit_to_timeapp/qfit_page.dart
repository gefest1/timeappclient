import 'package:flutter/material.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';

import 'package:li/components/atoms/leading_icon.dart';
import 'package:li/components/molecules/load_waves_finish.dart';
import 'package:li/components/molecules/minute/main_page_card.dart';
import 'package:li/components/pages/qfit_to_timeapp/qr_scan_page.dart';
import 'package:li/main.dart';

import '../../../utils/color.dart';

class QfitPage extends StatefulWidget {
  const QfitPage({Key? key}) : super(key: key);

  @override
  State<QfitPage> createState() => _QfitPageState();
}

class _QfitPageState extends State<QfitPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(
        leading: const LeadingIcon(),
        actions: [],
        centerTitle: true,
        title: const Text(
          'Поминутная оплата',
          style: TextStyle(
            fontSize: 18,
            height: 21 / 18,
            fontWeight: FontWeight.w400,
            color: ColorData.allMainBlack,
          ),
        ),
      ),
      body: ListView(
        padding: EdgeInsets.only(
          bottom: 100,
        ),
        children: [
          LoadWavesFinish(
            onTap: () {
              routerDelegate.push(const QrScanPage());
            },
            title: 'Начать',
          ),
          const SizedBox(height: 20),
          const MainPageCards(),
        ],
      ),
    );
  }
}

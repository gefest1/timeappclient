import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class UpdateBlur extends StatelessWidget {
  const UpdateBlur({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned.fill(
      child: DecoratedBox(
        decoration: const BoxDecoration(
          color: Colors.black54,
          borderRadius: BorderRadius.all(Radius.circular(16)),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              RepaintBoundary(
                child: SvgPicture.asset(
                  'assets/svg/icon/timer.svg',
                  color: Colors.white,
                ),
              ),
              const Text(
                'Скоро будет доступно обновление',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  height: 21 / 14,
                  color: Colors.white,
                ),
                textAlign: TextAlign.center,
              )
            ],
          ),
        ),
      ),
    );
  }
}

part of '../GlobalSessionBloc.dart';

extension GetSession on GlobalSessionBloc {
  void getSession(SessionEvent event, Emitter<SessionState> emit) async {
    final List<GlobalSessionModel> globalActiveSessions =
        await GetActiveSession.getActiveSession(
      client: graphqlProvider.client,
    );
    bool minuteBloc = false;
    for (final activeSession in globalActiveSessions) {
      minuteBloc =
          minuteBloc || (activeSession.type == AllowedSessionTypes.MINUTEPAY);
    }
    if (minuteBloc) {
      this.sessionMinuteBloc.add(GetActiveSessionEvent());
    }
  }
}

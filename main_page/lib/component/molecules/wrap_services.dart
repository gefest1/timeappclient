import 'package:flutter/material.dart';
import 'package:li/data/models/session_minute.dart';
import 'package:li/utils/color.dart';
import 'package:main_page/component/molecules/active_tile_service.dart';

class ActiveServices extends StatelessWidget {
  final List<SessionMinute> sessions;

  const ActiveServices({
    required this.sessions,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: const Color(0xFFF8F8F8),
      ),
      padding: const EdgeInsets.only(
        bottom: 20,
        right: 15,
        left: 15,
        top: 15,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'Активные услуги',
            style: TextStyle(
              color: ColorData.mainBlack,
              fontWeight: FontWeight.w600,
              fontSize: 24,
              height: 29 / 24,
            ),
          ),
          const SizedBox(height: 10),
          Column(
            children: List.generate(
              sessions.length * 2,
              (index) => index % 2 == 1
                  ? const SizedBox(height: 10)
                  : ActiveTileService(
                      session: sessions[index ~/ 2],
                    ),
            ),
          ),
        ],
      ),
    );
  }
}

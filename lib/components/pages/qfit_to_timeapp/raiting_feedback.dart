import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/atoms/star_painter.dart';
import 'package:li/utils/color.dart';
import 'dart:math' as math;

import 'package:time_app_components/utils/testStyles.dart';

class RaingFeedback extends StatelessWidget {
  final String rating;
  final String ratingCount;
  final int fiveStars;
  final int fourStars;
  final int threeStars;
  final int twoStars;
  final int oneStars;
  final int sum;

  const RaingFeedback({
    required this.ratingCount,
    required this.rating,
    required this.fiveStars,
    required this.fourStars,
    required this.threeStars,
    required this.twoStars,
    required this.oneStars,
    required this.sum,
    Key? key,
  }) : super(key: key);

  Widget getElement(int count, double percent) {
    return Row(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: List.generate(
            5,
            (index) => (4 - index) < count
                ? CustomPaint(
                    painter: StarCustomPainter(
                      color: ColorData.allMainBlack,
                    ),
                    size: Size(10, 10),
                  )
                // SvgPicture.asset(
                //     'assets/svg/icon/star.svg',
                //     height: 10,
                //     width: 10,
                //     color: ColorData.allMainBlack,
                //   )
                : const SizedBox(
                    width: 10,
                    height: 10,
                  ),
          ),
        ),
        const SizedBox(width: 6),
        Expanded(
          child: Row(
            children: [
              Expanded(
                flex: (percent * 100).round(),
                child: Container(
                  decoration: BoxDecoration(
                    color: ColorData.specsButtonColorDefault,
                    borderRadius: BorderRadius.all(
                      Radius.circular(0.5),
                    ),
                  ),
                  height: 1,
                ),
              ),
              Expanded(
                flex: ((1 - percent) * 100).round(),
                child: Container(
                  decoration: BoxDecoration(
                    color: ColorData.allTextInactive,
                    borderRadius: BorderRadius.all(
                      Radius.circular(0.5),
                    ),
                  ),
                  height: 1,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final sum = math.max(this.sum, 1);
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text(
          'Рейтинг и отзывы',
          style: const TextStyle(
              fontSize: H2TextStyle.fontSize,
              fontWeight: H2TextStyle.fontWeight,
              height: H2TextStyle.height,
              color: ColorData.allMainBlack),
        ),
        const SizedBox(height: 6),
        Column(
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        rating,
                        style: TextStyle(
                          fontSize: 80,
                          color: ColorData.allMainBlack,
                          height: 91 / 80,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      getElement(5, fiveStars / sum),
                      const SizedBox(height: 6),
                      getElement(4, fourStars / sum),
                      const SizedBox(height: 6),
                      getElement(3, fourStars / sum),
                      const SizedBox(height: 6),
                      getElement(2, twoStars / sum),
                      const SizedBox(height: 6),
                      getElement(1, oneStars / sum),
                    ],
                  ),
                ),
              ],
            ),
            const SizedBox(height: 4),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Средняя оценка',
                  style: TextStyle(
                    fontSize: P3TextStyle.fontSize,
                    color: ColorData.allMainBlack,
                    height: P3TextStyle.height,
                    fontWeight: P3TextStyle.fontWeight,
                  ),
                ),
                Text(
                  ratingCount + ' отзывов',
                  style: TextStyle(
                    fontSize: P3TextStyle.fontSize,
                    color: ColorData.allMainBlack,
                    height: P3TextStyle.height,
                    fontWeight: P3TextStyle.fontWeight,
                  ),
                ),
              ],
            )
          ],
        ),
      ],
    );
  }
}

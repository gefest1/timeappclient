import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:li/data/models/Contacts.dart';
import 'package:li/data/models/Sex.dart';
import 'package:li/data/models/photo_url.dart';

class Specialist extends Equatable {
  final String? id;
  final PhotoUrl? certifications;
  final Contacts? contacts;
  final DateTime? dateOfBirth;
  final String? description;
  final String? email;
  final String? fullName;
  final List<String>? instituionIds;
  final String? phoneNumber;
  final PhotoUrl? photoURL;
  final SexEnum? sex;
  final String? videoUrl;

  const Specialist({
    this.id,
    this.certifications,
    this.contacts,
    this.dateOfBirth,
    this.description,
    this.email,
    this.fullName,
    this.instituionIds,
    this.phoneNumber,
    this.photoURL,
    this.sex,
    this.videoUrl,
  });

  Specialist copyWith({
    String? id,
    PhotoUrl? certifications,
    Contacts? contacts,
    DateTime? dateOfBirth,
    String? description,
    String? email,
    String? fullName,
    List<String>? instituionIds,
    String? phoneNumber,
    PhotoUrl? photoURL,
    SexEnum? sex,
    String? videoUrl,
  }) {
    return Specialist(
      id: id ?? this.id,
      certifications: certifications ?? this.certifications,
      contacts: contacts ?? this.contacts,
      dateOfBirth: dateOfBirth ?? this.dateOfBirth,
      description: description ?? this.description,
      email: email ?? this.email,
      fullName: fullName ?? this.fullName,
      instituionIds: instituionIds ?? this.instituionIds,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      photoURL: photoURL ?? this.photoURL,
      sex: sex ?? this.sex,
      videoUrl: videoUrl ?? this.videoUrl,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'certifications': certifications?.toMap(),
      'contacts': contacts?.toMap(),
      'dateOfBirth': dateOfBirth?.millisecondsSinceEpoch,
      'description': description,
      'email': email,
      'fullName': fullName,
      'instituionIds': instituionIds,
      'phoneNumber': phoneNumber,
      'photoURL': photoURL?.toMap(),
      'sex': sex?.toMap(),
      'videoUrl': videoUrl,
    };
  }

  factory Specialist.fromMap(Map<String, dynamic> map) {
    return Specialist(
      id: map['id'],
      certifications: map['certifications'] != null
          ? PhotoUrl.fromMap(map['certifications'])
          : null,
      contacts:
          map['contacts'] != null ? Contacts.fromMap(map['contacts']) : null,
      dateOfBirth: map['dateOfBirth'] != null
          ? DateTime.tryParse(map['dateOfBirth'])
          : null,
      description: map['description'],
      email: map['email'],
      fullName: map['fullName'],
      instituionIds: map['instituionIds'] == null
          ? null
          : List<String>.from(map['instituionIds']),
      phoneNumber: map['phoneNumber'],
      photoURL:
          map['photoURL'] != null ? PhotoUrl.fromMap(map['photoURL']) : null,
      sex: map['sex'] != null ? SexEnum.fromMap(map['sex']) : null,
      videoUrl: map['videoUrl'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Specialist.fromJson(String source) =>
      Specialist.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Specialist(id: $id, certifications: $certifications, contacts: $contacts, dateOfBirth: $dateOfBirth, description: $description, email: $email, fullName: $fullName, instituionIds: $instituionIds, phoneNumber: $phoneNumber, photoURL: $photoURL, sex: $sex, videoUrl: $videoUrl)';
  }

  @override
  List<Object?> get props {
    return [
      id,
      certifications,
      contacts,
      dateOfBirth,
      description,
      email,
      fullName,
      instituionIds,
      phoneNumber,
      photoURL,
      sex,
      videoUrl,
    ];
  }
}

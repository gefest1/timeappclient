import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/components/pages/qfit_to_timeapp/qr_scan_page.dart';
import 'package:li/const.dart';
import 'package:li/main.dart';
import 'package:li/svgs/bigSvg/fast_main.dart';

class QrButton extends StatelessWidget {
  const QrButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: GestureDetector(
        onTap: () {
          routerDelegate.push(const QrScanPage());
        },
        child: Container(
          decoration: const BoxDecoration(
            color: Colors.white,
            shape: BoxShape.circle,
            boxShadow: bigBlur,
          ),
          height: 40,
          width: 40,
          alignment: Alignment.center,
          child: RepaintBoundary(
            child: SvgPicture.string(fast_button),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/atoms/bottom_nav_bar.dart';
import 'package:li/components/molecules/minute/all_clubs_search.dart';
import 'package:li/components/pages/qfit_to_timeapp/map_view/map_view.dart';
import 'package:li/data/models/allowed_institution_types.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/utils/color.dart';

class AllInstitutionsPage extends StatefulWidget {
  final String title;
  final num clubsCount;
  final AllowedInstitutionTypes type;

  const AllInstitutionsPage({
    required this.clubsCount,
    required this.title,
    required this.type,
    Key? key,
  }) : super(key: key);

  @override
  _AllInstitutionsPageState createState() => _AllInstitutionsPageState();
}

class _AllInstitutionsPageState extends State<AllInstitutionsPage>
    with SingleTickerProviderStateMixin {
  late final TabController tabController = TabController(
    vsync: this,
    length: 2,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        leading: IconButton(
          icon: SvgPicture.asset('assets/svg/icon/backArrow.svg'),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text(
          widget.title,
          style: TextStyle(
            color: ColorData.allMainBlack,
          ),
        ),
      ),
      body: Stack(
        children: [
          TabBarView(
            physics: NeverScrollableScrollPhysics(),
            controller: tabController,
            children: [
              AllClubsSearch(type: widget.type),
              MapView(
                type: widget.type,
              ),
            ],
          ),
          BottomNavBar(tabController: tabController),
        ],
      ),
    );
  }
}

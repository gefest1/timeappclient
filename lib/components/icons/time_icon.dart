import 'package:flutter/material.dart';

class TimeIconPainter extends CustomPainter {
  const TimeIconPainter({Listenable? repaint}) : super(repaint: repaint);
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.1344873, size.height * 0.8653484);
    path_0.cubicTo(
        size.width * 0.1335038,
        size.height * 0.8595258,
        size.width * 0.1306481,
        size.height * 0.8577032,
        size.width * 0.1285911,
        size.height * 0.8611774);
    path_0.cubicTo(
        size.width * 0.1191213,
        size.height * 0.8771548,
        size.width * 0.1072673,
        size.height * 0.8860000,
        size.width * 0.09503165,
        size.height * 0.8860000);
    path_0.cubicTo(
        size.width * 0.06802620,
        size.height * 0.8860000,
        size.width * 0.05352304,
        size.height * 0.8448000,
        size.width * 0.05352304,
        size.height * 0.7714129);
    path_0.lineTo(size.width * 0.05352304, size.height * 0.4115290);
    path_0.cubicTo(
        size.width * 0.05352304,
        size.height * 0.4062000,
        size.width * 0.05519658,
        size.height * 0.4018742,
        size.width * 0.05726127,
        size.height * 0.4018742);
    path_0.lineTo(size.width * 0.1258013, size.height * 0.4018742);
    path_0.cubicTo(
        size.width * 0.1278658,
        size.height * 0.4018742,
        size.width * 0.1295392,
        size.height * 0.3975516,
        size.width * 0.1295392,
        size.height * 0.3922194);
    path_0.lineTo(size.width * 0.1295392, size.height * 0.3098126);
    path_0.cubicTo(
        size.width * 0.1295392,
        size.height * 0.3044829,
        size.width * 0.1278658,
        size.height * 0.3001574,
        size.width * 0.1258013,
        size.height * 0.3001574);
    path_0.lineTo(size.width * 0.05726127, size.height * 0.3001574);
    path_0.cubicTo(
        size.width * 0.05519658,
        size.height * 0.3001574,
        size.width * 0.05352304,
        size.height * 0.2958348,
        size.width * 0.05352304,
        size.height * 0.2905019);
    path_0.lineTo(size.width * 0.05352304, size.height * 0.1604552);
    path_0.cubicTo(
        size.width * 0.05352304,
        size.height * 0.1551226,
        size.width * 0.05184949,
        size.height * 0.1508000,
        size.width * 0.04978506,
        size.height * 0.1508000);
    path_0.lineTo(size.width * 0.009251342, size.height * 0.1508000);
    path_0.cubicTo(
        size.width * 0.007186785,
        size.height * 0.1508000,
        size.width * 0.005513253,
        size.height * 0.1551226,
        size.width * 0.005513253,
        size.height * 0.1604552);
    path_0.lineTo(size.width * 0.005513253, size.height * 0.7765613);
    path_0.cubicTo(
        size.width * 0.005513253,
        size.height * 0.9143258,
        size.width * 0.03601987,
        size.height * 0.9902968,
        size.width * 0.08953051,
        size.height * 0.9902968);
    path_0.cubicTo(
        size.width * 0.1100044,
        size.height * 0.9902968,
        size.width * 0.1309304,
        size.height * 0.9762871,
        size.width * 0.1454063,
        size.height * 0.9471677);
    path_0.cubicTo(
        size.width * 0.1467013,
        size.height * 0.9445613,
        size.width * 0.1470949,
        size.height * 0.9400065,
        size.width * 0.1464392,
        size.height * 0.9361226);
    path_0.lineTo(size.width * 0.1344873, size.height * 0.8653484);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_0, paint_0_fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.1959253, size.height * 0.1688052);
    path_1.cubicTo(
        size.width * 0.2149291,
        size.height * 0.1688052,
        size.width * 0.2284329,
        size.height * 0.1327526,
        size.width * 0.2284329,
        size.height * 0.08640032);
    path_1.cubicTo(
        size.width * 0.2284329,
        size.height * 0.04262387,
        size.width * 0.2144291,
        size.height * 0.007859742,
        size.width * 0.1959253,
        size.height * 0.007859742);
    path_1.cubicTo(
        size.width * 0.1774228,
        size.height * 0.007859742,
        size.width * 0.1634203,
        size.height * 0.04391032,
        size.width * 0.1634203,
        size.height * 0.08897613);
    path_1.cubicTo(
        size.width * 0.1634203,
        size.height * 0.1340406,
        size.width * 0.1774228,
        size.height * 0.1688052,
        size.width * 0.1959253,
        size.height * 0.1688052);
    path_1.close();
    path_1.moveTo(size.width * 0.1719203, size.height * 0.9728935);
    path_1.cubicTo(
        size.width * 0.1719203,
        size.height * 0.9782290,
        size.width * 0.1735949,
        size.height * 0.9825516,
        size.width * 0.1756582,
        size.height * 0.9825516);
    path_1.lineTo(size.width * 0.2161924, size.height * 0.9825516);
    path_1.cubicTo(
        size.width * 0.2182570,
        size.height * 0.9825516,
        size.width * 0.2199304,
        size.height * 0.9782290,
        size.width * 0.2199304,
        size.height * 0.9728935);
    path_1.lineTo(size.width * 0.2199304, size.height * 0.3097939);
    path_1.cubicTo(
        size.width * 0.2199304,
        size.height * 0.3044606,
        size.width * 0.2182570,
        size.height * 0.3001384,
        size.width * 0.2161924,
        size.height * 0.3001384);
    path_1.lineTo(size.width * 0.1756582, size.height * 0.3001384);
    path_1.cubicTo(
        size.width * 0.1735949,
        size.height * 0.3001384,
        size.width * 0.1719203,
        size.height * 0.3044606,
        size.width * 0.1719203,
        size.height * 0.3097939);
    path_1.lineTo(size.width * 0.1719203, size.height * 0.9728935);
    path_1.close();

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_1, paint_1_fill);

    Path path_2 = Path();
    path_2.moveTo(size.width * 0.5987494, size.height * 0.2970797);
    path_2.cubicTo(
        size.width * 0.5563430,
        size.height * 0.2970797,
        size.width * 0.5210392,
        size.height * 0.3405129,
        size.width * 0.5003684,
        size.height * 0.4101484);
    path_2.cubicTo(
        size.width * 0.4986506,
        size.height * 0.4159419,
        size.width * 0.4951038,
        size.height * 0.4154710,
        size.width * 0.4936076,
        size.height * 0.4092806);
    path_2.cubicTo(
        size.width * 0.4753633,
        size.height * 0.3337613,
        size.width * 0.4429316,
        size.height * 0.2970797,
        size.width * 0.4047114,
        size.height * 0.2970797);
    path_2.cubicTo(
        size.width * 0.3694949,
        size.height * 0.2970797,
        size.width * 0.3400911,
        size.height * 0.3270065,
        size.width * 0.3205911,
        size.height * 0.3815968);
    path_2.cubicTo(
        size.width * 0.3181190,
        size.height * 0.3885194,
        size.width * 0.3131937,
        size.height * 0.3842419,
        size.width * 0.3131937,
        size.height * 0.3748194);
    path_2.lineTo(size.width * 0.3131937, size.height * 0.3131752);
    path_2.cubicTo(
        size.width * 0.3131937,
        size.height * 0.3078419,
        size.width * 0.3115190,
        size.height * 0.3035197,
        size.width * 0.3094544,
        size.height * 0.3035197);
    path_2.lineTo(size.width * 0.2709203, size.height * 0.3035197);
    path_2.cubicTo(
        size.width * 0.2688557,
        size.height * 0.3035197,
        size.width * 0.2671823,
        size.height * 0.3078419,
        size.width * 0.2671823,
        size.height * 0.3131752);
    path_2.lineTo(size.width * 0.2671823, size.height * 0.9762774);
    path_2.cubicTo(
        size.width * 0.2671823,
        size.height * 0.9816097,
        size.width * 0.2688557,
        size.height * 0.9859290,
        size.width * 0.2709203,
        size.height * 0.9859290);
    path_2.lineTo(size.width * 0.3114557, size.height * 0.9859290);
    path_2.cubicTo(
        size.width * 0.3135203,
        size.height * 0.9859290,
        size.width * 0.3151937,
        size.height * 0.9816097,
        size.width * 0.3151937,
        size.height * 0.9762774);
    path_2.lineTo(size.width * 0.3151937, size.height * 0.6344258);
    path_2.cubicTo(
        size.width * 0.3151937,
        size.height * 0.4863548,
        size.width * 0.3467013,
        size.height * 0.4078129,
        size.width * 0.3947089,
        size.height * 0.4078129);
    path_2.cubicTo(
        size.width * 0.4382177,
        size.height * 0.4078129,
        size.width * 0.4632228,
        size.height * 0.4734806,
        size.width * 0.4632228,
        size.height * 0.6073871);
    path_2.lineTo(size.width * 0.4632228, size.height * 0.9762774);
    path_2.cubicTo(
        size.width * 0.4632228,
        size.height * 0.9816097,
        size.width * 0.4648962,
        size.height * 0.9859290,
        size.width * 0.4669608,
        size.height * 0.9859290);
    path_2.lineTo(size.width * 0.5074962, size.height * 0.9859290);
    path_2.cubicTo(
        size.width * 0.5095608,
        size.height * 0.9859290,
        size.width * 0.5112342,
        size.height * 0.9816097,
        size.width * 0.5112342,
        size.height * 0.9762774);
    path_2.lineTo(size.width * 0.5112342, size.height * 0.6344258);
    path_2.cubicTo(
        size.width * 0.5112342,
        size.height * 0.4863548,
        size.width * 0.5427405,
        size.height * 0.4078129,
        size.width * 0.5907494,
        size.height * 0.4078129);
    path_2.cubicTo(
        size.width * 0.6342582,
        size.height * 0.4078129,
        size.width * 0.6592633,
        size.height * 0.4734806,
        size.width * 0.6592633,
        size.height * 0.6073871);
    path_2.lineTo(size.width * 0.6592633, size.height * 0.9762774);
    path_2.cubicTo(
        size.width * 0.6592633,
        size.height * 0.9816097,
        size.width * 0.6609367,
        size.height * 0.9859290,
        size.width * 0.6630013,
        size.height * 0.9859290);
    path_2.lineTo(size.width * 0.7035354, size.height * 0.9859290);
    path_2.cubicTo(
        size.width * 0.7055987,
        size.height * 0.9859290,
        size.width * 0.7072734,
        size.height * 0.9816097,
        size.width * 0.7072734,
        size.height * 0.9762774);
    path_2.lineTo(size.width * 0.7072734, size.height * 0.5932258);
    path_2.cubicTo(
        size.width * 0.7072734,
        size.height * 0.3923613,
        size.width * 0.6632646,
        size.height * 0.2970797,
        size.width * 0.5987494,
        size.height * 0.2970797);
    path_2.close();

    Paint paint_2_fill = Paint()..style = PaintingStyle.fill;
    paint_2_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_2, paint_2_fill);

    Path path_3 = Path();
    path_3.moveTo(size.width * 0.8697278, size.height * 0.8855000);
    path_3.cubicTo(
        size.width * 0.8269165,
        size.height * 0.8855000,
        size.width * 0.7947456,
        size.height * 0.8354645,
        size.width * 0.7803380,
        size.height * 0.7480161);
    path_3.cubicTo(
        size.width * 0.7794342,
        size.height * 0.7425290,
        size.width * 0.7807684,
        size.height * 0.7363968,
        size.width * 0.7830329,
        size.height * 0.7352613);
    path_3.lineTo(size.width * 0.9861658, size.height * 0.6333968);
    path_3.cubicTo(
        size.width * 0.9879494,
        size.height * 0.6325000,
        size.width * 0.9892291,
        size.height * 0.6283742,
        size.width * 0.9891595,
        size.height * 0.6236839);
    path_3.cubicTo(
        size.width * 0.9862291,
        size.height * 0.4249065,
        size.width * 0.9302633,
        size.height * 0.2970797,
        size.width * 0.8587253,
        size.height * 0.2970797);
    path_3.cubicTo(
        size.width * 0.7817076,
        size.height * 0.2970797,
        size.width * 0.7256975,
        size.height * 0.4425774,
        size.width * 0.7256975,
        size.height * 0.6447226);
    path_3.cubicTo(
        size.width * 0.7256975,
        size.height * 0.8481645,
        size.width * 0.7827089,
        size.height * 0.9936613,
        size.width * 0.8682278,
        size.height * 0.9936613);
    path_3.cubicTo(
        size.width * 0.9109684,
        size.height * 0.9936613,
        size.width * 0.9466342,
        size.height * 0.9572194,
        size.width * 0.9701873,
        size.height * 0.8890645);
    path_3.cubicTo(
        size.width * 0.9713962,
        size.height * 0.8855645,
        size.width * 0.9713924,
        size.height * 0.8803000,
        size.width * 0.9702203,
        size.height * 0.8767065);
    path_3.lineTo(size.width * 0.9489937, size.height * 0.8115387);
    path_3.cubicTo(
        size.width * 0.9475367,
        size.height * 0.8070710,
        size.width * 0.9448911,
        size.height * 0.8069516,
        size.width * 0.9433013,
        size.height * 0.8111065);
    path_3.cubicTo(
        size.width * 0.9241532,
        size.height * 0.8610710,
        size.width * 0.8992000,
        size.height * 0.8855000,
        size.width * 0.8697278,
        size.height * 0.8855000);
    path_3.close();
    path_3.moveTo(size.width * 0.8587253, size.height * 0.4013742);
    path_3.cubicTo(
        size.width * 0.9002975,
        size.height * 0.4013742,
        size.width * 0.9311228,
        size.height * 0.4663387,
        size.width * 0.9408114,
        size.height * 0.5590516);
    path_3.cubicTo(
        size.width * 0.9413646,
        size.height * 0.5643258,
        size.width * 0.9400152,
        size.height * 0.5695968,
        size.width * 0.9379380,
        size.height * 0.5706097);
    path_3.lineTo(size.width * 0.7766367, size.height * 0.6490097);
    path_3.cubicTo(
        size.width * 0.7743354,
        size.height * 0.6501290,
        size.width * 0.7722063,
        size.height * 0.6455677,
        size.width * 0.7722063,
        size.height * 0.6395161);
    path_3.lineTo(size.width * 0.7722063, size.height * 0.6382903);
    path_3.cubicTo(
        size.width * 0.7722063,
        size.height * 0.4953677,
        size.width * 0.8082139,
        size.height * 0.4013742,
        size.width * 0.8587253,
        size.height * 0.4013742);
    path_3.close();

    Paint paint_3_fill = Paint()..style = PaintingStyle.fill;
    paint_3_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_3, paint_3_fill);

    Path path_4 = Path();
    path_4.moveTo(size.width * 0.1959253, size.height * 0.1688055);
    path_4.cubicTo(
        size.width * 0.2149291,
        size.height * 0.1688055,
        size.width * 0.2284329,
        size.height * 0.1327529,
        size.width * 0.2284329,
        size.height * 0.08640032);
    path_4.cubicTo(
        size.width * 0.2284329,
        size.height * 0.04262387,
        size.width * 0.2144291,
        size.height * 0.007859742,
        size.width * 0.1959253,
        size.height * 0.007859742);
    path_4.cubicTo(
        size.width * 0.1774228,
        size.height * 0.007859742,
        size.width * 0.1634203,
        size.height * 0.04391032,
        size.width * 0.1634203,
        size.height * 0.08897613);
    path_4.cubicTo(
        size.width * 0.1634203,
        size.height * 0.1340406,
        size.width * 0.1774228,
        size.height * 0.1688055,
        size.width * 0.1959253,
        size.height * 0.1688055);
    path_4.close();

    Paint paint_4_fill = Paint()..style = PaintingStyle.fill;
    paint_4_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_4, paint_4_fill);

    Path path_5 = Path();
    path_5.moveTo(size.width * 0.1959127, size.height * 0.1609455);
    path_5.cubicTo(
        size.width * 0.2044911,
        size.height * 0.1609455,
        size.width * 0.2118089,
        size.height * 0.1529616,
        size.width * 0.2169911,
        size.height * 0.1398248);
    path_5.cubicTo(
        size.width * 0.2222658,
        size.height * 0.1264552,
        size.width * 0.2253671,
        size.height * 0.1076506,
        size.width * 0.2253671,
        size.height * 0.08640387);
    path_5.cubicTo(
        size.width * 0.2253671,
        size.height * 0.06664161,
        size.width * 0.2221924,
        size.height * 0.04892871,
        size.width * 0.2169266,
        size.height * 0.03620645);
    path_5.cubicTo(
        size.width * 0.2116722,
        size.height * 0.02351129,
        size.width * 0.2043139,
        size.height * 0.01572571,
        size.width * 0.1959127,
        size.height * 0.01572571);
    path_5.cubicTo(
        size.width * 0.1875405,
        size.height * 0.01572571,
        size.width * 0.1802013,
        size.height * 0.02377719,
        size.width * 0.1749506,
        size.height * 0.03687645);
    path_5.cubicTo(
        size.width * 0.1696557,
        size.height * 0.05009194,
        size.width * 0.1664608,
        size.height * 0.06849742,
        size.width * 0.1664608,
        size.height * 0.08897935);
    path_5.cubicTo(
        size.width * 0.1664608,
        size.height * 0.1093365,
        size.width * 0.1696177,
        size.height * 0.1273323,
        size.width * 0.1748582,
        size.height * 0.1402010);
    path_5.cubicTo(
        size.width * 0.1801165,
        size.height * 0.1531129,
        size.width * 0.1874911,
        size.height * 0.1609455,
        size.width * 0.1959127,
        size.height * 0.1609455);
    path_5.close();
    path_5.moveTo(size.width * 0.2212620, size.height * 0.1510055);
    path_5.cubicTo(
        size.width * 0.2149633,
        size.height * 0.1669700,
        size.width * 0.2061582,
        size.height * 0.1766713,
        size.width * 0.1959127,
        size.height * 0.1766713);
    path_5.cubicTo(
        size.width * 0.1858709,
        size.height * 0.1766713,
        size.width * 0.1770203,
        size.height * 0.1671900,
        size.width * 0.1706582,
        size.height * 0.1515639);
    path_5.cubicTo(
        size.width * 0.1642278,
        size.height * 0.1357723,
        size.width * 0.1603544,
        size.height * 0.1137787,
        size.width * 0.1603544,
        size.height * 0.08897935);
    path_5.cubicTo(
        size.width * 0.1603544,
        size.height * 0.06412548,
        size.width * 0.1642468,
        size.height * 0.04174806,
        size.width * 0.1707038,
        size.height * 0.02563574);
    path_5.cubicTo(size.width * 0.1770671, size.height * 0.009759452,
        size.width * 0.1858962, 0, size.width * 0.1959127, 0);
    path_5.cubicTo(
        size.width * 0.2059190,
        0,
        size.width * 0.2147430,
        size.height * 0.009419581,
        size.width * 0.2211013,
        size.height * 0.02478032);
    path_5.cubicTo(
        size.width * 0.2275722,
        size.height * 0.04041613,
        size.width * 0.2314759,
        size.height * 0.06216161,
        size.width * 0.2314759,
        size.height * 0.08640387);
    path_5.cubicTo(
        size.width * 0.2314759,
        size.height * 0.1119545,
        size.width * 0.2276924,
        size.height * 0.1347032,
        size.width * 0.2212620,
        size.height * 0.1510055);
    path_5.close();

    Paint paint_5_fill = Paint()..style = PaintingStyle.fill;
    paint_5_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_5, paint_5_fill);

    Path path_6 = Path();
    path_6.moveTo(size.width * 0.1959253, size.height * 0.1688055);
    path_6.cubicTo(
        size.width * 0.2149291,
        size.height * 0.1688055,
        size.width * 0.2284329,
        size.height * 0.1327529,
        size.width * 0.2284329,
        size.height * 0.08640032);
    path_6.cubicTo(
        size.width * 0.2284329,
        size.height * 0.04262387,
        size.width * 0.2144291,
        size.height * 0.007859742,
        size.width * 0.1959253,
        size.height * 0.007859742);
    path_6.cubicTo(
        size.width * 0.1774228,
        size.height * 0.007859742,
        size.width * 0.1634203,
        size.height * 0.04391032,
        size.width * 0.1634203,
        size.height * 0.08897613);
    path_6.cubicTo(
        size.width * 0.1634203,
        size.height * 0.1340406,
        size.width * 0.1774228,
        size.height * 0.1688055,
        size.width * 0.1959253,
        size.height * 0.1688055);
    path_6.close();

    Paint paint_6_fill = Paint()..style = PaintingStyle.fill;
    paint_6_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_6, paint_6_fill);

    Path path_7 = Path();
    path_7.moveTo(size.width * 0.1959127, size.height * 0.1609458);
    path_7.cubicTo(
        size.width * 0.2044911,
        size.height * 0.1609458,
        size.width * 0.2118089,
        size.height * 0.1529616,
        size.width * 0.2169911,
        size.height * 0.1398252);
    path_7.cubicTo(
        size.width * 0.2222658,
        size.height * 0.1264548,
        size.width * 0.2253671,
        size.height * 0.1076510,
        size.width * 0.2253671,
        size.height * 0.08640387);
    path_7.cubicTo(
        size.width * 0.2253671,
        size.height * 0.06664161,
        size.width * 0.2221924,
        size.height * 0.04892871,
        size.width * 0.2169266,
        size.height * 0.03620645);
    path_7.cubicTo(
        size.width * 0.2116722,
        size.height * 0.02351132,
        size.width * 0.2043139,
        size.height * 0.01572574,
        size.width * 0.1959127,
        size.height * 0.01572574);
    path_7.cubicTo(
        size.width * 0.1875405,
        size.height * 0.01572574,
        size.width * 0.1802013,
        size.height * 0.02377716,
        size.width * 0.1749519,
        size.height * 0.03687645);
    path_7.cubicTo(
        size.width * 0.1696557,
        size.height * 0.05009194,
        size.width * 0.1664608,
        size.height * 0.06849710,
        size.width * 0.1664608,
        size.height * 0.08897935);
    path_7.cubicTo(
        size.width * 0.1664608,
        size.height * 0.1093361,
        size.width * 0.1696190,
        size.height * 0.1273326,
        size.width * 0.1748582,
        size.height * 0.1402013);
    path_7.cubicTo(
        size.width * 0.1801165,
        size.height * 0.1531132,
        size.width * 0.1874911,
        size.height * 0.1609458,
        size.width * 0.1959127,
        size.height * 0.1609458);
    path_7.close();
    path_7.moveTo(size.width * 0.2212620, size.height * 0.1510058);
    path_7.cubicTo(
        size.width * 0.2149646,
        size.height * 0.1669703,
        size.width * 0.2061582,
        size.height * 0.1766713,
        size.width * 0.1959127,
        size.height * 0.1766713);
    path_7.cubicTo(
        size.width * 0.1858722,
        size.height * 0.1766713,
        size.width * 0.1770215,
        size.height * 0.1671903,
        size.width * 0.1706582,
        size.height * 0.1515639);
    path_7.cubicTo(
        size.width * 0.1642278,
        size.height * 0.1357726,
        size.width * 0.1603544,
        size.height * 0.1137787,
        size.width * 0.1603544,
        size.height * 0.08897935);
    path_7.cubicTo(
        size.width * 0.1603544,
        size.height * 0.06412581,
        size.width * 0.1642468,
        size.height * 0.04174806,
        size.width * 0.1707038,
        size.height * 0.02563577);
    path_7.cubicTo(size.width * 0.1770671, size.height * 0.009759452,
        size.width * 0.1858962, 0, size.width * 0.1959127, 0);
    path_7.cubicTo(
        size.width * 0.2059190,
        0,
        size.width * 0.2147430,
        size.height * 0.009419613,
        size.width * 0.2211013,
        size.height * 0.02478035);
    path_7.cubicTo(
        size.width * 0.2275722,
        size.height * 0.04041613,
        size.width * 0.2314747,
        size.height * 0.06216097,
        size.width * 0.2314747,
        size.height * 0.08640387);
    path_7.cubicTo(
        size.width * 0.2314747,
        size.height * 0.1119548,
        size.width * 0.2276924,
        size.height * 0.1347035,
        size.width * 0.2212620,
        size.height * 0.1510058);
    path_7.close();

    Paint paint_7_fill = Paint()..style = PaintingStyle.fill;
    paint_7_fill.color = Color(0xff00D72C).withOpacity(1.0);
    canvas.drawPath(path_7, paint_7_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}

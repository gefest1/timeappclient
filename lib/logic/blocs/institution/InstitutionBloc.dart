import "package:http/http.dart";
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'package:li/logic/blocs/institution/InstitutionEvent.dart';
import 'package:li/logic/blocs/institution/InstitutionState.dart';
import 'package:li/network/graphql_provider.dart';

part './InstitutionReaction/editCertificates.dart';

class InstitutionBloc extends Bloc<InstitutionEvent, InstitutionState> {
  final GraphqlProvider graphqlProvider;

  InstitutionBloc(this.graphqlProvider) : super(InitInstitutionState()) {
    on<EditDataCertificatgeInstitutionEvent>(editCertificate);
  }
}

import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/data/models/session_minute.dart';

mixin StartSession {
  static Future<SessionMinute> startSession({
    required GraphQLClient client,
    required String institutionId,
    required String creditCardId,
    required String serviceId,
    required bool useBonus,
  }) async {
    final _startSession = await client.mutate(
      MutationOptions(
        document: gql('''
          mutation(
            \$creditCardId: String!
    				\$institutionId: String!
            \$serviceId: String!
            \$useBonus: Boolean
          ) {
            startSession(
              creditCardId: \$creditCardId
              institutionId : \$institutionId
              serviceId: \$serviceId
              useBonus: \$useBonus
            ){
              _id
              client {
                _id
              }
    					endTime
    					price
              institutionId
              institution {            
                type
                _id
                iconPath
                location {
                  address
                  latitude
                  longitude
                }
                name
                rating
                description
              }
    					service {
                _id
    						endTime
    						name
    						price
    						startTime
                bufferTimeMinutes
              }
    					startTime
              status
              usedNumOfBonus
              bonusEarnt
            }
        }'''),
        variables: {
          "creditCardId": creditCardId,
          "institutionId": institutionId,
          "serviceId": serviceId,
          "useBonus": useBonus,
        },
      ),
    );
    if (_startSession.hasException) throw _startSession.exception!;
    return SessionMinute.fromMap(_startSession.data!['startSession'])!;
  }
}

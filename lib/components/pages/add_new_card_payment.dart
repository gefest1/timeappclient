import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/data/models/client_model.dart';
import 'package:li/logic/blocs/Payment/paymentBloc.dart';
import 'package:li/logic/blocs/Payment/paymentEvent.dart';
import 'package:li/logic/blocs/Payment/paymentState.dart';
import 'package:li/main.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:time_app_components/components/molecules/CustomAppBar.dart';
import 'package:time_app_components/components/molecules/CustomTextField.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

class AddNewCardPayment extends StatefulWidget {
  const AddNewCardPayment({Key? key}) : super(key: key);

  @override
  _AddNewCardPaymentState createState() => _AddNewCardPaymentState();
}

class _AddNewCardPaymentState extends State<AddNewCardPayment> {
  late final TextEditingController cardNumController = TextEditingController();

  late final TextEditingController nameController = TextEditingController()
    ..addListener(validate);
  late final TextEditingController cvvController = TextEditingController()
    ..addListener(validate);
  late final TextEditingController expireController = TextEditingController()
    ..addListener(validate);
  late final PaymentBloc paymentBloc =
      BlocProvider.of<PaymentBloc>(context, listen: false);
  late final StreamSubscription<PaymentState> subscription;

  StreamController<bool> _streamValid = StreamController<bool>();

  @override
  void initState() {
    super.initState();
    subscription = paymentBloc.stream.listen((event) {
      if (event is PaymentLoadingSuccessPrepare) {
        Navigator.pop(context);
      }
    });
  }

  @override
  void dispose() {
    subscription.cancel();
    nameController.dispose();
    cvvController.dispose();
    expireController.dispose();
    _streamValid.close();
    super.dispose();
  }

  validate() {
    bool _isNewValid = cardNumController.text.length == 19 &&
        cvvController.text.length == 3 &&
        expireController.text.length == 5;

    _streamValid.add(_isNewValid);
  }

  @override
  Widget build(BuildContext context) {
    final _media = MediaQuery.of(context);
    return GestureDetector(onTap: () {
      final _f = FocusScope.of(context);
      if (_f.hasFocus || _f.hasPrimaryFocus) {
        _f.unfocus();
      }
    }, child: BlocBuilder<PaymentBloc, PaymentState>(
      builder: (_, state) {
        if (state is PaymentLoadingInProgress) {
          return Material(
            color: Colors.white,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
        return Scaffold(
          backgroundColor: Colors.white,
          appBar: CustomAppBar(
            leading: IconButton(
              icon: SvgPicture.asset('assets/svg/icon/backArrow.svg'),
              onPressed: () => Navigator.pop(context),
            ),
            title: Column(
              children: [
                Text(
                  'Банковская карта',
                  style: TextStyle(
                    color: ColorData.allMainBlack,
                    fontSize: 18,
                  ),
                ),
                Text(
                  'Новый способ оплаты',
                  style: TextStyle(
                    color: ColorData.allMainGray,
                    fontSize: P4TextStyle.fontSize,
                    fontWeight: P4TextStyle.fontWeight,
                    height: P4TextStyle.height,
                  ),
                ),
              ],
            ),
          ),
          body: Stack(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: CustomScrollView(
                  slivers: [
                    SliverToBoxAdapter(
                      child: const SizedBox(
                        height: 40,
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: Text(
                        'Добавление банковской карты',
                        style: TextStyle(
                          fontSize: H2TextStyle.fontSize,
                          fontWeight: H2TextStyle.fontWeight,
                          height: H2TextStyle.height,
                        ),
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: const SizedBox(
                        height: 10,
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: Text(
                        'Мы спишем с вашей карты 10 тенге и сразу его вернем, чтобы подтвердить, что вы являетесь владельцем карты.',
                        style: TextStyle(
                          fontSize: P3TextStyle.fontSize,
                          fontWeight: P3TextStyle.fontWeight,
                          height: P3TextStyle.height,
                        ),
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: const SizedBox(
                        height: 40,
                      ),
                    ),
                    SliverToBoxAdapter(
                      child: Column(
                        children: [
                          CustomTextField(
                            controller: cardNumController,
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              MaskTextInputFormatter(
                                mask: "#### #### #### ####",
                                filter: {
                                  "#": RegExp(r'[0-9]'),
                                },
                              ),
                            ],
                            label: 'Номер карты',
                            labelColor: ColorData.allMainActivegray,
                            prefixIcon: Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: SvgPicture.asset(
                                'assets/svg/icon/Card.svg',
                                color: ColorData.allMainActivegray,
                              ),
                            ),
                          ),
                          const SizedBox(height: 10),
                          CustomTextField(
                            controller: nameController,
                            keyboardType: TextInputType.name,
                            label: 'Имя и Фамилия',
                            labelColor: ColorData.allMainActivegray,
                            inputFormatters: [
                              UpperCaseTextFormatter(),
                            ],
                          ),
                          const SizedBox(height: 10),
                          Row(
                            children: [
                              Flexible(
                                // flex: 13,
                                child: CustomTextField(
                                  controller: expireController,
                                  keyboardType: TextInputType.number,
                                  label: 'ММ/ГГ',
                                  inputFormatters: [
                                    MaskTextInputFormatter(
                                      mask: "##/##",
                                    ),
                                  ],
                                  labelColor: ColorData.allMainActivegray,
                                  prefixIcon: Padding(
                                    padding: const EdgeInsets.only(left: 20.0),
                                    child: SvgPicture.asset(
                                        'assets/svg/icon/ClockIcon.svg'),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                // flex: 8,
                                child: CustomTextField(
                                  controller: cvvController,
                                  keyboardType: TextInputType.number,
                                  label: 'CVV',
                                  inputFormatters: [
                                    MaskTextInputFormatter(
                                      mask: "###",
                                    ),
                                  ],
                                  labelColor: ColorData.allMainActivegray,
                                  prefixIcon: Padding(
                                    padding: const EdgeInsets.only(left: 20.0),
                                    child: SvgPicture.asset(
                                        'assets/svg/icon/CVV.svg'),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).viewInsets.bottom,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              StreamBuilder<bool>(
                initialData: false,
                stream: _streamValid.stream,
                builder: (ctx, _valid) {
                  log(_valid.toString(), name: "VALID");
                  if (!(_valid.data ?? false)) return SizedBox();
                  return Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: DecoratedBox(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: [
                          const BoxShadow(
                            offset: Offset(0, -20),
                            blurRadius: 40,
                            color: Color(0x14000000),
                          )
                        ],
                        color: Colors.white,
                      ),
                      child: Padding(
                        padding: EdgeInsets.only(bottom: _media.padding.bottom),
                        child: Container(
                          height: 100,
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Center(
                            child: SizedBox(
                              height: 60,
                              width: double.infinity,
                              child: ElevatedButton(
                                onPressed: () {
                                  final _client = Client.fromJson(
                                    sharedPreferences.getString('client')!,
                                  );
                                  if (_client.id != null)
                                    paymentBloc.add(
                                      AuthPaymentRequested(
                                        amount: 10,
                                        cardHolderName: nameController.text,
                                        cardNumber: cardNumController.text
                                            .replaceAll('-', '')
                                            .replaceAll(' ', ''),
                                        cvcCode: cvvController.text,
                                        expireDate: expireController.text,
                                        accountId: _client.id!,
                                      ),
                                    );
                                  _streamValid.close();
                                  _streamValid = StreamController<bool>();
                                  FirebaseAnalytics().logEvent(
                                      name: 'PaymentInfoAdded',
                                      parameters: <String, dynamic>{
                                        "ClientID": _client.id,
                                        "ClientName": _client.fullName,
                                      });
                                },
                                style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(
                                    ColorData.clientsButtonColorDefault,
                                  ),
                                  shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(16),
                                    ),
                                  ),
                                  elevation: MaterialStateProperty.all(0),
                                ),
                                child: Text(
                                  'Добавить карту',
                                  style: TextStyle(
                                    color: Color(0xffFCFCFC),
                                    fontSize: 18,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
        );
      },
    ));
  }
}

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toUpperCase(),
      selection: newValue.selection,
    );
  }
}

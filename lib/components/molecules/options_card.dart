import 'package:flutter/material.dart';
import 'package:li/components/atoms/options_little_card.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class OptionsCard extends StatelessWidget {
  const OptionsCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Text(
              'Дополнительно',
              textAlign: TextAlign.left,
              style: TextStyle(
                fontSize: H2TextStyle.fontSize,
                fontWeight: H2TextStyle.fontWeight,
                height: H2TextStyle.height,
                color: ColorData.allMainBlack,
              ),
            ),
          ),
          SizedBox(
            height: 13,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: OptionsLittleCard(
                    title: 'Парковка', svg: 'assets/svg/icon/car.svg'),
              ),
              Expanded(
                child: OptionsLittleCard(
                    title: 'Парковка', svg: 'assets/svg/icon/car.svg'),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                child: OptionsLittleCard(
                    title: 'Парковка', svg: 'assets/svg/icon/car.svg'),
              ),
              Expanded(
                child: OptionsLittleCard(
                    title: 'Парковка', svg: 'assets/svg/icon/car.svg'),
              ),
            ],
          )
        ],
      ),
    );
  }
}

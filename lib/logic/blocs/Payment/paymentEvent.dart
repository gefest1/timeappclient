import 'package:equatable/equatable.dart';

abstract class PaymentEvent extends Equatable {
  const PaymentEvent();

  @override
  List<Object?> get props => [];
}

class MakePayment extends PaymentEvent {
  final int amount;
  final String token;
  final String id;

  const MakePayment(
    this.amount,
    this.token,
    this.id,
  ) : super();
}

class AuthPaymentRequested extends PaymentEvent {
  final int amount;
  final String cardNumber;
  final String expireDate;
  final String cvcCode;
  final String cardHolderName;

  final String accountId;
  final String? email;

  AuthPaymentRequested({
    required this.accountId,
    required this.amount,
    required this.cardNumber,
    required this.cvcCode,
    required this.expireDate,
    required this.cardHolderName,
    this.email,
  }) : super();
}

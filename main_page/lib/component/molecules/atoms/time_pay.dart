// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';
import 'package:time_app_components/utils/color.dart';

class TimePayCustomPainter extends CustomPainter {
  final bool active;

  const TimePayCustomPainter({
    this.active = false,
    Listenable? repaint,
  }) : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    final activeColor1 = active ? const Color(0xff25BA00) : ColorData.allMainGray;
    final activeColor2 = active ? const Color(0xff00D72C) : ColorData.allMainGray;

    Path path_0 = Path();
    path_0.moveTo(size.width * 0.2626302, size.height * 0.1292836);
    path_0.cubicTo(
        size.width * 0.2626302,
        size.height * 0.1292836,
        size.width * 0.2522050,
        size.height * 0.09932188,
        size.width * 0.2841129,
        size.height * 0.07199094);
    path_0.lineTo(size.width * 0.2902734, size.height * 0.07696547);
    path_0.lineTo(size.width * 0.2780050, size.height * 0.1252813);
    path_0.lineTo(size.width * 0.2626302, size.height * 0.1292836);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Colors.black;
    canvas.drawPath(path_0, paint_0_fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.2626302, size.height * 0.1292836);
    path_1.cubicTo(
        size.width * 0.2626302,
        size.height * 0.1292836,
        size.width * 0.2522050,
        size.height * 0.09932188,
        size.width * 0.2841129,
        size.height * 0.07199094);
    path_1.lineTo(size.width * 0.2902734, size.height * 0.07696547);
    path_1.lineTo(size.width * 0.2780050, size.height * 0.1252813);
    path_1.lineTo(size.width * 0.2626302, size.height * 0.1292836);
    path_1.close();

    Paint paint_1_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004195209;
    paint_1_stroke.color = Colors.black;
    canvas.drawPath(path_1, paint_1_stroke);

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = Colors.black;
    canvas.drawPath(path_1, paint_1_fill);

    Path path_2 = Path();
    path_2.moveTo(size.width * 0.4129353, size.height * 0.1093211);
    path_2.lineTo(size.width * 0.4242554, size.height * 0.08765078);
    path_2.cubicTo(
        size.width * 0.4242554,
        size.height * 0.08765078,
        size.width * 0.4641144,
        size.height * 0.09411172,
        size.width * 0.4350496,
        size.height * 0.03092953);
    path_2.lineTo(size.width * 0.4005619, size.height * 0.03910609);
    path_2.lineTo(size.width * 0.4005619, size.height * 0.07009672);
    path_2.lineTo(size.width * 0.3847655, size.height * 0.09365391);
    path_2.lineTo(size.width * 0.4129353, size.height * 0.1093211);
    path_2.close();

    Paint paint_2_fill = Paint()..style = PaintingStyle.fill;
    paint_2_fill.color = Colors.black;
    canvas.drawPath(path_2, paint_2_fill);

    Path path_3 = Path();
    path_3.moveTo(size.width * 0.4129353, size.height * 0.1093211);
    path_3.lineTo(size.width * 0.4242554, size.height * 0.08765078);
    path_3.cubicTo(
        size.width * 0.4242554,
        size.height * 0.08765078,
        size.width * 0.4641144,
        size.height * 0.09411172,
        size.width * 0.4350496,
        size.height * 0.03092953);
    path_3.lineTo(size.width * 0.4005619, size.height * 0.03910609);
    path_3.lineTo(size.width * 0.4005619, size.height * 0.07009672);
    path_3.lineTo(size.width * 0.3847655, size.height * 0.09365391);
    path_3.lineTo(size.width * 0.4129353, size.height * 0.1093211);
    path_3.close();

    Paint paint_3_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004195209;
    paint_3_stroke.color = Colors.black;
    canvas.drawPath(path_3, paint_3_stroke);

    Paint paint_3_fill = Paint()..style = PaintingStyle.fill;
    paint_3_fill.color = Colors.white;
    canvas.drawPath(path_3, paint_3_fill);

    Path path_4 = Path();
    path_4.moveTo(size.width * 0.4353518, size.height * 0.02155625);
    path_4.cubicTo(
        size.width * 0.4353518,
        size.height * 0.02155625,
        size.width * 0.4373525,
        size.height * 0.01721070,
        size.width * 0.4344568,
        size.height * 0.01235055);
    path_4.cubicTo(
        size.width * 0.4344568,
        size.height * 0.01235055,
        size.width * 0.4447770,
        size.height * 0.007661891,
        size.width * 0.4479885,
        0);
    path_4.cubicTo(
        size.width * 0.4479885,
        0,
        size.width * 0.4553072,
        size.height * 0.01555250,
        size.width * 0.4485676,
        size.height * 0.02710250);
    path_4.cubicTo(
        size.width * 0.4485676,
        size.height * 0.02710250,
        size.width * 0.4538331,
        size.height * 0.02298570,
        size.width * 0.4567820,
        size.height * 0.01109258);
    path_4.lineTo(size.width * 0.4586777, size.height * 0.01275078);
    path_4.cubicTo(
        size.width * 0.4586777,
        size.height * 0.01275078,
        size.width * 0.4534647,
        size.height * 0.03848102,
        size.width * 0.4313504,
        size.height * 0.03316344);
    path_4.cubicTo(
        size.width * 0.4313504,
        size.height * 0.03316344,
        size.width * 0.4222935,
        size.height * 0.03699438,
        size.width * 0.4201353,
        size.height * 0.05025977);
    path_4.cubicTo(
        size.width * 0.4201353,
        size.height * 0.05025977,
        size.width * 0.4099201,
        size.height * 0.04362703,
        size.width * 0.4088669,
        size.height * 0.05414789);
    path_4.cubicTo(
        size.width * 0.4078144,
        size.height * 0.06466867,
        size.width * 0.4131849,
        size.height * 0.06535477,
        size.width * 0.4131849,
        size.height * 0.06535477);
    path_4.cubicTo(
        size.width * 0.4131849,
        size.height * 0.06535477,
        size.width * 0.3904388,
        size.height * 0.1012625,
        size.width * 0.3444719,
        size.height * 0.07221617);
    path_4.cubicTo(
        size.width * 0.3444719,
        size.height * 0.07221617,
        size.width * 0.3286237,
        size.height * 0.06060898,
        size.width * 0.3188827,
        size.height * 0.06249586);
    path_4.cubicTo(
        size.width * 0.3188827,
        size.height * 0.06249586,
        size.width * 0.3048770,
        size.height * 0.06632680,
        size.width * 0.2979791,
        size.height * 0.07307391);
    path_4.cubicTo(
        size.width * 0.2979791,
        size.height * 0.07307391,
        size.width * 0.2790763,
        size.height * 0.08473828,
        size.width * 0.2663871,
        size.height * 0.07924922);
    path_4.lineTo(size.width * 0.2620698, size.height * 0.07341695);
    path_4.cubicTo(
        size.width * 0.2620698,
        size.height * 0.07341695,
        size.width * 0.2769180,
        size.height * 0.07095828,
        size.width * 0.2819201,
        size.height * 0.06798500);
    path_4.lineTo(size.width * 0.2566986, size.height * 0.06729883);
    path_4.lineTo(size.width * 0.2470633, size.height * 0.05237531);
    path_4.cubicTo(
        size.width * 0.2470633,
        size.height * 0.05237531,
        size.width * 0.2788129,
        size.height * 0.03722305,
        size.width * 0.3220417,
        size.height * 0.05540578);
    path_4.cubicTo(
        size.width * 0.3220417,
        size.height * 0.05540578,
        size.width * 0.3288338,
        size.height * 0.06003719,
        size.width * 0.3352050,
        size.height * 0.05317586);
    path_4.cubicTo(
        size.width * 0.3352050,
        size.height * 0.05317586,
        size.width * 0.3492108,
        size.height * 0.03316344,
        size.width * 0.3701669,
        size.height * 0.03579359);
    path_4.cubicTo(
        size.width * 0.3701669,
        size.height * 0.03579359,
        size.width * 0.3785914,
        size.height * 0.03779484,
        size.width * 0.3891223,
        size.height * 0.02658789);
    path_4.cubicTo(
        size.width * 0.3890698,
        size.height * 0.02664508,
        size.width * 0.4135532,
        size.height * 0.001372281,
        size.width * 0.4353518,
        size.height * 0.02155625);
    path_4.close();

    Paint paint_4_fill = Paint()..style = PaintingStyle.fill;
    paint_4_fill.color = Colors.black;
    canvas.drawPath(path_4, paint_4_fill);

    Path path_5 = Path();
    path_5.moveTo(size.width * 0.3589532, size.height * 0.4105852);
    path_5.cubicTo(
        size.width * 0.3589532,
        size.height * 0.4105852,
        size.width * 0.3901770,
        size.height * 0.4310555,
        size.width * 0.4251914,
        size.height * 0.4240227);
    path_5.lineTo(size.width * 0.4219799, size.height * 0.4053250);
    path_5.lineTo(size.width * 0.3772770, size.height * 0.3977203);
    path_5.lineTo(size.width * 0.3589532, size.height * 0.4105852);
    path_5.close();

    Paint paint_5_fill = Paint()..style = PaintingStyle.fill;
    paint_5_fill.color = Colors.black;
    canvas.drawPath(path_5, paint_5_fill);

    Path path_6 = Path();
    path_6.moveTo(size.width * 0.3589532, size.height * 0.4105852);
    path_6.cubicTo(
        size.width * 0.3589532,
        size.height * 0.4105852,
        size.width * 0.3901770,
        size.height * 0.4310555,
        size.width * 0.4251914,
        size.height * 0.4240227);
    path_6.lineTo(size.width * 0.4219799, size.height * 0.4053250);
    path_6.lineTo(size.width * 0.3772770, size.height * 0.3977203);
    path_6.lineTo(size.width * 0.3589532, size.height * 0.4105852);
    path_6.close();

    Paint paint_6_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004195209;
    paint_6_stroke.color = Colors.black;
    canvas.drawPath(path_6, paint_6_stroke);

    Paint paint_6_fill = Paint()..style = PaintingStyle.fill;
    paint_6_fill.color = Colors.black;
    canvas.drawPath(path_6, paint_6_fill);

    Path path_7 = Path();
    path_7.moveTo(size.width * 0.1100475, size.height * 0.3415836);
    path_7.cubicTo(
        size.width * 0.1100475,
        size.height * 0.3415836,
        size.width * 0.09498849,
        size.height * 0.3766906,
        size.width * 0.1143647,
        size.height * 0.4351844);
    path_7.lineTo(size.width * 0.1375324, size.height * 0.4141422);
    path_7.lineTo(size.width * 0.1344784, size.height * 0.3547914);
    path_7.lineTo(size.width * 0.1100475, size.height * 0.3415836);
    path_7.close();

    Paint paint_7_fill = Paint()..style = PaintingStyle.fill;
    paint_7_fill.color = Colors.black;
    canvas.drawPath(path_7, paint_7_fill);

    Path path_8 = Path();
    path_8.moveTo(size.width * 0.1100475, size.height * 0.3415836);
    path_8.cubicTo(
        size.width * 0.1100475,
        size.height * 0.3415836,
        size.width * 0.09498849,
        size.height * 0.3766906,
        size.width * 0.1143647,
        size.height * 0.4351844);
    path_8.lineTo(size.width * 0.1375324, size.height * 0.4141422);
    path_8.lineTo(size.width * 0.1344784, size.height * 0.3547914);
    path_8.lineTo(size.width * 0.1100475, size.height * 0.3415836);
    path_8.close();

    Paint paint_8_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004195209;
    paint_8_stroke.color = Colors.black;
    canvas.drawPath(path_8, paint_8_stroke);

    Paint paint_8_fill = Paint()..style = PaintingStyle.fill;
    paint_8_fill.color = Colors.black;
    canvas.drawPath(path_8, paint_8_fill);

    Path path_9 = Path();
    path_9.moveTo(size.width * 0.4284957, size.height * 0.3916648);
    path_9.lineTo(size.width * 0.4188072, size.height * 0.4239703);
    path_9.lineTo(size.width * 0.4120676, size.height * 0.4463273);
    path_9.lineTo(size.width * 0.3697871, size.height * 0.4460414);
    path_9.lineTo(size.width * 0.3757367, size.height * 0.4180234);
    path_9.lineTo(size.width * 0.3818971, size.height * 0.3892062);
    path_9.lineTo(size.width * 0.4284957, size.height * 0.3916648);
    path_9.close();

    Paint paint_9_fill = Paint()..style = PaintingStyle.fill;
    paint_9_fill.color = activeColor2;
    canvas.drawPath(path_9, paint_9_fill);

    Path path_10 = Path();
    path_10.moveTo(size.width * 0.5544036, size.height * 0.1571820);
    path_10.cubicTo(
        size.width * 0.5544036,
        size.height * 0.1571820,
        size.width * 0.5468741,
        size.height * 0.1701047,
        size.width * 0.5382396,
        size.height * 0.1754789);
    path_10.lineTo(size.width * 0.4504137, size.height * 0.2386609);
    path_10.cubicTo(
        size.width * 0.4411460,
        size.height * 0.2453508,
        size.width * 0.4286151,
        size.height * 0.2418063,
        size.width * 0.4234547,
        size.height * 0.2311133);
    path_10.lineTo(size.width * 0.4172942, size.height * 0.2183055);
    path_10.lineTo(size.width * 0.4079223, size.height * 0.1988078);
    path_10.lineTo(size.width * 0.4466223, size.height * 0.1766797);
    path_10.lineTo(size.width * 0.4508871, size.height * 0.1848562);
    path_10.lineTo(size.width * 0.4550993, size.height * 0.1928617);
    path_10.lineTo(size.width * 0.5106489, size.height * 0.1578680);
    path_10.lineTo(size.width * 0.5226014, size.height * 0.1343680);
    path_10.cubicTo(
        size.width * 0.5226014,
        size.height * 0.1343680,
        size.width * 0.5280770,
        size.height * 0.1400859,
        size.width * 0.5261813,
        size.height * 0.1465469);
    path_10.lineTo(size.width * 0.5413460, size.height * 0.1312234);
    path_10.cubicTo(
        size.width * 0.5427676,
        size.height * 0.1294508,
        size.width * 0.5449791,
        size.height * 0.1285930,
        size.width * 0.5470849,
        size.height * 0.1291078);
    path_10.cubicTo(
        size.width * 0.5485065,
        size.height * 0.1295078,
        size.width * 0.5496647,
        size.height * 0.1304797,
        size.width * 0.5495072,
        size.height * 0.1328242);
    path_10.cubicTo(
        size.width * 0.5495072,
        size.height * 0.1328242,
        size.width * 0.5552993,
        size.height * 0.1325383,
        size.width * 0.5530878,
        size.height * 0.1409438);
    path_10.cubicTo(
        size.width * 0.5530878,
        size.height * 0.1409438,
        size.width * 0.5589849,
        size.height * 0.1411719,
        size.width * 0.5545619,
        size.height * 0.1494633);
    path_10.cubicTo(
        size.width * 0.5545619,
        size.height * 0.1495203,
        size.width * 0.5571417,
        size.height * 0.1506062,
        size.width * 0.5544036,
        size.height * 0.1571820);
    path_10.close();

    Paint paint_10_fill = Paint()..style = PaintingStyle.fill;
    paint_10_fill.color = Colors.black;
    canvas.drawPath(path_10, paint_10_fill);

    Path path_11 = Path();
    path_11.moveTo(size.width * 0.5544036, size.height * 0.1571820);
    path_11.cubicTo(
        size.width * 0.5544036,
        size.height * 0.1571820,
        size.width * 0.5468741,
        size.height * 0.1701047,
        size.width * 0.5382396,
        size.height * 0.1754789);
    path_11.lineTo(size.width * 0.4504137, size.height * 0.2386609);
    path_11.cubicTo(
        size.width * 0.4411460,
        size.height * 0.2453508,
        size.width * 0.4286151,
        size.height * 0.2418063,
        size.width * 0.4234547,
        size.height * 0.2311133);
    path_11.lineTo(size.width * 0.4172942, size.height * 0.2183055);
    path_11.lineTo(size.width * 0.4079223, size.height * 0.1988078);
    path_11.lineTo(size.width * 0.4466223, size.height * 0.1766797);
    path_11.lineTo(size.width * 0.4508871, size.height * 0.1848562);
    path_11.lineTo(size.width * 0.4550993, size.height * 0.1928617);
    path_11.lineTo(size.width * 0.5106489, size.height * 0.1578680);
    path_11.lineTo(size.width * 0.5226014, size.height * 0.1343680);
    path_11.cubicTo(
        size.width * 0.5226014,
        size.height * 0.1343680,
        size.width * 0.5280770,
        size.height * 0.1400859,
        size.width * 0.5261813,
        size.height * 0.1465469);
    path_11.lineTo(size.width * 0.5413460, size.height * 0.1312234);
    path_11.cubicTo(
        size.width * 0.5427676,
        size.height * 0.1294508,
        size.width * 0.5449791,
        size.height * 0.1285930,
        size.width * 0.5470849,
        size.height * 0.1291078);
    path_11.cubicTo(
        size.width * 0.5485065,
        size.height * 0.1295078,
        size.width * 0.5496647,
        size.height * 0.1304797,
        size.width * 0.5495072,
        size.height * 0.1328242);
    path_11.cubicTo(
        size.width * 0.5495072,
        size.height * 0.1328242,
        size.width * 0.5552993,
        size.height * 0.1325383,
        size.width * 0.5530878,
        size.height * 0.1409438);
    path_11.cubicTo(
        size.width * 0.5530878,
        size.height * 0.1409438,
        size.width * 0.5589849,
        size.height * 0.1411719,
        size.width * 0.5545619,
        size.height * 0.1494633);
    path_11.cubicTo(
        size.width * 0.5545619,
        size.height * 0.1495203,
        size.width * 0.5571417,
        size.height * 0.1506062,
        size.width * 0.5544036,
        size.height * 0.1571820);
    path_11.close();

    Paint paint_11_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004195209;
    paint_11_stroke.color = Colors.black;
    canvas.drawPath(path_11, paint_11_stroke);

    Paint paint_11_fill = Paint()..style = PaintingStyle.fill;
    paint_11_fill.color = Colors.white;
    canvas.drawPath(path_11, paint_11_fill);

    Path path_12 = Path();
    path_12.moveTo(size.width * 0.1563302, size.height * 0.1232820);
    path_12.lineTo(size.width * 0.1318993, size.height * 0.1232820);
    path_12.cubicTo(
        size.width * 0.1318993,
        size.height * 0.1232820,
        size.width * 0.1337950,
        size.height * 0.1113891,
        size.width * 0.1479058,
        size.height * 0.1155055);
    path_12.lineTo(size.width * 0.1563302, size.height * 0.1232820);
    path_12.close();

    Paint paint_12_fill = Paint()..style = PaintingStyle.fill;
    paint_12_fill.color = Colors.black;
    canvas.drawPath(path_12, paint_12_fill);

    Path path_13 = Path();
    path_13.moveTo(size.width * 0.1563302, size.height * 0.1232820);
    path_13.lineTo(size.width * 0.1318993, size.height * 0.1232820);
    path_13.cubicTo(
        size.width * 0.1318993,
        size.height * 0.1232820,
        size.width * 0.1337950,
        size.height * 0.1113891,
        size.width * 0.1479058,
        size.height * 0.1155055);
    path_13.lineTo(size.width * 0.1563302, size.height * 0.1232820);
    path_13.close();

    Paint paint_13_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004195209;
    paint_13_stroke.color = Colors.black;
    canvas.drawPath(path_13, paint_13_stroke);

    Paint paint_13_fill = Paint()..style = PaintingStyle.fill;
    paint_13_fill.color = Colors.black;
    canvas.drawPath(path_13, paint_13_fill);

    Path path_14 = Path();
    path_14.moveTo(size.width * 0.1357475, size.height * 0.4193508);
    path_14.lineTo(size.width * 0.1111058, size.height * 0.4183211);
    path_14.lineTo(size.width * 0.1096309, size.height * 0.4182641);
    path_14.lineTo(size.width * 0.06692921, size.height * 0.4164344);
    path_14.lineTo(size.width * 0.06155856, size.height * 0.3660602);
    path_14.lineTo(size.width * 0.09831079, size.height * 0.3648594);
    path_14.lineTo(size.width * 0.1311662, size.height * 0.3637164);
    path_14.lineTo(size.width * 0.1357475, size.height * 0.4193508);
    path_14.close();

    Paint paint_14_fill = Paint()..style = PaintingStyle.fill;
    paint_14_fill.color = activeColor2;
    canvas.drawPath(path_14, paint_14_fill);

    Path path_15 = Path();
    path_15.moveTo(size.width * 0.3883065, size.height * 0.1651953);
    path_15.lineTo(size.width * 0.4078410, size.height * 0.2100227);
    path_15.lineTo(size.width * 0.4496475, size.height * 0.1783461);
    path_15.lineTo(size.width * 0.4137906, size.height * 0.1075594);
    path_15.lineTo(size.width * 0.3883065, size.height * 0.1651953);
    path_15.close();

    Paint paint_15_fill = Paint()..style = PaintingStyle.fill;
    paint_15_fill.color = Colors.black;
    canvas.drawPath(path_15, paint_15_fill);

    Path path_16 = Path();
    path_16.moveTo(size.width * 0.3883065, size.height * 0.1651953);
    path_16.lineTo(size.width * 0.4078410, size.height * 0.2100227);
    path_16.lineTo(size.width * 0.4496475, size.height * 0.1783461);
    path_16.lineTo(size.width * 0.4137906, size.height * 0.1075594);
    path_16.lineTo(size.width * 0.3883065, size.height * 0.1651953);
    path_16.close();

    Paint paint_16_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004195209;
    paint_16_stroke.color = Colors.black;
    canvas.drawPath(path_16, paint_16_stroke);

    Paint paint_16_fill = Paint()..style = PaintingStyle.fill;
    paint_16_fill.color = activeColor2;
    canvas.drawPath(path_16, paint_16_fill);

    Path path_17 = Path();
    path_17.moveTo(size.width * 0.2930619, size.height * 0.07290648);
    path_17.lineTo(size.width * 0.2808460, size.height * 0.1157328);
    path_17.lineTo(size.width * 0.2602058, size.height * 0.1125312);
    path_17.lineTo(size.width * 0.2544137, size.height * 0.1131031);
    path_17.lineTo(size.width * 0.1721691, size.height * 0.1210508);
    path_17.cubicTo(
        size.width * 0.1589532,
        size.height * 0.1267109,
        size.width * 0.1494230,
        size.height * 0.1230516,
        size.width * 0.1494230,
        size.height * 0.1230516);
    path_17.cubicTo(
        size.width * 0.1289935,
        size.height * 0.1113875,
        size.width * 0.1287302,
        size.height * 0.09606406,
        size.width * 0.1287302,
        size.height * 0.09606406);
    path_17.cubicTo(
        size.width * 0.1274669,
        size.height * 0.09103203,
        size.width * 0.1329424,
        size.height * 0.08874453,
        size.width * 0.1329424,
        size.height * 0.08874453);
    path_17.cubicTo(
        size.width * 0.1333640,
        size.height * 0.08188359,
        size.width * 0.1388396,
        size.height * 0.08234063,
        size.width * 0.1388396,
        size.height * 0.08234063);
    path_17.cubicTo(
        size.width * 0.1405770,
        size.height * 0.07467898,
        size.width * 0.1468432,
        size.height * 0.07748078,
        size.width * 0.1468432,
        size.height * 0.07748078);
    path_17.cubicTo(
        size.width * 0.1491597,
        size.height * 0.06810352,
        size.width * 0.1607964,
        size.height * 0.08279844,
        size.width * 0.1607964,
        size.height * 0.08279844);
    path_17.lineTo(size.width * 0.1746964, size.height * 0.09331953);
    path_17.lineTo(size.width * 0.2614698, size.height * 0.07502211);
    path_17.lineTo(size.width * 0.2799511, size.height * 0.07376414);
    path_17.lineTo(size.width * 0.2930619, size.height * 0.07290648);
    path_17.close();

    Paint paint_17_fill = Paint()..style = PaintingStyle.fill;
    paint_17_fill.color = Colors.black;
    canvas.drawPath(path_17, paint_17_fill);

    Path path_18 = Path();
    path_18.moveTo(size.width * 0.2930619, size.height * 0.07290648);
    path_18.lineTo(size.width * 0.2808460, size.height * 0.1157328);
    path_18.lineTo(size.width * 0.2602058, size.height * 0.1125312);
    path_18.lineTo(size.width * 0.2544137, size.height * 0.1131031);
    path_18.lineTo(size.width * 0.1721691, size.height * 0.1210508);
    path_18.cubicTo(
        size.width * 0.1589532,
        size.height * 0.1267109,
        size.width * 0.1494230,
        size.height * 0.1230516,
        size.width * 0.1494230,
        size.height * 0.1230516);
    path_18.cubicTo(
        size.width * 0.1289935,
        size.height * 0.1113875,
        size.width * 0.1287302,
        size.height * 0.09606406,
        size.width * 0.1287302,
        size.height * 0.09606406);
    path_18.cubicTo(
        size.width * 0.1274669,
        size.height * 0.09103203,
        size.width * 0.1329424,
        size.height * 0.08874453,
        size.width * 0.1329424,
        size.height * 0.08874453);
    path_18.cubicTo(
        size.width * 0.1333640,
        size.height * 0.08188359,
        size.width * 0.1388396,
        size.height * 0.08234063,
        size.width * 0.1388396,
        size.height * 0.08234063);
    path_18.cubicTo(
        size.width * 0.1405770,
        size.height * 0.07467898,
        size.width * 0.1468432,
        size.height * 0.07748078,
        size.width * 0.1468432,
        size.height * 0.07748078);
    path_18.cubicTo(
        size.width * 0.1491597,
        size.height * 0.06810352,
        size.width * 0.1607964,
        size.height * 0.08279844,
        size.width * 0.1607964,
        size.height * 0.08279844);
    path_18.lineTo(size.width * 0.1746964, size.height * 0.09331953);
    path_18.lineTo(size.width * 0.2614698, size.height * 0.07502211);
    path_18.lineTo(size.width * 0.2799511, size.height * 0.07376414);
    path_18.lineTo(size.width * 0.2930619, size.height * 0.07290648);
    path_18.close();

    Paint paint_18_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004195209;
    paint_18_stroke.color = Colors.black;
    canvas.drawPath(path_18, paint_18_stroke);

    Paint paint_18_fill = Paint()..style = PaintingStyle.fill;
    paint_18_fill.color = Colors.white;
    canvas.drawPath(path_18, paint_18_fill);

    Path path_19 = Path();
    path_19.moveTo(size.width * 0.3511439, size.height * 0.4990102);
    path_19.lineTo(size.width * 0.3245014, size.height * 0.4825422);
    path_19.cubicTo(
        size.width * 0.3238691,
        size.height * 0.4821422,
        size.width * 0.3230799,
        size.height * 0.4823711,
        size.width * 0.3227108,
        size.height * 0.4830570);
    path_19.cubicTo(
        size.width * 0.3223424,
        size.height * 0.4837430,
        size.width * 0.3225532,
        size.height * 0.4846008,
        size.width * 0.3231849,
        size.height * 0.4850016);
    path_19.lineTo(size.width * 0.3410345, size.height * 0.4960367);
    path_19.lineTo(size.width * 0.3113906, size.height * 0.4869453);
    path_19.cubicTo(
        size.width * 0.3107058,
        size.height * 0.4867164,
        size.width * 0.3099691,
        size.height * 0.4871742,
        size.width * 0.3097583,
        size.height * 0.4879172);
    path_19.cubicTo(
        size.width * 0.3095475,
        size.height * 0.4886609,
        size.width * 0.3099691,
        size.height * 0.4894609,
        size.width * 0.3106532,
        size.height * 0.4896898);
    path_19.lineTo(size.width * 0.3351899, size.height * 0.4971805);
    path_19.lineTo(size.width * 0.3135496, size.height * 0.4948930);
    path_19.cubicTo(
        size.width * 0.3128122,
        size.height * 0.4948359,
        size.width * 0.3121806,
        size.height * 0.4954078,
        size.width * 0.3121281,
        size.height * 0.4962078);
    path_19.cubicTo(
        size.width * 0.3120748,
        size.height * 0.4970086,
        size.width * 0.3126014,
        size.height * 0.4976945,
        size.width * 0.3133388,
        size.height * 0.4977516);
    path_19.lineTo(size.width * 0.3497748, size.height * 0.5016398);
    path_19.lineTo(size.width * 0.3498273, size.height * 0.5014117);
    path_19.lineTo(size.width * 0.3498799, size.height * 0.5014117);
    path_19.lineTo(size.width * 0.3511439, size.height * 0.4990102);
    path_19.close();

    Paint paint_19_fill = Paint()..style = PaintingStyle.fill;
    paint_19_fill.color = Colors.black;
    canvas.drawPath(path_19, paint_19_fill);

    Path path_20 = Path();
    path_20.moveTo(size.width * 0.3038496, size.height * 0.4846570);
    path_20.lineTo(size.width * 0.2953719, size.height * 0.4820844);
    path_20.cubicTo(
        size.width * 0.2946878,
        size.height * 0.4818555,
        size.width * 0.2939504,
        size.height * 0.4823133,
        size.width * 0.2937396,
        size.height * 0.4830563);
    path_20.cubicTo(
        size.width * 0.2935295,
        size.height * 0.4838000,
        size.width * 0.2939504,
        size.height * 0.4846000,
        size.width * 0.2946353,
        size.height * 0.4848289);
    path_20.lineTo(size.width * 0.3031122, size.height * 0.4874016);
    path_20.cubicTo(
        size.width * 0.3037964,
        size.height * 0.4876305,
        size.width * 0.3045338,
        size.height * 0.4871734,
        size.width * 0.3047446,
        size.height * 0.4864297);
    path_20.cubicTo(
        size.width * 0.3049554,
        size.height * 0.4856867,
        size.width * 0.3045338,
        size.height * 0.4848859,
        size.width * 0.3038496,
        size.height * 0.4846570);
    path_20.close();

    Paint paint_20_fill = Paint()..style = PaintingStyle.fill;
    paint_20_fill.color = Colors.black;
    canvas.drawPath(path_20, paint_20_fill);

    Path path_21 = Path();
    path_21.moveTo(size.width * 0.2942367, size.height * 0.4928844);
    path_21.lineTo(size.width * 0.2758604, size.height * 0.4909398);
    path_21.cubicTo(
        size.width * 0.2751230,
        size.height * 0.4908828,
        size.width * 0.2744914,
        size.height * 0.4914547,
        size.width * 0.2744388,
        size.height * 0.4922555);
    path_21.cubicTo(
        size.width * 0.2743863,
        size.height * 0.4930555,
        size.width * 0.2749129,
        size.height * 0.4937422,
        size.width * 0.2756496,
        size.height * 0.4937992);
    path_21.lineTo(size.width * 0.2940259, size.height * 0.4957430);
    path_21.cubicTo(
        size.width * 0.2947633,
        size.height * 0.4958000,
        size.width * 0.2953950,
        size.height * 0.4952289,
        size.width * 0.2954475,
        size.height * 0.4944281);
    path_21.cubicTo(
        size.width * 0.2955000,
        size.height * 0.4936273,
        size.width * 0.2949734,
        size.height * 0.4929984,
        size.width * 0.2942367,
        size.height * 0.4928844);
    path_21.close();

    Paint paint_21_fill = Paint()..style = PaintingStyle.fill;
    paint_21_fill.color = Colors.black;
    canvas.drawPath(path_21, paint_21_fill);

    Path path_22 = Path();
    path_22.moveTo(size.width * 0.4460906, size.height * 0.4841797);
    path_22.cubicTo(
        size.width * 0.4460906,
        size.height * 0.4841797,
        size.width * 0.4370871,
        size.height * 0.4836078,
        size.width * 0.4372453,
        size.height * 0.4838359);
    path_22.cubicTo(
        size.width * 0.4385612,
        size.height * 0.4857805,
        size.width * 0.4360345,
        size.height * 0.4918414,
        size.width * 0.4312957,
        size.height * 0.4943000);
    path_22.cubicTo(
        size.width * 0.4294525,
        size.height * 0.4952719,
        size.width * 0.4275568,
        size.height * 0.4959578,
        size.width * 0.4255036,
        size.height * 0.4959578);
    path_22.lineTo(size.width * 0.4172892, size.height * 0.4959578);
    path_22.lineTo(size.width * 0.4172892, size.height * 0.4827500);
    path_22.lineTo(size.width * 0.4053899, size.height * 0.4827500);
    path_22.lineTo(size.width * 0.4053899, size.height * 0.4959578);
    path_22.lineTo(size.width * 0.3745353, size.height * 0.4959578);
    path_22.lineTo(size.width * 0.3745353, size.height * 0.4827500);
    path_22.lineTo(size.width * 0.3626353, size.height * 0.4827500);
    path_22.lineTo(size.width * 0.3626353, size.height * 0.4959578);
    path_22.lineTo(size.width * 0.3510518, size.height * 0.4959578);
    path_22.cubicTo(
        size.width * 0.3498403,
        size.height * 0.4959578,
        size.width * 0.3488928,
        size.height * 0.4969875,
        size.width * 0.3488928,
        size.height * 0.4983023);
    path_22.lineTo(size.width * 0.3488928, size.height * 0.5016758);
    path_22.lineTo(size.width * 0.4262935, size.height * 0.5016758);
    path_22.lineTo(size.width * 0.4277676, size.height * 0.5016758);
    path_22.lineTo(size.width * 0.4325590, size.height * 0.5016758);
    path_22.cubicTo(
        size.width * 0.4334014,
        size.height * 0.5016758,
        size.width * 0.4342964,
        size.height * 0.5015617,
        size.width * 0.4350863,
        size.height * 0.5013898);
    path_22.cubicTo(
        size.width * 0.4388777,
        size.height * 0.5005320,
        size.width * 0.4481446,
        size.height * 0.4962437,
        size.width * 0.4460906,
        size.height * 0.4841797);
    path_22.close();

    Paint paint_22_fill = Paint()..style = PaintingStyle.fill;
    paint_22_fill.color = Colors.black;
    canvas.drawPath(path_22, paint_22_fill);

    Path path_23 = Path();
    path_23.moveTo(size.width * 0.2672129, size.height * 0.2839047);
    path_23.lineTo(size.width * 0.2114986, size.height * 0.3462828);
    path_23.lineTo(size.width * 0.1098245, size.height * 0.3414797);
    path_23.cubicTo(
        size.width * 0.1098245,
        size.height * 0.3414797,
        size.width * 0.1319388,
        size.height * 0.3977430,
        size.width * 0.1142475,
        size.height * 0.4354234);
    path_23.lineTo(size.width * 0.2220288, size.height * 0.4283336);
    path_23.cubicTo(
        size.width * 0.2330338,
        size.height * 0.4275906,
        size.width * 0.2431957,
        size.height * 0.4218156,
        size.width * 0.2500410,
        size.height * 0.4124383);
    path_23.lineTo(size.width * 0.3353906, size.height * 0.3038922);
    path_23.lineTo(size.width * 0.2672129, size.height * 0.2839047);
    path_23.close();

    Paint paint_23_fill = Paint()..style = PaintingStyle.fill;
    paint_23_fill.color = Colors.black;
    canvas.drawPath(path_23, paint_23_fill);

    Path path_24 = Path();
    path_24.moveTo(size.width * 0.2672129, size.height * 0.2839047);
    path_24.lineTo(size.width * 0.2114986, size.height * 0.3462828);
    path_24.lineTo(size.width * 0.1098245, size.height * 0.3414797);
    path_24.cubicTo(
        size.width * 0.1098245,
        size.height * 0.3414797,
        size.width * 0.1319388,
        size.height * 0.3977430,
        size.width * 0.1142475,
        size.height * 0.4354234);
    path_24.lineTo(size.width * 0.2220288, size.height * 0.4283336);
    path_24.cubicTo(
        size.width * 0.2330338,
        size.height * 0.4275906,
        size.width * 0.2431957,
        size.height * 0.4218156,
        size.width * 0.2500410,
        size.height * 0.4124383);
    path_24.lineTo(size.width * 0.3353906, size.height * 0.3038922);
    path_24.lineTo(size.width * 0.2672129, size.height * 0.2839047);
    path_24.close();

    Paint paint_24_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004195209;
    paint_24_stroke.color = Colors.black;
    canvas.drawPath(path_24, paint_24_stroke);

    Paint paint_24_fill = Paint()..style = PaintingStyle.fill;
    paint_24_fill.color = Colors.white;
    canvas.drawPath(path_24, paint_24_fill);

    Path path_25 = Path();
    path_25.moveTo(size.width * 0.4140633, size.height * 0.1072766);
    path_25.cubicTo(
        size.width * 0.4140633,
        size.height * 0.1072766,
        size.width * 0.4136424,
        size.height * 0.1601664,
        size.width * 0.3965302,
        size.height * 0.1669141);
    path_25.cubicTo(
        size.width * 0.3854201,
        size.height * 0.1712594,
        size.width * 0.3724669,
        size.height * 0.1970469,
        size.width * 0.3642007,
        size.height * 0.2160297);
    path_25.cubicTo(
        size.width * 0.3591460,
        size.height * 0.2276945,
        size.width * 0.3583374,
        size.height * 0.2306602,
        size.width * 0.3583374,
        size.height * 0.2306602);
    path_25.lineTo(size.width * 0.2776094, size.height * 0.1874852);
    path_25.lineTo(size.width * 0.2801655, size.height * 0.1835523);
    path_25.lineTo(size.width * 0.3141799, size.height * 0.1271180);
    path_25.lineTo(size.width * 0.2628432, size.height * 0.1290617);
    path_25.cubicTo(
        size.width * 0.2628432,
        size.height * 0.1290617,
        size.width * 0.2785338,
        size.height * 0.1112219,
        size.width * 0.2823245,
        size.height * 0.09995781);
    path_25.cubicTo(
        size.width * 0.2872216,
        size.height * 0.08532031,
        size.width * 0.2839043,
        size.height * 0.07194062,
        size.width * 0.2839043,
        size.height * 0.07194062);
    path_25.lineTo(size.width * 0.3467727, size.height * 0.07096859);
    path_25.lineTo(size.width * 0.3937388, size.height * 0.08046016);
    path_25.cubicTo(
        size.width * 0.3937388,
        size.height * 0.08046016,
        size.width * 0.3995835,
        size.height * 0.09584141,
        size.width * 0.4049014,
        size.height * 0.1009297);
    path_25.cubicTo(
        size.width * 0.4123259,
        size.height * 0.1079633,
        size.width * 0.4140633,
        size.height * 0.1072766,
        size.width * 0.4140633,
        size.height * 0.1072766);
    path_25.close();

    Paint paint_25_fill = Paint()..style = PaintingStyle.fill;
    paint_25_fill.color = Colors.black;
    canvas.drawPath(path_25, paint_25_fill);

    Path path_26 = Path();
    path_26.moveTo(size.width * 0.4140633, size.height * 0.1072766);
    path_26.cubicTo(
        size.width * 0.4140633,
        size.height * 0.1072766,
        size.width * 0.4136424,
        size.height * 0.1601664,
        size.width * 0.3965302,
        size.height * 0.1669141);
    path_26.cubicTo(
        size.width * 0.3854201,
        size.height * 0.1712594,
        size.width * 0.3724669,
        size.height * 0.1970469,
        size.width * 0.3642007,
        size.height * 0.2160297);
    path_26.cubicTo(
        size.width * 0.3591460,
        size.height * 0.2276945,
        size.width * 0.3583374,
        size.height * 0.2306602,
        size.width * 0.3583374,
        size.height * 0.2306602);
    path_26.lineTo(size.width * 0.2776094, size.height * 0.1874852);
    path_26.lineTo(size.width * 0.2801655, size.height * 0.1835523);
    path_26.lineTo(size.width * 0.3141799, size.height * 0.1271180);
    path_26.lineTo(size.width * 0.2628432, size.height * 0.1290617);
    path_26.cubicTo(
        size.width * 0.2628432,
        size.height * 0.1290617,
        size.width * 0.2785338,
        size.height * 0.1112219,
        size.width * 0.2823245,
        size.height * 0.09995781);
    path_26.cubicTo(
        size.width * 0.2872216,
        size.height * 0.08532031,
        size.width * 0.2839043,
        size.height * 0.07194062,
        size.width * 0.2839043,
        size.height * 0.07194062);
    path_26.lineTo(size.width * 0.3467727, size.height * 0.07096859);
    path_26.lineTo(size.width * 0.3937388, size.height * 0.08046016);
    path_26.cubicTo(
        size.width * 0.3937388,
        size.height * 0.08046016,
        size.width * 0.3995835,
        size.height * 0.09584141,
        size.width * 0.4049014,
        size.height * 0.1009297);
    path_26.cubicTo(
        size.width * 0.4123259,
        size.height * 0.1079633,
        size.width * 0.4140633,
        size.height * 0.1072766,
        size.width * 0.4140633,
        size.height * 0.1072766);
    path_26.close();

    Paint paint_26_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004195209;
    paint_26_stroke.color = Colors.black;
    canvas.drawPath(path_26, paint_26_stroke);

    Paint paint_26_fill = Paint()..style = PaintingStyle.fill;
    paint_26_fill.color = activeColor1;
    canvas.drawPath(path_26, paint_26_fill);

    Path path_27 = Path();
    path_27.moveTo(size.width * 0.3466309, size.height * 0.2228195);
    path_27.lineTo(size.width * 0.4249266, size.height * 0.2639883);
    path_27.cubicTo(
        size.width * 0.4249266,
        size.height * 0.2639883,
        size.width * 0.4726302,
        size.height * 0.2821711,
        size.width * 0.4685237,
        size.height * 0.3044703);
    path_27.lineTo(size.width * 0.4251367, size.height * 0.4240875);
    path_27.cubicTo(
        size.width * 0.4251367,
        size.height * 0.4240875,
        size.width * 0.4009165,
        size.height * 0.3983570,
        size.width * 0.3588993,
        size.height * 0.4106508);
    path_27.lineTo(size.width * 0.3883324, size.height * 0.3229961);
    path_27.lineTo(size.width * 0.2680719, size.height * 0.2860586);
    path_27.cubicTo(
        size.width * 0.2587525,
        size.height * 0.2832000,
        size.width * 0.2513281,
        size.height * 0.2755383,
        size.width * 0.2483266,
        size.height * 0.2655891);
    path_27.cubicTo(
        size.width * 0.2435885,
        size.height * 0.2497508,
        size.width * 0.2451151,
        size.height * 0.2232203,
        size.width * 0.2784446,
        size.height * 0.1858828);
    path_27.lineTo(size.width * 0.3466309, size.height * 0.2228195);
    path_27.close();

    Paint paint_27_fill = Paint()..style = PaintingStyle.fill;
    paint_27_fill.color = Colors.black;
    canvas.drawPath(path_27, paint_27_fill);

    Path path_28 = Path();
    path_28.moveTo(size.width * 0.3466309, size.height * 0.2228195);
    path_28.lineTo(size.width * 0.4249266, size.height * 0.2639883);
    path_28.cubicTo(
        size.width * 0.4249266,
        size.height * 0.2639883,
        size.width * 0.4726302,
        size.height * 0.2821711,
        size.width * 0.4685237,
        size.height * 0.3044703);
    path_28.lineTo(size.width * 0.4251367, size.height * 0.4240875);
    path_28.cubicTo(
        size.width * 0.4251367,
        size.height * 0.4240875,
        size.width * 0.4009165,
        size.height * 0.3983570,
        size.width * 0.3588993,
        size.height * 0.4106508);
    path_28.lineTo(size.width * 0.3883324, size.height * 0.3229961);
    path_28.lineTo(size.width * 0.2680719, size.height * 0.2860586);
    path_28.cubicTo(
        size.width * 0.2587525,
        size.height * 0.2832000,
        size.width * 0.2513281,
        size.height * 0.2755383,
        size.width * 0.2483266,
        size.height * 0.2655891);
    path_28.cubicTo(
        size.width * 0.2435885,
        size.height * 0.2497508,
        size.width * 0.2451151,
        size.height * 0.2232203,
        size.width * 0.2784446,
        size.height * 0.1858828);
    path_28.lineTo(size.width * 0.3466309, size.height * 0.2228195);
    path_28.close();

    Paint paint_28_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004195209;
    paint_28_stroke.color = Colors.black;
    canvas.drawPath(path_28, paint_28_stroke);

    Paint paint_28_fill = Paint()..style = PaintingStyle.fill;
    paint_28_fill.color = Colors.white;
    canvas.drawPath(path_28, paint_28_fill);

    Path path_29 = Path();
    path_29.moveTo(size.width * 0.4184151, size.height * 0.4321672);
    path_29.lineTo(size.width * 0.4128863, size.height * 0.4612711);
    path_29.lineTo(size.width * 0.4521655, size.height * 0.4852859);
    path_29.lineTo(size.width * 0.3610755, size.height * 0.4852859);
    path_29.lineTo(size.width * 0.3686043, size.height * 0.4339398);
    path_29.lineTo(size.width * 0.4184151, size.height * 0.4321672);
    path_29.close();

    Paint paint_29_fill = Paint()..style = PaintingStyle.fill;
    paint_29_fill.color = Colors.black;
    canvas.drawPath(path_29, paint_29_fill);

    Path path_30 = Path();
    path_30.moveTo(size.width * 0.03069691, size.height * 0.4469219);
    path_30.cubicTo(
        size.width * 0.02911734,
        size.height * 0.4486375,
        size.width * 0.02322014,
        size.height * 0.4469219,
        size.width * 0.02021892,
        size.height * 0.4422906);
    path_30.cubicTo(
        size.width * 0.01906050,
        size.height * 0.4405180,
        size.width * 0.01811273,
        size.height * 0.4385164,
        size.width * 0.01779683,
        size.height * 0.4363437);
    path_30.lineTo(size.width * 0.01642784, size.height * 0.4275383);
    path_30.lineTo(size.width * 0.02838022, size.height * 0.4253656);
    path_30.lineTo(size.width * 0.02643201, size.height * 0.4126719);
    path_30.lineTo(size.width * 0.01447971, size.height * 0.4148445);
    path_30.lineTo(size.width * 0.009372302, size.height * 0.3817961);
    path_30.lineTo(size.width * 0.02132460, size.height * 0.3796234);
    path_30.lineTo(size.width * 0.01937647, size.height * 0.3669297);
    path_30.lineTo(size.width * 0.007424101, size.height * 0.3691023);
    path_30.lineTo(size.width * 0.005528604, size.height * 0.3566945);
    path_30.cubicTo(
        size.width * 0.005317993,
        size.height * 0.3554367,
        size.width * 0.004212266,
        size.height * 0.3545789,
        size.width * 0.003053892,
        size.height * 0.3547508);
    path_30.lineTo(0, size.height * 0.3553227);
    path_30.lineTo(size.width * 0.01279475, size.height * 0.4382313);
    path_30.lineTo(size.width * 0.01305806, size.height * 0.4398320);
    path_30.lineTo(size.width * 0.01384784, size.height * 0.4449781);
    path_30.cubicTo(
        size.width * 0.01400583,
        size.height * 0.4458930,
        size.width * 0.01421640,
        size.height * 0.4468078,
        size.width * 0.01453230,
        size.height * 0.4476656);
    path_30.cubicTo(
        size.width * 0.01595396,
        size.height * 0.4514961,
        size.width * 0.02132460,
        size.height * 0.4607016,
        size.width * 0.03196058,
        size.height * 0.4565281);
    path_30.cubicTo(
        size.width * 0.03185532,
        size.height * 0.4564133,
        size.width * 0.03085489,
        size.height * 0.4466930,
        size.width * 0.03069691,
        size.height * 0.4469219);
    path_30.close();

    Paint paint_30_fill = Paint()..style = PaintingStyle.fill;
    paint_30_fill.color = Colors.black;
    canvas.drawPath(path_30, paint_30_fill);

    Path path_31 = Path();
    path_31.moveTo(size.width * 0.07453813, size.height * 0.4181430);
    path_31.lineTo(size.width * 0.04715820, size.height * 0.4170570);
    path_31.lineTo(size.width * 0.03183612, size.height * 0.4630852);
    path_31.lineTo(size.width * 0.01677719, size.height * 0.3654820);
    path_31.lineTo(size.width * 0.06469180, size.height * 0.3650820);
    path_31.lineTo(size.width * 0.07453813, size.height * 0.4181430);
    path_31.close();

    Paint paint_31_fill = Paint()..style = PaintingStyle.fill;
    paint_31_fill.color = Colors.black;
    canvas.drawPath(path_31, paint_31_fill);

    Path path_32 = Path();
    path_32.moveTo(size.width * 0.3219129, size.height * 0.07044094);
    path_32.lineTo(size.width * 0.3198065, size.height * 0.06912586);
    path_32.lineTo(size.width * 0.3311273, size.height * 0.04802703);
    path_32.lineTo(size.width * 0.3332331, size.height * 0.04939930);
    path_32.lineTo(size.width * 0.3219129, size.height * 0.07044094);
    path_32.close();

    Paint paint_32_fill = Paint()..style = PaintingStyle.fill;
    paint_32_fill.color = const Color(0xff434343);
    canvas.drawPath(path_32, paint_32_fill);

    Path path_33 = Path();
    path_33.moveTo(size.width * 0.3312842, size.height * 0.06824648);
    path_33.lineTo(size.width * 0.3183309, size.height * 0.05132164);
    path_33.lineTo(size.width * 0.3202266, size.height * 0.04960633);
    path_33.lineTo(size.width * 0.3331799, size.height * 0.06658828);
    path_33.lineTo(size.width * 0.3312842, size.height * 0.06824648);
    path_33.close();

    Paint paint_33_fill = Paint()..style = PaintingStyle.fill;
    paint_33_fill.color = const Color(0xff434343);
    canvas.drawPath(path_33, paint_33_fill);

    Path path_34 = Path();
    path_34.moveTo(size.width * 0.6692698, size.height * 0.5726242);
    path_34.cubicTo(
        size.width * 0.6692698,
        size.height * 0.5726242,
        size.width * 0.6623338,
        size.height * 0.6000328,
        size.width * 0.6402511,
        size.height * 0.6039492);
    path_34.cubicTo(
        size.width * 0.6181683,
        size.height * 0.6078656,
        size.width * 0.5992374,
        size.height * 0.6088398,
        size.width * 0.6055460,
        size.height * 0.6230352);
    path_34.cubicTo(
        size.width * 0.6118554,
        size.height * 0.6372305,
        size.width * 0.6357439,
        size.height * 0.6264625,
        size.width * 0.6542237,
        size.height * 0.6230352);
    path_34.cubicTo(
        size.width * 0.6727036,
        size.height * 0.6196078,
        size.width * 0.6884741,
        size.height * 0.6284188,
        size.width * 0.6907295,
        size.height * 0.6156992);
    path_34.cubicTo(
        size.width * 0.6929849,
        size.height * 0.6029789,
        size.width * 0.6844655,
        size.height * 0.5770422,
        size.width * 0.6844655,
        size.height * 0.5770422);
    path_34.lineTo(size.width * 0.6692698, size.height * 0.5726242);
    path_34.close();

    Paint paint_34_fill = Paint()..style = PaintingStyle.fill;
    paint_34_fill.color = Colors.black;
    canvas.drawPath(path_34, paint_34_fill);

    Path path_35 = Path();
    path_35.moveTo(size.width * 0.6692698, size.height * 0.5726242);
    path_35.cubicTo(
        size.width * 0.6692698,
        size.height * 0.5726242,
        size.width * 0.6623338,
        size.height * 0.6000328,
        size.width * 0.6402511,
        size.height * 0.6039492);
    path_35.cubicTo(
        size.width * 0.6181683,
        size.height * 0.6078656,
        size.width * 0.5992374,
        size.height * 0.6088398,
        size.width * 0.6055460,
        size.height * 0.6230352);
    path_35.cubicTo(
        size.width * 0.6118554,
        size.height * 0.6372305,
        size.width * 0.6357439,
        size.height * 0.6264625,
        size.width * 0.6542237,
        size.height * 0.6230352);
    path_35.cubicTo(
        size.width * 0.6727036,
        size.height * 0.6196078,
        size.width * 0.6884741,
        size.height * 0.6284188,
        size.width * 0.6907295,
        size.height * 0.6156992);
    path_35.cubicTo(
        size.width * 0.6929849,
        size.height * 0.6029789,
        size.width * 0.6844655,
        size.height * 0.5770422,
        size.width * 0.6844655,
        size.height * 0.5770422);
    path_35.lineTo(size.width * 0.6692698, size.height * 0.5726242);
    path_35.close();

    Paint paint_35_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_35_stroke.color = Colors.black;
    canvas.drawPath(path_35, paint_35_stroke);

    Paint paint_35_fill = Paint()..style = PaintingStyle.fill;
    paint_35_fill.color = activeColor1;
    canvas.drawPath(path_35, paint_35_fill);

    Path path_36 = Path();
    path_36.moveTo(size.width * 0.6352921, size.height * 0.4287227);
    path_36.cubicTo(
        size.width * 0.6258266,
        size.height * 0.4390016,
        size.width * 0.6240324,
        size.height * 0.4478086,
        size.width * 0.6303338,
        size.height * 0.4624930);
    path_36.cubicTo(
        size.width * 0.6366353,
        size.height * 0.4771773,
        size.width * 0.6689906,
        size.height * 0.5721266,
        size.width * 0.6640813,
        size.height * 0.5848508);
    path_36.cubicTo(
        size.width * 0.6591727,
        size.height * 0.5975742,
        size.width * 0.6745036,
        size.height * 0.5951250,
        size.width * 0.6817209,
        size.height * 0.5877852);
    path_36.cubicTo(
        size.width * 0.6889381,
        size.height * 0.5804453,
        size.width * 0.6866791,
        size.height * 0.4698336,
        size.width * 0.6749655,
        size.height * 0.4620039);
    path_36.cubicTo(
        size.width * 0.6938957,
        size.height * 0.4629820,
        size.width * 0.7119216,
        size.height * 0.4624930,
        size.width * 0.7119216,
        size.height * 0.4624930);
    path_36.lineTo(size.width * 0.7015410, size.height * 0.4149477);
    path_36.cubicTo(
        size.width * 0.7015410,
        size.height * 0.4149477,
        size.width * 0.6447568,
        size.height * 0.4184445,
        size.width * 0.6352921,
        size.height * 0.4287227);
    path_36.close();

    Paint paint_36_fill = Paint()..style = PaintingStyle.fill;
    paint_36_fill.color = Colors.black;
    canvas.drawPath(path_36, paint_36_fill);

    Path path_37 = Path();
    path_37.moveTo(size.width * 0.6352921, size.height * 0.4287227);
    path_37.cubicTo(
        size.width * 0.6258266,
        size.height * 0.4390016,
        size.width * 0.6240324,
        size.height * 0.4478086,
        size.width * 0.6303338,
        size.height * 0.4624930);
    path_37.cubicTo(
        size.width * 0.6366353,
        size.height * 0.4771773,
        size.width * 0.6689906,
        size.height * 0.5721266,
        size.width * 0.6640813,
        size.height * 0.5848508);
    path_37.cubicTo(
        size.width * 0.6591727,
        size.height * 0.5975742,
        size.width * 0.6745036,
        size.height * 0.5951250,
        size.width * 0.6817209,
        size.height * 0.5877852);
    path_37.cubicTo(
        size.width * 0.6889381,
        size.height * 0.5804453,
        size.width * 0.6866791,
        size.height * 0.4698336,
        size.width * 0.6749655,
        size.height * 0.4620039);
    path_37.cubicTo(
        size.width * 0.6938957,
        size.height * 0.4629820,
        size.width * 0.7119216,
        size.height * 0.4624930,
        size.width * 0.7119216,
        size.height * 0.4624930);
    path_37.lineTo(size.width * 0.7015410, size.height * 0.4149477);
    path_37.cubicTo(
        size.width * 0.7015410,
        size.height * 0.4149477,
        size.width * 0.6447568,
        size.height * 0.4184445,
        size.width * 0.6352921,
        size.height * 0.4287227);
    path_37.close();

    Paint paint_37_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_37_stroke.color = Colors.black;
    canvas.drawPath(path_37, paint_37_stroke);

    Paint paint_37_fill = Paint()..style = PaintingStyle.fill;
    paint_37_fill.color = Colors.white;
    canvas.drawPath(path_37, paint_37_fill);

    Path path_38 = Path();
    path_38.moveTo(size.width * 0.9816906, size.height * 0.6011172);
    path_38.lineTo(size.width * 0.9482014, size.height * 0.5937406);
    path_38.lineTo(size.width * 0.9482014, size.height * 0.4730219);
    path_38.lineTo(size.width * 0.9367626, size.height * 0.4576570);
    path_38.lineTo(size.width * 0.9367626, size.height * 0.5922695);
    path_38.lineTo(size.width * 0.9028849, size.height * 0.5865633);
    path_38.cubicTo(
        size.width * 0.9015324,
        size.height * 0.5863375,
        size.width * 0.9001511,
        size.height * 0.5866898,
        size.width * 0.8990288,
        size.height * 0.5875477);
    path_38.cubicTo(
        size.width * 0.8979137,
        size.height * 0.5884047,
        size.width * 0.8971367,
        size.height * 0.5897000,
        size.width * 0.8968777,
        size.height * 0.5911609);
    path_38.lineTo(size.width * 0.8968489, size.height * 0.5913523);
    path_38.cubicTo(
        size.width * 0.8967122,
        size.height * 0.5921055,
        size.width * 0.8967194,
        size.height * 0.5928805,
        size.width * 0.8968633,
        size.height * 0.5936305);
    path_38.cubicTo(
        size.width * 0.8970144,
        size.height * 0.5943812,
        size.width * 0.8972950,
        size.height * 0.5950922,
        size.width * 0.8976978,
        size.height * 0.5957227);
    path_38.cubicTo(
        size.width * 0.8981007,
        size.height * 0.5963523,
        size.width * 0.8986115,
        size.height * 0.5968883,
        size.width * 0.8992086,
        size.height * 0.5972992);
    path_38.cubicTo(
        size.width * 0.8998058,
        size.height * 0.5977102,
        size.width * 0.9004748,
        size.height * 0.5979867,
        size.width * 0.9011727,
        size.height * 0.5981141);
    path_38.lineTo(size.width * 0.9797626, size.height * 0.6124273);
    path_38.cubicTo(
        size.width * 0.9811439,
        size.height * 0.6127055,
        size.width * 0.9825683,
        size.height * 0.6123758,
        size.width * 0.9837266,
        size.height * 0.6115117);
    path_38.cubicTo(
        size.width * 0.9848849,
        size.height * 0.6106477,
        size.width * 0.9856763,
        size.height * 0.6093195,
        size.width * 0.9859353,
        size.height * 0.6078195);
    path_38.cubicTo(
        size.width * 0.9861871,
        size.height * 0.6063203,
        size.width * 0.9858849,
        size.height * 0.6047711,
        size.width * 0.9850863,
        size.height * 0.6035148);
    path_38.cubicTo(
        size.width * 0.9842950,
        size.height * 0.6022578,
        size.width * 0.9830719,
        size.height * 0.6013953,
        size.width * 0.9816906,
        size.height * 0.6011172);
    path_38.lineTo(size.width * 0.9816906, size.height * 0.6011172);
    path_38.close();

    Paint paint_38_fill = Paint()..style = PaintingStyle.fill;
    paint_38_fill.color = Colors.black;
    canvas.drawPath(path_38, paint_38_fill);

    Path path_39 = Path();
    path_39.moveTo(size.width * 0.9816906, size.height * 0.6011172);
    path_39.lineTo(size.width * 0.9482014, size.height * 0.5937406);
    path_39.lineTo(size.width * 0.9482014, size.height * 0.4730219);
    path_39.lineTo(size.width * 0.9367626, size.height * 0.4576570);
    path_39.lineTo(size.width * 0.9367626, size.height * 0.5922695);
    path_39.lineTo(size.width * 0.9028849, size.height * 0.5865633);
    path_39.cubicTo(
        size.width * 0.9015324,
        size.height * 0.5863375,
        size.width * 0.9001511,
        size.height * 0.5866898,
        size.width * 0.8990288,
        size.height * 0.5875477);
    path_39.cubicTo(
        size.width * 0.8979137,
        size.height * 0.5884047,
        size.width * 0.8971367,
        size.height * 0.5897000,
        size.width * 0.8968777,
        size.height * 0.5911609);
    path_39.lineTo(size.width * 0.8968489, size.height * 0.5913523);
    path_39.cubicTo(
        size.width * 0.8967122,
        size.height * 0.5921055,
        size.width * 0.8967194,
        size.height * 0.5928805,
        size.width * 0.8968633,
        size.height * 0.5936305);
    path_39.cubicTo(
        size.width * 0.8970144,
        size.height * 0.5943812,
        size.width * 0.8972950,
        size.height * 0.5950922,
        size.width * 0.8976978,
        size.height * 0.5957227);
    path_39.cubicTo(
        size.width * 0.8981007,
        size.height * 0.5963523,
        size.width * 0.8986115,
        size.height * 0.5968883,
        size.width * 0.8992086,
        size.height * 0.5972992);
    path_39.cubicTo(
        size.width * 0.8998058,
        size.height * 0.5977102,
        size.width * 0.9004748,
        size.height * 0.5979867,
        size.width * 0.9011727,
        size.height * 0.5981141);
    path_39.lineTo(size.width * 0.9797626, size.height * 0.6124273);
    path_39.cubicTo(
        size.width * 0.9811439,
        size.height * 0.6127055,
        size.width * 0.9825683,
        size.height * 0.6123758,
        size.width * 0.9837266,
        size.height * 0.6115117);
    path_39.cubicTo(
        size.width * 0.9848849,
        size.height * 0.6106477,
        size.width * 0.9856763,
        size.height * 0.6093195,
        size.width * 0.9859353,
        size.height * 0.6078195);
    path_39.cubicTo(
        size.width * 0.9861871,
        size.height * 0.6063203,
        size.width * 0.9858849,
        size.height * 0.6047711,
        size.width * 0.9850863,
        size.height * 0.6035148);
    path_39.cubicTo(
        size.width * 0.9842950,
        size.height * 0.6022578,
        size.width * 0.9830719,
        size.height * 0.6013953,
        size.width * 0.9816906,
        size.height * 0.6011172);
    path_39.lineTo(size.width * 0.9816906, size.height * 0.6011172);
    path_39.close();

    Paint paint_39_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_39_stroke.color = Colors.black;
    canvas.drawPath(path_39, paint_39_stroke);

    Paint paint_39_fill = Paint()..style = PaintingStyle.fill;
    paint_39_fill.color = Colors.white;
    canvas.drawPath(path_39, paint_39_fill);

    Path path_40 = Path();
    path_40.moveTo(size.width * 0.9355755, size.height * 0.5459508);
    path_40.lineTo(size.width * 0.6911115, size.height * 0.5875500);
    path_40.lineTo(size.width * 0.6911115, size.height * 0.5970953);
    path_40.lineTo(size.width * 0.9371007, size.height * 0.5566813);
    path_40.cubicTo(
        size.width * 0.9377482,
        size.height * 0.5565766,
        size.width * 0.9383741,
        size.height * 0.5563328,
        size.width * 0.9389424,
        size.height * 0.5559641);
    path_40.cubicTo(
        size.width * 0.9395036,
        size.height * 0.5555953,
        size.width * 0.9399928,
        size.height * 0.5551094,
        size.width * 0.9403885,
        size.height * 0.5545336);
    path_40.cubicTo(
        size.width * 0.9407770,
        size.height * 0.5539578,
        size.width * 0.9410576,
        size.height * 0.5533039,
        size.width * 0.9412158,
        size.height * 0.5526094);
    path_40.cubicTo(
        size.width * 0.9413741,
        size.height * 0.5519148,
        size.width * 0.9414029,
        size.height * 0.5511938,
        size.width * 0.9413022,
        size.height * 0.5504867);
    path_40.cubicTo(
        size.width * 0.9412014,
        size.height * 0.5497797,
        size.width * 0.9409784,
        size.height * 0.5491016,
        size.width * 0.9406331,
        size.height * 0.5484898);
    path_40.cubicTo(
        size.width * 0.9402878,
        size.height * 0.5478789,
        size.width * 0.9398417,
        size.height * 0.5473477,
        size.width * 0.9393094,
        size.height * 0.5469258);
    path_40.cubicTo(
        size.width * 0.9387770,
        size.height * 0.5465047,
        size.width * 0.9381727,
        size.height * 0.5462016,
        size.width * 0.9375324,
        size.height * 0.5460344);
    path_40.cubicTo(
        size.width * 0.9368921,
        size.height * 0.5458664,
        size.width * 0.9362302,
        size.height * 0.5458383,
        size.width * 0.9355755,
        size.height * 0.5459508);
    path_40.lineTo(size.width * 0.9355755, size.height * 0.5459508);
    path_40.close();

    Paint paint_40_fill = Paint()..style = PaintingStyle.fill;
    paint_40_fill.color = Colors.black;
    canvas.drawPath(path_40, paint_40_fill);

    Path path_41 = Path();
    path_41.moveTo(size.width * 0.9355755, size.height * 0.5459508);
    path_41.lineTo(size.width * 0.6911115, size.height * 0.5875500);
    path_41.lineTo(size.width * 0.6911115, size.height * 0.5970953);
    path_41.lineTo(size.width * 0.9371007, size.height * 0.5566813);
    path_41.cubicTo(
        size.width * 0.9377482,
        size.height * 0.5565766,
        size.width * 0.9383741,
        size.height * 0.5563328,
        size.width * 0.9389424,
        size.height * 0.5559641);
    path_41.cubicTo(
        size.width * 0.9395036,
        size.height * 0.5555953,
        size.width * 0.9399928,
        size.height * 0.5551094,
        size.width * 0.9403885,
        size.height * 0.5545336);
    path_41.cubicTo(
        size.width * 0.9407770,
        size.height * 0.5539578,
        size.width * 0.9410576,
        size.height * 0.5533039,
        size.width * 0.9412158,
        size.height * 0.5526094);
    path_41.cubicTo(
        size.width * 0.9413741,
        size.height * 0.5519148,
        size.width * 0.9414029,
        size.height * 0.5511938,
        size.width * 0.9413022,
        size.height * 0.5504867);
    path_41.cubicTo(
        size.width * 0.9412014,
        size.height * 0.5497797,
        size.width * 0.9409784,
        size.height * 0.5491016,
        size.width * 0.9406331,
        size.height * 0.5484898);
    path_41.cubicTo(
        size.width * 0.9402878,
        size.height * 0.5478789,
        size.width * 0.9398417,
        size.height * 0.5473477,
        size.width * 0.9393094,
        size.height * 0.5469258);
    path_41.cubicTo(
        size.width * 0.9387770,
        size.height * 0.5465047,
        size.width * 0.9381727,
        size.height * 0.5462016,
        size.width * 0.9375324,
        size.height * 0.5460344);
    path_41.cubicTo(
        size.width * 0.9368921,
        size.height * 0.5458664,
        size.width * 0.9362302,
        size.height * 0.5458383,
        size.width * 0.9355755,
        size.height * 0.5459508);
    path_41.lineTo(size.width * 0.9355755, size.height * 0.5459508);
    path_41.close();

    Paint paint_41_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_41_stroke.color = Colors.black;
    canvas.drawPath(path_41, paint_41_stroke);

    Paint paint_41_fill = Paint()..style = PaintingStyle.fill;
    paint_41_fill.color = Colors.white;
    canvas.drawPath(path_41, paint_41_fill);

    Path path_42 = Path();
    path_42.moveTo(size.width * 0.7263597, size.height * 0.6341602);
    path_42.lineTo(size.width * 0.6928640, size.height * 0.6267797);
    path_42.lineTo(size.width * 0.6928640, size.height * 0.5060648);
    path_42.lineTo(size.width * 0.6814245, size.height * 0.4907000);
    path_42.lineTo(size.width * 0.6814245, size.height * 0.6253125);
    path_42.lineTo(size.width * 0.6475460, size.height * 0.6196063);
    path_42.cubicTo(
        size.width * 0.6461921,
        size.height * 0.6193812,
        size.width * 0.6448101,
        size.height * 0.6197344,
        size.width * 0.6436906,
        size.height * 0.6205914);
    path_42.cubicTo(
        size.width * 0.6425712,
        size.height * 0.6214484,
        size.width * 0.6418007,
        size.height * 0.6227430,
        size.width * 0.6415410,
        size.height * 0.6242039);
    path_42.lineTo(size.width * 0.6415072, size.height * 0.6243953);
    path_42.cubicTo(
        size.width * 0.6413741,
        size.height * 0.6251484,
        size.width * 0.6413806,
        size.height * 0.6259227,
        size.width * 0.6415266,
        size.height * 0.6266734);
    path_42.cubicTo(
        size.width * 0.6416727,
        size.height * 0.6274234,
        size.width * 0.6419554,
        size.height * 0.6281344,
        size.width * 0.6423583,
        size.height * 0.6287641);
    path_42.cubicTo(
        size.width * 0.6427612,
        size.height * 0.6293945,
        size.width * 0.6432763,
        size.height * 0.6299305,
        size.width * 0.6438719,
        size.height * 0.6303414);
    path_42.cubicTo(
        size.width * 0.6444683,
        size.height * 0.6307523,
        size.width * 0.6451345,
        size.height * 0.6310289,
        size.width * 0.6458309,
        size.height * 0.6311570);
    path_42.lineTo(size.width * 0.7244317, size.height * 0.6454742);
    path_42.cubicTo(
        size.width * 0.7258129,
        size.height * 0.6457523,
        size.width * 0.7272446,
        size.height * 0.6454227,
        size.width * 0.7284029,
        size.height * 0.6445586);
    path_42.cubicTo(
        size.width * 0.7295540,
        size.height * 0.6436945,
        size.width * 0.7303525,
        size.height * 0.6423664,
        size.width * 0.7306043,
        size.height * 0.6408664);
    path_42.cubicTo(
        size.width * 0.7308633,
        size.height * 0.6393672,
        size.width * 0.7305612,
        size.height * 0.6378180,
        size.width * 0.7297626,
        size.height * 0.6365617);
    path_42.cubicTo(
        size.width * 0.7289712,
        size.height * 0.6353047,
        size.width * 0.7277482,
        size.height * 0.6344422,
        size.width * 0.7263669,
        size.height * 0.6341648);
    path_42.lineTo(size.width * 0.7263597, size.height * 0.6341602);
    path_42.close();

    Paint paint_42_fill = Paint()..style = PaintingStyle.fill;
    paint_42_fill.color = Colors.black;
    canvas.drawPath(path_42, paint_42_fill);

    Path path_43 = Path();
    path_43.moveTo(size.width * 0.7263597, size.height * 0.6341602);
    path_43.lineTo(size.width * 0.6928640, size.height * 0.6267797);
    path_43.lineTo(size.width * 0.6928640, size.height * 0.5060648);
    path_43.lineTo(size.width * 0.6814245, size.height * 0.4907000);
    path_43.lineTo(size.width * 0.6814245, size.height * 0.6253125);
    path_43.lineTo(size.width * 0.6475460, size.height * 0.6196063);
    path_43.cubicTo(
        size.width * 0.6461921,
        size.height * 0.6193812,
        size.width * 0.6448101,
        size.height * 0.6197344,
        size.width * 0.6436906,
        size.height * 0.6205914);
    path_43.cubicTo(
        size.width * 0.6425712,
        size.height * 0.6214484,
        size.width * 0.6418007,
        size.height * 0.6227430,
        size.width * 0.6415410,
        size.height * 0.6242039);
    path_43.lineTo(size.width * 0.6415072, size.height * 0.6243953);
    path_43.cubicTo(
        size.width * 0.6413741,
        size.height * 0.6251484,
        size.width * 0.6413806,
        size.height * 0.6259227,
        size.width * 0.6415266,
        size.height * 0.6266734);
    path_43.cubicTo(
        size.width * 0.6416727,
        size.height * 0.6274234,
        size.width * 0.6419554,
        size.height * 0.6281344,
        size.width * 0.6423583,
        size.height * 0.6287641);
    path_43.cubicTo(
        size.width * 0.6427612,
        size.height * 0.6293945,
        size.width * 0.6432763,
        size.height * 0.6299305,
        size.width * 0.6438719,
        size.height * 0.6303414);
    path_43.cubicTo(
        size.width * 0.6444683,
        size.height * 0.6307523,
        size.width * 0.6451345,
        size.height * 0.6310289,
        size.width * 0.6458309,
        size.height * 0.6311570);
    path_43.lineTo(size.width * 0.7244317, size.height * 0.6454742);
    path_43.cubicTo(
        size.width * 0.7258129,
        size.height * 0.6457523,
        size.width * 0.7272446,
        size.height * 0.6454227,
        size.width * 0.7284029,
        size.height * 0.6445586);
    path_43.cubicTo(
        size.width * 0.7295540,
        size.height * 0.6436945,
        size.width * 0.7303525,
        size.height * 0.6423664,
        size.width * 0.7306043,
        size.height * 0.6408664);
    path_43.cubicTo(
        size.width * 0.7308633,
        size.height * 0.6393672,
        size.width * 0.7305612,
        size.height * 0.6378180,
        size.width * 0.7297626,
        size.height * 0.6365617);
    path_43.cubicTo(
        size.width * 0.7289712,
        size.height * 0.6353047,
        size.width * 0.7277482,
        size.height * 0.6344422,
        size.width * 0.7263669,
        size.height * 0.6341648);
    path_43.lineTo(size.width * 0.7263597, size.height * 0.6341602);
    path_43.close();

    Paint paint_43_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_43_stroke.color = Colors.black;
    canvas.drawPath(path_43, paint_43_stroke);

    Paint paint_43_fill = Paint()..style = PaintingStyle.fill;
    paint_43_fill.color = Colors.white;
    canvas.drawPath(path_43, paint_43_fill);

    Path path_44 = Path();
    path_44.moveTo(size.width * 0.6266302, size.height * 0.4850117);
    path_44.lineTo(size.width * 0.6266302, size.height * 0.5043953);
    path_44.lineTo(size.width * 0.7169209, size.height * 0.5094125);
    path_44.lineTo(size.width * 0.9881799, size.height * 0.4730742);
    path_44.lineTo(size.width * 0.9881799, size.height * 0.4543555);
    path_44.lineTo(size.width * 0.9494820, size.height * 0.4510500);
    path_44.cubicTo(
        size.width * 0.9494820,
        size.height * 0.4510500,
        size.width * 0.6620777,
        size.height * 0.4628117,
        size.width * 0.6266302,
        size.height * 0.4850117);
    path_44.close();

    Paint paint_44_fill = Paint()..style = PaintingStyle.fill;
    paint_44_fill.color = Colors.black;
    canvas.drawPath(path_44, paint_44_fill);

    Path path_45 = Path();
    path_45.moveTo(size.width * 0.6266302, size.height * 0.4850117);
    path_45.lineTo(size.width * 0.6266302, size.height * 0.5043953);
    path_45.lineTo(size.width * 0.7169209, size.height * 0.5094125);
    path_45.lineTo(size.width * 0.9881799, size.height * 0.4730742);
    path_45.lineTo(size.width * 0.9881799, size.height * 0.4543555);
    path_45.lineTo(size.width * 0.9494820, size.height * 0.4510500);
    path_45.cubicTo(
        size.width * 0.9494820,
        size.height * 0.4510500,
        size.width * 0.6620777,
        size.height * 0.4628117,
        size.width * 0.6266302,
        size.height * 0.4850117);
    path_45.close();

    Paint paint_45_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_45_stroke.color = Colors.black;
    canvas.drawPath(path_45, paint_45_stroke);

    Paint paint_45_fill = Paint()..style = PaintingStyle.fill;
    paint_45_fill.color = Colors.white;
    canvas.drawPath(path_45, paint_45_fill);

    Path path_46 = Path();
    path_46.moveTo(size.width * 0.6208827, size.height * 0.4850219);
    path_46.lineTo(size.width * 0.6208827, size.height * 0.5163430);
    path_46.lineTo(size.width * 0.7141763, size.height * 0.5290711);
    path_46.lineTo(size.width * 0.9954101, size.height * 0.4907039);
    path_46.lineTo(size.width * 0.9981151, size.height * 0.4536969);
    path_46.lineTo(size.width * 0.7204892, size.height * 0.4907039);
    path_46.lineTo(size.width * 0.6208827, size.height * 0.4850219);
    path_46.close();

    Paint paint_46_fill = Paint()..style = PaintingStyle.fill;
    paint_46_fill.color = Colors.black;
    canvas.drawPath(path_46, paint_46_fill);

    Path path_47 = Path();
    path_47.moveTo(size.width * 0.6208827, size.height * 0.4850219);
    path_47.lineTo(size.width * 0.6208827, size.height * 0.5163430);
    path_47.lineTo(size.width * 0.7141763, size.height * 0.5290711);
    path_47.lineTo(size.width * 0.9954101, size.height * 0.4907039);
    path_47.lineTo(size.width * 0.9981151, size.height * 0.4536969);
    path_47.lineTo(size.width * 0.7204892, size.height * 0.4907039);
    path_47.lineTo(size.width * 0.6208827, size.height * 0.4850219);
    path_47.close();

    Paint paint_47_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_47_stroke.color = Colors.black;
    canvas.drawPath(path_47, paint_47_stroke);

    Paint paint_47_fill = Paint()..style = PaintingStyle.fill;
    paint_47_fill.color = Colors.white;
    canvas.drawPath(path_47, paint_47_fill);

    Path path_48 = Path();
    path_48.moveTo(size.width * 0.7135022, size.height * 0.07781930);
    path_48.cubicTo(
        size.width * 0.7288129,
        size.height * 0.07781930,
        size.width * 0.7412158,
        size.height * 0.06039883,
        size.width * 0.7412158,
        size.height * 0.03890961);
    path_48.cubicTo(size.width * 0.7412158, size.height * 0.01742047,
        size.width * 0.7288129, 0, size.width * 0.7135022, 0);
    path_48.cubicTo(
        size.width * 0.6981950,
        0,
        size.width * 0.6857856,
        size.height * 0.01742047,
        size.width * 0.6857856,
        size.height * 0.03890961);
    path_48.cubicTo(
        size.width * 0.6857856,
        size.height * 0.06039883,
        size.width * 0.6981950,
        size.height * 0.07781930,
        size.width * 0.7135022,
        size.height * 0.07781930);
    path_48.close();

    Paint paint_48_fill = Paint()..style = PaintingStyle.fill;
    paint_48_fill.color = Colors.black;
    canvas.drawPath(path_48, paint_48_fill);

    Path path_49 = Path();
    path_49.moveTo(size.width * 0.7135022, size.height * 0.07781930);
    path_49.cubicTo(
        size.width * 0.7288129,
        size.height * 0.07781930,
        size.width * 0.7412158,
        size.height * 0.06039883,
        size.width * 0.7412158,
        size.height * 0.03890961);
    path_49.cubicTo(size.width * 0.7412158, size.height * 0.01742047,
        size.width * 0.7288129, 0, size.width * 0.7135022, 0);
    path_49.cubicTo(
        size.width * 0.6981950,
        0,
        size.width * 0.6857856,
        size.height * 0.01742047,
        size.width * 0.6857856,
        size.height * 0.03890961);
    path_49.cubicTo(
        size.width * 0.6857856,
        size.height * 0.06039883,
        size.width * 0.6981950,
        size.height * 0.07781930,
        size.width * 0.7135022,
        size.height * 0.07781930);
    path_49.close();

    Paint paint_49_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_49_stroke.color = Colors.black;
    canvas.drawPath(path_49, paint_49_stroke);

    Paint paint_49_fill = Paint()..style = PaintingStyle.fill;
    paint_49_fill.color = Colors.white;
    canvas.drawPath(path_49, paint_49_fill);

    Path path_50 = Path();
    path_50.moveTo(size.width * 0.7231799, size.height * 0.07781930);
    path_50.cubicTo(
        size.width * 0.7384892,
        size.height * 0.07781930,
        size.width * 0.7508993,
        size.height * 0.06039883,
        size.width * 0.7508993,
        size.height * 0.03890961);
    path_50.cubicTo(size.width * 0.7508993, size.height * 0.01742047,
        size.width * 0.7384892, 0, size.width * 0.7231799, 0);
    path_50.cubicTo(
        size.width * 0.7078763,
        0,
        size.width * 0.6954669,
        size.height * 0.01742047,
        size.width * 0.6954669,
        size.height * 0.03890961);
    path_50.cubicTo(
        size.width * 0.6954669,
        size.height * 0.06039883,
        size.width * 0.7078763,
        size.height * 0.07781930,
        size.width * 0.7231799,
        size.height * 0.07781930);
    path_50.close();

    Paint paint_50_fill = Paint()..style = PaintingStyle.fill;
    paint_50_fill.color = Colors.black;
    canvas.drawPath(path_50, paint_50_fill);

    Path path_51 = Path();
    path_51.moveTo(size.width * 0.7231799, size.height * 0.07781930);
    path_51.cubicTo(
        size.width * 0.7384892,
        size.height * 0.07781930,
        size.width * 0.7508993,
        size.height * 0.06039883,
        size.width * 0.7508993,
        size.height * 0.03890961);
    path_51.cubicTo(size.width * 0.7508993, size.height * 0.01742047,
        size.width * 0.7384892, 0, size.width * 0.7231799, 0);
    path_51.cubicTo(
        size.width * 0.7078763,
        0,
        size.width * 0.6954669,
        size.height * 0.01742047,
        size.width * 0.6954669,
        size.height * 0.03890961);
    path_51.cubicTo(
        size.width * 0.6954669,
        size.height * 0.06039883,
        size.width * 0.7078763,
        size.height * 0.07781930,
        size.width * 0.7231799,
        size.height * 0.07781930);
    path_51.close();

    Paint paint_51_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_51_stroke.color = Colors.black;
    canvas.drawPath(path_51, paint_51_stroke);

    Paint paint_51_fill = Paint()..style = PaintingStyle.fill;
    paint_51_fill.color = Colors.white;
    canvas.drawPath(path_51, paint_51_fill);

    Path path_52 = Path();
    path_52.moveTo(size.width * 0.7785971, size.height * 0.2151633);
    path_52.cubicTo(
        size.width * 0.7799640,
        size.height * 0.2428000,
        size.width * 0.7777554,
        size.height * 0.2804055,
        size.width * 0.7614101,
        size.height * 0.2784328);
    path_52.cubicTo(
        size.width * 0.7450647,
        size.height * 0.2764602,
        size.width * 0.7366115,
        size.height * 0.2286250,
        size.width * 0.7327842,
        size.height * 0.2059523);
    path_52.cubicTo(
        size.width * 0.7289568,
        size.height * 0.1832797,
        size.width * 0.7306978,
        size.height * 0.06775328,
        size.width * 0.7306978,
        size.height * 0.06775328);
    path_52.lineTo(size.width * 0.7321511, size.height * 0.02870102);
    path_52.cubicTo(
        size.width * 0.7321511,
        size.height * 0.02870102,
        size.width * 0.7503741,
        size.height * 0.02764141,
        size.width * 0.7543957,
        size.height * 0.03365695);
    path_52.cubicTo(
        size.width * 0.7551439,
        size.height * 0.03036078,
        size.width * 0.7566187,
        size.height * 0.02731156,
        size.width * 0.7586906,
        size.height * 0.02477219);
    path_52.cubicTo(
        size.width * 0.7590000,
        size.height * 0.02440633,
        size.width * 0.7594029,
        size.height * 0.02414984,
        size.width * 0.7598417,
        size.height * 0.02403656);
    path_52.cubicTo(
        size.width * 0.7602878,
        size.height * 0.02392328,
        size.width * 0.7607482,
        size.height * 0.02395859,
        size.width * 0.7611727,
        size.height * 0.02413781);
    path_52.cubicTo(
        size.width * 0.7615971,
        size.height * 0.02431703,
        size.width * 0.7619640,
        size.height * 0.02463172,
        size.width * 0.7622230,
        size.height * 0.02504039);
    path_52.cubicTo(
        size.width * 0.7624748,
        size.height * 0.02544898,
        size.width * 0.7626115,
        size.height * 0.02593242,
        size.width * 0.7626115,
        size.height * 0.02642687);
    path_52.cubicTo(
        size.width * 0.7626259,
        size.height * 0.03183109,
        size.width * 0.7626187,
        size.height * 0.04054469,
        size.width * 0.7625108,
        size.height * 0.04715125);
    path_52.cubicTo(
        size.width * 0.7623453,
        size.height * 0.05745430,
        size.width * 0.7509496,
        size.height * 0.06657141,
        size.width * 0.7509496,
        size.height * 0.06657141);
    path_52.cubicTo(
        size.width * 0.7509496,
        size.height * 0.06657141,
        size.width * 0.7557842,
        size.height * 0.08881797,
        size.width * 0.7614101,
        size.height * 0.1164094);

    Paint paint_52_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461598;
    paint_52_stroke.color = Colors.black;
    canvas.drawPath(path_52, paint_52_stroke);

    Paint paint_52_fill = Paint()..style = PaintingStyle.fill;
    paint_52_fill.color = Colors.white;
    canvas.drawPath(path_52, paint_52_fill);

    Path path_53 = Path();
    path_53.moveTo(size.width * 0.7368201, size.height * 0.03548102);
    path_53.cubicTo(
        size.width * 0.7429784,
        size.height * 0.03548102,
        size.width * 0.7479712,
        size.height * 0.03290453,
        size.width * 0.7479712,
        size.height * 0.02972633);
    path_53.cubicTo(
        size.width * 0.7479712,
        size.height * 0.02654805,
        size.width * 0.7429784,
        size.height * 0.02397156,
        size.width * 0.7368201,
        size.height * 0.02397156);
    path_53.cubicTo(
        size.width * 0.7306547,
        size.height * 0.02397156,
        size.width * 0.7256619,
        size.height * 0.02654805,
        size.width * 0.7256619,
        size.height * 0.02972633);
    path_53.cubicTo(
        size.width * 0.7256619,
        size.height * 0.03290453,
        size.width * 0.7306547,
        size.height * 0.03548102,
        size.width * 0.7368201,
        size.height * 0.03548102);
    path_53.close();

    Paint paint_53_fill = Paint()..style = PaintingStyle.fill;
    paint_53_fill.color = Colors.black;
    canvas.drawPath(path_53, paint_53_fill);

    Path path_54 = Path();
    path_54.moveTo(size.width * 0.7368201, size.height * 0.03548102);
    path_54.cubicTo(
        size.width * 0.7429784,
        size.height * 0.03548102,
        size.width * 0.7479712,
        size.height * 0.03290453,
        size.width * 0.7479712,
        size.height * 0.02972633);
    path_54.cubicTo(
        size.width * 0.7479712,
        size.height * 0.02654805,
        size.width * 0.7429784,
        size.height * 0.02397156,
        size.width * 0.7368201,
        size.height * 0.02397156);
    path_54.cubicTo(
        size.width * 0.7306547,
        size.height * 0.02397156,
        size.width * 0.7256619,
        size.height * 0.02654805,
        size.width * 0.7256619,
        size.height * 0.02972633);
    path_54.cubicTo(
        size.width * 0.7256619,
        size.height * 0.03290453,
        size.width * 0.7306547,
        size.height * 0.03548102,
        size.width * 0.7368201,
        size.height * 0.03548102);
    path_54.close();

    Paint paint_54_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_54_stroke.color = Colors.black;
    canvas.drawPath(path_54, paint_54_stroke);

    Paint paint_54_fill = Paint()..style = PaintingStyle.fill;
    paint_54_fill.color = Colors.white;
    canvas.drawPath(path_54, paint_54_fill);

    Path path_55 = Path();
    path_55.moveTo(size.width * 0.7686978, size.height * 0.07781930);
    path_55.cubicTo(
        size.width * 0.7840000,
        size.height * 0.07781930,
        size.width * 0.7964101,
        size.height * 0.06039883,
        size.width * 0.7964101,
        size.height * 0.03890961);
    path_55.cubicTo(size.width * 0.7964101, size.height * 0.01742047,
        size.width * 0.7840000, 0, size.width * 0.7686978, 0);
    path_55.cubicTo(
        size.width * 0.7533885,
        0,
        size.width * 0.7409784,
        size.height * 0.01742047,
        size.width * 0.7409784,
        size.height * 0.03890961);
    path_55.cubicTo(
        size.width * 0.7409784,
        size.height * 0.06039883,
        size.width * 0.7533885,
        size.height * 0.07781930,
        size.width * 0.7686978,
        size.height * 0.07781930);
    path_55.close();

    Paint paint_55_fill = Paint()..style = PaintingStyle.fill;
    paint_55_fill.color = Colors.black;
    canvas.drawPath(path_55, paint_55_fill);

    Path path_56 = Path();
    path_56.moveTo(size.width * 0.7686978, size.height * 0.07781930);
    path_56.cubicTo(
        size.width * 0.7840000,
        size.height * 0.07781930,
        size.width * 0.7964101,
        size.height * 0.06039883,
        size.width * 0.7964101,
        size.height * 0.03890961);
    path_56.cubicTo(size.width * 0.7964101, size.height * 0.01742047,
        size.width * 0.7840000, 0, size.width * 0.7686978, 0);
    path_56.cubicTo(
        size.width * 0.7533885,
        0,
        size.width * 0.7409784,
        size.height * 0.01742047,
        size.width * 0.7409784,
        size.height * 0.03890961);
    path_56.cubicTo(
        size.width * 0.7409784,
        size.height * 0.06039883,
        size.width * 0.7533885,
        size.height * 0.07781930,
        size.width * 0.7686978,
        size.height * 0.07781930);
    path_56.close();

    Paint paint_56_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_56_stroke.color = Colors.black;
    canvas.drawPath(path_56, paint_56_stroke);

    Paint paint_56_fill = Paint()..style = PaintingStyle.fill;
    paint_56_fill.color = Colors.white;
    canvas.drawPath(path_56, paint_56_fill);

    Path path_57 = Path();
    path_57.moveTo(size.width * 0.7783885, size.height * 0.07781930);
    path_57.cubicTo(
        size.width * 0.7936978,
        size.height * 0.07781930,
        size.width * 0.8061079,
        size.height * 0.06039883,
        size.width * 0.8061079,
        size.height * 0.03890961);
    path_57.cubicTo(size.width * 0.8061079, size.height * 0.01742047,
        size.width * 0.7936978, 0, size.width * 0.7783885, 0);
    path_57.cubicTo(
        size.width * 0.7630863,
        0,
        size.width * 0.7506763,
        size.height * 0.01742047,
        size.width * 0.7506763,
        size.height * 0.03890961);
    path_57.cubicTo(
        size.width * 0.7506763,
        size.height * 0.06039883,
        size.width * 0.7630863,
        size.height * 0.07781930,
        size.width * 0.7783885,
        size.height * 0.07781930);
    path_57.close();

    Paint paint_57_fill = Paint()..style = PaintingStyle.fill;
    paint_57_fill.color = Colors.black;
    canvas.drawPath(path_57, paint_57_fill);

    Path path_58 = Path();
    path_58.moveTo(size.width * 0.7783885, size.height * 0.07781930);
    path_58.cubicTo(
        size.width * 0.7936978,
        size.height * 0.07781930,
        size.width * 0.8061079,
        size.height * 0.06039883,
        size.width * 0.8061079,
        size.height * 0.03890961);
    path_58.cubicTo(size.width * 0.8061079, size.height * 0.01742047,
        size.width * 0.7936978, 0, size.width * 0.7783885, 0);
    path_58.cubicTo(
        size.width * 0.7630863,
        0,
        size.width * 0.7506763,
        size.height * 0.01742047,
        size.width * 0.7506763,
        size.height * 0.03890961);
    path_58.cubicTo(
        size.width * 0.7506763,
        size.height * 0.06039883,
        size.width * 0.7630863,
        size.height * 0.07781930,
        size.width * 0.7783885,
        size.height * 0.07781930);
    path_58.close();

    Paint paint_58_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_58_stroke.color = Colors.black;
    canvas.drawPath(path_58, paint_58_stroke);

    Paint paint_58_fill = Paint()..style = PaintingStyle.fill;
    paint_58_fill.color = Colors.white;
    canvas.drawPath(path_58, paint_58_fill);

    Path path_59 = Path();
    path_59.moveTo(size.width * 0.7841583, size.height * 0.02764891);
    path_59.cubicTo(
        size.width * 0.7797266,
        size.height * 0.02764891,
        size.width * 0.7761367,
        size.height * 0.03269047,
        size.width * 0.7761367,
        size.height * 0.03890570);
    path_59.cubicTo(
        size.width * 0.7761367,
        size.height * 0.04512102,
        size.width * 0.7797266,
        size.height * 0.05016250,
        size.width * 0.7841583,
        size.height * 0.05016250);
    path_59.cubicTo(
        size.width * 0.7885827,
        size.height * 0.05016250,
        size.width * 0.7921727,
        size.height * 0.04512508,
        size.width * 0.7921727,
        size.height * 0.03890570);
    path_59.cubicTo(
        size.width * 0.7921727,
        size.height * 0.03268641,
        size.width * 0.7885827,
        size.height * 0.02764891,
        size.width * 0.7841583,
        size.height * 0.02764891);
    path_59.close();

    Paint paint_59_fill = Paint()..style = PaintingStyle.fill;
    paint_59_fill.color = Colors.white;
    canvas.drawPath(path_59, paint_59_fill);

    Path path_60 = Path();
    path_60.moveTo(size.width * 0.7841583, size.height * 0.02684023);
    path_60.cubicTo(
        size.width * 0.7793309,
        size.height * 0.02684023,
        size.width * 0.7753885,
        size.height * 0.03225672,
        size.width * 0.7753885,
        size.height * 0.03891211);
    path_60.cubicTo(
        size.width * 0.7753885,
        size.height * 0.04556758,
        size.width * 0.7793309,
        size.height * 0.05098398,
        size.width * 0.7841583,
        size.height * 0.05098398);
    path_60.cubicTo(
        size.width * 0.7889928,
        size.height * 0.05098398,
        size.width * 0.7929353,
        size.height * 0.04557164,
        size.width * 0.7929353,
        size.height * 0.03891211);
    path_60.cubicTo(
        size.width * 0.7929353,
        size.height * 0.03225258,
        size.width * 0.7890000,
        size.height * 0.02684023,
        size.width * 0.7841583,
        size.height * 0.02684023);
    path_60.close();
    path_60.moveTo(size.width * 0.7841583, size.height * 0.04935375);
    path_60.cubicTo(
        size.width * 0.7801511,
        size.height * 0.04935375,
        size.width * 0.7768921,
        size.height * 0.04467094,
        size.width * 0.7768921,
        size.height * 0.03891211);
    path_60.cubicTo(
        size.width * 0.7768921,
        size.height * 0.03315328,
        size.width * 0.7801511,
        size.height * 0.02847047,
        size.width * 0.7841583,
        size.height * 0.02847047);
    path_60.cubicTo(
        size.width * 0.7881727,
        size.height * 0.02847047,
        size.width * 0.7914317,
        size.height * 0.03315742,
        size.width * 0.7914317,
        size.height * 0.03891211);
    path_60.cubicTo(
        size.width * 0.7914317,
        size.height * 0.04466688,
        size.width * 0.7881727,
        size.height * 0.04935375,
        size.width * 0.7841583,
        size.height * 0.04935375);
    path_60.lineTo(size.width * 0.7841583, size.height * 0.04935375);
    path_60.close();

    Paint paint_60_fill = Paint()..style = PaintingStyle.fill;
    paint_60_fill.color = Colors.black;
    canvas.drawPath(path_60, paint_60_fill);

    Path path_61 = Path();
    path_61.moveTo(size.width * 0.7884388, size.height * 0.02764891);
    path_61.cubicTo(
        size.width * 0.7840144,
        size.height * 0.02764891,
        size.width * 0.7804245,
        size.height * 0.03269047,
        size.width * 0.7804245,
        size.height * 0.03890570);
    path_61.cubicTo(
        size.width * 0.7804245,
        size.height * 0.04512102,
        size.width * 0.7840144,
        size.height * 0.05016250,
        size.width * 0.7884388,
        size.height * 0.05016250);
    path_61.cubicTo(
        size.width * 0.7928705,
        size.height * 0.05016250,
        size.width * 0.7964604,
        size.height * 0.04512508,
        size.width * 0.7964604,
        size.height * 0.03890570);
    path_61.cubicTo(
        size.width * 0.7964604,
        size.height * 0.03268641,
        size.width * 0.7928705,
        size.height * 0.02764891,
        size.width * 0.7884388,
        size.height * 0.02764891);
    path_61.close();

    Paint paint_61_fill = Paint()..style = PaintingStyle.fill;
    paint_61_fill.color = Colors.white;
    canvas.drawPath(path_61, paint_61_fill);

    Path path_62 = Path();
    path_62.moveTo(size.width * 0.7884460, size.height * 0.02684023);
    path_62.cubicTo(
        size.width * 0.7836115,
        size.height * 0.02684023,
        size.width * 0.7796763,
        size.height * 0.03225672,
        size.width * 0.7796763,
        size.height * 0.03891211);
    path_62.cubicTo(
        size.width * 0.7796763,
        size.height * 0.04556758,
        size.width * 0.7836115,
        size.height * 0.05098398,
        size.width * 0.7884460,
        size.height * 0.05098398);
    path_62.cubicTo(
        size.width * 0.7932878,
        size.height * 0.05098398,
        size.width * 0.7972158,
        size.height * 0.04557164,
        size.width * 0.7972158,
        size.height * 0.03891211);
    path_62.cubicTo(
        size.width * 0.7972158,
        size.height * 0.03225258,
        size.width * 0.7932806,
        size.height * 0.02684023,
        size.width * 0.7884460,
        size.height * 0.02684023);
    path_62.close();
    path_62.moveTo(size.width * 0.7884460, size.height * 0.04935375);
    path_62.cubicTo(
        size.width * 0.7844388,
        size.height * 0.04935375,
        size.width * 0.7811799,
        size.height * 0.04467094,
        size.width * 0.7811799,
        size.height * 0.03891211);
    path_62.cubicTo(
        size.width * 0.7811799,
        size.height * 0.03315328,
        size.width * 0.7844388,
        size.height * 0.02847047,
        size.width * 0.7884460,
        size.height * 0.02847047);
    path_62.cubicTo(
        size.width * 0.7924532,
        size.height * 0.02847047,
        size.width * 0.7957194,
        size.height * 0.03315742,
        size.width * 0.7957194,
        size.height * 0.03891211);
    path_62.cubicTo(
        size.width * 0.7957194,
        size.height * 0.04466688,
        size.width * 0.7924532,
        size.height * 0.04935375,
        size.width * 0.7884460,
        size.height * 0.04935375);
    path_62.lineTo(size.width * 0.7884460, size.height * 0.04935375);
    path_62.close();

    Paint paint_62_fill = Paint()..style = PaintingStyle.fill;
    paint_62_fill.color = Colors.black;
    canvas.drawPath(path_62, paint_62_fill);

    Path path_63 = Path();
    path_63.moveTo(size.width * 0.8047626, size.height * 0.07781930);
    path_63.cubicTo(
        size.width * 0.8200719,
        size.height * 0.07781930,
        size.width * 0.8324820,
        size.height * 0.06039883,
        size.width * 0.8324820,
        size.height * 0.03890961);
    path_63.cubicTo(size.width * 0.8324820, size.height * 0.01742047,
        size.width * 0.8200719, 0, size.width * 0.8047626, 0);
    path_63.cubicTo(
        size.width * 0.7894604,
        0,
        size.width * 0.7770504,
        size.height * 0.01742047,
        size.width * 0.7770504,
        size.height * 0.03890961);
    path_63.cubicTo(
        size.width * 0.7770504,
        size.height * 0.06039883,
        size.width * 0.7894604,
        size.height * 0.07781930,
        size.width * 0.8047626,
        size.height * 0.07781930);
    path_63.close();

    Paint paint_63_fill = Paint()..style = PaintingStyle.fill;
    paint_63_fill.color = Colors.black;
    canvas.drawPath(path_63, paint_63_fill);

    Path path_64 = Path();
    path_64.moveTo(size.width * 0.8047626, size.height * 0.07781930);
    path_64.cubicTo(
        size.width * 0.8200719,
        size.height * 0.07781930,
        size.width * 0.8324820,
        size.height * 0.06039883,
        size.width * 0.8324820,
        size.height * 0.03890961);
    path_64.cubicTo(size.width * 0.8324820, size.height * 0.01742047,
        size.width * 0.8200719, 0, size.width * 0.8047626, 0);
    path_64.cubicTo(
        size.width * 0.7894604,
        0,
        size.width * 0.7770504,
        size.height * 0.01742047,
        size.width * 0.7770504,
        size.height * 0.03890961);
    path_64.cubicTo(
        size.width * 0.7770504,
        size.height * 0.06039883,
        size.width * 0.7894604,
        size.height * 0.07781930,
        size.width * 0.8047626,
        size.height * 0.07781930);
    path_64.close();

    Paint paint_64_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_64_stroke.color = Colors.black;
    canvas.drawPath(path_64, paint_64_stroke);

    Paint paint_64_fill = Paint()..style = PaintingStyle.fill;
    paint_64_fill.color = Colors.white;
    canvas.drawPath(path_64, paint_64_fill);

    Path path_65 = Path();
    path_65.moveTo(size.width * 0.8144460, size.height * 0.07781930);
    path_65.cubicTo(
        size.width * 0.8297554,
        size.height * 0.07781930,
        size.width * 0.8421655,
        size.height * 0.06039883,
        size.width * 0.8421655,
        size.height * 0.03890961);
    path_65.cubicTo(size.width * 0.8421655, size.height * 0.01742047,
        size.width * 0.8297554, 0, size.width * 0.8144460, 0);
    path_65.cubicTo(
        size.width * 0.7991367,
        0,
        size.width * 0.7867266,
        size.height * 0.01742047,
        size.width * 0.7867266,
        size.height * 0.03890961);
    path_65.cubicTo(
        size.width * 0.7867266,
        size.height * 0.06039883,
        size.width * 0.7991367,
        size.height * 0.07781930,
        size.width * 0.8144460,
        size.height * 0.07781930);
    path_65.close();

    Paint paint_65_fill = Paint()..style = PaintingStyle.fill;
    paint_65_fill.color = Colors.black;
    canvas.drawPath(path_65, paint_65_fill);

    Path path_66 = Path();
    path_66.moveTo(size.width * 0.8144460, size.height * 0.07781930);
    path_66.cubicTo(
        size.width * 0.8297554,
        size.height * 0.07781930,
        size.width * 0.8421655,
        size.height * 0.06039883,
        size.width * 0.8421655,
        size.height * 0.03890961);
    path_66.cubicTo(size.width * 0.8421655, size.height * 0.01742047,
        size.width * 0.8297554, 0, size.width * 0.8144460, 0);
    path_66.cubicTo(
        size.width * 0.7991367,
        0,
        size.width * 0.7867266,
        size.height * 0.01742047,
        size.width * 0.7867266,
        size.height * 0.03890961);
    path_66.cubicTo(
        size.width * 0.7867266,
        size.height * 0.06039883,
        size.width * 0.7991367,
        size.height * 0.07781930,
        size.width * 0.8144460,
        size.height * 0.07781930);
    path_66.close();

    Paint paint_66_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_66_stroke.color = Colors.black;
    canvas.drawPath(path_66, paint_66_stroke);

    Paint paint_66_fill = Paint()..style = PaintingStyle.fill;
    paint_66_fill.color = Colors.white;
    canvas.drawPath(path_66, paint_66_fill);

    Path path_67 = Path();
    path_67.moveTo(size.width * 0.8250432, size.height * 0.3856586);
    path_67.lineTo(size.width * 0.7602230, size.height * 0.3959937);
    path_67.cubicTo(
        size.width * 0.7602230,
        size.height * 0.3959937,
        size.width * 0.7159705,
        size.height * 0.4160008,
        size.width * 0.6974906,
        size.height * 0.4150227);
    path_67.cubicTo(
        size.width * 0.7024482,
        size.height * 0.4277508,
        size.width * 0.7037993,
        size.height * 0.4644555,
        size.width * 0.7037993,
        size.height * 0.4644555);
    path_67.lineTo(size.width * 0.7602230, size.height * 0.4644555);
    path_67.lineTo(size.width * 0.7602230, size.height * 0.4850133);
    path_67.lineTo(size.width * 0.8111799, size.height * 0.4825672);
    path_67.cubicTo(
        size.width * 0.8111799,
        size.height * 0.4825672,
        size.width * 0.8372086,
        size.height * 0.4669094,
        size.width * 0.8354029,
        size.height * 0.4360734);
    path_67.cubicTo(
        size.width * 0.8336043,
        size.height * 0.4052375,
        size.width * 0.8250432,
        size.height * 0.3856586,
        size.width * 0.8250432,
        size.height * 0.3856586);
    path_67.close();

    Paint paint_67_fill = Paint()..style = PaintingStyle.fill;
    paint_67_fill.color = Colors.black;
    canvas.drawPath(path_67, paint_67_fill);

    Path path_68 = Path();
    path_68.moveTo(size.width * 0.8250432, size.height * 0.3856586);
    path_68.lineTo(size.width * 0.7602230, size.height * 0.3959937);
    path_68.cubicTo(
        size.width * 0.7602230,
        size.height * 0.3959937,
        size.width * 0.7159705,
        size.height * 0.4160008,
        size.width * 0.6974906,
        size.height * 0.4150227);
    path_68.cubicTo(
        size.width * 0.7024482,
        size.height * 0.4277508,
        size.width * 0.7037993,
        size.height * 0.4644555,
        size.width * 0.7037993,
        size.height * 0.4644555);
    path_68.lineTo(size.width * 0.7602230, size.height * 0.4644555);
    path_68.lineTo(size.width * 0.7602230, size.height * 0.4850133);
    path_68.lineTo(size.width * 0.8111799, size.height * 0.4825672);
    path_68.cubicTo(
        size.width * 0.8111799,
        size.height * 0.4825672,
        size.width * 0.8372086,
        size.height * 0.4669094,
        size.width * 0.8354029,
        size.height * 0.4360734);
    path_68.cubicTo(
        size.width * 0.8336043,
        size.height * 0.4052375,
        size.width * 0.8250432,
        size.height * 0.3856586,
        size.width * 0.8250432,
        size.height * 0.3856586);
    path_68.close();

    Paint paint_68_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_68_stroke.color = Colors.black;
    canvas.drawPath(path_68, paint_68_stroke);

    Paint paint_68_fill = Paint()..style = PaintingStyle.fill;
    paint_68_fill.color = Colors.black;
    canvas.drawPath(path_68, paint_68_fill);

    Path path_69 = Path();
    path_69.moveTo(size.width * 0.8251942, size.height * 0.3822094);
    path_69.cubicTo(
        size.width * 0.8186835,
        size.height * 0.3776813,
        size.width * 0.8103381,
        size.height * 0.3759164,
        size.width * 0.8030504,
        size.height * 0.3786469);
    path_69.lineTo(size.width * 0.7960072, size.height * 0.3877031);
    path_69.cubicTo(
        size.width * 0.7921655,
        size.height * 0.4042992,
        size.width * 0.7819856,
        size.height * 0.4181805,
        size.width * 0.7720791,
        size.height * 0.4314875);
    path_69.cubicTo(
        size.width * 0.7687914,
        size.height * 0.4359055,
        size.width * 0.7654317,
        size.height * 0.4403883,
        size.width * 0.7611079,
        size.height * 0.4435914);
    path_69.cubicTo(
        size.width * 0.7578489,
        size.height * 0.4460086,
        size.width * 0.7538921,
        size.height * 0.4478750,
        size.width * 0.7521007,
        size.height * 0.4517062);
    path_69.cubicTo(
        size.width * 0.7509784,
        size.height * 0.4541516,
        size.width * 0.7509424,
        size.height * 0.4570047,
        size.width * 0.7510288,
        size.height * 0.4597227);
    path_69.cubicTo(
        size.width * 0.7513022,
        size.height * 0.4689352,
        size.width * 0.7526043,
        size.height * 0.4780773,
        size.width * 0.7548921,
        size.height * 0.4869516);
    path_69.cubicTo(
        size.width * 0.7557698,
        size.height * 0.4903266,
        size.width * 0.7568345,
        size.height * 0.4937539,
        size.width * 0.7589353,
        size.height * 0.4964070);
    path_69.cubicTo(
        size.width * 0.7636475,
        size.height * 0.5023492,
        size.width * 0.7718993,
        size.height * 0.5026914,
        size.width * 0.7791007,
        size.height * 0.5021867);
    path_69.cubicTo(
        size.width * 0.7923237,
        size.height * 0.5012844,
        size.width * 0.8054388,
        size.height * 0.4989938,
        size.width * 0.8182590,
        size.height * 0.4953477);
    path_69.cubicTo(
        size.width * 0.8242590,
        size.height * 0.4936195,
        size.width * 0.8305396,
        size.height * 0.4913492,
        size.width * 0.8345540,
        size.height * 0.4861977);
    path_69.cubicTo(
        size.width * 0.8376331,
        size.height * 0.4822648,
        size.width * 0.8390000,
        size.height * 0.4771258,
        size.width * 0.8401871,
        size.height * 0.4721289);
    path_69.cubicTo(
        size.width * 0.8457338,
        size.height * 0.4488367,
        size.width * 0.8489353,
        size.height * 0.4232625,
        size.width * 0.8397050,
        size.height * 0.4013930);
    path_69.cubicTo(
        size.width * 0.8364892,
        size.height * 0.3937719,
        size.width * 0.8316978,
        size.height * 0.3867328,
        size.width * 0.8251942,
        size.height * 0.3822094);
    path_69.close();

    Paint paint_69_fill = Paint()..style = PaintingStyle.fill;
    paint_69_fill.color = Colors.black;
    canvas.drawPath(path_69, paint_69_fill);

    Path path_70 = Path();
    path_70.moveTo(size.width * 0.8251942, size.height * 0.3822094);
    path_70.cubicTo(
        size.width * 0.8186835,
        size.height * 0.3776813,
        size.width * 0.8103381,
        size.height * 0.3759164,
        size.width * 0.8030504,
        size.height * 0.3786469);
    path_70.lineTo(size.width * 0.7960072, size.height * 0.3877031);
    path_70.cubicTo(
        size.width * 0.7921655,
        size.height * 0.4042992,
        size.width * 0.7819856,
        size.height * 0.4181805,
        size.width * 0.7720791,
        size.height * 0.4314875);
    path_70.cubicTo(
        size.width * 0.7687914,
        size.height * 0.4359055,
        size.width * 0.7654317,
        size.height * 0.4403883,
        size.width * 0.7611079,
        size.height * 0.4435914);
    path_70.cubicTo(
        size.width * 0.7578489,
        size.height * 0.4460086,
        size.width * 0.7538921,
        size.height * 0.4478750,
        size.width * 0.7521007,
        size.height * 0.4517062);
    path_70.cubicTo(
        size.width * 0.7509784,
        size.height * 0.4541516,
        size.width * 0.7509424,
        size.height * 0.4570047,
        size.width * 0.7510288,
        size.height * 0.4597227);
    path_70.cubicTo(
        size.width * 0.7513022,
        size.height * 0.4689352,
        size.width * 0.7526043,
        size.height * 0.4780773,
        size.width * 0.7548921,
        size.height * 0.4869516);
    path_70.cubicTo(
        size.width * 0.7557698,
        size.height * 0.4903266,
        size.width * 0.7568345,
        size.height * 0.4937539,
        size.width * 0.7589353,
        size.height * 0.4964070);
    path_70.cubicTo(
        size.width * 0.7636475,
        size.height * 0.5023492,
        size.width * 0.7718993,
        size.height * 0.5026914,
        size.width * 0.7791007,
        size.height * 0.5021867);
    path_70.cubicTo(
        size.width * 0.7923237,
        size.height * 0.5012844,
        size.width * 0.8054388,
        size.height * 0.4989938,
        size.width * 0.8182590,
        size.height * 0.4953477);
    path_70.cubicTo(
        size.width * 0.8242590,
        size.height * 0.4936195,
        size.width * 0.8305396,
        size.height * 0.4913492,
        size.width * 0.8345540,
        size.height * 0.4861977);
    path_70.cubicTo(
        size.width * 0.8376331,
        size.height * 0.4822648,
        size.width * 0.8390000,
        size.height * 0.4771258,
        size.width * 0.8401871,
        size.height * 0.4721289);
    path_70.cubicTo(
        size.width * 0.8457338,
        size.height * 0.4488367,
        size.width * 0.8489353,
        size.height * 0.4232625,
        size.width * 0.8397050,
        size.height * 0.4013930);
    path_70.cubicTo(
        size.width * 0.8364892,
        size.height * 0.3937719,
        size.width * 0.8316978,
        size.height * 0.3867328,
        size.width * 0.8251942,
        size.height * 0.3822094);
    path_70.close();

    Paint paint_70_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_70_stroke.color = Colors.black;
    canvas.drawPath(path_70, paint_70_stroke);

    Paint paint_70_fill = Paint()..style = PaintingStyle.fill;
    paint_70_fill.color = Colors.black;
    canvas.drawPath(path_70, paint_70_fill);

    Path path_71 = Path();
    path_71.moveTo(size.width * 0.7731942, size.height * 0.6039508);
    path_71.lineTo(size.width * 0.7574317, size.height * 0.6103164);
    path_71.cubicTo(
        size.width * 0.7574317,
        size.height * 0.6103164,
        size.width * 0.7475180,
        size.height * 0.6173188,
        size.width * 0.7407554,
        size.height * 0.6253227);
    path_71.cubicTo(
        size.width * 0.7340000,
        size.height * 0.6333273,
        size.width * 0.7308417,
        size.height * 0.6460469,
        size.width * 0.7254388,
        size.height * 0.6543656);
    path_71.cubicTo(
        size.width * 0.7200288,
        size.height * 0.6626836,
        size.width * 0.7209353,
        size.height * 0.6724773,
        size.width * 0.7385036,
        size.height * 0.6710063);
    path_71.cubicTo(
        size.width * 0.7560791,
        size.height * 0.6695352,
        size.width * 0.7700576,
        size.height * 0.6695391,
        size.width * 0.7732086,
        size.height * 0.6479992);
    path_71.cubicTo(
        size.width * 0.7733453,
        size.height * 0.6470539,
        size.width * 0.7734748,
        size.height * 0.6461000,
        size.width * 0.7735827,
        size.height * 0.6451469);
    path_71.cubicTo(
        size.width * 0.7747914,
        size.height * 0.6351328,
        size.width * 0.7747338,
        size.height * 0.6251352,
        size.width * 0.7743597,
        size.height * 0.6174937);
    path_71.cubicTo(
        size.width * 0.7739640,
        size.height * 0.6093992,
        size.width * 0.7731942,
        size.height * 0.6039508,
        size.width * 0.7731942,
        size.height * 0.6039508);
    path_71.close();

    Paint paint_71_fill = Paint()..style = PaintingStyle.fill;
    paint_71_fill.color = Colors.black;
    canvas.drawPath(path_71, paint_71_fill);

    Path path_72 = Path();
    path_72.moveTo(size.width * 0.7731942, size.height * 0.6039508);
    path_72.lineTo(size.width * 0.7574317, size.height * 0.6103164);
    path_72.cubicTo(
        size.width * 0.7574317,
        size.height * 0.6103164,
        size.width * 0.7475180,
        size.height * 0.6173188,
        size.width * 0.7407554,
        size.height * 0.6253227);
    path_72.cubicTo(
        size.width * 0.7340000,
        size.height * 0.6333273,
        size.width * 0.7308417,
        size.height * 0.6460469,
        size.width * 0.7254388,
        size.height * 0.6543656);
    path_72.cubicTo(
        size.width * 0.7200288,
        size.height * 0.6626836,
        size.width * 0.7209353,
        size.height * 0.6724773,
        size.width * 0.7385036,
        size.height * 0.6710063);
    path_72.cubicTo(
        size.width * 0.7560791,
        size.height * 0.6695352,
        size.width * 0.7700576,
        size.height * 0.6695391,
        size.width * 0.7732086,
        size.height * 0.6479992);
    path_72.cubicTo(
        size.width * 0.7733453,
        size.height * 0.6470539,
        size.width * 0.7734748,
        size.height * 0.6461000,
        size.width * 0.7735827,
        size.height * 0.6451469);
    path_72.cubicTo(
        size.width * 0.7747914,
        size.height * 0.6351328,
        size.width * 0.7747338,
        size.height * 0.6251352,
        size.width * 0.7743597,
        size.height * 0.6174937);
    path_72.cubicTo(
        size.width * 0.7739640,
        size.height * 0.6093992,
        size.width * 0.7731942,
        size.height * 0.6039508,
        size.width * 0.7731942,
        size.height * 0.6039508);
    path_72.close();

    Paint paint_72_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_72_stroke.color = Colors.black;
    canvas.drawPath(path_72, paint_72_stroke);

    Paint paint_72_fill = Paint()..style = PaintingStyle.fill;
    paint_72_fill.color = activeColor1;
    canvas.drawPath(path_72, paint_72_fill);

    Path path_73 = Path();
    path_73.moveTo(size.width * 0.7816403, size.height * 0.4390969);
    path_73.cubicTo(
        size.width * 0.7805899,
        size.height * 0.4388891,
        size.width * 0.7795468,
        size.height * 0.4386117,
        size.width * 0.7785180,
        size.height * 0.4383063);
    path_73.cubicTo(
        size.width * 0.7744460,
        size.height * 0.4370836,
        size.width * 0.7650072,
        size.height * 0.4357875,
        size.width * 0.7615036,
        size.height * 0.4483039);
    path_73.cubicTo(
        size.width * 0.7570000,
        size.height * 0.4644555,
        size.width * 0.7587986,
        size.height * 0.6029641,
        size.width * 0.7520360,
        size.height * 0.6151992);
    path_73.cubicTo(
        size.width * 0.7452806,
        size.height * 0.6274344,
        size.width * 0.7664604,
        size.height * 0.6279234,
        size.width * 0.7700504,
        size.height * 0.6200898);
    path_73.cubicTo(
        size.width * 0.7736475,
        size.height * 0.6122562,
        size.width * 0.8133885,
        size.height * 0.5287766,
        size.width * 0.8038921,
        size.height * 0.4829094);
    path_73.cubicTo(
        size.width * 0.7960791,
        size.height * 0.4451578,
        size.width * 0.7852302,
        size.height * 0.4398141,
        size.width * 0.7816403,
        size.height * 0.4390969);
    path_73.close();

    Paint paint_73_fill = Paint()..style = PaintingStyle.fill;
    paint_73_fill.color = Colors.black;
    canvas.drawPath(path_73, paint_73_fill);

    Path path_74 = Path();
    path_74.moveTo(size.width * 0.7816403, size.height * 0.4390969);
    path_74.cubicTo(
        size.width * 0.7805899,
        size.height * 0.4388891,
        size.width * 0.7795468,
        size.height * 0.4386117,
        size.width * 0.7785180,
        size.height * 0.4383063);
    path_74.cubicTo(
        size.width * 0.7744460,
        size.height * 0.4370836,
        size.width * 0.7650072,
        size.height * 0.4357875,
        size.width * 0.7615036,
        size.height * 0.4483039);
    path_74.cubicTo(
        size.width * 0.7570000,
        size.height * 0.4644555,
        size.width * 0.7587986,
        size.height * 0.6029641,
        size.width * 0.7520360,
        size.height * 0.6151992);
    path_74.cubicTo(
        size.width * 0.7452806,
        size.height * 0.6274344,
        size.width * 0.7664604,
        size.height * 0.6279234,
        size.width * 0.7700504,
        size.height * 0.6200898);
    path_74.cubicTo(
        size.width * 0.7736475,
        size.height * 0.6122562,
        size.width * 0.8133885,
        size.height * 0.5287766,
        size.width * 0.8038921,
        size.height * 0.4829094);
    path_74.cubicTo(
        size.width * 0.7960791,
        size.height * 0.4451578,
        size.width * 0.7852302,
        size.height * 0.4398141,
        size.width * 0.7816403,
        size.height * 0.4390969);
    path_74.close();

    Paint paint_74_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_74_stroke.color = Colors.black;
    canvas.drawPath(path_74, paint_74_stroke);

    Paint paint_74_fill = Paint()..style = PaintingStyle.fill;
    paint_74_fill.color = Colors.white;
    canvas.drawPath(path_74, paint_74_fill);

    Path path_75 = Path();
    path_75.moveTo(size.width * 0.7374245, size.height * 0.2711359);
    path_75.cubicTo(
        size.width * 0.7297266,
        size.height * 0.3085828,
        size.width * 0.7520288,
        size.height * 0.4047664,
        size.width * 0.7520288,
        size.height * 0.4047664);
    path_75.cubicTo(
        size.width * 0.7520288,
        size.height * 0.4047664,
        size.width * 0.8026547,
        size.height * 0.4157703,
        size.width * 0.8277122,
        size.height * 0.3900945);
    path_75.cubicTo(
        size.width * 0.8291079,
        size.height * 0.3688039,
        size.width * 0.8291079,
        size.height * 0.3563242,
        size.width * 0.8291079,
        size.height * 0.3563242);
    path_75.lineTo(size.width * 0.8392374, size.height * 0.2425336);
    path_75.lineTo(size.width * 0.7823957, size.height * 0.2000578);
    path_75.cubicTo(
        size.width * 0.7823957,
        size.height * 0.2000578,
        size.width * 0.7451223,
        size.height * 0.2336938,
        size.width * 0.7374245,
        size.height * 0.2711359);
    path_75.close();

    Paint paint_75_fill = Paint()..style = PaintingStyle.fill;
    paint_75_fill.color = Colors.black;
    canvas.drawPath(path_75, paint_75_fill);

    Path path_76 = Path();
    path_76.moveTo(size.width * 0.7374245, size.height * 0.2711359);
    path_76.cubicTo(
        size.width * 0.7297266,
        size.height * 0.3085828,
        size.width * 0.7520288,
        size.height * 0.4047664,
        size.width * 0.7520288,
        size.height * 0.4047664);
    path_76.cubicTo(
        size.width * 0.7520288,
        size.height * 0.4047664,
        size.width * 0.8026547,
        size.height * 0.4157703,
        size.width * 0.8277122,
        size.height * 0.3900945);
    path_76.cubicTo(
        size.width * 0.8291079,
        size.height * 0.3688039,
        size.width * 0.8291079,
        size.height * 0.3563242,
        size.width * 0.8291079,
        size.height * 0.3563242);
    path_76.lineTo(size.width * 0.8392374, size.height * 0.2425336);
    path_76.lineTo(size.width * 0.7823957, size.height * 0.2000578);
    path_76.cubicTo(
        size.width * 0.7823957,
        size.height * 0.2000578,
        size.width * 0.7451223,
        size.height * 0.2336938,
        size.width * 0.7374245,
        size.height * 0.2711359);
    path_76.close();

    Paint paint_76_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_76_stroke.color = Colors.black;
    canvas.drawPath(path_76, paint_76_stroke);

    Paint paint_76_fill = Paint()..style = PaintingStyle.fill;
    paint_76_fill.color = activeColor1;
    canvas.drawPath(path_76, paint_76_fill);

    Path path_77 = Path();
    path_77.moveTo(size.width * 0.8111727, size.height * 0.1165977);
    path_77.cubicTo(
        size.width * 0.8111727,
        size.height * 0.1165977,
        size.width * 0.8007194,
        size.height * 0.1022844,
        size.width * 0.7794101,
        size.height * 0.1081570);
    path_77.cubicTo(
        size.width * 0.7627338,
        size.height * 0.1127547,
        size.width * 0.7479281,
        size.height * 0.1202742,
        size.width * 0.7422806,
        size.height * 0.1233266);
    path_77.cubicTo(
        size.width * 0.7411511,
        size.height * 0.1239148,
        size.width * 0.7401511,
        size.height * 0.1247594,
        size.width * 0.7393525,
        size.height * 0.1258031);
    path_77.cubicTo(
        size.width * 0.7385468,
        size.height * 0.1268477,
        size.width * 0.7379640,
        size.height * 0.1280672,
        size.width * 0.7376331,
        size.height * 0.1293789);
    path_77.cubicTo(
        size.width * 0.7368561,
        size.height * 0.1326398,
        size.width * 0.7377626,
        size.height * 0.1365969,
        size.width * 0.7459353,
        size.height * 0.1374570);
    path_77.cubicTo(
        size.width * 0.7604676,
        size.height * 0.1389891,
        size.width * 0.7905755,
        size.height * 0.1448625,
        size.width * 0.7905755,
        size.height * 0.1448625);
    path_77.lineTo(size.width * 0.8084820, size.height * 0.1878797);
    path_77.lineTo(size.width * 0.8290935, size.height * 0.1525609);
    path_77.cubicTo(
        size.width * 0.8290935,
        size.height * 0.1525609,
        size.width * 0.8240144,
        size.height * 0.1151305,
        size.width * 0.8111727,
        size.height * 0.1165977);
    path_77.close();

    Paint paint_77_fill = Paint()..style = PaintingStyle.fill;
    paint_77_fill.color = Colors.black;
    canvas.drawPath(path_77, paint_77_fill);

    Path path_78 = Path();
    path_78.moveTo(size.width * 0.8119281, size.height * 0.1157430);
    path_78.lineTo(size.width * 0.8115108, size.height * 0.1157430);
    path_78.cubicTo(
        size.width * 0.8092734,
        size.height * 0.1131219,
        size.width * 0.8065971,
        size.height * 0.1109820,
        size.width * 0.8036331,
        size.height * 0.1094336);
    path_78.cubicTo(
        size.width * 0.7987050,
        size.height * 0.1067766,
        size.width * 0.7904676,
        size.height * 0.1042578,
        size.width * 0.7792374,
        size.height * 0.1073547);
    path_78.cubicTo(
        size.width * 0.7624748,
        size.height * 0.1119727,
        size.width * 0.7475827,
        size.height * 0.1195453,
        size.width * 0.7419568,
        size.height * 0.1225813);
    path_78.cubicTo(
        size.width * 0.7407266,
        size.height * 0.1232219,
        size.width * 0.7396475,
        size.height * 0.1241406,
        size.width * 0.7387770,
        size.height * 0.1252766);
    path_78.cubicTo(
        size.width * 0.7379065,
        size.height * 0.1264125,
        size.width * 0.7372662,
        size.height * 0.1277375,
        size.width * 0.7369137,
        size.height * 0.1291633);
    path_78.cubicTo(
        size.width * 0.7364245,
        size.height * 0.1312258,
        size.width * 0.7366547,
        size.height * 0.1330070,
        size.width * 0.7375899,
        size.height * 0.1344617);
    path_78.cubicTo(
        size.width * 0.7389424,
        size.height * 0.1365484,
        size.width * 0.7417194,
        size.height * 0.1378242,
        size.width * 0.7458705,
        size.height * 0.1382602);
    path_78.cubicTo(
        size.width * 0.7593165,
        size.height * 0.1396789,
        size.width * 0.7864892,
        size.height * 0.1448914,
        size.width * 0.7900647,
        size.height * 0.1455961);
    path_78.lineTo(size.width * 0.8083813, size.height * 0.1896125);
    path_78.lineTo(size.width * 0.8298777, size.height * 0.1527656);
    path_78.lineTo(size.width * 0.8298417, size.height * 0.1524633);
    path_78.cubicTo(
        size.width * 0.8296547,
        size.height * 0.1509148,
        size.width * 0.8247266,
        size.height * 0.1157430,
        size.width * 0.8119281,
        size.height * 0.1157430);
    path_78.close();
    path_78.moveTo(size.width * 0.8086043, size.height * 0.1861648);
    path_78.lineTo(size.width * 0.7911151, size.height * 0.1441375);
    path_78.lineTo(size.width * 0.7907410, size.height * 0.1440602);
    path_78.cubicTo(
        size.width * 0.7904388,
        size.height * 0.1440023,
        size.width * 0.7604388,
        size.height * 0.1381664,
        size.width * 0.7460432,
        size.height * 0.1366461);
    path_78.cubicTo(
        size.width * 0.7423453,
        size.height * 0.1362391,
        size.width * 0.7399281,
        size.height * 0.1352117,
        size.width * 0.7388417,
        size.height * 0.1335367);
    path_78.cubicTo(
        size.width * 0.7381727,
        size.height * 0.1324891,
        size.width * 0.7380144,
        size.height * 0.1311609,
        size.width * 0.7383885,
        size.height * 0.1295836);
    path_78.cubicTo(
        size.width * 0.7386906,
        size.height * 0.1283844,
        size.width * 0.7392302,
        size.height * 0.1272703,
        size.width * 0.7399640,
        size.height * 0.1263164);
    path_78.cubicTo(
        size.width * 0.7406978,
        size.height * 0.1253625,
        size.width * 0.7416115,
        size.height * 0.1245922,
        size.width * 0.7426475,
        size.height * 0.1240570);
    path_78.cubicTo(
        size.width * 0.7482302,
        size.height * 0.1210367,
        size.width * 0.7630072,
        size.height * 0.1135258,
        size.width * 0.7796331,
        size.height * 0.1089484);
    path_78.cubicTo(
        size.width * 0.7827986,
        size.height * 0.1080516,
        size.width * 0.7860576,
        size.height * 0.1075891,
        size.width * 0.7893381,
        size.height * 0.1075711);
    path_78.cubicTo(
        size.width * 0.7940360,
        size.height * 0.1075195,
        size.width * 0.7986763,
        size.height * 0.1086484,
        size.width * 0.8029065,
        size.height * 0.1108680);
    path_78.cubicTo(
        size.width * 0.8079856,
        size.height * 0.1135867,
        size.width * 0.8105755,
        size.height * 0.1170508,
        size.width * 0.8106187,
        size.height * 0.1171078);
    path_78.lineTo(size.width * 0.8108705, size.height * 0.1174586);
    path_78.lineTo(size.width * 0.8112806, size.height * 0.1174094);
    path_78.cubicTo(
        size.width * 0.8174173,
        size.height * 0.1167008,
        size.width * 0.8217338,
        size.height * 0.1261922,
        size.width * 0.8242662,
        size.height * 0.1342742);
    path_78.cubicTo(
        size.width * 0.8260504,
        size.height * 0.1401883,
        size.width * 0.8274101,
        size.height * 0.1462438,
        size.width * 0.8283381,
        size.height * 0.1523859);
    path_78.lineTo(size.width * 0.8086043, size.height * 0.1861648);
    path_78.close();

    Paint paint_78_fill = Paint()..style = PaintingStyle.fill;
    paint_78_fill.color = Colors.black;
    canvas.drawPath(path_78, paint_78_fill);

    Path path_79 = Path();
    path_79.moveTo(size.width * 0.8032950, size.height * 0.1438164);
    path_79.cubicTo(
        size.width * 0.7894388,
        size.height * 0.1423414,
        size.width * 0.7880863,
        size.height * 0.1328125,
        size.width * 0.7880863,
        size.height * 0.1328125);
    path_79.lineTo(size.width * 0.7722014, size.height * 0.1354328);
    path_79.lineTo(size.width * 0.7482014, size.height * 0.1393539);
    path_79.lineTo(size.width * 0.7484892, size.height * 0.1423492);
    path_79.cubicTo(
        size.width * 0.7491439,
        size.height * 0.1491961,
        size.width * 0.7507194,
        size.height * 0.1666031,
        size.width * 0.7512446,
        size.height * 0.1814258);
    path_79.cubicTo(
        size.width * 0.7519137,
        size.height * 0.2003531,
        size.width * 0.7569856,
        size.height * 0.2132156,
        size.width * 0.7705108,
        size.height * 0.2128328);
    path_79.cubicTo(
        size.width * 0.7753453,
        size.height * 0.2127023,
        size.width * 0.7785324,
        size.height * 0.2122906,
        size.width * 0.7806403,
        size.height * 0.2118141);
    path_79.cubicTo(
        size.width * 0.7844317,
        size.height * 0.2109539,
        size.width * 0.7847050,
        size.height * 0.2098937,
        size.width * 0.7847050,
        size.height * 0.2098937);
    path_79.lineTo(size.width * 0.7826763, size.height * 0.2205922);
    path_79.lineTo(size.width * 0.7823381, size.height * 0.2223773);
    path_79.cubicTo(
        size.width * 0.7823381,
        size.height * 0.2223773,
        size.width * 0.7671295,
        size.height * 0.2326523,
        size.width * 0.7620719,
        size.height * 0.2480539);
    path_79.cubicTo(
        size.width * 0.7570144,
        size.height * 0.2634555,
        size.width * 0.7617338,
        size.height * 0.2965086,
        size.width * 0.7735683,
        size.height * 0.2946703);
    path_79.cubicTo(
        size.width * 0.7854029,
        size.height * 0.2928320,
        size.width * 0.7999353,
        size.height * 0.2377750,
        size.width * 0.8066906,
        size.height * 0.2341070);
    path_79.cubicTo(
        size.width * 0.8134532,
        size.height * 0.2304391,
        size.width * 0.8097338,
        size.height * 0.1959313,
        size.width * 0.8097338,
        size.height * 0.1959313);
    path_79.lineTo(size.width * 0.8134532, size.height * 0.1735398);
    path_79.cubicTo(
        size.width * 0.8134532,
        size.height * 0.1735398,
        size.width * 0.7985612,
        size.height * 0.1533578,
        size.width * 0.8032950,
        size.height * 0.1438164);
    path_79.close();

    Paint paint_79_fill = Paint()..style = PaintingStyle.fill;
    paint_79_fill.color = Colors.black;
    canvas.drawPath(path_79, paint_79_fill);

    Path path_80 = Path();
    path_80.moveTo(size.width * 0.8032950, size.height * 0.1438164);
    path_80.cubicTo(
        size.width * 0.7894388,
        size.height * 0.1423414,
        size.width * 0.7880863,
        size.height * 0.1328125,
        size.width * 0.7880863,
        size.height * 0.1328125);
    path_80.lineTo(size.width * 0.7722014, size.height * 0.1354328);
    path_80.lineTo(size.width * 0.7482014, size.height * 0.1393539);
    path_80.lineTo(size.width * 0.7484892, size.height * 0.1423492);
    path_80.cubicTo(
        size.width * 0.7491439,
        size.height * 0.1491961,
        size.width * 0.7507194,
        size.height * 0.1666031,
        size.width * 0.7512446,
        size.height * 0.1814258);
    path_80.cubicTo(
        size.width * 0.7519137,
        size.height * 0.2003531,
        size.width * 0.7569856,
        size.height * 0.2132156,
        size.width * 0.7705108,
        size.height * 0.2128328);
    path_80.cubicTo(
        size.width * 0.7753453,
        size.height * 0.2127023,
        size.width * 0.7785324,
        size.height * 0.2122906,
        size.width * 0.7806403,
        size.height * 0.2118141);
    path_80.cubicTo(
        size.width * 0.7844317,
        size.height * 0.2109539,
        size.width * 0.7847050,
        size.height * 0.2098937,
        size.width * 0.7847050,
        size.height * 0.2098937);
    path_80.lineTo(size.width * 0.7826763, size.height * 0.2205922);
    path_80.lineTo(size.width * 0.7823381, size.height * 0.2223773);
    path_80.cubicTo(
        size.width * 0.7823381,
        size.height * 0.2223773,
        size.width * 0.7671295,
        size.height * 0.2326523,
        size.width * 0.7620719,
        size.height * 0.2480539);
    path_80.cubicTo(
        size.width * 0.7570144,
        size.height * 0.2634555,
        size.width * 0.7617338,
        size.height * 0.2965086,
        size.width * 0.7735683,
        size.height * 0.2946703);
    path_80.cubicTo(
        size.width * 0.7854029,
        size.height * 0.2928320,
        size.width * 0.7999353,
        size.height * 0.2377750,
        size.width * 0.8066906,
        size.height * 0.2341070);
    path_80.cubicTo(
        size.width * 0.8134532,
        size.height * 0.2304391,
        size.width * 0.8097338,
        size.height * 0.1959313,
        size.width * 0.8097338,
        size.height * 0.1959313);
    path_80.lineTo(size.width * 0.8134532, size.height * 0.1735398);
    path_80.cubicTo(
        size.width * 0.8134532,
        size.height * 0.1735398,
        size.width * 0.7985612,
        size.height * 0.1533578,
        size.width * 0.8032950,
        size.height * 0.1438164);
    path_80.close();

    Paint paint_80_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_80_stroke.color = Colors.black;
    canvas.drawPath(path_80, paint_80_stroke);

    Paint paint_80_fill = Paint()..style = PaintingStyle.fill;
    paint_80_fill.color = Colors.white;
    canvas.drawPath(path_80, paint_80_fill);

    Path path_81 = Path();
    path_81.moveTo(size.width * 0.8754317, size.height * 0.02764891);
    path_81.cubicTo(
        size.width * 0.8710072,
        size.height * 0.02764891,
        size.width * 0.8674101,
        size.height * 0.03269047,
        size.width * 0.8674101,
        size.height * 0.03890570);
    path_81.cubicTo(
        size.width * 0.8674101,
        size.height * 0.04512102,
        size.width * 0.8710072,
        size.height * 0.05016250,
        size.width * 0.8754317,
        size.height * 0.05016250);
    path_81.cubicTo(
        size.width * 0.8798633,
        size.height * 0.05016250,
        size.width * 0.8834460,
        size.height * 0.04512508,
        size.width * 0.8834460,
        size.height * 0.03890570);
    path_81.cubicTo(
        size.width * 0.8834460,
        size.height * 0.03268641,
        size.width * 0.8798489,
        size.height * 0.02764891,
        size.width * 0.8754317,
        size.height * 0.02764891);
    path_81.close();

    Paint paint_81_fill = Paint()..style = PaintingStyle.fill;
    paint_81_fill.color = Colors.black;
    canvas.drawPath(path_81, paint_81_fill);

    Path path_82 = Path();
    path_82.moveTo(size.width * 0.8754317, size.height * 0.02764891);
    path_82.cubicTo(
        size.width * 0.8710072,
        size.height * 0.02764891,
        size.width * 0.8674101,
        size.height * 0.03269047,
        size.width * 0.8674101,
        size.height * 0.03890570);
    path_82.cubicTo(
        size.width * 0.8674101,
        size.height * 0.04512102,
        size.width * 0.8710072,
        size.height * 0.05016250,
        size.width * 0.8754317,
        size.height * 0.05016250);
    path_82.cubicTo(
        size.width * 0.8798633,
        size.height * 0.05016250,
        size.width * 0.8834460,
        size.height * 0.04512508,
        size.width * 0.8834460,
        size.height * 0.03890570);
    path_82.cubicTo(
        size.width * 0.8834460,
        size.height * 0.03268641,
        size.width * 0.8798489,
        size.height * 0.02764891,
        size.width * 0.8754317,
        size.height * 0.02764891);
    path_82.close();

    Paint paint_82_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_82_stroke.color = Colors.black;
    canvas.drawPath(path_82, paint_82_stroke);

    Paint paint_82_fill = Paint()..style = PaintingStyle.fill;
    paint_82_fill.color = Colors.white;
    canvas.drawPath(path_82, paint_82_fill);

    Path path_83 = Path();
    path_83.moveTo(size.width * 0.8797050, size.height * 0.02764891);
    path_83.cubicTo(
        size.width * 0.8752734,
        size.height * 0.02764891,
        size.width * 0.8716835,
        size.height * 0.03269047,
        size.width * 0.8716835,
        size.height * 0.03890570);
    path_83.cubicTo(
        size.width * 0.8716835,
        size.height * 0.04512102,
        size.width * 0.8752734,
        size.height * 0.05016250,
        size.width * 0.8797050,
        size.height * 0.05016250);
    path_83.cubicTo(
        size.width * 0.8841295,
        size.height * 0.05016250,
        size.width * 0.8877194,
        size.height * 0.04512508,
        size.width * 0.8877194,
        size.height * 0.03890570);
    path_83.cubicTo(
        size.width * 0.8877194,
        size.height * 0.03268641,
        size.width * 0.8841295,
        size.height * 0.02764891,
        size.width * 0.8797050,
        size.height * 0.02764891);
    path_83.close();

    Paint paint_83_fill = Paint()..style = PaintingStyle.fill;
    paint_83_fill.color = Colors.black;
    canvas.drawPath(path_83, paint_83_fill);

    Path path_84 = Path();
    path_84.moveTo(size.width * 0.8797050, size.height * 0.02764891);
    path_84.cubicTo(
        size.width * 0.8752734,
        size.height * 0.02764891,
        size.width * 0.8716835,
        size.height * 0.03269047,
        size.width * 0.8716835,
        size.height * 0.03890570);
    path_84.cubicTo(
        size.width * 0.8716835,
        size.height * 0.04512102,
        size.width * 0.8752734,
        size.height * 0.05016250,
        size.width * 0.8797050,
        size.height * 0.05016250);
    path_84.cubicTo(
        size.width * 0.8841295,
        size.height * 0.05016250,
        size.width * 0.8877194,
        size.height * 0.04512508,
        size.width * 0.8877194,
        size.height * 0.03890570);
    path_84.cubicTo(
        size.width * 0.8877194,
        size.height * 0.03268641,
        size.width * 0.8841295,
        size.height * 0.02764891,
        size.width * 0.8797050,
        size.height * 0.02764891);
    path_84.close();

    Paint paint_84_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_84_stroke.color = Colors.black;
    canvas.drawPath(path_84, paint_84_stroke);

    Paint paint_84_fill = Paint()..style = PaintingStyle.fill;
    paint_84_fill.color = Colors.white;
    canvas.drawPath(path_84, paint_84_fill);

    Path path_85 = Path();
    path_85.moveTo(size.width * 0.8419353, size.height * 0.02299219);
    path_85.cubicTo(
        size.width * 0.8419353,
        size.height * 0.02299219,
        size.width * 0.8236835,
        size.height * 0.02299219,
        size.width * 0.8199640,
        size.height * 0.02923602);
    path_85.cubicTo(
        size.width * 0.8190576,
        size.height * 0.02598984,
        size.width * 0.8174317,
        size.height * 0.02303188,
        size.width * 0.8152374,
        size.height * 0.02061617);
    path_85.cubicTo(
        size.width * 0.8149137,
        size.height * 0.02026719,
        size.width * 0.8145036,
        size.height * 0.02003297,
        size.width * 0.8140576,
        size.height * 0.01994445);
    path_85.cubicTo(
        size.width * 0.8136043,
        size.height * 0.01985602,
        size.width * 0.8131439,
        size.height * 0.01991742,
        size.width * 0.8127266,
        size.height * 0.02012055);
    path_85.cubicTo(
        size.width * 0.8123094,
        size.height * 0.02032375,
        size.width * 0.8119640,
        size.height * 0.02065906,
        size.width * 0.8117266,
        size.height * 0.02108234);
    path_85.cubicTo(
        size.width * 0.8114892,
        size.height * 0.02150555,
        size.width * 0.8113741,
        size.height * 0.02199672,
        size.width * 0.8114029,
        size.height * 0.02249094);
    path_85.cubicTo(
        size.width * 0.8116619,
        size.height * 0.02788695,
        size.width * 0.8120935,
        size.height * 0.03659250,
        size.width * 0.8125324,
        size.height * 0.04318273);
    path_85.cubicTo(
        size.width * 0.8132086,
        size.height * 0.05346133,
        size.width * 0.8250360,
        size.height * 0.06190188,
        size.width * 0.8250360,
        size.height * 0.06190188);
    path_85.cubicTo(
        size.width * 0.8250360,
        size.height * 0.06190188,
        size.width * 0.8047698,
        size.height * 0.1842102,
        size.width * 0.8047698,
        size.height * 0.2118836);
    path_85.cubicTo(
        size.width * 0.8047698,
        size.height * 0.2395570,
        size.width * 0.8088273,
        size.height * 0.2770031,
        size.width * 0.8250360,
        size.height * 0.2740648);
    path_85.cubicTo(
        size.width * 0.8412446,
        size.height * 0.2711266,
        size.width * 0.8473453,
        size.height * 0.2228875,
        size.width * 0.8500504,
        size.height * 0.2000234);
    path_85.cubicTo(
        size.width * 0.8527554,
        size.height * 0.1771594,
        size.width * 0.8453165,
        size.height * 0.06189773,
        size.width * 0.8453165,
        size.height * 0.06189773);
    path_85.lineTo(size.width * 0.8419353, size.height * 0.02299219);
    path_85.close();

    Paint paint_85_fill = Paint()..style = PaintingStyle.fill;
    paint_85_fill.color = Colors.black;
    canvas.drawPath(path_85, paint_85_fill);

    Path path_86 = Path();
    path_86.moveTo(size.width * 0.8419353, size.height * 0.02299219);
    path_86.cubicTo(
        size.width * 0.8419353,
        size.height * 0.02299219,
        size.width * 0.8236835,
        size.height * 0.02299219,
        size.width * 0.8199640,
        size.height * 0.02923602);
    path_86.cubicTo(
        size.width * 0.8190576,
        size.height * 0.02598984,
        size.width * 0.8174317,
        size.height * 0.02303188,
        size.width * 0.8152374,
        size.height * 0.02061617);
    path_86.cubicTo(
        size.width * 0.8149137,
        size.height * 0.02026719,
        size.width * 0.8145036,
        size.height * 0.02003297,
        size.width * 0.8140576,
        size.height * 0.01994445);
    path_86.cubicTo(
        size.width * 0.8136043,
        size.height * 0.01985602,
        size.width * 0.8131439,
        size.height * 0.01991742,
        size.width * 0.8127266,
        size.height * 0.02012055);
    path_86.cubicTo(
        size.width * 0.8123094,
        size.height * 0.02032375,
        size.width * 0.8119640,
        size.height * 0.02065906,
        size.width * 0.8117266,
        size.height * 0.02108234);
    path_86.cubicTo(
        size.width * 0.8114892,
        size.height * 0.02150555,
        size.width * 0.8113741,
        size.height * 0.02199672,
        size.width * 0.8114029,
        size.height * 0.02249094);
    path_86.cubicTo(
        size.width * 0.8116619,
        size.height * 0.02788695,
        size.width * 0.8120935,
        size.height * 0.03659250,
        size.width * 0.8125324,
        size.height * 0.04318273);
    path_86.cubicTo(
        size.width * 0.8132086,
        size.height * 0.05346133,
        size.width * 0.8250360,
        size.height * 0.06190188,
        size.width * 0.8250360,
        size.height * 0.06190188);
    path_86.cubicTo(
        size.width * 0.8250360,
        size.height * 0.06190188,
        size.width * 0.8047698,
        size.height * 0.1842102,
        size.width * 0.8047698,
        size.height * 0.2118836);
    path_86.cubicTo(
        size.width * 0.8047698,
        size.height * 0.2395570,
        size.width * 0.8088273,
        size.height * 0.2770031,
        size.width * 0.8250360,
        size.height * 0.2740648);
    path_86.cubicTo(
        size.width * 0.8412446,
        size.height * 0.2711266,
        size.width * 0.8473453,
        size.height * 0.2228875,
        size.width * 0.8500504,
        size.height * 0.2000234);
    path_86.cubicTo(
        size.width * 0.8527554,
        size.height * 0.1771594,
        size.width * 0.8453165,
        size.height * 0.06189773,
        size.width * 0.8453165,
        size.height * 0.06189773);
    path_86.lineTo(size.width * 0.8419353, size.height * 0.02299219);
    path_86.close();

    Paint paint_86_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_86_stroke.color = Colors.black;
    canvas.drawPath(path_86, paint_86_stroke);

    Paint paint_86_fill = Paint()..style = PaintingStyle.fill;
    paint_86_fill.color = Colors.white;
    canvas.drawPath(path_86, paint_86_fill);

    Path path_87 = Path();
    path_87.moveTo(size.width * 0.8599712, size.height * 0.07781930);
    path_87.cubicTo(
        size.width * 0.8752806,
        size.height * 0.07781930,
        size.width * 0.8876906,
        size.height * 0.06039883,
        size.width * 0.8876906,
        size.height * 0.03890961);
    path_87.cubicTo(size.width * 0.8876906, size.height * 0.01742047,
        size.width * 0.8752806, 0, size.width * 0.8599712, 0);
    path_87.cubicTo(
        size.width * 0.8446619,
        0,
        size.width * 0.8322590,
        size.height * 0.01742047,
        size.width * 0.8322590,
        size.height * 0.03890961);
    path_87.cubicTo(
        size.width * 0.8322590,
        size.height * 0.06039883,
        size.width * 0.8446619,
        size.height * 0.07781930,
        size.width * 0.8599712,
        size.height * 0.07781930);
    path_87.close();

    Paint paint_87_fill = Paint()..style = PaintingStyle.fill;
    paint_87_fill.color = Colors.black;
    canvas.drawPath(path_87, paint_87_fill);

    Path path_88 = Path();
    path_88.moveTo(size.width * 0.8599712, size.height * 0.07781930);
    path_88.cubicTo(
        size.width * 0.8752806,
        size.height * 0.07781930,
        size.width * 0.8876906,
        size.height * 0.06039883,
        size.width * 0.8876906,
        size.height * 0.03890961);
    path_88.cubicTo(size.width * 0.8876906, size.height * 0.01742047,
        size.width * 0.8752806, 0, size.width * 0.8599712, 0);
    path_88.cubicTo(
        size.width * 0.8446619,
        0,
        size.width * 0.8322590,
        size.height * 0.01742047,
        size.width * 0.8322590,
        size.height * 0.03890961);
    path_88.cubicTo(
        size.width * 0.8322590,
        size.height * 0.06039883,
        size.width * 0.8446619,
        size.height * 0.07781930,
        size.width * 0.8599712,
        size.height * 0.07781930);
    path_88.close();

    Paint paint_88_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_88_stroke.color = Colors.black;
    canvas.drawPath(path_88, paint_88_stroke);

    Paint paint_88_fill = Paint()..style = PaintingStyle.fill;
    paint_88_fill.color = Colors.white;
    canvas.drawPath(path_88, paint_88_fill);

    Path path_89 = Path();
    path_89.moveTo(size.width * 0.8696547, size.height * 0.07781930);
    path_89.cubicTo(
        size.width * 0.8849640,
        size.height * 0.07781930,
        size.width * 0.8973669,
        size.height * 0.06039883,
        size.width * 0.8973669,
        size.height * 0.03890961);
    path_89.cubicTo(size.width * 0.8973669, size.height * 0.01742047,
        size.width * 0.8849640, 0, size.width * 0.8696547, 0);
    path_89.cubicTo(
        size.width * 0.8543453,
        0,
        size.width * 0.8419353,
        size.height * 0.01742047,
        size.width * 0.8419353,
        size.height * 0.03890961);
    path_89.cubicTo(
        size.width * 0.8419353,
        size.height * 0.06039883,
        size.width * 0.8543453,
        size.height * 0.07781930,
        size.width * 0.8696547,
        size.height * 0.07781930);
    path_89.close();

    Paint paint_89_fill = Paint()..style = PaintingStyle.fill;
    paint_89_fill.color = Colors.black;
    canvas.drawPath(path_89, paint_89_fill);

    Path path_90 = Path();
    path_90.moveTo(size.width * 0.8696547, size.height * 0.07781930);
    path_90.cubicTo(
        size.width * 0.8849640,
        size.height * 0.07781930,
        size.width * 0.8973669,
        size.height * 0.06039883,
        size.width * 0.8973669,
        size.height * 0.03890961);
    path_90.cubicTo(size.width * 0.8973669, size.height * 0.01742047,
        size.width * 0.8849640, 0, size.width * 0.8696547, 0);
    path_90.cubicTo(
        size.width * 0.8543453,
        0,
        size.width * 0.8419353,
        size.height * 0.01742047,
        size.width * 0.8419353,
        size.height * 0.03890961);
    path_90.cubicTo(
        size.width * 0.8419353,
        size.height * 0.06039883,
        size.width * 0.8543453,
        size.height * 0.07781930,
        size.width * 0.8696547,
        size.height * 0.07781930);
    path_90.close();

    Paint paint_90_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.006461597;
    paint_90_stroke.color = Colors.black;
    canvas.drawPath(path_90, paint_90_stroke);

    Paint paint_90_fill = Paint()..style = PaintingStyle.fill;
    paint_90_fill.color = Colors.white;
    canvas.drawPath(path_90, paint_90_fill);

    Path path_91 = Path();
    path_91.moveTo(size.width * 0.5415712, size.height * 0.3038180);
    path_91.lineTo(size.width * 0.5412266, size.height * 0.3400070);

    Paint paint_91_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002482230;
    paint_91_stroke.color = Colors.black;
    paint_91_stroke.strokeCap = StrokeCap.round;
    canvas.drawPath(path_91, paint_91_stroke);

    Paint paint_91_fill = Paint()..style = PaintingStyle.fill;
    paint_91_fill.color = Colors.black;
    canvas.drawPath(path_91, paint_91_fill);

    Path path_92 = Path();
    path_92.moveTo(size.width * 0.5682360, size.height * 0.3153078);
    path_92.lineTo(size.width * 0.5600971, size.height * 0.3400242);

    Paint paint_92_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002482230;
    paint_92_stroke.color = Colors.black;
    paint_92_stroke.strokeCap = StrokeCap.round;
    canvas.drawPath(path_92, paint_92_stroke);

    Paint paint_92_fill = Paint()..style = PaintingStyle.fill;
    paint_92_fill.color = Colors.black;
    canvas.drawPath(path_92, paint_92_fill);

    Path path_93 = Path();
    path_93.moveTo(size.width * 0.9156475, size.height * 0.7947109);
    path_93.cubicTo(
        size.width * 0.9546547,
        size.height * 0.9337344,
        size.width * 0.7505899,
        size.height * 0.9954844,
        size.width * 0.5461331,
        size.height * 0.9932656);
    path_93.cubicTo(
        size.width * 0.3279971,
        size.height * 0.9908984,
        size.width * 0.1556360,
        size.height * 0.9362109,
        size.width * 0.1918878,
        size.height * 0.7947109);
    path_93.cubicTo(
        size.width * 0.2014921,
        size.height * 0.7572266,
        size.width * 0.2547612,
        size.height * 0.7221508,
        size.width * 0.2774525,
        size.height * 0.6516477);
    path_93.cubicTo(
        size.width * 0.2981194,
        size.height * 0.5874352,
        size.width * 0.2482763,
        size.height * 0.5180219,
        size.width * 0.2994734,
        size.height * 0.4575852);
    path_93.cubicTo(
        size.width * 0.3563568,
        size.height * 0.3904344,
        size.width * 0.4510827,
        size.height * 0.3738742,
        size.width * 0.5415360,
        size.height * 0.3738742);
    path_93.cubicTo(
        size.width * 0.6250360,
        size.height * 0.3738742,
        size.width * 0.7277698,
        size.height * 0.3675203,
        size.width * 0.7800935,
        size.height * 0.4322586);
    path_93.cubicTo(
        size.width * 0.8494101,
        size.height * 0.5180219,
        size.width * 0.7875108,
        size.height * 0.5677648,
        size.width * 0.8144101,
        size.height * 0.6405977);
    path_93.cubicTo(
        size.width * 0.8413165,
        size.height * 0.7134312,
        size.width * 0.8926403,
        size.height * 0.7127305,
        size.width * 0.9156475,
        size.height * 0.7947109);
    path_93.close();

    Paint paint_93_fill = Paint()..style = PaintingStyle.fill;
    paint_93_fill.color = Colors.black;
    canvas.drawPath(path_93, paint_93_fill);

    Path path_94 = Path();
    path_94.moveTo(size.width * 0.9156475, size.height * 0.7947109);
    path_94.cubicTo(
        size.width * 0.9546547,
        size.height * 0.9337344,
        size.width * 0.7505899,
        size.height * 0.9954844,
        size.width * 0.5461331,
        size.height * 0.9932656);
    path_94.cubicTo(
        size.width * 0.3279971,
        size.height * 0.9908984,
        size.width * 0.1556360,
        size.height * 0.9362109,
        size.width * 0.1918878,
        size.height * 0.7947109);
    path_94.cubicTo(
        size.width * 0.2014921,
        size.height * 0.7572266,
        size.width * 0.2547612,
        size.height * 0.7221508,
        size.width * 0.2774525,
        size.height * 0.6516477);
    path_94.cubicTo(
        size.width * 0.2981194,
        size.height * 0.5874352,
        size.width * 0.2482763,
        size.height * 0.5180219,
        size.width * 0.2994734,
        size.height * 0.4575852);
    path_94.cubicTo(
        size.width * 0.3563568,
        size.height * 0.3904344,
        size.width * 0.4510827,
        size.height * 0.3738742,
        size.width * 0.5415360,
        size.height * 0.3738742);
    path_94.cubicTo(
        size.width * 0.6250360,
        size.height * 0.3738742,
        size.width * 0.7277698,
        size.height * 0.3675203,
        size.width * 0.7800935,
        size.height * 0.4322586);
    path_94.cubicTo(
        size.width * 0.8494101,
        size.height * 0.5180219,
        size.width * 0.7875108,
        size.height * 0.5677648,
        size.width * 0.8144101,
        size.height * 0.6405977);
    path_94.cubicTo(
        size.width * 0.8413165,
        size.height * 0.7134312,
        size.width * 0.8926403,
        size.height * 0.7127305,
        size.width * 0.9156475,
        size.height * 0.7947109);
    path_94.close();

    Paint paint_94_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_94_stroke.color = Colors.black;
    canvas.drawPath(path_94, paint_94_stroke);

    Paint paint_94_fill = Paint()..style = PaintingStyle.fill;
    paint_94_fill.color = Colors.black;
    canvas.drawPath(path_94, paint_94_fill);

    Path path_95 = Path();
    path_95.moveTo(size.width * 0.3847273, size.height * 0.4714891);
    path_95.cubicTo(
        size.width * 0.3976734,
        size.height * 0.4714891,
        size.width * 0.4081683,
        size.height * 0.4600922,
        size.width * 0.4081683,
        size.height * 0.4460336);
    path_95.cubicTo(
        size.width * 0.4081683,
        size.height * 0.4319750,
        size.width * 0.3976734,
        size.height * 0.4205781,
        size.width * 0.3847273,
        size.height * 0.4205781);
    path_95.cubicTo(
        size.width * 0.3717806,
        size.height * 0.4205781,
        size.width * 0.3612863,
        size.height * 0.4319750,
        size.width * 0.3612863,
        size.height * 0.4460336);
    path_95.cubicTo(
        size.width * 0.3612863,
        size.height * 0.4600922,
        size.width * 0.3717806,
        size.height * 0.4714891,
        size.width * 0.3847273,
        size.height * 0.4714891);
    path_95.close();

    Paint paint_95_fill = Paint()..style = PaintingStyle.fill;
    paint_95_fill.color = Colors.black;
    canvas.drawPath(path_95, paint_95_fill);

    Path path_96 = Path();
    path_96.moveTo(size.width * 0.3847273, size.height * 0.4714891);
    path_96.cubicTo(
        size.width * 0.3976734,
        size.height * 0.4714891,
        size.width * 0.4081683,
        size.height * 0.4600922,
        size.width * 0.4081683,
        size.height * 0.4460336);
    path_96.cubicTo(
        size.width * 0.4081683,
        size.height * 0.4319750,
        size.width * 0.3976734,
        size.height * 0.4205781,
        size.width * 0.3847273,
        size.height * 0.4205781);
    path_96.cubicTo(
        size.width * 0.3717806,
        size.height * 0.4205781,
        size.width * 0.3612863,
        size.height * 0.4319750,
        size.width * 0.3612863,
        size.height * 0.4460336);
    path_96.cubicTo(
        size.width * 0.3612863,
        size.height * 0.4600922,
        size.width * 0.3717806,
        size.height * 0.4714891,
        size.width * 0.3847273,
        size.height * 0.4714891);
    path_96.close();

    Paint paint_96_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_96_stroke.color = Colors.black;
    canvas.drawPath(path_96, paint_96_stroke);

    Paint paint_96_fill = Paint()..style = PaintingStyle.fill;
    paint_96_fill.color = Colors.white;
    canvas.drawPath(path_96, paint_96_fill);

    Path path_97 = Path();
    path_97.moveTo(size.width * 0.5538058, size.height * 0.4714891);
    path_97.cubicTo(
        size.width * 0.5667518,
        size.height * 0.4714891,
        size.width * 0.5772468,
        size.height * 0.4600922,
        size.width * 0.5772468,
        size.height * 0.4460336);
    path_97.cubicTo(
        size.width * 0.5772468,
        size.height * 0.4319750,
        size.width * 0.5667518,
        size.height * 0.4205781,
        size.width * 0.5538058,
        size.height * 0.4205781);
    path_97.cubicTo(
        size.width * 0.5408597,
        size.height * 0.4205781,
        size.width * 0.5303647,
        size.height * 0.4319750,
        size.width * 0.5303647,
        size.height * 0.4460336);
    path_97.cubicTo(
        size.width * 0.5303647,
        size.height * 0.4600922,
        size.width * 0.5408597,
        size.height * 0.4714891,
        size.width * 0.5538058,
        size.height * 0.4714891);
    path_97.close();

    Paint paint_97_fill = Paint()..style = PaintingStyle.fill;
    paint_97_fill.color = Colors.black;
    canvas.drawPath(path_97, paint_97_fill);

    Path path_98 = Path();
    path_98.moveTo(size.width * 0.5538058, size.height * 0.4714891);
    path_98.cubicTo(
        size.width * 0.5667518,
        size.height * 0.4714891,
        size.width * 0.5772468,
        size.height * 0.4600922,
        size.width * 0.5772468,
        size.height * 0.4460336);
    path_98.cubicTo(
        size.width * 0.5772468,
        size.height * 0.4319750,
        size.width * 0.5667518,
        size.height * 0.4205781,
        size.width * 0.5538058,
        size.height * 0.4205781);
    path_98.cubicTo(
        size.width * 0.5408597,
        size.height * 0.4205781,
        size.width * 0.5303647,
        size.height * 0.4319750,
        size.width * 0.5303647,
        size.height * 0.4460336);
    path_98.cubicTo(
        size.width * 0.5303647,
        size.height * 0.4600922,
        size.width * 0.5408597,
        size.height * 0.4714891,
        size.width * 0.5538058,
        size.height * 0.4714891);
    path_98.close();

    Paint paint_98_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_98_stroke.color = Colors.black;
    canvas.drawPath(path_98, paint_98_stroke);

    Paint paint_98_fill = Paint()..style = PaintingStyle.fill;
    paint_98_fill.color = Colors.white;
    canvas.drawPath(path_98, paint_98_fill);

    Path path_99 = Path();
    path_99.moveTo(size.width * 0.7092827, size.height * 0.4714891);
    path_99.cubicTo(
        size.width * 0.7222302,
        size.height * 0.4714891,
        size.width * 0.7327266,
        size.height * 0.4600922,
        size.width * 0.7327266,
        size.height * 0.4460336);
    path_99.cubicTo(
        size.width * 0.7327266,
        size.height * 0.4319750,
        size.width * 0.7222302,
        size.height * 0.4205781,
        size.width * 0.7092827,
        size.height * 0.4205781);
    path_99.cubicTo(
        size.width * 0.6963367,
        size.height * 0.4205781,
        size.width * 0.6858417,
        size.height * 0.4319750,
        size.width * 0.6858417,
        size.height * 0.4460336);
    path_99.cubicTo(
        size.width * 0.6858417,
        size.height * 0.4600922,
        size.width * 0.6963367,
        size.height * 0.4714891,
        size.width * 0.7092827,
        size.height * 0.4714891);
    path_99.close();

    Paint paint_99_fill = Paint()..style = PaintingStyle.fill;
    paint_99_fill.color = Colors.black;
    canvas.drawPath(path_99, paint_99_fill);

    Path path_100 = Path();
    path_100.moveTo(size.width * 0.7092827, size.height * 0.4714891);
    path_100.cubicTo(
        size.width * 0.7222302,
        size.height * 0.4714891,
        size.width * 0.7327266,
        size.height * 0.4600922,
        size.width * 0.7327266,
        size.height * 0.4460336);
    path_100.cubicTo(
        size.width * 0.7327266,
        size.height * 0.4319750,
        size.width * 0.7222302,
        size.height * 0.4205781,
        size.width * 0.7092827,
        size.height * 0.4205781);
    path_100.cubicTo(
        size.width * 0.6963367,
        size.height * 0.4205781,
        size.width * 0.6858417,
        size.height * 0.4319750,
        size.width * 0.6858417,
        size.height * 0.4460336);
    path_100.cubicTo(
        size.width * 0.6858417,
        size.height * 0.4600922,
        size.width * 0.6963367,
        size.height * 0.4714891,
        size.width * 0.7092827,
        size.height * 0.4714891);
    path_100.close();

    Paint paint_100_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_100_stroke.color = Colors.black;
    canvas.drawPath(path_100, paint_100_stroke);

    Paint paint_100_fill = Paint()..style = PaintingStyle.fill;
    paint_100_fill.color = Colors.white;
    canvas.drawPath(path_100, paint_100_fill);

    Path path_101 = Path();
    path_101.moveTo(size.width * 0.4490259, size.height * 0.5601273);
    path_101.cubicTo(
        size.width * 0.4619719,
        size.height * 0.5601273,
        size.width * 0.4724669,
        size.height * 0.5487305,
        size.width * 0.4724669,
        size.height * 0.5346719);
    path_101.cubicTo(
        size.width * 0.4724669,
        size.height * 0.5206133,
        size.width * 0.4619719,
        size.height * 0.5092164,
        size.width * 0.4490259,
        size.height * 0.5092164);
    path_101.cubicTo(
        size.width * 0.4360791,
        size.height * 0.5092164,
        size.width * 0.4255842,
        size.height * 0.5206133,
        size.width * 0.4255842,
        size.height * 0.5346719);
    path_101.cubicTo(
        size.width * 0.4255842,
        size.height * 0.5487305,
        size.width * 0.4360791,
        size.height * 0.5601273,
        size.width * 0.4490259,
        size.height * 0.5601273);
    path_101.close();

    Paint paint_101_fill = Paint()..style = PaintingStyle.fill;
    paint_101_fill.color = Colors.black;
    canvas.drawPath(path_101, paint_101_fill);

    Path path_102 = Path();
    path_102.moveTo(size.width * 0.4490259, size.height * 0.5601273);
    path_102.cubicTo(
        size.width * 0.4619719,
        size.height * 0.5601273,
        size.width * 0.4724669,
        size.height * 0.5487305,
        size.width * 0.4724669,
        size.height * 0.5346719);
    path_102.cubicTo(
        size.width * 0.4724669,
        size.height * 0.5206133,
        size.width * 0.4619719,
        size.height * 0.5092164,
        size.width * 0.4490259,
        size.height * 0.5092164);
    path_102.cubicTo(
        size.width * 0.4360791,
        size.height * 0.5092164,
        size.width * 0.4255842,
        size.height * 0.5206133,
        size.width * 0.4255842,
        size.height * 0.5346719);
    path_102.cubicTo(
        size.width * 0.4255842,
        size.height * 0.5487305,
        size.width * 0.4360791,
        size.height * 0.5601273,
        size.width * 0.4490259,
        size.height * 0.5601273);
    path_102.close();

    Paint paint_102_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_102_stroke.color = Colors.black;
    canvas.drawPath(path_102, paint_102_stroke);

    Paint paint_102_fill = Paint()..style = PaintingStyle.fill;
    paint_102_fill.color = Colors.white;
    canvas.drawPath(path_102, paint_102_fill);

    Path path_103 = Path();
    path_103.moveTo(size.width * 0.6351201, size.height * 0.5527883);
    path_103.cubicTo(
        size.width * 0.6480669,
        size.height * 0.5527883,
        size.width * 0.6585619,
        size.height * 0.5413914,
        size.width * 0.6585619,
        size.height * 0.5273320);
    path_103.cubicTo(
        size.width * 0.6585619,
        size.height * 0.5132734,
        size.width * 0.6480669,
        size.height * 0.5018766,
        size.width * 0.6351201,
        size.height * 0.5018766);
    path_103.cubicTo(
        size.width * 0.6221741,
        size.height * 0.5018766,
        size.width * 0.6116791,
        size.height * 0.5132734,
        size.width * 0.6116791,
        size.height * 0.5273320);
    path_103.cubicTo(
        size.width * 0.6116791,
        size.height * 0.5413914,
        size.width * 0.6221741,
        size.height * 0.5527883,
        size.width * 0.6351201,
        size.height * 0.5527883);
    path_103.close();

    Paint paint_103_fill = Paint()..style = PaintingStyle.fill;
    paint_103_fill.color = Colors.black;
    canvas.drawPath(path_103, paint_103_fill);

    Path path_104 = Path();
    path_104.moveTo(size.width * 0.6351201, size.height * 0.5527883);
    path_104.cubicTo(
        size.width * 0.6480669,
        size.height * 0.5527883,
        size.width * 0.6585619,
        size.height * 0.5413914,
        size.width * 0.6585619,
        size.height * 0.5273320);
    path_104.cubicTo(
        size.width * 0.6585619,
        size.height * 0.5132734,
        size.width * 0.6480669,
        size.height * 0.5018766,
        size.width * 0.6351201,
        size.height * 0.5018766);
    path_104.cubicTo(
        size.width * 0.6221741,
        size.height * 0.5018766,
        size.width * 0.6116791,
        size.height * 0.5132734,
        size.width * 0.6116791,
        size.height * 0.5273320);
    path_104.cubicTo(
        size.width * 0.6116791,
        size.height * 0.5413914,
        size.width * 0.6221741,
        size.height * 0.5527883,
        size.width * 0.6351201,
        size.height * 0.5527883);
    path_104.close();

    Paint paint_104_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_104_stroke.color = Colors.black;
    canvas.drawPath(path_104, paint_104_stroke);

    Paint paint_104_fill = Paint()..style = PaintingStyle.fill;
    paint_104_fill.color = Colors.white;
    canvas.drawPath(path_104, paint_104_fill);

    Path path_105 = Path();
    path_105.moveTo(size.width * 0.5448835, size.height * 0.6588516);
    path_105.cubicTo(
        size.width * 0.5578295,
        size.height * 0.6588516,
        size.width * 0.5683245,
        size.height * 0.6474547,
        size.width * 0.5683245,
        size.height * 0.6333961);
    path_105.cubicTo(
        size.width * 0.5683245,
        size.height * 0.6193375,
        size.width * 0.5578295,
        size.height * 0.6079406,
        size.width * 0.5448835,
        size.height * 0.6079406);
    path_105.cubicTo(
        size.width * 0.5319374,
        size.height * 0.6079406,
        size.width * 0.5214424,
        size.height * 0.6193375,
        size.width * 0.5214424,
        size.height * 0.6333961);
    path_105.cubicTo(
        size.width * 0.5214424,
        size.height * 0.6474547,
        size.width * 0.5319374,
        size.height * 0.6588516,
        size.width * 0.5448835,
        size.height * 0.6588516);
    path_105.close();

    Paint paint_105_fill = Paint()..style = PaintingStyle.fill;
    paint_105_fill.color = Colors.black;
    canvas.drawPath(path_105, paint_105_fill);

    Path path_106 = Path();
    path_106.moveTo(size.width * 0.5448835, size.height * 0.6588516);
    path_106.cubicTo(
        size.width * 0.5578295,
        size.height * 0.6588516,
        size.width * 0.5683245,
        size.height * 0.6474547,
        size.width * 0.5683245,
        size.height * 0.6333961);
    path_106.cubicTo(
        size.width * 0.5683245,
        size.height * 0.6193375,
        size.width * 0.5578295,
        size.height * 0.6079406,
        size.width * 0.5448835,
        size.height * 0.6079406);
    path_106.cubicTo(
        size.width * 0.5319374,
        size.height * 0.6079406,
        size.width * 0.5214424,
        size.height * 0.6193375,
        size.width * 0.5214424,
        size.height * 0.6333961);
    path_106.cubicTo(
        size.width * 0.5214424,
        size.height * 0.6474547,
        size.width * 0.5319374,
        size.height * 0.6588516,
        size.width * 0.5448835,
        size.height * 0.6588516);
    path_106.close();

    Paint paint_106_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_106_stroke.color = Colors.black;
    canvas.drawPath(path_106, paint_106_stroke);

    Paint paint_106_fill = Paint()..style = PaintingStyle.fill;
    paint_106_fill.color = Colors.white;
    canvas.drawPath(path_106, paint_106_fill);

    Path path_107 = Path();
    path_107.moveTo(size.width * 0.3650691, size.height * 0.6562117);
    path_107.cubicTo(
        size.width * 0.3780158,
        size.height * 0.6562117,
        size.width * 0.3885101,
        size.height * 0.6448148,
        size.width * 0.3885101,
        size.height * 0.6307563);
    path_107.cubicTo(
        size.width * 0.3885101,
        size.height * 0.6166977,
        size.width * 0.3780158,
        size.height * 0.6053008,
        size.width * 0.3650691,
        size.height * 0.6053008);
    path_107.cubicTo(
        size.width * 0.3521230,
        size.height * 0.6053008,
        size.width * 0.3416281,
        size.height * 0.6166977,
        size.width * 0.3416281,
        size.height * 0.6307563);
    path_107.cubicTo(
        size.width * 0.3416281,
        size.height * 0.6448148,
        size.width * 0.3521230,
        size.height * 0.6562117,
        size.width * 0.3650691,
        size.height * 0.6562117);
    path_107.close();

    Paint paint_107_fill = Paint()..style = PaintingStyle.fill;
    paint_107_fill.color = Colors.black;
    canvas.drawPath(path_107, paint_107_fill);

    Path path_108 = Path();
    path_108.moveTo(size.width * 0.3650691, size.height * 0.6562117);
    path_108.cubicTo(
        size.width * 0.3780158,
        size.height * 0.6562117,
        size.width * 0.3885101,
        size.height * 0.6448148,
        size.width * 0.3885101,
        size.height * 0.6307563);
    path_108.cubicTo(
        size.width * 0.3885101,
        size.height * 0.6166977,
        size.width * 0.3780158,
        size.height * 0.6053008,
        size.width * 0.3650691,
        size.height * 0.6053008);
    path_108.cubicTo(
        size.width * 0.3521230,
        size.height * 0.6053008,
        size.width * 0.3416281,
        size.height * 0.6166977,
        size.width * 0.3416281,
        size.height * 0.6307563);
    path_108.cubicTo(
        size.width * 0.3416281,
        size.height * 0.6448148,
        size.width * 0.3521230,
        size.height * 0.6562117,
        size.width * 0.3650691,
        size.height * 0.6562117);
    path_108.close();

    Paint paint_108_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_108_stroke.color = Colors.black;
    canvas.drawPath(path_108, paint_108_stroke);

    Paint paint_108_fill = Paint()..style = PaintingStyle.fill;
    paint_108_fill.color = Colors.white;
    canvas.drawPath(path_108, paint_108_fill);

    Path path_109 = Path();
    path_109.moveTo(size.width * 0.7240791, size.height * 0.6588516);
    path_109.cubicTo(
        size.width * 0.7370216,
        size.height * 0.6588516,
        size.width * 0.7475180,
        size.height * 0.6474547,
        size.width * 0.7475180,
        size.height * 0.6333961);
    path_109.cubicTo(
        size.width * 0.7475180,
        size.height * 0.6193375,
        size.width * 0.7370216,
        size.height * 0.6079406,
        size.width * 0.7240791,
        size.height * 0.6079406);
    path_109.cubicTo(
        size.width * 0.7111331,
        size.height * 0.6079406,
        size.width * 0.7006381,
        size.height * 0.6193375,
        size.width * 0.7006381,
        size.height * 0.6333961);
    path_109.cubicTo(
        size.width * 0.7006381,
        size.height * 0.6474547,
        size.width * 0.7111331,
        size.height * 0.6588516,
        size.width * 0.7240791,
        size.height * 0.6588516);
    path_109.close();

    Paint paint_109_fill = Paint()..style = PaintingStyle.fill;
    paint_109_fill.color = Colors.black;
    canvas.drawPath(path_109, paint_109_fill);

    Path path_110 = Path();
    path_110.moveTo(size.width * 0.7240791, size.height * 0.6588516);
    path_110.cubicTo(
        size.width * 0.7370216,
        size.height * 0.6588516,
        size.width * 0.7475180,
        size.height * 0.6474547,
        size.width * 0.7475180,
        size.height * 0.6333961);
    path_110.cubicTo(
        size.width * 0.7475180,
        size.height * 0.6193375,
        size.width * 0.7370216,
        size.height * 0.6079406,
        size.width * 0.7240791,
        size.height * 0.6079406);
    path_110.cubicTo(
        size.width * 0.7111331,
        size.height * 0.6079406,
        size.width * 0.7006381,
        size.height * 0.6193375,
        size.width * 0.7006381,
        size.height * 0.6333961);
    path_110.cubicTo(
        size.width * 0.7006381,
        size.height * 0.6474547,
        size.width * 0.7111331,
        size.height * 0.6588516,
        size.width * 0.7240791,
        size.height * 0.6588516);
    path_110.close();

    Paint paint_110_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_110_stroke.color = Colors.black;
    canvas.drawPath(path_110, paint_110_stroke);

    Paint paint_110_fill = Paint()..style = PaintingStyle.fill;
    paint_110_fill.color = Colors.white;
    canvas.drawPath(path_110, paint_110_fill);

    Path path_111 = Path();
    path_111.moveTo(size.width * 0.4490259, size.height * 0.7621078);
    path_111.cubicTo(
        size.width * 0.4619719,
        size.height * 0.7621078,
        size.width * 0.4724669,
        size.height * 0.7507109,
        size.width * 0.4724669,
        size.height * 0.7366523);
    path_111.cubicTo(
        size.width * 0.4724669,
        size.height * 0.7225938,
        size.width * 0.4619719,
        size.height * 0.7111969,
        size.width * 0.4490259,
        size.height * 0.7111969);
    path_111.cubicTo(
        size.width * 0.4360791,
        size.height * 0.7111969,
        size.width * 0.4255842,
        size.height * 0.7225938,
        size.width * 0.4255842,
        size.height * 0.7366523);
    path_111.cubicTo(
        size.width * 0.4255842,
        size.height * 0.7507109,
        size.width * 0.4360791,
        size.height * 0.7621078,
        size.width * 0.4490259,
        size.height * 0.7621078);
    path_111.close();

    Paint paint_111_fill = Paint()..style = PaintingStyle.fill;
    paint_111_fill.color = Colors.black;
    canvas.drawPath(path_111, paint_111_fill);

    Path path_112 = Path();
    path_112.moveTo(size.width * 0.4490259, size.height * 0.7621078);
    path_112.cubicTo(
        size.width * 0.4619719,
        size.height * 0.7621078,
        size.width * 0.4724669,
        size.height * 0.7507109,
        size.width * 0.4724669,
        size.height * 0.7366523);
    path_112.cubicTo(
        size.width * 0.4724669,
        size.height * 0.7225938,
        size.width * 0.4619719,
        size.height * 0.7111969,
        size.width * 0.4490259,
        size.height * 0.7111969);
    path_112.cubicTo(
        size.width * 0.4360791,
        size.height * 0.7111969,
        size.width * 0.4255842,
        size.height * 0.7225938,
        size.width * 0.4255842,
        size.height * 0.7366523);
    path_112.cubicTo(
        size.width * 0.4255842,
        size.height * 0.7507109,
        size.width * 0.4360791,
        size.height * 0.7621078,
        size.width * 0.4490259,
        size.height * 0.7621078);
    path_112.close();

    Paint paint_112_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_112_stroke.color = Colors.black;
    canvas.drawPath(path_112, paint_112_stroke);

    Paint paint_112_fill = Paint()..style = PaintingStyle.fill;
    paint_112_fill.color = Colors.white;
    canvas.drawPath(path_112, paint_112_fill);

    Path path_113 = Path();
    path_113.moveTo(size.width * 0.6351201, size.height * 0.7676164);
    path_113.cubicTo(
        size.width * 0.6480669,
        size.height * 0.7676164,
        size.width * 0.6585619,
        size.height * 0.7562195,
        size.width * 0.6585619,
        size.height * 0.7421609);
    path_113.cubicTo(
        size.width * 0.6585619,
        size.height * 0.7281023,
        size.width * 0.6480669,
        size.height * 0.7167055,
        size.width * 0.6351201,
        size.height * 0.7167055);
    path_113.cubicTo(
        size.width * 0.6221741,
        size.height * 0.7167055,
        size.width * 0.6116791,
        size.height * 0.7281023,
        size.width * 0.6116791,
        size.height * 0.7421609);
    path_113.cubicTo(
        size.width * 0.6116791,
        size.height * 0.7562195,
        size.width * 0.6221741,
        size.height * 0.7676164,
        size.width * 0.6351201,
        size.height * 0.7676164);
    path_113.close();

    Paint paint_113_fill = Paint()..style = PaintingStyle.fill;
    paint_113_fill.color = Colors.black;
    canvas.drawPath(path_113, paint_113_fill);

    Path path_114 = Path();
    path_114.moveTo(size.width * 0.6351201, size.height * 0.7676164);
    path_114.cubicTo(
        size.width * 0.6480669,
        size.height * 0.7676164,
        size.width * 0.6585619,
        size.height * 0.7562195,
        size.width * 0.6585619,
        size.height * 0.7421609);
    path_114.cubicTo(
        size.width * 0.6585619,
        size.height * 0.7281023,
        size.width * 0.6480669,
        size.height * 0.7167055,
        size.width * 0.6351201,
        size.height * 0.7167055);
    path_114.cubicTo(
        size.width * 0.6221741,
        size.height * 0.7167055,
        size.width * 0.6116791,
        size.height * 0.7281023,
        size.width * 0.6116791,
        size.height * 0.7421609);
    path_114.cubicTo(
        size.width * 0.6116791,
        size.height * 0.7562195,
        size.width * 0.6221741,
        size.height * 0.7676164,
        size.width * 0.6351201,
        size.height * 0.7676164);
    path_114.close();

    Paint paint_114_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_114_stroke.color = Colors.black;
    canvas.drawPath(path_114, paint_114_stroke);

    Paint paint_114_fill = Paint()..style = PaintingStyle.fill;
    paint_114_fill.color = Colors.white;
    canvas.drawPath(path_114, paint_114_fill);

    Path path_115 = Path();
    path_115.moveTo(size.width * 0.3416878, size.height * 0.8710703);
    path_115.cubicTo(
        size.width * 0.3546345,
        size.height * 0.8710703,
        size.width * 0.3651288,
        size.height * 0.8596719,
        size.width * 0.3651288,
        size.height * 0.8456172);
    path_115.cubicTo(
        size.width * 0.3651288,
        size.height * 0.8315547,
        size.width * 0.3546345,
        size.height * 0.8201563,
        size.width * 0.3416878,
        size.height * 0.8201563);
    path_115.cubicTo(
        size.width * 0.3287417,
        size.height * 0.8201563,
        size.width * 0.3182468,
        size.height * 0.8315547,
        size.width * 0.3182468,
        size.height * 0.8456172);
    path_115.cubicTo(
        size.width * 0.3182468,
        size.height * 0.8596719,
        size.width * 0.3287417,
        size.height * 0.8710703,
        size.width * 0.3416878,
        size.height * 0.8710703);
    path_115.close();

    Paint paint_115_fill = Paint()..style = PaintingStyle.fill;
    paint_115_fill.color = Colors.black;
    canvas.drawPath(path_115, paint_115_fill);

    Path path_116 = Path();
    path_116.moveTo(size.width * 0.3416878, size.height * 0.8710703);
    path_116.cubicTo(
        size.width * 0.3546345,
        size.height * 0.8710703,
        size.width * 0.3651288,
        size.height * 0.8596719,
        size.width * 0.3651288,
        size.height * 0.8456172);
    path_116.cubicTo(
        size.width * 0.3651288,
        size.height * 0.8315547,
        size.width * 0.3546345,
        size.height * 0.8201563,
        size.width * 0.3416878,
        size.height * 0.8201563);
    path_116.cubicTo(
        size.width * 0.3287417,
        size.height * 0.8201563,
        size.width * 0.3182468,
        size.height * 0.8315547,
        size.width * 0.3182468,
        size.height * 0.8456172);
    path_116.cubicTo(
        size.width * 0.3182468,
        size.height * 0.8596719,
        size.width * 0.3287417,
        size.height * 0.8710703,
        size.width * 0.3416878,
        size.height * 0.8710703);
    path_116.close();

    Paint paint_116_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_116_stroke.color = Colors.black;
    canvas.drawPath(path_116, paint_116_stroke);

    Paint paint_116_fill = Paint()..style = PaintingStyle.fill;
    paint_116_fill.color = Colors.white;
    canvas.drawPath(path_116, paint_116_fill);

    Path path_117 = Path();
    path_117.moveTo(size.width * 0.5448835, size.height * 0.8606406);
    path_117.cubicTo(
        size.width * 0.5578295,
        size.height * 0.8606406,
        size.width * 0.5683245,
        size.height * 0.8492422,
        size.width * 0.5683245,
        size.height * 0.8351875);
    path_117.cubicTo(
        size.width * 0.5683245,
        size.height * 0.8211250,
        size.width * 0.5578295,
        size.height * 0.8097344,
        size.width * 0.5448835,
        size.height * 0.8097344);
    path_117.cubicTo(
        size.width * 0.5319374,
        size.height * 0.8097344,
        size.width * 0.5214424,
        size.height * 0.8211250,
        size.width * 0.5214424,
        size.height * 0.8351875);
    path_117.cubicTo(
        size.width * 0.5214424,
        size.height * 0.8492422,
        size.width * 0.5319374,
        size.height * 0.8606406,
        size.width * 0.5448835,
        size.height * 0.8606406);
    path_117.close();

    Paint paint_117_fill = Paint()..style = PaintingStyle.fill;
    paint_117_fill.color = Colors.black;
    canvas.drawPath(path_117, paint_117_fill);

    Path path_118 = Path();
    path_118.moveTo(size.width * 0.5448835, size.height * 0.8606406);
    path_118.cubicTo(
        size.width * 0.5578295,
        size.height * 0.8606406,
        size.width * 0.5683245,
        size.height * 0.8492422,
        size.width * 0.5683245,
        size.height * 0.8351875);
    path_118.cubicTo(
        size.width * 0.5683245,
        size.height * 0.8211250,
        size.width * 0.5578295,
        size.height * 0.8097344,
        size.width * 0.5448835,
        size.height * 0.8097344);
    path_118.cubicTo(
        size.width * 0.5319374,
        size.height * 0.8097344,
        size.width * 0.5214424,
        size.height * 0.8211250,
        size.width * 0.5214424,
        size.height * 0.8351875);
    path_118.cubicTo(
        size.width * 0.5214424,
        size.height * 0.8492422,
        size.width * 0.5319374,
        size.height * 0.8606406,
        size.width * 0.5448835,
        size.height * 0.8606406);
    path_118.close();

    Paint paint_118_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_118_stroke.color = Colors.black;
    canvas.drawPath(path_118, paint_118_stroke);

    Paint paint_118_fill = Paint()..style = PaintingStyle.fill;
    paint_118_fill.color = Colors.white;
    canvas.drawPath(path_118, paint_118_fill);

    Path path_119 = Path();
    path_119.moveTo(size.width * 0.7561439, size.height * 0.8529922);
    path_119.cubicTo(
        size.width * 0.7690935,
        size.height * 0.8529922,
        size.width * 0.7795827,
        size.height * 0.8415938,
        size.width * 0.7795827,
        size.height * 0.8275313);
    path_119.cubicTo(
        size.width * 0.7795827,
        size.height * 0.8134766,
        size.width * 0.7690935,
        size.height * 0.8020781,
        size.width * 0.7561439,
        size.height * 0.8020781);
    path_119.cubicTo(
        size.width * 0.7431942,
        size.height * 0.8020781,
        size.width * 0.7327050,
        size.height * 0.8134766,
        size.width * 0.7327050,
        size.height * 0.8275313);
    path_119.cubicTo(
        size.width * 0.7327050,
        size.height * 0.8415938,
        size.width * 0.7431942,
        size.height * 0.8529922,
        size.width * 0.7561439,
        size.height * 0.8529922);
    path_119.close();

    Paint paint_119_fill = Paint()..style = PaintingStyle.fill;
    paint_119_fill.color = Colors.black;
    canvas.drawPath(path_119, paint_119_fill);

    Path path_120 = Path();
    path_120.moveTo(size.width * 0.7561439, size.height * 0.8529922);
    path_120.cubicTo(
        size.width * 0.7690935,
        size.height * 0.8529922,
        size.width * 0.7795827,
        size.height * 0.8415938,
        size.width * 0.7795827,
        size.height * 0.8275313);
    path_120.cubicTo(
        size.width * 0.7795827,
        size.height * 0.8134766,
        size.width * 0.7690935,
        size.height * 0.8020781,
        size.width * 0.7561439,
        size.height * 0.8020781);
    path_120.cubicTo(
        size.width * 0.7431942,
        size.height * 0.8020781,
        size.width * 0.7327050,
        size.height * 0.8134766,
        size.width * 0.7327050,
        size.height * 0.8275313);
    path_120.cubicTo(
        size.width * 0.7327050,
        size.height * 0.8415938,
        size.width * 0.7431942,
        size.height * 0.8529922,
        size.width * 0.7561439,
        size.height * 0.8529922);
    path_120.close();

    Paint paint_120_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_120_stroke.color = Colors.black;
    canvas.drawPath(path_120, paint_120_stroke);

    Paint paint_120_fill = Paint()..style = PaintingStyle.fill;
    paint_120_fill.color = Colors.white;
    canvas.drawPath(path_120, paint_120_fill);

    Path path_121 = Path();
    path_121.moveTo(size.width * 0.8211295, size.height * 0.7421648);
    path_121.cubicTo(
        size.width * 0.8340791,
        size.height * 0.7421648,
        size.width * 0.8445755,
        size.height * 0.7307680,
        size.width * 0.8445755,
        size.height * 0.7167094);
    path_121.cubicTo(
        size.width * 0.8445755,
        size.height * 0.7026508,
        size.width * 0.8340791,
        size.height * 0.6912539,
        size.width * 0.8211295,
        size.height * 0.6912539);
    path_121.cubicTo(
        size.width * 0.8081871,
        size.height * 0.6912539,
        size.width * 0.7976906,
        size.height * 0.7026508,
        size.width * 0.7976906,
        size.height * 0.7167094);
    path_121.cubicTo(
        size.width * 0.7976906,
        size.height * 0.7307680,
        size.width * 0.8081871,
        size.height * 0.7421648,
        size.width * 0.8211295,
        size.height * 0.7421648);
    path_121.close();

    Paint paint_121_fill = Paint()..style = PaintingStyle.fill;
    paint_121_fill.color = Colors.black;
    canvas.drawPath(path_121, paint_121_fill);

    Path path_122 = Path();
    path_122.moveTo(size.width * 0.8211295, size.height * 0.7421648);
    path_122.cubicTo(
        size.width * 0.8340791,
        size.height * 0.7421648,
        size.width * 0.8445755,
        size.height * 0.7307680,
        size.width * 0.8445755,
        size.height * 0.7167094);
    path_122.cubicTo(
        size.width * 0.8445755,
        size.height * 0.7026508,
        size.width * 0.8340791,
        size.height * 0.6912539,
        size.width * 0.8211295,
        size.height * 0.6912539);
    path_122.cubicTo(
        size.width * 0.8081871,
        size.height * 0.6912539,
        size.width * 0.7976906,
        size.height * 0.7026508,
        size.width * 0.7976906,
        size.height * 0.7167094);
    path_122.cubicTo(
        size.width * 0.7976906,
        size.height * 0.7307680,
        size.width * 0.8081871,
        size.height * 0.7421648,
        size.width * 0.8211295,
        size.height * 0.7421648);
    path_122.close();

    Paint paint_122_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_122_stroke.color = Colors.black;
    canvas.drawPath(path_122, paint_122_stroke);

    Paint paint_122_fill = Paint()..style = PaintingStyle.fill;
    paint_122_fill.color = Colors.white;
    canvas.drawPath(path_122, paint_122_fill);

    Path path_123 = Path();
    path_123.moveTo(size.width * 0.2809165, size.height * 0.7421648);
    path_123.cubicTo(
        size.width * 0.2938626,
        size.height * 0.7421648,
        size.width * 0.3043576,
        size.height * 0.7307680,
        size.width * 0.3043576,
        size.height * 0.7167094);
    path_123.cubicTo(
        size.width * 0.3043576,
        size.height * 0.7026508,
        size.width * 0.2938626,
        size.height * 0.6912539,
        size.width * 0.2809165,
        size.height * 0.6912539);
    path_123.cubicTo(
        size.width * 0.2679705,
        size.height * 0.6912539,
        size.width * 0.2574755,
        size.height * 0.7026508,
        size.width * 0.2574755,
        size.height * 0.7167094);
    path_123.cubicTo(
        size.width * 0.2574755,
        size.height * 0.7307680,
        size.width * 0.2679705,
        size.height * 0.7421648,
        size.width * 0.2809165,
        size.height * 0.7421648);
    path_123.close();

    Paint paint_123_fill = Paint()..style = PaintingStyle.fill;
    paint_123_fill.color = Colors.black;
    canvas.drawPath(path_123, paint_123_fill);

    Path path_124 = Path();
    path_124.moveTo(size.width * 0.2809165, size.height * 0.7421648);
    path_124.cubicTo(
        size.width * 0.2938626,
        size.height * 0.7421648,
        size.width * 0.3043576,
        size.height * 0.7307680,
        size.width * 0.3043576,
        size.height * 0.7167094);
    path_124.cubicTo(
        size.width * 0.3043576,
        size.height * 0.7026508,
        size.width * 0.2938626,
        size.height * 0.6912539,
        size.width * 0.2809165,
        size.height * 0.6912539);
    path_124.cubicTo(
        size.width * 0.2679705,
        size.height * 0.6912539,
        size.width * 0.2574755,
        size.height * 0.7026508,
        size.width * 0.2574755,
        size.height * 0.7167094);
    path_124.cubicTo(
        size.width * 0.2574755,
        size.height * 0.7307680,
        size.width * 0.2679705,
        size.height * 0.7421648,
        size.width * 0.2809165,
        size.height * 0.7421648);
    path_124.close();

    Paint paint_124_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_124_stroke.color = Colors.black;
    canvas.drawPath(path_124, paint_124_stroke);

    Paint paint_124_fill = Paint()..style = PaintingStyle.fill;
    paint_124_fill.color = Colors.white;
    canvas.drawPath(path_124, paint_124_fill);

    Path path_125 = Path();
    path_125.moveTo(size.width * 0.4391338, size.height * 0.9757344);
    path_125.cubicTo(
        size.width * 0.4520799,
        size.height * 0.9757344,
        size.width * 0.4625748,
        size.height * 0.9643359,
        size.width * 0.4625748,
        size.height * 0.9502734);
    path_125.cubicTo(
        size.width * 0.4625748,
        size.height * 0.9362187,
        size.width * 0.4520799,
        size.height * 0.9248203,
        size.width * 0.4391338,
        size.height * 0.9248203);
    path_125.cubicTo(
        size.width * 0.4261871,
        size.height * 0.9248203,
        size.width * 0.4156921,
        size.height * 0.9362187,
        size.width * 0.4156921,
        size.height * 0.9502734);
    path_125.cubicTo(
        size.width * 0.4156921,
        size.height * 0.9643359,
        size.width * 0.4261871,
        size.height * 0.9757344,
        size.width * 0.4391338,
        size.height * 0.9757344);
    path_125.close();

    Paint paint_125_fill = Paint()..style = PaintingStyle.fill;
    paint_125_fill.color = Colors.black;
    canvas.drawPath(path_125, paint_125_fill);

    Path path_126 = Path();
    path_126.moveTo(size.width * 0.4391338, size.height * 0.9757344);
    path_126.cubicTo(
        size.width * 0.4520799,
        size.height * 0.9757344,
        size.width * 0.4625748,
        size.height * 0.9643359,
        size.width * 0.4625748,
        size.height * 0.9502734);
    path_126.cubicTo(
        size.width * 0.4625748,
        size.height * 0.9362187,
        size.width * 0.4520799,
        size.height * 0.9248203,
        size.width * 0.4391338,
        size.height * 0.9248203);
    path_126.cubicTo(
        size.width * 0.4261871,
        size.height * 0.9248203,
        size.width * 0.4156921,
        size.height * 0.9362187,
        size.width * 0.4156921,
        size.height * 0.9502734);
    path_126.cubicTo(
        size.width * 0.4156921,
        size.height * 0.9643359,
        size.width * 0.4261871,
        size.height * 0.9757344,
        size.width * 0.4391338,
        size.height * 0.9757344);
    path_126.close();

    Paint paint_126_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_126_stroke.color = Colors.black;
    canvas.drawPath(path_126, paint_126_stroke);

    Paint paint_126_fill = Paint()..style = PaintingStyle.fill;
    paint_126_fill.color = Colors.white;
    canvas.drawPath(path_126, paint_126_fill);

    Path path_127 = Path();
    path_127.moveTo(size.width * 0.6523755, size.height * 0.9502656);
    path_127.cubicTo(
        size.width * 0.6653216,
        size.height * 0.9502656,
        size.width * 0.6758165,
        size.height * 0.9388672,
        size.width * 0.6758165,
        size.height * 0.9248047);
    path_127.cubicTo(
        size.width * 0.6758165,
        size.height * 0.9107500,
        size.width * 0.6653216,
        size.height * 0.8993516,
        size.width * 0.6523755,
        size.height * 0.8993516);
    path_127.cubicTo(
        size.width * 0.6394295,
        size.height * 0.8993516,
        size.width * 0.6289345,
        size.height * 0.9107500,
        size.width * 0.6289345,
        size.height * 0.9248047);
    path_127.cubicTo(
        size.width * 0.6289345,
        size.height * 0.9388672,
        size.width * 0.6394295,
        size.height * 0.9502656,
        size.width * 0.6523755,
        size.height * 0.9502656);
    path_127.close();

    Paint paint_127_fill = Paint()..style = PaintingStyle.fill;
    paint_127_fill.color = Colors.black;
    canvas.drawPath(path_127, paint_127_fill);

    Path path_128 = Path();
    path_128.moveTo(size.width * 0.6523755, size.height * 0.9502656);
    path_128.cubicTo(
        size.width * 0.6653216,
        size.height * 0.9502656,
        size.width * 0.6758165,
        size.height * 0.9388672,
        size.width * 0.6758165,
        size.height * 0.9248047);
    path_128.cubicTo(
        size.width * 0.6758165,
        size.height * 0.9107500,
        size.width * 0.6653216,
        size.height * 0.8993516,
        size.width * 0.6523755,
        size.height * 0.8993516);
    path_128.cubicTo(
        size.width * 0.6394295,
        size.height * 0.8993516,
        size.width * 0.6289345,
        size.height * 0.9107500,
        size.width * 0.6289345,
        size.height * 0.9248047);
    path_128.cubicTo(
        size.width * 0.6289345,
        size.height * 0.9388672,
        size.width * 0.6394295,
        size.height * 0.9502656,
        size.width * 0.6523755,
        size.height * 0.9502656);
    path_128.close();

    Paint paint_128_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_128_stroke.color = Colors.black;
    canvas.drawPath(path_128, paint_128_stroke);

    Paint paint_128_fill = Paint()..style = PaintingStyle.fill;
    paint_128_fill.color = Colors.white;
    canvas.drawPath(path_128, paint_128_fill);

    Path path_129 = Path();
    path_129.moveTo(size.width * 0.8082878, size.height * 0.5782383);
    path_129.cubicTo(
        size.width * 0.8083165,
        size.height * 0.5782383,
        size.width * 0.8083453,
        size.height * 0.5782367,
        size.width * 0.8083741,
        size.height * 0.5782367);
    path_129.cubicTo(
        size.width * 0.8101655,
        size.height * 0.5615523,
        size.width * 0.8133813,
        size.height * 0.5451961,
        size.width * 0.8141727,
        size.height * 0.5281375);
    path_129.cubicTo(
        size.width * 0.8122878,
        size.height * 0.5276094,
        size.width * 0.8103165,
        size.height * 0.5273281,
        size.width * 0.8082878,
        size.height * 0.5273281);
    path_129.cubicTo(
        size.width * 0.7953453,
        size.height * 0.5273281,
        size.width * 0.7848489,
        size.height * 0.5387250,
        size.width * 0.7848489,
        size.height * 0.5527844);
    path_129.cubicTo(
        size.width * 0.7848489,
        size.height * 0.5668430,
        size.width * 0.7953453,
        size.height * 0.5782383,
        size.width * 0.8082878,
        size.height * 0.5782383);
    path_129.close();

    Paint paint_129_fill = Paint()..style = PaintingStyle.fill;
    paint_129_fill.color = Colors.black;
    canvas.drawPath(path_129, paint_129_fill);

    Path path_130 = Path();
    path_130.moveTo(size.width * 0.8082878, size.height * 0.5782383);
    path_130.cubicTo(
        size.width * 0.8083165,
        size.height * 0.5782383,
        size.width * 0.8083453,
        size.height * 0.5782367,
        size.width * 0.8083741,
        size.height * 0.5782367);
    path_130.cubicTo(
        size.width * 0.8101655,
        size.height * 0.5615523,
        size.width * 0.8133813,
        size.height * 0.5451961,
        size.width * 0.8141727,
        size.height * 0.5281375);
    path_130.cubicTo(
        size.width * 0.8122878,
        size.height * 0.5276094,
        size.width * 0.8103165,
        size.height * 0.5273281,
        size.width * 0.8082878,
        size.height * 0.5273281);
    path_130.cubicTo(
        size.width * 0.7953453,
        size.height * 0.5273281,
        size.width * 0.7848489,
        size.height * 0.5387250,
        size.width * 0.7848489,
        size.height * 0.5527844);
    path_130.cubicTo(
        size.width * 0.7848489,
        size.height * 0.5668430,
        size.width * 0.7953453,
        size.height * 0.5782383,
        size.width * 0.8082878,
        size.height * 0.5782383);
    path_130.close();

    Paint paint_130_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_130_stroke.color = Colors.black;
    canvas.drawPath(path_130, paint_130_stroke);

    Paint paint_130_fill = Paint()..style = PaintingStyle.fill;
    paint_130_fill.color = Colors.white;
    canvas.drawPath(path_130, paint_130_fill);

    Path path_131 = Path();
    path_131.moveTo(size.width * 0.3043216, size.height * 0.5273320);
    path_131.cubicTo(
        size.width * 0.3043216,
        size.height * 0.5132734,
        size.width * 0.2938266,
        size.height * 0.5018766,
        size.width * 0.2808806,
        size.height * 0.5018766);
    path_131.cubicTo(
        size.width * 0.2798396,
        size.height * 0.5018766,
        size.width * 0.2788158,
        size.height * 0.5019586,
        size.width * 0.2778101,
        size.height * 0.5021016);
    path_131.cubicTo(
        size.width * 0.2745079,
        size.height * 0.5185859,
        size.width * 0.2752043,
        size.height * 0.5354414,
        size.width * 0.2769676,
        size.height * 0.5524281);
    path_131.cubicTo(
        size.width * 0.2782403,
        size.height * 0.5526609,
        size.width * 0.2795468,
        size.height * 0.5527883,
        size.width * 0.2808806,
        size.height * 0.5527883);
    path_131.cubicTo(
        size.width * 0.2938266,
        size.height * 0.5527883,
        size.width * 0.3043216,
        size.height * 0.5413914,
        size.width * 0.3043216,
        size.height * 0.5273320);
    path_131.close();

    Paint paint_131_fill = Paint()..style = PaintingStyle.fill;
    paint_131_fill.color = Colors.black;
    canvas.drawPath(path_131, paint_131_fill);

    Path path_132 = Path();
    path_132.moveTo(size.width * 0.3043216, size.height * 0.5273320);
    path_132.cubicTo(
        size.width * 0.3043216,
        size.height * 0.5132734,
        size.width * 0.2938266,
        size.height * 0.5018766,
        size.width * 0.2808806,
        size.height * 0.5018766);
    path_132.cubicTo(
        size.width * 0.2798396,
        size.height * 0.5018766,
        size.width * 0.2788158,
        size.height * 0.5019586,
        size.width * 0.2778101,
        size.height * 0.5021016);
    path_132.cubicTo(
        size.width * 0.2745079,
        size.height * 0.5185859,
        size.width * 0.2752043,
        size.height * 0.5354414,
        size.width * 0.2769676,
        size.height * 0.5524281);
    path_132.cubicTo(
        size.width * 0.2782403,
        size.height * 0.5526609,
        size.width * 0.2795468,
        size.height * 0.5527883,
        size.width * 0.2808806,
        size.height * 0.5527883);
    path_132.cubicTo(
        size.width * 0.2938266,
        size.height * 0.5527883,
        size.width * 0.3043216,
        size.height * 0.5413914,
        size.width * 0.3043216,
        size.height * 0.5273320);
    path_132.close();

    Paint paint_132_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_132_stroke.color = Colors.black;
    canvas.drawPath(path_132, paint_132_stroke);

    Paint paint_132_fill = Paint()..style = PaintingStyle.fill;
    paint_132_fill.color = Colors.white;
    canvas.drawPath(path_132, paint_132_fill);

    Path path_133 = Path();
    path_133.moveTo(size.width * 0.2492604, size.height * 0.9326953);
    path_133.cubicTo(
        size.width * 0.2542590,
        size.height * 0.9280312,
        size.width * 0.2574295,
        size.height * 0.9211172,
        size.width * 0.2574295,
        size.height * 0.9133984);
    path_133.cubicTo(
        size.width * 0.2574295,
        size.height * 0.8993359,
        size.width * 0.2469345,
        size.height * 0.8879375,
        size.width * 0.2339878,
        size.height * 0.8879375);
    path_133.cubicTo(
        size.width * 0.2248719,
        size.height * 0.8879375,
        size.width * 0.2169777,
        size.height * 0.8935937,
        size.width * 0.2131014,
        size.height * 0.9018438);
    path_133.cubicTo(
        size.width * 0.2227619,
        size.height * 0.9133125,
        size.width * 0.2349216,
        size.height * 0.9235703,
        size.width * 0.2492604,
        size.height * 0.9326953);
    path_133.close();

    Paint paint_133_fill = Paint()..style = PaintingStyle.fill;
    paint_133_fill.color = Colors.black;
    canvas.drawPath(path_133, paint_133_fill);

    Path path_134 = Path();
    path_134.moveTo(size.width * 0.2492604, size.height * 0.9326953);
    path_134.cubicTo(
        size.width * 0.2542590,
        size.height * 0.9280312,
        size.width * 0.2574295,
        size.height * 0.9211172,
        size.width * 0.2574295,
        size.height * 0.9133984);
    path_134.cubicTo(
        size.width * 0.2574295,
        size.height * 0.8993359,
        size.width * 0.2469345,
        size.height * 0.8879375,
        size.width * 0.2339878,
        size.height * 0.8879375);
    path_134.cubicTo(
        size.width * 0.2248719,
        size.height * 0.8879375,
        size.width * 0.2169777,
        size.height * 0.8935937,
        size.width * 0.2131014,
        size.height * 0.9018438);
    path_134.cubicTo(
        size.width * 0.2227619,
        size.height * 0.9133125,
        size.width * 0.2349216,
        size.height * 0.9235703,
        size.width * 0.2492604,
        size.height * 0.9326953);
    path_134.close();

    Paint paint_134_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_134_stroke.color = Colors.black;
    canvas.drawPath(path_134, paint_134_stroke);

    Paint paint_134_fill = Paint()..style = PaintingStyle.fill;
    paint_134_fill.color = Colors.white;
    canvas.drawPath(path_134, paint_134_fill);

    Path path_135 = Path();
    path_135.moveTo(size.width * 0.8951655, size.height * 0.8953281);
    path_135.cubicTo(
        size.width * 0.8915108,
        size.height * 0.8864375,
        size.width * 0.8833022,
        size.height * 0.8802344,
        size.width * 0.8737554,
        size.height * 0.8802344);
    path_135.cubicTo(
        size.width * 0.8608058,
        size.height * 0.8802344,
        size.width * 0.8503094,
        size.height * 0.8916328,
        size.width * 0.8503094,
        size.height * 0.9056875);
    path_135.cubicTo(
        size.width * 0.8503094,
        size.height * 0.9141250,
        size.width * 0.8540935,
        size.height * 0.9216016,
        size.width * 0.8599209,
        size.height * 0.9262344);
    path_135.cubicTo(
        size.width * 0.8738201,
        size.height * 0.9169453,
        size.width * 0.8856978,
        size.height * 0.9066484,
        size.width * 0.8951655,
        size.height * 0.8953281);
    path_135.close();

    Paint paint_135_fill = Paint()..style = PaintingStyle.fill;
    paint_135_fill.color = Colors.black;
    canvas.drawPath(path_135, paint_135_fill);

    Path path_136 = Path();
    path_136.moveTo(size.width * 0.8951655, size.height * 0.8953281);
    path_136.cubicTo(
        size.width * 0.8915108,
        size.height * 0.8864375,
        size.width * 0.8833022,
        size.height * 0.8802344,
        size.width * 0.8737554,
        size.height * 0.8802344);
    path_136.cubicTo(
        size.width * 0.8608058,
        size.height * 0.8802344,
        size.width * 0.8503094,
        size.height * 0.8916328,
        size.width * 0.8503094,
        size.height * 0.9056875);
    path_136.cubicTo(
        size.width * 0.8503094,
        size.height * 0.9141250,
        size.width * 0.8540935,
        size.height * 0.9216016,
        size.width * 0.8599209,
        size.height * 0.9262344);
    path_136.cubicTo(
        size.width * 0.8738201,
        size.height * 0.9169453,
        size.width * 0.8856978,
        size.height * 0.9066484,
        size.width * 0.8951655,
        size.height * 0.8953281);
    path_136.close();

    Paint paint_136_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_136_stroke.color = Colors.black;
    canvas.drawPath(path_136, paint_136_stroke);

    Paint paint_136_fill = Paint()..style = PaintingStyle.fill;
    paint_136_fill.color = Colors.white;
    canvas.drawPath(path_136, paint_136_fill);

    Path path_137 = Path();
    path_137.moveTo(size.width * 0.7975180, size.height * 0.6650781);
    path_137.cubicTo(
        size.width * 0.8178058,
        size.height * 0.7549367,
        size.width * 0.3087417,
        size.height * 0.8639219,
        size.width * 0.2850439,
        size.height * 0.7900781);
    path_137.cubicTo(
        size.width * 0.2850439,
        size.height * 0.7900781,
        size.width * 0.2848496,
        size.height * 0.6995180,
        size.width * 0.4319511,
        size.height * 0.6666937);
    path_137.lineTo(size.width * 0.6265626, size.height * 0.6205437);
    path_137.cubicTo(
        size.width * 0.7528849,
        size.height * 0.5908164,
        size.width * 0.7911367,
        size.height * 0.6367859,
        size.width * 0.7975180,
        size.height * 0.6650781);
    path_137.close();

    Paint paint_137_fill = Paint()..style = PaintingStyle.fill;
    paint_137_fill.color = Colors.black;
    canvas.drawPath(path_137, paint_137_fill);

    Path path_138 = Path();
    path_138.moveTo(size.width * 0.7975180, size.height * 0.6650781);
    path_138.cubicTo(
        size.width * 0.8178058,
        size.height * 0.7549367,
        size.width * 0.3087417,
        size.height * 0.8639219,
        size.width * 0.2850439,
        size.height * 0.7900781);
    path_138.cubicTo(
        size.width * 0.2850439,
        size.height * 0.7900781,
        size.width * 0.2848496,
        size.height * 0.6995180,
        size.width * 0.4319511,
        size.height * 0.6666937);
    path_138.lineTo(size.width * 0.6265626, size.height * 0.6205437);
    path_138.cubicTo(
        size.width * 0.7528849,
        size.height * 0.5908164,
        size.width * 0.7911367,
        size.height * 0.6367859,
        size.width * 0.7975180,
        size.height * 0.6650781);
    path_138.close();

    Paint paint_138_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_138_stroke.color = Colors.black;
    canvas.drawPath(path_138, paint_138_stroke);

    Paint paint_138_fill = Paint()..style = PaintingStyle.fill;
    paint_138_fill.color = Colors.white;
    canvas.drawPath(path_138, paint_138_fill);

    Path path_139 = Path();
    path_139.moveTo(size.width * 0.5853381, size.height * 0.7330133);
    path_139.cubicTo(
        size.width * 0.5515446,
        size.height * 0.7285586,
        size.width * 0.5179863,
        size.height * 0.7281695,
        size.width * 0.4841453,
        size.height * 0.7321758);
    path_139.cubicTo(
        size.width * 0.4494094,
        size.height * 0.7362891,
        size.width * 0.4150396,
        size.height * 0.7415891,
        size.width * 0.3800662,
        size.height * 0.7373750);
    path_139.cubicTo(
        size.width * 0.3621511,
        size.height * 0.7352172,
        size.width * 0.3444266,
        size.height * 0.7316211,
        size.width * 0.3268849,
        size.height * 0.7271539);
    path_139.cubicTo(
        size.width * 0.3234317,
        size.height * 0.7262742,
        size.width * 0.3199863,
        size.height * 0.7253695,
        size.width * 0.3165417,
        size.height * 0.7244719);
    path_139.cubicTo(
        size.width * 0.2849777,
        size.height * 0.7575883,
        size.width * 0.2850439,
        size.height * 0.7900859,
        size.width * 0.2850439,
        size.height * 0.7900859);
    path_139.cubicTo(
        size.width * 0.3008561,
        size.height * 0.8393594,
        size.width * 0.5327540,
        size.height * 0.8072266,
        size.width * 0.6793122,
        size.height * 0.7536961);
    path_139.cubicTo(
        size.width * 0.6484784,
        size.height * 0.7445406,
        size.width * 0.6170799,
        size.height * 0.7371977,
        size.width * 0.5853381,
        size.height * 0.7330133);
    path_139.close();

    Paint paint_139_fill = Paint()..style = PaintingStyle.fill;
    paint_139_fill.color = Colors.black;
    canvas.drawPath(path_139, paint_139_fill);

    Path path_140 = Path();
    path_140.moveTo(size.width * 0.5853381, size.height * 0.7330133);
    path_140.cubicTo(
        size.width * 0.5515446,
        size.height * 0.7285586,
        size.width * 0.5179863,
        size.height * 0.7281695,
        size.width * 0.4841453,
        size.height * 0.7321758);
    path_140.cubicTo(
        size.width * 0.4494094,
        size.height * 0.7362891,
        size.width * 0.4150396,
        size.height * 0.7415891,
        size.width * 0.3800662,
        size.height * 0.7373750);
    path_140.cubicTo(
        size.width * 0.3621511,
        size.height * 0.7352172,
        size.width * 0.3444266,
        size.height * 0.7316211,
        size.width * 0.3268849,
        size.height * 0.7271539);
    path_140.cubicTo(
        size.width * 0.3234317,
        size.height * 0.7262742,
        size.width * 0.3199863,
        size.height * 0.7253695,
        size.width * 0.3165417,
        size.height * 0.7244719);
    path_140.cubicTo(
        size.width * 0.2849777,
        size.height * 0.7575883,
        size.width * 0.2850439,
        size.height * 0.7900859,
        size.width * 0.2850439,
        size.height * 0.7900859);
    path_140.cubicTo(
        size.width * 0.3008561,
        size.height * 0.8393594,
        size.width * 0.5327540,
        size.height * 0.8072266,
        size.width * 0.6793122,
        size.height * 0.7536961);
    path_140.cubicTo(
        size.width * 0.6484784,
        size.height * 0.7445406,
        size.width * 0.6170799,
        size.height * 0.7371977,
        size.width * 0.5853381,
        size.height * 0.7330133);
    path_140.close();

    Paint paint_140_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_140_stroke.color = Colors.black;
    canvas.drawPath(path_140, paint_140_stroke);

    Paint paint_140_fill = Paint()..style = PaintingStyle.fill;
    paint_140_fill.color = Colors.black;
    canvas.drawPath(path_140, paint_140_fill);

    Path path_141 = Path();
    path_141.moveTo(size.width * 0.6245209, size.height * 0.7833047);
    path_141.cubicTo(
        size.width * 0.6327036,
        size.height * 0.8304141,
        size.width * 0.5964007,
        size.height * 0.8596094,
        size.width * 0.5964007,
        size.height * 0.8596094);
    path_141.cubicTo(
        size.width * 0.5483324,
        size.height * 0.8596094,
        size.width * 0.3130856,
        size.height * 0.8510234,
        size.width * 0.2860748,
        size.height * 0.7928594);
    path_141.cubicTo(
        size.width * 0.2547662,
        size.height * 0.7254297,
        size.width * 0.3157058,
        size.height * 0.6903078,
        size.width * 0.3637734,
        size.height * 0.6903078);
    path_141.cubicTo(
        size.width * 0.4118417,
        size.height * 0.6903078,
        size.width * 0.6245209,
        size.height * 0.7833047,
        size.width * 0.6245209,
        size.height * 0.7833047);
    path_141.close();

    Paint paint_141_fill = Paint()..style = PaintingStyle.fill;
    paint_141_fill.color = Colors.black;
    canvas.drawPath(path_141, paint_141_fill);

    Path path_142 = Path();
    path_142.moveTo(size.width * 0.6245209, size.height * 0.7833047);
    path_142.cubicTo(
        size.width * 0.6327036,
        size.height * 0.8304141,
        size.width * 0.5964007,
        size.height * 0.8596094,
        size.width * 0.5964007,
        size.height * 0.8596094);
    path_142.cubicTo(
        size.width * 0.5483324,
        size.height * 0.8596094,
        size.width * 0.3130856,
        size.height * 0.8510234,
        size.width * 0.2860748,
        size.height * 0.7928594);
    path_142.cubicTo(
        size.width * 0.2547662,
        size.height * 0.7254297,
        size.width * 0.3157058,
        size.height * 0.6903078,
        size.width * 0.3637734,
        size.height * 0.6903078);
    path_142.cubicTo(
        size.width * 0.4118417,
        size.height * 0.6903078,
        size.width * 0.6245209,
        size.height * 0.7833047,
        size.width * 0.6245209,
        size.height * 0.7833047);
    path_142.close();

    Paint paint_142_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_142_stroke.color = Colors.black;
    canvas.drawPath(path_142, paint_142_stroke);

    Paint paint_142_fill = Paint()..style = PaintingStyle.fill;
    paint_142_fill.color = Colors.white;
    canvas.drawPath(path_142, paint_142_fill);

    Path path_143 = Path();
    path_143.moveTo(size.width * 0.6061856, size.height * 0.8371641);
    path_143.cubicTo(
        size.width * 0.6508482,
        size.height * 0.8602734,
        size.width * 0.6816388,
        size.height * 0.8574219,
        size.width * 0.7021885,
        size.height * 0.8536719);
    path_143.cubicTo(
        size.width * 0.7227410,
        size.height * 0.8499141,
        size.width * 0.7309784,
        size.height * 0.8202187,
        size.width * 0.7190583,
        size.height * 0.8087656);
    path_143.cubicTo(
        size.width * 0.6991180,
        size.height * 0.7896172,
        size.width * 0.6556719,
        size.height * 0.8129531,
        size.width * 0.6224173,
        size.height * 0.7947109);
    path_143.lineTo(size.width * 0.6061856, size.height * 0.8371641);
    path_143.close();

    Paint paint_143_fill = Paint()..style = PaintingStyle.fill;
    paint_143_fill.color = Colors.black;
    canvas.drawPath(path_143, paint_143_fill);

    Path path_144 = Path();
    path_144.moveTo(size.width * 0.6061856, size.height * 0.8371641);
    path_144.cubicTo(
        size.width * 0.6508482,
        size.height * 0.8602734,
        size.width * 0.6816388,
        size.height * 0.8574219,
        size.width * 0.7021885,
        size.height * 0.8536719);
    path_144.cubicTo(
        size.width * 0.7227410,
        size.height * 0.8499141,
        size.width * 0.7309784,
        size.height * 0.8202187,
        size.width * 0.7190583,
        size.height * 0.8087656);
    path_144.cubicTo(
        size.width * 0.6991180,
        size.height * 0.7896172,
        size.width * 0.6556719,
        size.height * 0.8129531,
        size.width * 0.6224173,
        size.height * 0.7947109);
    path_144.lineTo(size.width * 0.6061856, size.height * 0.8371641);
    path_144.close();

    Paint paint_144_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_144_stroke.color = Colors.black;
    canvas.drawPath(path_144, paint_144_stroke);

    Paint paint_144_fill = Paint()..style = PaintingStyle.fill;
    paint_144_fill.color = activeColor2;
    canvas.drawPath(path_144, paint_144_fill);

    Path path_145 = Path();
    path_145.moveTo(size.width * 0.6089871, size.height * 0.8259922);
    path_145.cubicTo(
        size.width * 0.5817094,
        size.height * 0.8358984,
        size.width * 0.5513489,
        size.height * 0.8392656,
        size.width * 0.5228043,
        size.height * 0.8362656);
    path_145.cubicTo(
        size.width * 0.5132000,
        size.height * 0.8352500,
        size.width * 0.5038906,
        size.height * 0.8331094,
        size.width * 0.4944266,
        size.height * 0.8311953);
    path_145.cubicTo(
        size.width * 0.4851633,
        size.height * 0.8293281,
        size.width * 0.4757683,
        size.height * 0.8284687,
        size.width * 0.4663554,
        size.height * 0.8282266);
    path_145.cubicTo(
        size.width * 0.4479137,
        size.height * 0.8277500,
        size.width * 0.4295245,
        size.height * 0.8296562,
        size.width * 0.4111180,
        size.height * 0.8305313);
    path_145.cubicTo(
        size.width * 0.3901381,
        size.height * 0.8315234,
        size.width * 0.3688317,
        size.height * 0.8317500,
        size.width * 0.3476424,
        size.height * 0.8305391);
    path_145.cubicTo(
        size.width * 0.4286043,
        size.height * 0.8551172,
        size.width * 0.5615863,
        size.height * 0.8596250,
        size.width * 0.5963986,
        size.height * 0.8596250);
    path_145.cubicTo(
        size.width * 0.5963986,
        size.height * 0.8596250,
        size.width * 0.6142144,
        size.height * 0.8452891,
        size.width * 0.6220705,
        size.height * 0.8205078);
    path_145.cubicTo(
        size.width * 0.6173158,
        size.height * 0.8228125,
        size.width * 0.6127209,
        size.height * 0.8246406,
        size.width * 0.6089871,
        size.height * 0.8259922);
    path_145.close();

    Paint paint_145_fill = Paint()..style = PaintingStyle.fill;
    paint_145_fill.color = Colors.black;
    canvas.drawPath(path_145, paint_145_fill);

    Path path_146 = Path();
    path_146.moveTo(size.width * 0.6089871, size.height * 0.8259922);
    path_146.cubicTo(
        size.width * 0.5817094,
        size.height * 0.8358984,
        size.width * 0.5513489,
        size.height * 0.8392656,
        size.width * 0.5228043,
        size.height * 0.8362656);
    path_146.cubicTo(
        size.width * 0.5132000,
        size.height * 0.8352500,
        size.width * 0.5038906,
        size.height * 0.8331094,
        size.width * 0.4944266,
        size.height * 0.8311953);
    path_146.cubicTo(
        size.width * 0.4851633,
        size.height * 0.8293281,
        size.width * 0.4757683,
        size.height * 0.8284687,
        size.width * 0.4663554,
        size.height * 0.8282266);
    path_146.cubicTo(
        size.width * 0.4479137,
        size.height * 0.8277500,
        size.width * 0.4295245,
        size.height * 0.8296562,
        size.width * 0.4111180,
        size.height * 0.8305313);
    path_146.cubicTo(
        size.width * 0.3901381,
        size.height * 0.8315234,
        size.width * 0.3688317,
        size.height * 0.8317500,
        size.width * 0.3476424,
        size.height * 0.8305391);
    path_146.cubicTo(
        size.width * 0.4286043,
        size.height * 0.8551172,
        size.width * 0.5615863,
        size.height * 0.8596250,
        size.width * 0.5963986,
        size.height * 0.8596250);
    path_146.cubicTo(
        size.width * 0.5963986,
        size.height * 0.8596250,
        size.width * 0.6142144,
        size.height * 0.8452891,
        size.width * 0.6220705,
        size.height * 0.8205078);
    path_146.cubicTo(
        size.width * 0.6173158,
        size.height * 0.8228125,
        size.width * 0.6127209,
        size.height * 0.8246406,
        size.width * 0.6089871,
        size.height * 0.8259922);
    path_146.close();

    Paint paint_146_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_146_stroke.color = Colors.black;
    canvas.drawPath(path_146, paint_146_stroke);

    Paint paint_146_fill = Paint()..style = PaintingStyle.fill;
    paint_146_fill.color = Colors.black;
    canvas.drawPath(path_146, paint_146_fill);

    Path path_147 = Path();
    path_147.moveTo(size.width * 0.4479259, size.height * 0.7127383);
    path_147.lineTo(size.width * 0.5859000, size.height * 0.7668492);

    Paint paint_147_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002482230;
    paint_147_stroke.color = Colors.black;
    paint_147_stroke.strokeCap = StrokeCap.round;
    canvas.drawPath(path_147, paint_147_stroke);

    Paint paint_147_fill = Paint()..style = PaintingStyle.fill;
    paint_147_fill.color = Colors.black;
    canvas.drawPath(path_147, paint_147_fill);

    Path path_148 = Path();
    path_148.moveTo(size.width * 0.5627158, size.height * 0.9207500);
    path_148.cubicTo(
        size.width * 0.5627158,
        size.height * 0.9207500,
        size.width * 0.5194187,
        size.height * 0.8723906,
        size.width * 0.5194187,
        size.height * 0.8334375);
    path_148.lineTo(size.width * 0.6697194, size.height * 0.6870078);
    path_148.cubicTo(
        size.width * 0.6697194,
        size.height * 0.6870078,
        size.width * 0.7484892,
        size.height * 0.6686250,
        size.width * 0.7980863,
        size.height * 0.6686250);
    path_148.cubicTo(
        size.width * 0.7989640,
        size.height * 0.6806047,
        size.width * 0.7983094,
        size.height * 0.7027852,
        size.width * 0.7900144,
        size.height * 0.7155266);
    path_148.cubicTo(
        size.width * 0.7502086,
        size.height * 0.7766437,
        size.width * 0.6491741,
        size.height * 0.8615234,
        size.width * 0.5627158,
        size.height * 0.9207500);
    path_148.close();

    Paint paint_148_fill = Paint()..style = PaintingStyle.fill;
    paint_148_fill.color = Colors.black;
    canvas.drawPath(path_148, paint_148_fill);

    Path path_149 = Path();
    path_149.moveTo(size.width * 0.5627158, size.height * 0.9207500);
    path_149.cubicTo(
        size.width * 0.5627158,
        size.height * 0.9207500,
        size.width * 0.5194187,
        size.height * 0.8723906,
        size.width * 0.5194187,
        size.height * 0.8334375);
    path_149.lineTo(size.width * 0.6697194, size.height * 0.6870078);
    path_149.cubicTo(
        size.width * 0.6697194,
        size.height * 0.6870078,
        size.width * 0.7484892,
        size.height * 0.6686250,
        size.width * 0.7980863,
        size.height * 0.6686250);
    path_149.cubicTo(
        size.width * 0.7989640,
        size.height * 0.6806047,
        size.width * 0.7983094,
        size.height * 0.7027852,
        size.width * 0.7900144,
        size.height * 0.7155266);
    path_149.cubicTo(
        size.width * 0.7502086,
        size.height * 0.7766437,
        size.width * 0.6491741,
        size.height * 0.8615234,
        size.width * 0.5627158,
        size.height * 0.9207500);
    path_149.close();

    Paint paint_149_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_149_stroke.color = Colors.black;
    canvas.drawPath(path_149, paint_149_stroke);

    Paint paint_149_fill = Paint()..style = PaintingStyle.fill;
    paint_149_fill.color = Colors.white;
    canvas.drawPath(path_149, paint_149_fill);

    Path path_150 = Path();
    path_150.moveTo(size.width * 0.5484388, size.height * 0.9026172);
    path_150.cubicTo(
        size.width * 0.5084324,
        size.height * 0.9334531,
        size.width * 0.4780180,
        size.height * 0.9361875,
        size.width * 0.4574424,
        size.height * 0.9361875);
    path_150.cubicTo(
        size.width * 0.4368662,
        size.height * 0.9361875,
        size.width * 0.4243590,
        size.height * 0.9083828,
        size.width * 0.4342424,
        size.height * 0.8949453);
    path_150.cubicTo(
        size.width * 0.4507719,
        size.height * 0.8724766,
        size.width * 0.4965993,
        size.height * 0.8876563,
        size.width * 0.5262302,
        size.height * 0.8636797);
    path_150.lineTo(size.width * 0.5484388, size.height * 0.9026172);
    path_150.close();

    Paint paint_150_fill = Paint()..style = PaintingStyle.fill;
    paint_150_fill.color = Colors.black;
    canvas.drawPath(path_150, paint_150_fill);

    Path path_151 = Path();
    path_151.moveTo(size.width * 0.5484388, size.height * 0.9026172);
    path_151.cubicTo(
        size.width * 0.5084324,
        size.height * 0.9334531,
        size.width * 0.4780180,
        size.height * 0.9361875,
        size.width * 0.4574424,
        size.height * 0.9361875);
    path_151.cubicTo(
        size.width * 0.4368662,
        size.height * 0.9361875,
        size.width * 0.4243590,
        size.height * 0.9083828,
        size.width * 0.4342424,
        size.height * 0.8949453);
    path_151.cubicTo(
        size.width * 0.4507719,
        size.height * 0.8724766,
        size.width * 0.4965993,
        size.height * 0.8876563,
        size.width * 0.5262302,
        size.height * 0.8636797);
    path_151.lineTo(size.width * 0.5484388, size.height * 0.9026172);
    path_151.close();

    Paint paint_151_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_151_stroke.color = Colors.black;
    canvas.drawPath(path_151, paint_151_stroke);

    Paint paint_151_fill = Paint()..style = PaintingStyle.fill;
    paint_151_fill.color = activeColor2;
    canvas.drawPath(path_151, paint_151_fill);

    Path path_152 = Path();
    path_152.moveTo(size.width * 0.6696345, size.height * 0.6870195);
    path_152.lineTo(size.width * 0.5090072, size.height * 0.8435078);

    Paint paint_152_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002482230;
    paint_152_stroke.color = Colors.black;
    paint_152_stroke.strokeCap = StrokeCap.round;
    canvas.drawPath(path_152, paint_152_stroke);

    Paint paint_152_fill = Paint()..style = PaintingStyle.fill;
    paint_152_fill.color = Colors.black;
    canvas.drawPath(path_152, paint_152_fill);

    Path path_153 = Path();
    path_153.moveTo(size.width * 0.3567719, size.height * 0.4322359);
    path_153.cubicTo(
        size.width * 0.3567719,
        size.height * 0.4322359,
        size.width * 0.2926360,
        size.height * 0.5607164,
        size.width * 0.2786604,
        size.height * 0.5983016);
    path_153.cubicTo(
        size.width * 0.2646856,
        size.height * 0.6358859,
        size.width * 0.2594820,
        size.height * 0.6775688,
        size.width * 0.2848727,
        size.height * 0.6995336);
    path_153.lineTo(size.width * 0.3522014, size.height * 0.6203539);
    path_153.lineTo(size.width * 0.3901194, size.height * 0.5373523);
    path_153.lineTo(size.width * 0.3567719, size.height * 0.4322359);
    path_153.close();

    Paint paint_153_fill = Paint()..style = PaintingStyle.fill;
    paint_153_fill.color = Colors.black;
    canvas.drawPath(path_153, paint_153_fill);

    Path path_154 = Path();
    path_154.moveTo(size.width * 0.3567719, size.height * 0.4322359);
    path_154.cubicTo(
        size.width * 0.3567719,
        size.height * 0.4322359,
        size.width * 0.2926360,
        size.height * 0.5607164,
        size.width * 0.2786604,
        size.height * 0.5983016);
    path_154.cubicTo(
        size.width * 0.2646856,
        size.height * 0.6358859,
        size.width * 0.2594820,
        size.height * 0.6775688,
        size.width * 0.2848727,
        size.height * 0.6995336);
    path_154.lineTo(size.width * 0.3522014, size.height * 0.6203539);
    path_154.lineTo(size.width * 0.3901194, size.height * 0.5373523);
    path_154.lineTo(size.width * 0.3567719, size.height * 0.4322359);
    path_154.close();

    Paint paint_154_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_154_stroke.color = Colors.black;
    canvas.drawPath(path_154, paint_154_stroke);

    Paint paint_154_fill = Paint()..style = PaintingStyle.fill;
    paint_154_fill.color = activeColor2;
    canvas.drawPath(path_154, paint_154_fill);

    Path path_155 = Path();
    path_155.moveTo(size.width * 0.2975669, size.height * 0.5568461);
    path_155.cubicTo(
        size.width * 0.3011288,
        size.height * 0.5630703,
        size.width * 0.3050928,
        size.height * 0.5689984,
        size.width * 0.3094165,
        size.height * 0.5746234);
    path_155.cubicTo(
        size.width * 0.3182058,
        size.height * 0.5860562,
        size.width * 0.3284511,
        size.height * 0.5960117,
        size.width * 0.3384317,
        size.height * 0.6062047);
    path_155.cubicTo(
        size.width * 0.3451914,
        size.height * 0.6131062,
        size.width * 0.3486410,
        size.height * 0.6180117,
        size.width * 0.3514842,
        size.height * 0.6222555);
    path_155.lineTo(size.width * 0.3900950, size.height * 0.5373523);
    path_155.lineTo(size.width * 0.3567496, size.height * 0.4322359);
    path_155.cubicTo(
        size.width * 0.3567496,
        size.height * 0.4322359,
        size.width * 0.3209640,
        size.height * 0.5039219,
        size.width * 0.2970295,
        size.height * 0.5558648);
    path_155.cubicTo(
        size.width * 0.2972094,
        size.height * 0.5561922,
        size.width * 0.2973820,
        size.height * 0.5565227,
        size.width * 0.2975669,
        size.height * 0.5568461);
    path_155.close();

    Paint paint_155_fill = Paint()..style = PaintingStyle.fill;
    paint_155_fill.color = Colors.black;
    canvas.drawPath(path_155, paint_155_fill);

    Path path_156 = Path();
    path_156.moveTo(size.width * 0.2975669, size.height * 0.5568461);
    path_156.cubicTo(
        size.width * 0.3011288,
        size.height * 0.5630703,
        size.width * 0.3050928,
        size.height * 0.5689984,
        size.width * 0.3094165,
        size.height * 0.5746234);
    path_156.cubicTo(
        size.width * 0.3182058,
        size.height * 0.5860562,
        size.width * 0.3284511,
        size.height * 0.5960117,
        size.width * 0.3384317,
        size.height * 0.6062047);
    path_156.cubicTo(
        size.width * 0.3451914,
        size.height * 0.6131062,
        size.width * 0.3486410,
        size.height * 0.6180117,
        size.width * 0.3514842,
        size.height * 0.6222555);
    path_156.lineTo(size.width * 0.3900950, size.height * 0.5373523);
    path_156.lineTo(size.width * 0.3567496, size.height * 0.4322359);
    path_156.cubicTo(
        size.width * 0.3567496,
        size.height * 0.4322359,
        size.width * 0.3209640,
        size.height * 0.5039219,
        size.width * 0.2970295,
        size.height * 0.5558648);
    path_156.cubicTo(
        size.width * 0.2972094,
        size.height * 0.5561922,
        size.width * 0.2973820,
        size.height * 0.5565227,
        size.width * 0.2975669,
        size.height * 0.5568461);
    path_156.close();

    Paint paint_156_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_156_stroke.color = Colors.black;
    canvas.drawPath(path_156, paint_156_stroke);

    Paint paint_156_fill = Paint()..style = PaintingStyle.fill;
    paint_156_fill.color = Colors.black;
    canvas.drawPath(path_156, paint_156_fill);

    Path path_157 = Path();
    path_157.moveTo(size.width * 0.7609784, size.height * 0.5520008);
    path_157.lineTo(size.width * 0.6019173, size.height * 0.4665398);
    path_157.lineTo(size.width * 0.5844058, size.height * 0.3631742);
    path_157.cubicTo(
        size.width * 0.5844058,
        size.height * 0.3631742,
        size.width * 0.7524892,
        size.height * 0.4432133,
        size.width * 0.7719856,
        size.height * 0.4848578);
    path_157.cubicTo(
        size.width * 0.7851367,
        size.height * 0.5129344,
        size.width * 0.7827626,
        size.height * 0.5358094,
        size.width * 0.7609784,
        size.height * 0.5520008);
    path_157.close();

    Paint paint_157_fill = Paint()..style = PaintingStyle.fill;
    paint_157_fill.color = Colors.black;
    canvas.drawPath(path_157, paint_157_fill);

    Path path_158 = Path();
    path_158.moveTo(size.width * 0.7609784, size.height * 0.5520008);
    path_158.lineTo(size.width * 0.6019173, size.height * 0.4665398);
    path_158.lineTo(size.width * 0.5844058, size.height * 0.3631742);
    path_158.cubicTo(
        size.width * 0.5844058,
        size.height * 0.3631742,
        size.width * 0.7524892,
        size.height * 0.4432133,
        size.width * 0.7719856,
        size.height * 0.4848578);
    path_158.cubicTo(
        size.width * 0.7851367,
        size.height * 0.5129344,
        size.width * 0.7827626,
        size.height * 0.5358094,
        size.width * 0.7609784,
        size.height * 0.5520008);
    path_158.close();

    Paint paint_158_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_158_stroke.color = Colors.black;
    canvas.drawPath(path_158, paint_158_stroke);

    Paint paint_158_fill = Paint()..style = PaintingStyle.fill;
    paint_158_fill.color = activeColor2;
    canvas.drawPath(path_158, paint_158_fill);

    Path path_159 = Path();
    path_159.moveTo(size.width * 0.7029906, size.height * 0.4686906);
    path_159.cubicTo(
        size.width * 0.7057820,
        size.height * 0.4550047,
        size.width * 0.7063777,
        size.height * 0.4402320,
        size.width * 0.7022388,
        size.height * 0.4269219);
    path_159.cubicTo(
        size.width * 0.6485935,
        size.height * 0.3937406,
        size.width * 0.5844058,
        size.height * 0.3631742,
        size.width * 0.5844058,
        size.height * 0.3631742);
    path_159.lineTo(size.width * 0.6019173, size.height * 0.4665398);
    path_159.lineTo(size.width * 0.6864561, size.height * 0.5119617);
    path_159.cubicTo(
        size.width * 0.6942295,
        size.height * 0.4987250,
        size.width * 0.6998633,
        size.height * 0.4840289,
        size.width * 0.7029906,
        size.height * 0.4686906);
    path_159.close();

    Paint paint_159_fill = Paint()..style = PaintingStyle.fill;
    paint_159_fill.color = Colors.black;
    canvas.drawPath(path_159, paint_159_fill);

    Path path_160 = Path();
    path_160.moveTo(size.width * 0.7029906, size.height * 0.4686906);
    path_160.cubicTo(
        size.width * 0.7057820,
        size.height * 0.4550047,
        size.width * 0.7063777,
        size.height * 0.4402320,
        size.width * 0.7022388,
        size.height * 0.4269219);
    path_160.cubicTo(
        size.width * 0.6485935,
        size.height * 0.3937406,
        size.width * 0.5844058,
        size.height * 0.3631742,
        size.width * 0.5844058,
        size.height * 0.3631742);
    path_160.lineTo(size.width * 0.6019173, size.height * 0.4665398);
    path_160.lineTo(size.width * 0.6864561, size.height * 0.5119617);
    path_160.cubicTo(
        size.width * 0.6942295,
        size.height * 0.4987250,
        size.width * 0.6998633,
        size.height * 0.4840289,
        size.width * 0.7029906,
        size.height * 0.4686906);
    path_160.close();

    Paint paint_160_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_160_stroke.color = Colors.black;
    canvas.drawPath(path_160, paint_160_stroke);

    Paint paint_160_fill = Paint()..style = PaintingStyle.fill;
    paint_160_fill.color = Colors.black;
    canvas.drawPath(path_160, paint_160_fill);

    Path path_161 = Path();
    path_161.moveTo(size.width * 0.6547813, size.height * 0.5049648);
    path_161.cubicTo(
        size.width * 0.6781237,
        size.height * 0.4774953,
        size.width * 0.6893388,
        size.height * 0.4434250,
        size.width * 0.6911878,
        size.height * 0.4042539);
    path_161.cubicTo(
        size.width * 0.6911878,
        size.height * 0.4042539,
        size.width * 0.6061554,
        size.height * 0.3696070,
        size.width * 0.5844547,
        size.height * 0.3631469);
    path_161.cubicTo(
        size.width * 0.5627540,
        size.height * 0.3566859,
        size.width * 0.5326439,
        size.height * 0.3576672,
        size.width * 0.5069590,
        size.height * 0.3612828);
    path_161.cubicTo(
        size.width * 0.4543317,
        size.height * 0.3686914,
        size.width * 0.3817856,
        size.height * 0.4096680,
        size.width * 0.3567309,
        size.height * 0.4322336);
    path_161.cubicTo(
        size.width * 0.3316763,
        size.height * 0.4547992,
        size.width * 0.2899338,
        size.height * 0.5369297,
        size.width * 0.2899338,
        size.height * 0.5369297);
    path_161.cubicTo(
        size.width * 0.3080712,
        size.height * 0.5619719,
        size.width * 0.3344993,
        size.height * 0.5808516,
        size.width * 0.3718209,
        size.height * 0.5929898);
    path_161.lineTo(size.width * 0.3934748, size.height * 0.5478492);
    path_161.lineTo(size.width * 0.4319317, size.height * 0.6667078);
    path_161.cubicTo(
        size.width * 0.5033763,
        size.height * 0.6875055,
        size.width * 0.5699806,
        size.height * 0.6816664,
        size.width * 0.6265439,
        size.height * 0.6205578);
    path_161.lineTo(size.width * 0.6036763, size.height * 0.4772273);
    path_161.lineTo(size.width * 0.6547813, size.height * 0.5049648);
    path_161.close();

    Paint paint_161_fill = Paint()..style = PaintingStyle.fill;
    paint_161_fill.color = Colors.black;
    canvas.drawPath(path_161, paint_161_fill);

    Path path_162 = Path();
    path_162.moveTo(size.width * 0.6547813, size.height * 0.5049648);
    path_162.cubicTo(
        size.width * 0.6781237,
        size.height * 0.4774953,
        size.width * 0.6893388,
        size.height * 0.4434250,
        size.width * 0.6911878,
        size.height * 0.4042539);
    path_162.cubicTo(
        size.width * 0.6911878,
        size.height * 0.4042539,
        size.width * 0.6061554,
        size.height * 0.3696070,
        size.width * 0.5844547,
        size.height * 0.3631469);
    path_162.cubicTo(
        size.width * 0.5627540,
        size.height * 0.3566859,
        size.width * 0.5326439,
        size.height * 0.3576672,
        size.width * 0.5069590,
        size.height * 0.3612828);
    path_162.cubicTo(
        size.width * 0.4543317,
        size.height * 0.3686914,
        size.width * 0.3817856,
        size.height * 0.4096680,
        size.width * 0.3567309,
        size.height * 0.4322336);
    path_162.cubicTo(
        size.width * 0.3316763,
        size.height * 0.4547992,
        size.width * 0.2899338,
        size.height * 0.5369297,
        size.width * 0.2899338,
        size.height * 0.5369297);
    path_162.cubicTo(
        size.width * 0.3080712,
        size.height * 0.5619719,
        size.width * 0.3344993,
        size.height * 0.5808516,
        size.width * 0.3718209,
        size.height * 0.5929898);
    path_162.lineTo(size.width * 0.3934748, size.height * 0.5478492);
    path_162.lineTo(size.width * 0.4319317, size.height * 0.6667078);
    path_162.cubicTo(
        size.width * 0.5033763,
        size.height * 0.6875055,
        size.width * 0.5699806,
        size.height * 0.6816664,
        size.width * 0.6265439,
        size.height * 0.6205578);
    path_162.lineTo(size.width * 0.6036763, size.height * 0.4772273);
    path_162.lineTo(size.width * 0.6547813, size.height * 0.5049648);
    path_162.close();

    Paint paint_162_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_162_stroke.color = Colors.black;
    canvas.drawPath(path_162, paint_162_stroke);

    Paint paint_162_fill = Paint()..style = PaintingStyle.fill;
    paint_162_fill.color = activeColor2;
    canvas.drawPath(path_162, paint_162_fill);

    Path path_163 = Path();
    path_163.moveTo(size.width * 0.5836022, size.height * 0.3631359);
    path_163.lineTo(size.width * 0.5831899, size.height * 0.3636109);
    path_163.lineTo(size.width * 0.3580374, size.height * 0.4342219);
    path_163.lineTo(size.width * 0.3560043, size.height * 0.4328117);
    path_163.cubicTo(
        size.width * 0.3550734,
        size.height * 0.4336687,
        size.width * 0.3541216,
        size.height * 0.4346039,
        size.width * 0.3531504,
        size.height * 0.4356148);
    path_163.lineTo(size.width * 0.3574540, size.height * 0.4386000);
    path_163.lineTo(size.width * 0.5847058, size.height * 0.3673305);
    path_163.lineTo(size.width * 0.5851928, size.height * 0.3671789);
    path_163.lineTo(size.width * 0.5876252, size.height * 0.3643773);
    path_163.cubicTo(
        size.width * 0.5863525,
        size.height * 0.3639602,
        size.width * 0.5852022,
        size.height * 0.3635977,
        size.width * 0.5841899,
        size.height * 0.3632969);
    path_163.cubicTo(
        size.width * 0.5840000,
        size.height * 0.3632383,
        size.width * 0.5837957,
        size.height * 0.3631930,
        size.width * 0.5836022,
        size.height * 0.3631359);
    path_163.close();

    Paint paint_163_fill = Paint()..style = PaintingStyle.fill;
    paint_163_fill.color = Colors.black;
    canvas.drawPath(path_163, paint_163_fill);

    Path path_164 = Path();
    path_164.moveTo(size.width * 0.5836022, size.height * 0.3631359);
    path_164.lineTo(size.width * 0.5831899, size.height * 0.3636109);
    path_164.lineTo(size.width * 0.3580374, size.height * 0.4342219);
    path_164.lineTo(size.width * 0.3560043, size.height * 0.4328117);
    path_164.cubicTo(
        size.width * 0.3550734,
        size.height * 0.4336687,
        size.width * 0.3541216,
        size.height * 0.4346039,
        size.width * 0.3531504,
        size.height * 0.4356148);
    path_164.lineTo(size.width * 0.3574540, size.height * 0.4386000);
    path_164.lineTo(size.width * 0.5847058, size.height * 0.3673305);
    path_164.lineTo(size.width * 0.5851928, size.height * 0.3671789);
    path_164.lineTo(size.width * 0.5876252, size.height * 0.3643773);
    path_164.cubicTo(
        size.width * 0.5863525,
        size.height * 0.3639602,
        size.width * 0.5852022,
        size.height * 0.3635977,
        size.width * 0.5841899,
        size.height * 0.3632969);
    path_164.cubicTo(
        size.width * 0.5840000,
        size.height * 0.3632383,
        size.width * 0.5837957,
        size.height * 0.3631930,
        size.width * 0.5836022,
        size.height * 0.3631359);
    path_164.close();

    Paint paint_164_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_164_stroke.color = Colors.black;
    canvas.drawPath(path_164, paint_164_stroke);

    Paint paint_164_fill = Paint()..style = PaintingStyle.fill;
    paint_164_fill.color = activeColor2;
    canvas.drawPath(path_164, paint_164_fill);

    Path path_165 = Path();
    path_165.moveTo(size.width * 0.6067432, size.height * 0.3712008);
    path_165.lineTo(size.width * 0.5880885, size.height * 0.3890828);
    path_165.lineTo(size.width * 0.3664460, size.height * 0.4612703);
    path_165.lineTo(size.width * 0.3414245, size.height * 0.4498906);
    path_165.cubicTo(
        size.width * 0.3406353,
        size.height * 0.4509625,
        size.width * 0.3398410,
        size.height * 0.4520586,
        size.width * 0.3390432,
        size.height * 0.4531773);
    path_165.lineTo(size.width * 0.3662381, size.height * 0.4655453);
    path_165.lineTo(size.width * 0.5895439, size.height * 0.3928164);
    path_165.lineTo(size.width * 0.5899345, size.height * 0.3926891);
    path_165.lineTo(size.width * 0.6107719, size.height * 0.3727141);
    path_165.cubicTo(
        size.width * 0.6094036,
        size.height * 0.3721984,
        size.width * 0.6080604,
        size.height * 0.3716930,
        size.width * 0.6067432,
        size.height * 0.3712008);
    path_165.close();

    Paint paint_165_fill = Paint()..style = PaintingStyle.fill;
    paint_165_fill.color = Colors.black;
    canvas.drawPath(path_165, paint_165_fill);

    Path path_166 = Path();
    path_166.moveTo(size.width * 0.6067432, size.height * 0.3712008);
    path_166.lineTo(size.width * 0.5880885, size.height * 0.3890828);
    path_166.lineTo(size.width * 0.3664460, size.height * 0.4612703);
    path_166.lineTo(size.width * 0.3414245, size.height * 0.4498906);
    path_166.cubicTo(
        size.width * 0.3406353,
        size.height * 0.4509625,
        size.width * 0.3398410,
        size.height * 0.4520586,
        size.width * 0.3390432,
        size.height * 0.4531773);
    path_166.lineTo(size.width * 0.3662381, size.height * 0.4655453);
    path_166.lineTo(size.width * 0.5895439, size.height * 0.3928164);
    path_166.lineTo(size.width * 0.5899345, size.height * 0.3926891);
    path_166.lineTo(size.width * 0.6107719, size.height * 0.3727141);
    path_166.cubicTo(
        size.width * 0.6094036,
        size.height * 0.3721984,
        size.width * 0.6080604,
        size.height * 0.3716930,
        size.width * 0.6067432,
        size.height * 0.3712008);
    path_166.close();

    Paint paint_166_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_166_stroke.color = Colors.black;
    canvas.drawPath(path_166, paint_166_stroke);

    Paint paint_166_fill = Paint()..style = PaintingStyle.fill;
    paint_166_fill.color = activeColor2;
    canvas.drawPath(path_166, paint_166_fill);

    Path path_167 = Path();
    path_167.moveTo(size.width * 0.6307475, size.height * 0.3803867);
    path_167.lineTo(size.width * 0.5921237, size.height * 0.4143703);
    path_167.lineTo(size.width * 0.3740712, size.height * 0.4853875);
    path_167.lineTo(size.width * 0.3296360, size.height * 0.4670984);
    path_167.cubicTo(
        size.width * 0.3289065,
        size.height * 0.4682273,
        size.width * 0.3281777,
        size.height * 0.4693648,
        size.width * 0.3274504,
        size.height * 0.4705094);
    path_167.lineTo(size.width * 0.3739317, size.height * 0.4896406);
    path_167.lineTo(size.width * 0.5938892, size.height * 0.4180016);
    path_167.lineTo(size.width * 0.6348295, size.height * 0.3819805);
    path_167.cubicTo(
        size.width * 0.6334676,
        size.height * 0.3814477,
        size.width * 0.6321065,
        size.height * 0.3809156,
        size.width * 0.6307475,
        size.height * 0.3803867);
    path_167.close();

    Paint paint_167_fill = Paint()..style = PaintingStyle.fill;
    paint_167_fill.color = Colors.black;
    canvas.drawPath(path_167, paint_167_fill);

    Path path_168 = Path();
    path_168.moveTo(size.width * 0.6307475, size.height * 0.3803867);
    path_168.lineTo(size.width * 0.5921237, size.height * 0.4143703);
    path_168.lineTo(size.width * 0.3740712, size.height * 0.4853875);
    path_168.lineTo(size.width * 0.3296360, size.height * 0.4670984);
    path_168.cubicTo(
        size.width * 0.3289065,
        size.height * 0.4682273,
        size.width * 0.3281777,
        size.height * 0.4693648,
        size.width * 0.3274504,
        size.height * 0.4705094);
    path_168.lineTo(size.width * 0.3739317, size.height * 0.4896406);
    path_168.lineTo(size.width * 0.5938892, size.height * 0.4180016);
    path_168.lineTo(size.width * 0.6348295, size.height * 0.3819805);
    path_168.cubicTo(
        size.width * 0.6334676,
        size.height * 0.3814477,
        size.width * 0.6321065,
        size.height * 0.3809156,
        size.width * 0.6307475,
        size.height * 0.3803867);
    path_168.close();

    Paint paint_168_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_168_stroke.color = Colors.black;
    canvas.drawPath(path_168, paint_168_stroke);

    Paint paint_168_fill = Paint()..style = PaintingStyle.fill;
    paint_168_fill.color = activeColor2;
    canvas.drawPath(path_168, paint_168_fill);

    Path path_169 = Path();
    path_169.moveTo(size.width * 0.6578885, size.height * 0.3910828);
    path_169.lineTo(size.width * 0.5966259, size.height * 0.4393266);
    path_169.lineTo(size.width * 0.3816302, size.height * 0.5093492);
    path_169.lineTo(size.width * 0.3167518, size.height * 0.4880789);
    path_169.cubicTo(
        size.width * 0.3160540,
        size.height * 0.4892594,
        size.width * 0.3153626,
        size.height * 0.4904375,
        size.width * 0.3146777,
        size.height * 0.4916078);
    path_169.lineTo(size.width * 0.3816266, size.height * 0.5135586);
    path_169.lineTo(size.width * 0.5979928, size.height * 0.4430898);
    path_169.lineTo(size.width * 0.5982942, size.height * 0.4429922);
    path_169.lineTo(size.width * 0.6620842, size.height * 0.3927586);
    path_169.cubicTo(
        size.width * 0.6607122,
        size.height * 0.3922086,
        size.width * 0.6593129,
        size.height * 0.3916500,
        size.width * 0.6578885,
        size.height * 0.3910828);
    path_169.close();

    Paint paint_169_fill = Paint()..style = PaintingStyle.fill;
    paint_169_fill.color = Colors.black;
    canvas.drawPath(path_169, paint_169_fill);

    Path path_170 = Path();
    path_170.moveTo(size.width * 0.6578885, size.height * 0.3910828);
    path_170.lineTo(size.width * 0.5966259, size.height * 0.4393266);
    path_170.lineTo(size.width * 0.3816302, size.height * 0.5093492);
    path_170.lineTo(size.width * 0.3167518, size.height * 0.4880789);
    path_170.cubicTo(
        size.width * 0.3160540,
        size.height * 0.4892594,
        size.width * 0.3153626,
        size.height * 0.4904375,
        size.width * 0.3146777,
        size.height * 0.4916078);
    path_170.lineTo(size.width * 0.3816266, size.height * 0.5135586);
    path_170.lineTo(size.width * 0.5979928, size.height * 0.4430898);
    path_170.lineTo(size.width * 0.5982942, size.height * 0.4429922);
    path_170.lineTo(size.width * 0.6620842, size.height * 0.3927586);
    path_170.cubicTo(
        size.width * 0.6607122,
        size.height * 0.3922086,
        size.width * 0.6593129,
        size.height * 0.3916500,
        size.width * 0.6578885,
        size.height * 0.3910828);
    path_170.close();

    Paint paint_170_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_170_stroke.color = Colors.black;
    canvas.drawPath(path_170, paint_170_stroke);

    Paint paint_170_fill = Paint()..style = PaintingStyle.fill;
    paint_170_fill.color = activeColor2;
    canvas.drawPath(path_170, paint_170_fill);

    Path path_171 = Path();
    path_171.moveTo(size.width * 0.6822583, size.height * 0.4008789);
    path_171.lineTo(size.width * 0.6008453, size.height * 0.4647273);
    path_171.lineTo(size.width * 0.3893058, size.height * 0.5336234);
    path_171.lineTo(size.width * 0.3051137, size.height * 0.5082461);
    path_171.cubicTo(
        size.width * 0.3044360,
        size.height * 0.5094633,
        size.width * 0.3037727,
        size.height * 0.5106609,
        size.width * 0.3031281,
        size.height * 0.5118297);
    path_171.lineTo(size.width * 0.3888288, size.height * 0.5376617);
    path_171.lineTo(size.width * 0.3893460, size.height * 0.5378172);
    path_171.lineTo(size.width * 0.6022101, size.height * 0.4684883);
    path_171.lineTo(size.width * 0.6025101, size.height * 0.4683906);
    path_171.lineTo(size.width * 0.6864388, size.height * 0.4025695);
    path_171.cubicTo(
        size.width * 0.6852511,
        size.height * 0.4020891,
        size.width * 0.6838453,
        size.height * 0.4015195,
        size.width * 0.6822583,
        size.height * 0.4008789);
    path_171.close();

    Paint paint_171_fill = Paint()..style = PaintingStyle.fill;
    paint_171_fill.color = Colors.black;
    canvas.drawPath(path_171, paint_171_fill);

    Path path_172 = Path();
    path_172.moveTo(size.width * 0.6822583, size.height * 0.4008789);
    path_172.lineTo(size.width * 0.6008453, size.height * 0.4647273);
    path_172.lineTo(size.width * 0.3893058, size.height * 0.5336234);
    path_172.lineTo(size.width * 0.3051137, size.height * 0.5082461);
    path_172.cubicTo(
        size.width * 0.3044360,
        size.height * 0.5094633,
        size.width * 0.3037727,
        size.height * 0.5106609,
        size.width * 0.3031281,
        size.height * 0.5118297);
    path_172.lineTo(size.width * 0.3888288, size.height * 0.5376617);
    path_172.lineTo(size.width * 0.3893460, size.height * 0.5378172);
    path_172.lineTo(size.width * 0.6022101, size.height * 0.4684883);
    path_172.lineTo(size.width * 0.6025101, size.height * 0.4683906);
    path_172.lineTo(size.width * 0.6864388, size.height * 0.4025695);
    path_172.cubicTo(
        size.width * 0.6852511,
        size.height * 0.4020891,
        size.width * 0.6838453,
        size.height * 0.4015195,
        size.width * 0.6822583,
        size.height * 0.4008789);
    path_172.close();

    Paint paint_172_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_172_stroke.color = Colors.black;
    canvas.drawPath(path_172, paint_172_stroke);

    Paint paint_172_fill = Paint()..style = PaintingStyle.fill;
    paint_172_fill.color = activeColor2;
    canvas.drawPath(path_172, paint_172_fill);

    Path path_173 = Path();
    path_173.moveTo(size.width * 0.2908748, size.height * 0.5347859);
    path_173.lineTo(size.width * 0.3872216, size.height * 0.5605391);
    path_173.lineTo(size.width * 0.3889863, size.height * 0.5568609);
    path_173.lineTo(size.width * 0.2927727, size.height * 0.5311430);
    path_173.cubicTo(
        size.width * 0.2920036,
        size.height * 0.5326102,
        size.width * 0.2913662,
        size.height * 0.5338367,
        size.width * 0.2908748,
        size.height * 0.5347859);
    path_173.close();

    Paint paint_173_fill = Paint()..style = PaintingStyle.fill;
    paint_173_fill.color = Colors.black;
    canvas.drawPath(path_173, paint_173_fill);

    Path path_174 = Path();
    path_174.moveTo(size.width * 0.2908748, size.height * 0.5347859);
    path_174.lineTo(size.width * 0.3872216, size.height * 0.5605391);
    path_174.lineTo(size.width * 0.3889863, size.height * 0.5568609);
    path_174.lineTo(size.width * 0.2927727, size.height * 0.5311430);
    path_174.cubicTo(
        size.width * 0.2920036,
        size.height * 0.5326102,
        size.width * 0.2913662,
        size.height * 0.5338367,
        size.width * 0.2908748,
        size.height * 0.5347859);
    path_174.close();

    Paint paint_174_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_174_stroke.color = Colors.black;
    canvas.drawPath(path_174, paint_174_stroke);

    Paint paint_174_fill = Paint()..style = PaintingStyle.fill;
    paint_174_fill.color = activeColor2;
    canvas.drawPath(path_174, paint_174_fill);

    Path path_175 = Path();
    path_175.moveTo(size.width * 0.6062878, size.height * 0.4953250);
    path_175.lineTo(size.width * 0.6056158, size.height * 0.4911195);
    path_175.lineTo(size.width * 0.6051381, size.height * 0.4914922);
    path_175.lineTo(size.width * 0.3974532, size.height * 0.5591344);
    path_175.lineTo(size.width * 0.3967655, size.height * 0.5589508);
    path_175.lineTo(size.width * 0.3981165, size.height * 0.5631258);
    path_175.lineTo(size.width * 0.6062878, size.height * 0.4953250);
    path_175.close();

    Paint paint_175_fill = Paint()..style = PaintingStyle.fill;
    paint_175_fill.color = Colors.black;
    canvas.drawPath(path_175, paint_175_fill);

    Path path_176 = Path();
    path_176.moveTo(size.width * 0.6062878, size.height * 0.4953250);
    path_176.lineTo(size.width * 0.6056158, size.height * 0.4911195);
    path_176.lineTo(size.width * 0.6051381, size.height * 0.4914922);
    path_176.lineTo(size.width * 0.3974532, size.height * 0.5591344);
    path_176.lineTo(size.width * 0.3967655, size.height * 0.5589508);
    path_176.lineTo(size.width * 0.3981165, size.height * 0.5631258);
    path_176.lineTo(size.width * 0.6062878, size.height * 0.4953250);
    path_176.close();

    Paint paint_176_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_176_stroke.color = Colors.black;
    canvas.drawPath(path_176, paint_176_stroke);

    Paint paint_176_fill = Paint()..style = PaintingStyle.fill;
    paint_176_fill.color = activeColor2;
    canvas.drawPath(path_176, paint_176_fill);

    Path path_177 = Path();
    path_177.moveTo(size.width * 0.6888741, size.height * 0.4262539);
    path_177.lineTo(size.width * 0.6151504, size.height * 0.4837063);
    path_177.lineTo(size.width * 0.6189022, size.height * 0.4857430);
    path_177.lineTo(size.width * 0.6879396, size.height * 0.4319430);
    path_177.cubicTo(
        size.width * 0.6882770,
        size.height * 0.4300602,
        size.width * 0.6885899,
        size.height * 0.4281648,
        size.width * 0.6888741,
        size.height * 0.4262539);
    path_177.close();

    Paint paint_177_fill = Paint()..style = PaintingStyle.fill;
    paint_177_fill.color = Colors.black;
    canvas.drawPath(path_177, paint_177_fill);

    Path path_178 = Path();
    path_178.moveTo(size.width * 0.6888741, size.height * 0.4262539);
    path_178.lineTo(size.width * 0.6151504, size.height * 0.4837063);
    path_178.lineTo(size.width * 0.6189022, size.height * 0.4857430);
    path_178.lineTo(size.width * 0.6879396, size.height * 0.4319430);
    path_178.cubicTo(
        size.width * 0.6882770,
        size.height * 0.4300602,
        size.width * 0.6885899,
        size.height * 0.4281648,
        size.width * 0.6888741,
        size.height * 0.4262539);
    path_178.close();

    Paint paint_178_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_178_stroke.color = Colors.black;
    canvas.drawPath(path_178, paint_178_stroke);

    Paint paint_178_fill = Paint()..style = PaintingStyle.fill;
    paint_178_fill.color = activeColor2;
    canvas.drawPath(path_178, paint_178_fill);

    Path path_179 = Path();
    path_179.moveTo(size.width * 0.6104554, size.height * 0.5214953);
    path_179.lineTo(size.width * 0.6097871, size.height * 0.5173031);
    path_179.lineTo(size.width * 0.6093180, size.height * 0.5176578);
    path_179.lineTo(size.width * 0.4055878, size.height * 0.5842945);
    path_179.lineTo(size.width * 0.4049007, size.height * 0.5841102);
    path_179.lineTo(size.width * 0.4062518, size.height * 0.5882867);
    path_179.lineTo(size.width * 0.6104554, size.height * 0.5214953);
    path_179.close();

    Paint paint_179_fill = Paint()..style = PaintingStyle.fill;
    paint_179_fill.color = Colors.black;
    canvas.drawPath(path_179, paint_179_fill);

    Path path_180 = Path();
    path_180.moveTo(size.width * 0.6104554, size.height * 0.5214953);
    path_180.lineTo(size.width * 0.6097871, size.height * 0.5173031);
    path_180.lineTo(size.width * 0.6093180, size.height * 0.5176578);
    path_180.lineTo(size.width * 0.4055878, size.height * 0.5842945);
    path_180.lineTo(size.width * 0.4049007, size.height * 0.5841102);
    path_180.lineTo(size.width * 0.4062518, size.height * 0.5882867);
    path_180.lineTo(size.width * 0.6104554, size.height * 0.5214953);
    path_180.close();

    Paint paint_180_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_180_stroke.color = Colors.black;
    canvas.drawPath(path_180, paint_180_stroke);

    Paint paint_180_fill = Paint()..style = PaintingStyle.fill;
    paint_180_fill.color = activeColor2;
    canvas.drawPath(path_180, paint_180_fill);

    Path path_181 = Path();
    path_181.moveTo(size.width * 0.6784496, size.height * 0.4654234);
    path_181.lineTo(size.width * 0.6379129, size.height * 0.4960680);
    path_181.lineTo(size.width * 0.6416950, size.height * 0.4981203);
    path_181.lineTo(size.width * 0.6752691, size.height * 0.4727398);
    path_181.cubicTo(
        size.width * 0.6763892,
        size.height * 0.4703336,
        size.width * 0.6774540,
        size.height * 0.4678969,
        size.width * 0.6784496,
        size.height * 0.4654234);
    path_181.close();

    Paint paint_181_fill = Paint()..style = PaintingStyle.fill;
    paint_181_fill.color = Colors.black;
    canvas.drawPath(path_181, paint_181_fill);

    Path path_182 = Path();
    path_182.moveTo(size.width * 0.6784496, size.height * 0.4654234);
    path_182.lineTo(size.width * 0.6379129, size.height * 0.4960680);
    path_182.lineTo(size.width * 0.6416950, size.height * 0.4981203);
    path_182.lineTo(size.width * 0.6752691, size.height * 0.4727398);
    path_182.cubicTo(
        size.width * 0.6763892,
        size.height * 0.4703336,
        size.width * 0.6774540,
        size.height * 0.4678969,
        size.width * 0.6784496,
        size.height * 0.4654234);
    path_182.close();

    Paint paint_182_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_182_stroke.color = Colors.black;
    canvas.drawPath(path_182, paint_182_stroke);

    Paint paint_182_fill = Paint()..style = PaintingStyle.fill;
    paint_182_fill.color = activeColor2;
    canvas.drawPath(path_182, paint_182_fill);

    Path path_183 = Path();
    path_183.moveTo(size.width * 0.3159647, size.height * 0.5644297);
    path_183.lineTo(size.width * 0.3774525, size.height * 0.5809164);
    path_183.lineTo(size.width * 0.3792173, size.height * 0.5772375);
    path_183.lineTo(size.width * 0.3087763, size.height * 0.5583500);
    path_183.cubicTo(
        size.width * 0.3110842,
        size.height * 0.5604375,
        size.width * 0.3134777,
        size.height * 0.5624656,
        size.width * 0.3159647,
        size.height * 0.5644297);
    path_183.close();

    Paint paint_183_fill = Paint()..style = PaintingStyle.fill;
    paint_183_fill.color = Colors.black;
    canvas.drawPath(path_183, paint_183_fill);

    Path path_184 = Path();
    path_184.moveTo(size.width * 0.3159647, size.height * 0.5644297);
    path_184.lineTo(size.width * 0.3774525, size.height * 0.5809164);
    path_184.lineTo(size.width * 0.3792173, size.height * 0.5772375);
    path_184.lineTo(size.width * 0.3087763, size.height * 0.5583500);
    path_184.cubicTo(
        size.width * 0.3110842,
        size.height * 0.5604375,
        size.width * 0.3134777,
        size.height * 0.5624656,
        size.width * 0.3159647,
        size.height * 0.5644297);
    path_184.close();

    Paint paint_184_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_184_stroke.color = Colors.black;
    canvas.drawPath(path_184, paint_184_stroke);

    Paint paint_184_fill = Paint()..style = PaintingStyle.fill;
    paint_184_fill.color = activeColor2;
    canvas.drawPath(path_184, paint_184_fill);

    Path path_185 = Path();
    path_185.moveTo(size.width * 0.4139460, size.height * 0.6121125);
    path_185.lineTo(size.width * 0.6144763, size.height * 0.5468008);
    path_185.lineTo(size.width * 0.6138388, size.height * 0.5428008);
    path_185.lineTo(size.width * 0.4127137, size.height * 0.6083055);
    path_185.lineTo(size.width * 0.4139460, size.height * 0.6121125);
    path_185.close();

    Paint paint_185_fill = Paint()..style = PaintingStyle.fill;
    paint_185_fill.color = Colors.black;
    canvas.drawPath(path_185, paint_185_fill);

    Path path_186 = Path();
    path_186.moveTo(size.width * 0.4139460, size.height * 0.6121125);
    path_186.lineTo(size.width * 0.6144763, size.height * 0.5468008);
    path_186.lineTo(size.width * 0.6138388, size.height * 0.5428008);
    path_186.lineTo(size.width * 0.4127137, size.height * 0.6083055);
    path_186.lineTo(size.width * 0.4139460, size.height * 0.6121125);
    path_186.close();

    Paint paint_186_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_186_stroke.color = Colors.black;
    canvas.drawPath(path_186, paint_186_stroke);

    Paint paint_186_fill = Paint()..style = PaintingStyle.fill;
    paint_186_fill.color = activeColor2;
    canvas.drawPath(path_186, paint_186_fill);

    Path path_187 = Path();
    path_187.moveTo(size.width * 0.4220381, size.height * 0.6368852);
    path_187.lineTo(size.width * 0.6187072, size.height * 0.5728313);
    path_187.lineTo(size.width * 0.6180698, size.height * 0.5688320);
    path_187.lineTo(size.width * 0.4208072, size.height * 0.6330789);
    path_187.lineTo(size.width * 0.4220381, size.height * 0.6368852);
    path_187.close();

    Paint paint_187_fill = Paint()..style = PaintingStyle.fill;
    paint_187_fill.color = Colors.black;
    canvas.drawPath(path_187, paint_187_fill);

    Path path_188 = Path();
    path_188.moveTo(size.width * 0.4220381, size.height * 0.6368852);
    path_188.lineTo(size.width * 0.6187072, size.height * 0.5728313);
    path_188.lineTo(size.width * 0.6180698, size.height * 0.5688320);
    path_188.lineTo(size.width * 0.4208072, size.height * 0.6330789);
    path_188.lineTo(size.width * 0.4220381, size.height * 0.6368852);
    path_188.close();

    Paint paint_188_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_188_stroke.color = Colors.black;
    canvas.drawPath(path_188, paint_188_stroke);

    Paint paint_188_fill = Paint()..style = PaintingStyle.fill;
    paint_188_fill.color = activeColor2;
    canvas.drawPath(path_188, paint_188_fill);

    Path path_189 = Path();
    path_189.moveTo(size.width * 0.4290647, size.height * 0.6589234);
    path_189.lineTo(size.width * 0.6223007, size.height * 0.5959875);
    path_189.lineTo(size.width * 0.6216626, size.height * 0.5919875);
    path_189.lineTo(size.width * 0.4278331, size.height * 0.6551164);
    path_189.lineTo(size.width * 0.4290647, size.height * 0.6589234);
    path_189.close();

    Paint paint_189_fill = Paint()..style = PaintingStyle.fill;
    paint_189_fill.color = Colors.black;
    canvas.drawPath(path_189, paint_189_fill);

    Path path_190 = Path();
    path_190.moveTo(size.width * 0.4290647, size.height * 0.6589234);
    path_190.lineTo(size.width * 0.6223007, size.height * 0.5959875);
    path_190.lineTo(size.width * 0.6216626, size.height * 0.5919875);
    path_190.lineTo(size.width * 0.4278331, size.height * 0.6551164);
    path_190.lineTo(size.width * 0.4290647, size.height * 0.6589234);
    path_190.close();

    Paint paint_190_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_190_stroke.color = Colors.black;
    canvas.drawPath(path_190, paint_190_stroke);

    Paint paint_190_fill = Paint()..style = PaintingStyle.fill;
    paint_190_fill.color = activeColor2;
    canvas.drawPath(path_190, paint_190_fill);

    Path path_191 = Path();
    path_191.moveTo(size.width * 0.4644791, size.height * 0.6744859);
    path_191.lineTo(size.width * 0.6248741, size.height * 0.6222469);
    path_191.cubicTo(
        size.width * 0.6253576,
        size.height * 0.6217352,
        size.width * 0.6258410,
        size.height * 0.6212281,
        size.width * 0.6263223,
        size.height * 0.6207078);
    path_191.lineTo(size.width * 0.6258460, size.height * 0.6177219);
    path_191.lineTo(size.width * 0.4562727, size.height * 0.6729500);
    path_191.cubicTo(
        size.width * 0.4590158,
        size.height * 0.6735070,
        size.width * 0.4617525,
        size.height * 0.6740211,
        size.width * 0.4644791,
        size.height * 0.6744859);
    path_191.close();

    Paint paint_191_fill = Paint()..style = PaintingStyle.fill;
    paint_191_fill.color = Colors.black;
    canvas.drawPath(path_191, paint_191_fill);

    Path path_192 = Path();
    path_192.moveTo(size.width * 0.4644791, size.height * 0.6744859);
    path_192.lineTo(size.width * 0.6248741, size.height * 0.6222469);
    path_192.cubicTo(
        size.width * 0.6253576,
        size.height * 0.6217352,
        size.width * 0.6258410,
        size.height * 0.6212281,
        size.width * 0.6263223,
        size.height * 0.6207078);
    path_192.lineTo(size.width * 0.6258460, size.height * 0.6177219);
    path_192.lineTo(size.width * 0.4562727, size.height * 0.6729500);
    path_192.cubicTo(
        size.width * 0.4590158,
        size.height * 0.6735070,
        size.width * 0.4617525,
        size.height * 0.6740211,
        size.width * 0.4644791,
        size.height * 0.6744859);
    path_192.close();

    Paint paint_192_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_192_stroke.color = Colors.black;
    canvas.drawPath(path_192, paint_192_stroke);

    Paint paint_192_fill = Paint()..style = PaintingStyle.fill;
    paint_192_fill.color = activeColor2;
    canvas.drawPath(path_192, paint_192_fill);

    Path path_193 = Path();
    path_193.moveTo(size.width * 0.5212511, size.height * 0.6144219);
    path_193.cubicTo(
        size.width * 0.5212511,
        size.height * 0.6194563,
        size.width * 0.5062173,
        size.height * 0.6254641,
        size.width * 0.5028885,
        size.height * 0.6239813);
    path_193.cubicTo(
        size.width * 0.4985986,
        size.height * 0.6220711,
        size.width * 0.4969511,
        size.height * 0.6192992,
        size.width * 0.4969511,
        size.height * 0.6192992);
    path_193.cubicTo(
        size.width * 0.4969511,
        size.height * 0.6142648,
        size.width * 0.5174331,
        size.height * 0.6040219,
        size.width * 0.5201835,
        size.height * 0.6080742);
    path_193.cubicTo(
        size.width * 0.5201835,
        size.height * 0.6080742,
        size.width * 0.5212511,
        size.height * 0.6093883,
        size.width * 0.5212511,
        size.height * 0.6144219);
    path_193.close();

    Paint paint_193_fill = Paint()..style = PaintingStyle.fill;
    paint_193_fill.color = Colors.black;
    canvas.drawPath(path_193, paint_193_fill);

    Path path_194 = Path();
    path_194.moveTo(size.width * 0.5212511, size.height * 0.6144219);
    path_194.cubicTo(
        size.width * 0.5212511,
        size.height * 0.6194563,
        size.width * 0.5062173,
        size.height * 0.6254641,
        size.width * 0.5028885,
        size.height * 0.6239813);
    path_194.cubicTo(
        size.width * 0.4985986,
        size.height * 0.6220711,
        size.width * 0.4969511,
        size.height * 0.6192992,
        size.width * 0.4969511,
        size.height * 0.6192992);
    path_194.cubicTo(
        size.width * 0.4969511,
        size.height * 0.6142648,
        size.width * 0.5174331,
        size.height * 0.6040219,
        size.width * 0.5201835,
        size.height * 0.6080742);
    path_194.cubicTo(
        size.width * 0.5201835,
        size.height * 0.6080742,
        size.width * 0.5212511,
        size.height * 0.6093883,
        size.width * 0.5212511,
        size.height * 0.6144219);
    path_194.close();

    Paint paint_194_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_194_stroke.color = Colors.black;
    canvas.drawPath(path_194, paint_194_stroke);

    Paint paint_194_fill = Paint()..style = PaintingStyle.fill;
    paint_194_fill.color = activeColor2;
    canvas.drawPath(path_194, paint_194_fill);

    Path path_195 = Path();
    path_195.moveTo(size.width * 0.5668101, size.height * 0.5855445);
    path_195.cubicTo(
        size.width * 0.5710252,
        size.height * 0.5876398,
        size.width * 0.5830892,
        size.height * 0.5734617,
        size.width * 0.5834050,
        size.height * 0.5691500);
    path_195.cubicTo(
        size.width * 0.5838122,
        size.height * 0.5635961,
        size.width * 0.5822626,
        size.height * 0.5606125,
        size.width * 0.5822626,
        size.height * 0.5606125);
    path_195.cubicTo(
        size.width * 0.5780475,
        size.height * 0.5585172,
        size.width * 0.5598892,
        size.height * 0.5769781,
        size.width * 0.5619950,
        size.height * 0.5817164);
    path_195.cubicTo(
        size.width * 0.5619957,
        size.height * 0.5817164,
        size.width * 0.5625950,
        size.height * 0.5834477,
        size.width * 0.5668101,
        size.height * 0.5855445);
    path_195.close();

    Paint paint_195_fill = Paint()..style = PaintingStyle.fill;
    paint_195_fill.color = Colors.black;
    canvas.drawPath(path_195, paint_195_fill);

    Path path_196 = Path();
    path_196.moveTo(size.width * 0.5668101, size.height * 0.5855445);
    path_196.cubicTo(
        size.width * 0.5710252,
        size.height * 0.5876398,
        size.width * 0.5830892,
        size.height * 0.5734617,
        size.width * 0.5834050,
        size.height * 0.5691500);
    path_196.cubicTo(
        size.width * 0.5838122,
        size.height * 0.5635961,
        size.width * 0.5822626,
        size.height * 0.5606125,
        size.width * 0.5822626,
        size.height * 0.5606125);
    path_196.cubicTo(
        size.width * 0.5780475,
        size.height * 0.5585172,
        size.width * 0.5598892,
        size.height * 0.5769781,
        size.width * 0.5619950,
        size.height * 0.5817164);
    path_196.cubicTo(
        size.width * 0.5619957,
        size.height * 0.5817164,
        size.width * 0.5625950,
        size.height * 0.5834477,
        size.width * 0.5668101,
        size.height * 0.5855445);
    path_196.close();

    Paint paint_196_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_196_stroke.color = Colors.black;
    canvas.drawPath(path_196, paint_196_stroke);

    Paint paint_196_fill = Paint()..style = PaintingStyle.fill;
    paint_196_fill.color = activeColor2;
    canvas.drawPath(path_196, paint_196_fill);

    Path path_197 = Path();
    path_197.moveTo(size.width * 0.5728655, size.height * 0.5149102);
    path_197.cubicTo(
        size.width * 0.5764950,
        size.height * 0.5224766,
        size.width * 0.5904791,
        size.height * 0.5411984,
        size.width * 0.5845014,
        size.height * 0.5584414);
    path_197.cubicTo(
        size.width * 0.5788590,
        size.height * 0.5747148,
        size.width * 0.5029698,
        size.height * 0.6257078,
        size.width * 0.4906072,
        size.height * 0.6219086);
    path_197.cubicTo(
        size.width * 0.4770532,
        size.height * 0.6177438,
        size.width * 0.4612568,
        size.height * 0.6067516,
        size.width * 0.4541022,
        size.height * 0.5857008);
    path_197.cubicTo(
        size.width * 0.4530482,
        size.height * 0.5825984,
        size.width * 0.4473583,
        size.height * 0.5655680,
        size.width * 0.4593475,
        size.height * 0.5578875);
    path_197.cubicTo(
        size.width * 0.4648158,
        size.height * 0.5543836,
        size.width * 0.4749360,
        size.height * 0.5610055,
        size.width * 0.4831129,
        size.height * 0.5566500);
    path_197.cubicTo(
        size.width * 0.4961964,
        size.height * 0.5496805,
        size.width * 0.5045784,
        size.height * 0.5448367,
        size.width * 0.5131950,
        size.height * 0.5392500);
    path_197.cubicTo(
        size.width * 0.5199245,
        size.height * 0.5348859,
        size.width * 0.5288856,
        size.height * 0.5305305,
        size.width * 0.5369237,
        size.height * 0.5250891);
    path_197.cubicTo(
        size.width * 0.5436216,
        size.height * 0.5205555,
        size.width * 0.5391453,
        size.height * 0.5079945,
        size.width * 0.5530072,
        size.height * 0.5022992);
    path_197.cubicTo(
        size.width * 0.5585302,
        size.height * 0.5000289,
        size.width * 0.5705194,
        size.height * 0.5100211,
        size.width * 0.5728655,
        size.height * 0.5149102);
    path_197.close();

    Paint paint_197_fill = Paint()..style = PaintingStyle.fill;
    paint_197_fill.color = Colors.black;
    canvas.drawPath(path_197, paint_197_fill);

    Path path_198 = Path();
    path_198.moveTo(size.width * 0.5728655, size.height * 0.5149102);
    path_198.cubicTo(
        size.width * 0.5764950,
        size.height * 0.5224766,
        size.width * 0.5904791,
        size.height * 0.5411984,
        size.width * 0.5845014,
        size.height * 0.5584414);
    path_198.cubicTo(
        size.width * 0.5788590,
        size.height * 0.5747148,
        size.width * 0.5029698,
        size.height * 0.6257078,
        size.width * 0.4906072,
        size.height * 0.6219086);
    path_198.cubicTo(
        size.width * 0.4770532,
        size.height * 0.6177438,
        size.width * 0.4612568,
        size.height * 0.6067516,
        size.width * 0.4541022,
        size.height * 0.5857008);
    path_198.cubicTo(
        size.width * 0.4530482,
        size.height * 0.5825984,
        size.width * 0.4473583,
        size.height * 0.5655680,
        size.width * 0.4593475,
        size.height * 0.5578875);
    path_198.cubicTo(
        size.width * 0.4648158,
        size.height * 0.5543836,
        size.width * 0.4749360,
        size.height * 0.5610055,
        size.width * 0.4831129,
        size.height * 0.5566500);
    path_198.cubicTo(
        size.width * 0.4961964,
        size.height * 0.5496805,
        size.width * 0.5045784,
        size.height * 0.5448367,
        size.width * 0.5131950,
        size.height * 0.5392500);
    path_198.cubicTo(
        size.width * 0.5199245,
        size.height * 0.5348859,
        size.width * 0.5288856,
        size.height * 0.5305305,
        size.width * 0.5369237,
        size.height * 0.5250891);
    path_198.cubicTo(
        size.width * 0.5436216,
        size.height * 0.5205555,
        size.width * 0.5391453,
        size.height * 0.5079945,
        size.width * 0.5530072,
        size.height * 0.5022992);
    path_198.cubicTo(
        size.width * 0.5585302,
        size.height * 0.5000289,
        size.width * 0.5705194,
        size.height * 0.5100211,
        size.width * 0.5728655,
        size.height * 0.5149102);
    path_198.close();

    Paint paint_198_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_198_stroke.color = Colors.black;
    canvas.drawPath(path_198, paint_198_stroke);

    Paint paint_198_fill = Paint()..style = PaintingStyle.fill;
    paint_198_fill.color = Colors.black;
    canvas.drawPath(path_198, paint_198_fill);

    Path path_199 = Path();
    path_199.moveTo(size.width * 0.5462201, size.height * 0.5443234);
    path_199.cubicTo(
        size.width * 0.5506388,
        size.height * 0.5443234,
        size.width * 0.5542209,
        size.height * 0.5404336,
        size.width * 0.5542209,
        size.height * 0.5356352);
    path_199.cubicTo(
        size.width * 0.5542209,
        size.height * 0.5308367,
        size.width * 0.5506388,
        size.height * 0.5269469,
        size.width * 0.5462201,
        size.height * 0.5269469);
    path_199.cubicTo(
        size.width * 0.5418014,
        size.height * 0.5269469,
        size.width * 0.5382194,
        size.height * 0.5308367,
        size.width * 0.5382194,
        size.height * 0.5356352);
    path_199.cubicTo(
        size.width * 0.5382194,
        size.height * 0.5404336,
        size.width * 0.5418014,
        size.height * 0.5443234,
        size.width * 0.5462201,
        size.height * 0.5443234);
    path_199.close();

    Paint paint_199_fill = Paint()..style = PaintingStyle.fill;
    paint_199_fill.color = Colors.black;
    canvas.drawPath(path_199, paint_199_fill);

    Path path_200 = Path();
    path_200.moveTo(size.width * 0.5462201, size.height * 0.5443234);
    path_200.cubicTo(
        size.width * 0.5506388,
        size.height * 0.5443234,
        size.width * 0.5542209,
        size.height * 0.5404336,
        size.width * 0.5542209,
        size.height * 0.5356352);
    path_200.cubicTo(
        size.width * 0.5542209,
        size.height * 0.5308367,
        size.width * 0.5506388,
        size.height * 0.5269469,
        size.width * 0.5462201,
        size.height * 0.5269469);
    path_200.cubicTo(
        size.width * 0.5418014,
        size.height * 0.5269469,
        size.width * 0.5382194,
        size.height * 0.5308367,
        size.width * 0.5382194,
        size.height * 0.5356352);
    path_200.cubicTo(
        size.width * 0.5382194,
        size.height * 0.5404336,
        size.width * 0.5418014,
        size.height * 0.5443234,
        size.width * 0.5462201,
        size.height * 0.5443234);
    path_200.close();

    Paint paint_200_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_200_stroke.color = Colors.black;
    canvas.drawPath(path_200, paint_200_stroke);

    Paint paint_200_fill = Paint()..style = PaintingStyle.fill;
    paint_200_fill.color = activeColor2;
    canvas.drawPath(path_200, paint_200_fill);

    Path path_201 = Path();
    path_201.moveTo(size.width * 0.4870360, size.height * 0.5790070);
    path_201.cubicTo(
        size.width * 0.4914547,
        size.height * 0.5790070,
        size.width * 0.4950374,
        size.height * 0.5751172,
        size.width * 0.4950374,
        size.height * 0.5703187);
    path_201.cubicTo(
        size.width * 0.4950374,
        size.height * 0.5655203,
        size.width * 0.4914547,
        size.height * 0.5616305,
        size.width * 0.4870360,
        size.height * 0.5616305);
    path_201.cubicTo(
        size.width * 0.4826173,
        size.height * 0.5616305,
        size.width * 0.4790353,
        size.height * 0.5655203,
        size.width * 0.4790353,
        size.height * 0.5703187);
    path_201.cubicTo(
        size.width * 0.4790353,
        size.height * 0.5751172,
        size.width * 0.4826173,
        size.height * 0.5790070,
        size.width * 0.4870360,
        size.height * 0.5790070);
    path_201.close();

    Paint paint_201_fill = Paint()..style = PaintingStyle.fill;
    paint_201_fill.color = Colors.black;
    canvas.drawPath(path_201, paint_201_fill);

    Path path_202 = Path();
    path_202.moveTo(size.width * 0.4870360, size.height * 0.5790070);
    path_202.cubicTo(
        size.width * 0.4914547,
        size.height * 0.5790070,
        size.width * 0.4950374,
        size.height * 0.5751172,
        size.width * 0.4950374,
        size.height * 0.5703187);
    path_202.cubicTo(
        size.width * 0.4950374,
        size.height * 0.5655203,
        size.width * 0.4914547,
        size.height * 0.5616305,
        size.width * 0.4870360,
        size.height * 0.5616305);
    path_202.cubicTo(
        size.width * 0.4826173,
        size.height * 0.5616305,
        size.width * 0.4790353,
        size.height * 0.5655203,
        size.width * 0.4790353,
        size.height * 0.5703187);
    path_202.cubicTo(
        size.width * 0.4790353,
        size.height * 0.5751172,
        size.width * 0.4826173,
        size.height * 0.5790070,
        size.width * 0.4870360,
        size.height * 0.5790070);
    path_202.close();

    Paint paint_202_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_202_stroke.color = Colors.white;
    canvas.drawPath(path_202, paint_202_stroke);

    Paint paint_202_fill = Paint()..style = PaintingStyle.fill;
    paint_202_fill.color = activeColor2;
    canvas.drawPath(path_202, paint_202_fill);

    Path path_203 = Path();
    path_203.moveTo(size.width * 0.4940331, size.height * 0.5896258);
    path_203.cubicTo(
        size.width * 0.4959525,
        size.height * 0.5896258,
        size.width * 0.4975086,
        size.height * 0.5879359,
        size.width * 0.4975086,
        size.height * 0.5858516);
    path_203.cubicTo(
        size.width * 0.4975086,
        size.height * 0.5837672,
        size.width * 0.4959525,
        size.height * 0.5820773,
        size.width * 0.4940331,
        size.height * 0.5820773);
    path_203.cubicTo(
        size.width * 0.4921137,
        size.height * 0.5820773,
        size.width * 0.4905576,
        size.height * 0.5837672,
        size.width * 0.4905576,
        size.height * 0.5858516);
    path_203.cubicTo(
        size.width * 0.4905576,
        size.height * 0.5879359,
        size.width * 0.4921137,
        size.height * 0.5896258,
        size.width * 0.4940331,
        size.height * 0.5896258);
    path_203.close();

    Paint paint_203_fill = Paint()..style = PaintingStyle.fill;
    paint_203_fill.color = Colors.black;
    canvas.drawPath(path_203, paint_203_fill);

    Path path_204 = Path();
    path_204.moveTo(size.width * 0.4940331, size.height * 0.5896258);
    path_204.cubicTo(
        size.width * 0.4959525,
        size.height * 0.5896258,
        size.width * 0.4975086,
        size.height * 0.5879359,
        size.width * 0.4975086,
        size.height * 0.5858516);
    path_204.cubicTo(
        size.width * 0.4975086,
        size.height * 0.5837672,
        size.width * 0.4959525,
        size.height * 0.5820773,
        size.width * 0.4940331,
        size.height * 0.5820773);
    path_204.cubicTo(
        size.width * 0.4921137,
        size.height * 0.5820773,
        size.width * 0.4905576,
        size.height * 0.5837672,
        size.width * 0.4905576,
        size.height * 0.5858516);
    path_204.cubicTo(
        size.width * 0.4905576,
        size.height * 0.5879359,
        size.width * 0.4921137,
        size.height * 0.5896258,
        size.width * 0.4940331,
        size.height * 0.5896258);
    path_204.close();

    Paint paint_204_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_204_stroke.color = Colors.white;
    canvas.drawPath(path_204, paint_204_stroke);

    Paint paint_204_fill = Paint()..style = PaintingStyle.fill;
    paint_204_fill.color = activeColor2;
    canvas.drawPath(path_204, paint_204_fill);

    Path path_205 = Path();
    path_205.moveTo(size.width * 0.4901273, size.height * 0.6040609);
    path_205.cubicTo(
        size.width * 0.4920468,
        size.height * 0.6040609,
        size.width * 0.4936029,
        size.height * 0.6023711,
        size.width * 0.4936029,
        size.height * 0.6002859);
    path_205.cubicTo(
        size.width * 0.4936029,
        size.height * 0.5982016,
        size.width * 0.4920468,
        size.height * 0.5965117,
        size.width * 0.4901273,
        size.height * 0.5965117);
    path_205.cubicTo(
        size.width * 0.4882072,
        size.height * 0.5965117,
        size.width * 0.4866511,
        size.height * 0.5982016,
        size.width * 0.4866511,
        size.height * 0.6002859);
    path_205.cubicTo(
        size.width * 0.4866511,
        size.height * 0.6023711,
        size.width * 0.4882072,
        size.height * 0.6040609,
        size.width * 0.4901273,
        size.height * 0.6040609);
    path_205.close();

    Paint paint_205_fill = Paint()..style = PaintingStyle.fill;
    paint_205_fill.color = Colors.black;
    canvas.drawPath(path_205, paint_205_fill);

    Path path_206 = Path();
    path_206.moveTo(size.width * 0.4901273, size.height * 0.6040609);
    path_206.cubicTo(
        size.width * 0.4920468,
        size.height * 0.6040609,
        size.width * 0.4936029,
        size.height * 0.6023711,
        size.width * 0.4936029,
        size.height * 0.6002859);
    path_206.cubicTo(
        size.width * 0.4936029,
        size.height * 0.5982016,
        size.width * 0.4920468,
        size.height * 0.5965117,
        size.width * 0.4901273,
        size.height * 0.5965117);
    path_206.cubicTo(
        size.width * 0.4882072,
        size.height * 0.5965117,
        size.width * 0.4866511,
        size.height * 0.5982016,
        size.width * 0.4866511,
        size.height * 0.6002859);
    path_206.cubicTo(
        size.width * 0.4866511,
        size.height * 0.6023711,
        size.width * 0.4882072,
        size.height * 0.6040609,
        size.width * 0.4901273,
        size.height * 0.6040609);
    path_206.close();

    Paint paint_206_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_206_stroke.color = Colors.white;
    canvas.drawPath(path_206, paint_206_stroke);

    Paint paint_206_fill = Paint()..style = PaintingStyle.fill;
    paint_206_fill.color = activeColor2;
    canvas.drawPath(path_206, paint_206_fill);

    Path path_207 = Path();
    path_207.moveTo(size.width * 0.5089978, size.height * 0.5933945);
    path_207.cubicTo(
        size.width * 0.5109173,
        size.height * 0.5933945,
        size.width * 0.5124734,
        size.height * 0.5917047,
        size.width * 0.5124734,
        size.height * 0.5896203);
    path_207.cubicTo(
        size.width * 0.5124734,
        size.height * 0.5875359,
        size.width * 0.5109173,
        size.height * 0.5858461,
        size.width * 0.5089978,
        size.height * 0.5858461);
    path_207.cubicTo(
        size.width * 0.5070784,
        size.height * 0.5858461,
        size.width * 0.5055223,
        size.height * 0.5875359,
        size.width * 0.5055223,
        size.height * 0.5896203);
    path_207.cubicTo(
        size.width * 0.5055223,
        size.height * 0.5917047,
        size.width * 0.5070784,
        size.height * 0.5933945,
        size.width * 0.5089978,
        size.height * 0.5933945);
    path_207.close();

    Paint paint_207_fill = Paint()..style = PaintingStyle.fill;
    paint_207_fill.color = Colors.black;
    canvas.drawPath(path_207, paint_207_fill);

    Path path_208 = Path();
    path_208.moveTo(size.width * 0.5089978, size.height * 0.5933945);
    path_208.cubicTo(
        size.width * 0.5109173,
        size.height * 0.5933945,
        size.width * 0.5124734,
        size.height * 0.5917047,
        size.width * 0.5124734,
        size.height * 0.5896203);
    path_208.cubicTo(
        size.width * 0.5124734,
        size.height * 0.5875359,
        size.width * 0.5109173,
        size.height * 0.5858461,
        size.width * 0.5089978,
        size.height * 0.5858461);
    path_208.cubicTo(
        size.width * 0.5070784,
        size.height * 0.5858461,
        size.width * 0.5055223,
        size.height * 0.5875359,
        size.width * 0.5055223,
        size.height * 0.5896203);
    path_208.cubicTo(
        size.width * 0.5055223,
        size.height * 0.5917047,
        size.width * 0.5070784,
        size.height * 0.5933945,
        size.width * 0.5089978,
        size.height * 0.5933945);
    path_208.close();

    Paint paint_208_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_208_stroke.color = Colors.white;
    canvas.drawPath(path_208, paint_208_stroke);

    Paint paint_208_fill = Paint()..style = PaintingStyle.fill;
    paint_208_fill.color = activeColor2;
    canvas.drawPath(path_208, paint_208_fill);

    Path path_209 = Path();
    path_209.moveTo(size.width * 0.5034058, size.height * 0.6090500);
    path_209.cubicTo(
        size.width * 0.5053252,
        size.height * 0.6090500,
        size.width * 0.5068813,
        size.height * 0.6073602,
        size.width * 0.5068813,
        size.height * 0.6052758);
    path_209.cubicTo(
        size.width * 0.5068813,
        size.height * 0.6031914,
        size.width * 0.5053252,
        size.height * 0.6015016,
        size.width * 0.5034058,
        size.height * 0.6015016);
    path_209.cubicTo(
        size.width * 0.5014856,
        size.height * 0.6015016,
        size.width * 0.4999295,
        size.height * 0.6031914,
        size.width * 0.4999295,
        size.height * 0.6052758);
    path_209.cubicTo(
        size.width * 0.4999295,
        size.height * 0.6073602,
        size.width * 0.5014856,
        size.height * 0.6090500,
        size.width * 0.5034058,
        size.height * 0.6090500);
    path_209.close();

    Paint paint_209_fill = Paint()..style = PaintingStyle.fill;
    paint_209_fill.color = Colors.black;
    canvas.drawPath(path_209, paint_209_fill);

    Path path_210 = Path();
    path_210.moveTo(size.width * 0.5034058, size.height * 0.6090500);
    path_210.cubicTo(
        size.width * 0.5053252,
        size.height * 0.6090500,
        size.width * 0.5068813,
        size.height * 0.6073602,
        size.width * 0.5068813,
        size.height * 0.6052758);
    path_210.cubicTo(
        size.width * 0.5068813,
        size.height * 0.6031914,
        size.width * 0.5053252,
        size.height * 0.6015016,
        size.width * 0.5034058,
        size.height * 0.6015016);
    path_210.cubicTo(
        size.width * 0.5014856,
        size.height * 0.6015016,
        size.width * 0.4999295,
        size.height * 0.6031914,
        size.width * 0.4999295,
        size.height * 0.6052758);
    path_210.cubicTo(
        size.width * 0.4999295,
        size.height * 0.6073602,
        size.width * 0.5014856,
        size.height * 0.6090500,
        size.width * 0.5034058,
        size.height * 0.6090500);
    path_210.close();

    Paint paint_210_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_210_stroke.color = Colors.white;
    canvas.drawPath(path_210, paint_210_stroke);

    Paint paint_210_fill = Paint()..style = PaintingStyle.fill;
    paint_210_fill.color = activeColor2;
    canvas.drawPath(path_210, paint_210_fill);

    Path path_211 = Path();
    path_211.moveTo(size.width * 0.5486504, size.height * 0.5577500);
    path_211.cubicTo(
        size.width * 0.5505698,
        size.height * 0.5577500,
        size.width * 0.5521259,
        size.height * 0.5560602,
        size.width * 0.5521259,
        size.height * 0.5539758);
    path_211.cubicTo(
        size.width * 0.5521259,
        size.height * 0.5518914,
        size.width * 0.5505698,
        size.height * 0.5502016,
        size.width * 0.5486504,
        size.height * 0.5502016);
    path_211.cubicTo(
        size.width * 0.5467309,
        size.height * 0.5502016,
        size.width * 0.5451748,
        size.height * 0.5518914,
        size.width * 0.5451748,
        size.height * 0.5539758);
    path_211.cubicTo(
        size.width * 0.5451748,
        size.height * 0.5560602,
        size.width * 0.5467309,
        size.height * 0.5577500,
        size.width * 0.5486504,
        size.height * 0.5577500);
    path_211.close();

    Paint paint_211_fill = Paint()..style = PaintingStyle.fill;
    paint_211_fill.color = Colors.black;
    canvas.drawPath(path_211, paint_211_fill);

    Path path_212 = Path();
    path_212.moveTo(size.width * 0.5486504, size.height * 0.5577500);
    path_212.cubicTo(
        size.width * 0.5505698,
        size.height * 0.5577500,
        size.width * 0.5521259,
        size.height * 0.5560602,
        size.width * 0.5521259,
        size.height * 0.5539758);
    path_212.cubicTo(
        size.width * 0.5521259,
        size.height * 0.5518914,
        size.width * 0.5505698,
        size.height * 0.5502016,
        size.width * 0.5486504,
        size.height * 0.5502016);
    path_212.cubicTo(
        size.width * 0.5467309,
        size.height * 0.5502016,
        size.width * 0.5451748,
        size.height * 0.5518914,
        size.width * 0.5451748,
        size.height * 0.5539758);
    path_212.cubicTo(
        size.width * 0.5451748,
        size.height * 0.5560602,
        size.width * 0.5467309,
        size.height * 0.5577500,
        size.width * 0.5486504,
        size.height * 0.5577500);
    path_212.close();

    Paint paint_212_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_212_stroke.color = Colors.white;
    canvas.drawPath(path_212, paint_212_stroke);

    Paint paint_212_fill = Paint()..style = PaintingStyle.fill;
    paint_212_fill.color = activeColor2;
    canvas.drawPath(path_212, paint_212_fill);

    Path path_213 = Path();
    path_213.moveTo(size.width * 0.5447302, size.height * 0.5721852);
    path_213.cubicTo(
        size.width * 0.5466496,
        size.height * 0.5721852,
        size.width * 0.5482058,
        size.height * 0.5704953,
        size.width * 0.5482058,
        size.height * 0.5684109);
    path_213.cubicTo(
        size.width * 0.5482058,
        size.height * 0.5663258,
        size.width * 0.5466496,
        size.height * 0.5646359,
        size.width * 0.5447302,
        size.height * 0.5646359);
    path_213.cubicTo(
        size.width * 0.5428108,
        size.height * 0.5646359,
        size.width * 0.5412547,
        size.height * 0.5663258,
        size.width * 0.5412547,
        size.height * 0.5684109);
    path_213.cubicTo(
        size.width * 0.5412547,
        size.height * 0.5704953,
        size.width * 0.5428108,
        size.height * 0.5721852,
        size.width * 0.5447302,
        size.height * 0.5721852);
    path_213.close();

    Paint paint_213_fill = Paint()..style = PaintingStyle.fill;
    paint_213_fill.color = Colors.black;
    canvas.drawPath(path_213, paint_213_fill);

    Path path_214 = Path();
    path_214.moveTo(size.width * 0.5447302, size.height * 0.5721852);
    path_214.cubicTo(
        size.width * 0.5466496,
        size.height * 0.5721852,
        size.width * 0.5482058,
        size.height * 0.5704953,
        size.width * 0.5482058,
        size.height * 0.5684109);
    path_214.cubicTo(
        size.width * 0.5482058,
        size.height * 0.5663258,
        size.width * 0.5466496,
        size.height * 0.5646359,
        size.width * 0.5447302,
        size.height * 0.5646359);
    path_214.cubicTo(
        size.width * 0.5428108,
        size.height * 0.5646359,
        size.width * 0.5412547,
        size.height * 0.5663258,
        size.width * 0.5412547,
        size.height * 0.5684109);
    path_214.cubicTo(
        size.width * 0.5412547,
        size.height * 0.5704953,
        size.width * 0.5428108,
        size.height * 0.5721852,
        size.width * 0.5447302,
        size.height * 0.5721852);
    path_214.close();

    Paint paint_214_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_214_stroke.color = Colors.white;
    canvas.drawPath(path_214, paint_214_stroke);

    Paint paint_214_fill = Paint()..style = PaintingStyle.fill;
    paint_214_fill.color = activeColor2;
    canvas.drawPath(path_214, paint_214_fill);

    Path path_215 = Path();
    path_215.moveTo(size.width * 0.5636576, size.height * 0.5615187);
    path_215.cubicTo(
        size.width * 0.5655770,
        size.height * 0.5615187,
        size.width * 0.5671331,
        size.height * 0.5598289,
        size.width * 0.5671331,
        size.height * 0.5577445);
    path_215.cubicTo(
        size.width * 0.5671331,
        size.height * 0.5556602,
        size.width * 0.5655770,
        size.height * 0.5539703,
        size.width * 0.5636576,
        size.height * 0.5539703);
    path_215.cubicTo(
        size.width * 0.5617374,
        size.height * 0.5539703,
        size.width * 0.5601813,
        size.height * 0.5556602,
        size.width * 0.5601813,
        size.height * 0.5577445);
    path_215.cubicTo(
        size.width * 0.5601813,
        size.height * 0.5598289,
        size.width * 0.5617374,
        size.height * 0.5615187,
        size.width * 0.5636576,
        size.height * 0.5615187);
    path_215.close();

    Paint paint_215_fill = Paint()..style = PaintingStyle.fill;
    paint_215_fill.color = Colors.black;
    canvas.drawPath(path_215, paint_215_fill);

    Path path_216 = Path();
    path_216.moveTo(size.width * 0.5636576, size.height * 0.5615187);
    path_216.cubicTo(
        size.width * 0.5655770,
        size.height * 0.5615187,
        size.width * 0.5671331,
        size.height * 0.5598289,
        size.width * 0.5671331,
        size.height * 0.5577445);
    path_216.cubicTo(
        size.width * 0.5671331,
        size.height * 0.5556602,
        size.width * 0.5655770,
        size.height * 0.5539703,
        size.width * 0.5636576,
        size.height * 0.5539703);
    path_216.cubicTo(
        size.width * 0.5617374,
        size.height * 0.5539703,
        size.width * 0.5601813,
        size.height * 0.5556602,
        size.width * 0.5601813,
        size.height * 0.5577445);
    path_216.cubicTo(
        size.width * 0.5601813,
        size.height * 0.5598289,
        size.width * 0.5617374,
        size.height * 0.5615187,
        size.width * 0.5636576,
        size.height * 0.5615187);
    path_216.close();

    Paint paint_216_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_216_stroke.color = Colors.white;
    canvas.drawPath(path_216, paint_216_stroke);

    Paint paint_216_fill = Paint()..style = PaintingStyle.fill;
    paint_216_fill.color = activeColor2;
    canvas.drawPath(path_216, paint_216_fill);

    Path path_217 = Path();
    path_217.moveTo(size.width * 0.5579381, size.height * 0.5771445);
    path_217.cubicTo(
        size.width * 0.5598583,
        size.height * 0.5771445,
        size.width * 0.5614144,
        size.height * 0.5754539,
        size.width * 0.5614144,
        size.height * 0.5733695);
    path_217.cubicTo(
        size.width * 0.5614144,
        size.height * 0.5712852,
        size.width * 0.5598583,
        size.height * 0.5695953,
        size.width * 0.5579381,
        size.height * 0.5695953);
    path_217.cubicTo(
        size.width * 0.5560187,
        size.height * 0.5695953,
        size.width * 0.5544626,
        size.height * 0.5712852,
        size.width * 0.5544626,
        size.height * 0.5733695);
    path_217.cubicTo(
        size.width * 0.5544626,
        size.height * 0.5754539,
        size.width * 0.5560187,
        size.height * 0.5771445,
        size.width * 0.5579381,
        size.height * 0.5771445);
    path_217.close();

    Paint paint_217_fill = Paint()..style = PaintingStyle.fill;
    paint_217_fill.color = Colors.black;
    canvas.drawPath(path_217, paint_217_fill);

    Path path_218 = Path();
    path_218.moveTo(size.width * 0.5579381, size.height * 0.5771445);
    path_218.cubicTo(
        size.width * 0.5598583,
        size.height * 0.5771445,
        size.width * 0.5614144,
        size.height * 0.5754539,
        size.width * 0.5614144,
        size.height * 0.5733695);
    path_218.cubicTo(
        size.width * 0.5614144,
        size.height * 0.5712852,
        size.width * 0.5598583,
        size.height * 0.5695953,
        size.width * 0.5579381,
        size.height * 0.5695953);
    path_218.cubicTo(
        size.width * 0.5560187,
        size.height * 0.5695953,
        size.width * 0.5544626,
        size.height * 0.5712852,
        size.width * 0.5544626,
        size.height * 0.5733695);
    path_218.cubicTo(
        size.width * 0.5544626,
        size.height * 0.5754539,
        size.width * 0.5560187,
        size.height * 0.5771445,
        size.width * 0.5579381,
        size.height * 0.5771445);
    path_218.close();

    Paint paint_218_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_218_stroke.color = Colors.white;
    canvas.drawPath(path_218, paint_218_stroke);

    Paint paint_218_fill = Paint()..style = PaintingStyle.fill;
    paint_218_fill.color = activeColor2;
    canvas.drawPath(path_218, paint_218_fill);

    Path path_219 = Path();
    path_219.moveTo(size.width * 0.7226547, size.height * 0.4944070);
    path_219.lineTo(size.width * 0.6055367, size.height * 0.4902008);
    path_219.cubicTo(
        size.width * 0.5904079,
        size.height * 0.4880031,
        size.width * 0.5726374,
        size.height * 0.4815828,
        size.width * 0.5579482,
        size.height * 0.4934539);
    path_219.cubicTo(
        size.width * 0.5539173,
        size.height * 0.4967117,
        size.width * 0.5451849,
        size.height * 0.5242813,
        size.width * 0.5412496,
        size.height * 0.5333906);
    path_219.cubicTo(
        size.width * 0.5396101,
        size.height * 0.5371859,
        size.width * 0.5451863,
        size.height * 0.5540039,
        size.width * 0.5601849,
        size.height * 0.5356406);
    path_219.cubicTo(
        size.width * 0.5704453,
        size.height * 0.5230789,
        size.width * 0.5771209,
        size.height * 0.5133055,
        size.width * 0.5804791,
        size.height * 0.5180180);
    path_219.cubicTo(
        size.width * 0.5811928,
        size.height * 0.5190203,
        size.width * 0.5854511,
        size.height * 0.5376086,
        size.width * 0.5834619,
        size.height * 0.5443281);
    path_219.cubicTo(
        size.width * 0.5783633,
        size.height * 0.5615516,
        size.width * 0.5673935,
        size.height * 0.5579859,
        size.width * 0.5636612,
        size.height * 0.5615516);
    path_219.cubicTo(
        size.width * 0.5544734,
        size.height * 0.5703289,
        size.width * 0.5619842,
        size.height * 0.5817375,
        size.width * 0.5726388,
        size.height * 0.5771789);
    path_219.cubicTo(
        size.width * 0.6105763,
        size.height * 0.5609477,
        size.width * 0.6055374,
        size.height * 0.5484961,
        size.width * 0.6086129,
        size.height * 0.5390383);
    path_219.cubicTo(
        size.width * 0.6665094,
        size.height * 0.5631234,
        size.width * 0.7183036,
        size.height * 0.5735180,
        size.width * 0.7610647,
        size.height * 0.5519937);
    path_219.lineTo(size.width * 0.7226547, size.height * 0.4944070);
    path_219.close();

    Paint paint_219_fill = Paint()..style = PaintingStyle.fill;
    paint_219_fill.color = Colors.black;
    canvas.drawPath(path_219, paint_219_fill);

    Path path_220 = Path();
    path_220.moveTo(size.width * 0.7226547, size.height * 0.4944070);
    path_220.lineTo(size.width * 0.6055367, size.height * 0.4902008);
    path_220.cubicTo(
        size.width * 0.5904079,
        size.height * 0.4880031,
        size.width * 0.5726374,
        size.height * 0.4815828,
        size.width * 0.5579482,
        size.height * 0.4934539);
    path_220.cubicTo(
        size.width * 0.5539173,
        size.height * 0.4967117,
        size.width * 0.5451849,
        size.height * 0.5242813,
        size.width * 0.5412496,
        size.height * 0.5333906);
    path_220.cubicTo(
        size.width * 0.5396101,
        size.height * 0.5371859,
        size.width * 0.5451863,
        size.height * 0.5540039,
        size.width * 0.5601849,
        size.height * 0.5356406);
    path_220.cubicTo(
        size.width * 0.5704453,
        size.height * 0.5230789,
        size.width * 0.5771209,
        size.height * 0.5133055,
        size.width * 0.5804791,
        size.height * 0.5180180);
    path_220.cubicTo(
        size.width * 0.5811928,
        size.height * 0.5190203,
        size.width * 0.5854511,
        size.height * 0.5376086,
        size.width * 0.5834619,
        size.height * 0.5443281);
    path_220.cubicTo(
        size.width * 0.5783633,
        size.height * 0.5615516,
        size.width * 0.5673935,
        size.height * 0.5579859,
        size.width * 0.5636612,
        size.height * 0.5615516);
    path_220.cubicTo(
        size.width * 0.5544734,
        size.height * 0.5703289,
        size.width * 0.5619842,
        size.height * 0.5817375,
        size.width * 0.5726388,
        size.height * 0.5771789);
    path_220.cubicTo(
        size.width * 0.6105763,
        size.height * 0.5609477,
        size.width * 0.6055374,
        size.height * 0.5484961,
        size.width * 0.6086129,
        size.height * 0.5390383);
    path_220.cubicTo(
        size.width * 0.6665094,
        size.height * 0.5631234,
        size.width * 0.7183036,
        size.height * 0.5735180,
        size.width * 0.7610647,
        size.height * 0.5519937);
    path_220.lineTo(size.width * 0.7226547, size.height * 0.4944070);
    path_220.close();

    Paint paint_220_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_220_stroke.color = Colors.black;
    canvas.drawPath(path_220, paint_220_stroke);

    Paint paint_220_fill = Paint()..style = PaintingStyle.fill;
    paint_220_fill.color = Colors.white;
    canvas.drawPath(path_220, paint_220_fill);

    Path path_221 = Path();
    path_221.moveTo(size.width * 0.6754022, size.height * 0.4896695);
    path_221.lineTo(size.width * 0.7279424, size.height * 0.4934062);

    Paint paint_221_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002482230;
    paint_221_stroke.color = Colors.black;
    paint_221_stroke.strokeCap = StrokeCap.round;
    canvas.drawPath(path_221, paint_221_stroke);

    Paint paint_221_fill = Paint()..style = PaintingStyle.fill;
    paint_221_fill.color = Colors.black;
    canvas.drawPath(path_221, paint_221_fill);

    Path path_222 = Path();
    path_222.moveTo(size.width * 0.5214590, size.height * 0.5948156);
    path_222.cubicTo(
        size.width * 0.5157784,
        size.height * 0.5965187,
        size.width * 0.5093309,
        size.height * 0.6154953,
        size.width * 0.4936151,
        size.height * 0.6186398);
    path_222.cubicTo(
        size.width * 0.4859763,
        size.height * 0.6201688,
        size.width * 0.4712029,
        size.height * 0.6083508,
        size.width * 0.4707525,
        size.height * 0.6071508);
    path_222.cubicTo(
        size.width * 0.4686295,
        size.height * 0.6015070,
        size.width * 0.4737165,
        size.height * 0.5963539,
        size.width * 0.4921647,
        size.height * 0.5941070);
    path_222.cubicTo(
        size.width * 0.5014424,
        size.height * 0.5929766,
        size.width * 0.4982137,
        size.height * 0.5810883,
        size.width * 0.4936165,
        size.height * 0.5804758);
    path_222.cubicTo(
        size.width * 0.4825799,
        size.height * 0.5790055,
        size.width * 0.4775108,
        size.height * 0.5788758,
        size.width * 0.4715115,
        size.height * 0.5795648);
    path_222.cubicTo(
        size.width * 0.4613216,
        size.height * 0.5807344,
        size.width * 0.4408388,
        size.height * 0.5965195,
        size.width * 0.4281784,
        size.height * 0.6081992);
    path_222.lineTo(size.width * 0.3522007, size.height * 0.6203617);
    path_222.lineTo(size.width * 0.2786604, size.height * 0.5983086);
    path_222.cubicTo(
        size.width * 0.2646856,
        size.height * 0.6358938,
        size.width * 0.2594820,
        size.height * 0.6775766,
        size.width * 0.2848727,
        size.height * 0.6995414);
    path_222.cubicTo(
        size.width * 0.3129763,
        size.height * 0.7238547,
        size.width * 0.4295619,
        size.height * 0.6592617,
        size.width * 0.4487129,
        size.height * 0.6452883);
    path_222.cubicTo(
        size.width * 0.4668842,
        size.height * 0.6332031,
        size.width * 0.5125518,
        size.height * 0.6541000,
        size.width * 0.5283669,
        size.height * 0.6052812);
    path_222.cubicTo(
        size.width * 0.5292799,
        size.height * 0.6024594,
        size.width * 0.5272964,
        size.height * 0.5930656,
        size.width * 0.5214590,
        size.height * 0.5948156);
    path_222.close();

    Paint paint_222_fill = Paint()..style = PaintingStyle.fill;
    paint_222_fill.color = Colors.black;
    canvas.drawPath(path_222, paint_222_fill);

    Path path_223 = Path();
    path_223.moveTo(size.width * 0.5214590, size.height * 0.5948156);
    path_223.cubicTo(
        size.width * 0.5157784,
        size.height * 0.5965187,
        size.width * 0.5093309,
        size.height * 0.6154953,
        size.width * 0.4936151,
        size.height * 0.6186398);
    path_223.cubicTo(
        size.width * 0.4859763,
        size.height * 0.6201688,
        size.width * 0.4712029,
        size.height * 0.6083508,
        size.width * 0.4707525,
        size.height * 0.6071508);
    path_223.cubicTo(
        size.width * 0.4686295,
        size.height * 0.6015070,
        size.width * 0.4737165,
        size.height * 0.5963539,
        size.width * 0.4921647,
        size.height * 0.5941070);
    path_223.cubicTo(
        size.width * 0.5014424,
        size.height * 0.5929766,
        size.width * 0.4982137,
        size.height * 0.5810883,
        size.width * 0.4936165,
        size.height * 0.5804758);
    path_223.cubicTo(
        size.width * 0.4825799,
        size.height * 0.5790055,
        size.width * 0.4775108,
        size.height * 0.5788758,
        size.width * 0.4715115,
        size.height * 0.5795648);
    path_223.cubicTo(
        size.width * 0.4613216,
        size.height * 0.5807344,
        size.width * 0.4408388,
        size.height * 0.5965195,
        size.width * 0.4281784,
        size.height * 0.6081992);
    path_223.lineTo(size.width * 0.3522007, size.height * 0.6203617);
    path_223.lineTo(size.width * 0.2786604, size.height * 0.5983086);
    path_223.cubicTo(
        size.width * 0.2646856,
        size.height * 0.6358938,
        size.width * 0.2594820,
        size.height * 0.6775766,
        size.width * 0.2848727,
        size.height * 0.6995414);
    path_223.cubicTo(
        size.width * 0.3129763,
        size.height * 0.7238547,
        size.width * 0.4295619,
        size.height * 0.6592617,
        size.width * 0.4487129,
        size.height * 0.6452883);
    path_223.cubicTo(
        size.width * 0.4668842,
        size.height * 0.6332031,
        size.width * 0.5125518,
        size.height * 0.6541000,
        size.width * 0.5283669,
        size.height * 0.6052812);
    path_223.cubicTo(
        size.width * 0.5292799,
        size.height * 0.6024594,
        size.width * 0.5272964,
        size.height * 0.5930656,
        size.width * 0.5214590,
        size.height * 0.5948156);
    path_223.close();

    Paint paint_223_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_223_stroke.color = Colors.black;
    canvas.drawPath(path_223, paint_223_stroke);

    Paint paint_223_fill = Paint()..style = PaintingStyle.fill;
    paint_223_fill.color = Colors.white;
    canvas.drawPath(path_223, paint_223_fill);

    Path path_224 = Path();
    path_224.moveTo(size.width * 0.3508784, size.height * 0.6189648);
    path_224.lineTo(size.width * 0.3031561, size.height * 0.6280945);

    Paint paint_224_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.002482230;
    paint_224_stroke.color = Colors.black;
    paint_224_stroke.strokeCap = StrokeCap.round;
    canvas.drawPath(path_224, paint_224_stroke);

    Paint paint_224_fill = Paint()..style = PaintingStyle.fill;
    paint_224_fill.color = Colors.black;
    canvas.drawPath(path_224, paint_224_fill);

    Path path_225 = Path();
    path_225.moveTo(size.width * 0.4818784, size.height * 0.4160344);
    path_225.cubicTo(
        size.width * 0.4634058,
        size.height * 0.4255336,
        size.width * 0.4486849,
        size.height * 0.4091211,
        size.width * 0.4486849,
        size.height * 0.4091211);
    path_225.lineTo(size.width * 0.4872137, size.height * 0.3287352);
    path_225.lineTo(size.width * 0.5069727, size.height * 0.3612984);
    path_225.cubicTo(
        size.width * 0.5069719,
        size.height * 0.3612992,
        size.width * 0.5089353,
        size.height * 0.4021203,
        size.width * 0.4818784,
        size.height * 0.4160344);
    path_225.close();

    Paint paint_225_fill = Paint()..style = PaintingStyle.fill;
    paint_225_fill.color = Colors.black;
    canvas.drawPath(path_225, paint_225_fill);

    Path path_226 = Path();
    path_226.moveTo(size.width * 0.4818784, size.height * 0.4160344);
    path_226.cubicTo(
        size.width * 0.4634058,
        size.height * 0.4255336,
        size.width * 0.4486849,
        size.height * 0.4091211,
        size.width * 0.4486849,
        size.height * 0.4091211);
    path_226.lineTo(size.width * 0.4872137, size.height * 0.3287352);
    path_226.lineTo(size.width * 0.5069727, size.height * 0.3612984);
    path_226.cubicTo(
        size.width * 0.5069719,
        size.height * 0.3612992,
        size.width * 0.5089353,
        size.height * 0.4021203,
        size.width * 0.4818784,
        size.height * 0.4160344);
    path_226.close();

    Paint paint_226_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_226_stroke.color = Colors.black;
    canvas.drawPath(path_226, paint_226_stroke);

    Paint paint_226_fill = Paint()..style = PaintingStyle.fill;
    paint_226_fill.color = Colors.white;
    canvas.drawPath(path_226, paint_226_fill);

    Path path_227 = Path();
    path_227.moveTo(size.width * 0.3496662, size.height * 0.3021578);
    path_227.cubicTo(
        size.width * 0.3477223,
        size.height * 0.3246703,
        size.width * 0.3662597,
        size.height * 0.3458500,
        size.width * 0.3822122,
        size.height * 0.3481945);
    path_227.cubicTo(
        size.width * 0.3838201,
        size.height * 0.3484313,
        size.width * 0.3852820,
        size.height * 0.3471313,
        size.width * 0.3853683,
        size.height * 0.3453711);
    path_227.cubicTo(
        size.width * 0.3854964,
        size.height * 0.3427602,
        size.width * 0.3841216,
        size.height * 0.3393062,
        size.width * 0.3853763,
        size.height * 0.3346992);
    path_227.cubicTo(
        size.width * 0.3955914,
        size.height * 0.3339594,
        size.width * 0.4960640,
        size.height * 0.2980727,
        size.width * 0.4960640,
        size.height * 0.2980727);
    path_227.cubicTo(
        size.width * 0.5015856,
        size.height * 0.2426555,
        size.width * 0.4947626,
        size.height * 0.2146680,
        size.width * 0.4507676,
        size.height * 0.2109938);
    path_227.cubicTo(
        size.width * 0.4398014,
        size.height * 0.2100781,
        size.width * 0.4312532,
        size.height * 0.2203664,
        size.width * 0.4304820,
        size.height * 0.2352922);
    path_227.cubicTo(
        size.width * 0.4304820,
        size.height * 0.2352922,
        size.width * 0.4204029,
        size.height * 0.2086250,
        size.width * 0.3940144,
        size.height * 0.2171828);
    path_227.cubicTo(
        size.width * 0.3753072,
        size.height * 0.2232492,
        size.width * 0.3728036,
        size.height * 0.2632742,
        size.width * 0.3698245,
        size.height * 0.2715117);
    path_227.cubicTo(
        size.width * 0.3678079,
        size.height * 0.2770836,
        size.width * 0.3518647,
        size.height * 0.2766852,
        size.width * 0.3496662,
        size.height * 0.3021578);
    path_227.close();

    Paint paint_227_fill = Paint()..style = PaintingStyle.fill;
    paint_227_fill.color = Colors.black;
    canvas.drawPath(path_227, paint_227_fill);

    Path path_228 = Path();
    path_228.moveTo(size.width * 0.3496662, size.height * 0.3021578);
    path_228.cubicTo(
        size.width * 0.3477223,
        size.height * 0.3246703,
        size.width * 0.3662597,
        size.height * 0.3458500,
        size.width * 0.3822122,
        size.height * 0.3481945);
    path_228.cubicTo(
        size.width * 0.3838201,
        size.height * 0.3484313,
        size.width * 0.3852820,
        size.height * 0.3471313,
        size.width * 0.3853683,
        size.height * 0.3453711);
    path_228.cubicTo(
        size.width * 0.3854964,
        size.height * 0.3427602,
        size.width * 0.3841216,
        size.height * 0.3393062,
        size.width * 0.3853763,
        size.height * 0.3346992);
    path_228.cubicTo(
        size.width * 0.3955914,
        size.height * 0.3339594,
        size.width * 0.4960640,
        size.height * 0.2980727,
        size.width * 0.4960640,
        size.height * 0.2980727);
    path_228.cubicTo(
        size.width * 0.5015856,
        size.height * 0.2426555,
        size.width * 0.4947626,
        size.height * 0.2146680,
        size.width * 0.4507676,
        size.height * 0.2109938);
    path_228.cubicTo(
        size.width * 0.4398014,
        size.height * 0.2100781,
        size.width * 0.4312532,
        size.height * 0.2203664,
        size.width * 0.4304820,
        size.height * 0.2352922);
    path_228.cubicTo(
        size.width * 0.4304820,
        size.height * 0.2352922,
        size.width * 0.4204029,
        size.height * 0.2086250,
        size.width * 0.3940144,
        size.height * 0.2171828);
    path_228.cubicTo(
        size.width * 0.3753072,
        size.height * 0.2232492,
        size.width * 0.3728036,
        size.height * 0.2632742,
        size.width * 0.3698245,
        size.height * 0.2715117);
    path_228.cubicTo(
        size.width * 0.3678079,
        size.height * 0.2770836,
        size.width * 0.3518647,
        size.height * 0.2766852,
        size.width * 0.3496662,
        size.height * 0.3021578);
    path_228.close();

    Paint paint_228_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_228_stroke.color = Colors.black;
    canvas.drawPath(path_228, paint_228_stroke);

    Paint paint_228_fill = Paint()..style = PaintingStyle.fill;
    paint_228_fill.color = Colors.black;
    canvas.drawPath(path_228, paint_228_fill);

    Path path_229 = Path();
    path_229.moveTo(size.width * 0.4871079, size.height * 0.3287164);
    path_229.cubicTo(
        size.width * 0.4914511,
        size.height * 0.3659258,
        size.width * 0.4780892,
        size.height * 0.4091023,
        size.width * 0.4509827,
        size.height * 0.4091023);
    path_229.cubicTo(
        size.width * 0.4238770,
        size.height * 0.4091023,
        size.width * 0.4040511,
        size.height * 0.3756078,
        size.width * 0.3853489,
        size.height * 0.3346844);
    path_229.cubicTo(
        size.width * 0.3853489,
        size.height * 0.3346844,
        size.width * 0.3848935,
        size.height * 0.3264227,
        size.width * 0.3911410,
        size.height * 0.3179148);
    path_229.cubicTo(
        size.width * 0.3973878,
        size.height * 0.3094062,
        size.width * 0.4207036,
        size.height * 0.3029016,
        size.width * 0.4292662,
        size.height * 0.2932828);
    path_229.cubicTo(
        size.width * 0.4374885,
        size.height * 0.2840469,
        size.width * 0.4442266,
        size.height * 0.2545016,
        size.width * 0.4442266,
        size.height * 0.2545016);
    path_229.cubicTo(
        size.width * 0.4442266,
        size.height * 0.2545016,
        size.width * 0.4604072,
        size.height * 0.2797102,
        size.width * 0.4728043,
        size.height * 0.3052523);
    path_229.cubicTo(
        size.width * 0.4728043,
        size.height * 0.3052523,
        size.width * 0.4810209,
        size.height * 0.2834398,
        size.width * 0.4901511,
        size.height * 0.2913719);
    path_229.cubicTo(
        size.width * 0.5118338,
        size.height * 0.3102094,
        size.width * 0.4871079,
        size.height * 0.3287164,
        size.width * 0.4871079,
        size.height * 0.3287164);
    path_229.close();

    Paint paint_229_fill = Paint()..style = PaintingStyle.fill;
    paint_229_fill.color = Colors.black;
    canvas.drawPath(path_229, paint_229_fill);

    Path path_230 = Path();
    path_230.moveTo(size.width * 0.4871079, size.height * 0.3287164);
    path_230.cubicTo(
        size.width * 0.4914511,
        size.height * 0.3659258,
        size.width * 0.4780892,
        size.height * 0.4091023,
        size.width * 0.4509827,
        size.height * 0.4091023);
    path_230.cubicTo(
        size.width * 0.4238770,
        size.height * 0.4091023,
        size.width * 0.4040511,
        size.height * 0.3756078,
        size.width * 0.3853489,
        size.height * 0.3346844);
    path_230.cubicTo(
        size.width * 0.3853489,
        size.height * 0.3346844,
        size.width * 0.3848935,
        size.height * 0.3264227,
        size.width * 0.3911410,
        size.height * 0.3179148);
    path_230.cubicTo(
        size.width * 0.3973878,
        size.height * 0.3094062,
        size.width * 0.4207036,
        size.height * 0.3029016,
        size.width * 0.4292662,
        size.height * 0.2932828);
    path_230.cubicTo(
        size.width * 0.4374885,
        size.height * 0.2840469,
        size.width * 0.4442266,
        size.height * 0.2545016,
        size.width * 0.4442266,
        size.height * 0.2545016);
    path_230.cubicTo(
        size.width * 0.4442266,
        size.height * 0.2545016,
        size.width * 0.4604072,
        size.height * 0.2797102,
        size.width * 0.4728043,
        size.height * 0.3052523);
    path_230.cubicTo(
        size.width * 0.4728043,
        size.height * 0.3052523,
        size.width * 0.4810209,
        size.height * 0.2834398,
        size.width * 0.4901511,
        size.height * 0.2913719);
    path_230.cubicTo(
        size.width * 0.5118338,
        size.height * 0.3102094,
        size.width * 0.4871079,
        size.height * 0.3287164,
        size.width * 0.4871079,
        size.height * 0.3287164);
    path_230.close();

    Paint paint_230_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.004964460;
    paint_230_stroke.color = Colors.black;
    canvas.drawPath(path_230, paint_230_stroke);

    Paint paint_230_fill = Paint()..style = PaintingStyle.fill;
    paint_230_fill.color = Colors.white;
    canvas.drawPath(path_230, paint_230_fill);
  }

  @override
  bool shouldRepaint(covariant TimePayCustomPainter oldDelegate) {
    return oldDelegate.active != active;
  }
}

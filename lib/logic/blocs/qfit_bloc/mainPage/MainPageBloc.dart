import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/logic/blocs/qfit_bloc/mainPage/model/main_page_model.dart';
import 'package:li/network/graphql_provider.dart';

part './MainPageEvent.dart';
part './MainPageState.dart';

part 'reaction/get_data_reaction.dart';

class MainPageMinuteBloc
    extends Bloc<MainPageMinuteEvent, MainPageMinuteState> {
  final GraphqlMinuteProvider graphqlMinuteProvider;

  MainPageMinuteBloc(this.graphqlMinuteProvider)
      : super(MainPageMinuteState()) {
    on<GetDataMinuteEvent>(getData);
    add(GetDataMinuteEvent());
  }
}

import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:li/data/models/credit_card_model.dart';
import 'package:li/data/models/photo_url.dart';
import 'package:li/data/models/sex.dart';
import 'package:li/utils/date_time_parse.dart';

import 'city_enum.dart';

class Client extends Equatable {
  final String? id;
  final DateTime? dateOfBirth;
  final String? email;
  final String? fullName;
  final int? debt;
  final double? bonus;
  final String? phoneNumber;
  final PhotoUrl? photoURL;
  final SexEnum? sex;
  final List<CreditCardModel>? creditCards;
  final CityEnum? city;
  Client({
    this.id,
    this.dateOfBirth,
    this.email,
    this.fullName,
    this.debt,
    this.bonus,
    this.phoneNumber,
    this.photoURL,
    this.sex,
    this.creditCards,
    this.city,
  });

  Client copyWith({
    String? id,
    DateTime? dateOfBirth,
    String? email,
    String? fullName,
    int? debt,
    double? bonus,
    String? phoneNumber,
    PhotoUrl? photoURL,
    SexEnum? sex,
    List<CreditCardModel>? creditCards,
    CityEnum? city,
  }) {
    return Client(
      id: id ?? this.id,
      dateOfBirth: dateOfBirth ?? this.dateOfBirth,
      email: email ?? this.email,
      fullName: fullName ?? this.fullName,
      debt: debt ?? this.debt,
      bonus: bonus ?? this.bonus,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      photoURL: photoURL ?? this.photoURL,
      sex: sex ?? this.sex,
      creditCards: creditCards ?? this.creditCards,
      city: city ?? this.city,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'dateOfBirth': dateOfBirth?.millisecondsSinceEpoch,
      'email': email,
      'fullName': fullName,
      'debt': debt,
      'bonus': bonus,
      'phoneNumber': phoneNumber,
      'photoURL': photoURL?.toMap(),
      'sex': sex?.toMap(),
      'creditCards': creditCards?.map((e) => e.toMap()).toList(),
      'city': city?.toMap(),
    };
  }

  factory Client.fromMap(Map<String, dynamic> map) {
    return Client(
      id: map['_id'],
      dateOfBirth: dateTimeParse(map['dateOfBirth']),
      email: map['email'],
      fullName: map['fullName'],
      debt: map['debt']?.toInt(),
      bonus: map['bonus']?.toDouble(),
      phoneNumber: map['phoneNumber'],
      photoURL:
          map['photoURL'] != null ? PhotoUrl.fromMap(map['photoURL']) : null,
      sex: map['sex'] != null ? SexEnum.fromMap(map['sex']) : null,
      creditCards: map['creditCards'] != null
          ? List<CreditCardModel>.from(
              map['creditCards']?.map((x) => CreditCardModel.fromMap(x)),
            )
          : null,
      city: map['city'] != null ? CityEnum.fromMap(map['city']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Client.fromJson(String source) => Client.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Client(_id: $id, dateOfBirth: $dateOfBirth, email: $email, fullName: $fullName, debt: $debt, bonus: $bonus, phoneNumber: $phoneNumber, photoURL: $photoURL, sex: $sex, creditCards: $creditCards, city: $city)';
  }

  @override
  List<Object?> get props {
    return [
      id,
      dateOfBirth,
      email,
      fullName,
      debt,
      bonus,
      phoneNumber,
      photoURL,
      sex,
      creditCards,
      city,
    ];
  }
}

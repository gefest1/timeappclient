import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class LoadLocation extends StatefulWidget {
  const LoadLocation({Key? key}) : super(key: key);

  @override
  _LoadLocationState createState() => _LoadLocationState();
}

class _LoadLocationState extends State<LoadLocation>
    with SingleTickerProviderStateMixin {
  late Animation<double> animation;
  late AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 1500),
    );

    animation = Tween<double>(begin: .0, end: .4).animate(controller)
      ..addListener(() {
        if (this.mounted) {
          setState(() {});
        }
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.reset();
          controller.forward();
        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      });

    controller.forward();
  }

  List<double> _stops = List<double>.generate(8, (index) => index * 0.2 - 0.4);
  List<Color> _colors = List<Color>.generate(
    8,
    (index) =>
        index.isOdd ? Colors.transparent : Color(0xffF9F9F9).withOpacity(0.3),
  );

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      builder: (contx, child) {
        return DecoratedBox(
          position: DecorationPosition.foreground,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            gradient: LinearGradient(
              begin: Alignment(-1.0, -1.0),
              end: Alignment(1.0, 1.0),
              colors: _colors,
              stops: _stops.map((s) => s + animation.value).toList(),
            ),
          ),
          child: child,
        );
      },
      animation: controller,
      child: Container(
        height: 22,
        padding: const EdgeInsets.symmetric(
          horizontal: 8,
          vertical: 3,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(6),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: RepaintBoundary(
              child: SvgPicture.asset('assets/svg/icon/sendFilled.svg')),
        ),
      ),
    );
  }
}

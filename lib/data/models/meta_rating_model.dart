import 'dart:convert';

import 'package:li/data/models/grouped_raiting_model.dart';

class MetaRatingModel {
  final GroupedRatingModel? groupedRatings;
  final int? totalSum;
  MetaRatingModel({
    this.groupedRatings,
    this.totalSum,
  });

  MetaRatingModel copyWith({
    GroupedRatingModel? groupedRatings,
    int? totalSum,
  }) {
    return MetaRatingModel(
      groupedRatings: groupedRatings ?? this.groupedRatings,
      totalSum: totalSum ?? this.totalSum,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'groupedRatings': groupedRatings!.toMap(),
      'totalSum': totalSum,
    };
  }

  factory MetaRatingModel.fromMap(Map<String, dynamic> map) {
    return MetaRatingModel(
      groupedRatings: map['groupedRatings'] == null
          ? null
          : GroupedRatingModel.fromMap(map['groupedRatings']),
      totalSum: map['totalSum'],
    );
  }

  String toJson() => json.encode(toMap());

  factory MetaRatingModel.fromJson(String source) =>
      MetaRatingModel.fromMap(json.decode(source));

  @override
  String toString() =>
      'MetaRatingModel(groupedRatings: $groupedRatings, totalSum: $totalSum)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is MetaRatingModel &&
        other.groupedRatings == groupedRatings &&
        other.totalSum == totalSum;
  }

  @override
  int get hashCode => groupedRatings.hashCode ^ totalSum.hashCode;
}

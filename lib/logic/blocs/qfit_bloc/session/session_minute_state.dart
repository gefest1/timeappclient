part of 'session_minute_bloc.dart';

class SessionState extends Equatable {
  const SessionState();
  @override
  List<Object?> get props => [];
}

class StartedSessionState extends SessionState {
  final List<SessionMinute> sessions;

  const StartedSessionState({this.sessions = const []});

  @override
  List<Object?> get props => [sessions];
}

class LeaveCommentSuccess extends SessionState {
  const LeaveCommentSuccess();
}

class ErrorSessionState extends StartedSessionState {
  late final DateTime _dateTime;
  ErrorSessionState({final List<SessionMinute> sessions = const []})
      : super(sessions: sessions) {
    _dateTime = DateTime.now();
  }

  @override
  List<Object?> get props => [sessions, _dateTime];
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:li/utils/color.dart';

bool hey = false;

class MapPointer extends StatefulWidget {
  final String? title;
  final String? label;
  final String? tile;

  final VoidCallback? onTap;

  const MapPointer({
    this.title,
    this.label,
    this.tile,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  State<MapPointer> createState() => _MapPointerState();
}

class _MapPointerState extends State<MapPointer> {
  late PointerDownEvent initEvent;

  @override
  Widget build(BuildContext context) {
    return Listener(
      behavior: HitTestBehavior.translucent,
      onPointerDown: (PointerDownEvent _) {
        initEvent = _;
        hey = true;
      },
      onPointerUp: (_) async {
        if (!hey) return;

        hey = false;
        final _diff = initEvent.position - _.position;
        if (_diff.dx > -10 &&
            _diff.dx < 10 &&
            _diff.dy > -10 &&
            _diff.dy < 10) {
          widget.onTap?.call();
        }
      },
      child: RepaintBoundary(
        child: IgnorePointer(
          ignoring: true,
          child: CustomPaint(
            painter: MapPointerlPainter(),
            child: Padding(
              padding: const EdgeInsets.only(bottom: 20),
              child: SizedBox(
                width: 194,
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            if (widget.title != null)
                              RichText(
                                text: TextSpan(
                                  text: widget.title!,
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600,
                                    height: 17 / 14,
                                    color: ColorData.allMainBlack,
                                  ),
                                ),
                              ),
                            if (widget.label != null)
                              Text(
                                widget.label!,
                                style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                  height: 14 / 12,
                                  color: ColorData.allMainActivegray,
                                ),
                              ),
                          ],
                        ),
                      ),
                      if (widget.tile != null)
                        Row(
                          children: [
                            RepaintBoundary(
                              child: SvgPicture.asset(
                                'assets/svg/icon/sendFilled.svg',
                                height: 12,
                                width: 12,
                                color: ColorData.allMainBlack,
                              ),
                            ),
                            const SizedBox(width: 2),
                            Text(
                              widget.tile!,
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 12,
                                height: 14 / 12,
                                color: ColorData.allMainBlack,
                              ),
                            )
                          ],
                        )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class MapPointerlPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path _path = Path();

    _path.cubicTo(7.02767, 0, 13.1132, 4.87915, 14.6412, 11.7387);
    _path.lineTo(16.7662, 21.279);
    _path.cubicTo(16.9942, 22.3024, 18.4431, 22.3298, 18.7095, 21.3157);
    _path.lineTo(21.3702, 11.1885);
    _path.cubicTo(23.1022, 4.59591, 28.9686, 0, 35.7849, 0);

    _path.close();
    _path = _path.shift(
      Offset(size.width / 2 - 18, size.height - 21),
    );

    _path = Path.combine(
      PathOperation.union,
      _path,
      Path()
        ..addRRect(
          RRect.fromLTRBR(
            0,
            0,
            size.width,
            size.height - 20,
            Radius.circular(16),
          ),
        ),
    );
    canvas.drawPath(
      _path,
      BoxShadow(
        offset: Offset(1, 1),
        blurRadius: 5,
        color: Colors.black12,
      ).toPaint(),
    );
    canvas.drawPath(
      _path,
      Paint()..color = Colors.white,
    );
  }

  @override
  bool shouldRepaint(MapPointerlPainter oldDelegate) => true;

  @override
  bool shouldRebuildSemantics(MapPointerlPainter oldDelegate) => true;
}

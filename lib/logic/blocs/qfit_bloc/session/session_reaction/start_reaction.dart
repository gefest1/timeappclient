part of '../session_minute_bloc.dart';

extension StartReaction on SessionMinuteBloc {
  void startSession(
    StartSessionEvent event,
    Emitter<SessionState> emit,
  ) async {
    final _initBlock = !blockBackRoutes;
    if (_initBlock) routerDelegate.blockWidgets();

    try {
      final _newSession = await StartSession.startSession(
        client: graphqlMinuteProvider.client,
        institutionId: event.institutionId,
        creditCardId: event.creditCardId,
        serviceId: event.serviceId,
        useBonus: event.useBonus,
      );

      final List<SessionMinute> _lastS = (state is StartedSessionState)
          ? (this.state as StartedSessionState).sessions
          : [];
      if (event.completer != null) {
        event.completer!.complete();
      }
      emit(
        StartedSessionState(
          sessions: [
            ..._lastS,
            _newSession,
          ],
        ),
      );

      if (_initBlock) routerDelegate.unBlockWidgets();
      routerDelegate.pushAndRemoveUntil(ActiveMinutePage(session: _newSession));
      return;
    } catch (e) {
      if (event.completer != null) {
        event.completer!.completeError(e);
      }
      emit(
        ErrorSessionState(
          sessions: (state is StartedSessionState)
              ? (this.state as StartedSessionState).sessions
              : [],
        ),
      );
      log(e.toString());
    }

    if (_initBlock) routerDelegate.unBlockWidgets();
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';

import 'package:li/data/models/review/comment_model.dart';
import 'package:li/utils/color.dart';
import 'package:readmore/readmore.dart';
import 'package:time_app_components/utils/testStyles.dart';

class ReviewCard extends StatefulWidget {
  final CommentModel comments;
  final Color? backgroundColor;

  const ReviewCard({
    required this.comments,
    this.backgroundColor,
    Key? key,
  }) : super(key: key);

  @override
  State<ReviewCard> createState() => _ReviewCardState();
}

class _ReviewCardState extends State<ReviewCard> {
  @override
  Widget build(BuildContext context) {
    DateTime parseDt = DateTime.parse(widget.comments.dateAdded!);
    var newDt = DateFormat.Md().format(parseDt);
    String updatedDt = newDt;
    return DecoratedBox(
      decoration: BoxDecoration(
        color: Color(0xffF8F8F8),
        borderRadius: BorderRadius.circular(16),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Row(
                children: [
                  Expanded(
                    child: Row(
                      children: [
                        Container(
                          height: 40,
                          width: 40,
                          alignment: Alignment.center,
                          child: widget.comments.client?.photoURL?.xl == null
                              ? Text(
                                  '${widget.comments.client?.fullName}'
                                      .substring(0, 1),
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                      fontSize: 30, color: Colors.white),
                                )
                              : null,
                          decoration: BoxDecoration(
                            image: widget.comments.client?.photoURL?.xl == null
                                ? null
                                : DecorationImage(
                                    image: NetworkImage(
                                      widget.comments.client!.photoURL!.xl!,
                                    ),
                                    fit: BoxFit.cover,
                                  ),
                            shape: BoxShape.circle,
                            color: widget.backgroundColor,
                          ),
                        ),
                        const SizedBox(width: 10),
                        if (widget.comments.client?.fullName != null)
                          Expanded(
                            child: Text(
                              widget.comments.client!.fullName!,
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                height: 19 / 16,
                              ),
                            ),
                          ),
                      ],
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Row(
                        children: List.generate(
                          widget.comments.rating!,
                          (index) => SvgPicture.asset(
                            'assets/svg/icon/star.svg',
                            color: Color(0xffffe600),
                            height: 20,
                            width: 20,
                          ),
                        ),
                      ),
                      Text(
                        updatedDt,
                        style: TextStyle(
                          fontSize: P3TextStyle.fontSize,
                          fontWeight: P3TextStyle.fontWeight,
                          height: P3TextStyle.height,
                          color: ColorData.allMainActivegray,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            const SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: ReadMoreText(
                widget.comments.text,
                trimLines: 7,
                trimMode: TrimMode.Line,
                trimCollapsedText: '...читать полностью',
                delimiter: ' ',
                trimExpandedText: 'скрыть',
                style: TextStyle(
                  fontWeight: P3TextStyle.fontWeight,
                  fontSize: P3TextStyle.fontSize,
                  height: P3TextStyle.height,
                ),
                lessStyle: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: P3TextStyle.fontSize,
                  height: P3TextStyle.height,
                  color: ColorData.specsButtonColorDefault,
                ),
                moreStyle: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: P3TextStyle.fontSize,
                  height: P3TextStyle.height,
                  color: ColorData.specsButtonColorDefault,
                ),
              ),
            ),
            // Padding(
            //   padding: const EdgeInsets.only(top: 20.0),
            //   child: SizedBox(
            //     height: 68,
            //     child: ListView.separated(
            //       padding: const EdgeInsets.symmetric(horizontal: 16),
            //       scrollDirection: Axis.horizontal,
            //       itemBuilder: (ctx, _) => Container(
            //         height: 68,
            //         width: 92,
            //         decoration: BoxDecoration(
            //           borderRadius: BorderRadius.all(
            //             Radius.circular(16),
            //           ),
            //           color: Colors.black26,
            //         ),
            //       ),
            //       separatorBuilder: (ctx, _) => SizedBox(width: 10),
            //       itemCount: 8,
            //     ),
            //   ),
            // ),
            // const SizedBox(height: 6),
            // Center(
            //   child: GestureDetector(
            //     behavior: HitTestBehavior.opaque,
            //     onTap: () {
            //       log('daw');
            //     },
            //     child: IgnorePointer(
            //       child: Padding(
            //         padding:
            //             const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
            //         child: Text(
            //           'Ответить',
            //           style: TextStyle(
            //             fontWeight: P3TextStyle.fontWeight,
            //             fontSize: P3TextStyle.fontSize,
            //             height: P3TextStyle.height,
            //             color: ColorData.specsButtonColorDefault,
            //           ),
            //         ),
            //       ),
            //     ),
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}

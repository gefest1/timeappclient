import 'package:flutter/material.dart';

class DrawnLine {
  final List<Offset> path;
  final Color color;
  final double width;
  final Color fillColor;

  const DrawnLine({
    required this.path,
    required this.color,
    required this.width,
    required this.fillColor,
  });
}

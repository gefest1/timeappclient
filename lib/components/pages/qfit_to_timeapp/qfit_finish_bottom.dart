import 'dart:async';
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/const.dart';
import 'package:li/data/models/credit_card_model.dart';
import 'package:li/data/models/session_minute.dart';
import 'package:li/logic/blocs/qfit_bloc/session/session_minute_bloc.dart';
import 'package:li/main.dart';
import 'package:li/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

import 'feedback/feedback.dart';
import 'tile_information.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

class QFitFinishBottom extends StatefulWidget {
  final SessionMinute session;

  const QFitFinishBottom({
    required this.session,
    Key? key,
  }) : super(key: key);

  @override
  _QFitFinishBottomState createState() => _QFitFinishBottomState();
}

class _QFitFinishBottomState extends State<QFitFinishBottom> {
  final ValueNotifier<CreditCardModel?> creditCard =
      ValueNotifier<CreditCardModel?>(null);

  @override
  Widget build(BuildContext context) {
    final diffTime = DateTime.now().difference(widget.session.startTime!);
    Duration _priceDuration = Duration(
      milliseconds: math.max(
        0,
        (diffTime -
                Duration(
                  minutes:
                      widget.session.service?.bufferTimeMinutes?.toInt() ?? 0,
                ))
            .inMilliseconds,
      ),
    );

    return Material(
      borderRadius: BorderRadius.vertical(
        top: Radius.circular(16),
      ),
      color: Colors.white,
      child: Stack(
        children: [
          CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Padding(
                  padding: EdgeInsets.only(
                    top: 62,
                    left: 20,
                    right: 20,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 20),
                      // const SizedBox(height: 30),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                          color: Color(
                            0xFFF9F9F9,
                          ),
                        ),
                        padding: EdgeInsets.all(15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        '${widget.session.institution!.name}',
                                        style: TextStyle(
                                          color: Color(0xff434343),
                                          fontSize: H2TextStyle.fontSize,
                                          fontWeight: H2TextStyle.fontWeight,
                                          height: H2TextStyle.height,
                                        ),
                                      ),
                                      Text(
                                        (widget.session.institution?.location
                                                    ?.address !=
                                                null)
                                            ? '${widget.session.institution!.location!.address}'
                                            : '',
                                        style: TextStyle(
                                          color: Color(0xff434343),
                                          fontSize: P4TextStyle.fontSize,
                                          fontWeight: P4TextStyle.fontWeight,
                                          height: P4TextStyle.height,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SvgPicture.asset(
                                  'assets/svg/service_gym.svg',
                                ),
                              ],
                            ),
                            SizedBox(height: 10),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Expanded(
                                  child: Container(
                                    height: 30,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(6),
                                      color: Color(
                                        0xFF434343,
                                      ),
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          '${widget.session.service!.price} тг/мин',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: P4TextStyle.fontSize,
                                            fontWeight: P4TextStyle.fontWeight,
                                            height: P4TextStyle.height,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 10),
                                TileInformation(
                                  isStar: true,
                                  title:
                                      widget.session.institution!.rating == null
                                          ? null
                                          : widget.session.institution!.rating
                                              .toString(),
                                ),
                                const SizedBox(width: 10),
                                TileInformation(
                                  title: (widget.session.institution
                                              ?.ratingCount ??
                                          0)
                                      .toString(),
                                  svgPath: 'assets/svg/icon/ic_share.svg',
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 30),
                      Text(
                        'О сессии',
                        style: TextStyle(
                          fontWeight: H2TextStyle.fontWeight,
                          fontSize: H2TextStyle.fontSize,
                          height: H2TextStyle.height,
                          color: ColorData.allMainBlack,
                        ),
                      ),
                      const SizedBox(height: 10),
                      Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Услуга',
                                style: TextStyle(
                                  color: Color(0xff979797),
                                  fontSize: P3TextStyle.fontSize,
                                  fontWeight: P3TextStyle.fontWeight,
                                  height: P3TextStyle.height,
                                ),
                              ),
                              Text(
                                widget.session.service!.name!,
                                style: TextStyle(
                                  color: Color(0xff434343),
                                  fontSize: P3TextStyle.fontSize,
                                  fontWeight: P3TextStyle.fontWeight,
                                  height: P3TextStyle.height,
                                ),
                              )
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Длительность сессии',
                                style: TextStyle(
                                  color: Color(0xff979797),
                                  fontSize: P3TextStyle.fontSize,
                                  fontWeight: P3TextStyle.fontWeight,
                                  height: P3TextStyle.height,
                                ),
                              ),
                              Text(
                                diffTime.inMinutes.toString() + ' мин',
                                style: TextStyle(
                                  color: Color(0xff434343),
                                  fontSize: P3TextStyle.fontSize,
                                  fontWeight: P3TextStyle.fontWeight,
                                  height: P3TextStyle.height,
                                ),
                              )
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Стоимость сессии',
                                style: TextStyle(
                                  color: Color(0xff979797),
                                  fontSize: P3TextStyle.fontSize,
                                  fontWeight: P3TextStyle.fontWeight,
                                  height: P3TextStyle.height,
                                ),
                              ),
                              Text(
                                '${widget.session.service!.price! * _priceDuration.inMinutes} тенге',
                                style: TextStyle(
                                  color: Color(0xff434343),
                                  fontSize: P3TextStyle.fontSize,
                                  fontWeight: P3TextStyle.fontWeight,
                                  height: P3TextStyle.height,
                                ),
                              )
                            ],
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
              SliverLayoutBuilder(
                builder: (ctx, SliverConstraints constraints) {
                  final _dd = constraints.viewportMainAxisExtent -
                      constraints.precedingScrollExtent -
                      140;
                  return SliverToBoxAdapter(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        children: [
                          if (_dd > 0) SizedBox(height: _dd),
                          const SizedBox(height: 20),
                          ElevatedButton(
                            onPressed: () async {
                              final _sessionBloc =
                                  BlocProvider.of<SessionMinuteBloc>(context,
                                      listen: false);

                              final Completer<SessionMinute> completer =
                                  Completer<SessionMinute>();

                              _sessionBloc.add(
                                FinishSessionEvent(
                                  sessionId: widget.session,
                                  completer: completer,
                                ),
                              );
                              final result = await completer.future;
                              routerDelegate.pushAndRemoveUntil(
                                FeedbackPage(
                                  session: result,
                                  institutionId: widget.session.institutionId!,
                                ),
                              );
                              FirebaseAnalytics().logEvent(
                                  name: 'PayPerMinuteEnding',
                                  parameters: <String, dynamic>{
                                    'duration': diffTime.inMinutes.toDouble(),
                                    'Institution': widget
                                        .session.institution?.name
                                        .toString(),
                                    'InstitutionType': widget
                                        .session.institution?.type
                                        .toString(),
                                    'ClientID':
                                        widget.session.client?.id.toString(),
                                    'ClientNumber': widget
                                        .session.client?.phoneNumber
                                        .toString(),
                                  });
                            },
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all(
                                ColorData.clientsButtonColorDefault,
                              ),
                              minimumSize: MaterialStateProperty.all(
                                Size(double.infinity, 60),
                              ),
                              shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(16),
                                  side: BorderSide.none,
                                ),
                              ),
                            ),
                            child: Text(
                              'Завершить сессию',
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 18,
                              ),
                            ),
                          ),
                          const SizedBox(height: 40),
                        ],
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
          Positioned(
            left: 0,
            right: 0,
            top: 0,
            child: Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                boxShadow: bigBlur,
                color: Colors.white,
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(16),
                ),
              ),
              padding: EdgeInsets.all(20),
              child: Text(
                'Подтверждение',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 18,
                  height: 21 / 18,
                  color: ColorData.allMainBlack,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

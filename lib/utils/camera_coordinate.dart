import 'dart:convert';

class CameraCoordinate {
  final num latitude;
  final num longitude;
  final bool finalCordinate;
  final num zoom;
  final num azimuth;
  final num tilt;
  const CameraCoordinate({
    required this.latitude,
    required this.longitude,
    required this.finalCordinate,
    required this.zoom,
    required this.azimuth,
    required this.tilt,
  });

  CameraCoordinate copyWith({
    num? latitude,
    num? longitude,
    bool? finalCordinate,
    num? zoom,
    num? azimuth,
    num? tilt,
  }) {
    return CameraCoordinate(
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
      finalCordinate: finalCordinate ?? this.finalCordinate,
      zoom: zoom ?? this.zoom,
      azimuth: azimuth ?? this.azimuth,
      tilt: tilt ?? this.tilt,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'latitude': latitude,
      'longitude': longitude,
      'final': finalCordinate,
      'zoom': zoom,
      'azimuth': azimuth,
      'tilt': tilt,
    };
  }

  factory CameraCoordinate.fromMap(Map<String, dynamic> map) {
    return CameraCoordinate(
      latitude: map['latitude'],
      longitude: map['longitude'],
      finalCordinate: map['final'],
      zoom: map['zoom'],
      azimuth: map['azimuth'],
      tilt: map['tilt'],
    );
  }

  String toJson() => json.encode(toMap());

  factory CameraCoordinate.fromJson(String source) =>
      CameraCoordinate.fromMap(json.decode(source));

  @override
  String toString() {
    return 'CameraCordinate(latitude: $latitude, longitude: $longitude, finalCordinate: $finalCordinate, zoom: $zoom, azimuth: $azimuth, tilt: $tilt)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is CameraCoordinate &&
        other.latitude == latitude &&
        other.longitude == longitude &&
        other.finalCordinate == finalCordinate &&
        other.zoom == zoom &&
        other.azimuth == azimuth &&
        other.tilt == tilt;
  }

  @override
  int get hashCode {
    return latitude.hashCode ^
        longitude.hashCode ^
        finalCordinate.hashCode ^
        zoom.hashCode ^
        azimuth.hashCode ^
        tilt.hashCode;
  }
}

import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:li/data/models/tag_minute.dart';

import 'allowed_institution_types.dart';
import 'contacts.dart';
import 'photo_url.dart';
import 'worker.dart';
import 'location.dart';
import 'work_times.dart';

class InstitutionsMinute extends Equatable {
  final String? id;
  final PhotoUrl? avatarURL;
  final num? averagePrice;
  final String? city;
  final String? dateAdded;
  final String? description;
  final List<PhotoUrl?>? galleryURLs;
  final Location? location;
  final String? name;
  final num? rating;
  final num? ratingCount;
  final AllowedInstitutionTypes? type;
  final List<Worker?>? workers;
  final Contacts? contacts;
  final List<WorkTimes>? workTimes;
  final List<TagMinute>? tags;
  final String? iconPath;

  InstitutionsMinute({
    this.id,
    this.avatarURL,
    this.averagePrice,
    this.city,
    this.dateAdded,
    this.description,
    this.galleryURLs,
    this.location,
    this.name,
    this.rating,
    this.ratingCount,
    this.tags,
    this.type,
    this.workers,
    this.contacts,
    this.workTimes,
    this.iconPath,
  });

  InstitutionsMinute copyWith({
    String? id,
    PhotoUrl? avatarURL,
    num? averagePrice,
    String? city,
    String? dateAdded,
    String? description,
    List<PhotoUrl?>? galleryURLs,
    Location? location,
    String? name,
    num? rating,
    num? ratingCount,
    List<TagMinute>? tags,
    AllowedInstitutionTypes? type,
    List<Worker?>? workers,
    Contacts? contacts,
    List<WorkTimes>? workTimes,
    String? iconPath,
  }) {
    return InstitutionsMinute(
      id: id ?? this.id,
      avatarURL: avatarURL ?? this.avatarURL,
      averagePrice: averagePrice ?? this.averagePrice,
      city: city ?? this.city,
      dateAdded: dateAdded ?? this.dateAdded,
      description: description ?? this.description,
      galleryURLs: galleryURLs ?? this.galleryURLs,
      location: location ?? this.location,
      name: name ?? this.name,
      rating: rating ?? this.rating,
      ratingCount: ratingCount ?? this.ratingCount,
      tags: tags ?? this.tags,
      type: type ?? this.type,
      workers: workers ?? this.workers,
      contacts: contacts ?? this.contacts,
      workTimes: workTimes ?? this.workTimes,
      iconPath: iconPath ?? this.iconPath,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'avatarURL': avatarURL?.toMap(),
      'averagePrice': averagePrice,
      'city': city,
      'dateAdded': dateAdded,
      'description': description,
      'galleryURLs': galleryURLs?.map((x) => x?.toMap()).toList(),
      'location': location?.toMap(),
      'name': name,
      'rating': rating,
      'ratingCount': ratingCount,
      'tags': tags?.map((x) => x.toMap()).toList(),
      'type': type?.toMap(),
      'workers': workers?.map((x) => x?.toMap()).toList(),
      'contacts': contacts?.toMap(),
      'workTimes': workTimes?.map((x) => x.toMap()).toList(),
      'iconPath': iconPath
    };
  }

  factory InstitutionsMinute.fromMap(Map<String, dynamic> map) {
    return InstitutionsMinute(
      id: map['_id'] != null ? map['_id'] : null,
      avatarURL:
          map['avatarURL'] != null ? PhotoUrl.fromMap(map['avatarURL']) : null,
      averagePrice: map['averagePrice'] != null ? map['averagePrice'] : null,
      city: map['city'] != null ? map['city'] : null,
      dateAdded: map['dateAdded'] != null ? map['dateAdded'] : null,
      description: map['description'] != null ? map['description'] : null,
      galleryURLs: map['galleryURLs'] != null
          ? List<PhotoUrl?>.from(
              map['galleryURLs']?.map((x) => PhotoUrl?.fromMap(x)),
            )
          : null,
      location:
          map['location'] != null ? Location.fromMap(map['location']) : null,
      name: map['name'] != null ? map['name'] : null,
      rating: map['rating'] != null ? map['rating'] : null,
      ratingCount: map['ratingCount'] != null ? map['ratingCount'] : null,
      tags: map['tags'] != null
          ? List<TagMinute>.from(
              map['tags']?.map((x) => TagMinute?.fromMap(x)),
            )
          : null,
      type: map['type'] != null
          ? AllowedInstitutionTypes.fromMap(map['type'])
          : null,
      workers: map['workers'] != null
          ? List<Worker?>.from(map['workers']?.map((x) => Worker?.fromMap(x)))
          : null,
      contacts:
          map['contacts'] != null ? Contacts.fromMap(map['contacts']) : null,
      workTimes: map['workTimes'] != null
          ? List<WorkTimes>.from(
              map['workTimes'].map((x) => WorkTimes.fromMap(x)))
          : null,
      iconPath: map['iconPath'] != null ? map['iconPath'] : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory InstitutionsMinute.fromJson(String source) =>
      InstitutionsMinute.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object?> get props {
    return [
      id,
      avatarURL,
      averagePrice,
      city,
      dateAdded,
      description,
      galleryURLs,
      location,
      name,
      rating,
      ratingCount,
      tags,
      type,
      workers,
      contacts,
      workTimes,
      iconPath,
    ];
  }
}

part of '../bonus_bloc.dart';

extension EnterPromoCodeReaction on BonusBloc {
  void usePromocode(
    EnterPromoCode event,
    Emitter<BonusState> emit,
  ) async {
    try {
      final _mutationResult = await graphqlProvider.client.mutate(
        MutationOptions(
          document: gql('''
        mutation(\$code: String!){
          usePromocode(code:\$code)
        }
        '''),
          variables: {
            'code': event.promoCode,
          },
        ),
      );
      if (_mutationResult.hasException) throw _mutationResult.exception!;
      event.completer.complete(_mutationResult.data!['usePromocode']);
      getdebtBloc?.add(GetCurrentDebtOfUser());
    } catch (e) {
      event.completer.completeError(e);
      log(e.toString());
    }
  }
}

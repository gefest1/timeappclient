part of 'commentsofclub_bloc.dart';

abstract class CommentsofclubState extends Equatable {
  const CommentsofclubState();

  @override
  List<Object> get props => [];
}

class CommentsofclubInitial extends CommentsofclubState {}

class CommentsFetched extends CommentsofclubState {
  final Map<String, List<CommentModel>> comments;
  final Map<String, MetaRatingModel> metaRatingModel;

  const CommentsFetched(this.comments, this.metaRatingModel);

  @override
  List<Object> get props => [this.comments];
}

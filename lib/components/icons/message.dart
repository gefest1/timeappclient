// ignore_for_file: non_constant_identifier_namess

import 'package:flutter/material.dart';

class MessageCustomPainter extends CustomPainter {
  const MessageCustomPainter({Listenable? repaint}) : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.09782429, size.height * 0.4510857);
    path_0.cubicTo(size.width * 0.04388286, size.height * 0.4510857, 0,
        size.height * 0.4949679, 0, size.height * 0.5489107);
    path_0.cubicTo(
        0,
        size.height * 0.6028536,
        size.width * 0.04388286,
        size.height * 0.6467357,
        size.width * 0.09782429,
        size.height * 0.6467357);
    path_0.cubicTo(
        size.width * 0.1517675,
        size.height * 0.6467357,
        size.width * 0.1956486,
        size.height * 0.6028536,
        size.width * 0.1956486,
        size.height * 0.5489107);
    path_0.cubicTo(
        size.width * 0.1956486,
        size.height * 0.4949679,
        size.width * 0.1517675,
        size.height * 0.4510857,
        size.width * 0.09782429,
        size.height * 0.4510857);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_0, paint_0_fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.4565143, size.height * 0.01630511);
    path_1.cubicTo(
        size.width * 0.4025679,
        size.height * 0.01630511,
        size.width * 0.3586893,
        size.height * 0.06018786,
        size.width * 0.3586893,
        size.height * 0.1141293);
    path_1.cubicTo(
        size.width * 0.3586893,
        size.height * 0.1680729,
        size.width * 0.4025714,
        size.height * 0.2119536,
        size.width * 0.4565143,
        size.height * 0.2119536);
    path_1.cubicTo(
        size.width * 0.5104571,
        size.height * 0.2119536,
        size.width * 0.5543357,
        size.height * 0.1680707,
        size.width * 0.5543357,
        size.height * 0.1141293);
    path_1.cubicTo(
        size.width * 0.5543357,
        size.height * 0.06018786,
        size.width * 0.5104536,
        size.height * 0.01630511,
        size.width * 0.4565143,
        size.height * 0.01630511);
    path_1.close();

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_1, paint_1_fill);

    Path path_2 = Path();
    path_2.moveTo(size.width * 0.9347607, size.height * 0.7119536);
    path_2.lineTo(size.width * 0.3695607, size.height * 0.7119536);
    path_2.lineTo(size.width * 0.3695607, size.height * 0.6467357);
    path_2.lineTo(size.width * 0.9347714, size.height * 0.6467357);
    path_2.cubicTo(
        size.width * 0.9707429,
        size.height * 0.6467357,
        size.width * 0.9999964,
        size.height * 0.6174821,
        size.width * 0.9999964,
        size.height * 0.5815179);
    path_2.cubicTo(
        size.width * 0.9999964,
        size.height * 0.5455571,
        size.width * 0.9707429,
        size.height * 0.5163000,
        size.width * 0.9347714,
        size.height * 0.5163000);
    path_2.lineTo(size.width * 0.7924714, size.height * 0.5163000);
    path_2.cubicTo(
        size.width * 0.7822571,
        size.height * 0.3427707,
        size.width * 0.6535357,
        size.height * 0.2120186,
        size.width * 0.5979250,
        size.height * 0.2120186);
    path_2.cubicTo(
        size.width * 0.5491750,
        size.height * 0.2120186,
        size.width * 0.5367857,
        size.height * 0.2519893,
        size.width * 0.5160179,
        size.height * 0.2943350);
    path_2.cubicTo(
        size.width * 0.4996893,
        size.height * 0.3276321,
        size.width * 0.4948643,
        size.height * 0.3372686,
        size.width * 0.4840036,
        size.height * 0.3590107);
    path_2.lineTo(size.width * 0.3729964, size.height * 0.4144821);
    path_2.cubicTo(
        size.width * 0.3420664,
        size.height * 0.4299321,
        size.width * 0.3258361,
        size.height * 0.4704179,
        size.width * 0.3459218,
        size.height * 0.5053000);
    path_2.cubicTo(
        size.width * 0.3639679,
        size.height * 0.5365000,
        size.width * 0.4025750,
        size.height * 0.5455679,
        size.width * 0.4313357,
        size.height * 0.5311607);
    path_2.lineTo(size.width * 0.5617679, size.height * 0.4659429);
    path_2.cubicTo(
        size.width * 0.5723071,
        size.height * 0.4606786,
        size.width * 0.5813643,
        size.height * 0.4522107,
        size.width * 0.5879750,
        size.height * 0.4414643);
    path_2.lineTo(size.width * 0.6229536, size.height * 0.3845321);
    path_2.lineTo(size.width * 0.6463036, size.height * 0.4471357);
    path_2.cubicTo(
        size.width * 0.6547750,
        size.height * 0.4698321,
        size.width * 0.6600071,
        size.height * 0.4930179,
        size.width * 0.6620357,
        size.height * 0.5163000);
    path_2.lineTo(size.width * 0.5249893, size.height * 0.5163000);
    path_2.cubicTo(
        size.width * 0.5070286,
        size.height * 0.5668786,
        size.width * 0.4588286,
        size.height * 0.6032536,
        size.width * 0.4021643,
        size.height * 0.6032536);
    path_2.cubicTo(
        size.width * 0.3471914,
        size.height * 0.6032536,
        size.width * 0.3001564,
        size.height * 0.5690250,
        size.width * 0.2809904,
        size.height * 0.5208000);
    path_2.cubicTo(
        size.width * 0.2567207,
        size.height * 0.5301750,
        size.width * 0.2395839,
        size.height * 0.5535321,
        size.width * 0.2393789,
        size.height * 0.5810357);
    path_2.lineTo(size.width * 0.2393400, size.height * 0.5810214);
    path_2.cubicTo(
        size.width * 0.2393400,
        size.height * 0.5880000,
        size.width * 0.2393400,
        size.height * 0.6712429,
        size.width * 0.2393400,
        size.height * 0.7119536);
    path_2.lineTo(size.width * 0.06521679, size.height * 0.7119536);
    path_2.cubicTo(size.width * 0.02925389, size.height * 0.7119536, 0,
        size.height * 0.7412071, 0, size.height * 0.7771679);
    path_2.cubicTo(
        0,
        size.height * 0.8131321,
        size.width * 0.02947071,
        size.height * 0.8423857,
        size.width * 0.06543357,
        size.height * 0.8423857);
    path_2.lineTo(size.width * 0.06543357, size.height * 0.9510821);
    path_2.cubicTo(
        size.width * 0.06543357,
        size.height * 0.9690964,
        size.width * 0.07981250,
        size.height * 0.9836893,
        size.width * 0.09782429,
        size.height * 0.9836893);
    path_2.cubicTo(
        size.width * 0.1158379,
        size.height * 0.9836893,
        size.width * 0.1304318,
        size.height * 0.9690929,
        size.width * 0.1304318,
        size.height * 0.9510821);
    path_2.lineTo(size.width * 0.1304318, size.height * 0.8421679);
    path_2.lineTo(size.width * 0.1779629, size.height * 0.8421679);
    path_2.cubicTo(
        size.width * 0.1671093,
        size.height * 0.8727464,
        size.width * 0.1802168,
        size.height * 0.9075964,
        size.width * 0.2099550,
        size.height * 0.9224643);
    path_2.cubicTo(
        size.width * 0.2414804,
        size.height * 0.9382286,
        size.width * 0.2809746,
        size.height * 0.9262893,
        size.width * 0.2974629,
        size.height * 0.8932929);
    path_2.lineTo(size.width * 0.3229179, size.height * 0.8423857);
    path_2.lineTo(size.width * 0.6741214, size.height * 0.8423857);
    path_2.lineTo(size.width * 0.6741214, size.height * 0.8858643);
    path_2.cubicTo(
        size.width * 0.6741214,
        size.height * 0.9218286,
        size.width * 0.7031571,
        size.height * 0.9510821,
        size.width * 0.7391214,
        size.height * 0.9510821);
    path_2.cubicTo(
        size.width * 0.7750857,
        size.height * 0.9510821,
        size.width * 0.8043393,
        size.height * 0.9218286,
        size.width * 0.8043393,
        size.height * 0.8858643);
    path_2.lineTo(size.width * 0.8043393, size.height * 0.8423857);
    path_2.lineTo(size.width * 0.8697714, size.height * 0.8423857);
    path_2.lineTo(size.width * 0.8697714, size.height * 0.9510821);
    path_2.cubicTo(
        size.width * 0.8697714,
        size.height * 0.9690964,
        size.width * 0.8841500,
        size.height * 0.9836893,
        size.width * 0.9021607,
        size.height * 0.9836893);
    path_2.cubicTo(
        size.width * 0.9201750,
        size.height * 0.9836893,
        size.width * 0.9347679,
        size.height * 0.9690929,
        size.width * 0.9347679,
        size.height * 0.9510821);
    path_2.lineTo(size.width * 0.9347679, size.height * 0.8423857);
    path_2.cubicTo(
        size.width * 0.9707357,
        size.height * 0.8423786,
        size.width * 0.9999857,
        size.height * 0.8131286,
        size.width * 0.9999857,
        size.height * 0.7771679);
    path_2.cubicTo(
        size.width * 0.9999893,
        size.height * 0.7412071,
        size.width * 0.9707357,
        size.height * 0.7119536,
        size.width * 0.9347607,
        size.height * 0.7119536);
    path_2.close();

    Paint paint_2_fill = Paint()..style = PaintingStyle.fill;
    paint_2_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_2, paint_2_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}

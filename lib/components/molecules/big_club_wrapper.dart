import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:li/components/molecules/big_clubs_card.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/components/pages/qfit_to_timeapp/qfit_detail_page.dart';
import 'package:li/data/models/institution_minute.dart';
import 'package:li/logic/blocs/comments_of_club/commentsofclub_bloc.dart';
import 'package:li/logic/providers/location/location_provider.dart';
import 'package:li/main.dart';
import 'package:li/utils/getWeekTIme.dart';
import 'package:li/utils/launch_universal_link.dart';
import 'package:li/utils/locationsBetween.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class BugClubCardWrapper extends StatelessWidget {
  final InstitutionsMinute club;

  const BugClubCardWrapper({
    required this.club,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String? locationLabel;
    final locationProvider = Provider.of<LocationProvider>(context);
    final myLocation = locationProvider.location;
    if (myLocation != null) {
      locationLabel = locationBetweenText(
        myLocation.latitude,
        myLocation.longitude,
        club.location!.latitude!.toDouble(),
        club.location!.longitude!.toDouble(),
      );
    }
    final _workTime = getWorkTime(club.workTimes);
    final workStartTime = _workTime?.startTime;
    final workEndTime = _workTime?.endTime;

    final addValueDesc = club.tags?.fold<String?>(null, (combined, tag) {
      if (tag.name != null) {
        combined = (combined ?? '') + ", " + tag.name!;
      }
      return combined;
    });
    String? desc;

    if (addValueDesc != null && addValueDesc.isNotEmpty) {
      desc = addValueDesc.substring(2);
    }

    return SizeTapAnimation(
      child: BigClubsCard(
        onInstagramTap: club.contacts?.instagram == null
            ? null
            : () {
                HapticFeedback.mediumImpact();
                launchUniversalLinkIos(
                  'https://www.instagram.com/${club.contacts!.instagram!}/',
                );
              },
        onPhoneTap: club.contacts?.phoneNumber == null
            ? null
            : () {
                HapticFeedback.mediumImpact();
                final _txt =
                    "tel://${club.contacts!.phoneNumber}".replaceAll(" ", "");

                launch(_txt);
              },
        onTap: () {
          final commentsBloc =
              BlocProvider.of<CommentsofclubBloc>(context, listen: false);

          commentsBloc.add(GetCommentsOfClubEvent(institutionId: club.id!));
          routerDelegate.push(
            QfitDetailPage(
              institution: club,
            ),
          );
        },
        galleryURLs: club.galleryURLs?.map((e) => e!).toList(),
        endTime: workEndTime,
        startTime: workStartTime,
        title: club.name!,
        address: club.location!.address!,
        price: club.averagePrice!,
        desc: desc,
        rating: club.rating?.toStringAsFixed(1),
        ratingCount: club.ratingCount?.toStringAsFixed(0),
        locationLabel: locationLabel,
        iconPath: club.iconPath,
      ),
    );
  }
}

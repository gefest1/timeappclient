import 'package:flutter/material.dart';

Path getPath1(Size size) {
  return Path()
    ..moveTo(size.width * 0.07453500, size.height * 0.2312066)
    ..lineTo(size.width * 0.06255000, size.height * 0.2314450)
    ..lineTo(size.width * 0.06255500, size.height * 0.2426239)
    ..cubicTo(
        size.width * 0.06257000,
        size.height * 0.2651854,
        size.width * 0.06004000,
        size.height * 0.2779386,
        size.width * 0.05119250,
        size.height * 0.2999339)
    ..cubicTo(
        size.width * 0.04925750,
        size.height * 0.3047412,
        size.width * 0.04771250,
        size.height * 0.3087440,
        size.width * 0.04775750,
        size.height * 0.3088334)
    ..cubicTo(
        size.width * 0.04780250,
        size.height * 0.3089228,
        size.width * 0.05539500,
        size.height * 0.3084659,
        size.width * 0.06463000,
        size.height * 0.3078203)
    ..cubicTo(
        size.width * 0.08880250,
        size.height * 0.3061218,
        size.width * 0.08886000,
        size.height * 0.3061169,
        size.width * 0.08886000,
        size.height * 0.3066532)
    ..cubicTo(
        size.width * 0.08886000,
        size.height * 0.3069214,
        size.width * 0.08187000,
        size.height * 0.3283456,
        size.width * 0.07332500,
        size.height * 0.3542692)
    ..cubicTo(
        size.width * 0.06478250,
        size.height * 0.3801928,
        size.width * 0.05784500,
        size.height * 0.4015028,
        size.width * 0.05791250,
        size.height * 0.4016269)
    ..cubicTo(
        size.width * 0.05826750,
        size.height * 0.4022825,
        size.width * 0.1196450,
        size.height * 0.4619912,
        size.width * 0.1199650,
        size.height * 0.4619912)
    ..cubicTo(
        size.width * 0.1201750,
        size.height * 0.4619912,
        size.width * 0.1208600,
        size.height * 0.4596173,
        size.width * 0.1214850,
        size.height * 0.4567121)
    ..cubicTo(
        size.width * 0.1310850,
        size.height * 0.4121702,
        size.width * 0.1433425,
        size.height * 0.3760410,
        size.width * 0.1514675,
        size.height * 0.3683434)
    ..cubicTo(
        size.width * 0.1589375,
        size.height * 0.3612616,
        size.width * 0.1629800,
        size.height * 0.3412627,
        size.width * 0.1649550,
        size.height * 0.3016423)
    ..cubicTo(
        size.width * 0.1657250,
        size.height * 0.2862173,
        size.width * 0.1656300,
        size.height * 0.2844195,
        size.width * 0.1639525,
        size.height * 0.2826267)
    ..cubicTo(
        size.width * 0.1592175,
        size.height * 0.2775612,
        size.width * 0.1556575,
        size.height * 0.2692378,
        size.width * 0.1518075,
        size.height * 0.2542349)
    ..cubicTo(
        size.width * 0.1503750,
        size.height * 0.2486529,
        size.width * 0.1491150,
        size.height * 0.2440890,
        size.width * 0.1490075,
        size.height * 0.2440890)
    ..cubicTo(
        size.width * 0.1489000,
        size.height * 0.2440890,
        size.width * 0.1406200,
        size.height * 0.2410447,
        size.width * 0.1306050,
        size.height * 0.2373300)
    ..lineTo(size.width * 0.1123975, size.height * 0.2305759)
    ..lineTo(size.width * 0.09946000, size.height * 0.2307696)
    ..cubicTo(
        size.width * 0.09234500,
        size.height * 0.2308789,
        size.width * 0.08112750,
        size.height * 0.2310775,
        size.width * 0.07453500,
        size.height * 0.2312066)
    ..moveTo(size.width * 0.1675175, size.height * 0.2963732)
    ..cubicTo(
        size.width * 0.1675175,
        size.height * 0.3303321,
        size.width * 0.1610100,
        size.height * 0.3644946,
        size.width * 0.1531150,
        size.height * 0.3719936)
    ..cubicTo(
        size.width * 0.1516825,
        size.height * 0.3733543,
        size.width * 0.1505150,
        size.height * 0.3746803,
        size.width * 0.1505225,
        size.height * 0.3749435)
    ..cubicTo(
        size.width * 0.1505550,
        size.height * 0.3761751,
        size.width * 0.1635200,
        size.height * 0.4297307,
        size.width * 0.1638225,
        size.height * 0.4298797)
    ..cubicTo(
        size.width * 0.1644800,
        size.height * 0.4301975,
        size.width * 0.1943375,
        size.height * 0.3882281,
        size.width * 0.1942150,
        size.height * 0.3871604)
    ..cubicTo(
        size.width * 0.1939875,
        size.height * 0.3851987,
        size.width * 0.1681600,
        size.height * 0.2922711,
        size.width * 0.1678350,
        size.height * 0.2922512)
    ..cubicTo(
        size.width * 0.1676600,
        size.height * 0.2922363,
        size.width * 0.1675175,
        size.height * 0.2940937,
        size.width * 0.1675175,
        size.height * 0.2963732)
    ..moveTo(size.width * 0.4889450, size.height * 0.3125630)
    ..cubicTo(
        size.width * 0.4807650,
        size.height * 0.3137897,
        size.width * 0.4808675,
        size.height * 0.3137251,
        size.width * 0.4808600,
        size.height * 0.3178322)
    ..cubicTo(
        size.width * 0.4808575,
        size.height * 0.3197739,
        size.width * 0.4805850,
        size.height * 0.3236426,
        size.width * 0.4802550,
        size.height * 0.3264287)
    ..cubicTo(
        size.width * 0.4799250,
        size.height * 0.3292097,
        size.width * 0.4797225,
        size.height * 0.3316233,
        size.width * 0.4798075,
        size.height * 0.3317872)
    ..cubicTo(
        size.width * 0.4798900,
        size.height * 0.3319560,
        size.width * 0.4868850,
        size.height * 0.3281221,
        size.width * 0.4953500,
        size.height * 0.3232701)
    ..cubicTo(
        size.width * 0.5047325,
        size.height * 0.3179017,
        size.width * 0.5104575,
        size.height * 0.3142366,
        size.width * 0.5100100,
        size.height * 0.3138989)
    ..cubicTo(
        size.width * 0.5075750,
        size.height * 0.3120416,
        size.width * 0.4969250,
        size.height * 0.3113662,
        size.width * 0.4889450,
        size.height * 0.3125630)
    ..moveTo(size.width * 0.4961725, size.height * 0.3259668)
    ..lineTo(size.width * 0.4789550, size.height * 0.3359191)
    ..lineTo(size.width * 0.4778900, size.height * 0.3392017)
    ..cubicTo(
        size.width * 0.4724725,
        size.height * 0.3559329,
        size.width * 0.4649400,
        size.height * 0.3605812,
        size.width * 0.4581000,
        size.height * 0.3514087)
    ..lineTo(size.width * 0.4562650, size.height * 0.3489504)
    ..lineTo(size.width * 0.4360200, size.height * 0.3605564)
    ..lineTo(size.width * 0.4157775, size.height * 0.3721624)
    ..lineTo(size.width * 0.4138825, size.height * 0.3764632)
    ..cubicTo(
        size.width * 0.4114250,
        size.height * 0.3820452,
        size.width * 0.4113100,
        size.height * 0.3816131,
        size.width * 0.4163750,
        size.height * 0.3857748)
    ..lineTo(size.width * 0.4207550, size.height * 0.3893753)
    ..lineTo(size.width * 0.4683500, size.height * 0.3610083)
    ..lineTo(size.width * 0.5159450, size.height * 0.3326364)
    ..lineTo(size.width * 0.5189200, size.height * 0.3274815)
    ..arcToPoint(Offset(size.width * 0.5222525, size.height * 0.3217306),
        radius:
            Radius.elliptical(size.width * 1.118925, size.height * 2.222724),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..cubicTo(
        size.width * 0.5226275,
        size.height * 0.3211049,
        size.width * 0.5151275,
        size.height * 0.3158258,
        size.width * 0.5140300,
        size.height * 0.3159450)
    ..cubicTo(
        size.width * 0.5136800,
        size.height * 0.3159847,
        size.width * 0.5056450,
        size.height * 0.3204940,
        size.width * 0.4961725,
        size.height * 0.3259668)
    ..moveTo(size.width * 0.5208325, size.height * 0.3282364)
    ..cubicTo(
        size.width * 0.5190800,
        size.height * 0.3312955,
        size.width * 0.5172625,
        size.height * 0.3342902,
        size.width * 0.5167950,
        size.height * 0.3348960)
    ..cubicTo(
        size.width * 0.5163275,
        size.height * 0.3354969,
        size.width * 0.4944900,
        size.height * 0.3487666,
        size.width * 0.4682700,
        size.height * 0.3643754)
    ..lineTo(size.width * 0.4205950, size.height * 0.3927573)
    ..lineTo(size.width * 0.4155925, size.height * 0.3885062)
    ..cubicTo(
        size.width * 0.4128425,
        size.height * 0.3861622,
        size.width * 0.4105550,
        size.height * 0.3843545,
        size.width * 0.4105100,
        size.height * 0.3844786)
    ..lineTo(size.width * 0.4085900, size.height * 0.3896087)
    ..cubicTo(
        size.width * 0.4075100,
        size.height * 0.3924891,
        size.width * 0.4069150,
        size.height * 0.3948083,
        size.width * 0.4071400,
        size.height * 0.3952454)
    ..cubicTo(
        size.width * 0.4081650,
        size.height * 0.3972169,
        size.width * 0.4237425,
        size.height * 0.4080284,
        size.width * 0.4249575,
        size.height * 0.4076062)
    ..cubicTo(
        size.width * 0.4256600,
        size.height * 0.4073629,
        size.width * 0.4468025,
        size.height * 0.3948779,
        size.width * 0.4719425,
        size.height * 0.3798650)
    ..lineTo(size.width * 0.5176525, size.height * 0.3525658)
    ..lineTo(size.width * 0.5252300, size.height * 0.3403837)
    ..lineTo(size.width * 0.5328075, size.height * 0.3282016)
    ..lineTo(size.width * 0.5290525, size.height * 0.3254652)
    ..cubicTo(
        size.width * 0.5241800,
        size.height * 0.3219094,
        size.width * 0.5245275,
        size.height * 0.3217952,
        size.width * 0.5208325,
        size.height * 0.3282364)
    ..moveTo(size.width * 0.5265250, size.height * 0.3423056)
    ..lineTo(size.width * 0.5184775, size.height * 0.3552922)
    ..lineTo(size.width * 0.4712275, size.height * 0.3834457)
    ..lineTo(size.width * 0.4239775, size.height * 0.4115991)
    ..lineTo(size.width * 0.4168150, size.height * 0.4062703)
    ..arcToPoint(Offset(size.width * 0.4076375, size.height * 0.3993921),
        radius:
            Radius.elliptical(size.width * 1.241057, size.height * 2.465338),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..lineTo(size.width * 0.4056250, size.height * 0.3978377)
    ..lineTo(size.width * 0.4034925, size.height * 0.4041150)
    ..cubicTo(
        size.width * 0.4023200,
        size.height * 0.4075715,
        size.width * 0.4013625,
        size.height * 0.4107002,
        size.width * 0.4013650,
        size.height * 0.4110677)
    ..cubicTo(
        size.width * 0.4013700,
        size.height * 0.4114352,
        size.width * 0.4071975,
        size.height * 0.4152045,
        size.width * 0.4143150,
        size.height * 0.4194407)
    ..lineTo(size.width * 0.4272600, size.height * 0.4271383)
    ..lineTo(size.width * 0.4735150, size.height * 0.3995411)
    ..lineTo(size.width * 0.5197700, size.height * 0.3719390)
    ..lineTo(size.width * 0.5316750, size.height * 0.3547857)
    ..cubicTo(
        size.width * 0.5382225,
        size.height * 0.3453499,
        size.width * 0.5437350,
        size.height * 0.3373493,
        size.width * 0.5439250,
        size.height * 0.3370067)
    ..cubicTo(
        size.width * 0.5441125,
        size.height * 0.3366640,
        size.width * 0.5423925,
        size.height * 0.3350351,
        size.width * 0.5400975,
        size.height * 0.3333863)
    ..cubicTo(
        size.width * 0.5378025,
        size.height * 0.3317375,
        size.width * 0.5356225,
        size.height * 0.3301483,
        size.width * 0.5352500,
        size.height * 0.3298504)
    ..cubicTo(
        size.width * 0.5347850,
        size.height * 0.3294829,
        size.width * 0.5320750,
        size.height * 0.3333466,
        size.width * 0.5265250,
        size.height * 0.3423056)
    ..moveTo(size.width * 0.4377125, size.height * 0.3444560)
    ..cubicTo(
        size.width * 0.4293075,
        size.height * 0.3531220,
        size.width * 0.4192925,
        size.height * 0.3652942,
        size.width * 0.4197725,
        size.height * 0.3662477)
    ..cubicTo(
        size.width * 0.4198500,
        size.height * 0.3664016,
        size.width * 0.4273325,
        size.height * 0.3622797,
        size.width * 0.4363975,
        size.height * 0.3570900)
    ..lineTo(size.width * 0.4528800, size.height * 0.3476492)
    ..lineTo(size.width * 0.4503975, size.height * 0.3451661)
    ..cubicTo(
        size.width * 0.4490325,
        size.height * 0.3438004,
        size.width * 0.4472200,
        size.height * 0.3415954,
        size.width * 0.4463675,
        size.height * 0.3402595)
    ..cubicTo(
        size.width * 0.4443925,
        size.height * 0.3371706,
        size.width * 0.4451075,
        size.height * 0.3368229,
        size.width * 0.4377125,
        size.height * 0.3444560)
    ..moveTo(size.width * 0.5331775, size.height * 0.3564394)
    ..cubicTo(
        size.width * 0.5235925,
        size.height * 0.3702405,
        size.width * 0.5197000,
        size.height * 0.3753656,
        size.width * 0.5181000,
        size.height * 0.3762943)
    ..cubicTo(
        size.width * 0.5169150,
        size.height * 0.3769896,
        size.width * 0.4959800,
        size.height * 0.3894548,
        size.width * 0.4715800,
        size.height * 0.4040008)
    ..lineTo(size.width * 0.4272150, size.height * 0.4304508)
    ..lineTo(size.width * 0.4140275, size.height * 0.4225198)
    ..cubicTo(
        size.width * 0.4067750,
        size.height * 0.4181545,
        size.width * 0.4005925,
        size.height * 0.4147725,
        size.width * 0.4002925,
        size.height * 0.4150059)
    ..cubicTo(
        size.width * 0.3993350,
        size.height * 0.4157359,
        size.width * 0.3962150,
        size.height * 0.4268354,
        size.width * 0.3968025,
        size.height * 0.4274264)
    ..cubicTo(
        size.width * 0.3970875,
        size.height * 0.4277194,
        size.width * 0.4048200,
        size.height * 0.4320648,
        size.width * 0.4139850,
        size.height * 0.4370906)
    ..lineTo(size.width * 0.4306500, size.height * 0.4462234)
    ..lineTo(size.width * 0.4759425, size.height * 0.4192669)
    ..lineTo(size.width * 0.5212350, size.height * 0.3923153)
    ..lineTo(size.width * 0.5372975, size.height * 0.3692522)
    ..cubicTo(
        size.width * 0.5461300,
        size.height * 0.3565636,
        size.width * 0.5536450,
        size.height * 0.3458564,
        size.width * 0.5539925,
        size.height * 0.3454542)
    ..cubicTo(
        size.width * 0.5543425,
        size.height * 0.3450519,
        size.width * 0.5545050,
        size.height * 0.3444758,
        size.width * 0.5543550,
        size.height * 0.3441779)
    ..cubicTo(
        size.width * 0.5541475,
        size.height * 0.3437706,
        size.width * 0.5464875,
        size.height * 0.3378360,
        size.width * 0.5461650,
        size.height * 0.3378360)
    ..cubicTo(
        size.width * 0.5461275,
        size.height * 0.3378360,
        size.width * 0.5402850,
        size.height * 0.3462090,
        size.width * 0.5331775,
        size.height * 0.3564394)
    ..moveTo(size.width * 0.5394300, size.height * 0.3701412)
    ..cubicTo(
        size.width * 0.5300200,
        size.height * 0.3836443,
        size.width * 0.5219875,
        size.height * 0.3948679,
        size.width * 0.5215775,
        size.height * 0.3950815)
    ..cubicTo(
        size.width * 0.5202775,
        size.height * 0.3957569,
        size.width * 0.5207375,
        size.height * 0.3965117,
        size.width * 0.5249900,
        size.height * 0.4006685)
    ..lineTo(size.width * 0.5291475, size.height * 0.4047308)
    ..lineTo(size.width * 0.5431675, size.height * 0.3847319)
    ..cubicTo(
        size.width * 0.5586375,
        size.height * 0.3626621,
        size.width * 0.5576400,
        size.height * 0.3647578,
        size.width * 0.5580925,
        size.height * 0.3533802)
    ..cubicTo(
        size.width * 0.5585275,
        size.height * 0.3424149,
        size.width * 0.5591325,
        size.height * 0.3418686,
        size.width * 0.5394300,
        size.height * 0.3701412)
    ..moveTo(size.width * 0.5661225, size.height * 0.3738807)
    ..cubicTo(
        size.width * 0.5665300,
        size.height * 0.3833513,
        size.width * 0.5652225,
        size.height * 0.3990693,
        size.width * 0.5631375,
        size.height * 0.4097566)
    ..cubicTo(
        size.width * 0.5629000,
        size.height * 0.4109733,
        size.width * 0.5633950,
        size.height * 0.4112216,
        size.width * 0.5670400,
        size.height * 0.4116984)
    ..cubicTo(
        size.width * 0.5748075,
        size.height * 0.4127214,
        size.width * 0.5767425,
        size.height * 0.4133223,
        size.width * 0.5767425,
        size.height * 0.4146930)
    ..cubicTo(
        size.width * 0.5767425,
        size.height * 0.4154479,
        size.width * 0.5762700,
        size.height * 0.4160687,
        size.width * 0.5755750,
        size.height * 0.4162276)
    ..cubicTo(
        size.width * 0.5740100,
        size.height * 0.4165851,
        size.width * 0.5735600,
        size.height * 0.4150059,
        size.width * 0.5812400,
        size.height * 0.4360676)
    ..cubicTo(
        size.width * 0.5895275,
        size.height * 0.4587879,
        size.width * 0.5893900,
        size.height * 0.4585893,
        size.width * 0.5924525,
        size.height * 0.4523617)
    ..cubicTo(
        size.width * 0.5992700,
        size.height * 0.4384911,
        size.width * 0.5973375,
        size.height * 0.4131485,
        size.width * 0.5882600,
        size.height * 0.3974553)
    ..cubicTo(
        size.width * 0.5835125,
        size.height * 0.3892412,
        size.width * 0.5724875,
        size.height * 0.3742532,
        size.width * 0.5668250,
        size.height * 0.3683087)
    ..lineTo(size.width * 0.5658375, size.height * 0.3672707)
    ..lineTo(size.width * 0.5661225, size.height * 0.3738807)
    ..moveTo(size.width * 0.5439075, size.height * 0.3874881)
    ..cubicTo(
        size.width * 0.5368400,
        size.height * 0.3975745,
        size.width * 0.5310525,
        size.height * 0.4060270,
        size.width * 0.5310475,
        size.height * 0.4062753)
    ..cubicTo(
        size.width * 0.5310250,
        size.height * 0.4073033,
        size.width * 0.5375325,
        size.height * 0.4130045,
        size.width * 0.5387275,
        size.height * 0.4130045)
    ..cubicTo(
        size.width * 0.5396825,
        size.height * 0.4130045,
        size.width * 0.5416350,
        size.height * 0.4107697,
        size.width * 0.5461250,
        size.height * 0.4045371)
    ..lineTo(size.width * 0.5522250, size.height * 0.3960697)
    ..lineTo(size.width * 0.5534850, size.height * 0.3899712)
    ..cubicTo(
        size.width * 0.5549400,
        size.height * 0.3829242,
        size.width * 0.5571775,
        size.height * 0.3689741,
        size.width * 0.5568325,
        size.height * 0.3691182)
    ..cubicTo(
        size.width * 0.5567925,
        size.height * 0.3691330,
        size.width * 0.5509775,
        size.height * 0.3774018,
        size.width * 0.5439075,
        size.height * 0.3874881)
    ..moveTo(size.width * 0.4751675, size.height * 0.4228475)
    ..lineTo(size.width * 0.4305675, size.height * 0.4494862)
    ..lineTo(size.width * 0.4133050, size.height * 0.4400008)
    ..cubicTo(
        size.width * 0.4038125,
        size.height * 0.4347863,
        size.width * 0.3958750,
        size.height * 0.4304706,
        size.width * 0.3956650,
        size.height * 0.4304160)
    ..cubicTo(
        size.width * 0.3950625,
        size.height * 0.4302621,
        size.width * 0.3909800,
        size.height * 0.4442568,
        size.width * 0.3914100,
        size.height * 0.4450018)
    ..cubicTo(
        size.width * 0.3916225,
        size.height * 0.4453693,
        size.width * 0.3998300,
        size.height * 0.4495558,
        size.width * 0.4096525,
        size.height * 0.4543134)
    ..cubicTo(
        size.width * 0.4194725,
        size.height * 0.4590661,
        size.width * 0.4278125,
        size.height * 0.4631284,
        size.width * 0.4281850,
        size.height * 0.4633320)
    ..cubicTo(
        size.width * 0.4286450,
        size.height * 0.4635903,
        size.width * 0.4290925,
        size.height * 0.4626914,
        size.width * 0.4295825,
        size.height * 0.4605261)
    ..cubicTo(
        size.width * 0.4299800,
        size.height * 0.4587780,
        size.width * 0.4308450,
        size.height * 0.4555103,
        size.width * 0.4315050,
        size.height * 0.4532655)
    ..lineTo(size.width * 0.4327050, size.height * 0.4491883)
    ..lineTo(size.width * 0.4341100, size.height * 0.4572931)
    ..cubicTo(
        size.width * 0.4352075,
        size.height * 0.4636101,
        size.width * 0.4356650,
        size.height * 0.4652986,
        size.width * 0.4361900,
        size.height * 0.4649709)
    ..cubicTo(
        size.width * 0.4365600,
        size.height * 0.4647375,
        size.width * 0.4502325,
        size.height * 0.4566028,
        size.width * 0.4665750,
        size.height * 0.4468989)
    ..cubicTo(
        size.width * 0.4948150,
        size.height * 0.4301230,
        size.width * 0.4963400,
        size.height * 0.4291149,
        size.width * 0.4973300,
        size.height * 0.4265970)
    ..cubicTo(
        size.width * 0.4979050,
        size.height * 0.4251419,
        size.width * 0.4989175,
        size.height * 0.4233938,
        size.width * 0.4995825,
        size.height * 0.4227085)
    ..cubicTo(
        size.width * 0.5002475,
        size.height * 0.4220281,
        size.width * 0.5012125,
        size.height * 0.4199771,
        size.width * 0.5017300,
        size.height * 0.4181545)
    ..cubicTo(
        size.width * 0.5038000,
        size.height * 0.4108343,
        size.width * 0.5101150,
        size.height * 0.4078694,
        size.width * 0.5181425,
        size.height * 0.4104419)
    ..cubicTo(
        size.width * 0.5220325,
        size.height * 0.4116885,
        size.width * 0.5218000,
        size.height * 0.4122298,
        size.width * 0.5210400,
        size.height * 0.4036581)
    ..arcToPoint(Offset(size.width * 0.5204075, size.height * 0.3963230),
        radius:
            Radius.elliptical(size.width * 0.2314725, size.height * 0.4598160),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..cubicTo(
        size.width * 0.5204075,
        size.height * 0.3958612,
        size.width * 0.5181925,
        size.height * 0.3971573,
        size.width * 0.4751675,
        size.height * 0.4228475)
    ..moveTo(size.width * 0.3520400, size.height * 0.4062803)
    ..cubicTo(
        size.width * 0.3502875,
        size.height * 0.4086839,
        size.width * 0.3488100,
        size.height * 0.4107250,
        size.width * 0.3487550,
        size.height * 0.4108194)
    ..cubicTo(
        size.width * 0.3487025,
        size.height * 0.4109137,
        size.width * 0.3494200,
        size.height * 0.4130045,
        size.width * 0.3503500,
        size.height * 0.4154628)
    ..cubicTo(
        size.width * 0.3540750,
        size.height * 0.4253108,
        size.width * 0.3512225,
        size.height * 0.4304160,
        size.width * 0.3453775,
        size.height * 0.4243622)
    ..lineTo(size.width * 0.3424775, size.height * 0.4213577)
    ..lineTo(size.width * 0.3408900, size.height * 0.4249929)
    ..cubicTo(
        size.width * 0.3390450,
        size.height * 0.4292291,
        size.width * 0.3364025,
        size.height * 0.4389778,
        size.width * 0.3350000,
        size.height * 0.4467250)
    ..lineTo(size.width * 0.3340150, size.height * 0.4521481)
    ..lineTo(size.width * 0.3375075, size.height * 0.4585496)
    ..cubicTo(
        size.width * 0.3413625,
        size.height * 0.4656115,
        size.width * 0.3417075,
        size.height * 0.4673944,
        size.width * 0.3399125,
        size.height * 0.4709601)
    ..cubicTo(
        size.width * 0.3391125,
        size.height * 0.4725493,
        size.width * 0.3385400,
        size.height * 0.4729814,
        size.width * 0.3376800,
        size.height * 0.4726586)
    ..arcToPoint(Offset(size.width * 0.3343500, size.height * 0.4716852),
        radius:
            Radius.elliptical(size.width * 0.06591000, size.height * 0.1309290),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..lineTo(size.width * 0.3321750, size.height * 0.4711588)
    ..lineTo(size.width * 0.3319225, size.height * 0.4752311)
    ..arcToPoint(Offset(size.width * 0.3313900, size.height * 0.4852976),
        radius:
            Radius.elliptical(size.width * 0.2002575, size.height * 0.3978079),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..lineTo(size.width * 0.3311100, size.height * 0.4912918)
    ..lineTo(size.width * 0.3328850, size.height * 0.4927022)
    ..cubicTo(
        size.width * 0.3378525,
        size.height * 0.4966503,
        size.width * 0.3379250,
        size.height * 0.5048396,
        size.width * 0.3330100,
        size.height * 0.5063990)
    ..cubicTo(
        size.width * 0.3313950,
        size.height * 0.5069105,
        size.width * 0.3312075,
        size.height * 0.5072134,
        size.width * 0.3312075,
        size.height * 0.5092992)
    ..cubicTo(
        size.width * 0.3312075,
        size.height * 0.5109977,
        size.width * 0.3309500,
        size.height * 0.5117625,
        size.width * 0.3302500,
        size.height * 0.5121349)
    ..cubicTo(
        size.width * 0.3282675,
        size.height * 0.5131927,
        size.width * 0.3279800,
        size.height * 0.5127805,
        size.width * 0.3272000,
        size.height * 0.5078094)
    ..cubicTo(
        size.width * 0.3267825,
        size.height * 0.5051425,
        size.width * 0.3257750,
        size.height * 0.5004147,
        size.width * 0.3249575,
        size.height * 0.4973058)
    ..lineTo(size.width * 0.3234750, size.height * 0.4916543)
    ..lineTo(size.width * 0.3220725, size.height * 0.4929852)
    ..cubicTo(
        size.width * 0.3189925,
        size.height * 0.4959004,
        size.width * 0.3171100,
        size.height * 0.4921708,
        size.width * 0.3178825,
        size.height * 0.4846817)
    ..lineTo(size.width * 0.3183175, size.height * 0.4804406)
    ..lineTo(size.width * 0.3160550, size.height * 0.4765570)
    ..cubicTo(
        size.width * 0.3148100,
        size.height * 0.4744216,
        size.width * 0.3128875,
        size.height * 0.4716306,
        size.width * 0.3117875,
        size.height * 0.4703493)
    ..lineTo(size.width * 0.3097825, size.height * 0.4680251)
    ..lineTo(size.width * 0.3080225, size.height * 0.4700761)
    ..cubicTo(
        size.width * 0.3048275,
        size.height * 0.4737958,
        size.width * 0.3025275,
        size.height * 0.4716653,
        size.width * 0.3027950,
        size.height * 0.4652341)
    ..lineTo(size.width * 0.3029325, size.height * 0.4618521)
    ..lineTo(size.width * 0.3004175, size.height * 0.4598110)
    ..cubicTo(
        size.width * 0.2990325,
        size.height * 0.4586886,
        size.width * 0.2978375,
        size.height * 0.4577699,
        size.width * 0.2977600,
        size.height * 0.4577699)
    ..cubicTo(
        size.width * 0.2976825,
        size.height * 0.4577699,
        size.width * 0.2976200,
        size.height * 0.4655619,
        size.width * 0.2976200,
        size.height * 0.4750821)
    ..lineTo(size.width * 0.2976200, size.height * 0.4923992)
    ..lineTo(size.width * 0.2999850, size.height * 0.4923992)
    ..cubicTo(
        size.width * 0.3066400,
        size.height * 0.4923992,
        size.width * 0.3060825,
        size.height * 0.5016860,
        size.width * 0.2989925,
        size.height * 0.5089218)
    ..cubicTo(
        size.width * 0.2987850,
        size.height * 0.5091353,
        size.width * 0.3019325,
        size.height * 0.5264823,
        size.width * 0.3027675,
        size.height * 0.5297401)
    ..lineTo(size.width * 0.3035875, size.height * 0.5329384)
    ..lineTo(size.width * 0.3067450, size.height * 0.5329384)
    ..cubicTo(
        size.width * 0.3116175,
        size.height * 0.5329384,
        size.width * 0.3126850,
        size.height * 0.5374328,
        size.width * 0.3094600,
        size.height * 0.5443805)
    ..lineTo(size.width * 0.3076950, size.height * 0.5481796)
    ..lineTo(size.width * 0.3093325, size.height * 0.5529025)
    ..cubicTo(
        size.width * 0.3102350,
        size.height * 0.5555048,
        size.width * 0.3118100,
        size.height * 0.5594827,
        size.width * 0.3128375,
        size.height * 0.5617523)
    ..lineTo(size.width * 0.3147050, size.height * 0.5658792)
    ..lineTo(size.width * 0.3210425, size.height * 0.5658792)
    ..cubicTo(
        size.width * 0.3245300,
        size.height * 0.5658792,
        size.width * 0.3273800,
        size.height * 0.5656656,
        size.width * 0.3273800,
        size.height * 0.5654074)
    ..cubicTo(
        size.width * 0.3273800,
        size.height * 0.5612060,
        size.width * 0.3182175,
        size.height * 0.5230606,
        size.width * 0.3140525,
        size.height * 0.5099200)
    ..cubicTo(
        size.width * 0.3114200,
        size.height * 0.5016115,
        size.width * 0.3109775,
        size.height * 0.4991533,
        size.width * 0.3121150,
        size.height * 0.4991533)
    ..cubicTo(
        size.width * 0.3135900,
        size.height * 0.4991533,
        size.width * 0.3227900,
        size.height * 0.5333009,
        size.width * 0.3269700,
        size.height * 0.5542881)
    ..lineTo(size.width * 0.3292875, size.height * 0.5659289)
    ..lineTo(size.width * 0.3341150, size.height * 0.5656905)
    ..lineTo(size.width * 0.3389400, size.height * 0.5654571)
    ..lineTo(size.width * 0.3391100, size.height * 0.5527883)
    ..cubicTo(
        size.width * 0.3394750,
        size.height * 0.5254146,
        size.width * 0.3437525,
        size.height * 0.4747394,
        size.width * 0.3460900,
        size.height * 0.4700960)
    ..cubicTo(
        size.width * 0.3474650,
        size.height * 0.4673646,
        size.width * 0.3474725,
        size.height * 0.4715114,
        size.width * 0.3461100,
        size.height * 0.4803413)
    ..cubicTo(
        size.width * 0.3438450,
        size.height * 0.4950164,
        size.width * 0.3426950,
        size.height * 0.5050879,
        size.width * 0.3420300,
        size.height * 0.5160483)
    ..cubicTo(
        size.width * 0.3419325,
        size.height * 0.5176722,
        size.width * 0.3416475,
        size.height * 0.5220425,
        size.width * 0.3414000,
        size.height * 0.5257572)
    ..cubicTo(
        size.width * 0.3411500,
        size.height * 0.5294769,
        size.width * 0.3408300,
        size.height * 0.5398314,
        size.width * 0.3406875,
        size.height * 0.5487756)
    ..lineTo(size.width * 0.3404275, size.height * 0.5650349)
    ..lineTo(size.width * 0.3511425, size.height * 0.5650349)
    ..lineTo(size.width * 0.3540650, size.height * 0.5600240)
    ..cubicTo(
        size.width * 0.3588550,
        size.height * 0.5518099,
        size.width * 0.3648575,
        size.height * 0.5371845,
        size.width * 0.3686850,
        size.height * 0.5243816)
    ..cubicTo(
        size.width * 0.3696400,
        size.height * 0.5211883,
        size.width * 0.3705950,
        size.height * 0.5185811,
        size.width * 0.3708100,
        size.height * 0.5185811)
    ..cubicTo(
        size.width * 0.3733775,
        size.height * 0.5185811,
        size.width * 0.3663100,
        size.height * 0.5398563,
        size.width * 0.3582550,
        size.height * 0.5563739)
    ..lineTo(size.width * 0.3540350, size.height * 0.5650349)
    ..lineTo(size.width * 0.3585725, size.height * 0.5650349)
    ..cubicTo(
        size.width * 0.3657275,
        size.height * 0.5650349,
        size.width * 0.3673300,
        size.height * 0.5626015,
        size.width * 0.3652125,
        size.height * 0.5549734)
    ..cubicTo(
        size.width * 0.3622525,
        size.height * 0.5443060,
        size.width * 0.3654825,
        size.height * 0.5389822,
        size.width * 0.3701300,
        size.height * 0.5468686)
    ..lineTo(size.width * 0.3723650, size.height * 0.5506627)
    ..lineTo(size.width * 0.3737375, size.height * 0.5475589)
    ..cubicTo(
        size.width * 0.3765575,
        size.height * 0.5411773,
        size.width * 0.3780325,
        size.height * 0.5355903,
        size.width * 0.3784475,
        size.height * 0.5296954)
    ..cubicTo(
        size.width * 0.3786675,
        size.height * 0.5266015,
        size.width * 0.3789375,
        size.height * 0.5234777,
        size.width * 0.3790500,
        size.height * 0.5227527)
    ..cubicTo(
        size.width * 0.3791750,
        size.height * 0.5219581,
        size.width * 0.3787500,
        size.height * 0.5206967,
        size.width * 0.3779800,
        size.height * 0.5195644)
    ..cubicTo(
        size.width * 0.3756050,
        size.height * 0.5160731,
        size.width * 0.3764625,
        size.height * 0.5102031,
        size.width * 0.3795375,
        size.height * 0.5089069)
    ..cubicTo(
        size.width * 0.3809650,
        size.height * 0.5083060,
        size.width * 0.3818025,
        size.height * 0.5053759,
        size.width * 0.3818025,
        size.height * 0.5009858)
    ..cubicTo(
        size.width * 0.3818025,
        size.height * 0.4979266,
        size.width * 0.3800775,
        size.height * 0.4856402,
        size.width * 0.3796475,
        size.height * 0.4856402)
    ..cubicTo(
        size.width * 0.3791150,
        size.height * 0.4856402,
        size.width * 0.3713550,
        size.height * 0.4974896,
        size.width * 0.3684100,
        size.height * 0.5028034)
    ..lineTo(size.width * 0.3658600, size.height * 0.5074021)
    ..lineTo(size.width * 0.3670200, size.height * 0.5121349)
    ..cubicTo(
        size.width * 0.3692350,
        size.height * 0.5211585,
        size.width * 0.3665475,
        size.height * 0.5257523,
        size.width * 0.3622550,
        size.height * 0.5202795)
    ..lineTo(size.width * 0.3602775, size.height * 0.5177616)
    ..lineTo(size.width * 0.3567975, size.height * 0.5206768)
    ..cubicTo(
        size.width * 0.3548825,
        size.height * 0.5222809,
        size.width * 0.3533175,
        size.height * 0.5233983,
        size.width * 0.3533175,
        size.height * 0.5231649)
    ..cubicTo(
        size.width * 0.3533175,
        size.height * 0.5229265,
        size.width * 0.3537175,
        size.height * 0.5211734,
        size.width * 0.3542075,
        size.height * 0.5192664)
    ..cubicTo(
        size.width * 0.3558400,
        size.height * 0.5129096,
        size.width * 0.3580850,
        size.height * 0.5004792,
        size.width * 0.3576525,
        size.height * 0.5001962)
    ..cubicTo(
        size.width * 0.3563975,
        size.height * 0.4993668,
        size.width * 0.3526050,
        size.height * 0.4932981,
        size.width * 0.3521400,
        size.height * 0.4913812)
    ..cubicTo(
        size.width * 0.3508600,
        size.height * 0.4860872,
        size.width * 0.3525225,
        size.height * 0.4837829,
        size.width * 0.3573550,
        size.height * 0.4841504)
    ..lineTo(size.width * 0.3603325, size.height * 0.4843738)
    ..lineTo(size.width * 0.3612425, size.height * 0.4753453)
    ..cubicTo(
        size.width * 0.3617450,
        size.height * 0.4703840,
        size.width * 0.3622825,
        size.height * 0.4641117,
        size.width * 0.3624400,
        size.height * 0.4614101)
    ..lineTo(size.width * 0.3627250, size.height * 0.4565035)
    ..lineTo(size.width * 0.3599300, size.height * 0.4545220)
    ..cubicTo(
        size.width * 0.3547675,
        size.height * 0.4508669,
        size.width * 0.3544600,
        size.height * 0.4432636,
        size.width * 0.3593950,
        size.height * 0.4412970)
    ..cubicTo(
        size.width * 0.3623550,
        size.height * 0.4401150,
        size.width * 0.3622225,
        size.height * 0.4404825,
        size.width * 0.3615925,
        size.height * 0.4351438)
    ..cubicTo(
        size.width * 0.3608100,
        size.height * 0.4285040,
        size.width * 0.3586625,
        size.height * 0.4166348,
        size.width * 0.3567925,
        size.height * 0.4086144)
    ..lineTo(size.width * 0.3552300, size.height * 0.4019199)
    ..lineTo(size.width * 0.3520400, size.height * 0.4062803)
    ..moveTo(size.width * 0.6425125, size.height * 0.4120311)
    ..cubicTo(
        size.width * 0.6405900,
        size.height * 0.4210746,
        size.width * 0.6387475,
        size.height * 0.4315880,
        size.width * 0.6380225,
        size.height * 0.4376667)
    ..cubicTo(
        size.width * 0.6377575,
        size.height * 0.4398816,
        size.width * 0.6392775,
        size.height * 0.4416943,
        size.width * 0.6414175,
        size.height * 0.4417191)
    ..cubicTo(
        size.width * 0.6423475,
        size.height * 0.4417241,
        size.width * 0.6441325,
        size.height * 0.4452550,
        size.width * 0.6441325,
        size.height * 0.4470876)
    ..cubicTo(
        size.width * 0.6441325,
        size.height * 0.4503206,
        size.width * 0.6429525,
        size.height * 0.4524809,
        size.width * 0.6400700,
        size.height * 0.4545220)
    ..lineTo(size.width * 0.6372750, size.height * 0.4565035)
    ..lineTo(size.width * 0.6375600, size.height * 0.4614101)
    ..cubicTo(
        size.width * 0.6377175,
        size.height * 0.4641117,
        size.width * 0.6382550,
        size.height * 0.4703840,
        size.width * 0.6387575,
        size.height * 0.4753453)
    ..lineTo(size.width * 0.6396675, size.height * 0.4843738)
    ..lineTo(size.width * 0.6426450, size.height * 0.4841504)
    ..cubicTo(
        size.width * 0.6497325,
        size.height * 0.4836140,
        size.width * 0.6503825,
        size.height * 0.4908249,
        size.width * 0.6439200,
        size.height * 0.4983189)
    ..cubicTo(
        size.width * 0.6429850,
        size.height * 0.4994065,
        size.width * 0.6421700,
        size.height * 0.5003551,
        size.width * 0.6421125,
        size.height * 0.5004296)
    ..cubicTo(
        size.width * 0.6418900,
        size.height * 0.5007127,
        size.width * 0.6436575,
        size.height * 0.5100342,
        size.width * 0.6451475,
        size.height * 0.5164357)
    ..cubicTo(
        size.width * 0.6470175,
        size.height * 0.5244809,
        size.width * 0.6471750,
        size.height * 0.5241581,
        size.width * 0.6430975,
        size.height * 0.5206371)
    ..lineTo(size.width * 0.6397425, size.height * 0.5177368)
    ..lineTo(size.width * 0.6377550, size.height * 0.5202696)
    ..cubicTo(
        size.width * 0.6334375,
        size.height * 0.5257721,
        size.width * 0.6307375,
        size.height * 0.5210642,
        size.width * 0.6329100,
        size.height * 0.5118221)
    ..lineTo(size.width * 0.6340025, size.height * 0.5071786)
    ..lineTo(size.width * 0.6315200, size.height * 0.5026743)
    ..cubicTo(
        size.width * 0.6284775,
        size.height * 0.4971469,
        size.width * 0.6209650,
        size.height * 0.4856402,
        size.width * 0.6204025,
        size.height * 0.4856402)
    ..cubicTo(
        size.width * 0.6198925,
        size.height * 0.4856402,
        size.width * 0.6183175,
        size.height * 0.4970724,
        size.width * 0.6179450,
        size.height * 0.5034788)
    ..lineTo(size.width * 0.6176675, size.height * 0.5082216)
    ..lineTo(size.width * 0.6197400, size.height * 0.5088324)
    ..cubicTo(
        size.width * 0.6244175,
        size.height * 0.5102031,
        size.width * 0.6246325,
        size.height * 0.5193558,
        size.width * 0.6200425,
        size.height * 0.5216452)
    ..cubicTo(
        size.width * 0.6194950,
        size.height * 0.5219183,
        size.width * 0.6190475,
        size.height * 0.5225739,
        size.width * 0.6190500,
        size.height * 0.5231053)
    ..cubicTo(
        size.width * 0.6190550,
        size.height * 0.5259161,
        size.width * 0.6242950,
        size.height * 0.5433922,
        size.width * 0.6266100,
        size.height * 0.5483286)
    ..lineTo(size.width * 0.6276875, size.height * 0.5506280)
    ..lineTo(size.width * 0.6292825, size.height * 0.5477625)
    ..cubicTo(
        size.width * 0.6314900,
        size.height * 0.5437994,
        size.width * 0.6325525,
        size.height * 0.5428211,
        size.width * 0.6340125,
        size.height * 0.5434021)
    ..lineTo(size.width * 0.6352700, size.height * 0.5438988)
    ..lineTo(size.width * 0.6340175, size.height * 0.5407403)
    ..cubicTo(
        size.width * 0.6315700,
        size.height * 0.5345673,
        size.width * 0.6275175,
        size.height * 0.5216502,
        size.width * 0.6276400,
        size.height * 0.5204037)
    ..cubicTo(
        size.width * 0.6279050,
        size.height * 0.5176871,
        size.width * 0.6286900,
        size.height * 0.5188393,
        size.width * 0.6304700,
        size.height * 0.5245703)
    ..cubicTo(
        size.width * 0.6339175,
        size.height * 0.5356747,
        size.width * 0.6387100,
        size.height * 0.5476781,
        size.width * 0.6431675,
        size.height * 0.5563739)
    ..lineTo(size.width * 0.6476025, size.height * 0.5650349)
    ..lineTo(size.width * 0.6586925, size.height * 0.5650349)
    ..lineTo(size.width * 0.6584025, size.height * 0.5517305)
    ..cubicTo(
        size.width * 0.6578650,
        size.height * 0.5271279,
        size.width * 0.6563675,
        size.height * 0.5064933,
        size.width * 0.6536050,
        size.height * 0.4856402)
    ..cubicTo(
        size.width * 0.6519475,
        size.height * 0.4731154,
        size.width * 0.6517525,
        size.height * 0.4703493,
        size.width * 0.6524925,
        size.height * 0.4698427)
    ..cubicTo(
        size.width * 0.6534075,
        size.height * 0.4692120,
        size.width * 0.6563925,
        size.height * 0.4898267,
        size.width * 0.6579600,
        size.height * 0.5076008)
    ..cubicTo(
        size.width * 0.6596175,
        size.height * 0.5264128,
        size.width * 0.6600775,
        size.height * 0.5363402,
        size.width * 0.6600775,
        size.height * 0.5534587)
    ..cubicTo(
        size.width * 0.6600775,
        size.height * 0.5670959,
        size.width * 0.6596050,
        size.height * 0.5660828,
        size.width * 0.6658075,
        size.height * 0.5657004)
    ..lineTo(size.width * 0.6698375, size.height * 0.5654571)
    ..lineTo(size.width * 0.6726400, size.height * 0.5519391)
    ..cubicTo(
        size.width * 0.6772525,
        size.height * 0.5297203,
        size.width * 0.6855900,
        size.height * 0.4991533,
        size.width * 0.6870425,
        size.height * 0.4991533)
    ..cubicTo(
        size.width * 0.6881825,
        size.height * 0.4991533,
        size.width * 0.6877550,
        size.height * 0.5016364,
        size.width * 0.6853300,
        size.height * 0.5090708)
    ..cubicTo(
        size.width * 0.6814050,
        size.height * 0.5211089,
        size.width * 0.6717675,
        size.height * 0.5611365,
        size.width * 0.6717675,
        size.height * 0.5654074)
    ..cubicTo(
        size.width * 0.6717675,
        size.height * 0.5656656,
        size.width * 0.6748125,
        size.height * 0.5658792,
        size.width * 0.6785325,
        size.height * 0.5658792)
    ..lineTo(size.width * 0.6852950, size.height * 0.5658792)
    ..lineTo(size.width * 0.6871625, size.height * 0.5617523)
    ..arcToPoint(Offset(size.width * 0.6906800, size.height * 0.5528677),
        radius:
            Radius.elliptical(size.width * 0.05332500, size.height * 0.1059292),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..lineTo(size.width * 0.6923300, size.height * 0.5481051)
    ..lineTo(size.width * 0.6907950, size.height * 0.5449566)
    ..cubicTo(
        size.width * 0.6873250,
        size.height * 0.5378450,
        size.width * 0.6883375,
        size.height * 0.5329384,
        size.width * 0.6932725,
        size.height * 0.5329384)
    ..lineTo(size.width * 0.6964750, size.height * 0.5329384)
    ..lineTo(size.width * 0.6975675, size.height * 0.5280814)
    ..cubicTo(
        size.width * 0.6986675,
        size.height * 0.5231897,
        size.width * 0.7011900,
        size.height * 0.5090807,
        size.width * 0.7009975,
        size.height * 0.5089019)
    ..cubicTo(
        size.width * 0.6940450,
        size.height * 0.5024459,
        size.width * 0.6934450,
        size.height * 0.4923992,
        size.width * 0.7000150,
        size.height * 0.4923992)
    ..lineTo(size.width * 0.7023800, size.height * 0.4923992)
    ..lineTo(size.width * 0.7023800, size.height * 0.4750274)
    ..cubicTo(
        size.width * 0.7023800,
        size.height * 0.4553364,
        size.width * 0.7027625,
        size.height * 0.4568809,
        size.width * 0.6987700,
        size.height * 0.4603871)
    ..cubicTo(
        size.width * 0.6969850,
        size.height * 0.4619564,
        size.width * 0.6969400,
        size.height * 0.4620855,
        size.width * 0.6971575,
        size.height * 0.4652738)
    ..cubicTo(
        size.width * 0.6975850,
        size.height * 0.4716107,
        size.width * 0.6951800,
        size.height * 0.4738058,
        size.width * 0.6919450,
        size.height * 0.4700364)
    ..lineTo(size.width * 0.6901500, size.height * 0.4679456)
    ..lineTo(size.width * 0.6875900, size.height * 0.4712333)
    ..cubicTo(
        size.width * 0.6861825,
        size.height * 0.4730410,
        size.width * 0.6842775,
        size.height * 0.4758717,
        size.width * 0.6833600,
        size.height * 0.4775205)
    ..lineTo(size.width * 0.6816900, size.height * 0.4805151)
    ..lineTo(size.width * 0.6821225, size.height * 0.4847215)
    ..cubicTo(
        size.width * 0.6828900,
        size.height * 0.4921708,
        size.width * 0.6810000,
        size.height * 0.4958954,
        size.width * 0.6779275,
        size.height * 0.4929852)
    ..lineTo(size.width * 0.6765250, size.height * 0.4916543)
    ..lineTo(size.width * 0.6750425, size.height * 0.4973058)
    ..cubicTo(
        size.width * 0.6742250,
        size.height * 0.5004147,
        size.width * 0.6732175,
        size.height * 0.5051425,
        size.width * 0.6728000,
        size.height * 0.5078094)
    ..cubicTo(
        size.width * 0.6720200,
        size.height * 0.5127805,
        size.width * 0.6717325,
        size.height * 0.5131927,
        size.width * 0.6697500,
        size.height * 0.5121349)
    ..cubicTo(
        size.width * 0.6690500,
        size.height * 0.5117625,
        size.width * 0.6687925,
        size.height * 0.5109977,
        size.width * 0.6687925,
        size.height * 0.5092843)
    ..cubicTo(
        size.width * 0.6687925,
        size.height * 0.5071638,
        size.width * 0.6686175,
        size.height * 0.5068956,
        size.width * 0.6668900,
        size.height * 0.5063791)
    ..cubicTo(
        size.width * 0.6619850,
        size.height * 0.5049190,
        size.width * 0.6620925,
        size.height * 0.4973009,
        size.width * 0.6670800,
        size.height * 0.4928313)
    ..lineTo(size.width * 0.6688850, size.height * 0.4912123)
    ..lineTo(size.width * 0.6686075, size.height * 0.4852578)
    ..arcToPoint(Offset(size.width * 0.6680775, size.height * 0.4752410),
        radius:
            Radius.elliptical(size.width * 0.2019325, size.height * 0.4011353),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..lineTo(size.width * 0.6678250, size.height * 0.4711737)
    ..lineTo(size.width * 0.6662900, size.height * 0.4716752)
    ..cubicTo(
        size.width * 0.6624875,
        size.height * 0.4729118,
        size.width * 0.6606550,
        size.height * 0.4725890,
        size.width * 0.6598175,
        size.height * 0.4705330)
    ..cubicTo(
        size.width * 0.6583975,
        size.height * 0.4670517,
        size.width * 0.6587725,
        size.height * 0.4653682,
        size.width * 0.6624725,
        size.height * 0.4585843)
    ..lineTo(size.width * 0.6659450, size.height * 0.4522177)
    ..lineTo(size.width * 0.6651900, size.height * 0.4476041)
    ..cubicTo(
        size.width * 0.6640925,
        size.height * 0.4408997,
        size.width * 0.6609450,
        size.height * 0.4291993,
        size.width * 0.6591100,
        size.height * 0.4249929)
    ..lineTo(size.width * 0.6575225, size.height * 0.4213577)
    ..lineTo(size.width * 0.6546225, size.height * 0.4243622)
    ..cubicTo(
        size.width * 0.6487525,
        size.height * 0.4304409,
        size.width * 0.6456925,
        size.height * 0.4247843,
        size.width * 0.6497225,
        size.height * 0.4152989)
    ..cubicTo(
        size.width * 0.6506900,
        size.height * 0.4130144,
        size.width * 0.6513050,
        size.height * 0.4108988,
        size.width * 0.6510875,
        size.height * 0.4105959)
    ..arcToPoint(Offset(size.width * 0.6451300, size.height * 0.4025953),
        radius:
            Radius.elliptical(size.width * 0.2769750, size.height * 0.5502058),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.6448075,
        size.height * 0.4022229,
        size.width * 0.6439075,
        size.height * 0.4054658,
        size.width * 0.6425125,
        size.height * 0.4120311)
    ..moveTo(size.width * 0.5464625, size.height * 0.4078645)
    ..cubicTo(
        size.width * 0.5427550,
        size.height * 0.4130740,
        size.width * 0.5425750,
        size.height * 0.4138488,
        size.width * 0.5450825,
        size.height * 0.4138488)
    ..cubicTo(
        size.width * 0.5467300,
        size.height * 0.4138488,
        size.width * 0.5465950,
        size.height * 0.4141120,
        size.width * 0.5495100,
        size.height * 0.4051927)
    ..cubicTo(
        size.width * 0.5504950,
        size.height * 0.4021732,
        size.width * 0.5505350,
        size.height * 0.4021384,
        size.width * 0.5464625,
        size.height * 0.4078645)
    ..moveTo(size.width * 0.4653475, size.height * 0.4507725)
    ..cubicTo(
        size.width * 0.4494475,
        size.height * 0.4602977,
        size.width * 0.4363625,
        size.height * 0.4682287,
        size.width * 0.4362700,
        size.height * 0.4684025)
    ..cubicTo(
        size.width * 0.4361150,
        size.height * 0.4686856,
        size.width * 0.4387075,
        size.height * 0.4839368,
        size.width * 0.4390400,
        size.height * 0.4847115)
    ..cubicTo(
        size.width * 0.4391200,
        size.height * 0.4848953,
        size.width * 0.4433025,
        size.height * 0.4826158,
        size.width * 0.4483325,
        size.height * 0.4796460)
    ..cubicTo(
        size.width * 0.4573800,
        size.height * 0.4742974,
        size.width * 0.4574850,
        size.height * 0.4742080,
        size.width * 0.4579350,
        size.height * 0.4715213)
    ..cubicTo(
        size.width * 0.4589100,
        size.height * 0.4657059,
        size.width * 0.4604725,
        size.height * 0.4643104,
        size.width * 0.4653175,
        size.height * 0.4649361)
    ..lineTo(size.width * 0.4696000, size.height * 0.4654923)
    ..lineTo(size.width * 0.4735700, size.height * 0.4616237)
    ..arcToPoint(Offset(size.width * 0.4822850, size.height * 0.4523964),
        radius:
            Radius.elliptical(size.width * 0.1991625, size.height * 0.3956327),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..arcToPoint(Offset(size.width * 0.4907525, size.height * 0.4430699),
        radius:
            Radius.elliptical(size.width * 0.5252350, size.height * 1.043370),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..cubicTo(
        size.width * 0.4940600,
        size.height * 0.4395389,
        size.width * 0.4957475,
        size.height * 0.4363854,
        size.width * 0.4957475,
        size.height * 0.4337384)
    ..cubicTo(
        size.width * 0.4957475,
        size.height * 0.4326756,
        size.width * 0.4929575,
        size.height * 0.4342400,
        size.width * 0.4653475,
        size.height * 0.4507725)
    ..moveTo(size.width * 0.5105050, size.height * 0.4338030)
    ..cubicTo(
        size.width * 0.5100250,
        size.height * 0.4347565,
        size.width * 0.5120850,
        size.height * 0.4408649,
        size.width * 0.5128900,
        size.height * 0.4408699)
    ..cubicTo(
        size.width * 0.5134275,
        size.height * 0.4408748,
        size.width * 0.5135950,
        size.height * 0.4404279,
        size.width * 0.5134425,
        size.height * 0.4393999)
    ..arcToPoint(Offset(size.width * 0.5129425, size.height * 0.4356007),
        radius: Radius.elliptical(
            size.width * 0.04340000, size.height * 0.08621332),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..cubicTo(
        size.width * 0.5126750,
        size.height * 0.4333808,
        size.width * 0.5112475,
        size.height * 0.4323280,
        size.width * 0.5105050,
        size.height * 0.4338030)
    ..moveTo(size.width * 0.8290825, size.height * 0.4388188)
    ..cubicTo(
        size.width * 0.8282625,
        size.height * 0.4394346,
        size.width * 0.8272875,
        size.height * 0.4399611,
        size.width * 0.8269150,
        size.height * 0.4399859)
    ..cubicTo(
        size.width * 0.8261500,
        size.height * 0.4400405,
        size.width * 0.8178425,
        size.height * 0.4578692,
        size.width * 0.8135150,
        size.height * 0.4687502)
    ..cubicTo(
        size.width * 0.7949400,
        size.height * 0.5154325,
        size.width * 0.7922525,
        size.height * 0.5489146,
        size.width * 0.7997575,
        size.height * 0.6401041)
    ..cubicTo(
        size.width * 0.8016950,
        size.height * 0.6636489,
        size.width * 0.8078775,
        size.height * 0.7240379,
        size.width * 0.8084875,
        size.height * 0.7253838)
    ..cubicTo(
        size.width * 0.8089900,
        size.height * 0.7264962,
        size.width * 0.8195925,
        size.height * 0.7281052,
        size.width * 0.8265300,
        size.height * 0.7281201)
    ..cubicTo(
        size.width * 0.8417025,
        size.height * 0.7281648,
        size.width * 0.8523725,
        size.height * 0.7233377,
        size.width * 0.8611825,
        size.height * 0.7124369)
    ..cubicTo(
        size.width * 0.8663175,
        size.height * 0.7060851,
        size.width * 0.8659225,
        size.height * 0.7084242,
        size.width * 0.8665100,
        size.height * 0.6807425)
    ..cubicTo(
        size.width * 0.8668600,
        size.height * 0.6642647,
        size.width * 0.8680100,
        size.height * 0.6360715,
        size.width * 0.8701600,
        size.height * 0.5913558)
    ..cubicTo(
        size.width * 0.8718875,
        size.height * 0.5554353,
        size.width * 0.8733000,
        size.height * 0.5258466,
        size.width * 0.8733000,
        size.height * 0.5256033)
    ..cubicTo(
        size.width * 0.8733000,
        size.height * 0.5253649,
        size.width * 0.8722400,
        size.height * 0.5270931,
        size.width * 0.8709425,
        size.height * 0.5294471)
    ..cubicTo(
        size.width * 0.8636425,
        size.height * 0.5427019,
        size.width * 0.8562600,
        size.height * 0.5328043,
        size.width * 0.8527150,
        size.height * 0.5050084)
    ..cubicTo(
        size.width * 0.8516325,
        size.height * 0.4965112,
        size.width * 0.8503400,
        size.height * 0.4807088,
        size.width * 0.8503400,
        size.height * 0.4759512)
    ..cubicTo(
        size.width * 0.8503400,
        size.height * 0.4651894,
        size.width * 0.8468650,
        size.height * 0.4757227,
        size.width * 0.8395525,
        size.height * 0.5086387)
    ..cubicTo(
        size.width * 0.8295350,
        size.height * 0.5537319,
        size.width * 0.8251125,
        size.height * 0.5658891,
        size.width * 0.8199500,
        size.height * 0.5625071)
    ..cubicTo(
        size.width * 0.8121775,
        size.height * 0.5574118,
        size.width * 0.8084600,
        size.height * 0.5128948,
        size.width * 0.8138250,
        size.height * 0.4891513)
    ..cubicTo(
        size.width * 0.8161000,
        size.height * 0.4790699,
        size.width * 0.8227050,
        size.height * 0.4639528,
        size.width * 0.8277000,
        size.height * 0.4573875)
    ..cubicTo(
        size.width * 0.8293300,
        size.height * 0.4552470,
        size.width * 0.8294275,
        size.height * 0.4548497,
        size.width * 0.8301150,
        size.height * 0.4478027)
    ..cubicTo(
        size.width * 0.8305100,
        size.height * 0.4437602,
        size.width * 0.8309300,
        size.height * 0.4397922,
        size.width * 0.8310475,
        size.height * 0.4389778)
    ..cubicTo(
        size.width * 0.8312950,
        size.height * 0.4372892,
        size.width * 0.8311325,
        size.height * 0.4372793,
        size.width * 0.8290825,
        size.height * 0.4388188)
    ..moveTo(size.width * 0.5131875, size.height * 0.4448875)
    ..cubicTo(
        size.width * 0.5132050,
        size.height * 0.4467399,
        size.width * 0.5140175,
        size.height * 0.4479716,
        size.width * 0.5140250,
        size.height * 0.4461539)
    ..cubicTo(
        size.width * 0.5140275,
        size.height * 0.4451110,
        size.width * 0.5138400,
        size.height * 0.4442568,
        size.width * 0.5136050,
        size.height * 0.4442568)
    ..cubicTo(
        size.width * 0.5133725,
        size.height * 0.4442568,
        size.width * 0.5131825,
        size.height * 0.4445399,
        size.width * 0.5131875,
        size.height * 0.4448875)
    ..moveTo(size.width * 0.3928675, size.height * 0.4548547)
    ..cubicTo(
        size.width * 0.3991250,
        size.height * 0.4674142,
        size.width * 0.3988275,
        size.height * 0.4671212,
        size.width * 0.4118000,
        size.height * 0.4733886)
    ..cubicTo(
        size.width * 0.4181000,
        size.height * 0.4764329,
        size.width * 0.4235450,
        size.height * 0.4790997,
        size.width * 0.4239000,
        size.height * 0.4793182)
    ..cubicTo(
        size.width * 0.4245625,
        size.height * 0.4797255,
        size.width * 0.4281775,
        size.height * 0.4676824,
        size.width * 0.4276850,
        size.height * 0.4667041)
    ..cubicTo(
        size.width * 0.4273775,
        size.height * 0.4660982,
        size.width * 0.3913350,
        size.height * 0.4484781,
        size.width * 0.3903975,
        size.height * 0.4484781)
    ..cubicTo(
        size.width * 0.3899550,
        size.height * 0.4484781,
        size.width * 0.3908875,
        size.height * 0.4508817,
        size.width * 0.3928675,
        size.height * 0.4548547)
    ..moveTo(size.width * 0.5244925, size.height * 0.4522177)
    ..cubicTo(
        size.width * 0.5241500,
        size.height * 0.4549391,
        size.width * 0.5242550,
        size.height * 0.4554109,
        size.width * 0.5250050,
        size.height * 0.4544922)
    ..cubicTo(
        size.width * 0.5253700,
        size.height * 0.4540403,
        size.width * 0.5254200,
        size.height * 0.4532159,
        size.width * 0.5251650,
        size.height * 0.4518402)
    ..lineTo(size.width * 0.5247925, size.height * 0.4498438)
    ..lineTo(size.width * 0.5244925, size.height * 0.4522177)
    ..moveTo(size.width * 0.5243300, size.height * 0.4583360)
    ..cubicTo(
        size.width * 0.5240300,
        size.height * 0.4585744,
        size.width * 0.5234150,
        size.height * 0.4602132,
        size.width * 0.5229625,
        size.height * 0.4619812)
    ..cubicTo(
        size.width * 0.5219575,
        size.height * 0.4658946,
        size.width * 0.5193475,
        size.height * 0.4704585,
        size.width * 0.5159450,
        size.height * 0.4742626)
    ..cubicTo(
        size.width * 0.5139425,
        size.height * 0.4764974,
        size.width * 0.5078500,
        size.height * 0.4871897,
        size.width * 0.5095675,
        size.height * 0.4854515)
    ..cubicTo(
        size.width * 0.5098000,
        size.height * 0.4852131,
        size.width * 0.5137225,
        size.height * 0.4828343,
        size.width * 0.5182825,
        size.height * 0.4801575)
    ..cubicTo(
        size.width * 0.5282500,
        size.height * 0.4743123,
        size.width * 0.5273225,
        size.height * 0.4759164,
        size.width * 0.5264925,
        size.height * 0.4659939)
    ..cubicTo(
        size.width * 0.5258325,
        size.height * 0.4581076,
        size.width * 0.5256075,
        size.height * 0.4573130,
        size.width * 0.5243300,
        size.height * 0.4583360)
    ..moveTo(size.width * 0.3885100, size.height * 0.4812054)
    ..cubicTo(
        size.width * 0.3867475,
        size.height * 0.4885206,
        size.width * 0.3851850,
        size.height * 0.4952945,
        size.width * 0.3850400,
        size.height * 0.4962530)
    ..cubicTo(
        size.width * 0.3847150,
        size.height * 0.4983736,
        size.width * 0.4112450,
        size.height * 0.5136446,
        size.width * 0.4121450,
        size.height * 0.5118518)
    ..cubicTo(
        size.width * 0.4124750,
        size.height * 0.5111963,
        size.width * 0.4103225,
        size.height * 0.5064784,
        size.width * 0.4045075,
        size.height * 0.4951207)
    ..cubicTo(
        size.width * 0.4000525,
        size.height * 0.4864249,
        size.width * 0.3954775,
        size.height * 0.4767408,
        size.width * 0.3943375,
        size.height * 0.4736071)
    ..cubicTo(
        size.width * 0.3917200,
        size.height * 0.4663962,
        size.width * 0.3922400,
        size.height * 0.4657158,
        size.width * 0.3885100,
        size.height * 0.4812054)
    ..moveTo(size.width * 0.4712625, size.height * 0.4732893)
    ..cubicTo(
        size.width * 0.4707525,
        size.height * 0.4759263,
        size.width * 0.4712675,
        size.height * 0.4776148,
        size.width * 0.4725825,
        size.height * 0.4776148)
    ..cubicTo(
        size.width * 0.4737100,
        size.height * 0.4776148,
        size.width * 0.4738525,
        size.height * 0.4773318,
        size.width * 0.4738525,
        size.height * 0.4750821)
    ..cubicTo(
        size.width * 0.4738525,
        size.height * 0.4722017,
        size.width * 0.4717525,
        size.height * 0.4707466,
        size.width * 0.4712625,
        size.height * 0.4732893)
    ..moveTo(size.width * 0.4047625, size.height * 0.4734085)
    ..cubicTo(
        size.width * 0.4047625,
        size.height * 0.4745656,
        size.width * 0.4137450,
        size.height * 0.4829336,
        size.width * 0.4191075,
        size.height * 0.4867725)
    ..lineTo(size.width * 0.4221875, size.height * 0.4889775)
    ..lineTo(size.width * 0.4230350, size.height * 0.4856799)
    ..lineTo(size.width * 0.4238825, size.height * 0.4823774)
    ..lineTo(size.width * 0.4216575, size.height * 0.4814438)
    ..cubicTo(
        size.width * 0.4204325,
        size.height * 0.4809323,
        size.width * 0.4161300,
        size.height * 0.4788613,
        size.width * 0.4120950,
        size.height * 0.4768401)
    ..cubicTo(
        size.width * 0.4080625,
        size.height * 0.4748189,
        size.width * 0.4047625,
        size.height * 0.4732744,
        size.width * 0.4047625,
        size.height * 0.4734085)
    ..moveTo(size.width * 0.4480375, size.height * 0.4829932)
    ..cubicTo(
        size.width * 0.4436625,
        size.height * 0.4856551,
        size.width * 0.4399800,
        size.height * 0.4880339,
        size.width * 0.4398550,
        size.height * 0.4882822)
    ..cubicTo(
        size.width * 0.4396500,
        size.height * 0.4886895,
        size.width * 0.4420075,
        size.height * 0.5024757,
        size.width * 0.4424225,
        size.height * 0.5033050)
    ..cubicTo(
        size.width * 0.4430700,
        size.height * 0.5045863,
        size.width * 0.4511625,
        size.height * 0.4984282,
        size.width * 0.4539275,
        size.height * 0.4945496)
    ..arcToPoint(Offset(size.width * 0.4576850, size.height * 0.4894344),
        radius:
            Radius.elliptical(size.width * 0.1071175, size.height * 0.2127870),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..cubicTo(
        size.width * 0.4586625,
        size.height * 0.4882872,
        size.width * 0.4577275,
        size.height * 0.4780022,
        size.width * 0.4566550,
        size.height * 0.4780966)
    ..cubicTo(
        size.width * 0.4562925,
        size.height * 0.4781264,
        size.width * 0.4524150,
        size.height * 0.4803313,
        size.width * 0.4480375,
        size.height * 0.4829932)
    ..moveTo(size.width * 0.5106300, size.height * 0.4880190)
    ..cubicTo(
        size.width * 0.4913350,
        size.height * 0.4996598,
        size.width * 0.4911250,
        size.height * 0.4998485,
        size.width * 0.4893175,
        size.height * 0.5069800)
    ..cubicTo(
        size.width * 0.4885850,
        size.height * 0.5098753,
        size.width * 0.4873925,
        size.height * 0.5137390,
        size.width * 0.4866700,
        size.height * 0.5155616)
    ..cubicTo(
        size.width * 0.4855650,
        size.height * 0.5183427,
        size.width * 0.4854900,
        size.height * 0.5188045,
        size.width * 0.4861925,
        size.height * 0.5184271)
    ..cubicTo(
        size.width * 0.4992600,
        size.height * 0.5114099,
        size.width * 0.5288950,
        size.height * 0.4927518,
        size.width * 0.5287375,
        size.height * 0.4916344)
    ..arcToPoint(Offset(size.width * 0.5280550, size.height * 0.4841603),
        radius:
            Radius.elliptical(size.width * 0.06605000, size.height * 0.1312071),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..cubicTo(
        size.width * 0.5277825,
        size.height * 0.4807932,
        size.width * 0.5274325,
        size.height * 0.4780618,
        size.width * 0.5272800,
        size.height * 0.4780916)
    ..cubicTo(
        size.width * 0.5271250,
        size.height * 0.4781214,
        size.width * 0.5196325,
        size.height * 0.4825860,
        size.width * 0.5106300,
        size.height * 0.4880190)
    ..moveTo(size.width * 0.5072375, size.height * 0.4815133)
    ..cubicTo(
        size.width * 0.5064200,
        size.height * 0.4821639,
        size.width * 0.5063175,
        size.height * 0.4825115,
        size.width * 0.5067775,
        size.height * 0.4830777)
    ..cubicTo(
        size.width * 0.5072925,
        size.height * 0.4837084,
        size.width * 0.5093525,
        size.height * 0.4819652,
        size.width * 0.5093525,
        size.height * 0.4808975)
    ..cubicTo(
        size.width * 0.5093525,
        size.height * 0.4803462,
        size.width * 0.5083400,
        size.height * 0.4806392,
        size.width * 0.5072375,
        size.height * 0.4815133)
    ..moveTo(size.width * 0.5021600, size.height * 0.4874777)
    ..cubicTo(
        size.width * 0.4994925,
        size.height * 0.4912173,
        size.width * 0.4995575,
        size.height * 0.4912918,
        size.width * 0.5034625,
        size.height * 0.4889825)
    ..cubicTo(
        size.width * 0.5067600,
        size.height * 0.4870308,
        size.width * 0.5067150,
        size.height * 0.4871102,
        size.width * 0.5054200,
        size.height * 0.4854664)
    ..cubicTo(
        size.width * 0.5045375,
        size.height * 0.4843391,
        size.width * 0.5042900,
        size.height * 0.4844930,
        size.width * 0.5021600,
        size.height * 0.4874777)
    ..moveTo(size.width * 0.5048900, size.height * 0.5105060)
    ..arcToPoint(Offset(size.width * 0.4785300, size.height * 0.5264227),
        radius:
            Radius.elliptical(size.width * 5.654443, size.height * 11.23245),
        rotation: 0,
        largeArc: false,
        clockwise: false)
    ..cubicTo(
        size.width * 0.4766575,
        size.height * 0.5275947,
        size.width * 0.4693875,
        size.height * 0.5318855,
        size.width * 0.4623725,
        size.height * 0.5359578)
    ..cubicTo(
        size.width * 0.4474925,
        size.height * 0.5445990,
        size.width * 0.4492875,
        size.height * 0.5431936,
        size.width * 0.4497900,
        size.height * 0.5458257)
    ..cubicTo(
        size.width * 0.4502050,
        size.height * 0.5479909,
        size.width * 0.4554300,
        size.height * 0.5511544,
        size.width * 0.4613125,
        size.height * 0.5528032)
    ..cubicTo(
        size.width * 0.4628575,
        size.height * 0.5532352,
        size.width * 0.5293425,
        size.height * 0.5151444,
        size.width * 0.5304900,
        size.height * 0.5131729)
    ..cubicTo(
        size.width * 0.5309650,
        size.height * 0.5123485,
        size.width * 0.5290975,
        size.height * 0.4966304,
        size.width * 0.5285275,
        size.height * 0.4966702)
    ..cubicTo(
        size.width * 0.5281550,
        size.height * 0.4967000,
        size.width * 0.5175175,
        size.height * 0.5029276,
        size.width * 0.5048900,
        size.height * 0.5105060)
    ..moveTo(size.width * 0.4985125, size.height * 0.5355456)
    ..arcToPoint(Offset(size.width * 0.4687500, size.height * 0.5533743),
        radius:
            Radius.elliptical(size.width * 39.03795, size.height * 77.54818),
        rotation: 0,
        largeArc: false,
        clockwise: true)
    ..lineTo(size.width * 0.4670500, size.height * 0.5543874)
    ..lineTo(size.width * 0.4693875, size.height * 0.5549784)
    ..cubicTo(
        size.width * 0.4897050,
        size.height * 0.5601482,
        size.width * 0.5106900,
        size.height * 0.5475986,
        size.width * 0.5265725,
        size.height * 0.5207960)
    ..cubicTo(
        size.width * 0.5284900,
        size.height * 0.5175580,
        size.width * 0.5287025,
        size.height * 0.5174488,
        size.width * 0.4985125,
        size.height * 0.5355456)
    ..moveTo(size.width * 0.4597100, size.height * 0.5312995)
    ..cubicTo(
        size.width * 0.4580775,
        size.height * 0.5319401,
        size.width * 0.4519900,
        size.height * 0.5380039,
        size.width * 0.4530175,
        size.height * 0.5379642)
    ..cubicTo(
        size.width * 0.4532525,
        size.height * 0.5379542,
        size.width * 0.4562175,
        size.height * 0.5362707,
        size.width * 0.4596100,
        size.height * 0.5342196)
    ..cubicTo(
        size.width * 0.4657225,
        size.height * 0.5305198,
        size.width * 0.4657775,
        size.height * 0.5289207,
        size.width * 0.4597100,
        size.height * 0.5312995)
    ..moveTo(size.width * 0.6360550, size.height * 0.5485769)
    ..cubicTo(
        size.width * 0.6360550,
        size.height * 0.5497440,
        size.width * 0.6354800,
        size.height * 0.5527187,
        size.width * 0.6347800,
        size.height * 0.5551919)
    ..cubicTo(
        size.width * 0.6331925,
        size.height * 0.5607739,
        size.width * 0.6331675,
        size.height * 0.5603667,
        size.width * 0.6352200,
        size.height * 0.5629392)
    ..cubicTo(
        size.width * 0.6367925,
        size.height * 0.5649108,
        size.width * 0.6372700,
        size.height * 0.5650747,
        size.width * 0.6409200,
        size.height * 0.5648512)
    ..lineTo(size.width * 0.6449025, size.height * 0.5646078)
    ..lineTo(size.width * 0.6404775, size.height * 0.5555346)
    ..cubicTo(
        size.width * 0.6366825,
        size.height * 0.5477476,
        size.width * 0.6360550,
        size.height * 0.5467593,
        size.width * 0.6360550,
        size.height * 0.5485769)
    ..moveTo(size.width * 0.5625000, size.height * 0.6681979)
    ..cubicTo(
        size.width * 0.5602775,
        size.height * 0.6719176,
        size.width * 0.5555525,
        size.height * 0.6794464,
        size.width * 0.5519975,
        size.height * 0.6849241)
    ..lineTo(size.width * 0.5455350, size.height * 0.6948913)
    ..lineTo(size.width * 0.5471100, size.height * 0.6954078)
    ..cubicTo(
        size.width * 0.5522625,
        size.height * 0.6971012,
        size.width * 0.5643575,
        size.height * 0.6953631,
        size.width * 0.5671775,
        size.height * 0.6925274)
    ..cubicTo(
        size.width * 0.5731975,
        size.height * 0.6864686,
        size.width * 0.5747275,
        size.height * 0.6686896,
        size.width * 0.5697075,
        size.height * 0.6630827)
    ..cubicTo(
        size.width * 0.5673750,
        size.height * 0.6604804,
        size.width * 0.5669200,
        size.height * 0.6608032,
        size.width * 0.5625000,
        size.height * 0.6681979)
    ..moveTo(size.width * 0.4857575, size.height * 0.7133556)
    ..cubicTo(
        size.width * 0.4816525,
        size.height * 0.7172690,
        size.width * 0.4774000,
        size.height * 0.7191959,
        size.width * 0.4674750,
        size.height * 0.7216492)
    ..cubicTo(
        size.width * 0.4507325,
        size.height * 0.7257860,
        size.width * 0.4464025,
        size.height * 0.7341739,
        size.width * 0.4522675,
        size.height * 0.7511087)
    ..cubicTo(
        size.width * 0.4572325,
        size.height * 0.7654362,
        size.width * 0.4778000,
        size.height * 0.7593576,
        size.width * 0.4937250,
        size.height * 0.7388571)
    ..lineTo(size.width * 0.4965900, size.height * 0.7351672)
    ..lineTo(size.width * 0.4929375, size.height * 0.7233675)
    ..cubicTo(
        size.width * 0.4909300,
        size.height * 0.7168767,
        size.width * 0.4891600,
        size.height * 0.7113344,
        size.width * 0.4890100,
        size.height * 0.7110463)
    ..cubicTo(
        size.width * 0.4888575,
        size.height * 0.7107583,
        size.width * 0.4873925,
        size.height * 0.7117962,
        size.width * 0.4857575,
        size.height * 0.7133556)
    ..moveTo(size.width * 0.1356850, size.height * 0.7581210)
    ..cubicTo(
        size.width * 0.1344550,
        size.height * 0.7687487,
        size.width * 0.1333750,
        size.height * 0.7778914,
        size.width * 0.1332875,
        size.height * 0.7784377)
    ..cubicTo(
        size.width * 0.1331775,
        size.height * 0.7791330,
        size.width * 0.1337800,
        size.height * 0.7793267,
        size.width * 0.1353375,
        size.height * 0.7790833)
    ..cubicTo(
        size.width * 0.1365500,
        size.height * 0.7788946,
        size.width * 0.1447175,
        size.height * 0.7783185,
        size.width * 0.1534875,
        size.height * 0.7778120)
    ..cubicTo(
        size.width * 0.1622550,
        size.height * 0.7773005,
        size.width * 0.1695500,
        size.height * 0.7766300,
        size.width * 0.1696975,
        size.height * 0.7763221)
    ..cubicTo(
        size.width * 0.1700850,
        size.height * 0.7754977,
        size.width * 0.1726200,
        size.height * 0.7599436,
        size.width * 0.1726200,
        size.height * 0.7583792)
    ..cubicTo(
        size.width * 0.1726200,
        size.height * 0.7568993,
        size.width * 0.1678725,
        size.height * 0.7512776,
        size.width * 0.1639025,
        size.height * 0.7480595)
    ..cubicTo(
        size.width * 0.1566650,
        size.height * 0.7421993,
        size.width * 0.1512825,
        size.height * 0.7398503,
        size.width * 0.1436875,
        size.height * 0.7392494)
    ..lineTo(size.width * 0.1379250, size.height * 0.7387925)
    ..lineTo(size.width * 0.1356850, size.height * 0.7581210);
}

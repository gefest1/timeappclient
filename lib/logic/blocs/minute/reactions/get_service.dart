part of '../MinuteInstiutionBloc.dart';

extension GetServiceReaction on MinuteInstitutionBloc {
  void getService(
    GetServiceEvent event,
    Emitter<MinuteInstitutionState> emit,
  ) async {
    routerDelegate.blockWidgets();
    try {
      final _mutationResult = await graphqlProvider.client.mutate(
        MutationOptions(
          document: gql('''
          query (\$serviceId: String!){
            getServiceById(serviceId:  \$serviceId) {
              _id
              endTime
              bufferTimeMinutes
              institution {
                _id
                iconPath
                averagePrice
                avatarURL {
                  M
                  thumbnail
                  XL
                }
                city
                dateAdded
                description
                galleryURLs {
                  M
                  thumbnail
                  XL
                }
                name
                rating
                ratingCount
                location {
                  address
                  latitude
                  longitude
                }
                type
              } 
              name
              price
              startTime
            }
          }
          '''),
          variables: {
            'serviceId': event.serviceId,
          },
        ),
      );

      if (_mutationResult.hasException) throw _mutationResult.exception!;

      final ServiceMinute _data = ServiceMinute.fromMap(
        (_mutationResult.data!['getServiceById'] as Map<String, dynamic>?),
      )!;

      emit(FetchedService([_data]));
    } catch (e) {
      print(e.toString());
    }
    event.state.complete(state);
    routerDelegate.unBlockWidgets();
  }
}

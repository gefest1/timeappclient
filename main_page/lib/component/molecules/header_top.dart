import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:time_app_components/utils/color.dart';

import 'package:li/components/pages/search_main_page.dart';
import 'package:li/main.dart';

class HeaderTop extends StatelessWidget {
  const HeaderTop({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(width: 20),
              SvgPicture.asset('assets/svg/icon/locationL.svg'),
              const SizedBox(width: 4),
              const Text(
                'Сейфуллина, 120',
                style: TextStyle(
                  fontSize: 18,
                  height: 21 / 18,
                  fontWeight: FontWeight.w400,
                  color: ColorData.clientsButtonColorPressed,
                ),
              ),
              const SizedBox(width: 4),
              SvgPicture.asset('assets/svg/icon/arrowChevronDown.svg'),
            ],
          ),
        ),
        IconButton(
          onPressed: () {
            routerDelegate.push(const SearchMainPage());
          },
          icon: SvgPicture.asset(
            'assets/svg/icon/searchMain.svg',
          ),
        ),
      ],
    );
  }
}

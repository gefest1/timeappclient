import 'package:flutter/material.dart';
import 'package:li/components/atoms/instagram_contact.dart';
import 'package:li/components/atoms/phone_contact.dart';
import 'package:li/components/atoms/telegram_contact.dart';
import 'package:li/components/atoms/whatsapp_contact.dart';
import 'package:time_app_components/utils/color.dart';
import 'package:time_app_components/utils/testStyles.dart';

class ContactCard extends StatelessWidget {
  const ContactCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'Контакты',
          style: TextStyle(
            fontSize: H2TextStyle.fontSize,
            fontWeight: H2TextStyle.fontWeight,
            height: H2TextStyle.height,
            color: ColorData.allMainBlack,
          ),
        ),
        const SizedBox(height: 10),
        Row(
          children: [
            Expanded(
              child: PhoneContact(),
            ),
            const SizedBox(width: 10),
            Expanded(
              child: InstagramContact(),
            ),
          ],
        ),
        const SizedBox(height: 10),
        Row(
          children: [
            Expanded(
              child: WhatsAppContact(),
            ),
            const SizedBox(width: 10),
            Expanded(
              child: TelegramContact(),
            ),
          ],
        ),
      ],
    );
  }
}

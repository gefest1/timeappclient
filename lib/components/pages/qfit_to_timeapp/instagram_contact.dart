import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:li/components/molecules/size_tap_animation.dart';
import 'package:li/utils/color.dart';
import 'package:li/utils/launch_universal_link.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../const.dart';

class InstagramContact extends StatelessWidget {
  final String label;
  const InstagramContact({required this.label, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: GestureDetector(
        onTap: () {
          if (Platform.isIOS)
            launchUniversalLinkIos(
              'https://www.instagram.com/$label/',
            );
          if (Platform.isAndroid) launch('https://www.instagram.com/$label/');
        },
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(16)),
            boxShadow: bigBlur,
          ),
          padding: EdgeInsets.all(16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              RepaintBoundary(
                child: SvgPicture.asset('assets/svg/icon/instagram.svg'),
              ),
              Text(
                'Instagram',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  color: ColorData.allMainBlack,
                ),
              ),
              const SizedBox(height: 2),
              Text(
                '$label',
                style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 16,
                    color: ColorData.allMainBlack,
                    overflow: TextOverflow.ellipsis),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

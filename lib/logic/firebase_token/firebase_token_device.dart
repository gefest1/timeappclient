import 'dart:developer';

import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:li/network/graphql_provider.dart';
import 'package:device_info/device_info.dart';
import 'dart:io';

Future<void> sendId({
  required String fireBaseToken,
  required String deviceId,
  required GraphqlProvider provider,
}) async {
  final mutationResult = await provider.client.mutate(
    MutationOptions(
      document: gql('''    mutation
        (\$token: String!, \$deviceId: String!)
      {
        	saveFireBaseToken(deviceInfo: \$deviceId, fireBaseToken: \$token) {
         _id
       }
      }
        '''),
      variables: {
        'token': fireBaseToken,
        'deviceId': deviceId,
      },
    ),
  );
  if (mutationResult.hasException) throw mutationResult.exception!;
}

Future<String> getId() async {
  var deviceInfo = DeviceInfoPlugin();
  if (Platform.isIOS) {
    var iosDeviceInfo = await deviceInfo.iosInfo;
    return iosDeviceInfo.identifierForVendor;
  } else {
    var androidDeviceInfo = await deviceInfo.androidInfo;
    return androidDeviceInfo.androidId;
  }
}

Future<void> deleteID({
  required String deviceId,
  required GraphqlProvider provider,
}) async {
  try {
    final mutationResult = await provider.client.mutate(
      MutationOptions(
        document: gql('''   mutation(\$deviceId: String!){
        	removeFireBaseTokenWithDeviceId(deviceInfo: \$deviceId) {
         _id
       }
     }
        '''),
        variables: {
          'deviceId': deviceId,
        },
      ),
    );
    if (mutationResult.hasException) throw mutationResult.exception!;
  } catch (e) {
    log(e.toString());
  }
}
